<?php
/**
 * Created by PhpStorm.
 * User: Hugh
 * Date: 2019/11/04
 * Time: 13:40
 */

namespace app\index\controller;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class B2bNews extends BookingBase
{
    public function index(){

        $data['status'] = 1;
        $data['company_id'] =  session('booking_user')['company_id'];

        $_GET['page'] = 1;
        $data['page'] = $_GET['page'];

        $result = $this->callSoaErp('post', '/btob/getB2bNews',$data);
        $_GET['news_id'] = $result['data']?$result['data'][0]['news_id']:0;
//        var_dump($result['data'][0]);exit;
        $this->assign('news',$result['data']);
        $this->assign('all_news',$result['data']);
        unset($result);

        $result = $this->callSoaErp('post', '/btob/getb2bNewsCount',$data);
        $this->assign('countNews',$result['data']);

        $this->assign('site_title','Home');
        $links = $this->links($result['data'],$_GET['page'],8);
        $this->assign('links',$links);

//        var_dump($links);exit;

        return $this->fetch('index');
    }


    public function article(){
        // 获取新闻内容
        $data['status'] = 1;
        $data['company_id'] =  session('booking_user')['company_id'];
        $data['news_id'] = $_GET['news_id'];
        $result = $this->callSoaErp('post','/btob/getB2bNews',$data);
        $this->assign('news',$result['data']);
        unset($data);unset($result);

        //获取新闻列表
        $data['status'] = 1;
        $data['company_id'] =  session('booking_user')['company_id'];
        $data['page'] = $_GET['page'];
        $result = $this->callSoaErp('post', '/btob/getB2bNews',$data);
        $this->assign('all_news',$result['data']);
        unset($data);unset($result);

        $data['company_id'] =  session('booking_user')['company_id'];
        $result = $this->callSoaErp('post', '/btob/getb2bNewsCount',$data);
        $this->assign('countNews',$result['data']);

        $links = $this->links($result['data'],$_GET['page'],8);
        $this->assign('links',$links);
        $this->assign('site_title','Home');
        return $this->fetch('index');
    }



    public function links($count,$page,$pageSize){
        $pageC = ceil($count/$pageSize); //最大页数
        $topPage = $page-1; //前一页
        $endPage= $page+1; //后一页
        $linkH = '';

        if($page>=4){
            $linkH .= "<a href='/b2b_news/article?news_id={$_GET['news_id']}&page=1'>‹ First</a>";
        }
        if($page!=1){
            $linkH .= "<a href='/b2b_news/article?news_id={$_GET['news_id']}&page={$topPage}'>&nbsp;<</a>";
        }

         if($page>5){
             if($page>5){
                for($i=$page-5;$i<$page;$i++){
                    if($i==$page){
                        $linkH .= "<strong>&nbsp;{$i}</strong>";
                    }else{
                        $linkH .= "<a href='/b2b_news/article?news_id={$_GET['news_id']}&page={$i}'>&nbsp;{$i}</a>";
                    }
                }
             }
         }else{
             for($i=1;$i<$page;$i++){
                 $linkH .= "<a href='/b2b_news/article?news_id={$_GET['news_id']}&page={$i}'>&nbsp;{$i}</a>";
             }
         }

        if(($pageC-$page)>5){
            for($i=$page;$i<=$page+5;$i++){
                if($i==$page){
                    $linkH .= "<strong>&nbsp;{$i}</strong>";
                }else{
                    $linkH .= "<a href='/b2b_news/article?news_id={$_GET['news_id']}&page={$i}'>&nbsp;{$i}</a>";
                }
            }

            $linkH .= "<a href='/b2b_news/article?news_id={$_GET['news_id']}&page={$endPage}'>&nbsp;<</a>";
            $linkH .= "<a href='/b2b_news/article?news_id={$_GET['news_id']}&page={$pageC}'>&nbsp;Last ›</a>";
        }else{
            for($i=$page;$i<=$pageC;$i++){
                if($i==$page){
                    $linkH .= "<strong>&nbsp;{$i}</strong>";
                }else{
                    $linkH .= "<a href='/b2b_news/article?news_id={$_GET['news_id']}&page={$i}'>&nbsp;{$i}</a>";
                }
            }
        }



        return $linkH;
    }

}