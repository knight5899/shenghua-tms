<?php

namespace app\index\controller;

use app\common\help\Contents;
use app\common\help\Help;
use think\Cache;
use think\Request;

class Form extends Base
{
    /**
     *  圣华
     *
     */
    public function index(){

		return $this->fetch('index');

    }

    /**
     *  圣华
     *  运单报表
     */
    public function showOrderFormManage(){

        //读取所有的项目

        $params = Request::instance()->param();

        $data = $params;

        //$projectResult = $this->callSoaErp('post', '/source/getProject', $data);
        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        $multi_project_id = input('multi_project_id');
        if (!empty($multi_project_id))
            $projectResult = $this->callSoaErp('post', '/source/getProject', ['multi_project_id' => $multi_project_id]);


        $this->assign('projectResult', $projectResult['data']);

        //获取所有承运商
        $supplierParams['status'] = 1;
        $supplierResult = "";
        if (!empty($params['supplier_uuid'])) {
            $supplierParams['supplier_uuid'] = $params['supplier_uuid'];

            $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);


        }
        $this->assign('supplierResult', $supplierResult['data']);
        $data['page'] = $this->page();
        $data['page_size'] = $params['limit'] ? $params['limit'] : 30;
        if (isset($params['download'])) {
            $data['page_size']=Contents::PAGE_SIZE_MAX;
        }
        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        } else
            $data['status'] = 1;

        if (!empty($params['accept_goods_company'])) {
            $data['accept_goods_company'] = $params['accept_goods_company'];
        }
        if (!empty($params['orders_number'])) {
            $data['orders_number'] = trim($params['orders_number']);
        }
        if (!empty($params['supplier_uuid'])) {
            $data['supplier_uuid'] = $params['supplier_uuid'];
        }
        if (is_numeric($params['choose_company_id'])) {
            $data['choose_company_id'] = $params['choose_company_id'];
        }
        $result = $this->callSoaErp('post', '/order/getOrder', $data);

        $data = $result['data']['list'];

        for ($i = 0; $i < count($data); $i++) {
            if (is_numeric($data[$i]['true_time'])) {
                $data[$i]['true_time_unix'] = $data[$i]['true_time'];
                $data[$i]['true_time'] = date('Y-m-d', $data[$i]['true_time']);

            } else {
                $data[$i]['true_time'] = '&nbsp';
            }
            if (!empty($data[$i]['send_time'])) {
                $data[$i]['send_time'] = date('Y-m-d', $data[$i]['send_time']);
            } else {
                $data[$i]['send_time'] = '';
            }


            $jfjs = 0;
            $jfsl = 0;
            $jfzl = 0;
            $jftj = 0;
            for ($j = 0; $j < count($data[$i]['orders_goods_info']); $j++) {
                $jfjs += $data[$i]['orders_goods_info'][$j]['realy_count'];
                $jfsl += $data[$i]['orders_goods_info'][$j]['realy_pack_count'];
                $jfzl += $data[$i]['orders_goods_info'][$j]['realy_weight'];
                $jftj += $data[$i]['orders_goods_info'][$j]['realy_volume'];


            }

            $data[$i]['jfjs'] = $jfjs;
            $data[$i]['jfsl'] = $jfsl;
            $data[$i]['jfzl'] = $jfzl;
            $data[$i]['jftj'] = $jftj;

        }

        if ($this->request->isAjax()) {

            return json(['code' => 200, 'data' => $data, 'count' => $result['data']['count']]);
        }

        $ab_type = $this->callSoaErp('post', '/order/getAbnormalType');

        $result = Help::getOrder($result);

        $this->getPageParams($result);

        return $this->fetch('order_form');
    }

    /**
     *  圣华
     *  发运报表
     */
    public function showShipmentLineOverFormManage(){
		$params['status'] = 1;
		$result = $this->callSoaErp('post', '/source/getProject', $params);		
		$this->assign('projectResult',$result['data']);
		
        return $this->fetch('shipment_live_over_form');
    }
	
	
	public function showShortBargeFormManage(){
		$params['status'] = 1;
		$result = $this->callSoaErp('post', '/source/getSupplier', $params);
		
		$this->assign('supplier_result',$result['data']);
		$params['status'] = 1;
		$projectResult = $this->callSoaErp('post', '/source/getProject', $params);
		$this->assign('projectResult',$projectResult['data']);
		$this->assign('supplier_result',$result['data']);		
		
		return $this->fetch('short_barge');
	}
	public function showShortBargeFormDownload(){
        $params = Request::instance()->param();


        $data = [
            'page' => $params['page'],
            'page_size' => $params['limit'],
            'status'=>1


        ];

        if(!empty($params['send_goods_company'])){
            $data['send_goods_company'] = $params['send_goods_company'];
        }
        if(!empty($params['orders_number'])){
            $data['orders_number'] = $params['orders_number'];
        }
        if(!empty($params['finance_number'])){
            $data['finance_number'] = $params['finance_number'];
        }
        if(!empty($params['supplier_uuid'])){
            $data['supplier_uuid'] = $params['supplier_uuid'];
        }

        if(!empty($params['start_shipment_time'])){
            $data['start_shipment_time'] = strtotime($params['start_shipment_time']);
        }
        if(is_numeric($params['is_confirm'])){
            $data['is_confirm'] = $params['is_confirm'];
        }

        if(!empty($params['end_shipment_time'])){
            $data['end_shipment_time'] = strtotime($params['end_shipment_time']);
        }
        $result = $this->callSoaErp('post', '/shipment/getShortBargeInfo', $data);

        if(is_numeric($data['page'])){//代表有分页
            $data_result = $result['data']['list'];
        }else{
            $data_result = $result['data'];
        }

        for($i=0;$i<count($data_result);$i++){
            if(is_numeric($data_result[$i]['shipment_time'])){
                $data_result[$i]['shipment_time']= date('Y-m-d',$data_result[$i]['shipment_time']);
            }else{
                $data_result[$i]['shipment_time']='';
            }
            if(is_numeric($data_result[$i]['pickup_time'])){
                $data_result[$i]['pickup_time']= date('Y-m-d',$data_result[$i]['pickup_time']);
            }else{
                $data_result[$i]['pickup_time']='';
            }
            $jianshu=0;
            $shuliang=0;
            $zhongliang=0;
            $tiji=0;
            $goods_name='';
            for($j=0;$j<count($data_result[$i]['shipment_goods']);$j++){
                if($j==0){
                    $goods_name=$data_result[$i]['shipment_goods'][$j]['goods_name'];
                }else{
                    $goods_name.='/'.$data_result[$i]['shipment_goods'][$j]['goods_name'];
                }
                $jianshu+=$data_result[$i]['shipment_goods'][$j]['shipment_count'];
                $shuliang+=$data_result[$i]['shipment_goods'][$j]['shipment_pack_count'];
                $zhongliang+=$data_result[$i]['shipment_goods'][$j]['shipment_weight'];
                $tiji+=$data_result[$i]['shipment_goods'][$j]['shipment_volume'];
            }
            $data_result[$i]['goods_name'] = $goods_name;
            $data_result[$i]['shipment_count'] = $jianshu;
            $data_result[$i]['shipment_pack_count'] = $shuliang;
            $data_result[$i]['shipment_weight'] = $zhongliang;
            $data_result[$i]['shipment_volume'] = $tiji;
        }

//        if($params['is_download']==1){//daoru
//
//            return $data_result;
//
//            exit();
//        }

        $r = [
            'code'=>200,
            'data'=>$data_result,
            'count'=>$result['data']['count']

        ];
        $this->assign('short_barge_data',$data_result);
        return $this->fetch('short_barge_download');
    }




	
	public function getShortBargeAjax(){
		$params = Request::instance()->param();
		
 		$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
 			'status'=>1


 		];
		
 		if(!empty($params['send_goods_company'])){
 			$data['send_goods_company'] = $params['send_goods_company'];
 		}
 		if(!empty($params['orders_number'])){
 			$data['orders_number'] = $params['orders_number'];
 		}
 		if(!empty($params['finance_number'])){
 			$data['finance_number'] = $params['finance_number'];
 		}
 		if(!empty($params['supplier_uuid'])){
 			$data['supplier_uuid'] = $params['supplier_uuid'];
 		}	
		
		if(!empty($params['start_shipment_time'])){
			$data['start_shipment_time'] = strtotime($params['start_shipment_time']);			
		}
 		if(is_numeric($params['is_confirm'])){
 			$data['is_confirm'] = $params['is_confirm'];
 		}				
 		if(is_numeric($params['project_id'])){
 			$data['project_id'] = $params['project_id'];
 		}		
		if(!empty($params['end_shipment_time'])){
			$data['end_shipment_time'] = strtotime($params['end_shipment_time']);			
		}
 		$result = $this->callSoaErp('post', '/shipment/getShortBargeInfo', $data);
		

		if(is_numeric($data['page'])){//代表有分页
			$data_result = $result['data']['list'];
		}else{
			$data_result = $result['data'];
		}
	
		for($i=0;$i<count($data_result);$i++){
			if(is_numeric($data_result[$i]['shipment_time'])){
				$data_result[$i]['shipment_time']= date('Y-m-d',$data_result[$i]['shipment_time']);
			}else{
				$data_result[$i]['shipment_time']='';
			}
			if(is_numeric($data_result[$i]['pickup_time'])){
				$data_result[$i]['pickup_time']= date('Y-m-d',$data_result[$i]['pickup_time']);
			}else{
				$data_result[$i]['pickup_time']='';
			}	
			$jianshu=0;
			$shuliang=0;
			$zhongliang=0;
			$tiji=0;
			$goods_name='';
			for($j=0;$j<count($data_result[$i]['shipment_goods']);$j++){
				if($j==0){
					$goods_name=$data_result[$i]['shipment_goods'][$j]['goods_name'];
				}else{
					$goods_name.='/'.$data_result[$i]['shipment_goods'][$j]['goods_name'];
				}
				$jianshu+=$data_result[$i]['shipment_goods'][$j]['shipment_count'];
				$shuliang+=$data_result[$i]['shipment_goods'][$j]['shipment_pack_count'];
				$zhongliang+=$data_result[$i]['shipment_goods'][$j]['shipment_weight'];
				$tiji+=$data_result[$i]['shipment_goods'][$j]['shipment_volume'];				
			}
			$data_result[$i]['goods_name'] = $goods_name;
			$data_result[$i]['shipment_count'] = $jianshu;
			$data_result[$i]['shipment_pack_count'] = $shuliang;
			$data_result[$i]['shipment_weight'] = $zhongliang;
			$data_result[$i]['shipment_volume'] = $tiji;
		}

		if($params['is_download']==1){//daoru
			
			return $data_result;
			
			exit();
		}
		
 		$r = [
 				'code'=>200,
 				'data'=>$data_result,
 				'count'=>$result['data']['count']
 		
 		];
		return $r;
	}
	
	//导出短驳报表
	public function exportShortBarge(){
		ini_set('max_execution_time', '0');
		ini_set("memory_limit", -1);
		$params = Request::instance()->param();
	
	
        $data = $this->getShortBargeAjax($params);

	
        $new_data = [];
        foreach($data as $key=>$val) {
            $new_data[$key]['shipment_time'] = $val['shipment_time']; //短驳日期			
			$new_data[$key]['finance_number'] = $val['finance_number']; //短驳号
			$new_data[$key]['orders_number'] = $val['orders_number']; //运单号
			$new_data[$key]['pickup_time'] = $val['pickup_time']; //运单日期
			$new_data[$key]['goods_name'] = $val['goods_name']; //货物名称
			
			$new_data[$key]['shipment_count'] = $val['shipment_count']; //件数
			$new_data[$key]['shipment_pack_count'] = $val['shipment_pack_count']; //数量
			$new_data[$key]['shipment_weight'] = $val['shipment_weight']; //重量
			$new_data[$key]['shipment_volume'] = $val['shipment_volume']; //体积

			$new_data[$key]['send_goods_company'] = $val['send_goods_company']; //发货方
			$new_data[$key]['accept_goods_company'] = $val['accept_goods_company']; //收货方
			$new_data[$key]['short_barge_supplier_name'] = $val['short_barge_supplier_name']; //短驳承运商
			$new_data[$key]['short_barge_money'] = $val['short_barge_money']; //短驳费
			$new_data[$key]['short_barge_abnormal_money'] = $val['short_barge_abnormal_money']; //短驳理赔
        }
		
//        echo "<pre>";
//        var_dump($new_data);exit;
//        echo "</pre>";

        $name = "短驳报表";
        $xAxis = [];
        $title = "短驳报表";
        $header = array('短驳日期', '短驳号','运单号','运单日期','货物名称','件数','数量','重量','体积','发货方', '收货方', '短驳承运商', '短驳费', '短驳理赔');

        //导出excel
        $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name);		
		
		
	}
}