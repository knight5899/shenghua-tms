<?php
/**
 * Created by PhpStorm.
 * User: 胡
 * Date: 2018/8/13
 * Time: 9:24
 */
namespace app\index\controller;

use app\common\help\Help;
use think\helper\Arr;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;

 
class Bookings extends Base
{

    /**
     * 
     */
    public function showBookingList(){
        return $this->fetch('booking_list');
    }



    /**
     *
     */
    public function companyOrderManageAdd(){
    	session('user_version','guodanna');
    	//获取时区
    	 
    	$time_zone = session('time_zone');
    	 
    	 
    	$this->assign('time_zone',$time_zone);
    	//获取房型
    	 
    	$room_type_result =  $this->callSoaErp('post','/system/getRoomType', $return_receipt);
   
    	$this->assign('room_type_result',$room_type_result['data']);
    	$company_order_number = input('company_order_number');
    	if(!empty($company_order_number)){
    		$number_params = [
    				'company_order_number'=>$company_order_number
    		];
    	
    		$result = $this->callSoaErp('post', '/branchcompany/getCompanyOrder', $number_params);
    		 
    		
    		 
    		$this->assign('company_order',$result['data'][0]);
    	}
    	
    	//获取所有应付
    	
    	$cope_params = [
    		'company_id'=>session('user')['company_id']	
    	];
    	$cope_result = $this->callSoaErp('post', '/finance/getCope',$cope_params );
    	 
  
    	
    	
    	//获取所有代理商
    	$distributor_params = [
    			'company_id'=>session('user')['company_id']
    			 
    	];
    	$distributor_result = $this->callSoaErp('post','/btob/getDistributor',$distributor_params);
    	 
    	//获取所有审批中的数据
    	
    	
    	
    	$finance_data['finance_type'] = 2;
    	$finance_data['company_order_number'] = $company_order_number;
    	$finance_result = $this->callSoaErp('post','/finance/getFinanceApprove',$finance_data);

    	$this->assign('finance_result',$finance_result['data']);
    	
    	$this->assign('distributor_result',$distributor_result['data']);

    	$this->assign('cope_result',$cope_result['data']);
    	if(!empty($company_order_number)){
    		if($result['data'][0]['is_new']==1){
    			 
    			return $this->fetch('company_order_newManage_add');
    		}else{
    			return $this->fetch('company_order_add');
    		}
    	
    	}else{
    		return $this->fetch('company_order_add');
    	}
    	
    	
    	
    	return $this->fetch('company_order_add');
    }

    /**
    *
    */
    public function companyOrderAddvisitor(){
        return $this->fetch('company_order_addvisitor');
    }
    
    /*新订单添加游客*/
    public function companyOrderAddvisitorAdd(){
    	return $this->fetch('company_order_addvisitor_add');
    }    
    /**
    *新订单
   	*/
	public function company_order_newManage_add(){
        //获取时区

    	$time_zone = session('time_zone');


        $this->assign('time_zone',$time_zone);

        return $this->fetch('company_order_newManage_add');
	}
	
	//收 待我审批
	public function financeApproveByMeReceivable(){
		//首先必须是财务角色
		 
		$param = Request::instance()->param();
		$data['company_id'] = session('user')['company_id'];
		$data['status'] = 1;
		$data['is_like'] = 1;
		if(!empty($param['create_user_name'])){
			$data['create_user_name'] = $param['create_user_name'];
		}
		 
		if(!empty($param['team_product_number'])){
			$data['team_product_number'] = $param['team_product_number'];
		}
		if(is_numeric($param['finance_type'])){
			$data['finance_type'] = $param['finance_type'];
		}
		if(!empty($param['create_time'])){
			$data['create_time'] = $param['create_time'];
		}
		if(!empty($param['company_order_number'])){
			$data['company_order_number'] = $param['company_order_number'];
		}
			
		if(session('user')['role_id']==5 || session('user')['role_id']==6 || session('user')['role_id']==1){
			$data['page']=$this->page();
			$data['page_size']=$this->_page_size;
			$data['finance_type']=1;
			$result = $this->callSoaErp('post','/finance/getFinanceApprove',$data);
		
		
			$this->getPageParams($result);
		}		
		
		return $this->fetch('finance_approve_by_me');
	}
	//收 我发起的
	public function iPostFinanceApproveReceivable(){
		//首先必须是财务角色
		 
		$param = Request::instance()->param();
		$data['is_like'] = 1;
		if(!empty($param['create_time'])){
			$data['create_time'] = $param['create_time'];
		}
		if(!empty($param['status'])){
			if($param['status'] == 1){
				$data['status'] = 1;
			}else if($param['status'] == 2){
				$data['approve_result'] = 1;
		
			}else if($param['status'] == 3){
				$data['approve_result'] = 2;
		
			}
				
		
		}
		
		if(!empty($param['company_order_number'])){
			$data['company_order_number'] = $param['company_order_number'];
		}
		if(!empty($param['create_user_name'])){
			$data['create_user_name'] = $param['create_user_name'];
		}
		
		if(!empty($param['team_product_number'])){
			$data['team_product_number'] = $param['team_product_number'];
		}
		if(is_numeric($param['finance_type'])){
			$data['finance_type'] = $param['finance_type'];
		}
		if(session('user')['company_id']==1 && (session('user')['role_id']==5 || session('user')['role_id']==1)){
		
		
		}else{
			$data['company_id'] = session('user')['company_id'];
			$data['create_user_id'] =  session('user')['user_id'];
		}
		 
		$data['page']=$this->page();
		$data['page_size']=$this->_page_size;
		$data['finance_type']=1;
		$result = $this->callSoaErp('post','/finance/getFinanceApprove',$data);
		$this->getPageParams($result);
		
		
		
		$this->assign('result',$result);	
	
		return $this->fetch('i_post_finance_approve');
	}
	//收 已审批
	public function financeApproveOverReceivable(){
		//首先必须是财务角色
		$param = Request::instance()->param();
		$data['is_like'] = 1;
		$data['company_id'] = session('user')['company_id'];
		$data['status'] =  2;
		$data['finance_type']=1;
		if(!empty($param['create_time'])){
			$data['create_time'] = $param['create_time'];
		}
		if(!empty($param['company_order_number'])){
			$data['company_order_number'] = $param['company_order_number'];
		}
		if(!empty($param['approve_time'])){
			$data['approve_time'] = $param['approve_time'];
		}
		if(!empty($param['approve_result'])){
			$data['approve_result'] = $param['approve_result'];
		}
		if(!empty($param['create_user_name'])){
			$data['create_user_name'] = $param['create_user_name'];
		}
		
		if(!empty($param['team_product_number'])){
			$data['team_product_number'] = $param['team_product_number'];
		}
		if(is_numeric($param['finance_type'])){
			$data['finance_type'] = $param['finance_type'];
		}
		if(session('user')['role_id']==5 || session('user')['role_id']==1 || session('user')['role_id']==6){
		
		
		
		
			$this->assign('result',$result);
		
		}else{
			$data['create_user_id'] =  session('user')['user_id'];
			 
		
		}
		
		$data['page']=$this->page();
		$data['page_size']=$this->_page_size;
		$result = $this->callSoaErp('post','/finance/getFinanceApprove',$data);
		 
		$this->getPageParams($result);
		 	
	
		return $this->fetch('finance_approve_over');
	}
	//付

	public function financeApproveByMePay(){
	
		//首先必须是财务角色
			
		$param = Request::instance()->param();
		$data['company_id'] = session('user')['company_id'];
		$data['status'] = 1;
		$data['is_like'] = 1;
		$data['finance_type']=2;
		if(!empty($param['create_user_name'])){
			$data['create_user_name'] = $param['create_user_name'];
		}
			
		if(!empty($param['team_product_number'])){
			$data['team_product_number'] = $param['team_product_number'];
		}
		if(is_numeric($param['finance_type'])){
			$data['finance_type'] = $param['finance_type'];
		}
		if(!empty($param['create_time'])){
			$data['create_time'] = $param['create_time'];
		}
		if(!empty($param['company_order_number'])){
			$data['company_order_number'] = $param['company_order_number'];
		}
			
		if(session('user')['role_id']==5 || session('user')['role_id']==6 || session('user')['role_id']==1){
			$data['page']=$this->page();
			$data['page_size']=$this->_page_size;
			$result = $this->callSoaErp('post','/finance/getFinanceApprove',$data);
		
		
			$this->getPageParams($result);
		}

		return $this->fetch('finance_approve_by_me_pay');
	}
	public function iPostFinanceApprovePay(){
		//首先必须是财务角色
		 
		$param = Request::instance()->param();
		$data['is_like'] = 1;
		if(!empty($param['create_time'])){
			$data['create_time'] = $param['create_time'];
		}
		if(!empty($param['status'])){
			if($param['status'] == 1){
				$data['status'] = 1;
			}else if($param['status'] == 2){
				$data['approve_result'] = 1;
		
			}else if($param['status'] == 3){
				$data['approve_result'] = 2;
		
			}
				
		
		}
		
		if(!empty($param['company_order_number'])){
			$data['company_order_number'] = $param['company_order_number'];
		}
		if(!empty($param['create_user_name'])){
			$data['create_user_name'] = $param['create_user_name'];
		}
		
		if(!empty($param['team_product_number'])){
			$data['team_product_number'] = $param['team_product_number'];
		}
		if(is_numeric($param['finance_type'])){
			$data['finance_type'] = $param['finance_type'];
		}
		if(session('user')['company_id']==1 && (session('user')['role_id']==5 || session('user')['role_id']==1)){
		
		
		}else{
			$data['company_id'] = session('user')['company_id'];
			$data['create_user_id'] =  session('user')['user_id'];
		}
		 
		$data['page']=$this->page();
		$data['page_size']=$this->_page_size;
		$result = $this->callSoaErp('post','/finance/getFinanceApprove',$data);
		$this->getPageParams($result);
		
		
		
		$this->assign('result',$result);	
	
		return $this->fetch('i_post_finance_approve_pay');
	}
	public function financeApproveOverPay(){
	
		//首先必须是财务角色
		$param = Request::instance()->param();
		$data['is_like'] = 1;
		$data['company_id'] = session('user')['company_id'];
		$data['status'] =  2;
		if(!empty($param['create_time'])){
			$data['create_time'] = $param['create_time'];
		}
		if(!empty($param['company_order_number'])){
			$data['company_order_number'] = $param['company_order_number'];
		}
		if(!empty($param['approve_time'])){
			$data['approve_time'] = $param['approve_time'];
		}
		if(!empty($param['approve_result'])){
			$data['approve_result'] = $param['approve_result'];
		}
		if(!empty($param['create_user_name'])){
			$data['create_user_name'] = $param['create_user_name'];
		}
		
		if(!empty($param['team_product_number'])){
			$data['team_product_number'] = $param['team_product_number'];
		}
		if(is_numeric($param['finance_type'])){
			$data['finance_type'] = $param['finance_type'];
		}
		if(session('user')['role_id']==5 || session('user')['role_id']==1 || session('user')['role_id']==6){
		
		
		
		
			$this->assign('result',$result);
		
		}else{
			$data['create_user_id'] =  session('user')['user_id'];
			 
		
		}
		
		$data['page']=$this->page();
		$data['page_size']=$this->_page_size;
		$result = $this->callSoaErp('post','/finance/getFinanceApprove',$data);
		 
		$this->getPageParams($result);
		 	
		return $this->fetch('finance_approve_over_pay');
	}
	
	//显示付款
	
	public function financeCope(){
		
		$data['page']=$this->page();
		$data['page_size']=$this->_page_size;
		$data['company_id'] = session('user')['company_id'];
		$data['finance_type'] = 2;
		$result  = $this->callSoaErp('post','/finance/getFinanceApprove',$data);
		$this->getPageParams($result);
		$this->assign('finance_result',$finance_result);
		return $this->fetch('fiancne_cope');
		
	}
	//显示付款新增页面
	
	public function financeCopeAdd(){
	
		//获取 所有的订单编号
		$data['company_id'] = session('user')['company_id'];
		$company_order_result = $this->callSoaErp('post','/branchcompany/getCompanyOrder',$data);
		
		$this->assign('company_order_result',$company_order_result['data']);
		return $this->fetch('fiancne_cope_add');
	
	}


    public function companyOrderManageInfo(){
        session('user_version','guodanna');
        //获取时区

        $time_zone = session('time_zone');


        $this->assign('time_zone',$time_zone);
        //获取房型

        $room_type_result =  $this->callSoaErp('post','/system/getRoomType', $return_receipt);

        $this->assign('room_type_result',$room_type_result['data']);
        $company_order_number = input('company_order_number');

        $number_params = [
            'company_order_number'=>$company_order_number
        ];

        $result = $this->callSoaErp('post', '/branchcompany/getCompanyOrder', $number_params);



        $this->assign('company_order',$result['data'][0]);


        //获取所有代理商
        $distributor_params = [
            'company_id'=>session('user')['company_id']

        ];
        $distributor_result = $this->callSoaErp('post','/btob/getDistributor',$distributor_params);

        $this->assign('distributor_result',$distributor_result['data']);

        if(!empty($company_order_number)){
            if($result['data'][0]['is_new']==1){

                return $this->fetch('company_order_newManage_info');
            }else{
                return $this->fetch('company_order_info');
            }

        }else{
            return $this->fetch('company_order_info');
        }



        return $this->fetch('company_order_info');
    }

}