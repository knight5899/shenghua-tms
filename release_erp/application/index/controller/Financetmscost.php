<?php


namespace app\index\controller;


use think\Request;

class Financetmscost extends Base
{

    public function tmscost(){
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/Dispatchfee/getDispatchfee', $data);
        $this->assign('lists', $result['data']['data']);
        $this->getPageParams($result);
        return $this->fetch('tmscost');
    }

}