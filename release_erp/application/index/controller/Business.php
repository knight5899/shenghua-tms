<?php

namespace app\index\controller;

use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
class Business extends Base
{


  //  $data = Request::instance()->param();
    
    /**
     *  圣华
     *  业务驾驶舱
     */
	 public function index(){

        return $this->fetch('index');		 
		 
		 
	 }
    
    /**
     * 客户管理显示新增页面
     */
    public function showCustomerAdd(){


        return $this->fetch('customer_add');
    }    
    
    /**
     * 客户新增数据
     */
    public function addCustomerAjax(){
    	$type_id = input("type_id");
    	$customer_name = input("customer_name");
    	$customer_service_name = input("customer_service_name");
    	$business_name = input("business_name");
    	$status = input("status");
    	$data = [
    			"type_id"=>$type_id,
    			"customer_name"=>$customer_name,
    			"customer_service_name"=>$customer_service_name,
    			"business_name"=>$business_name,
    			"status"=>$status,
    	];

    	$result = $this->callSoaErp('post', '/source/addCustomer',$data);

    	return   $result;//['code' => '400', 'msg' => $data];
    }
    
    
    /**
     * 供应商显示页面
     */
    public function showSupplierManage(){

       $data=[
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,

        ];
        $supplier_type = input("supplier_type");
        $supplier_name = input("supplier_name");
        $supplier_uuid= input("supplier_uuid");
        $supplier_id= input("supplier_id");
        if(is_numeric(input('status'))){
        	$data['status'] = input('status');
        }
 

        $linkman = input("linkman");
        if(is_numeric($supplier_type)){
            $data['supplier_type'] = $supplier_type;
        }
        if(!empty($supplier_name)){
            $data['supplier_name'] = $supplier_name;
        }

        if(!empty($supplier_uuid)){
            $data['supplier_uuid'] = $supplier_uuid;
        }
        if(!empty($supplier_id)){
        	$data['supplier_id'] = $supplier_id;
        }       
        if(!empty($linkman)){
            $data['linkman'] = $linkman;
        }


	
        $result = $this->callSoaErp('post','/source/getSupplier',$data);

        $this->getPageParams($result);


        return $this->fetch('supplier_manage');
    }

    /**
     * 供应商显示新增页面
     */
    public function showSupplierAdd(){

        //获取上一个页面的URl
        $url = $_SERVER["HTTP_REFERER"];
        $url_data = explode("/",$url);
        $this->assign('url',$url_data[4]);

        //获取供应商类别
        $data = [
            'status'=>1

        ];
        $supplier_result = $this->callSoaErp('post', '/system/getSuppliertype');
        $this->assign('supplier_data_result',$supplier_result['data']);


        //获取公司信息
        $data = [
            'status'=>1
        ];
        $company_result =  $this->callSoaErp('post', '/system/getCompany',$data);

        //获取城市3级联动信息
        $data1['status'] = 1;
        $country_result = $this->callSoaErp('post', '/system/getCountryCity',$data1);

        $this->assign('country_result',$country_result['data']);
        $this->assign('company_result',$company_result['data']);
        $this->assign('supplier_types',$this->types);
        return $this->fetch('supplier_add');
    }

    /**
     * 供应商新增数据
     */
    public function addSupplierAjax(){
 	
        $supplier_name = input("supplier_name");
        $supplier_type = input('supplier_type');
        $linkman = input("linkman");
        $address = input("address");
        $zip_code = input("zip_code");
        $phone = input("phone");
        $website = input("website");
        $fax = input("fax");  
        $cellphone = input('cellphone');
        $email = input("email");
        $remark = input("remark");
        $account_name = input('account_name');
        $bank_code = input('bank_code');
        $bank_name = input('bank_name');
        $bank_number = input('bank_number');
        $status = input("status");
        $data = [
         
            "supplier_name"=>$supplier_name,
            "supplier_type"=>$supplier_type,
            "linkman"=>$linkman,
            "address"=>$address,
            "zip_code"=>$zip_code,
            "phone"=>$phone,
            "website"=>$website,
            "fax"=>$fax,
        	'cellphone'=>$cellphone,
            "email"=>$email,
            "remark"=>$remark,
        	'account_name'=>$account_name,
        	'bank_code'=>$bank_code,
        	'bank_name'=>$bank_name,
        	'bank_number'=>$bank_number,
        	"status"=>$status,
          
        ];
  
        $result = $this->callSoaErp('post', '/source/addSupplier',$data);
        return   $result;//['code' => '400', 'msg' => $data];
    }

    /**
     * 供应商修改页面
     */
    public function showSupplierEdit(){
        //读取相应供应商信息
        $data=["supplier_id"=>input("supplier_id")];
        $result = $this->callSoaErp('post', '/source/getSupplier',$data);

        $this->assign('data',$result['data'][0]);

   


        $this->assign('supplier_types',$this->types);
        return $this->fetch('supplier_edit');

    }

    /**
     * 供应商修改数据
     */
    public function editSupplierAjax(){
        $supplier_id = input("supplier_id");
        $supplier_type = input("supplier_type");
        $supplier_name = input("supplier_name");

        $linkman = input("linkman");
        $address = input("address");
        $zip_code = input("zip_code");
        $phone = input("phone");
        $website = input("website");
        $fax = input("fax");
        $email = input("email");
        $remark = input("remark");
        $status = input("status");

        $account_name = input('account_name');
        $bank_code = input('bank_code');
        $bank_name = input('bank_name');
        $bank_number = input('bank_number');
        $cellphone = input('cellphone');
        $data = [
            "supplier_id"=>$supplier_id,
            "supplier_type"=>$supplier_type,
            "supplier_name"=>$supplier_name,
            "linkman"=>$linkman,
            "address"=>$address,
            "zip_code"=>$zip_code,
            "phone"=>$phone,
            "website"=>$website,
            "fax"=>$fax,
            "email"=>$email,
            "remark"=>$remark,
            "status"=>$status,
            "user_id"=>$user_id,
            "pay_message" => $pay_message,
        	'account_name'=>$account_name,
        	'bank_code'=>$bank_code,
        	'bank_name'=>$bank_name,
        	'bank_number'=>$bank_number,
            'cellphone'=>$cellphone
        ];

        $result = $this->callSoaErp('post', '/source/updateSupplierBySupplierId',$data);
        return   $result;
    }

    /**
     * 供应商显示详情
     */
    public function showSupplierInfo(){
        //获取上一个页面的URl
        $url = $_SERVER["HTTP_REFERER"];
        $url_data = explode("/",$url);
        $this->assign('url',$url_data[4]);

        //读取相应供应商信息
        $data=["supplier_id"=>input("supplier_id")];
        $result = $this->callSoaErp('post', '/source/getSupplier',$data);
        $this->assign('data',$result['data'][0]);

        $supplier_type_id = input("supplier_type_id");
        if($supplier_type_id==5){
            $this->assign('button',$supplier_type_id);
        }

        $this->assign('supplier_types',$this->types);

        return $this->fetch('supplier_info');
    }

    /**
     * 车辆显示页面
     */
    public function showVehicleTypeManage(){
    
    	$data=[
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    	];

    	$vehicle_type_uuid = input("vehicle_type_uuid");
    	$vehicle_type_id = input("vehicle_type_id");
    	$vehicle_type_name = input("vehicle_type_name");
    	if(is_numeric(input('status'))){
    		$data['status'] = input('status');
    	}

    
    	if(!empty($vehicle_type_uuid)){
    		$data['vehicle_type_uuid'] =$vehicle_type_uuid;
    	}
    	if(!empty($vehicle_type_id)){
    		$data['vehicle_type_id'] = $vehicle_type_id;
    	}
    	
    	if(!empty($vehicle_type_name)){
    		$data['vehicle_type_name'] = $vehicle_type_name;
    	}
    
    
    
    	$result = $this->callSoaErp('post','/source/getVehicleType',$data);
    
    	$this->getPageParams($result);
    
    
    	return $this->fetch('vehicle_type_manage');
    }
    /**
     * 车辆类型显示新增页面
     */
    public function showVehicleTypeAdd(){
    
    	return $this->fetch('vehicle_type_add');
    }

    /**
     * 车辆类型新增AJAX
     */
    public function addVehicleTypeAjax(){
    
		$data = Request::instance()->param();
		
		
    	$result = $this->callSoaErp('post', '/source/addVehicleType',$data);
 
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 车辆类型修改页面
     */
    public function showVehicleTypeEdit(){
    	//读取相应供应商信息
    	$data=["vehicle_type_id"=>input("vehicle_type_id")];

    	$result = $this->callSoaErp('post', '/source/getVehicleType',$data);
    	
    	$this->assign('data',$result['data'][0]);

    	return $this->fetch('vehicle_type_edit');
    
    }    
    /**
     * 车辆类型修改AJAX
     */
    public function editVehicleTypeAjax(){

    	$data = Request::instance()->param();
    
   
    
    	$result = $this->callSoaErp('post', '/source/updateVehicleTypeByVehicleTypeId',$data);
    	return   $result;
    }    
    /**
     * 车辆显示页面
     */
    public function showVehicleManage(){
    
    	$data=[
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    	];
    	$supplier_id = input("supplier_id");
    	$number_plate= input("number_plate");
    	$vehicle_id= input("vehicle_id");
    	$vehicle_type_id= input("vehicle_type_id");
    
    	if(is_numeric(input('status'))){
    		$data['status'] = input('status');
    	}
    	if(is_numeric(input('supplier_id'))){
    		$data['supplier_id'] = input('supplier_id');
    	}    
    	if(!empty(input('number_plate'))){
    		$data['number_plate'] = input('number_plate');
    	}
    	if(!empty(input('number_plate'))){
    		$data['number_plate'] = input('number_plate');
    	}
    	if(!empty(input('max_load'))){
    		$data['max_load'] = input('max_load');
    	}    	
    	if(is_numeric(input('vehicle_type_id'))){
    		$data['vehicle_type_id'] = input('vehicle_type_id');
    	}
   
    	$supplie_data['status'] = 1;
    	$supplier_result = $this->callSoaErp('post','/source/getSupplier',$supplie_data);
    	$vehicle_type_data['status'] = 1;
    	$vehicle_type_result = $this->callSoaErp('post','/source/getVehicleType',$vehicle_type_data);
    	
    
    	$result = $this->callSoaErp('post','/source/getVehicle',$data);
    
    	$this->getPageParams($result);
    
    	$this->assign('supplier_result',$supplier_result['data']);
    	$this->assign('vehicle_type_result',$vehicle_type_result['data']);
    
    	return $this->fetch('vehicle_manage');
    }    
    /**
     * 车辆显示新增页面
     */
    public function showVehicleAdd(){
    	$supplie_data['status'] = 1;
    	$supplier_result = $this->callSoaErp('post','/source/getSupplier',$supplie_data);
    	$vehicle_type_data['status'] = 1;
    	$vehicle_type_result = $this->callSoaErp('post','/source/getVehicleType',$vehicle_type_data);
    	     	
    	//如果是修改页面
    	$vehicle_id = input('vehicle_id');
		
    	if(is_numeric($vehicle_id)){
    		$vehicle_data['vehicle_id'] = $vehicle_id;
    		$vehicle_result =  $this->callSoaErp('post','/source/getVehicle',$vehicle_data);
    		$this->assign('vehicle_result',$vehicle_result['data'][0]);
    		
    	}
    	
    	
    	$this->assign('supplier_result',$supplier_result['data']);
    	$this->assign('vehicle_type_result',$vehicle_type_result['data']);    
    	return $this->fetch('vehicle_add');
    }
    /**
     * 车辆新增AJAX
     */
    public function addVehicleAjax(){
    
    	$data = Request::instance()->param();
    
    	
    	$result = $this->callSoaErp('post', '/source/addVehicle',$data);
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 车辆修改AJAX
     */
    public function editVehicleAjax(){
    
    	$data = Request::instance()->param();
    
    	
    	$result = $this->callSoaErp('post', '/source/updateVehicleByVehicleId',$data);

    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 车辆显示AJAX
     */
    public function showVehicleAjax(){
    
    	$data = Request::instance()->param();
    
    	 
    	$result = $this->callSoaErp('post', '/source/getVehicle',$data);
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 司机显示页面
     */
    public function showDriverManage(){
    
    	$data=[
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    	];


    	if(!empty(input('number_plate'))){
    		$data['number_plate'] = input('number_plate');
    	}
    	if(!empty(input('driver_name'))){
    		$data['driver_name'] = input('driver_name');
    	}
    	if(is_numeric(input('supplier_id'))){
    		$data['supplier_id'] = input('supplier_id');
    	}

    	if(is_numeric(input('vehicle_id'))){
    		$data['vehicle_id'] = input('vehicle_id');
    	}
    	 
    	$supplie_data['status'] = 1;
    	$supplier_result = $this->callSoaErp('post','/source/getSupplier',$supplie_data);
    	$vehicle_data['status'] = 1;
    	$vehicle_result = $this->callSoaErp('post','/source/getVehicle',$vehicle_data);
    	 

    	$result = $this->callSoaErp('post','/source/getDriver',$data);

    	$this->getPageParams($result);
    
    	$this->assign('supplier_result',$supplier_result['data']);
    	$this->assign('$vehicle_result',$vehicle_result['data']);
    
    	return $this->fetch('driver_manage');
    }    
    /**
     * 司机显示新增页面
     */
    public function showDriverAdd(){


    	$supplier_data['status'] = 1;
    	$supplier_result =  $this->callSoaErp('post','/source/getSupplier',$supplier_data);
    	 
    	$this->assign('supplier_result',$supplier_result['data']);

    	
    	//如果是修改页面
    	$driver_id = input('driver_id');
    	
    	if(is_numeric($driver_id)){
    		
  

    		
    		$driver_data['driver_id'] = $driver_id;
    		$driver_result =  $this->callSoaErp('post','/source/getDriver',$driver_data);
    		
    		$this->assign('driver_result',$driver_result['data'][0]);
    		
    		$vehicle_data['supplier_id'] = $driver_result['data'][0]['supplier_id'];
    	
    		$vehicle_result =  $this->callSoaErp('post','/source/getVehicle',$vehicle_data);

    		$this->assign('vehicle_result',$vehicle_result['data']);
    	
    	}
    	
    	return $this->fetch('driver_add');
    }    
    
    /**
     * 司机新增AJAX
     */
    public function addDriverAjax(){
   
    	$data = Request::instance()->param();
    	

    	 
    	$result = $this->callSoaErp('post', '/source/addDriver',$data);

    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 司机修改AJAX
     */
    public function editDriverAjax(){

    	$data = Request::instance()->param();
    

    	$result = $this->callSoaErp('post', '/source/updateDriverByDriverId',$data);
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }    
    
}

