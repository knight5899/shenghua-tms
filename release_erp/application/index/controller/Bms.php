<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Request;

class Bms extends Base
{
    /**
     *  圣华
     *  仓储管理
     */
    public function index(){

		return $this->fetch('index');

    }

    /**
     *  圣华
     *  仓库显示页面
     */
    public function showWarehouse(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/warehouse/getWarehouse',$data_all);

        $this->assign('warehouseResult',$result['data']['list']);
        $this->getPageParams($result);

        return $this->fetch('warehouse_manage');

    }

    /**
     *  圣华
     *  仓库新增页面
     */
    public function showWarehouseAdd(){

        //获取公司信息
        $data = [
            'status'=>1
        ];
        $company_result = $this->callSoaErp('post', '/system/getCompany',$data);
        $this->assign("companyResult",$company_result['data']);

        //获取省市区信息
        $city = Help::getCity('-1');
        $this->assign('shengResult',$city);

        $data2 = [
            'ware_id'=>input('ware_id')
        ];
        //获取所有仓库
        $warehouse = $this->callSoaErp('post','/warehouse/getWarehouse',$data2);

        //获取所有客户信息
        $user_info = $this->callSoaErp('post','/user/getUser');
        $this->assign("userResult", $user_info['data']);
//        dump($user_info['data']);exit;

        if(is_numeric(input('ware_id'))) {
            //通过拿到的省再去拿市数据
            $cityResult = Help::getCity($warehouse['data'][0]['ware_province_id']);
            $this->assign('cityResult', $cityResult);
            //通过拿到的市再去区
            $areaResult = Help::getCity($warehouse['data'][0]['ware_city_id']);
            $this->assign('areaResult', $areaResult);

//            dump($warehouse['data'][0]);exit;
            $this->assign("bindResult", $warehouse['data'][0]);
        }
        return $this->fetch('warehouse_add');

    }

    /**
     * 圣华
     * 仓库新增AJAX / 修改AJAX
     */
    public function addwarehouseAjax(Request $request)
    {

        $data = $request->param();

        if(is_numeric($data['ware_id'])){
            //修改仓库
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/warehouse/updateWarehouseByWarehouseId',$data);
            return $result;
        }else {
            //新增仓库
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/warehouse/addWarehouse', $data);
            return $result; //['code' => '400', 'msg' => $data];
        }

    }
}