<?php

namespace app\index\controller;
use app\common\help\Contents;
use app\common\help\Help;
use think\Request;
use think\Session;
use think\Cache;
class Bill extends Base
{

    /**
     *  圣华
     *  客户账单页面
     *  韩冰
     */
    public function showCustomerBillManage(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
		
        $data=[
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,

        ];

        $data['status'] = input("status");
        $params = Request::instance()->param();
        $data_all = array_merge($params,$data);

        if($data['status']==2){
            $data_all['status'] = 1;

            return $this->fetch('customer_yes_bill');

        }else{
            $data_all['verify_status'] = 2;

            //获取客户账单信息
            $result = $this->callSoaErp('post','/bill/getCustomerBill',$data_all);

            $this->assign("customerBillResult", $result['data']['list']);
            $this->getPageParams($result);
//            echo "<pre>";
//            var_dump($result);exit;
//            echo "</pre>";

            $multi_project_id = input('multi_project_id');
            if (!empty($multi_project_id)) {
                $projectResult = $this->callSoaErp('post', '/source/getProject', ['multi_project_id' => $multi_project_id]);
            }

            $this->assign('projectResult', $projectResult['data']);

            return $this->fetch('customer_bill');
        }

    }

    public function getCustomerBillData(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],
			'status'=>1
        ];

        if(!empty($params['start_order_time'])){
            $data['start_order_time'] = strtotime($params['start_order_time']);
        }
        if(!empty($params['end_order_time'])){
            $data['end_order_time'] = strtotime($params['end_order_time']);
        }

        $data_all = array_merge($params,$data);
		if(is_numeric($params['choose_company_id'])){
            $data_all['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data_all['choose_company_id'] =session('user')['company_id'];
		}
        $data_all['status'] = 0;
        $result = $this->callSoaErp('post','/bill/getCustomerBill',$data_all);

        $new = [
            "code"=> 200,
            "count"=>$result['data']['count'],
            "data"=>$result['data']['list']

        ];

        return $new;
    }

    //待开票
    public function showCustomerBillMissInvoiceManage(){
        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        return $this->fetch('customer_bill_miss_invoice');
    }
	//确认客户账单
	public function customerBillCheck(){
		$params = Request::instance()->param();
		
	
		$data['customer_bill_number'] = $params['customer_bill_number'];

		$result = $this->callSoaErp('post', '/bill/getCustomerBillInfo', $data);
	
	

		$this->assign('result',$result['data'][0]);
		return $this->fetch('customer_bill_check');
		
	}
    //已开票
    public function showCustomerBillDoneInvoiceManage(){
        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        return $this->fetch('customer_bill_done_invoice');
    }

    //已送出
    public function showCustomerBillSendInvoiceManage(){
        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        return $this->fetch('customer_bill_send_invoice');
    }

    //已关账
    public function showCustomerBillCloseInvoiceManage(){
        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        return $this->fetch('customer_bill_close_invoice');
    }

    public function getCustomerYesBillData(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],

        ];

        $data_all = array_merge($params,$data);

        $data_all['customer_bill_status'] = 1;
        $data_all['customer_invoice_status'] = 1;
        $result = $this->callSoaErp('post','/bill/getCustomerYesBill2',$data_all);
//        error_log(print_r($result,1));
        for($i=0;$i<count($result['data']['list']);$i++){

        	$result['data']['list'][$i]['customer_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['customer_bill_status']];

        }	

        $new = [
            "status"=> 200,
            "message"=> null,
            "total"=>$result['data']['count'],
            "rows"=>[
            "item"=>$result['data']['list']
            ]
        ];
        return $new;
    }

    public function getCustomerYesBillData2(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],

        ];

        $data_all = array_merge($params,$data);

        $data_all['customer_bill_status'] = 2;
        $data_all['customer_invoice_status'] = 1;
        $result = $this->callSoaErp('post','/bill/getCustomerYesBill2',$data_all);
//        error_log(print_r($result,1));
        for($i=0;$i<count($result['data']['list']);$i++){

            $result['data']['list'][$i]['customer_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['customer_bill_status']];
            $result['data']['list'][$i]['customer_bill_invoice_status_name']=Contents::baseConfig()['bill']['customer_invoice_status'][$result['data']['list'][$i]['customer_invoice_status']];
        }

        $new = [
            "status"=> 200,
            "message"=> null,
            "total"=>$result['data']['count'],
            "rows"=>[
                "item"=>$result['data']['list']
            ]
        ];
        return $new;
    }

    public function getCustomerYesBillData3(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],

        ];

        $data_all = array_merge($params,$data);

        $data_all['customer_bill_status'] = 2;
        $data_all['customer_invoice_status'] = 4;
        $result = $this->callSoaErp('post','/bill/getCustomerYesBill2',$data_all);
//        error_log(print_r($result,1));
        for($i=0;$i<count($result['data']['list']);$i++){

            $result['data']['list'][$i]['customer_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['customer_bill_status']];
            $result['data']['list'][$i]['customer_bill_invoice_status_name']=Contents::baseConfig()['bill']['customer_invoice_status'][$result['data']['list'][$i]['customer_invoice_status']];
        }

        $new = [
            "status"=> 200,
            "message"=> null,
            "total"=>$result['data']['count'],
            "rows"=>[
                "item"=>$result['data']['list']
            ]
        ];
        return $new;
    }

    public function getCustomerYesBillData4(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],

        ];

        $data_all = array_merge($params,$data);

        $data_all['customer_bill_status'] = 2;
        $data_all['customer_invoice_status'] = 3;
        $result = $this->callSoaErp('post','/bill/getCustomerYesBill2',$data_all);
//        error_log(print_r($result,1));
        for($i=0;$i<count($result['data']['list']);$i++){

            $result['data']['list'][$i]['customer_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['customer_bill_status']];
            $result['data']['list'][$i]['customer_bill_invoice_status_name']=Contents::baseConfig()['bill']['customer_invoice_status'][$result['data']['list'][$i]['customer_invoice_status']];
        }

        $new = [
            "status"=> 200,
            "message"=> null,
            "total"=>$result['data']['count'],
            "rows"=>[
                "item"=>$result['data']['list']
            ]
        ];
        return $new;
    }

    public function getCustomerYesBillData5(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],

        ];

        $data_all = array_merge($params,$data);

        $data_all['customer_bill_status'] = 4;
//        $data_all['customer_invoice_status'] = 3;
        $result = $this->callSoaErp('post','/bill/getCustomerYesBill2',$data_all);
//        error_log(print_r($result,1));
        for($i=0;$i<count($result['data']['list']);$i++){

            $result['data']['list'][$i]['customer_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['customer_bill_status']];
            $result['data']['list'][$i]['customer_bill_invoice_status_name']=Contents::baseConfig()['bill']['customer_invoice_status'][$result['data']['list'][$i]['customer_invoice_status']];
        }

        $new = [
            "status"=> 200,
            "message"=> null,
            "total"=>$result['data']['count'],
            "rows"=>[
                "item"=>$result['data']['list']
            ]
        ];
        return $new;
    }

    /**
     *  圣华
     *  对账单显示页面
     *  韩冰
     */
    public function showCustomerBillInfoManage(){

        $data = Request::instance()->param()['data'];



        $data_info2 = json_decode($data,true);
        $data_info3 = explode(',', $data_info2);
        foreach($data_info3 as $key => $val){
            $data_info[$key]['finance_id'] = $val;
        }
	

        //获取选中账单信息
        $result = $this->callSoaErp('post','/bill/getCustomerBillInData',$data_info);
		
	
		$projectParams['project_id']=$result['data'][0]['project_id'];
		$project_result = $this->callSoaErp('post','/source/getProject',$projectParams);
		
		$this->assign('project_result',$project_result['data'][0]);
//        echo "<pre>";
//        var_dump($project_result);exit;
//        echo "</pre>";

        //获取分公司

        if (!empty(Cache::get('company_cache'))) {
            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        $user_id = session("user")['user_id'];
        $user_name = session("user")['nickname'];

        $nums = 0.00;
        $nums2 = 0.00;
        foreach($result['data'] as $key=>$val){
            $nums+=number_format($val['money'], 2, '.', '');
            $nums2+=number_format($val['abnormal_money'], 2, '.', '');
        }

        //生成对账编号
//        $customerBillNumber = Help::getReconciliation();
        $customerBillNumber = "待生成...";

        $this->assign('customer_bill_name',date("Y")."年".$project_result['data'][0]['project_name'].date('m',strtotime('-1 month'))."月对账单");
        $this->assign('customerBillResult',$result['data']);
        $this->assign('customerBillNumber',$customerBillNumber);
        $this->assign('money',number_format($nums,2, '.', '')); //运单总计
        $this->assign('abnormal_money',number_format($nums2,2, '.', '')); //赔款
        $this->assign('now',date("Y-m-d")); //对账时间
        if($result['data'][0]['tax_rate']==null){
            $tax_rate = 9;
            $customer_bill_tax_type = 1;
        }else{
            $tax_rate = $result['data'][0]['tax_rate'];
            $customer_bill_tax_type = $result['data'][0]['tax_type'];
        }
        $this->assign('customer_bill_tax_type',$customer_bill_tax_type); //是否税
        $this->assign('tax_rate',$tax_rate); //税率
        $this->assign('customer_bill_money',number_format(($nums-$nums2),2, '.', '')); //开票金额
        $this->assign('send_name',$result['data'][0]['project_name']); //发货方
        $this->assign('project_id',$result['data'][0]['project_id']); //发货方id
        $this->assign('user_id',$user_id); //创建人
        $this->assign('user_name',$user_name); //创建人名称

        $this->getPageParams($result);
//
        return $this->fetch('customer_finance_bill');

    }

    public function showCustomerBillInvoiceInfoManage(){

        $data = Request::instance()->param()['data'];

        $data_info=[
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,

        ];

        $data_info = json_decode($data,true);
        $aaa['customer_bill_number'] = $data_info[0]['customer_bill_number'];
        $result2 = $this->callSoaErp('post','/bill/getCustomerBillOnlly',$aaa);

        $this->assign('project_result',$result2['data'][0]['project_name']);
//        echo "<pre>";
//        var_dump($result2);exit;
//        echo "</pre>";

        //获取账单明细
        $result3 = $this->callSoaErp('post','/bill/getCustomerBillInfoOnlly',$aaa);
        foreach($result3['data'] as $kk1 => $vv1){
            $params_info[$kk1]['finance_id'] = $vv1['finance_id'];
        }
        $result = $this->callSoaErp('post','/bill/getCustomerBillInData',$params_info);

        //获取分公司
        if (!empty(Cache::get('company_cache'))) {
            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        $user_id = session("user")['user_id'];
        $user_name = session("user")['nickname'];

        $nums = 0.000;
        $nums2 = 0.000;
        foreach($result2['data'][0]['info'] as $key=>$val){
            $nums+=number_format($val['money'], 3, '.', '');
            $nums2+=number_format($val['abnormal_money'], 3, '.', '');
        }

        //生成对账编号
//        $customerBillNumber = Help::getReconciliation();

        $this->assign('customerBillResult',$result['data']);
        $this->assign('customerBillNumber',$data_info[0]['customer_bill_number']);
        $this->assign('customer_bill_name',$result2['data'][0]['customer_bill_name']); //客户账单名称
        $this->assign('money',number_format($nums,3, '.', '')); //运单总计
        $this->assign('abnormal_money',number_format($nums2,3, '.', '')); //赔款
        $this->assign('now',date("Y-m-d",$data_info[0]['customer_bill_date'])); //对账时间
        $this->assign('customer_bill_tax_type',$result2['data'][0]['customer_bill_tax_type']); //是否税
        $this->assign('tax_rate',$result2['data'][0]['customer_bill_tax']); //税率
        $this->assign('send_name',$result2['data'][0]['project_name']); //发货方
        $this->assign('project_id',$result2['data'][0]['project_id']); //发货方id
        $this->assign('remark',$result2['data'][0]['remark']); //备注
        $this->assign('user_id',$user_id); //创建人
        $this->assign('user_name',$user_name); //创建人名称

        $this->getPageParams($result2);
//
        return $this->fetch('customer_finance_invoice_bill');

    }

    public function showCustomerBillDoneInvoiceInfoManage(){

        $data = Request::instance()->param()['data'];

        $data_info=[
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,

        ];

        $data_info = json_decode($data,true);
        $aaa['customer_bill_number'] = $data_info[0]['customer_bill_number'];
        $result2 = $this->callSoaErp('post','/bill/getCustomerBillOnlly',$aaa);

        $this->assign('project_result',$result2['data'][0]['project_name']);
//        echo "<pre>";
//        var_dump($result2);exit;
//        echo "</pre>";

        //获取账单明细
        $result3 = $this->callSoaErp('post','/bill/getCustomerBillInfoOnlly',$aaa);
        foreach($result3['data'] as $kk1 => $vv1){
            $params_info[$kk1]['finance_id'] = $vv1['finance_id'];
        }
        $result = $this->callSoaErp('post','/bill/getCustomerBillInData',$params_info);

        //获取分公司
        if (!empty(Cache::get('company_cache'))) {
            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        $user_id = session("user")['user_id'];
        $user_name = session("user")['nickname'];

        $nums = 0.000;
        $nums2 = 0.000;
        foreach($result2['data'][0]['info'] as $key=>$val){
            $nums+=number_format($val['money'], 3, '.', '');
            $nums2+=number_format($val['abnormal_money'], 3, '.', '');
        }

        //生成对账编号
//        $customerBillNumber = Help::getReconciliation();

        $this->assign('customerBillResult',$result['data']);
        $this->assign('customerBillNumber',$data_info[0]['customer_bill_number']);
        $this->assign('customer_bill_name',$result2['data'][0]['customer_bill_name']); //客户账单名称
        $this->assign('money',number_format($nums,3, '.', '')); //运单总计
        $this->assign('abnormal_money',number_format($nums2,3, '.', '')); //赔款
        $this->assign('now',date("Y-m-d",$data_info[0]['customer_bill_date'])); //对账时间
        $this->assign('customer_bill_tax_type',$result2['data'][0]['customer_bill_tax_type']); //是否税
        $this->assign('tax_rate',$result2['data'][0]['customer_bill_tax']); //税率
        $this->assign('send_name',$result2['data'][0]['project_name']); //发货方
        $this->assign('project_id',$result2['data'][0]['project_id']); //发货方id
        $this->assign('remark',$result2['data'][0]['remark']); //备注
        $this->assign('user_id',$user_id); //创建人
        $this->assign('user_name',$user_name); //创建人名称

        $this->getPageParams($result2);
//
        return $this->fetch('customer_finance_done_invoice_bill');

    }

    public function showCustomerYesBillInfoManage(){

        $data = Request::instance()->param();

        //获取选中账单信息
        $result = $this->callSoaErp('post','/bill/getCustomerYesBillInData',$data);

//
//        $nums = 0;
//        //计算总计
//        foreach($result['data'] as $key=>$val){
//           $num += $val['abnormal_money'];
//        }
//
//        $asd[count($result['data'])]['customer_bill_number'] = "";
//        $asd[count($result['data'])]['pickup_time'] = "";
//        $asd[count($result['data'])]['abnormal_money'] = $num;
//        $aaa = array_merge($result['data'],$asd);
//        echo "<pre>";
//        var_dump($result['data']);exit;
//        echo "</pre>";

        $this->assign('customerYesBillInfoResult',$result['data']);
        $this->assign('customer_bill_number',$data['customer_bill_number']);

        return $this->fetch('customer_yes_bill_info');
    }

    public function getCustomerYesBillAjax(){

        $data = Request::instance()->param();

        //获取选中账单信息
        $result = $this->callSoaErp('post','/bill/getCustomerYesBillInData',$data);

        foreach($result['data'] as $key=>$val){
            $result['data'][$key]['customer_bill_number'] = $data['customer_bill_number'];
        }

        $new = [
            "status"=> 200,
            "message"=> null,
            "total"=>count($result['data']),
            "rows"=>[
                "item"=>$result['data']
            ]
        ];
        return $new;
    }

    public function addCustomerBillAjax(){

        $data = Request::instance()->param();
     
		array_unshift($data, array_pop($data));
		
        $result = $this->callSoaErp('post', '/bill/addCustomerBill',$data);
        return $result;

    }

    public function editCustomerBillAjax(){

        $data = Request::instance()->param();

        $result = $this->callSoaErp('post', '/bill/editCustomerInvoiceBill',$data);
        return $result;

    }

    /**
     *  圣华
     *  承运商账单页面
     *  韩冰
     */
    public function showSupplierBillManage(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
		$supplierParams =[];
		$result = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);
		$this->assign('supplierResult',$result['data']);

 		return $this->fetch('supplier_bill');
    }

    public function getSupplierBillManageAjax(){

 		$params = Request::instance()->param();
 		$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
			'status'=>1,
			'shipment_finance_status'=>2

 		];
 		if(!empty($params['finance_number'])){
 			$data['finance_number'] = $params['finance_number'];
 		}
		if(!empty($params['supplier_uuid'])){
 			$data['supplier_uuid'] = $params['supplier_uuid'];
 		}
		if(!empty($params['start_shipment_time'])){
 			$data['start_shipment_time'] = strtotime($params['start_shipment_time']);
 		}
		if(!empty($params['end_shipment_time'])){
 			$data['end_shipment_time'] = strtotime($params['end_shipment_time']);
 		}
		if(!empty($params['orders_number'])){
 			$data['orders_number'] = $params['orders_number'];
 		}		
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}
 		$result = $this->callSoaErp('post', '/shipment/getShipmentInfo', $data);


 		for($i=0;$i<count($result['data']['list']);$i++){

 			if(is_numeric($result['data']['list'][$i]['shipment_time'])){//发运日期
 				$result['data']['list'][$i]['shipment_time'] = date('Y-m-d',$result['data']['list'][$i]['shipment_time']);
 			}else{
 				$result['data']['list'][$i]['shipment_time']='';
 			}
			if(is_numeric($result['data']['list'][$i]['pickup_time'])){//发运日期
 				$result['data']['list'][$i]['pickup_time'] = date('Y-m-d',$result['data']['list'][$i]['pickup_time']);
 			}else{
 				$result['data']['list'][$i]['pickup_time']='';
 			}
 			$result['data']['list'][$i]['shipment_type_name']=Contents::baseConfig()['shipment']['shipment_type'][$result['data']['list'][$i]['shipment_type']];
 			$result['data']['list'][$i]['shipment_finance_name']=Contents::baseConfig()['shipment']['shipment_finance_status'][$result['data']['list'][$i]['shipment_finance_status']];

 			$sh_ab = $result['data']['list'][$i]['shipment_abnormal'];
 			$ab_money = 0;
 			for($j=0;$j<count($sh_ab);$j++){
 				$ab_money+=$sh_ab[$j]['shipment_abnormal_money'];
 			}

			if($result['data']['list'][$i]['shipment_type']==3){//代表短驳
				$shipment_short_barge_info = $result['data']['list'][$i]['shipment_short_barge_info'];
				$shipment_short_barge_orders_number='';
				for($k=0;$k<count($shipment_short_barge_info);$k++){
					$shipment_short_barge_orders_number.='/'.$shipment_short_barge_info[$k]['orders_number'];
				}

				$result['data']['list'][$i]['orders_number'] = trim($shipment_short_barge_orders_number,'/');
			}

 			$result['data']['list'][$i]['shipment_abnormal_money'] = $ab_money;
 		}
 		$r = [
			'code'=>200,
			'data'=>$result['data']['list'],
			'count'=>	$result['data']['count'],

		];
 		return $r;
    }
	public function addSupplierBill(){

		$params = Request::instance()->param();
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

		if(!empty($params['shipment_id'])){
			$shipmentString = $params['shipment_id'];
			$shipmentArray = explode(',',$shipmentString);


			$shipment_id_string ='';
			for($i=0;$i<count($shipmentArray);$i++){
				if($i==0){
					$shipment_id_string="'".$shipmentArray[$i]."'";
				}else{
					$shipment_id_string.=",'".$shipmentArray[$i]."'";
				}
			}

			

		}else{
			//获取shipment_uuid
			$supplierBillNumberParams['supplier_bill_number'] = $params['supplier_bill_number'];

			//获取对账信息

			$supplier_bill_result  = $this->callSoaErp('post', '/bill/getSupplierBill', $supplierBillNumberParams);


			$supplier_bill_number_result = $supplier_bill_result['data'][0]['supplier_bill_info_array'];

			for($i=0;$i<count($supplier_bill_number_result);$i++){
				if($i==0){
					$shipment_uuid_string="'".$supplier_bill_number_result[$i]['shipment_uuid']."'";
				}else{
					$shipment_uuid_string.=",'".$supplier_bill_number_result[$i]['shipment_uuid']."'";
				}
			}

			$this->assign('supplier_bill_result',$supplier_bill_result['data'][0]);
		}
		

		$data = [

				'status'=>1,
				'shipment_id_string'=>$shipment_id_string,
				'shipment_uuid_string'=>$shipment_uuid_string
		];

		
		$result = $this->callSoaErp('post', '/shipment/getShipmentInfo', $data);
		
		$supplier_uuid = $result['data'][0]['supplier_uuid'];

		$supplierParams['supplier_uuid']= $supplier_uuid;
		$this->assign('supplier_uuid',$supplier_uuid);
		//获取默认税率
		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);


//        var_dump($supplierResult['data'][0]);exit;
		$this->assign('supplierResult',$supplierResult['data'][0]);
        $this->assign('supplier_bill_name',$supplierResult['data'][0]['supplier_name'].date("Y")."年".date('m',strtotime('-1 month'))."月对账单");


        $pay_all_money = 0;
		$abnormal_all_money = 0;
		$dfk=0;
		$dfk_array = [];
		for($i=0;$i<count($result['data']);$i++){

			if(is_numeric($result['data'][$i]['shipment_time'])){//发运日期
				$result['data'][$i]['shipment_time'] = date('Y-m-d',$result['data'][$i]['shipment_time']);
			}else{
				$result['data'][$i]['shipment_time']='';
			}
			$result['data'][$i]['shipment_type_name']=Contents::baseConfig()['shipment']['shipment_type'][$result['data'][$i]['shipment_type']];
			$result['data'][$i]['shipment_finance_name']=Contents::baseConfig()['shipment']['shipment_finance_status'][$result['data'][$i]['shipment_finance_status']];

			$sh_ab = $result['data'][$i]['shipment_abnormal'];
			$ab_money = 0;
			for($j=0;$j<count($sh_ab);$j++){
				$ab_money+=$sh_ab[$j]['shipment_abnormal_money'];
			}
			$result['data'][$i]['shipment_abnormal_money'] = $ab_money;
			$pay_all_money = $pay_all_money+$result['data'][$i]['pay_all_money'];

			$abnormal_all_money=$abnormal_all_money+$ab_money;
			
			//先判断dfk_array
			if(!in_array($result['data'][$i]['orders_number'],$dfk_array)){
				$dfk+=$result['data'][$i]['dfk'];
				
				$dfk_array[]=$result['data'][$i]['orders_number'];
			}
		}
		$this->assign('pay_all_money',$pay_all_money);
		$this->assign('abnormal_all_money',$abnormal_all_money);
		$this->assign('dfk',$dfk);
		$this->assign('result',$result['data']);
		return $this->fetch('add_supplier_bill');

	}
	public function getsupplierbillinfo(){

		$params = Request::instance()->param();
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);


			//获取shipment_uuid
			$supplierBillNumberParams['supplier_bill_number'] = $params['supplier_bill_number'];

			//获取对账信息

			$supplier_bill_result  = $this->callSoaErp('post', '/bill/getSupplierBill', $supplierBillNumberParams);


			$supplier_bill_number_result = $supplier_bill_result['data'][0]['supplier_bill_info_array'];

			for($i=0;$i<count($supplier_bill_number_result);$i++){
				if($i==0){
					$shipment_id_string="'".$supplier_bill_number_result[$i]['shipment_id']."'";
				}else{
					$shipment_id_string.=",'".$supplier_bill_number_result[$i]['shipment_id']."'";
				}
			}

			$this->assign('supplier_bill_result',$supplier_bill_result['data'][0]);
		
		

		$data = [

				'status'=>1,

				'shipment_id_string'=>$shipment_id_string
		];


		$result = $this->callSoaErp('post', '/shipment/getShipmentInfo', $data);

		$supplier_uuid = $result['data'][0]['supplier_uuid'];

		$supplierParams['supplier_uuid']= $supplier_uuid;
		$this->assign('supplier_uuid',$supplier_uuid);
		//获取默认税率
		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);



		$this->assign('supplierResult',$supplierResult['data'][0]);



		$pay_all_money = 0;
		$abnormal_all_money = 0;
		for($i=0;$i<count($result['data']);$i++){

			if(is_numeric($result['data'][$i]['shipment_time'])){//发运日期
				$result['data'][$i]['shipment_time'] = date('Y-m-d',$result['data'][$i]['shipment_time']);
			}else{
				$result['data'][$i]['shipment_time']='';
			}
			$result['data'][$i]['shipment_type_name']=Contents::baseConfig()['shipment']['shipment_type'][$result['data'][$i]['shipment_type']];
			$result['data'][$i]['shipment_finance_name']=Contents::baseConfig()['shipment']['shipment_finance_status'][$result['data'][$i]['shipment_finance_status']];

			$sh_ab = $result['data'][$i]['shipment_abnormal'];
			$ab_money = 0;
			for($j=0;$j<count($sh_ab);$j++){
				$ab_money+=$sh_ab[$j]['shipment_abnormal_money'];
			}
			$result['data'][$i]['shipment_abnormal_money'] = $ab_money;
			$pay_all_money = $pay_all_money+$result['data'][$i]['pay_all_money'];

			$abnormal_all_money=$abnormal_all_money+$ab_money;
		}
		$this->assign('pay_all_money',$pay_all_money);
		$this->assign('abnormal_all_money',$abnormal_all_money);

		$this->assign('result',$result['data']);
		return $this->fetch('supplier_bill_info');

	}	
	//添加承运商对账AJAX
    public function addSupplierBillAjax(){

        $data = Request::instance()->param();

	
        $data['shipment_id_array'] = explode(',',$data['shipment_id_array']);
		$data['supplier_bill_date'] = strtotime($data['supplier_bill_date']);

		if(!empty($data['supplier_bill_number'])){
			$result = $this->callSoaErp('post', '/bill/updateSupplierBill',$data);
		}else{
			$result = $this->callSoaErp('post', '/bill/addSupplierBill',$data);
		}

		
        return $result;

    }
    //修改承运商对账
    public function updateSupplierBillAjax(){
    	$data = Request::instance()->param();

    	if(!empty($data['supplier_bill_number_array'])){
    		$data1 = explode(',',$data['supplier_bill_number_array']);
    		unset($data['supplier_bill_number_array']);
    		$data['supplier_bill_number_array'] = $data1;
    	}

	
    	$result = $this->callSoaErp('post', '/bill/updateSupplierBill',$data);


    	return $result;
    }
    //已对账
    public function showSupplierBillOverManage(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

    	return $this->fetch('supplier_bill_over');
    }
	    //已对账
    public function getSupplierBillOverAjax(){
    	$params = Request::instance()->param();
    	$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
    		'status'=>1

    	];
    	if(!empty($params['supplier_bill_name'])){
    		$data['supplier_bill_name'] = $params['supplier_bill_name'];
    	}
    	if(is_numeric($params['supplier_bill_status'])){
    		$data['supplier_bill_status'] = $params['supplier_bill_status'];
    	}
		if(is_numeric($params['shipment_invoice_status'])){
    		$data['shipment_invoice_status'] = $params['shipment_invoice_status'];
    	}
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}


    	$result = $this->callSoaErp('post', '/bill/getSupplierBill',$data);
    	for($i=0;$i<count($result['data']['list']);$i++){
    		$result['data']['list'][$i]['supplier_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['supplier_bill_status']];
    		$result['data']['list'][$i]['supplier_bill_date'] = date('Y-m-d',$result['data']['list'][$i]['supplier_bill_date']);
    		if($result['data']['list'][$i]['supplier_bill_tax_type']==1){

				$result['data']['list'][$i]['supplier_bill_tax_type_name']='否';
			}else{
				$result['data']['list'][$i]['supplier_bill_tax_type_name']='是';
			}
    	}

 		$r = [
			'code'=>200,
			'data'=>$result['data']['list'],
			'count'=>	$result['data']['count'],

		];
 		return $r;
    }
    //未开票
    public function supplierBillMissInvoice(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

    	return $this->fetch('supplier_bill_miss_invoice');
    }
    //已开票
    public function supplierBillDoneInvoice(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

    	return $this->fetch('supplier_bill_done_invoice');
    }
    //财务已收票
    public function supplierBillFinanceGetInvoice(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

    	return $this->fetch('supplier_bill_finance_get_invoice');
    }
	 //财务已上传付款通知
    public function supplierBillAddCostInfo(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

    	return $this->fetch('supplier_bill_add_cost_info');
    }
	 //财务确认付款通知
    public function supplierBillAggreCostInfo(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

    	return $this->fetch('supplier_bill_aggre_cost_info');
    }	
	 //承运商已付款
    public function supplierBillDonePay(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

    	return $this->fetch('supplier_bill_done_pay');
    }		
	
	//账单上传发票凭证
	public function supplierBillAddInvoice(){
		
		$data = Request::instance()->param();
		
		return $this->fetch('supplier_bill_add_invoice');
	}
	
	//客服确认

	public function supplierBillCost(){
		
		$params = Request::instance()->param();

		//获取shipment_uuid
		$supplierBillNumberParams['supplier_bill_number'] = $params['supplier_bill_number'];

		//获取对账信息

		$supplier_bill_result  = $this->callSoaErp('post', '/bill/getSupplierBill', $supplierBillNumberParams);
	
		$supplier_bill_number_result = $supplier_bill_result['data'][0]['supplier_bill_info_array'];
		
	
	
		for($i=0;$i<count($supplier_bill_number_result);$i++){
			if($i==0){
				$shipment_id_string="'".$supplier_bill_number_result[$i]['shipment_id']."'";
			}else{
				$shipment_id_string.=",'".$supplier_bill_number_result[$i]['shipment_id']."'";
			}
		}

		$this->assign('supplier_bill_result',$supplier_bill_result['data'][0]);

		

		
		$data = [

				'status'=>1,

				'shipment_id_string'=>$shipment_id_string
		];


		$result = $this->callSoaErp('post', '/shipment/getShipmentInfo', $data);
	
		

		$pay_all_money = 0;
		$abnormal_all_money = 0;
		for($i=0;$i<count($result['data']);$i++){

			if(is_numeric($result['data'][$i]['shipment_time'])){//发运日期
				$result['data'][$i]['shipment_time'] = date('Y-m-d',$result['data'][$i]['shipment_time']);
			}else{
				$result['data'][$i]['shipment_time']='';
			}
			$result['data'][$i]['shipment_type_name']=Contents::baseConfig()['shipment']['shipment_type'][$result['data'][$i]['shipment_type']];
			$result['data'][$i]['shipment_finance_name']=Contents::baseConfig()['shipment']['shipment_finance_status'][$result['data'][$i]['shipment_finance_status']];
		
			$sh_ab = $result['data'][$i]['shipment_abnormal'];
			$ab_money = 0;
			for($j=0;$j<count($sh_ab);$j++){
				$ab_money+=$sh_ab[$j]['shipment_abnormal_money'];
			}
			$result['data'][$i]['shipment_abnormal_money'] = $ab_money;
			$pay_all_money = $pay_all_money+$result['data'][$i]['pay_all_money'];

			$abnormal_all_money=$abnormal_all_money+$ab_money;
		}
		$this->assign('pay_all_money',$pay_all_money);
		$this->assign('abnormal_all_money',$abnormal_all_money);

		$this->assign('result',$result['data']);
		return $this->fetch('supplier_bill_cost');		
		
	}	
	
    //客户账单导出
    public function export(){
        $data = Request::instance()->param();

        //获取选中账单信息
        $result = $this->callSoaErp('post','/bill/getCustomerYesBillInData',$data);
//        var_dump($result);exit;

        $new_data = [];
        foreach($result['data'] as $key=>$val) {
            $new_data[$key]['finance_id'] = $key+1; //序号
            $new_data[$key]['customer_bill_number'] = $data['customer_bill_number']; //客户账单编号
            $new_data[$key]['orders_number'] = $val['orders_number']; //运单编号
            $new_data[$key]['pickup_time'] =  date('Y-m-d', $val['pickup_time']); //运单日期
            $new_data[$key]['verify_status'] = Contents::baseConfig()['order']['verify_status'][$val['verify_status']]; //开票状态
            $new_data[$key]['project_name'] = $val['project_name']; //项目
            $new_data[$key]['send_goods_name'] = $val['send_goods_name']; //发货方
            $new_data[$key]['send_city_name'] = $val['send_city_name']; //发站
            $new_data[$key]['accept_city_name'] = $val['accept_city_name']; //到站
            $new_data[$key]['accept_goods_name'] = $val['accept_goods_name']; //收货方
            $new_data[$key]['goods_name'] = $val['goods_name']; //货物名称
            $new_data[$key]['realy_count'] = $val['realy_count']; //件数
            $new_data[$key]['realy_pack_count'] = $val['realy_pack_count']; //数量
            $new_data[$key]['realy_weight'] = $val['realy_weight']; //重量
            $new_data[$key]['realy_volume'] = $val['realy_volume']; //体积
            $new_data[$key]['delivery_method'] = Contents::baseConfig()['order']['delivery_method'][$val['delivery_method']]; //送货方式
            $new_data[$key]['abnormal_status'] = Contents::baseConfig()['order']['abnormal']['abnormal_status'][$val['abnormal_status']]; //异常
            $new_data[$key]['money'] = $val['money']; //总费用
            $new_data[$key]['abnormal_money'] = $val['abnormal_money']; //赔款
        }

        $name = "客户已对账单";
        $xAxis = [];
        $title = "客户已对账单";
        $header = array('序号', '对账编号', '运单编号', '运单日期', '开票状态', '项目' , '发货方' , '发站', '到站', '收货方', '货物名称', '件数', '数量', '重量', '体积', '送货方式', '异常', '总费用', '赔款');

        $filter = [
            [
                "code"=>"A",
                "value"=>8
            ],
            [
                "code"=>"B",
                "value"=>24
            ],
            [
                "code"=>"C",
                "value"=>26
            ],
            [
                "code"=>"D",
                "value"=>26
            ]
        ];

        //导出excel
        $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name, $filter);
    }
}