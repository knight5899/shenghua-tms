<?php


namespace app\index\controller\device;


use app\index\controller\Base;

class Smartbox extends Base
{

    public function smartboxManage(){

        $data=[];
        $result = $this->callSoaErp('post','/device.Smartboxmanage/getboxList',$data);


        $this->getPageParams($result);


        return $this->fetch('device/smart_box_manage');
    }

}