<?php
/**
 * Created by PhpStorm.
 * User: 胡
 * Date: 20190/02/19
 * Time: 16:40
 */

namespace app\index\controller;

use app\common\help\Contents;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class Dispatch extends Base
{


    public function uploadDispatchChildAjax(){
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/dispatch/uploadDispatchChild', $data);
        return json($data);
    }

    public function uploadDispatch(){


        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/dispatch/getDispatchById', $data);

        // echo json_encode($result);exit();

        $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierData);

        $this->assign('supplierResult', $supplierResult['data']);


        //获取所有省
        $province_data = Help::getCity(0, 1);

        //获取所有市
        $city_data = Help::getCity(0, 2);
        //获取所有区
        $area_data = Help::getCity(0, 3);


        $this->assign('provinceResult', $province_data);
        $this->assign('cityResult', $city_data);
        $this->assign('areaResult', $area_data);

        $this->getPageParams($result);
        $zhongzhuan_total=0;
        $tihuo_total=0;
        foreach ($result['data']['list']['dispatch_info'] as $k=>$v){
            if($v['dispatch_type']==1){
                $zhongzhuan_total++;
            }if($v['dispatch_type']==2){
                $tihuo_total++;
            }

        }
        $this->assign('zhongzhuan_total', $zhongzhuan_total);
        $this->assign('tihuo_total', $tihuo_total);


        preg_match_all('/\d+/',$result['data']['list']['order_number'],$orders_id);


        $orderData['orders_id'] = implode(',',$orders_id[0]);
        $orderData['orders_goods_id_is_like'] = 1;
        $orderGoodsResult = $this->callSoaErp('post', '/order/getOrderGoods', $orderData);
        $this->assign('orderGoodsResult', $orderGoodsResult['data']);

        return $this->fetch('upload_dispatch');

    }
    public function dispatchGoods()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/dispatch/getDispatchGoods', $data);
        $this->getPageParams($result);
        return $this->fetch('dispatch_goods');

    }

    public function getTransportAjax()
    {
        $data = Request::instance()->param();
        if (isset($data['limit']))
            $data['page_size'] = $data['limit'];
        $result = $this->callSoaErp('post', '/dispatch/getTransport', $data);
        return $this->getLayuiData($result);


    }


    public function getDispatchAjax()
    {
        $data = Request::instance()->param();

        if (isset($data['limit']))
            $data['page_size'] = $data['limit'];
        $result = $this->callSoaErp('post', '/dispatch/getDispatch', $data);
        return $this->getLayuiData($result);

    }

    public function showTransportManage()
    {
        $data = Request::instance()->param();


        // $result = $this->callSoaErp('post','/dispatch/getDispatch');
        $result = $this->callSoaErp('post', '/dispatch/getTransport', $data);


        $this->getPageParams($result);
        return $this->fetch('show_transport_manage');
    }


    public function showDispatchManage()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/dispatch/getDispatch', $data);
        $this->assign('lists', $result['data']['data']);
        $this->getPageParams($result);
        return $this->fetch('show_dispatch_manage');
    }

    public function PickupOrderAddAjax()
    {
        $params = Request::instance()->param();


        $data = $this->callSoaErp('post', '/dispatch/addTransport', $params);
        return $data;
    }



    public function changeOrderStatus(){
        $params = Request::instance()->param();
        $order_number=$params['orders_number'];
        preg_match_all('/\d+/',$order_number,$order_number);
        $orders_id=implode(',',$order_number[0]);

        $data = [
            'page' => $this->page(),
            'page_size' => $this->_page_size,
            'orders_id_is_like'=>1,
            'orders_id'=>$orders_id

        ];
         $order_status=Contents::orderStatus();
         array_pop($order_status);

           $this->assign('orderStatus',$order_status);
        $result = $this->callSoaErp('post', '/order/getOrder', $data);
        $result = Help::getOrder($result);
        $this->getPageParams($result);
        return $this->fetch('change_order_status');

    }

    /**
     *  揽单显示页面
     */
    public function showPickupOrderManage()
    {
        //读取所有的项目

        $params = Request::instance()->param();


        $projectResult = $this->callSoaErp('post', '/source/getProject', $data);
        $this->assign('projectResult', $projectResult['data']);

        $data = [
            'page' => $this->page(),
            'page_size' => $this->_page_size,
            'order_status' => 1

        ];
        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        }
        if (!empty($params['accept_name'])) {
            $data['accept_name'] = $params['accept_name'];
        }
        if (!empty($params['accept_cellphone'])) {
            $data['accept_cellphone'] = $params['accept_cellphone'];
        }
        $data['despatch_finish']=0;


        $result = $this->callSoaErp('post', '/order/getOrder', $data);

        $this->assign('despatch_record', $result['data']['despatch_record']);
        $this->assign('despatch_print_record', $result['data']['despatch_print_record']);

        $result = Help::getOrder($result);

        $this->getPageParams($result);
        return $this->fetch('pickup_order_manage');


    }


    /**
     * 揽单新增页面
     */
    public function showPickupOrderAdd()
    {

        $orders_id = input('orders_id');
        //获取所有城市
        //如果选择了项目
        if (!empty($orders_id)) {
            //获取订单信息
            $orderData['orders_id'] = $orders_id;
            $orderData['orders_id_is_like'] = 1;
            //获取订单信息
            $orderResult = $this->callSoaErp('post', '/order/getOrder', $orderData);

            $orderList=$orderResult['data'];

            if(count($orderList)>1)
            for ($i=0;$i<count($orderList);$i++)
            {
                for ($j=0;$j<count($orderList);$j++)
                {
                    if($orderList[$i]['accept_area_name'] != $orderList[$j]['accept_area_name'] || $orderList[$i]['accept_address'] != $orderList[$j]['accept_address'])
                    return $this->error("收货地址不一样不能合并订单");
                }
            }

            //echo json_encode($orderList,256);exit();


            $this->assign('orderResult', $orderResult['data']);
            $orderGoodsData['orders_id'] = $orders_id;
            $orderGoodsData['orders_goods_id_is_like'] = 1;
            $orderGoodsResult = $this->callSoaErp('post', '/order/getOrderGoods', $orderGoodsData);
            $this->assign('orderGoodsResult', $orderGoodsResult['data']);

            //获取货物信息
            //获取所有省
            $province_data = Help::getCity(0, 1);
            //获取所有市
            $city_data = Help::getCity(0, 2);
            //获取所有区
            $area_data = Help::getCity(0, 3);
            $this->assign('provinceResult', $province_data);
            $this->assign('cityResult', $city_data);
            $this->assign('areaResult', $area_data);
            //获取供应商信息

            $supplierData['status'] = 1;
            $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierData);

            $this->assign('supplierResult', $supplierResult['data']);
        }
        $result = $this->callSoaErp('post','/system/getTaxrate',['status'=>1]);
        $this->assign('fee', $result['data']['list']);

        return $this->fetch('pickup_order_add');
    }

    /**
     * 新增运单
     */
    public function addTransportAjax()
    {
        $data = Request::instance()->param();


        $dataResult = [];
        for ($i = 0; $i < count($data['orders_number']); $i++) {
            $dataResult[$i]['orders_number'] = $data['orders_number'][$i];

            $dataResult[$i]['supplier_id'] = $data['supplier_id'][$i];

            if (is_numeric($data['pickup_time'][$i])) {
                $dataResult[$i]['pickup_time'] = strtotime($data['pickup_time'][$i]);
            }

            if (is_numeric($data['send_time'][$i])) {
                $dataResult[$i]['send_time'] = strtotime($data['send_time'][$i]);
            }

            $dataResult[$i]['send_province_id'] = $data['send_province_id'][$i];
            $dataResult[$i]['send_city_id'] = $data['send_city_id'][$i];
            $dataResult[$i]['send_area_id'] = $data['send_area_id'][$i];
            $dataResult[$i]['send_address'] = $data['send_address'][$i];
            $dataResult[$i]['accept_province_id'] = $data['accept_province_id'][$i];
            $dataResult[$i]['accept_city_id'] = $data['accept_city_id'][$i];
            $dataResult[$i]['accept_area_id'] = $data['accept_area_id'][$i];
            $dataResult[$i]['accept_address'] = $data['accept_address'][$i];
            $dataResult[$i]['remark'] = $data['remark'][$i];

        }
        $dataResult['dispatch_type'] = 5;

        //$this->callSoaErp('post','/dispatch/addTransport',$data);


    }
}