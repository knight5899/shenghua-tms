<?php
namespace app\index\controller;
use app\common\help\Help;
use think\Lang;
use think\config;
use think\Request;
use think\Controller;
use app\common\help\Contents;
use Underscore\Types\Arrays;
use think\Cache;

class BookingBase extends Controller
{
    protected $_soaerpConfig;
    protected $_soaerpUrl;
    protected $_controller_name;
    protected $_function_name;
    protected $_page_size;
    protected $_authConfig;
    protected $_company_id;

    public function __construct()
    {
        if(empty(session('booking_user')) || is_null(session('booking_user'))){
            echo "<script>location.href='/booking_login/index'</script>";
        }
        parent::__construct();


        //开始判断权限是否有
        $controller_name =  strtolower(Request::instance()->controller());
        $function_name =  Request::instance()->action();

        $this->assign('controller_name',$controller_name);
        $this->assign('function_name',$function_name);


    }

    /**
     * 读取soaerp方法
     */
    public function callSoaErp($method,$function,$data=[]){

        $soaerp_config = config('soaerp');
        $soa_erp_url = $soaerp_config['ip'].':'.$soaerp_config['port'];
        $data['appKey'] = 'nexus';
        $data['appSecret']='nexusIt';

        $result = Help::http($method,$soa_erp_url.$function,$data);

        //dump($result);exit();
        $result = json_decode($result,true);

        if($result['code']==200){
            //$this->outPut($result['data']);
            $result = ['code' => '200', 'msg' => 'success','data'=>$result['data']];

        }else{
            //$this->outPutError($result);
            $result = ['code' => '400', 'msg' => $result['msg']];
        }
        return $result;
    }
    //错误日志
    protected function outPutError($errorMsg) {

        // \think\Response::create(['code' => '400', 'msg' => $errorMsg['msg']], 'json')->send();
        //防止意外发生
        // exit;
    }
    //输出 之后加上日志存放REDIS
    public function outPut($result) {

        if(empty($result)){
            $result = [];
        }
        // \think\Response::create(['code' => '200', 'data' => $result], 'json')->send();
        //防止意外发生
        //exit;
    }

    //获取当前页的方法
    public function page(){
    	$page = input('page')?input('page'):1;
    	return $page;
    }
	/**
	 * 获取分页的一些参数
	 */
    public function getPageParams($result){
    	$this->assign('total',$result['data']['count']);//总数
    	$this->assign('page',$this->page()); //当前页数
    	$this->assign('total_page',$result['data']['page_count']);//总共分几页
    	$this->assign('data',$result['data']['list']);//分页数据
    
    }
    
    protected function input() {
    	$contents = file_get_contents("php://input");
    	
    	$param = json_decode($contents, TRUE);
    	if (empty($param))
    		return $this->output([], 'json error', '0004');
    	return $param;
    }

}
