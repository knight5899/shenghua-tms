<?php
/**
 * Created by PhpStorm.
 * User: 胡
 * Date: 20190/02/19
 * Time: 16:40
 */

namespace app\index\controller;

use app\common\help\Contents;
use think\Validate;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class Ordermobile extends Base
{


    public function getOrders(){
        $params = Request::instance()->param();
        //$projectResult = $this->callSoaErp('post', '/order/subFinance', $params);

        //return $projectResult;
        return $this->fetch('search_orders');
    }
    /**
     * 获取客户跟踪AJAX
     */
    public function getOrderCustomerFollowAjax(){
    	$data = Request::instance()->param();
    
    
    	$result = $this->callSoaErp('post', '/order/getOrderCustomerFollow',$data);
    
		$result = $result['data'];
		
		for($i=0;$i<count($result);$i++){
			$result[$i]['orders_status_name'] =$this->baseConfig['order']['order_status'][$result[$i]['order_status']]['order_status_name'];
			$result[$i]['create_time'] = date('Y-m-d H:i:s',$result[$i]['create_time']); 
		}
	
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }
}