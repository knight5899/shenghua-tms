<?php
/**
 * Created by PhpStorm.
 * User: Hugh
 * Date: 2019/11/04
 * Time: 13:40
 */

namespace app\index\controller;
use app\common\help\Contents;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class B2bReporting extends Base
{
    public function top10AgentsByState(){

        $w['created_from'] = $_GET['created_from'];
        $w['created_to'] = $_GET['created_to'];

        $top10AgentsByState = $this->callSoaErp('post','/b2b_operate/top10AgentsByState',$w);

        $ar = Arrays::group($top10AgentsByState['data'],'is_inbound');
        $rr = [];
        foreach($ar as $k=>$v){
            $rr[$k] = Arrays::group($v,'state');
            foreach($rr[$k] as $kk=>$vv){
                $rr[$k][$kk] = Arrays::group($vv,'agent_id');
                foreach($rr[$k][$kk] as $kkk=>$vvv){
//                    echo $kkk;
                    $rr[$k][$kk][$kkk]['count'] = count($vvv);
                    $rr[$k][$kk][$kkk]['distributor_name'] = $vvv[0]['distributor_name'];
                    $rr[$k][$kk][$kkk]['distributor_name_chinese'] = $vvv[0]['distributor_name_chinese'];
                }
            }
        }
//        echo '<pre>';print_r($rr);exit;

        $this->assign('dd',$rr);

        return $this->fetch('top10AgentsByState');
    }

    public function top10AgentsByStateExcel(){

        $w['created_from'] = $_GET['created_from'];
        $w['created_to'] = $_GET['created_to'];

        $top10AgentsByState = $this->callSoaErp('post','/b2b_operate/top10AgentsByState',$w);

        $ar = Arrays::group($top10AgentsByState['data'],'is_inbound');
        $rr = [];
        foreach($ar as $k=>$v){
            $rr[$k] = Arrays::group($v,'state');
            foreach($rr[$k] as $kk=>$vv){
                $rr[$k][$kk] = Arrays::group($vv,'agent_id');
                foreach($rr[$k][$kk] as $kkk=>$vvv){
//                    echo $kkk;
                    $rr[$k][$kk][$kkk]['count'] = count($vvv);
                    $rr[$k][$kk][$kkk]['distributor_name'] = $vvv[0]['distributor_name'];
                    $rr[$k][$kk][$kkk]['distributor_name_chinese'] = $vvv[0]['distributor_name_chinese'];
                }
            }
        }

        $this->assign('dd',$rr);

        $fileName = "TopTen Agent Excel " . date("Y-m-d His");
        header("HTTP/1.0 200 OK");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header('Cache-Control: max-age=0'); //no cache
        header("Content-disposition: attachment; filename=\"{$fileName}.xls\"");

        return $this->fetch('top10AgentsByState_msxls');
    }


    public function reportBookings(){
        $w['BookingCreatedFrom'] = $_GET['BookingCreatedFrom'];
        $w['BookingCreatedTo'] = $_GET['BookingCreatedTo'];
        $w['TourDateFrom'] = $_GET['TourDateFrom'];
        $w['TourDateTo'] = $_GET['TourDateTo'];
        $w['TourName'] = $_GET['TourName'];
        $w['TourType'] = $_GET['TourType'];
        $w['AgentName'] = $_GET['AgentName'];
        $w['State'] = $_GET['State'];

        $reportBookings =  $this->callSoaErp('post','/b2b_operate/reportBookings',$w);
        $reportBookingsGroup = Arrays::group($reportBookings['data'],'is_inbound');
//        echo '<pre>';print_r($reportBookingsGroup);exit;
        $this->assign('dd',$reportBookingsGroup);

        //获取代理的 State
        $distributorState = $this->callSoaErp('post','/b2b_operate/getDistributorState',[]);
        $this->assign('distributorState',$distributorState['data']);
        return $this->fetch('reportBookings');
    }

    public function reportBookingsExcel(){

        $w['BookingCreatedFrom'] = $_GET['BookingCreatedFrom'];
        $w['BookingCreatedTo'] = $_GET['BookingCreatedTo'];
        $w['TourDateFrom'] = $_GET['TourDateFrom'];
        $w['TourDateTo'] = $_GET['TourDateTo'];
        $w['TourName'] = $_GET['TourName'];
        $w['TourType'] = $_GET['TourType'];
        $w['AgentName'] = $_GET['AgentName'];
        $w['State'] = $_GET['State'];

        $reportBookings =  $this->callSoaErp('post','/b2b_operate/reportBookings',$w);
        $this->assign('dd',$reportBookings['data']);

        $fileName = "Dynamic Report - Booking " . date("Y-m-d His");
        header("HTTP/1.0 200 OK");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header('Cache-Control: max-age=0'); //no cache
        header("Content-disposition: attachment; filename=\"{$fileName}.xls\"");

        return $this->fetch('reportBookings_msxls');
    }



    public function reportTours(){
        $w['BookingCreatedFrom'] = $_GET['BookingCreatedFrom'];
        $w['BookingCreatedTo'] = $_GET['BookingCreatedTo'];
        $w['TourDateFrom'] = $_GET['TourDateFrom'];
        $w['TourDateTo'] = $_GET['TourDateTo'];
        $w['TourName'] = $_GET['TourName'];
        $w['TourType'] = $_GET['TourType'];
        $w['AgentName'] = $_GET['AgentName'];
        $w['State'] = $_GET['State'];


        $reportTours = $this->callSoaErp('post','/b2b_operate/reportTours',$w);
        $this->assign('dd',$reportTours['data']);
//        echo '<pre>';print_r($reportTours);exit;
        //获取代理的 State
        $distributorState = $this->callSoaErp('post','/b2b_operate/getDistributorState',[]);
        $this->assign('distributorState',$distributorState['data']);

        return $this->fetch('reportTours');
    }

    public function reportToursExcel(){
        $w['BookingCreatedFrom'] = $_GET['BookingCreatedFrom'];
        $w['BookingCreatedTo'] = $_GET['BookingCreatedTo'];
        $w['TourDateFrom'] = $_GET['TourDateFrom'];
        $w['TourDateTo'] = $_GET['TourDateTo'];
        $w['TourName'] = $_GET['TourName'];
        $w['TourType'] = $_GET['TourType'];
        $w['AgentName'] = $_GET['AgentName'];
        $w['State'] = $_GET['State'];
        $reportTours = $this->callSoaErp('post','/b2b_operate/reportTours',$w);
        $this->assign('dd',$reportTours['data']);

        $fileName = "Dynamic Report - Tour " . date("Y-m-d His");
        header("HTTP/1.0 200 OK");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header('Cache-Control: max-age=0'); //no cache
        header("Content-disposition: attachment; filename=\"{$fileName}.xls\"");

        return $this->fetch('reportTours_msxls');
    }



    public function reportAgents(){
        $w['BookingCreatedFrom'] = $_GET['BookingCreatedFrom'];
        $w['BookingCreatedTo'] = $_GET['BookingCreatedTo'];
        $w['TourDateFrom'] = $_GET['TourDateFrom'];
        $w['TourDateTo'] = $_GET['TourDateTo'];
        $w['TourName'] = $_GET['TourName'];
        $w['State'] = $_GET['State'];

        $reportAgents = $this->callSoaErp('post','/b2b_operate/reportAgents',$w);
//        echo "<pre>";print_r($reportAgents);exit;
        $this->assign('dd',$reportAgents['data']);


        //获取代理的 State
        $distributorState = $this->callSoaErp('post','/b2b_operate/getDistributorState',$w);
        $this->assign('distributorState',$distributorState['data']);

        return $this->fetch('reportAgents');
    }


    public function reportAgentsExcel(){
        $w['BookingCreatedFrom'] = $_GET['BookingCreatedFrom'];
        $w['BookingCreatedTo'] = $_GET['BookingCreatedTo'];
        $w['TourDateFrom'] = $_GET['TourDateFrom'];
        $w['TourDateTo'] = $_GET['TourDateTo'];
        $w['TourName'] = $_GET['TourName'];
        $w['State'] = $_GET['State'];

        $reportAgents = $this->callSoaErp('post','/b2b_operate/reportAgents',$w);
//        echo "<pre>";print_r($reportAgents);exit;
        $this->assign('dd',$reportAgents['data']);


        $fileName = "Dynamic Report - Agent " . date("Y-m-d His");
        header("HTTP/1.0 200 OK");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header('Cache-Control: max-age=0'); //no cache
        header("Content-disposition: attachment; filename=\"{$fileName}.xls\"");

        return $this->fetch('reportAgents_excel');
    }


    public function noOfPax(){
        $data['status'] = 1;
        $data['company_id'] =  session('user')['company_id'];
        $result = $this->callSoaErp('post', '/b2b_tour/getB2bTour',$data);
        $this->assign('all_tours',$result['data']);

        $arrGroup = Arrays::group($result['data'],'btb_tour_id');
        $this->assign('group_all_tours',$arrGroup);

        return $this->fetch('noOfPax');
    }



}