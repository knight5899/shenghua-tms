<?php


namespace app\index\controller;

use app\common\help\Contents;
use app\common\help\Help;
use think\Request;

class Financetmsprice extends Base
{
	public function tmsprice(){
    //读取所有的项目

    $params = Request::instance()->param();

	
    $projectResult = $this->callSoaErp('post', '/source/getProject', $data);

    $this->assign('projectResult', $projectResult['data']);

    $data = [
        'page' => $this->page(),
        'page_size' => $this->_page_size,


    ];
    if (is_numeric(input('project_id'))) {

        $data['project_id'] = input('project_id');
    }
    $status = input('status');
    if (is_numeric($status)) {
        $data['status'] = $status;
    }
    if (!empty($params['accept_name'])) {
        $data['accept_name'] = $params['accept_name'];
    }
    if (!empty($params['accept_cellphone'])) {
        $data['accept_cellphone'] = $params['accept_cellphone'];
    }


    $result = $this->callSoaErp('post', '/order/getOrder', $data);


    $result = Help::getOrder($result);

    $this->getPageParams($result);
    return $this->fetch('tmsprice');

}

	//待开票
	public function pricereceipt(){
		
		$projectData =[];
		$projectResult = $this->callSoaErp('post', '/source/getProject', $projectData);

		$this->assign('projectResult',$projectResult['data']);

		return $this->fetch('pricereceipt');
		
	}
	
	//已开票
	public function invoiced(){
		
		$projectData =[];
		$projectResult = $this->callSoaErp('post', '/source/getProject', $projectData);

		$this->assign('projectResult',$projectResult['data']);

		return $this->fetch('invoiced');
		
	}
	//已送出
	public function sendout(){
		
		$projectData =[];
		$projectResult = $this->callSoaErp('post', '/source/getProject', $projectData);

		$this->assign('projectResult',$projectResult['data']);

		return $this->fetch('sendout');
		
	}	
	//已收款
	public function received(){
		
		$projectData =[];
		$projectResult = $this->callSoaErp('post', '/source/getProject', $projectData);

		$this->assign('projectResult',$projectResult['data']);

		return $this->fetch('received');
		
	}	
	//收入开票
	public function getPricereceiptAjax(){
		$params = Request::instance()->param();
		
		
		$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
		];
		
		if(is_numeric($params['project_id'])){
			$data['project_id'] = $params['project_id'];
		}
		if(is_numeric($params['customer_invoice_status'])){
			$data['customer_invoice_status'] = $params['customer_invoice_status'];
		}
				
		
		if(!empty($params['customer_bill_number'])){
			$data['customer_bill_number'] = $params['customer_bill_number'];
		}	
		if(!empty($params['customer_bill_status'])){
			$data['customer_bill_status'] = $params['customer_bill_status'];
		}
		
    	if(!empty($params['customer_bill_date'])){
    		$data['customer_bill_date'] = $params['customer_bill_date'];
    	}		
		$data['status'] = 1;

		
		$result = $this->callSoaErp('post', '/bill/getCustomerBillInfo', $data);
	
		for($i=0;$i<count($result['data']['list']);$i++){
			$result['data']['list'][$i]['create_time'] = date('Y-m-d',$result['data']['list'][$i]['create_time']);
			if($result['data']['list'][$i]['invoice_type']==1){
				$invoice_type='增值税专票';
				
			}elseif($result['data']['list'][$i]['invoice_type']==2){
				$invoice_type='增值税普票';
			}else{
				$invoice_type='';
			}
			
			if(is_numeric($result['data']['list'][$i]['create_invoice_time'])){
				$result['data']['list'][$i]['create_invoice_time']=date('Y-m-d',$result['data']['list'][$i]['create_invoice_time']);
			}else{
				$result['data']['list'][$i]['create_invoice_time']='';
			}
			if(is_numeric($result['data']['list'][$i]['create_over_time'])){
				$result['data']['list'][$i]['create_over_time']=date('Y-m-d',$result['data']['list'][$i]['create_over_time']);
			}else{
				$result['data']['list'][$i]['create_over_time']='';
			}
			if(is_numeric($result['data']['list'][$i]['back_time'])){
				$result['data']['list'][$i]['back_time']=date('Y-m-d',$result['data']['list'][$i]['back_time']);
			}else{
				$result['data']['list'][$i]['back_time']='';
			}	
			if($result['data']['list'][$i]['customer_bill_tax']==999999){
				$result['data']['list'][$i]['customer_bill_tax']='不开票';
			}			
			if($result['data']['list'][$i]['customer_bill_tax_type']==1){
				$result['data']['list'][$i]['customer_bill_tax_type_name']='否';
			}else if($result['data']['list'][$i]['customer_bill_tax_type']==2){
				$result['data']['list'][$i]['customer_bill_tax_type_name']='是';
			}else{
				$result['data']['list'][$i]['customer_bill_tax_type_name']='';
			}
			if($result['data']['list'][$i]['customer_invoice_status']==1){
				$result['data']['list'][$i]['customer_invoice_status_name']='未开票';
			}else if($result['data']['list'][$i]['customer_invoice_status']==2){
				$result['data']['list'][$i]['customer_invoice_status_name']='已开票';
			}else if($result['data']['list'][$i]['customer_invoice_status']==3){
				$result['data']['list'][$i]['customer_invoice_status_name']='已送出';
			}else{
				$result['data']['list'][$i]['customer_invoice_status_name']='不开票';
			}
			if($result['data']['list'][$i]['pay_type']==1){
				$result['data']['list'][$i]['pay_type']='现付';
			}else if($result['data']['list'][$i]['pay_type']==2){
				$result['data']['list'][$i]['pay_type']='到付';
			}else{
				$result['data']['list'][$i]['pay_type']='月结';
			}
			
			$result['data']['list'][$i]['customer_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['customer_bill_status']];

		}
	

		
 		$r = [
			'code'=>200,
			'data'=>$result['data']['list'],
			'count'=>	$result['data']['count'],

		];	
 		return $r;
		
	}
	
	//修改显示页面
	public function updatepricereceipt(){
		$params = Request::instance()->param();
		
	
		$data['customer_bill_id'] = $params['customer_bill_id'];

		$result = $this->callSoaErp('post', '/bill/getCustomerBillInfo', $data);
	
	

		$this->assign('result',$result['data'][0]);
		return $this->fetch('update_pricereceipt');
		
	}
	//显示关账页面
	public function updatepricereceiptGz(){
		$params = Request::instance()->param();
		
	
		$data['customer_bill_id'] = $params['customer_bill_id'];

		$result = $this->callSoaErp('post', '/bill/getCustomerBillInfo', $data);
	
	

		$this->assign('result',$result['data'][0]);
		return $this->fetch('update_pricereceipt_gz');
		
	}	
	public function updatepricereceiptAjax(){
		$params = Request::instance()->param();

		
		if($params['is_back']==2){
			$data['customer_bill_id'] = $params['customer_bill_id'];
			$data['back_remark'] = $params['back_remark'];
			$data['customer_bill_status'] =3;
			
			
		}else{
			if($params['customer_bill_status'] == 4){
				
				if(!empty($params['create_over_time'])){
					$data['create_over_time'] = strtotime($params['create_over_time']);
				}
				$data['customer_bill_id'] = $params['customer_bill_id'];
				$data['customer_bill_status'] =4;
			}else if($params['customer_invoice_status']==2){
				$data['customer_bill_id'] = $params['customer_bill_id'];

				$data['customer_invoice_name'] = session('user')['nickname'];
				$data['customer_invoice_status'] =2;				
			}			
			else{
				$data['customer_bill_id'] = $params['customer_bill_id'];
				$data['customer_bill_tax'] = $params['customer_bill_tax'];	
					
				$data['customer_bill_status'] =2;
				$data['customer_bill_number'] =$params['customer_bill_number'];
			}
	
		}
		
		
		$result = $this->callSoaErp('post', '/bill/updateCustomerBill', $data);

	

		return $result;
	}
}