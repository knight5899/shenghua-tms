<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Request;

class Wmsmonitor extends Base
{
    /**
     *  圣华
     *  温湿度点位显示页面
     */
    public function showTHPoint(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);
        $result = $this->callSoaErp('post','/Wmsmonitor/getTHPoint',$data_all);
        $this->assign('thpointResult',$result['data']['list']);
        $this->getPageParams($result);
        //        var_dump($result);exit;

        return $this->fetch('th_point_manage');
    }

    /**
     *  圣华
     *  温湿度点位新增页面
     */
    public function showTHPointAdd(){

        //获取货区信息
        $data = [
            'status'=>1
        ];
        $th_point_result = $this->callSoaErp('post', '/position/getPosition',$data);
        $this->assign("positionResult",$th_point_result['data']);

        $input = input('wms_th_point_uuid');
        if(!empty($input)) {
            $data2 = [
                'wms_th_point_uuid' => $input,
            ];
            //获取所有温湿度点位
            $thpoint_result = $this->callSoaErp('post', '/Wmsmonitor/getTHPoint', $data2);
            $this->assign("thpointResult", $thpoint_result['data'][0]);
        }

        return $this->fetch('th_point_add');
    }

    /**
     * 圣华
     * 温湿度点位新增AJAX / 修改AJAX
     */
    public function THPointAddAjax(Request $request)
    {
        $data = $request->param();

        if(!empty($data['wms_th_point_uuid'])){
            //修改温湿度点位
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/Wmsmonitor/updateTHPointByTHPointId', $data);
            return $result;
        }else{
            //新增温湿度点位
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/Wmsmonitor/addTHPoint', $data);
            return $result; //['code' => '400', 'msg' => $data];
        }
    }

    /**
     *  圣华
     *  温湿度数据显示页面
     */
    public function showTH(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/Wmsmonitor/getTH',$data_all);
        $this->assign('thResult',$result['data']['list']);
        $this->getPageParams($result);

        return $this->fetch('th_manage');
    }

    /**
     *  圣华
     *  温湿度数据新增页面
     */
    public function showTHAdd(){
        return $this->fetch('th_add');
    }

    /**
     * 圣华
     * 温湿度数据新增AJAX / 修改AJAX
     */
    public function THAddAjax(Request $request){

    }

    /**
     * 圣华
     * 温湿度弹框页面
     */
    public function showTHOpen(){
        return $this->fetch('th_open_add');
    }

    public function showTHVisualization(){
        return $this->fetch('th_visualization_manage');
    }
}