<?php

namespace app\index\controller;

class Turnoverbox extends Base
{
    /**
     *  圣华
     *  智能周转箱
     */
    public function index(){
        return $this->fetch('index');
    }

    /**
     *  圣华
     *  智能周转箱地图-demo
     */
    public function turnoverboxMap(){
        return $this->fetch('show_info');
    }

    /**
     *  圣华
     *  智能周转箱地图-实时
     */
    public function turnoverboxMapNow(){
        return $this->fetch('show_info_now');
    }

    /**
     *  圣华
     *  获取智能周转箱地图-实时AJAX
     */
    public function getTurnoverBoxNowAjax(){

        $data = [
            "status"=>1
        ];
        $result = $this->callSoaErp('post','/turnoverbox/getTurnoverbox',$data);

        //组装经纬度
        if($result){
            foreach($result['data'] as $key=>$val){
                 $new = explode(",", $val['bd_location']);
                 $bd_jingdu = $new[0];
                 $bd_weidu =  $new[1];
                 $result['data'][$key]['bd_jingdu'] = $bd_jingdu;
                 $result['data'][$key]['bg_weidu'] = $bd_weidu;
            }
        }

        return $result;
    }

}