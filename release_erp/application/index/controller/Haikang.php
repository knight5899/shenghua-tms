<?php

namespace app\index\controller;
use app\common\help\Contents;
use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
class Haikang extends Base
{

	public $pre_url ;
	protected $app_key = "22457463";
	protected $app_secret = "U2436c1wR4iIUY80taMM";
	public $time ;//时间戳
	public $content_type="application/json";//类型
	public $accept="*/*" ;//accept
	
	public $person_list_url = "/artemis/api/resource/v1/person/personList";//获取人员列表
	public $person_list_url_v2 = '/artemis/api/resource/v2/person/personList';//获取人员列表v2
	public $get_door_list = "/artemis/api/resource/v1/acsDoor/acsDoorList";//获取门禁列表
	public $get_door_list_v2 = '/artemis/api/resource/v2/door/search';//查询门禁点列表v2
	public $get_door_event = '/artemis/api/acs/v1/door/events';//查询门禁点事件
	public $get_door_event_v2 = '/artemis/api/acs/v2/door/events';//查询门禁点事件v2
	
	public $door_control_url = '/artemis/api/acs/v1/door/doControl';//门禁点反控	
	public $get_door_status = '/artemis/api/nms/v1/online/door/get';//获取门禁设备在线状态
	public $park_list ='/artemis/api/resource/v1/park/parkList';//获取停车库列表
	
	public $entrance_list = '/artemis/api/resource/v1/entrance/entranceList';//获取出入库列表
	public $roadway_list = '/artemis/api/resource/v1/roadway/roadwayList';//获取车道列表
	
	public $device_control = '/artemis/api/pms/v1/deviceControl';//根据车道编码反控道闸
	public $cross_records = '/artemis/api/pms/v1/crossRecords/page';//查询过车记录
	public $parking_space =  '/artemis/api/pms/v1/parkingSpace/spaceNo';//查询车位信息
	public function __construct()
	{
		$this->pre_url = Contents::hikApiUrl();
		if($app_key!='') $this->app_key = $app_key;
		if($app_secret!='') $this->app_secret = $app_secret;
		$this->charset = 'utf-8';
		list($msec, $sec) = explode(' ', microtime());
		$this->time = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);		
		
		
	}
	/**
	 * 获取人员列表
	 */
	public function get_person_list(){
		//请求参数
		$postData['pageNo'] =1;
		$postData['pageSize'] = 1000;
	
		$sign = $this->get_sign($postData,$this->person_list_url);

		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
				)
		);
		$result = $this->curlPost($this->pre_url.$this->person_list_url,json_encode($postData),$options);
	
		return json_decode($result,true);
	}
	
	/**
	 * 查询人员列表v2
	 */
	public function person_list_v2($postData){
	
		//接口地址
		$url = $this->person_list_url_v2;
		$sign =$this->get_sign($postData,$url);
	
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
	
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
	
		return json_decode($result,true);
	
	}
	/**
	 * 以appSecret为密钥，使用HmacSHA256算法对签名字符串生成消息摘要，对消息摘要使用BASE64算法生成签名（签名过程中的编码方式全为UTF-8）
	 */
	function get_sign($postData,$url){
		$sign_str = $this->get_sign_str($postData,$url); //签名字符串
		$priKey=$this->app_secret;
		$sign = hash_hmac('sha256', $sign_str, $priKey,true); //生成消息摘要
		$result = base64_encode($sign);
		return $result;
	}
	
	function get_sign_str($postData,$url){
		// $next = "\n";
		$next = "\n";
		$str = "POST".$next.$this->accept.$next.$this->content_type.$next;
		$str .= "x-ca-key:".$this->app_key.$next;
		$str .= "x-ca-timestamp:".$this->time.$next;
		$str .= $url;
		return $str;
	}
	
	public function getSignContent($params) {
		ksort($params);
		$stringToBeSigned = "";
		$i = 0;$len = count($params);
		foreach ($params as $k => $v) {
			if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {
				// 转换成目标字符集
				$v = $this->characet($v, $this->charset);
				if ($i == 0) {
					$stringToBeSigned .= "?$k" . "=" . "$v";
				}else {
					$stringToBeSigned .= "&" . "$k" . "=" . "$v";
				}
				$i++;
			}
		}
		unset ($k, $v);
		return $stringToBeSigned;
	}
	
	function get_message($postData){
		$str = str_replace(array('{','}','"'),'',json_encode($postData));
		return base64_encode(md5($str));
	}
	/**
	 * 校验$value是否非空
	 *  if not set ,return true;
	 *    if is null , return true;
	 **/
	protected function checkEmpty($value) {
		if (!isset($value))
			return true;
		if ($value === null)
			return true;
		if (trim($value) === "")
			return true;
		return false;
	}
	
	/**
	 * 转换字符集编码
	 * @param $data
	 * @param $targetCharset
	 * @return string
	 */
	function characet($data, $targetCharset) {
		if (!empty($data)) {
			$fileType = $this->charset;
			if (strcasecmp($fileType, $targetCharset) != 0) {
				$data = mb_convert_encoding($data, $targetCharset, $fileType);
			}
		}
		return $data;
	}
	
	public function curlPost($url = '', $postData = '', $options = array())
	{
		if (is_array($postData)) {
			$postData = http_build_query($postData);
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置cURL允许执行的最长秒数
		if (!empty($options)) {
			curl_setopt_array($ch, $options);
		}
		//https请求 不验证证书和host
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}	
	/**
	 * 获取门禁列表
	 */
	function get_door_list($postData){


		//请求参数
	//	$postData['pageNo'] =1;
	//	$postData['pageSize'] = 100;
		//接口地址
		$url = $this->get_door_list;
		$sign =$this->get_sign($postData,$url);
		
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",

				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
	
		return json_decode($result,true);
	}	
	/**
	 * 获取门禁列表V2
	 */
	function get_door_list_v2($postData){
	
	
		//请求参数
		//	$postData['pageNo'] =1;
		//	$postData['pageSize'] = 100;
		//接口地址
		$url = $this->get_door_list_v2;
		$sign =$this->get_sign($postData,$url);
	
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
	
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
		
		return json_decode($result,true);
	}
	//查询门禁点事件
	public function  get_door_event($postData){
	

		//接口地址
		$url = $this->get_door_event;
		$sign =$this->get_sign($postData,$url);
	
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
	
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
	
		return json_decode($result,true);
	}	
	
	//查询门禁点事件
	public function  get_door_event_v2($postData){
	
	
		//接口地址
		$url = $this->get_door_event_v2;
		$sign =$this->get_sign($postData,$url);
	
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
	
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
	
		return json_decode($result,true);
	}	
	/**
	 * 门禁点反控
	 */
	public function door_control($postData){
		$url=$this->door_control_url;
		
		$sign = $this->get_sign($postData,$url);
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
		return json_decode($result,true);
	}	
	
	/**
	 * 获取门禁设备在线状态
	 */
	public function get_door_status(){
		$postData = [];
		$url = $this->get_door_status;
		$sign = $this->get_sign($postData,$url);
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
		dump($result);
		//return json_decode($result,true);		
	}
	/**
	 * 获取停车库列表
	 */
	public function  park_list($postData){
	

		//接口地址
		$url = $this->park_list;
		$sign =$this->get_sign($postData,$url);
	
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
	
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
	
		return json_decode($result,true);
	}
	
	/**
	 * 获取出入口列表
	 */
	public function entrance_list($postData){
		//接口地址
		$url = $this->entrance_list;
		$sign =$this->get_sign($postData,$url);
		
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
		
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
		
		return json_decode($result,true);		
		
	}
	
	/**
	 * 获取车道列表
	 */
	public function roadway_list($postData){

		//接口地址
		$url = $this->roadway_list;
		$sign =$this->get_sign($postData,$url);
		
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
		
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);

		return json_decode($result,true);		
		
	}
	/**
	 * 根据车道反控道闸$device_control
	 */
	public function device_control($postData){
		
		$url = $this->device_control;
		$sign =$this->get_sign($postData,$url);
		
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
		
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);		
		return json_decode($result,true);				
	}
	
	/**
	 * 查询过车记录
	 */
	public function cross_records($postData){
		
		//接口地址
		$url = $this->cross_records;
		$sign =$this->get_sign($postData,$url);
		
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
		
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
		
		return json_decode($result,true);		
		
	}
	
	/**
	 * 查询车位信息
	 */
	public function parking_space($postData){
		
		//接口地址
		$url = $this->parking_space;
		$sign =$this->get_sign($postData,$url);
		
		$options = array(
				CURLOPT_HTTPHEADER => array(
						"Accept:".$this->accept,
						"Content-Type:".$this->content_type,
						"x-Ca-Key:".$this->app_key,
						"X-Ca-Signature:".$sign,
						"X-Ca-Timestamp:".$this->time,
						"X-Ca-Signature-Headers:"."x-ca-key,x-ca-timestamp",
		
				)
		);
		$result = $this->curlPost($this->pre_url.$url,json_encode($postData),$options);
		
		return json_decode($result,true);		
	}
	
	
	
}

