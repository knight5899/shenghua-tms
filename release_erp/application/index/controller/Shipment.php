<?php
/**
 * Created by PhpStorm.
 * User: 胡
 * Date: 2019/02/19
 * Time: 16:40
 */

namespace app\index\controller;

use app\common\help\Contents;
use think\Validate;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use think\Cache;
use app\common\help\Help;

class Shipment extends Base
{

	public function showLineManage(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
		$projectResult= $this->callSoaErp('post', '/source/getProject',[]);
		

		$this->assign('projectResult',$projectResult['data']);
		return $this->fetch('line_manage');
	}
	
	public function showLineAjax(){

		$params = Request::instance()->param();
		
	
		$data = [
				'page' => $params['page'],
				'page_size' => $params['limit'],
				//'orders_id_is_like'=>1,
				'status'=>1
		
		];
		if(!empty($params['orders_number'])){
			$data['orders_number'] =$params['orders_number'];
		}
		if(!empty($params['start_pickup_time'])){
			$data['start_pickup_time'] =strtotime($params['start_pickup_time']);
		}
		if(!empty($params['end_pickup_time'])){
			$data['end_pickup_time'] =strtotime($params['end_pickup_time']);
		}
        if(!empty($params['send_goods_company'])){
            $data['send_goods_company'] =$params['send_goods_company'];
        }
        if(!empty($params['accept_location_name'])){
            $data['accept_location_name'] =$params['accept_location_name'];
        }
        if(!empty($params['accept_goods_company'])){
            $data['accept_goods_company'] =$params['accept_goods_company'];
        }
        if(!empty($params['project_id'])){
            $data['project_id'] =$params['project_id'];
        }
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}
	
		$data['shipment_status'] = 0;
			
		$result = $this->callSoaErp('post', '/order/getShipmentNeedOrder', $data);		
	

		$result = Help::getOrder($result);
	
		for($i=0;$i<count($result['data']['list']);$i++){
			$list = $result['data']['list'];
			if(is_numeric($result['data']['list'][$i]['pickup_time'])){
				$result['data']['list'][$i]['pickup_time'] = date('Y-m-d',$result['data']['list'][$i]['pickup_time']);
			}else{
				$result['data']['list'][$i]['pickup_time']='';
			}
			if(is_numeric($result['data']['list'][$i]['send_time'])){
				$result['data']['list'][$i]['send_time'] = date('Y-m-d',$result['data']['list'][$i]['send_time']);
			}else{
				$result['data']['list'][$i]['send_time']='';
			}			
			if($result['data']['list'][$i]['bargain_type']==1){
				$result['data']['list'][$i]['bargain_type'] = '合同';
			}else{
				$result['data']['list'][$i]['bargain_type']='单票';
			}
			$result['data']['list'][$i]['send_all_address']=$list[$i]['send_province_name'].$list[$i]['send_city_name'].$list[$i]['send_area_name'].$list[$i]['send_address'];
			$result['data']['list'][$i]['accept_all_address']=$list[$i]['accept_province_name'].$list[$i]['accept_city_name'].$list[$i]['accept_area_name'].$list[$i]['accept_address'];
			$estimated_count=0;
			$estimated_pack_count=0;
			$estimated_weight=0;
			$estimated_volume=0;
			for($j=0;$j<count($list[$i]['orders_goods_info']);$j++){
				
			
				$estimated_count+=$list[$i]['orders_goods_info'][$j]['estimated_count'];
				$estimated_pack_count+=$list[$i]['orders_goods_info'][$j]['estimated_pack_count'];
				$result['data']['list'][$i]['estimated_pack_unit']=Contents::baseConfig()['order']['goods_pack'][$list[$i]['orders_goods_info'][$j]['estimated_pack_unit']];
				$estimated_weight+=$list[$i]['orders_goods_info'][$j]['estimated_weight'];
				$estimated_volume+=$list[$i]['orders_goods_info'][$j]['estimated_volume'];
				if($j==0){
					$result['data']['list'][$i]['goods_name']=$list[$i]['orders_goods_info'][$j]['goods_name'];
				}else{
					$result['data']['list'][$i]['goods_name'].='/'.$list[$i]['orders_goods_info'][$j]['goods_name'];
						
				}
				
			}
			$result['data']['list'][$i]['estimated_count'] = $estimated_count;
			$result['data']['list'][$i]['estimated_pack_count'] = $estimated_pack_count;
			$result['data']['list'][$i]['estimated_weight'] = $estimated_weight;
			$result['data']['list'][$i]['estimated_volume'] = $estimated_volume;
		}

		$r = [
			'code'=>200,
			'data'=>$result['data']['list'],
			'count'=>	$result['data']['count'],

		];

        if($params['export']==1){ //导出参数
            return $result;
        }else{
            return $r;
        }

	}
	//删除发运
	public function delShipment(){
		$params = Request::instance()->param();		
		$result = $this->callSoaErp('post', '/shipment/delShipment',$params);
		
	
		return $result;
		
	}
	//删除短驳
	public function delShortBarge(){
		$params = Request::instance()->param();		
		$result = $this->callSoaErp('post', '/shipment/delShortBarge',$params);
		return $result;
		
	}	
	//发运完成
	public function showLineOverManage(){
		$params = Request::instance()->param();
        //分公司
        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }
		
		$this->assign('comapnyResult', $comapnyResult);
	
		//获取项目
		$projectResult= $this->callSoaErp('post', '/source/getProject',[]);
		

		$this->assign('projectResult',$projectResult['data']);

		return $this->fetch('line_over_manage');
	}
	//发运完成
	public function getLineOverAjax(){
		$params = Request::instance()->param();
		$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
			'status'=>1

		];
	
		if(is_numeric($params['project_id'])){
			$data['project_id'] = $params['project_id'];
		}
		if(!empty($params['orders_number'])){
			$data['orders_number'] = $params['orders_number'];
		}
		if(!empty($params['finance_number'])){
			$data['finance_number'] = $params['finance_number'];
		}
		if(!empty($params['start_shipment_time'])){
			$data['start_shipment_time'] = strtotime($params['start_shipment_time']);
		}
		if(!empty($params['end_shipment_time'])){
			$data['end_shipment_time'] = strtotime($params['end_shipment_time']);
		}
		if(!empty($params['start_pickup_time'])){
			$data['start_pickup_time'] = strtotime($params['start_pickup_time']);
		}
		if(!empty($params['end_pickup_time'])){
			$data['end_pickup_time'] = strtotime($params['end_pickup_time']);
		}		
        if(!empty($params['send_goods_company'])){
            $data['send_goods_company'] = $params['send_goods_company'];
        }
        if(!empty($params['accept_goods_company'])){
            $data['accept_goods_company'] = $params['accept_goods_company'];
        }
        if(!empty($params['send_location_name'])){
            $data['send_location_name'] = $params['send_location_name'];
        }
        if(!empty($params['accept_location_name'])){
            $data['accept_location_name'] = $params['accept_location_name'];
        }
        if(!empty($params['supplier_name'])){
            $data['supplier_name'] = $params['supplier_name'];
        }
        if(!empty($params['supplier_shipment_number'])){
            $data['supplier_shipment_number'] = $params['supplier_shipment_number'];
        }

		
		
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}		
		$result = $this->callSoaErp('post', '/shipment/getShipment', $data);
	

	
		$count  = $result['data']['count'];
		$data = $result['data']['list'];


		for($i=0;$i<count($data);$i++){
			if(is_numeric($data[$i]['shipment_time'])){
				$data[$i]['shipment_time'] = date('Y-m-d',$data[$i]['shipment_time']);
			}else{
				$data[$i]['shipment_time']='';
			}
			if(is_numeric($data[$i]['need_time'])){
				$data[$i]['need_time'] = date('Y-m-d',$data[$i]['need_time']);
			}else{
				$data[$i]['need_time']='';
			}
			if(is_numeric($data[$i]['pickup_time'])){
				$data[$i]['pickup_time'] = date('Y-m-d',$data[$i]['pickup_time']);
			}
			
			
			if($data[$i]['is_short_barge'] >= 1){
				$data[$i]['is_short_barge'] ='是';
			}else{
				$data[$i]['is_short_barge'] ='否';
			}
			if($data[$i]['is_print'] == 0){
				$data[$i]['is_print'] ='未打印';
			}else{
				$data[$i]['is_print'] ='已打印';
			}
			$data[$i]['accept_all_address']=$data[$i]['accept_province_name'].$data[$i]['accept_city_name'].$data[$i]['accept_area_name'].$data[$i]['accept_address'];
			$data[$i]['shipment_finance_name']=Contents::baseConfig()['shipment']['shipment_finance_status'][$data[$i]['shipment_finance_status']];	
			$shipment_count=0;
			$shipment_pack_count=0;
			$shipment_weight=0;
			$shipment_volume=0;
			for($j=0;$j<count($data[$i]['shipment_goods']);$j++){


				$shipment_count+=$data[$i]['shipment_goods'][$j]['shipment_count'];
				$shipment_pack_count+=$data[$i]['shipment_goods'][$j]['shipment_pack_count'];
				$shipment_weight+=$data[$i]['shipment_goods'][$j]['shipment_weight'];
				$shipment_volume+=$data[$i]['shipment_goods'][$j]['shipment_volume'];
				if($j==0){
					$data[$i]['goods_name']=$data[$i]['shipment_goods'][$j]['goods_name'];
				}else{
					$data[$i]['goods_name'].='/'.$data[$i]['shipment_goods'][$j]['goods_name'];

				}

			}
			$yunfei=0;
			$songhuofei=0;
			$xiehuofei=0;
			$qibufei=0;
			$tihuofei=0;
			$qitafei=0;;
			for($k=0;$k<count($data[$i]['shipment_cost']);$k++){
				if($data[$i]['shipment_cost'][$k]['cost_id'] ==5){
					$yunfei=$data[$i]['shipment_cost'][$k]['cost_money'];
				}else if($data[$i]['shipment_cost'][$k]['cost_id'] ==6){
					$tihuofei=$data[$i]['shipment_cost'][$k]['cost_money'];
				}else if($data[$i]['shipment_cost'][$k]['cost_id'] ==7){
					$songhuofei=$data[$i]['shipment_cost'][$k]['cost_money'];
				}else if($data[$i]['shipment_cost'][$k]['cost_id'] ==8){
					$xiehuofei=$data[$i]['shipment_cost'][$k]['cost_money'];
				}else if($data[$i]['shipment_cost'][$k]['cost_id'] ==9){
					$qibufei=$data[$i]['shipment_cost'][$k]['cost_money'];
				}else if($data[$i]['shipment_cost'][$k]['cost_id'] ==10){
					$qitafei=$data[$i]['shipment_cost'][$k]['cost_money'];
				}
			}
			$data[$i]['yunfei'] = $yunfei;
			$data[$i]['songhuofei'] = $songhuofei;
			$data[$i]['xiehuofei'] = $xiehuofei;
			$data[$i]['qibufei'] = $qibufei;
			$data[$i]['tihuofei'] = $tihuofei;
			$data[$i]['qitafei'] = $qitafei;			
			$data[$i]['shipment_count'] = $shipment_count;
			$data[$i]['shipment_pack_unit'] = $shipment_pack_count;
			$data[$i]['shipment_weight'] = $shipment_weight;
			$data[$i]['shipment_volume'] = $shipment_volume;
		}

		$r = [
			'code'=>200,
			'data'=>$data,
			'count'=>$count

		];

        if($params['export']==1){ //导出参数
            return $result;
        }else{
            return $r;
        }
	}


    public function addLine()
    {
        $params = Request::instance()->param();
        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }
		$this->assign('comapnyResult', $comapnyResult);

		//获取成本费用名称
		$cost_params['cost_type'] =2;
		$cost_params['status'] =1;
		$costResult = $this->callSoaErp('post', '/system/getCost', $cost_params);


		$this->assign("costResult",$costResult['data']);

        $data['orders_number'] =$params['orders_number'];
        $result = $this->callSoaErp('post', '/order/getShipmentNeedOrder', $data);
	
	
        $result = $result['data'][0];
	
		//获取承运商数据
		$supplierParams['status'] = 1;
		$supplierParams['send_location_id'] = $result['send_location_id'];
		$supplierParams['accept_location_id'] = $result['accept_location_id'];
		
		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);
	
		
	
	
		$this->assign('send_location_id',$result['send_location_id']);
		$this->assign('accept_location_id',$result['accept_location_id']);

		$this->assign("supplierResult",$supplierResult['data']);
		
		$this->assign('company_id',session('user')['company_id']);
		
		
		
        //如果有数据
        if(!empty($params['shipment_uuid'])){
        	$shipmentParams['shipment_uuid'] = $params['shipment_uuid'];

        	$shipmentResult = $this->callSoaErp('post', '/shipment/getShipment', $shipmentParams);
        	$shipmentResult= $shipmentResult['data'][0];
			
			$this->assign('company_id',$shipmentResult['company_id']);
        


        	$this->assign("shipmentResult",$shipmentResult);

        	//获得供应商后开始查询线路
        	$supplierLineParams['status'] = 1;
        	$supplierLineParams['supplier_uuid'] =$shipmentResult['supplier_uuid'];
        	$supplierLineResult = $this->callSoaErp('post', '/source/getSupplierLine', $supplierLineParams);
        	$this->assign("supplierLineResult",$supplierLineResult['data']);

        	//查询线路详情

        	$shipmentSupplierLineParams['supplier_line_uuid'] =$shipmentResult['supplier_line_uuid'];
        	$shipmentSupplierLineResult = $this->callSoaErp('post', '/source/getSupplierLine', $shipmentSupplierLineParams);
        	//dump($shipmentSupplierLineResult);
        	//exit();
        	$this->assign("shipmentSupplierLineResult",$shipmentSupplierLineResult['data'][0]);


         		$shipmentGoods = $shipmentResult['shipment_goods'];
        	for($i=0;$i<count($result['orders_goods_info']);$i++){
				for($j=0;$j<count($shipmentGoods);$j++){
					if($result['orders_goods_info'][$i]['goods_id'] == $shipmentGoods[$j]['goods_id']){
						$result['orders_goods_info'][$i]['shipment_count'] = $shipmentGoods[$j]['shipment_count'];
						$result['orders_goods_info'][$i]['shipment_pack_count'] = $shipmentGoods[$j]['shipment_pack_count'];
						$result['orders_goods_info'][$i]['shipment_weight'] = $shipmentGoods[$j]['shipment_weight'];
						$result['orders_goods_info'][$i]['shipment_volume'] = $shipmentGoods[$j]['shipment_volume'];
						$result['orders_goods_info'][$i]['shipment_count'] = $shipmentGoods[$j]['shipment_count'];
						$result['orders_goods_info'][$i]['shipment_charge_type'] = $shipmentGoods[$j]['shipment_charge_type'];
						$result['orders_goods_info'][$i]['unit_price'] = $shipmentGoods[$j]['unit_price'];
						$result['orders_goods_info'][$i]['shipment_money'] = $shipmentGoods[$j]['shipment_money'];
					}

				}
        	}

        }


		$this->assign("result",$result);

        return $this->fetch('add_line');
    }
    public function addLineEasy()
    {
        $params = Request::instance()->param();
		//获取承运商数据
		$supplierParams['status'] = 1;

		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $data);


		$this->assign("supplierResult",$supplierResult['data']);

		//获取成本费用名称
		$cost_params['cost_type'] =2;
		$cost_params['status'] =1;
		$costResult = $this->callSoaErp('post', '/system/getCost', $cost_params);


		$this->assign("costResult",$costResult['data']);

        $data['orders_number'] =$params['orders_number'];
        $result = $this->callSoaErp('post', '/order/getShipmentNeedOrder', $data);


        $result = $result['data'][0];
        //如果有数据
        if(!empty($params['shipment_uuid'])){
        	$shipmentParams['shipment_uuid'] = $params['shipment_uuid'];

        	$shipmentResult = $this->callSoaErp('post', '/shipment/getShipment', $shipmentParams);
        	$shipmentResult= $shipmentResult['data'][0];


        	//for(){}


        	$this->assign("shipmentResult",$shipmentResult);

        	//获得供应商后开始查询线路
        	$supplierLineParams['status'] = 1;
        	$supplierLineParams['supplier_uuid'] =$shipmentResult['supplier_uuid'];
        	$supplierLineResult = $this->callSoaErp('post', '/source/getSupplierLine', $supplierLineParams);
        	$this->assign("supplierLineResult",$supplierLineResult['data']);

        	//查询线路详情

        	$shipmentSupplierLineParams['supplier_line_uuid'] =$shipmentResult['supplier_line_uuid'];
        	$shipmentSupplierLineResult = $this->callSoaErp('post', '/source/getSupplierLine', $shipmentSupplierLineParams);
        	//dump($shipmentSupplierLineResult);
        	//exit();
        	$this->assign("shipmentSupplierLineResult",$shipmentSupplierLineResult['data'][0]);


         		$shipmentGoods = $shipmentResult['shipment_goods'];
        	for($i=0;$i<count($result['orders_goods_info']);$i++){
				for($j=0;$j<count($shipmentGoods);$j++){
					if($result['orders_goods_info'][$i]['goods_id'] == $shipmentGoods[$j]['goods_id']){
						$result['orders_goods_info'][$i]['shipment_count'] = $shipmentGoods[$j]['shipment_count'];
						$result['orders_goods_info'][$i]['shipment_pack_count'] = $shipmentGoods[$j]['shipment_pack_count'];
						$result['orders_goods_info'][$i]['shipment_weight'] = $shipmentGoods[$j]['shipment_weight'];
						$result['orders_goods_info'][$i]['shipment_volume'] = $shipmentGoods[$j]['shipment_volume'];
						$result['orders_goods_info'][$i]['shipment_count'] = $shipmentGoods[$j]['shipment_count'];
						$result['orders_goods_info'][$i]['shipment_charge_type'] = $shipmentGoods[$j]['shipment_charge_type'];
						$result['orders_goods_info'][$i]['unit_price'] = $shipmentGoods[$j]['unit_price'];
						$result['orders_goods_info'][$i]['shipment_money'] = $shipmentGoods[$j]['shipment_money'];
					}

				}
        	}

        }


		$this->assign("result",$result);

        return $this->fetch('add_line_easy');
    }	
	
	
	//获取发运详情
    public function getLineInfo()
    {
        $params = Request::instance()->param();
		//获取承运商数据
		$supplierParams['status'] = 1;

		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $data);


		$this->assign("supplierResult",$supplierResult['data']);

		//获取成本费用名称
		$cost_params['cost_type'] =2;
		$cost_params['status'] =1;
		$costResult = $this->callSoaErp('post', '/system/getCost', $cost_params);


		$this->assign("costResult",$costResult['data']);

        $data['orders_number'] =$params['orders_number'];
        $result = $this->callSoaErp('post', '/order/getShipmentNeedOrder', $data);


        $result = $result['data'][0];
        //如果有数据
        if(!empty($params['shipment_uuid'])){
        	$shipmentParams['shipment_uuid'] = $params['shipment_uuid'];

        	$shipmentResult = $this->callSoaErp('post', '/shipment/getShipment', $shipmentParams);
        	$shipmentResult= $shipmentResult['data'][0];


        	//for(){}


        	$this->assign("shipmentResult",$shipmentResult);

        	//获得供应商后开始查询线路
        	$supplierLineParams['status'] = 1;
        	$supplierLineParams['supplier_uuid'] =$shipmentResult['supplier_uuid'];
        	$supplierLineResult = $this->callSoaErp('post', '/source/getSupplierLine', $supplierLineParams);
        	$this->assign("supplierLineResult",$supplierLineResult['data']);

        	//查询线路详情

        	$shipmentSupplierLineParams['supplier_line_uuid'] =$shipmentResult['supplier_line_uuid'];
        	$shipmentSupplierLineResult = $this->callSoaErp('post', '/source/getSupplierLine', $shipmentSupplierLineParams);
        	//dump($shipmentSupplierLineResult);
        	//exit();
        	$this->assign("shipmentSupplierLineResult",$shipmentSupplierLineResult['data'][0]);


         		$shipmentGoods = $shipmentResult['shipment_goods'];
        	for($i=0;$i<count($result['orders_goods_info']);$i++){
				for($j=0;$j<count($shipmentGoods);$j++){
					if($result['orders_goods_info'][$i]['goods_id'] == $shipmentGoods[$j]['goods_id']){
						$result['orders_goods_info'][$i]['shipment_count'] = $shipmentGoods[$j]['shipment_count'];
						$result['orders_goods_info'][$i]['shipment_pack_count'] = $shipmentGoods[$j]['shipment_pack_count'];
						$result['orders_goods_info'][$i]['shipment_weight'] = $shipmentGoods[$j]['shipment_weight'];
						$result['orders_goods_info'][$i]['shipment_volume'] = $shipmentGoods[$j]['shipment_volume'];
						$result['orders_goods_info'][$i]['shipment_count'] = $shipmentGoods[$j]['shipment_count'];
						$result['orders_goods_info'][$i]['shipment_charge_type'] = $shipmentGoods[$j]['shipment_charge_type'];
						$result['orders_goods_info'][$i]['unit_price'] = $shipmentGoods[$j]['unit_price'];
						$result['orders_goods_info'][$i]['shipment_money'] = $shipmentGoods[$j]['shipment_money'];
					}

				}
        	}

        }



	//	dump($shipmentResult);exit();

		$this->assign("result",$result);

        return $this->fetch('line_info');
    }

    /**
     * 发运新增数据
     */
    public function addShipmentAjax(){
    	$data = Request::instance()->param();
	
    	$params = [];
    	$params['orders_number'] = $data['orders_number'];
    	if(!empty($data['shipment_time'])){
    		$params['shipment_time'] = strtotime($data['shipment_time']);
    	}
    	if(!empty($data['need_time'])){
    		$params['need_time'] = strtotime($data['need_time']);
    	}


    	$params['shipment_type'] = $data['shipment_type'];
    	$params['supplier_uuid'] = $data['supplier_uuid'];
    	$params['supplier_line_uuid'] = $data['supplier_line_uuid'];
    	$params['supplier_shipment_number'] = $data['supplier_shipment_number'];
    	$params['is_short_barge'] = $data['is_short_barge'];
    	//开始存放商品
    	$goods_id = $data['goods_id'];
    	$shipment_count = $data['shipment_count'];
    	$shipment_pack_unit = $data['shipment_pack_unit'];
    	$shipment_pack_count = $data['shipment_pack_count'];
    	$shipment_weight= $data['shipment_weight'];
    	$shipment_volume = $data['shipment_volume'];
    	$shipment_charge_type = $data['shipment_charge_type'];
		
    	$unit_price = $data['unit_price'];
    	$shipment_money = $data['shipment_money'];
    	$shipment_goods = [];
    	for($i=0;$i<count($shipment_charge_type);$i++){
    		$shipment_goods[$i]['goods_id'] = $goods_id[$i];
    		$shipment_goods[$i]['shipment_count'] = $shipment_count[$i];
    		$shipment_goods[$i]['shipment_pack_unit'] = $shipment_pack_unit[$i];
    		$shipment_goods[$i]['shipment_pack_count'] = $shipment_pack_count[$i];
    		$shipment_goods[$i]['shipment_weight'] = $shipment_weight[$i];
    		$shipment_goods[$i]['shipment_volume'] = $shipment_volume[$i];
    		$shipment_goods[$i]['shipment_charge_type'] = $shipment_charge_type[$i];
    		$shipment_goods[$i]['unit_price'] = $unit_price[$i];
    		$shipment_goods[$i]['shipment_money'] = $shipment_money[$i];
    	}
    	$params['shipment_goods'] = $shipment_goods;

    	//开始存放费用
    	$shipment_cost = [];

    	$cost_id = $data['cost_id'];
    	$cost_money = $data['cost_money'];
		
		
		


    	for($i=0;$i<count($cost_money);$i++){
    		if($cost_money[$i]!='' && $cost_money[$i]!=0){
    			$shipment_cost[$i]['cost_id'] = $cost_id[$i];
    			$shipment_cost[$i]['cost_money'] = $cost_money[$i];
    		}


    	}
		


    	$params['shipment_cost'] = array_values($shipment_cost);

    	//开始存放支付方式
    	$shipment_pay = [];

    	$pay_type = $data['pay_type'];
    	$pay_money = $data['pay_money'];
    	for($i=0;$i<count($pay_type);$i++){
    		$shipment_pay[$i]['pay_type'] = $pay_type[$i];
    		$shipment_pay[$i]['pay_money'] = $pay_money[$i];

    	}
    	$params['shipment_pay'] = $shipment_pay;



    	//最后的
    	$params['shipment_status'] = $data['shipment_status'];
    	$params['shipment_remark'] = $data['shipment_remark'];
    	$params['pay_all_money'] = $data['pay_all_money'];
    	$params['orders_id'] = $data['orders_id'];

    	$params['shipment_uuid'] = $data['shipment_uuid'];
		$params['choose_company_id'] = $data['choose_company_id'];
		$params['shipment_pay_type'] = $data['shipment_pay_type'];
	
    	if(!empty($params['shipment_uuid'])){


    		$result = $this->callSoaErp('post', '/shipment/updateShipment',$params);

    	}else{
			
    		$result = $this->callSoaErp('post', '/shipment/addShipment',$params);
		
    	}

	

    	return   $result;//['code' => '400', 'msg' => $data];
    }
 	//获取承运商线路报价
 	public function getSupplierPriceAjax(){
 		$params = Request::instance()->param();
 	//	$data['']$params['']shipment_charge_type
 		$supplierLineParams['supplier_line_uuid'] = $params['supplier_line_uuid'];

 		$supplierLineResult = $this->callSoaErp('post', '/source/getSupplierLine',$supplierLineParams);
 		$supplierLineResult = $supplierLineResult['data'][0];

 		$configParams['supplier_id'] = $supplierLineResult['supplier_id'];
 		$configParams['billing_unit'] = $params['shipment_charge_type'];
 		$configParams['departure_id'] = $supplierLineResult['start_location_id'];
 		$configParams['arrival_id'] = $supplierLineResult['end_location_id'];

 		$supplierLineResult = $this->callSoaErp('post', '/source/getPricingConfigureAndPrice',$configParams);

 		return $supplierLineResult['data'];
 	}

 	//获取短驳
 	public function showShortBargeManage(){

		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
 		$projectResult = $this->callSoaErp('post', '/source/getProject', []);

 		$this->assign('projectResult',$projectResult['data']);


 		return $this->fetch('short_barge_manage');
 	}
 	//获取短驳AJAX
 	public function getShortBargeAjax(){
 		$params = Request::instance()->param();

		
		
 		$data = [
				'page' => $params['page'],
				'page_size' => $params['limit'],
 				//'orders_id_is_like'=>1,
				'status'=>1


 		];
 		if(!empty($params['start_pickup_time'])){
 			$data['start_pickup_time'] = strtotime($params['start_pickup_time']);
 		}
 		if(!empty($params['end_pickup_time'])){
 			$data['end_pickup_time'] =strtotime($params['end_pickup_time']);
 		}
 		if(!empty($params['send_goods_company'])){
 			$data['send_goods_company'] =$params['send_goods_company'];
 		}		
 		if(is_numeric($params['project_id'])){
 			$data['project_id'] = $params['project_id'];
 		}
 		if(!empty($params['orders_number'])){
 			$data['orders_number'] = $params['orders_number'];
 		}
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}
 		$data['is_short_barge'] = 1;
 		$result = $this->callSoaErp('post', '/order/getShortBrageNeedOrder', $data);


		$count  = $result['data']['count'];
		$data = $result['data']['list'];

		
		for($i=0;$i<count($data);$i++){
			if(is_numeric($data[$i]['shipment_time'])){
				$data[$i]['shipment_time'] = date('Y-m-d',$data[$i]['shipment_time']);
			}else{
				$data[$i]['shipment_time']='';
			}
			if(is_numeric($data[$i]['pickup_time'])){
				$data[$i]['pickup_time'] = date('Y-m-d',$data[$i]['pickup_time']);
			}else{
				$data[$i]['pickup_time']='';
			}

			$data[$i]['send_all_address']=$data[$i]['send_province_name'].$data[$i]['send_city_name'].$data[$i]['send_area_name'].$data[$i]['send_address'];

			$data[$i]['accept_all_address']=$data[$i]['accept_province_name'].$data[$i]['accept_city_name'].$data[$i]['accept_area_name'].$data[$i]['accept_address'];
			$shipment_count=0;
			$shipment_pack_count=0;
			$shipment_weight=0;
			$shipment_volume=0;

			for($j=0;$j<count($data[$i]['orders_goods_info']);$j++){


				$shipment_count+=$data[$i]['orders_goods_info'][$j]['estimated_count'];
				$shipment_pack_count+=$data[$i]['orders_goods_info'][$j]['estimated_pack_count'];
				$shipment_weight+=$data[$i]['orders_goods_info'][$j]['estimated_weight'];
				$shipment_volume+=$data[$i]['orders_goods_info'][$j]['estimated_volume'];
				if($j==0){
					$data[$i]['goods_name']=$data[$i]['orders_goods_info'][$j]['goods_name'];
				}else{
					$data[$i]['goods_name'].='/'.$data[$i]['orders_goods_info'][$j]['goods_name'];

				}

			}
			$supplier_name='';
			for($k=0;$k<count($data[$i]['shipmentInfo']);$k++){
				$supplier_name.='/'.$data[$i]['shipmentInfo'][$k]['supplier_name'];


			}
			$data[$i]['supplier_name'] = trim($supplier_name,'/');
			$data[$i]['shipment_count'] = $shipment_count;
			$data[$i]['shipment_pack_unit'] = $shipment_pack_count;
			$data[$i]['shipment_weight'] = $shipment_weight;
			$data[$i]['shipment_volume'] = $shipment_volume;
		}

		$r = [
				'code'=>200,
				'data'=>$data,
				'count'=>$count

		];

        if($params['export']==1){ //导出参数
            return $result;
        }else{
            return $r;
        }
 	}

 	//添加短驳
 	public  function addShortBarge(){


 		//获取承运商
 		//获取承运商数据
 		$supplierParams['status'] = 1;

 		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $data);

 		$this->assign('supplierResult',$supplierResult['data']);
 		//开始获取运单
 		$params = Request::instance()->param();




 		$ordersNumber= $params['orders_number'];
 		$ordersNumber = explode(',',$ordersNumber);
 		$key ='';
 		for($i=0;$i<count($ordersNumber);$i++){
 			if($i==0){
 				$key="'".$ordersNumber[$i]."'";
 			}else{
 				$key.=",'".$ordersNumber[$i]."'";
 			}
 		}
 		$shortBargeParams['orders_number_array'] = $key;
 		$ordersResult = $this->callSoaErp('post', '/order/getShortBrageNeedOrder', $shortBargeParams);
 		$ordersResult = $ordersResult['data'];
 		for($i=0;$i<count($ordersResult);$i++){
 			if(is_numeric($ordersResult[$i]['pickup_time'])){
 				$ordersResult[$i]['pickup_time'] = date('Y-m-d',$ordersResult[$i]['pickup_time']);
 			}else{
 				$ordersResult[$i]['pickup_time']='';
 			}
 			$ordersGoodsInfo = $ordersResult[$i]['orders_goods_info'];
 			for($j=0;$j<count($ordersGoodsInfo);$j++){
 				if($j==0){
 					$goodsName =$ordersGoodsInfo[$j]['goods_name'];
 					$estimatedCount =$ordersGoodsInfo[$j]['estimated_count'];
 					$estimatedPackCount =$ordersGoodsInfo[$j]['estimated_pack_count'];
 					$estimatedPackUnit =Contents::baseConfig()['order']['goods_pack'][$ordersGoodsInfo[$j]['estimated_pack_unit']];

 					$estimatedWeight =$ordersGoodsInfo[$j]['estimated_weight'];
 					$estimatedVolume =$ordersGoodsInfo[$j]['estimated_volume'];
 				}else{
 					$goodsName.="<br />".$ordersGoodsInfo[$j]['goods_name'];
 					$estimatedCount+=$ordersGoodsInfo[$j]['estimated_count'];
 					$estimatedPackCount+=$ordersGoodsInfo[$j]['estimated_pack_count'];
 					$estimatedPackUnit.="<br />".Contents::baseConfig()['order']['goods_pack'][$ordersGoodsInfo[$j]['estimated_pack_unit']];
 					$estimatedWeight+=$ordersGoodsInfo[$j]['estimated_weight'];
 					$estimatedVolume+=$ordersGoodsInfo[$j]['estimated_volume'];
 				}



 			}
 			$ordersResult[$i]['goodsName'] = $goodsName;
 			$ordersResult[$i]['estimatedCount'] = $estimatedCount;
 			$ordersResult[$i]['estimatedPackCount'] = $estimatedPackCount;
 			$ordersResult[$i]['estimatedPackUnit'] = $estimatedPackUnit;
 			$ordersResult[$i]['estimatedWeight'] = $estimatedWeight;
 			$ordersResult[$i]['estimatedVolume'] = $estimatedVolume;

 			$shipmentInfo = $ordersResult[$i]['shipmentInfo'];
 			for($k=0;$k<count($shipmentInfo);$k++){
 				if($k==0){
 					$supplierName=$shipmentInfo[$k]['supplier_name'];
 				}else{
 					$supplierName.="<br />".$shipmentInfo[$k]['supplier_name'];
 				}
 			}
 			$ordersResult[$i]['supplierName']=$supplierName;
 		}
		$sort = array_column($ordersResult, 'supplierName');
        array_multisort($sort, SORT_DESC, $ordersResult);
 		//dump($ordersResult);exit();
 		$this->assign('ordersResult',$ordersResult);


 		return $this->fetch('update_short_barge');
 	}
 	//修改短驳
 	public  function updateShortBarge(){

 		//获取承运商
 		//获取承运商数据
 		$supplierParams['status'] = 1;

 		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $data);




 		$this->assign('supplierResult',$supplierResult['data']);
 		//获取未分配的承运商

 		$shortBargeParam['is_short_barge'] = 1;
 		$shortBargeResult = $this->callSoaErp('post', '/order/getShortBrageNeedOrder', $shortBargeParam);
 		$this->assign('shortBargeResult',$shortBargeResult['data']);

 		//开始获取运单
 		$params = Request::instance()->param();

 		$shipment_uuid = $params['shipment_uuid'];
 		$shipmentParams['shipment_uuid']=$shipment_uuid;
 		$shipmentResult = $this->callSoaErp('post', '/shipment/getShipmentShortBarge', $shipmentParams);

 		$shipmentResult = $shipmentResult['data'][0];
			
	
 		$this->assign('shipmentResult',$shipmentResult);

 		//通过承运商获取车辆
 		$vehicleParams['supplier_uuid'] = $shipmentResult['supplier_uuid'];
 		$vehicleResult = $this->callSoaErp('post', '/source/getVehicle',$vehicleParams);
 		$this->assign('vehicleResult',$vehicleResult['data']);

 		//
 		$nowVehicleParams['vehicle_id'] = $vehicleResult['data'][0]['vehicle_id'];
 		$nowVehicleResult = $this->callSoaErp('post', '/source/getVehicle',$nowVehicleParams);


 		$this->assign('nowVehicleResult',$nowVehicleResult['data'][0]);


 		$shipment_goods = $shipmentResult['shipment_goods'];
		
	

 		//开始获取所有订单
 		$shortBargeParams['status'] =1;
 		$shortBargeParams['shipment_uuid'] =$shipmentResult['shipment_uuid'];
 		$shipmentOrders = $this->callSoaErp('post', '/shipment/getShortBargeInfo', $shortBargeParams);
	
 		$shipmentOrders = $shipmentOrders['data'];
 		for($i=0;$i<count($shipmentOrders);$i++){
 			if($i==0){
 				$key="'".$shipmentOrders[$i]['orders_number']."'";
 			}else{
 				$key.=",'".$shipmentOrders[$i]['orders_number']."'";
 			}
 		}


 		$shortBargeParams['orders_number_array'] = $key;
 		$ordersResult = $this->callSoaErp('post', '/order/getShortBrageNeedOrder', $shortBargeParams);
 		$ordersResult = $ordersResult['data'];
		
	
 		for($i=0;$i<count($ordersResult);$i++){
 			if(is_numeric($ordersResult[$i]['pickup_time'])){
 				$ordersResult[$i]['pickup_time'] = date('Y-m-d',$ordersResult[$i]['pickup_time']);
 			}else{
 				$ordersResult[$i]['pickup_time']='';
 			}
 			$ordersGoodsInfo = $ordersResult[$i]['orders_goods_info'];
 			for($j=0;$j<count($ordersGoodsInfo);$j++){
 				if($j==0){
 					$goodsName =$ordersGoodsInfo[$j]['goods_name'];
 					$estimatedCount =$ordersGoodsInfo[$j]['estimated_count'];
 					$estimatedPackCount =$ordersGoodsInfo[$j]['estimated_pack_count'];
 					$estimatedPackUnit =Contents::baseConfig()['order']['goods_pack'][$ordersGoodsInfo[$j]['estimated_pack_unit']];

 					$estimatedWeight =$ordersGoodsInfo[$j]['estimated_weight'];
 					$estimatedVolume =$ordersGoodsInfo[$j]['estimated_volume'];
 				}else{
 					$goodsName.="<br />".$ordersGoodsInfo[$j]['goods_name'];
 					$estimatedCount.="<br />".$ordersGoodsInfo[$j]['estimated_count'];
 					$estimatedPackCount.="<br />".$ordersGoodsInfo[$j]['estimated_pack_count'];
 					$estimatedPackUnit.="<br />".Contents::baseConfig()['order']['goods_pack'][$ordersGoodsInfo[$j]['estimated_pack_unit']];
 					$estimatedWeight.="<br />".$ordersGoodsInfo[$j]['estimated_weight'];
 					$estimatedVolume.="<br />".$ordersGoodsInfo[$j]['estimated_volume'];
 				}



 			}
 			$ordersResult[$i]['goodsName'] = $goodsName;
 			$ordersResult[$i]['estimatedCount'] = $estimatedCount;
 			$ordersResult[$i]['estimatedPackCount'] = $estimatedPackCount;
 			$ordersResult[$i]['estimatedPackUnit'] = $estimatedPackUnit;
 			$ordersResult[$i]['estimatedWeight'] = $estimatedWeight;
 			$ordersResult[$i]['estimatedVolume'] = $estimatedVolume;

 			$shipmentInfo = $ordersResult[$i]['shipmentInfo'];
 			for($k=0;$k<count($shipmentInfo);$k++){
 				if($k==0){
 					$supplierName=$shipmentInfo[$k]['supplier_name'];
 				}else{
 					$supplierName.="<br />".$shipmentInfo[$k]['supplier_name'];
 				}
 			}
 			$ordersResult[$i]['supplierName']=$supplierName;
			for($j=0;$j<count($shipment_goods);$j++){
				if($ordersResult[$i]['orders_number'] == $shipment_goods[$j]['orders_number']){
					$ordersResult[$i]['short_barge_money'] = $shipment_goods[$j]['short_barge_money'];
				}
			}
 		}
	
		$sort = array_column($ordersResult, 'supplierName');
        array_multisort($sort, SORT_DESC, $ordersResult);
	
	
 		$this->assign('ordersResult',$ordersResult);


 		return $this->fetch('update_short_barge');
 	}

	//获取短驳详情

 	public  function shortBargeInfo(){

 		//获取承运商
 		//获取承运商数据
 		$supplierParams['status'] = 1;

 		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $data);




 		$this->assign('supplierResult',$supplierResult['data']);
 		//获取未分配的承运商

 		$shortBargeParam['is_short_barge'] = 1;
 		$shortBargeResult = $this->callSoaErp('post', '/order/getShortBrageNeedOrder', $shortBargeParam);
 		$this->assign('shortBargeResult',$shortBargeResult['data']);

 		//开始获取运单
 		$params = Request::instance()->param();

 		$shipment_uuid = $params['shipment_uuid'];
 		$shipmentParams['shipment_uuid']=$shipment_uuid;
	
 		$shipmentResult = $this->callSoaErp('post', '/shipment/getShipmentShortBarge', $shipmentParams);
	
 		$shipmentResult = $shipmentResult['data'][0];



 		$this->assign('shipmentResult',$shipmentResult);

 		//通过承运商获取车辆
 		$vehicleParams['supplier_uuid'] = $shipmentResult['supplier_uuid'];
 		$vehicleResult = $this->callSoaErp('post', '/source/getVehicle',$vehicleParams);
 		$this->assign('vehicleResult',$vehicleResult['data']);

 		//
 		$nowVehicleParams['vehicle_id'] = $vehicleResult['data'][0]['vehicle_id'];
 		$nowVehicleResult = $this->callSoaErp('post', '/source/getVehicle',$nowVehicleParams);


 		$this->assign('nowVehicleResult',$nowVehicleResult['data'][0]);


 		$shipment_goods = $shipmentResult['shipment_goods'];

 		//开始获取所有订单
 		$shortBargeParams['status'] =1;
 		$shortBargeParams['shipment_uuid'] =$shipmentResult['shipment_uuid'];
 		$shipmentOrders = $this->callSoaErp('post', '/shipment/getShortBargeInfo', $shortBargeParams);
 		$shipmentOrders = $shipmentOrders['data'];
 		for($i=0;$i<count($shipmentOrders);$i++){
 			if($i==0){
 				$key="'".$shipmentOrders[$i]['orders_number']."'";
 			}else{
 				$key.=",'".$shipmentOrders[$i]['orders_number']."'";
 			}
 		}


 		$shortBargeParams['orders_number_array'] = $key;
 		$ordersResult = $this->callSoaErp('post', '/order/getShortBrageNeedOrder', $shortBargeParams);
 		$ordersResult = $ordersResult['data'];
 		for($i=0;$i<count($ordersResult);$i++){
 			if(is_numeric($ordersResult[$i]['pickup_time'])){
 				$ordersResult[$i]['pickup_time'] = date('Y-m-d',$ordersResult[$i]['pickup_time']);
 			}else{
 				$ordersResult[$i]['pickup_time']='';
 			}
 			$ordersGoodsInfo = $ordersResult[$i]['orders_goods_info'];
 			for($j=0;$j<count($ordersGoodsInfo);$j++){
 				if($j==0){
 					$goodsName =$ordersGoodsInfo[$j]['goods_name'];
 					$estimatedCount =$ordersGoodsInfo[$j]['estimated_count'];
 					$estimatedPackCount =$ordersGoodsInfo[$j]['estimated_pack_count'];
 					$estimatedPackUnit =Contents::baseConfig()['order']['goods_pack'][$ordersGoodsInfo[$j]['estimated_pack_unit']];

 					$estimatedWeight =$ordersGoodsInfo[$j]['estimated_weight'];
 					$estimatedVolume =$ordersGoodsInfo[$j]['estimated_volume'];
 				}else{
 					$goodsName.="<br />".$ordersGoodsInfo[$j]['goods_name'];
 					$estimatedCount.="<br />".$ordersGoodsInfo[$j]['estimated_count'];
 					$estimatedPackCount.="<br />".$ordersGoodsInfo[$j]['estimated_pack_count'];
 					$estimatedPackUnit.="<br />".Contents::baseConfig()['order']['goods_pack'][$ordersGoodsInfo[$j]['estimated_pack_unit']];
 					$estimatedWeight.="<br />".$ordersGoodsInfo[$j]['estimated_weight'];
 					$estimatedVolume.="<br />".$ordersGoodsInfo[$j]['estimated_volume'];
 				}



 			}
 			$ordersResult[$i]['goodsName'] = $goodsName;
 			$ordersResult[$i]['estimatedCount'] = $estimatedCount;
 			$ordersResult[$i]['estimatedPackCount'] = $estimatedPackCount;
 			$ordersResult[$i]['estimatedPackUnit'] = $estimatedPackUnit;
 			$ordersResult[$i]['estimatedWeight'] = $estimatedWeight;
 			$ordersResult[$i]['estimatedVolume'] = $estimatedVolume;

 			$shipmentInfo = $ordersResult[$i]['shipmentInfo'];
 			for($k=0;$k<count($shipmentInfo);$k++){
 				if($k==0){
 					$supplierName=$shipmentInfo[$k]['supplier_name'];
 				}else{
 					$supplierName.="<br />".$shipmentInfo[$k]['supplier_name'];
 				}
 			}
 			$ordersResult[$i]['supplierName']=$supplierName;
			for($j=0;$j<count($shipment_goods);$j++){
				if($ordersResult[$i]['orders_number'] == $shipment_goods[$j]['orders_number']){
					$ordersResult[$i]['short_barge_money'] = $shipment_goods[$j]['short_barge_money'];
				}
			}
 		}


 		$this->assign('ordersResult',$ordersResult);


 		return $this->fetch('short_barge_info');
 	}
 	//获取订单货物AJAX
 	public function getShortBrageNeedOrderAjax(){
 		$params = Request::instance()->param();

 		$shortBargeParams['orders_number_array'] = "'".$params['orders_number']."'";
 		$ordersResult = $this->callSoaErp('post', '/order/getShortBrageNeedOrder', $shortBargeParams);

 		$ordersResult = $ordersResult['data'];

 		for($i=0;$i<count($ordersResult);$i++){

 			if(is_numeric($ordersResult[$i]['pickup_time'])){
 				$ordersResult[$i]['pickup_time'] = date('Y-m-d',$ordersResult[$i]['pickup_time']);
 			}else{
 				$ordersResult[$i]['pickup_time']='';
 			}
 			$ordersGoodsInfo = $ordersResult[$i]['orders_goods_info'];
 			for($j=0;$j<count($ordersGoodsInfo);$j++){
 				if($j==0){
 					$goodsName =$ordersGoodsInfo[$j]['goods_name'];
 					$estimatedCount =$ordersGoodsInfo[$j]['estimated_count'];
 					$estimatedPackCount =$ordersGoodsInfo[$j]['estimated_pack_count'];
 					$estimatedPackUnit =Contents::baseConfig()['order']['goods_pack'][$ordersGoodsInfo[$j]['estimated_pack_unit']];

 					$estimatedWeight =$ordersGoodsInfo[$j]['estimated_weight'];
 					$estimatedVolume =$ordersGoodsInfo[$j]['estimated_volume'];
 				}else{
 					$goodsName.="<br />".$ordersGoodsInfo[$j]['goods_name'];
 					$estimatedCount.="<br />".$ordersGoodsInfo[$j]['estimated_count'];
 					$estimatedPackCount.="<br />".$ordersGoodsInfo[$j]['estimated_pack_count'];
 					$estimatedPackUnit.="<br />".Contents::baseConfig()['order']['goods_pack'][$ordersGoodsInfo[$j]['estimated_pack_unit']];
 					$estimatedWeight.="<br />".$ordersGoodsInfo[$j]['estimated_weight'];
 					$estimatedVolume.="<br />".$ordersGoodsInfo[$j]['estimated_volume'];
 				}



 			}
 			$ordersResult[$i]['goodsName'] = $goodsName;
 			$ordersResult[$i]['estimatedCount'] = $estimatedCount;
 			$ordersResult[$i]['estimatedPackCount'] = $estimatedPackCount;
 			$ordersResult[$i]['estimatedPackUnit'] = $estimatedPackUnit;
 			$ordersResult[$i]['estimatedWeight'] = $estimatedWeight;
 			$ordersResult[$i]['estimatedVolume'] = $estimatedVolume;

 			$shipmentInfo = $ordersResult[$i]['shipmentInfo'];
 			for($k=0;$k<count($shipmentInfo);$k++){
 				if($k==0){
 					$supplierName=$shipmentInfo[$k]['supplier_name'];
 				}else{
 					$supplierName.="<br />".$shipmentInfo[$k]['supplier_name'];
 				}
 			}
 			$ordersResult[$i]['supplierName']=$supplierName;
 			for($j=0;$j<count($shipment_goods);$j++){
 				if($ordersResult[$i]['orders_number'] == $shipment_goods[$j]['orders_number']){
 					$ordersResult[$i]['short_barge_money'] = $shipment_goods[$j]['short_barge_money'];
 				}
 			}
 		}

 		return $ordersResult[0];


 	}

 	//短驳新增AJAX
 	public function addShortBargeAjax(){
 		$data = Request::instance()->param();

 		$params = [];
		//短驳日期
 		if(!empty($data['shipment_time'])){
 			$params['shipment_time'] = strtotime($data['shipment_time']);
 		}
 		if(!empty($data['shipment_uuid'])){
 			$params['shipment_uuid'] =$data['shipment_uuid'];
 		}

 		$params['supplier_vehicle_id'] = $data['supplier_vehicle_id'];
 		$params['supplier_uuid'] = $data['supplier_uuid'];


 		$orders_number=$data['orders_number'];
 		$cost_money=$data['cost_money'];
 		$shipment_short_barge = [];
 		for($i=0;$i<count($orders_number);$i++){
 			$shipment_short_barge[$i]['orders_number'] = trim($orders_number[$i]);
 			$shipment_short_barge[$i]['short_barge_money']  = $cost_money[$i];

 		}
 		$params['shipment_short_barge'] = $shipment_short_barge;



 		//最后的
 		$params['pay_all_money'] = $data['pay_all_money'];
 		$params['distribution_type'] = $data['distribution_type'];
 		$params['shipment_remark'] = $data['shipment_remark'];


 		$params['shipment_short_barge_uuid'] = $data['shipment_short_barge_uuid'];




 		if(!empty($params['shipment_uuid'])){


 			$result = $this->callSoaErp('post', '/shipment/updateShortBarge',$params);

 		}else{

 			$result = $this->callSoaErp('post', '/shipment/addShortBarge',$params);
 		}

	
 		return   $result;//['code' => '400', 'msg' => $data];

 	}
 	//短驳已安排
 	public function showShortBargeOverManage(){

		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
 		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', []);
 		$this->assign('supplierResult',$supplierResult['data']);
 		return $this->fetch('short_barge_over_manage');
 	}

 	//短驳已安排
 	public function getShortBargeOverAjax(){
 		$params = Request::instance()->param();
 		$data = [
				'page' => $params['page'],
				'page_size' => $params['limit'],
 				'status'=>1


 		];
 		if(!empty($params['supplier_uuid'])){
 			$data['supplier_uuid'] = $params['supplier_uuid'];
 		}
 		if(!empty($params['orders_number'])){
 			$data['orders_number'] = $params['orders_number'];
 		}
 		if(!empty($params['start_shipment_time'])){
 			$data['start_shipment_time'] = strtotime($params['start_shipment_time']);
 		}
 		if(!empty($params['end_shipment_time'])){
 			$data['end_shipment_time'] = strtotime($params['end_shipment_time']);
 		}
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}		
 		$result = $this->callSoaErp('post', '/shipment/getShortBarge', $data);

 		//获取所有承运商


 		for($i=0;$i<count($result['data']['list']);$i++){
            //分配方式
            $result['data']['list'][$i]['distribution_type_name'] = Contents::baseConfig()['shipment']['short_barge_distribution_type'][$result['data']['list'][$i]['distribution_type']];
            //承运商类型
            $result['data']['list'][$i]['supplier_type_name'] = Contents::baseConfig()['supplier']['supplier_type'][$result['data']['list'][$i]['supplier_type']];
 			if(is_numeric($result['data']['list'][$i]['pickup_time'])){//发运日期
 				$result['data']['list'][$i]['pickup_time'] = date('Y-m-d',$result['data']['list'][$i]['pickup_time']);
 			}else{
 				$result['data']['list'][$i]['pickup_time']='';
 			}
 			if(is_numeric($result['data']['list'][$i]['shipment_time'])){//短驳日期
 				$result['data']['list'][$i]['shipment_time'] = date('Y-m-d',$result['data']['list'][$i]['shipment_time']);
 			}else{
 				$result['data']['list'][$i]['shipment_time']='';
 			}
			$result['data']['list'][$i]['shipment_finance_name']=Contents::baseConfig()['shipment']['shipment_finance_status'][$result['data']['list'][$i]['shipment_finance_status']];
 			$result['data']['list'][$i]['send_all_address']=$result['data']['list'][$i]['send_province_name'].$result['data']['list'][$i]['send_city_name'].$result['data']['list'][$i]['send_area_name'].$result['data']['list'][$i]['send_address'];
 			$result['data']['list'][$i]['accept_all_address']=$result['data']['list'][$i]['accept_province_name'].$result['data']['list'][$i]['accept_city_name'].$result['data']['list'][$i]['accept_area_name'].$result['data']['list'][$i]['accept_address'];

 			$shipmentInfo = $result['data']['list'][$i]['shipment_info'];
 			$shipment_supplier_name='';
 			for($j=0;$j<count($shipmentInfo);$j++){
 				if($j==0){
 					$shipment_supplier_name=$shipmentInfo[$j]['supplier_name'];
 				}else{
 					$shipment_supplier_name.="/".$shipmentInfo[$j]['supplier_name'];
 				}
 			}
 			$result['data']['list'][$i]['shipment_supplier_name']=$shipment_supplier_name;
 			$goodsInfo = $result['data']['list'][$i]['orders_goods_info'];
 			$shipment_supplier_name='';


 			$shipment_weight= 0;
 			$shipment_volume = 0;

 			for($j=0;$j<count($goodsInfo);$j++){
 				if($j==0){
 					$goods_name=$goodsInfo[$j]['goods_name'];

 				}else{
 					$goods_name.="/".$goodsInfo[$j]['goods_name'];

 				}
 				$shipment_weight+=$goodsInfo[$j]['shipment_weight'];
 				$shipment_volume+=$goodsInfo[$j]['shipment_volume'];
 			}
 			$result['data']['list'][$i]['goods_name']=$goods_name;
 			$result['data']['list'][$i]['shipment_weight']=$shipment_weight;
 			$result['data']['list'][$i]['shipment_volume']=$shipment_volume;
 		}
			
 		$r = [
 				'code'=>200,
 				'data'=>$result['data']['list'],
 				'count'=>$result['data']['count']
 		
 		];

        if($params['export']==1){ //导出参数
            return $result;
        }else{
            return $r;
        }
 	}
	
	
 	//异常管理
 	public function abnormalManner(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
 		return $this->fetch('abnormal_manage');
 	}
 	
 	//获取异常管理AJAX
 	public function getAbnormalAjax(){
 		$params = Request::instance()->param();
 		$data = [
				'page' => $params['page'],
				'page_size' => $params['limit'],
 				'status'=>1,
				
				//'shipment_abnormal_status'=>0
 		
 		];
 		if(!empty($params['finance_number'])){
 			$data['finance_number'] = $params['finance_number'];
 		}
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}	
 		if(!empty($params['orders_number'])){
 			$data['orders_number'] = $params['orders_number'];
 		}	
 		if(!empty($params['supplier_name'])){
 			$data['supplier_name'] = $params['supplier_name'];
 		}	
 		if(!empty($params['start_pickup_time'])){
 			$data['pickup_time'] = $params['start_pickup_time'];
 		}
 		if(!empty($params['end_pickup_time'])){
 			$data['pickup_time_end'] = $params['end_pickup_time'];
 		}		
 		if(!empty($params['shipment_status'])){
 			$data['shipment_status'] = $params['shipment_status'];
 		}	
		
		if(is_numeric($params['shipment_status_not3'])){
            $data['shipment_status_not3'] =1;
        }		
		

		if(is_numeric($params['handle'])){
            $data['handle'] =$params['handle'];
        }		
 		$result = $this->callSoaErp('post', '/order/getOrderAbnormal', $data);
		for($i=0;$i<count($result['data']['list']);$i++){
			if(is_numeric($result['data']['list'][$i]['pickup_time'])){
				$result['data']['list'][$i]['pickup_time'] = date('Y-m-d',$result['data']['list'][$i]['pickup_time']);
			}else{
				$result['data']['list'][$i]['pickup_time'] ='';
			}
			if(is_numeric($result['data']['list'][$i]['true_time'])){
				$result['data']['list'][$i]['true_time'] = date('Y-m-d',$result['data']['list'][$i]['true_time']);
			}else{
				$result['data']['list'][$i]['true_time'] ='';
			}			
		}

 		$r = [
 				'code'=>200,
 				'data'=>$result['data']['list'],
 				'count'=>$result['data']['count']

 		];

 		return $r;
 	}
 	//添加异常
 	public function addAbnormal(){
 		
 		$params = Request::instance()->param();
 		
		$orderAbnormalParams['abnormal_number'] = $params['abnormal_number'];
		
		$orderAbnormalResult = $this->callSoaErp('post', '/order/getShipmentNeedOrderAbnormal',$orderAbnormalParams);
		$orderAbnormalResult = $orderAbnormalResult['data'][0];
	
		//$orderAbnormalResult['abnormal_type_name']= Contents::baseConfig()['order']['abnormal_type'][$orderAbnormalResult['abnormal_type_id']];
		$orderAbnormalResult['return_goods_type_name']= Contents::baseConfig()['abnormal']['return_goods_type'][$orderAbnormalResult['return_goods_type']];		
		
		$orderAbnormalResult['indemnity_type_name']= Contents::baseConfig()['abnormal']['indemnity_type'][$orderAbnormalResult['indemnity_type']];				
		
		$orderAbnormalResult['damaged_name']= Contents::baseConfig()['abnormal']['damaged'][$orderAbnormalResult['damaged']];		
		$orderAbnormalResult['responsible_party_name']= Contents::baseConfig()['abnormal']['responsible_party'][$orderAbnormalResult['responsible_party']];				
		
		
		$this->assign('orderAbnormalResult',$orderAbnormalResult);
		
		//获取运单下的所有干线
		$shipmentParams['status'] =1;
		$shipmentParams['orders_number'] =$orderAbnormalResult['orders_number'];
		$shipmentResult = $this->callSoaErp('post', '/shipment/getShipment',$shipmentParams);
		$this->assign('shipmentResult',$shipmentResult['data']);
		//获取运单下的所有短驳

		$shortBargeResult = $this->callSoaErp('post', '/shipment/getShortBargeInfo',$shipmentParams);
		
		//dump($shortBargeResult);exit();
		$this->assign('shortBargeResult',$shortBargeResult['data']);		
	
 		return $this->fetch('abnormal_add');
 	}
	//修改异常
 	public function updateAbnormal(){
 		
 		$params = Request::instance()->param();
 		
		$orderAbnormalParams['abnormal_number'] = $params['abnormal_number'];
		
		$orderAbnormalResult = $this->callSoaErp('post', '/order/getShipmentNeedOrderAbnormal',$orderAbnormalParams);
		$orderAbnormalResult = $orderAbnormalResult['data'][0];
	
		//$orderAbnormalResult['abnormal_type_name']= Contents::baseConfig()['order']['abnormal_type'][$orderAbnormalResult['abnormal_type_id']];
		$orderAbnormalResult['return_goods_type_name']= Contents::baseConfig()['abnormal']['return_goods_type'][$orderAbnormalResult['return_goods_type']];		
		
		$orderAbnormalResult['indemnity_type_name']= Contents::baseConfig()['abnormal']['indemnity_type'][$orderAbnormalResult['indemnity_type']];				
		
		$orderAbnormalResult['damaged_name']= Contents::baseConfig()['abnormal']['damaged'][$orderAbnormalResult['damaged']];		
		$orderAbnormalResult['responsible_party_name']= Contents::baseConfig()['abnormal']['responsible_party'][$orderAbnormalResult['responsible_party']];				
		
		
		$this->assign('orderAbnormalResult',$orderAbnormalResult);
		
		//获取运单下的所有干线
		$shipmentParams['status'] =1;
		$shipmentParams['shipment_type_is_not_3'] =1;
		$shipmentParams['orders_abnormal_number'] =$orderAbnormalResult['abnormal_number'];
		$shipmentResult = $this->callSoaErp('post', '/shipment/getShipmentAbnormal',$shipmentParams);
		
		
		$this->assign('shipmentResult',$shipmentResult['data']);
		//获取运单下的所有短驳
		$shortBargeParams['status'] =1;

		$shortBargeParams['orders_number'] =$orderAbnormalResult['orders_number'];
		
	
		$shortBargeResult = $this->callSoaErp('post', '/shipment/getShortBargeInfo',$shortBargeParams);
		
		$this->assign('shortBargeResult',$shortBargeResult['data']);
 		return $this->fetch('abnormal_update');
 	}	
	//异常详情
 	public function abnormalInfo(){
 		
 		$params = Request::instance()->param();
 		
		$orderAbnormalParams['abnormal_number'] = $params['abnormal_number'];
		
		$orderAbnormalResult = $this->callSoaErp('post', '/order/getShipmentNeedOrderAbnormal',$orderAbnormalParams);
		$orderAbnormalResult = $orderAbnormalResult['data'][0];
	
		//$orderAbnormalResult['abnormal_type_name']= Contents::baseConfig()['order']['abnormal_type'][$orderAbnormalResult['abnormal_type_id']];
		$orderAbnormalResult['return_goods_type_name']= Contents::baseConfig()['abnormal']['return_goods_type'][$orderAbnormalResult['return_goods_type']];		
		
		$orderAbnormalResult['indemnity_type_name']= Contents::baseConfig()['abnormal']['indemnity_type'][$orderAbnormalResult['indemnity_type']];				
		
		$orderAbnormalResult['damaged_name']= Contents::baseConfig()['abnormal']['damaged'][$orderAbnormalResult['damaged']];		
		$orderAbnormalResult['responsible_party_name']= Contents::baseConfig()['abnormal']['responsible_party'][$orderAbnormalResult['responsible_party']];				
		
		
		$this->assign('orderAbnormalResult',$orderAbnormalResult);
		
		//获取运单下的所有干线
		$shipmentParams['status'] =1;
		$shipmentParams['shipment_type_is_not_3'] =1;
		$shipmentParams['orders_abnormal_number'] =$orderAbnormalResult['abnormal_number'];
		$shipmentResult = $this->callSoaErp('post', '/shipment/getShipmentAbnormal',$shipmentParams);
		
		
		$this->assign('shipmentResult',$shipmentResult['data']);
		//获取运单下的所有短驳
		$shortBargeParams['status'] =1;

		$shortBargeParams['orders_number'] =$orderAbnormalResult['orders_number'];
		
	
		$shortBargeResult = $this->callSoaErp('post', '/shipment/getShortBargeInfo',$shortBargeParams);
		
		$this->assign('shortBargeResult',$shortBargeResult['data']);
 		return $this->fetch('abnormal_info');
 	}		
	//删除异常管理
	public function delAbnormal(){
		$params = Request::instance()->param();
	

		$result = $this->callSoaErp('post', '/shipment/delShipmentAbnormal', $params);
		
		
		
		return $result;
	}
 	//异常管理-待处理
 	public function abnormalInfoManner(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
		
 		return $this->fetch('abnormal_info_manage');
 	}
 	//已处理
 	public function abnormalDoneManner(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
		
 		return $this->fetch('abnormal_done_manage');
 	}	
 	//获取异常管理AJAX
 	public function getAbnormalInfoAjax(){
 		$params = Request::instance()->param();
 		$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
			'status'=>1
 	
 		];
 		if(!empty($params['finance_number'])){
 			$data['finance_number'] = $params['finance_number'];
 		}
 
 		$data['status'] = 1;
 	
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}	
		if(!empty($params['orders_number'])){
			$data['orders_number'] =$params['orders_number'];
		}
		if(!empty($params['supplier_name'])){
			$data['supplier_name'] =$params['supplier_name'];
		}
		if(!empty($params['start_shipment_time'])){
			$data['start_shipment_time'] =strtotime($params['start_shipment_time']);
		}	
		if(!empty($params['end_shipment_time'])){
			$data['end_shipment_time'] =strtotime($params['end_shipment_time']);
		}			
 		$result = $this->callSoaErp('post', '/shipment/getShipmentAbnormal', $data);
	
 			
 		for($i=0;$i<count($result['data']['list']);$i++){
 			$result['data']['list'][$i]['shipment_type_name']=Contents::baseConfig()['shipment']['shipment_type'][$result['data']['list'][$i]['shipment_type']];
 	
 	
 			$result['data']['list'][$i]['solve_type_name']=Contents::baseConfig()['abnormal']['solve_type'][$result['data']['list'][$i]['solve_type']];

  			$result['data']['list'][$i]['shipment_time']=date('Y-m-d',$result['data']['list'][$i]['shipment_time']);
		
			$orders_abnormal_result ='';
			for($k=0;$k<count($result['data']['list'][$i]['orders_abnormal']);$k++){
				if($k==0){
					$orders_abnormal_result=$result['data']['list'][$i]['orders_abnormal'][$k]['abnormal_number'].'-'.$result['data']['list'][$i]['orders_abnormal'][$k]['abnormal_type_name'].'-'.$result['data']['list'][$i]['orders_abnormal'][$k]['abnormal_money'];
				}else{
					
					$orders_abnormal_result.='/'.$result['data']['list'][$i]['orders_abnormal'][$k]['abnormal_number'].'-'.$result['data']['list'][$i]['orders_abnormal'][$k]['abnormal_type_name'].'-'.$result['data']['list'][$i]['orders_abnormal'][$k]['abnormal_money'];

				}
				
			}
			$result['data']['list'][$i]['orders_abnormal_result']= $orders_abnormal_result;
		
		}
	 	$r = [
	 			'code'=>200,
	 			'data'=>$result['data']['list'],
	 			'count'=>$result['data']['count']
	 	
	 	];
 		return $r;
 	} 	
 	

 		

 	/**
 	 * 添加发运异常数据111
 	 */
 	public function addShipmentAbnormalAjax(){
 		$params = Request::instance()->param();


		
		if($params['shipment_status']==3){
			
			
			$result = $this->callSoaErp('post', '/shipment/updateShipmentAbnormal',$params);
		}else if($params['shipment_status']==2){
			$new_data = [];
			for($i=0;$i<count($params['shipment_abnormal_id']);$i++){
				$key['shipment_abnormal_id'] =$params['shipment_abnormal_id'][$i];
				$key['shipment_abnormal_money'] =$params['shipment_money'][$i];
			
				$new_data[] = $key;
			}
			$params['shipmentData'] = $new_data;
			$key = [];
			$new_data = [];
			for($i=0;$i<count($params['shipment_short_barge_id']);$i++){
				$key['shipment_short_barge_id'] =$params['shipment_short_barge_id'][$i];
				$key['shipment_abnormal_money'] =$params['short_barge_money'][$i];
			
				$new_data[] = $key;
			}			
			$params['shortBargeData'] = $new_data;
			
 			$result = $this->callSoaErp('post', '/shipment/updateShipmentAbnormal',$params);
			
 		}else{
 	
			$new_data = [];
			for($i=0;$i<count($params['shipment_number']);$i++){
				$key['shipment_number'] =$params['shipment_number'][$i];
				$key['shipment_abnormal_money'] =$params['shipment_money'][$i];
				$key['shipment_uuid'] =$params['shipment_uuid'][$i];
				$key['is_type'] =1;//干线
				$new_data[] = $key;
			}
			for($i=0;$i<count($params['short_barge_number']);$i++){
				$key['shipment_number'] =$params['short_barge_number'][$i];
				$key['shipment_abnormal_money'] =$params['short_barge_money'][$i];
				$key['shipment_uuid'] =$params['short_barge_shipment_uuid'][$i];
				$key['is_type'] =2;//短驳
				$new_data[] = $key;
			}			
			$params['data'] = $new_data;
			
 			$result = $this->callSoaErp('post', '/shipment/addShipmentAbnormal',$params);
 		}
 		 


		
 		return   $result;//['code' => '400', 'msg' => $data];
 	}	
 	
 	//获取成本核算-未审核
 	public function getShipmentCostCheck(){
 	
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
		
 		//获取所有承运商
 		$supplierParams =[];
 		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);
 			
 		$this->assign('supplierResult',$supplierResult['data']);
		//获取所有客户
		$projectParams = [];
		
		$projectResult = $this->callSoaErp('post', '/source/getProject', $projectParams);
		
		$this->assign('projectResult',$projectResult['data']);

 		return $this->fetch('cost_check');
 		
 	}
 	//获取成本核算-已审核
 	public function getShipmentCostChecked(){
 	
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
		
 		//获取所有承运商
 		$supplierParams =[];
 		$supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);
 			
 		$this->assign('supplierResult',$supplierResult['data']);
		//获取所有客户
		$projectParams = [];
		
		$projectResult = $this->callSoaErp('post', '/source/getProject', $projectParams);
		
		$this->assign('projectResult',$projectResult['data']);

 		return $this->fetch('cost_checked');
 		
 	}	
	//获取成本核算AJAX
 	public function getShipmentCostCheckAjax(){
 		$params = Request::instance()->param();
 		$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
			'status'=>1,

 		];
 		if(!empty($params['finance_number'])){
 			$data['finance_number'] = $params['finance_number'];
 		}
 		if(!empty($params['supplier_uuid'])){
 			$data['supplier_uuid'] = $params['supplier_uuid'];
 		}
		if(!empty($params['orders_number'])){
 			$data['orders_number'] = $params['orders_number'];
 		}
 		if(is_numeric($params['shipment_finance_status'])){
 			$data['shipment_finance_status'] = $params['shipment_finance_status'];
 		}		
 		if(is_numeric($params['project_id'])){
 			$data['project_id'] = $params['project_id'];
 		}	
 		if(!empty($params['start_shipment_time'])){
 			$data['start_shipment_time'] = strtotime($params['start_shipment_time']);
 		}	 
 		if(!empty($params['end_shipment_time'])){
 			$data['end_shipment_time'] = strtotime($params['end_shipment_time']);
 		}	
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		} 	
		//代表已审核状态
		if(is_numeric($params['is_checked'])){
			if(empty($params['shipment_finance_status'])){
				$data['shipment_finance_status_not_miss'] = 1;
				
			}
         
        }
		
 		$result = $this->callSoaErp('post', '/shipment/getShipmentInfo', $data);
 		
		

 		for($i=0;$i<count($result['data']['list']);$i++){
 		
 			if(is_numeric($result['data']['list'][$i]['shipment_time'])){//发运日期
 				$result['data']['list'][$i]['shipment_time'] = date('Y-m-d',$result['data']['list'][$i]['shipment_time']);
 			}else{
 				$result['data']['list'][$i]['shipment_time']='';
 			}
 			$result['data']['list'][$i]['shipment_type_name']=Contents::baseConfig()['shipment']['shipment_type'][$result['data']['list'][$i]['shipment_type']];
 			$result['data']['list'][$i]['shipment_finance_name']=Contents::baseConfig()['shipment']['shipment_finance_status'][$result['data']['list'][$i]['shipment_finance_status']];
 			
			//开始获取运费子项
			$list_yunfei = 0;
			$list_tihuo = 0;
			$list_songhuo =0;
			$list_zhuangxie = 0;
			$shipment_cost_result = $result['data']['list'][$i]['shipment_cost'];
			for($k=0;$k<count($shipment_cost_result);$k++){
				if($shipment_cost_result[$k]['cost_id'] ==5){
					$list_yunfei = $shipment_cost_result[$k]['cost_money'];
				}else if($shipment_cost_result[$k]['cost_id'] ==6){
					$list_tihuo = $shipment_cost_result[$k]['cost_money'];
					
				}else if($shipment_cost_result[$k]['cost_id'] ==7){
					$list_songhuo = $shipment_cost_result[$k]['cost_money'];
					
				}else if($shipment_cost_result[$k]['cost_id'] ==8){
					$list_zhuangxie = $shipment_cost_result[$k]['cost_money'];
					
				}
			}
			
			$result['data']['list'][$i]['list_yunfei'] = $list_yunfei;
			$result['data']['list'][$i]['list_tihuo'] = $list_tihuo;
			$result['data']['list'][$i]['list_songhuo'] = $list_songhuo;
			$result['data']['list'][$i]['list_zhuangxie'] = $list_zhuangxie;
			
			
			
			if($result['data']['list'][$i]['shipment_type']==3){//代表短驳
				$shipment_short_barge_info = $result['data']['list'][$i]['shipment_short_barge_info'];
				$shipment_short_barge_orders_number='';
				for($k=0;$k<count($shipment_short_barge_info);$k++){
					$shipment_short_barge_orders_number.='/'.$shipment_short_barge_info[$k]['orders_number'];
				}
				
				$result['data']['list'][$i]['orders_number'] = trim($shipment_short_barge_orders_number,'/');
			}
			
 			$sh_ab = $result['data']['list'][$i]['shipment_abnormal'];
 			$ab_money = 0;
 			for($j=0;$j<count($sh_ab);$j++){
 				$ab_money+=$sh_ab[$j]['shipment_abnormal_money'];
 			}
 			$result['data']['list'][$i]['shipment_abnormal_money'] = $ab_money;
 		}
	 	$r = [
	 			'code'=>200,
	 			'data'=>$result['data']['list'],
	 			'count'=>$result['data']['count']
	 	
	 	];
 		return $r;
 		
 	}
 	//提交成本核算
 	public function addCostCheckAjax(){
 		$params = Request::instance()->param();
 		$data['shipment_uuid_array'] =explode(',',$params['shipment_uuid']);
		$data['shipment_finance_status'] = 2;
 		$result = $this->callSoaErp('post', '/shipment/updateShipmentFinanceStatus', $data);
 		return $result;
 	}

    //未发运导出
    public function export(){

        $params = Request::instance()->param();
        $data = $this->showLineAjax($params);

        $new_data = [];
        foreach($data['data'] as $key=>$val){
            $new_data[$key]['orders_number'] = $val['orders_number']; //运单号
            $new_data[$key]['pickup_time'] = date('Y-m-d',$val['pickup_time']); //发运日期
            $new_data[$key]['pickup_time_end'] = date('Y-m-d',$val['pickup_time']); //预计到货日期
            $new_data[$key]['project_name'] = $val['project_name']; //项目名称
            $new_data[$key]['send_location_name'] = $val['send_location_name']; //发站
            $new_data[$key]['accept_location_name'] = $val['accept_location_name']; //到站
            $new_data[$key]['send_goods_company'] = $val['send_goods_company']; //发货方
            $new_data[$key]['send_cellphone'] = $val['send_cellphone']; //发货方联系方式
            $new_data[$key]['send_address'] = $val['send_address']; //发货地址
            $new_data[$key]['accept_goods_company'] = $val['accept_goods_company']; //收货方
            $new_data[$key]['accept_name'] = $val['accept_name']; //收货人
            $new_data[$key]['accept_cellphone'] = $val['accept_cellphone']; //收货方联系方式
            $new_data[$key]['accept_address'] = $val['accept_address']; //收货地址
            foreach($val['orders_goods_info'] as $k1=>$v1){
                if($v1['estimated_pack_unit']==1){
                    $estimated_pack_unit = "纸箱";
                }else if($v1['estimated_pack_unit']==2){
                    $estimated_pack_unit = "托盘";
                }else if($v1['estimated_pack_unit']==3){
                    $estimated_pack_unit = "桶";
                }else if($v1['estimated_pack_unit']==4){
                    $estimated_pack_unit = "袋装";
                }else if($v1['estimated_pack_unit']==5){
                    $estimated_pack_unit = "编织袋";
                }

                if($k1!=count($val['orders_goods_info'])-1){
                    $goods_name .= $v1['goods_name']."/";
                    $estimated_pack_unit2 .= $estimated_pack_unit."/";
                }else{
                    $goods_name .= $v1['goods_name'];
                    $estimated_pack_unit2 .= $estimated_pack_unit;
                }
                $estimated_count += $v1['estimated_count'];
                $estimated_pack_count += $v1['estimated_pack_count'];
                $estimated_volume += $v1['estimated_volume'];
                $estimated_weight += $v1['estimated_weight'];
            }
            $new_data[$key]['goods_name'] = $goods_name; //货物名称
            $new_data[$key]['estimated_count'] = $estimated_count; //下单件数
            $new_data[$key]['estimated_pack_count'] = $estimated_pack_count; //下单数量
            $new_data[$key]['estimated_pack_unit'] = $estimated_pack_unit2; //包装单位
            $new_data[$key]['estimated_volume'] = $estimated_volume; //下单体积
            $new_data[$key]['estimated_weight'] = $estimated_weight; //下单重量
            if($val['bargain_type']==1){
                $bargain_type = "合同";
            }else{
                $bargain_type = "单票";
            }
            $new_data[$key]['bargain_type'] = $bargain_type; //交货方式
            $new_data[$key]['remark'] = $val['remark']; //备注
        }

//        echo "<pre>";
//        var_dump($new_data);exit;
//        echo "</pre>";

        $name = "发运安排导出";
        $xAxis = [];
        $title = "发运安排导出";
        $header = array('运单号', '发运日期', '预计到货日期', '项目名称', '发站', '到站', '发货方', '发货方联系方式', '发货地址', '收货方', '收货人', '收货方联系方式', '收货地址', '货物名称', '下单件数', '下单数量', '包装单位', '下单体积', '下单重量', '交货方式', '备注');

        $filter = [
            [
                "code"=>"A",
                "value"=>20
            ],
            [
                "code"=>"B",
                "value"=>20
            ]
        ];

        //导出excel
        $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name, $filter);
    }

    //已发运导出
    public function export2(){
        $params = Request::instance()->param();
		$params['status'] = 1;
        $data = $this->getLineOverAjax($params);
	
        $new_data = [];
        foreach($data['data'] as $key=>$val){
            //财务状态
            $new_data[$key]['shipment_finance_name'] = Contents::baseConfig()['shipment']['shipment_finance_status'][$val['shipment_finance_status']];
            $new_data[$key]['pickup_time'] = date('Y-m-d',$val['pickup_time']); //运单日期
            $new_data[$key]['shipment_time'] = date('Y-m-d',$val['shipment_time']); //发运日期
            $new_data[$key]['project_name'] = $val['project_name']; //项目
            $new_data[$key]['orders_number'] = $val['orders_number']; //运单号
            $new_data[$key]['finance_number'] = $val['finance_number']; //合同号
            $new_data[$key]['supplier_name'] = $val['supplier_name']; //承运商
            $new_data[$key]['supplier_shipment_number'] = $val['supplier_shipment_number']; //专线编号
            $new_data[$key]['send_location_name'] = $val['send_location_name']; //发站
            $new_data[$key]['accept_location_name'] = $val['accept_location_name']; //到站
            $new_data[$key]['send_goods_company'] = $val['send_goods_company']; //发货方
            $new_data[$key]['accept_goods_company'] = $val['accept_goods_company']; //收货方
            $new_data[$key]['accept_address'] = $val['accept_address']; //收货地址
			$shipment_count =0;
			$shipment_pack_count=0;
			$shipment_volume=0;
			$shipment_weight=0;
            foreach($val['shipment_goods'] as $k1=>$v1){
                if($k1==0){
                    $goods_name = $val['shipment_goods'][$k1]['goods_name'];
                }else{
                    $goods_name.='/'.$val['shipment_goods'][$k1]['goods_name'];
                }
                $shipment_count += $val['shipment_goods'][$k1]['shipment_count'];
                $shipment_pack_count += $val['shipment_goods'][$k1]['shipment_pack_count'];
                $shipment_volume += $val['shipment_goods'][$k1]['shipment_volume'];
                $shipment_weight += $val['shipment_goods'][$k1]['shipment_weight'];
            }
		
            $new_data[$key]['goods_name'] = $goods_name; //货物名称
            $new_data[$key]['shipment_count'] = $shipment_count; //发运件数
            $new_data[$key]['shipment_pack_count'] = $shipment_pack_count; //发运数量
            $new_data[$key]['shipment_volume'] = $shipment_volume; //发运体积
            $new_data[$key]['shipment_weight'] = $shipment_weight; //发运重量
            $new_data[$key]['pay_all_money'] = $val['pay_all_money']; //运费
			//开始计算费用类别
			$yunfei = 0;
			$songhuofei=0;
			$qitafei=0;
			$xiehuofei=0;
			$qibufei =0;
			$tihuofei = 0;
			foreach($val['shipment_cost'] as $k2=>$v2){
                if($val['shipment_cost'][$k2]['cost_id']==5){
                    $yunfei = $val['shipment_cost'][$k2]['cost_money'];
                }else if($val['shipment_cost'][$k2]['cost_id']==6){
                    $tihuofei = $val['shipment_cost'][$k2]['cost_money'];
                }else if($val['shipment_cost'][$k2]['cost_id']==7){
                    $songhuofei = $val['shipment_cost'][$k2]['cost_money'];
                }else if($val['shipment_cost'][$k2]['cost_id']==8){
                    $xiehuofei = $val['shipment_cost'][$k2]['cost_money'];
                }else if($val['shipment_cost'][$k2]['cost_id']==9){
                    $qibufei = $val['shipment_cost'][$k2]['cost_money'];
                }else if($val['shipment_cost'][$k2]['cost_id']==10){
                    $qitafei = $val['shipment_cost'][$k2]['cost_money'];
                }

            }
			
			 $new_data[$key]['yunfei'] = $yunfei;
			 $new_data[$key]['songhuofei'] = $songhuofei;
			 $new_data[$key]['qitafei'] = $qitafei;
			 $new_data[$key]['xiehuofei'] = $xiehuofei;
			 $new_data[$key]['qibufei'] = $qibufei;
			 $new_data[$key]['tihuofei'] = $tihuofei;			
			
            if($val['is_short_barge'] == 1){
                $is_short_barge ='是';
            }else{
                $is_short_barge ='否';
            }
            $new_data[$key]['is_short_barge'] = $is_short_barge; //是否短驳
            if($val['is_print'] == 0){
                $is_print ='未打印';
            }else{
                $is_print ='已打印';
            }
            $new_data[$key]['is_print'] = $is_print; //是否打印
            $new_data[$key]['remark'] = $val['remark']; //备注
        }

//        echo "<pre>";
//        var_dump($new_data);exit;
//        echo "</pre>";

        $name = "发运安排导出-已发运";
        $xAxis = [];
        $title = "发运安排导出-已发运";
        $header = array('财务状态', '运单日期', '发运日期', '项目', '运单号', '合同号', '承运商', '专线编号', '发站', '到站', '发货方', '收货方', '收货地址', '货物名称', '发运件数', '发运数量', '发运体积', '发运重量', '总运费','运费','送货费','其他费','卸货费','起步费','提货费', '是否短驳', '是否打印', '备注');

        //导出excel
        $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name);

    }

    //未安排短驳导出
    public function export3(){
        $params = Request::instance()->param();
		$params['status'] = 1;
        $data = $this->getShortBargeAjax($params);

        $new_data = [];
        foreach($data['data'] as $key=>$val) {
            $new_data[$key]['pickup_time'] = date('Y-m-d', $val['pickup_time']); //运单日期
            $new_data[$key]['project_name'] = $val['project_name']; //项目
            $new_data[$key]['orders_number'] = $val['orders_number']; //运单号
            $new_data[$key]['send_goods_company'] = $val['send_goods_company']; //发货方
            $new_data[$key]['send_address'] = $val['send_address']; //发货地址
            $new_data[$key]['send_location_name'] = $val['send_location_name']; //发站
            $new_data[$key]['accept_goods_company'] = $val['accept_goods_company']; //收货方
            $new_data[$key]['accept_address'] = $val['accept_address']; //收货地址
            $new_data[$key]['accept_location_name'] = $val['accept_location_name']; //到站
            foreach($val['shipmentInfo']  as $k2=>$v2){
                if($k2==0){
                    $supplier_name = $val['shipmentInfo'][$k2]['supplier_name'];
                }else{
                    $supplier_name.='/'.$val['shipmentInfo'][$k2]['supplier_name'];
                }
            }
            $new_data[$key]['supplier_name'] = $supplier_name; //承运商
			$shipment_count = 0;
			$shipment_pack_count =0;
			$shipment_volume=0;
			$shipment_weight = 0;
            if(count($val['orders_goods_info'])>0){
                foreach($val['orders_goods_info'] as $k1=>$v1){
                    if($k1==0){
                        $goods_name = $val['orders_goods_info'][$k1]['goods_name'];
                    }else{
                        $goods_name.='/'.$val['orders_goods_info'][$k1]['goods_name'];
                    }
                    $shipment_count += $val['orders_goods_info'][$k1]['estimated_count'];
                    $shipment_pack_count += $val['orders_goods_info'][$k1]['estimated_pack_count'];
                    $shipment_volume += $val['orders_goods_info'][$k1]['estimated_weight'];
                    $shipment_weight += $val['orders_goods_info'][$k1]['estimated_volume'];
                }
            }else{
                $goods_name = "";
                $shipment_count = 0;
                $shipment_pack_count = 0;
                $shipment_volume = 0;
                $shipment_weight = 0;
            }

            $new_data[$key]['goods_name'] = $goods_name; //货物名称
            $new_data[$key]['shipment_count'] = $shipment_count; //发运件数
            $new_data[$key]['shipment_pack_count'] = $shipment_pack_count; //发运数量
            $new_data[$key]['shipment_volume'] = $shipment_volume; //发运体积
            $new_data[$key]['shipment_weight'] = $shipment_weight; //发运重量
            $new_data[$key]['remark'] = $val['remark']; //备注
        }

//        echo "<pre>";
//        var_dump($new_data);exit;
//        echo "</pre>";

        $name = "短驳安排导出-未安排";
        $xAxis = [];
        $title = "短驳安排导出-未安排";
        $header = array('运单日期', '项目', '运单号', '发货方', '发货地址', '发站', '收货方', '收货地址', '到站', '承运商', '货物名称', '发运件数', '发运数量', '发运体积', '发运重量', '备注');

        //导出excel
        $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name);

    }

    //已安排短驳导出
    public function export4(){
        $params = Request::instance()->param();
        $data = $this->getShortBargeOverAjax($params);

        $new_data = [];
        foreach($data['data'] as $key=>$val) {
            $new_data[$key]['shipment_time'] = date('Y-m-d', $val['shipment_time']); //短驳日期
            $new_data[$key]['shipment_finance_name'] = Contents::baseConfig()['shipment']['shipment_finance_status'][$val['shipment_finance_status']]; //财务状态
            $new_data[$key]['finance_number'] = $val['finance_number']; //短驳号
            $new_data[$key]['supplier_name'] = $val['supplier_name']; //承运商
            $new_data[$key]['supplier_type_name'] = Contents::baseConfig()['supplier']['supplier_type'][$val['supplier_type']]; //承运商类型
            $new_data[$key]['pay_all_money'] = $val['pay_all_money']; //成本
            $new_data[$key]['distribution_type_name'] = Contents::baseConfig()['shipment']['short_barge_distribution_type'][$val['distribution_type']]; //分配方式
            $new_data[$key]['shipment_remark'] = $val['shipment_remark']; //发运备注
            $new_data[$key]['create_user_name'] = $val['create_user_name']; //创建人
        }

//        echo "<pre>";
//        var_dump($new_data);exit;
//        echo "</pre>";

        $name = "短驳安排导出-已安排";
        $xAxis = [];
        $title = "短驳安排导出-已安排";
        $header = array('短驳日期', '财务状态', '短驳号', '承运商', '承运商类型', '成本', '分配方式', '发运备注', '创建人');

        //导出excel
        $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name);

    }
	
	
	

 	
}