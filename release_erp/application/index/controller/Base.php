<?php
namespace app\index\controller;
use app\common\help\Help;
use think\Lang;
use think\config;
use think\Request;
use think\Controller;
use app\common\help\Contents;
use Underscore\Types\Arrays;
use think\Cache;

class Base extends Controller
{
    protected $_soaerpConfig;
    protected $_soaerpUrl;
    protected $_controller_name;
    protected $_function_name;
    protected $_page_size;
    protected $_authConfig;
    protected $_http_referer;
    protected  $baseConfig;

    public $room_type;
	public $_fax;
    public function __construct()
    {
    	
        if(empty(session('user')) || is_null(session('user'))){
        	$controller_name = strtolower(Request::instance()->controller());
     
        	if(strtolower(Request::instance()->controller())!='screen' &&  strpos($controller_name,'mobile')==false){
        		echo "<script>location.href='/login/show'</script>";
        	}
        	
     
        }
	        /***
         * 判断是否是初始密码
         */
        if(session('user')['base_password']==1){
        	if(strtolower(Request::instance()->controller())=='system' && strtolower(Request::instance()->action())=='showchangepassword'){
        		//$this->assign('base_tixing','请重置您的密码Please reset your password');
        	}else{
        		if(strtolower(Request::instance()->action())=='sendsystememailajax'){
        			
        		}else{
        			echo "<script>location.href='/system/showChangePassword/user_id/".session('user')['user_id']."'</script>";
        		}
        		
        		
        		
        		
        	}
        	
        	
        }	


       
	
        $this->_soaerpConfig= config('soaerp');

        $this->_soaerpUrl = $this->_soaerpConfig['ip'].':'.$this->_soaerpConfig['port'];
        parent::__construct();

        $url_params = help::delUrlPage(Request::instance()->query());
        $url_params = substr($url_params, strpos($url_params, '&'));
		$now_url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$now_url = str_ireplace('&','_-',$now_url);
		$this->assign('now_url',$now_url);
		$params = Request::instance()->param();
		if(!empty($params['after_url'])){
			$after_url = $params['after_url'];
			$after_url = str_ireplace('_-','&',$after_url);
			$this->assign('after_url',"http://".$after_url);
		}
		
        $this->assign('controller_name',strtolower(Request::instance()->controller()));
        $this->assign('function_name',strtolower(Request::instance()->action()));
		$this->assign('url_params',$url_params);
        $this->assign('page_url','http://'.Request::instance()->host().Request::instance()->baseUrl());
      


		$this->assign('goodsPackagingType',Contents::goodsPackagingType());
		$this->baseConfig=Contents::baseConfig();
		$this->assign('baseConfig',Contents::baseConfig());
		$this->assign('fax',Contents::getFax());
		$this->_fax = Contents::getFax();


       	//分页默认数量
       	$this->_page_size = Contents::PAGE_SIZE;
       	$this->assign('page_size',Contents::PAGE_SIZE);
       	//获取权限
       // $this->_authConfig = $this->callSoaErp('post','/system/getAuthConfig',[])['data'];
       	
       	//开始判断权限是否有
       	$controller_name =  strtolower(Request::instance()->controller());
       	$function_name =  Request::instance()->action();
		$user_auth_config  = session('authConfig');
		$check_auth = false;

        $this->assign('auth_id',array_column($user_auth_config,'auth_id'));
		//echo $controller_name.' '.$function_name;

		for($i=0;$i<count($user_auth_config);$i++){
			if($controller_name==$user_auth_config[$i]['controller_name'] && $function_name== strtolower($user_auth_config[$i]['function_name'])){
				$check_auth =  true;
			}
		}
		if($check_auth==false){
			
			//echo 1;
			//$this->error('没权限','/');
		}
		$this->_http_referer = $_SERVER['HTTP_REFERER'];
		$this->assign('http_referer',$_SERVER['HTTP_REFERER']);
	
		
		//dump(session('user'));

		$this->assign('hikApiUrl',Contents::hikApiUrl());
	
    }



    /**
     * 读取soaerp方法
     */
    public function callSoaErp($method,$function,$data=[]){
		
        $params = Request::instance()->param();

        $data['appKey'] = 'nexus';
        $data['appSecret']='nexusIt';
		
        $data['user_id']  = session('user')['user_id'];

		
	
        $result = Help::http($method,$this->_soaerpUrl.$function,$data);
	
        if(isset($params['look_data']))
        {echo $result;exit();

        }
      
		$result = json_decode($result,true);

		//dump($result);
		if($result['code']==200){
            $this->outPut($result['data']);
            $result = ['code' => '200', 'msg' => 'success','data'=>$result['data'],'count'=>$result['data_count']];

        }else{
            $this->outPutError($result);
            $result = ['code' => '400', 'msg' => $result['msg']];
        }
        return $result;
    }

    /**
     * 读取soaerp方法
     */
    public function callSoaErp2($method,$function,$data=[]){

        $data['appKey'] = 'nexus';
        $data['appSecret']='nexusIt';

        $data['user_id']  = 1;



        $result = Help::http($method,$this->_soaerpUrl.$function,$data);


        $result = json_decode($result,true);

        //dump($result);
        if($result['code']==200){
            $this->outPut($result['data']);
            $result = ['code' => '200', 'msg' => 'success','data'=>$result['data'],'count'=>$result['data_count']];

        }else{
            $this->outPutError($result);
            $result = ['code' => '400', 'msg' => $result['msg']];
        }
        return $result;
    }
   
    
    //错误日志
    protected function outPutError($errorMsg) {

         \think\Response::create(['code' => '400', 'msg' => $errorMsg['msg']], 'json')->send();
        //防止意外发生
         exit;
    }
    //输出 之后加上日志存放REDIS
    public function outPut($result) {

        if(empty($result)){
            $result = [];
        }
        // \think\Response::create(['code' => '200', 'data' => $result], 'json')->send();
        //防止意外发生
        //exit;
    }



    
    //获取当前页的方法
    public function page(){
    	$page = input('page')?input('page'):1;
    	return $page;
    }
	/**
	 * 获取分页的一些参数
	 */
    //获取layui,table数据
	public function getLayuiData($result){
     $data['code']=0;
     $data['msg']='';
     $data['count']=$result['data']['count'];
     $data['data']=$result['data']['list'];
     return json($data);
    }

    public function getPageParams($result){
    	$this->assign('total',$result['data']['count']);//总数
    	$this->assign('page',$this->page()); //当前页数
    	$this->assign('total_page',$result['data']['page_count']);//总共分几页
     
			$this->assign('data',$result['data']['list']);//分页数据

    
    }
    //获取分页的一些参数(海康)
    
    public function getHaikangPageParams($result){
    	$this->assign('total',$result['data']['total']);//总数
    	$this->assign('page',$this->page()); //当前页数
    	$this->assign('total_page',ceil($result['data']['total']/$result['data']['pageSize']));//总共分几页
    	$this->assign('data',$result['data']['list']);//分页数据
    
    }    
    
    protected function input() {
    	$contents = file_get_contents("php://input");
    	
    	$param = json_decode($contents, TRUE);
    	if (empty($param))
    		return $this->output([], 'json error', '0004');
    	return $param;
    }
    
    
    /**
     * 读取soaerp方法 用于测试
     */
    public function callSoaErpTest($method,$function,$data=[]){
    
    	$data['appKey'] = 'nexus';
    	$data['appSecret']='nexusIt';
    	$data['lang_id']  = session('user')['language_id'];
    	$data['user_id']  = session('user')['user_id'];
    	 

    
    	$result = Help::http($method,$this->_soaerpUrl.$function,$data);
    	//error_log(print_r($result,1));
   		 dump($result);
    	$result = json_decode($result,true);
    	//error_log(print_r($result,1));
    	dump($result);
    	if($result['code']==200){
    		$this->outPut($result['data']);
    		$result = ['code' => '200', 'msg' => 'success','data'=>$result['data']];
    
    	}else{
    		//$this->outPutError($result);
    		$result = ['code' => '400', 'msg' => $result['msg']];
    	}
    	return $result;
    }
    /**
     * 读取soaerp方法 用于测试
     */
    public function callHaikang($method,$function,$data=[]){
    
    	$data['appKey'] = 'nexus';
    	$data['appSecret']='nexusIt';
    	$data['lang_id']  = session('user')['language_id'];
    	$data['user_id']  = session('user')['user_id'];
    
    
    
    	$result = Help::http($method,'https://192.168.1.14'.$function,$data);
    	//error_log(print_r($result,1));
    	dump($result);
    	$result = json_decode($result,true);
    	//error_log(print_r($result,1));
    	dump($result);
    	if($result['code']==200){
    		$this->outPut($result['data']);
    		$result = ['code' => '200', 'msg' => 'success','data'=>$result['data']];
    
    	}else{
    		//$this->outPutError($result);
    		$result = ['code' => '400', 'msg' => $result['msg']];
    	}
    	return $result;
    }
}
