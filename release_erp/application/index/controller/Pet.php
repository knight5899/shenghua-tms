<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Request;

/***
 * 宠物 供应链
 *
 **/
class Pet extends Base
{
    public function showPetNewsManage(){
        $post = Request::instance()->param();

        $post['page'] = $this->page();
        $post['size'] = $this->_page_size;
        $post['choose_company_id'] = session('user')['company_id'];
        $post['website_uuid'] = session('website_uuid');

        $result = $this->callSoaErp('post','/ota_article/getArticleList',$post);

        $this->getPageParams($result);

        return $this->fetch('pet_news_manage');
    }

    public function showPetNewsAdd(){

        $data['status'] = 1;
        $data['choose_company_id'] = session('user')['company_id'];
        $data['website_uuid'] = session('website_uuid');
        $result = $this->callSoaErp('post','/ota_article/getArticleTypeList',$data);
        $this->assign('result',$result['data']);

        return $this->fetch('pet_news_add');
    }

    public function addPetNewsAjax(){

//        $title = input("title");
//        $ota_article_type_uuid = input("ota_article_type_uuid");
        $choose_company_id = session('user')['company_id'];
//        $status = input("status");
        $user_id = session('user')['user_id'];
//        $content = input("content");
//        $start_time = input("start_time");
//        $end_time = input("end_time");
//        $keywords = input("keywords");
//        $description = input("description");
        $website_uuid = session('website_uuid');
//
//        $data = [
//            "title"=>$title,
//            "ota_article_type_uuid"=>$ota_article_type_uuid,
//            'choose_company_id'=>$choose_company_id,
//            'user_id'=>$user_id,
//            'status'=>$status,
//            "content"=>$content,
//            'start_time'=>$start_time,
//            'end_time'=>$end_time,
//            'keywords'=>$keywords,
//            'description'=>$description,
//            'website_uuid'=>$website_uuid
//        ];
        $param = Request::instance()->param();
        $param['choose_company_id'] = $choose_company_id;
        $param['user_id'] = $user_id;
        $param['website_uuid'] = $website_uuid;

        return $this->callSoaErp('post','/ota_article/addOtaArticle',$param);

    }

    public function editPetNewsAjax(){

        $choose_company_id = session('user')['company_id'];
        $user_id = session('user')['user_id'];
//        $ota_article_id = input("ota_article_id");
//        $title = input("title");
//        $pid = input("pid");
//        $content = input("content");
//        $start_time = input("start_time");
//        $end_time = input("end_time");
//        $keywords = input("keywords");
//        $description = input("description");
//        $status = input("status");
//        $data = [
//            "ota_article_id" => $ota_article_id,
//            "title"=>$title,
//            "pid"=>$pid,
//            'choose_company_id'=>$choose_company_id,
//            'user_id'=>$user_id,
//            'status'=>$status,
//            "content"=>$content,
//            'start_time'=>$start_time,
//            'end_time'=>$end_time,
//            'keywords'=>$keywords,
//            'description'=>$description,
//        ];
        $param = Request::instance()->param();
        $param['choose_company_id'] = $choose_company_id;
        $param['user_id'] = $user_id;
        return $this->callSoaErp('post','/ota_article/updateOtaArticle',$param);

    }



    
    //显示文章分类页面
    public function showPetArticleTypeManage(){

        $post = Request::instance()->param();

        $post['page'] = $this->page();
        $post['size'] = $this->_page_size;

        $result = $this->callSoaErp('post','/pet/getPetArticleType',$post);
	
        $this->getPageParams($result);
		
        return $this->fetch('pet_article_type_manage');
    }

    
    //显示文章分类新增页面
    
    public function showPetArticleTypeAdd(){


       // $result = $this->callSoaErp('post','/ota_article/getArticleTypeList',$data);
		//获取分类
		$pet_article_type_data['status'] =1;
		$result = $this->callSoaErp('post','/pet/getPetArticleType',$pet_article_type_data);
   		$list = $result['data'];
	
		//获取当前数据
		$pet_article_type_id = input('pet_article_type_id');
		if(is_numeric($pet_article_type_id)){
			$article_type_data['pet_article_type_id'] = $pet_article_type_id;
			$article_type_result = $this->callSoaErp('post','/pet/getPetArticleType',$article_type_data);
			
			$this->assign('pet_article_type_result',$article_type_result['data'][0]);
			
		}
   		

   		$this->assign('result',$result['data']);
        return $this->fetch('pet_article_type_add');
    }


    /**
     * 新增或修改 文章分类AJAX
     */
    public function addPetArticleTypeAjax(){


        $param = Request::instance()->param();

		if(is_numeric($param['pet_article_type_id'])){
			
			return $this->callSoaErp('post','/pet/updatePetArticleTypeByPetArticleTypeId',$param);
		}else{
			return $this->callSoaErp('post','/pet/addPetArticleType',$param);
		}

    }

    //显示文章页面
    public function showPetArticleManage(){
    
    	$post = Request::instance()->param();
    
    	$post['page'] = $this->page();
    	$post['size'] = $this->_page_size;
    
    	$result = $this->callSoaErp('post','/pet/getPetArticle',$post);
		
    	$this->getPageParams($result);
    
    	
    	//获取分类
    	$article_type_data['status'] = 1;
    	$article_type_result = $this->callSoaErp('post','/pet/getPetArticleType',$article_type_data);
    	
    	$this->assign('article_type_result',$article_type_result['data']);
    	
    	return $this->fetch('pet_article_manage');
    }
    //显示文章新增页面
    
    public function showPetArticleAdd(){
    
    
    	// $result = $this->callSoaErp('post','/ota_article/getArticleTypeList',$data);
    	//获取分类
    	$pet_article_type_data['status'] =1;
    	$result = $this->callSoaErp('post','/pet/getPetArticleType',$pet_article_type_data);
    	$list = $result['data'];
    
    	//获取当前数据
    	$pet_article_id = input('pet_article_id');
    	if(is_numeric($pet_article_id)){
    		$article_data['pet_article_id'] = $pet_article_id;
    		$article_result = $this->callSoaErp('post','/pet/getPetArticle',$article_type_data);
    			
    		$this->assign('pet_article_result',$article_result['data'][0]);
    			
    	}
    	 
    
    	$this->assign('result',$result['data']);
    	return $this->fetch('pet_article_add');
    }
    /**
     * 新增或修改文章AJAX
     */
    public function addPetArticleAjax(){
    
    
    	$param = Request::instance()->param();

    	if(is_numeric($param['pet_article_id'])){

    		return $this->callSoaErp('post','/pet/updatePetArticleByPetArticleId',$param);
    	}else{
    		return $this->callSoaErp('post','/pet/addPetArticle',$param);
    	}
    
    }
}