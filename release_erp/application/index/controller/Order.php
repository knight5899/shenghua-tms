<?php
/**
 * Created by PhpStorm.
 * User: 胡
 * Date: 20190/02/19
 * Time: 16:40
 */

namespace app\index\controller;

use app\common\help\Contents;
use think\Validate;
use \Underscore\Types\Arrays;
use think\Cache;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class Order extends Base
{


    public function subFinance()
    {
        $params = Request::instance()->param();

        $projectResult = $this->callSoaErp('post', '/order/subFinance', $params);

        return $projectResult;
    }

    public function addIncomeAjax()
    {
        $params = Request::instance()->param();
        $projectResult = $this->callSoaErp('post', '/order/addIncome', $params);

        return $projectResult;

    }

    public function getsupplierAjax()
    {
        $supplierParams = Request::instance()->param();

        $supplierParams['status'] = 1;
        $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);


        $data = [];
        foreach ($supplierResult['data'] as $k => $v) {
            $data[] = ['value' => $v['supplier_uuid'], 'name' => $v['supplier_name']];
        }

        $data = ['code' => 0, 'data' => $data];
        return json_encode($data, 256);


    }


    public function getProjectAjax()
    {
        $params = Request::instance()->param();
        $projectResult = $this->callSoaErp('post', '/source/getProject', $params);
        $sort = array_column($projectResult['data'], 'orders_count');
        array_multisort($sort, SORT_DESC, $projectResult['data']);
        $data = [];
        foreach ($projectResult['data'] as $k => $v) {
            $data[] = ['value' => $v['project_id'], 'name' => $v['project_name']];
        }

        $data = ['code' => 0, 'data' => $data];
        return json_encode($data, 256);
    }

    public function addOrderIncome()
    {
        $params = Request::instance()->param();

        $project_id = input('project_id');
        $orders_id = input('orders_id');

        //获取所有城市
        //获取所有项目

        $productData['status'] = 1;


        // $this->assign('projectResult', $projectResult['data']);

        //如果选择了项目
        if (is_numeric($project_id)) {
            //获取所有的收发货信息
            $project_data['project_id'] = $project_id;
            $project_data['status'] = 1;
            $sendResult = $this->callSoaErp('post', '/source/getSendGoods', $project_data);


            $this->assign('sendResult', $sendResult['data']);
            $acceptResult = $this->callSoaErp('post', '/source/getAcceptGoods', $project_data);

            $this->assign('acceptResult', $acceptResult['data']);
            $this->assign('result', $orderResult['data'][0]);
            //获取所有省
            $province_data = Help::getCity(0, 1);
            //获取所有市
            $city_data = Help::getCity(0, 2);
            //获取所有区
            $area_data = Help::getCity(0, 3);
            $this->assign('provinceResult', $province_data);
            $this->assign('cityResult', $city_data);
            $this->assign('areaResult', $area_data);
            //获取货物
            $goodsData['project_id'] = $project_id;
            $goodsResult = $this->callSoaErp('post', '/source/getGoods', $goodsData);
            $this->assign('goodsResult', $goodsResult['data']);
        }
        if (is_numeric($orders_id)) {
            $orderData['orders_id'] = $orders_id;
            $orderResult = $this->callSoaErp('post', '/order/getOrder', $orderData);

//            var_dump($orderResult);exit;
            $this->assign('result', $orderResult['data'][0]);

            $orderResult = $this->callSoaErp('post', '/Order/getOrderIncome', $orderData);
            $remarkData = ['orders_id' => $params['orders_id'], 'type' => 3];
            $order_remark_result = $this->callSoaErp('post', '/Order/getOrderRemark', $remarkData);

            $this->assign('income_remark', $order_remark_result['data'][0]['content']);

            $this->assign('orders_income', $orderResult['data']);
        }


        $allIncome = $this->callSoaErp('post', '/Cost/getAll', []);

        $this->assign('all_income', $allIncome['data']);

        return $this->fetch('order_income_add');
    }

    public function downloadIncomeManage()
    {
        $params = Request::instance()->param();
        $data = $params;
        $projectData = [];
        $projectResult = $this->callSoaErp('post', '/source/getProject', $projectData);
        $this->assign('projectResult', $projectResult['data']);

        $data['page_size'] = $this->_page_size;

        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        }
        if (is_numeric($params['is_invoice'])) {
            $data['is_invoice'] = $params['is_invoice'];
        }

        if (!empty($params['accept_name'])) {
            $data['accept_name'] = $params['accept_name'];
        }
        if (!empty($params['accept_cellphone'])) {
            $data['accept_cellphone'] = $params['accept_cellphone'];
        }
        $data['order_status'] = ['>', 6];

        $result = $this->callSoaErp('post', '/order/getOrderIncomeList', $data);
        //echo json_encode($result,256);exit();
        $result = Help::getOrder($result);

        $bargain_price_all = 0;
        $real_money_all = 0;
        $estimated_count_all = 0;

        foreach ($result['data'] as $k => $v) {
            $bargain_price_all += $v['bargain_price'];
            $real_money_all += $v['real_money'];
            foreach ($v['orders_goods_info'] as $k1 => $v1)
                $estimated_count_all += $v1['estimated_count'];
        }
        $this->assign('bargain_price_all', $bargain_price_all);
        $this->assign('real_money_all', $real_money_all);
        $this->assign('estimated_count_all', $estimated_count_all);
        $put_title = ["运单ID", "项目名称", "运单编号", "客户单号", "支付方式", "计价方式", "预计收入", "实际收入", "运输类型", "货物名称", "下单包装单位", "规格/型号", "重量", "体积", "长", "宽", "高", "下单件数", "下单数量", "下单重量", "下单体积", "计费件数", "计费数量", "计费包装单位", "计费重量", "计费体积", "发货信息", "发货方", "发货电话", "收货信息", "收货方", "收货电话", "代收货款", "代收状态", "货值", "运单时间", "送达时间", "备注", "创建人", "创建时间", "核算状态", "回单状态", "是否异常", "运费", "提货费", "送货费", "卸货费", "其他费", "总运费"];
        $put_data = [];
        foreach ($result['data'] as $k => $v) {
            $tmp = [];
            $tmp1 = ['goods_name' => ' ',
                'goods_packaging_type_name' => ' ',
                'goods_specification' => ' ',
                'goods_weight' => ' ',
                'goods_volume' => ' ',
                'goods_length' => ' ',
                'goods_width' => ' ',
                'goods_height' => ' ',
                'estimated_count' => ' ',
                'estimated_pack_count' => ' ',
                'estimated_weight' => ' ',
                'estimated_volume' => ' ',
                'realy_count' => ' ',
                'realy_pack_count' => ' ',
                'realy_pack_unit' => ' ',
                'realy_weight' => ' ',
                'realy_volume' => ' '
            ];
            $tmp[] = $v['orders_id'];
            $tmp[] = $v['project_name'];
            $tmp[] = $v['orders_number'];
            $tmp[] = $v['customer_order_number'];
            $tmp[] = $this->baseConfig['order']['pay_type_name'][$v['pay_type']];
            $tmp[] = ($v['bargain_type'] == 2) ? "单票" : "合同";
            $tmp[] = $v['bargain_price'];
            $tmp[] = $v['real_money'];
            $tmp[] = $this->baseConfig['order']['transportation_type_name'][$v['transportation_type']];
            foreach ($v['orders_goods_info'] as $k1 => $v1) {
                $tmp1['goods_name'] .= $v1['goods_name'] . PHP_EOL;
                $tmp1['goods_packaging_type_name'] .= $v1['goods_packaging_type_name'] . PHP_EOL;
                $tmp1['goods_specification'] .= $v1['goods_specification'] . '/' . $v1['model'] . PHP_EOL;
                $tmp1['goods_weight'] .= $v1['goods_weight'] . PHP_EOL;
                $tmp1['goods_volume'] .= $v1['goods_volume'] . PHP_EOL;
                $tmp1['goods_length'] .= $v1['goods_length'] . PHP_EOL;
                $tmp1['goods_width'] .= $v1['goods_width'] . PHP_EOL;
                $tmp1['goods_height'] .= $v1['goods_height'] . PHP_EOL;
                $tmp1['estimated_count'] .= $v1['estimated_count'] . PHP_EOL;
                $tmp1['estimated_pack_count'] .= $v1['estimated_pack_count'] . PHP_EOL;
                $tmp1['estimated_weight'] .= $v1['estimated_weight'] . PHP_EOL;
                $tmp1['estimated_volume'] .= $v1['estimated_volume'] . PHP_EOL;
                $tmp1['realy_count'] .= $v1['realy_count'] . PHP_EOL;
                $tmp1['realy_pack_count'] .= $v1['realy_pack_count'] . PHP_EOL;
                $tmp1['realy_pack_unit'] .= $this->baseConfig['order']['goods_pack'][$v1['realy_pack_unit']] . PHP_EOL;
                $tmp1['realy_weight'] .= $v1['realy_weight'] . PHP_EOL;
                $tmp1['realy_volume'] .= $v1['realy_volume'] . PHP_EOL;
            }
            $tmp1 = array_values($tmp1);
            $tmp = array_merge($tmp, $tmp1);
            $tmp[] = $v['send_province_name'] . $v['send_city_name'] . $v['send_area_name'];
            $tmp[] = $v['send_name'];
            $tmp[] = $v['send_cellphone'];
            $tmp[] = $v['accept_province_name'] . $v['accept_city_name'] . $v['accept_area_name'];
            $tmp[] = $v['accept_name'];
            $tmp[] = $v['accept_cellphone'];
            $tmp[] = $v['replacement_prive'];
            $tmp[] = ($v['replacement_prive_type'] == 1) ? "已收" : "未收";
            $tmp[] = $v['insurance_goods'];
            $tmp[] = date("Y-m-d", $v['pickup_time']);
            $tmp[] = date("Y-m-d", $v['send_time']);
            $tmp[] = $v['remark'];
            $tmp[] = $v['create_user_name'];
            $tmp[] = date("Y-m-d", $v['create_time']);
            $tmp[] = $this->baseConfig['order']['verify_status'][$v['verify_status']];
            $tmp[] = $v['order_status'] > 6 ? "已上传" : "未上传";
            $tmp[] = $v['abnormal_count'] > 0 ? "是" : "否";
            $tmp[] = $v['yun_money'];
            $tmp[] = $v['ti_money'];
            $tmp[] = $v['song_money'];
            $tmp[] = $v['xie_money'];
            $tmp[] = $v['other_money'];
            $tmp[] = $v['money'];


            $put_data[] = $tmp;
        }
        //echo json_encode($put_data,256);exit();

        put_csv($put_data, $put_title);
        echo json_encode($result);
        exit();
        $this->getPageParams($result);
        return $this->fetch('income_accounting');
    }

    public function incomeAccountingManage()
    {
        $params = Request::instance()->param();
        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
        $data = $params;
        $projectData = [];
        $projectResult = $this->callSoaErp('post', '/source/getProject', $projectData);
        $sort = array_column($projectResult['data'], 'orders_count');
        array_multisort($sort, SORT_DESC, $projectResult['data']);
        $this->assign('projectResult', $projectResult['data']);
        $data['page'] = $this->page();
        $data['page_size'] = $params['limit'] ? $params['limit'] : $this->_page_size;

        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');

        $data['status'] = 1;

        if (is_numeric($params['verify_status'])) {
            $data['verify_status'] = $params['verify_status'];
        }
        if (is_numeric($params['choose_company_id'])) {
            $data['choose_company_id'] = $params['choose_company_id'];
        } else {
            $data['choose_company_id'] = session('user')['company_id'];
        }
        if (!empty($params['accept_name'])) {
            $data['accept_name'] = $params['accept_name'];
        }
        if (!empty($params['accept_cellphone'])) {
            $data['accept_cellphone'] = $params['accept_cellphone'];
        }
        if (!empty($params['total_fee'])) {
            $data['total_fee'] = $params['total_fee'];
        }
        if (isset($data['download'])) {
            $data['page_size'] = Contents::PAGE_SIZE_MAX;
        }
        $result = $this->callSoaErp('post', '/order/getOrderIncomeList', $data);


        //echo json_encode($result,256);exit();
        $result = ['code' => 200, 'data' => $result['data']['list'], 'count' => $result['data']['count']];
        $result = Help::getOrder($result);

        $bargain_price_all = 0;
        $real_money_all = 0;
        $estimated_count_all = 0;

        foreach ($result['data'] as $k => $v) {
            $result['data'][$k]['order_index'] = $k + 1;
            $bargain_price_all += $v['bargain_price'];
            $real_money_all += $v['real_money'];
            foreach ($v['orders_goods_info'] as $k1 => $v1)
                $estimated_count_all += $v1['estimated_count'];
        }
        $this->assign('bargain_price_all', $bargain_price_all);
        $this->assign('real_money_all', $real_money_all);
        $this->assign('estimated_count_all', $estimated_count_all);

        $result['totalRow'] = ['bargain_price' => $bargain_price_all, 'real_money' => $real_money_all, 'estimated_count' => $estimated_count_all];

        $this->getPageParams($result);

        if ($this->request->isAjax()) {

            return json($result);
        }
        if (isset($params['download'])) {
            $name = "收入核算导出";
            $xAxis = [];
            $title = "收入核算导出";
            $global_index = 1;


            $header = array('序号', '运单编号', '发货方', '运单日期', '货物名称', '件数', '数量', '体积', '重量','发运件数', '发运数量', '发运体积', '发运重量', '收货方名称', '收货地址', '联系电话', '收货人', '到站', '创建日期',
                '客户订单号', '要求到货日期', '发货地址', '发站', '货物价值', '交货方式', '运费', '提货费', '送货费', '卸货费', '打包费', '收入核算备注');

            $new_data = array_map(function ($old_data) {
                $tmp = [];
                $goods_inf = $old_data['orders_goods_info'];
                $tmp[] = $old_data['order_index'];
                $tmp[] = $old_data['orders_number'];
                $tmp[] = $old_data['send_goods_company'];
                $tmp[] = $old_data['pickup_time'] ? date('Y-m-d', $old_data['pickup_time']) : "";
                $tmp[] = implode("/", array_column($goods_inf, 'goods_name'));
                $tmp[] = array_sum(array_column($goods_inf, 'realy_count'));
                $tmp[] = array_sum(array_column($goods_inf, 'realy_pack_count'));
                $tmp[] = array_sum(array_column($goods_inf, 'realy_volume'));
                $tmp[] = array_sum(array_column($goods_inf, 'realy_weight'));

                $tmp[]=$old_data['shipment_count'];
                $tmp[]=$old_data['shipment_pack_count'];
                $tmp[]=$old_data['shipment_volume'];
                $tmp[]=$old_data['shipment_weight'];


                $tmp[] = $old_data['accept_goods_company'];
                $tmp[] = $old_data['accept_address'];
                $tmp[] = $old_data['accept_cellphone'];
                $tmp[] = $old_data['accept_name'];
                $tmp[] = $old_data['accept_location_name'];
                $tmp[] = $old_data['create_time'] ? date('Y-m-d', $old_data['create_time']) : "";
                $tmp[] = $old_data['customer_order_number'];
                $tmp[] = $old_data['send_time'] ? date('Y-m-d', $old_data['send_time']) : "";
                $tmp[] = $old_data['send_address'];
                $tmp[] = $old_data['send_location_name'];
                $tmp[] = $old_data['insurance_goods'];
                $tmp[] = ($old_data['delivery_method'] == 2) ? "自提" : "送货";
                $tmp[] = $old_data['yun_money'];
                $tmp[] = $old_data['ti_money'];
                $tmp[] = $old_data['song_money'];
                $tmp[] = $old_data['xie_money'];
                $tmp[] = $old_data['pack_money'];
                $tmp[] = isset($old_data['income_remark']['content']) ? $old_data['income_remark']['content'] : "";

                return $tmp;
            }, $result['data']);
            $tmp = array_fill(0, count($header), " ");
            $tmp[0] = "合计";
            $tmp[5] = array_sum(array_column($new_data, 5));
            $tmp[6] = array_sum(array_column($new_data, 6));
            $tmp[7] = array_sum(array_column($new_data, 7));
            $tmp[8] = array_sum(array_column($new_data, 8));

            $tmp[9] = array_sum(array_column($new_data, 9));
            $tmp[10] = array_sum(array_column($new_data, 10));
            $tmp[11] = array_sum(array_column($new_data, 11));
            $tmp[12] = array_sum(array_column($new_data, 12));


            $tmp[25] = array_sum(array_column($new_data, 25));
            $tmp[26] = array_sum(array_column($new_data, 26));
            $tmp[27] = array_sum(array_column($new_data, 27));
            $tmp[28] = array_sum(array_column($new_data, 28));
            $tmp[29] = array_sum(array_column($new_data, 29));
            $new_data[] = $tmp;
            // echo json_encode($new_data,256);exit();
            $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name);

            return $excel;
        }

        return $this->fetch('income_accounting');
    }


    public function addReceiptAbnormal()
    {
        $params = Request::instance()->param();
        $orders_id = $params['orders_id'];

        $rule = ['orders_id' => "require|number"];
        $v = new Validate($rule);


        $data = [
            'page' => $this->page(),
            'page_size' => $this->_page_size,
            //'orders_id_is_like'=>1,
            'orders_id' => $orders_id

        ];
        if (!$v->check($data)) {
            $this->error($v->getError());

        }


        $this->assign('orderStatus', Contents::orderStatus());
        $result = $this->callSoaErp('post', '/order/getOrder', $data);

        $result = Help::getOrder($result);
        $this->getPageParams($result);
        return $this->fetch('add_receipt_abnormal');
    }

    //获取异常订单数据
    public function orderAbnormalData()
    {

        $params = Request::instance()->param();

        $data = [
            'page' => $this->page(),
            'page_size' => $this->_page_size,
        ];
        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        }
        if (!empty($params['accept_name'])) {
            $data['accept_name'] = $params['accept_name'];
        }
        if (!empty($params['accept_cellphone'])) {
            $data['accept_cellphone'] = $params['accept_cellphone'];
        }
        if (!empty($params['keyword'])) {
            $data['orders_number'] = $params['keyword'];
        }
        //$data['abnormal_status'] = 0;

        $result = $this->callSoaErp('post', '/order/getOrder', $data);
        $result1 = [];

        //var_dump($result['data']['list']);

        foreach ($result['data']['list'] as $k => $v) {
            $result1[] = ['value' => $v['orders_id'], 'name' => $v['orders_number'], 'send_name' => $v['send_name'], 'transport_number' => $v['transport_number']];
        }
        return json(['data' => $result1, 'code' => 0]);
    }

//ajax添加异常数据
    public function OrderAbnormalAdd()
    {
        $params = Request::instance()->param();
        $projectResult = $this->callSoaErp('post', '/order/OrderAbnormalAdd', $params);
        return $projectResult;
    }

    //添加异常数据

    private function abnormalRecurrence($dat, $pid = 0)
    {
        $data = [];

        foreach ($dat as $k => $v) {
            $tmp = [];

            if ($v['abnormal_type_pid'] == $pid) {
                $tmp['name'] = $v['abnormal_type_name'];
                $tmp['id'] = $v['abnormal_type_id'];
                $tmp['pid'] = $v['abnormal_type_pid'];
                //echo json_encode($tmp,265);exit();
                $tmp['child'] = $this->abnormalRecurrence($dat, $v['abnormal_type_id']);
                $data[] = $tmp;
            }


        }
        return $data;

    }

    public function getAbnormalTypeAjax()
    {
        $params = Request::instance()->param();
        $ab_type = $this->callSoaErp('post', '/order/getAbnormalType', ['keyword' => $params['keyword']]);


        $new_data = array_map(function ($item) {

            return ['value' => $item['abnormal_type_id'], 'name' => $item['abnormal_type_name']];

        }, $ab_type['data']['list']);

        return json_encode(['code' => 0, 'data' => $new_data]);
        exit();

    }

    public function showOrderAbnormalAdd()
    {
        $params = Request::instance()->param();


        $ab_type = $this->callSoaErp('post', '/order/getAbnormalType');


        $ab_type_list = $this->abnormalRecurrence($ab_type['data']['list']);


        $this->assign('abnormalType', $ab_type_list);

        if (isset($params['abnormal_id'])) {


            $data['abnormal_id'] = $params['abnormal_id'];


            $result = $this->callSoaErp('post', '/order/getOrderAbnormal', $data);
            // var_dump($result);exit();
            $this->getPageParams($result);
        }
        if ($params['small'])
            return $this->fetch('order_abnormal_add_small');
        else
            return $this->fetch('order_abnormal_add');
    }



	//展示异常数据列表
    public function showOrderAbnormalManage()
    {
        //读取所有的项目

        $params = Request::instance()->param();

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }


        $supplierResult = "";
        if (!empty($params['supplier_uuid'])) {
            $supplierParams['supplier_uuid'] = $params['supplier_uuid'];

            $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);


        }
        $this->assign('supplierResult', $supplierResult['data']);
        $this->assign('comapnyResult', $comapnyResult);
        $multi_project_id = input('multi_project_id');
        if (!empty($multi_project_id))
            $projectResult = $this->callSoaErp('post', '/source/getProject', ['multi_project_id' => $multi_project_id]);
        $this->assign('projectResult', $projectResult['data']);

        //$projectResult = $this->callSoaErp('post', '/source/getProject', $data);
        // $this->assign('projectResult', $projectResult['data']);
        if (!empty($params['multi_abnormal_type_id']))
            $ab_type = $this->callSoaErp('post', '/order/getAbnormalType', $params);

        $ab_type_list = [];
        foreach ($ab_type['data']['list'] as $k => $v) {

            $ab_type_list[$v['abnormal_type_id']] = $v['abnormal_type_name'];
        }


        //  echo json_encode($ab_type_list);exit();

        $this->assign('abnormalTypeResult', $ab_type['data']['list']);
        $data = $params;
        $data ['page'] = $this->page();
        $data ['page_size'] = $params['limit'] ? $params['limit'] : $this->_page_size;

        $data['orders_number'] = $params['orders_number'];
        $data['solve_way'] = $params['solve_way'];

        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $data['status'] = 1;

        if (is_numeric($params['choose_company_id'])) {
            $data['choose_company_id'] = $params['choose_company_id'];
        } else {
            $data['choose_company_id'] = session('user')['company_id'];
        }
        if (!empty($params['accept_name'])) {
            $data['accept_name'] = $params['accept_name'];
        }
        if (!empty($params['accept_cellphone'])) {
            $data['accept_cellphone'] = $params['accept_cellphone'];
        }
        $data['order_status'] = ['>', 6];
        if (isset($params['download'])) {
            $data['page_size'] = Contents::PAGE_SIZE_MAX;
        }

        $result = $this->callSoaErp('post', '/order/getOrderAbnormal', $data);


        //echo json_encode($result);exit();
        $result = Help::getOrder($result);


        $this->getPageParams($result);


        if ($this->request->isAjax()) {
            return json(['code' => 200, 'count' => $result['data']['count'], 'data' => $result['data']['list']]);
            //return  json( array_merge($result['data'],['code'=>200]) );
        }


        if (isset($params['download'])) {
            $name = "异常导出";
            $xAxis = [];
            $title = "异常导出";
            $header = array('序号', '发货方', '运单编号', '货物名称', '件数', '数量', '重量', '体积','发运件数', '发运数量', '发运重量', '发运体积', '收货方名称', '收货方地址', '联系电话', '收货人', '承运商', '理赔事件描述', '责任方', '异常类型', '是否产生货损', '货损数量', '退货处理方式', '理赔处理方式', '理赔金额(发货方)', '理赔金额(承运商)', '运单日期', '实际收货日期', '客户编号', '异常编号', '包装单位');
            $new_data = array_map(function ($old_data) {
                $tmp = [];
                $goods_inf = $old_data['goods_info'];
                $orders_info = $old_data['order_info'];
                $tmp[]=$old_data['index'];
                $tmp[]=$orders_info['send_goods_company'];
                $tmp[]=$old_data['orders_number'];
                $tmp[] = implode("/", array_column($goods_inf, 'goods_name'));
                $tmp[] = array_sum(array_column($goods_inf, 'estimated_count'));
                $tmp[] = array_sum(array_column($goods_inf, 'estimated_pack_count'));
                $tmp[] = array_sum(array_column($goods_inf, 'estimated_weight'));
                $tmp[] = array_sum(array_column($goods_inf, 'estimated_volume'));

                $tmp[]=$old_data['shipment_count'];
                $tmp[]=$old_data['shipment_pack_count'];
                $tmp[]=$old_data['shipment_weight'];
                $tmp[]=$old_data['shipment_volume'];

                $tmp[] = $orders_info['accept_goods_company'];
                $tmp[] = $orders_info['accept_address'];
                $tmp[] = $orders_info['accept_cellphone'];
                $tmp[] = $orders_info['accept_name'];
                $tmp[] = $old_data['supplier_name'];
                $tmp[] = $old_data['indemnity_event_description'];
                $tmp[] = $this->baseConfig['abnormal']['responsible_party'][$old_data['responsible_party']];
                $tmp[] = $old_data['abnormal_type_name'];
                $tmp[] = $this->baseConfig['abnormal']['damaged'][$old_data['damaged']];
                $tmp[] = $old_data['abnormal_count'];
                $tmp[] = $this->baseConfig['abnormal']['return_goods_type'][$old_data['return_goods_type']];
                $tmp[] = $this->baseConfig['abnormal']['indemnity_type'][$old_data['indemnity_type']];
                $tmp[] = $old_data['abnormal_money'];
                $tmp[] = $old_data['supplier_abnormal_money'];
                $tmp[] = date('Y-m-d',$orders_info['pickup_time']);
                $tmp[] =$orders_info['true_time']? date('Y-m-d',$orders_info['true_time']):"";
                $tmp[] = $orders_info['customer_order_number'];
                $tmp[] = $old_data['abnormal_number'];
                $tmp[] = implode("/", array_map(function ($e) {
                    return $this->baseConfig['order']['goods_pack'][$e['estimated_pack_unit']];
                }, $goods_inf));


                return $tmp;
            }, $result['data']['list']);

            $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name);

            return $excel;


        }
        return $this->fetch('order_abnormal_manage');


    }

    //审查表
    public function orderAbnormalReview(){
        $params = Request::instance()->param();
        $Result = $this->callSoaErp('post', '/order/getAbnormalReview', $params);

        $this->assign('abnormalResult', $Result['data']);
        return $this->fetch('order_abnormal_review');
    } //承运商赔款明细
    public function orderAbnormalSupplierReview(){
        $params = Request::instance()->param();
        $Result = $this->callSoaErp('post', '/order/getAbnormalReview', $params);

        $this->assign('abnormalResult', $Result['data']);
        return $this->fetch('order_abnormal_supplier_review');
    }


    //回单管理
    public function showOrderReceiptManage()
    {
        //读取所有的项目


        $params = Request::instance()->param();
        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
        $projectResult = $this->callSoaErp('post', '/source/getProject', $data);

        $sort = array_column($projectResult['data'], 'orders_count');
        array_multisort($sort, SORT_DESC, $projectResult['data']);
        $this->assign('projectResult', $projectResult['data']);

        //获取所有承运商
        $supplierParams['status'] = 1;
        $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);
        $this->assign('supplierResult', $supplierResult['data']);
        $data = $params;
        $data['page'] = $this->page();
        $data['page_size'] = $params['limit'] ? $params['limit'] : $this->_page_size;

        $data['orders_number'] = $params['orders_number'];
        $data['receipt_status'] = $params['receipt_status'];

        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }

        $data['status'] = 1;

        if (!empty($params['accept_name'])) {
            $data['accept_name'] = $params['accept_name'];
        }
        if (!empty($params['supplier_uuid'])) {
            $data['supplier_uuid'] = $params['supplier_uuid'];
        }
        if (!empty($params['shipment_number'])) {
            $data['shipment_number'] = $params['shipment_number'];
        }
        if (!empty($params['accept_cellphone'])) {
            $data['accept_cellphone'] = $params['accept_cellphone'];
        }
        if (is_numeric($params['choose_company_id'])) {
            $data['choose_company_id'] = $params['choose_company_id'];
        } else {
            $data['choose_company_id'] = session('user')['company_id'];
        }
        $data['order_status'] = ['>', 6];

        if (isset($params['download'])) {
            $data['page_size'] = Contents::PAGE_SIZE_MAX;
        }

        $result = $this->callSoaErp('post', '/order/getOrderReceipt', $data);
        foreach ($result['data']['list'] as $k => $v) {
            $result['data']['list'][$k]['index'] = $k + 1;
            $params['orders_number'] = $v['orders_number'];
            $goods_inf = $v['orders_goods_info'];

            $result['data']['list'][$k]['xdjs']= array_sum(array_column($goods_inf, 'estimated_count'));
            $result['data']['list'][$k]['xdsl']= array_sum(array_column($goods_inf, 'estimated_pack_count'));
            $result['data']['list'][$k]['xdzl']= array_sum(array_column($goods_inf, 'estimated_weight'));
            $result['data']['list'][$k]['xdtj']= array_sum(array_column($goods_inf, 'estimated_volume'));
        }


        $result = ['code' => 200, 'data' => $result['data']['list'], 'count' => $result['data']['count']];


        $result = Help::getOrder($result);
        if ($this->request->isAjax()) {

            return json($result);
        }

        if (isset($params['download'])) {

            $name = "回单导出";
            $xAxis = [];
            $title = "回单导出";
            $header = array('序号', '回单状态', '运单日期', '收货方','收货方地址', '货物名称', '件数','数量','重量','体积','发运件数','发运数量','发运重量','发运体积', '承运商', '运单编号', '发货方', '发站', '到站', '是否异常', '发运时间', '专线编号', '客户编号', '回单份数', '包装', '收回日期', '操作员', '送回日期', '操作员');
            $new_data = array_map(function ($old_data) {
                $tmp = [];
                $goods_inf = $old_data['orders_goods_info'];
                $shipment_inf = null;
                if (count($old_data['shipment_info']) > 0)
                    $shipment_inf = $old_data['shipment_info'][0];
                $tmp[] = $old_data['index'];
                $tmp[] = $this->baseConfig['order']['receipt_status'][$old_data['receipt_status']];
                $tmp[] = $old_data['pickup_time'] ? date('Y-m-d', $old_data['pickup_time']) : "";
                $tmp[] = $old_data['accept_goods_company'];
                $tmp[] = $old_data['accept_address'];
                $tmp[] = implode("/", array_column($goods_inf, 'goods_name'));
                $tmp[] = array_sum(array_column($goods_inf, 'estimated_count'));
                $tmp[] = array_sum(array_column($goods_inf, 'estimated_pack_count'));
                $tmp[] = array_sum(array_column($goods_inf, 'estimated_weight'));
                $tmp[] = array_sum(array_column($goods_inf, 'estimated_volume'));
                $tmp[]=$old_data['shipment_count'];
                $tmp[]=$old_data['shipment_pack_count'];
                $tmp[]=$old_data['shipment_weight'];
                $tmp[]=$old_data['shipment_volume'];


                $tmp[] = trim($old_data['supplier_name']);
                $tmp[] = $old_data['orders_number'];
                $tmp[] = $old_data['send_goods_company'];
                $tmp[] = $old_data['send_location_name'];
                $tmp[] = $old_data['accept_location_name'];
                $tmp[] = $old_data['abnormal_count'];
                $tmp[] = $shipment_inf ? date('Y-m-d', $shipment_inf['create_time']) : "";
                $tmp[] = $old_data['shipment_number'] ? $old_data['shipment_number'] : "";
                $tmp[] = $old_data['customer_order_number'] . " ";
                $tmp[] = $old_data['receipt_count'];
                $tmp[] = implode("/", array_map(function ($e) {
                    return $this->baseConfig['order']['goods_pack'][$e['estimated_pack_unit']];
                }, $goods_inf));
                $tmp[] = $old_data['receipt_accept_time'] ? date('Y-m-d', $old_data['receipt_accept_time']) : "";
                $tmp[] = $old_data['receipt_accept_user_name'];
                $tmp[] = $old_data['receipt_send_time'] ? date('Y-m-d', $old_data['receipt_send_time']) : "";
                $tmp[] = $old_data['receipt_send_user_name'];


                /*
                $tmp[] = trim($old_data['project_name']);
                $tmp[] = $old_data['customer_order_number'];
                $tmp[] = $old_data['send_location_name'];
                $tmp[] = $old_data['money'];
                $tmp[] = implode("/", array_map(function ($e) {
                    return $this->baseConfig['order']['goods_pack'][$e['estimated_pack_unit']];
                }, $goods_inf));
                //$tmp[] = array_sum(array_column($goods_inf,'estimated_pack_count'));
                //$tmp[] = array_sum(array_column($goods_inf,'estimated_count'));
                $tmp[] = array_sum(array_column($goods_inf, 'realy_count'));

                $tmp[] = implode("/", array_map(function ($e) {
                    return $this->baseConfig['order']['goods_pack'][$e['realy_pack_unit']];
                }, $goods_inf));
                $tmp[] = array_sum(array_column($goods_inf, 'realy_weight'));
                $tmp[] = array_sum(array_column($goods_inf, 'realy_volume'));
                $tmp[] = $old_data['send_address'];
                $tmp[] = $old_data['send_cellphone'];
                $tmp[] = $old_data['accept_address'];
                $tmp[] = $old_data['accept_name'];
                $tmp[] = $old_data['accept_cellphone'];
                $tmp[] = $old_data['replacement_prive'];
                $tmp[] = $old_data['send_time'] ? date('Y-m-d', $old_data['send_time']) : "";
                $tmp[] = $old_data['remark'];
                $tmp[] = $old_data['create_user_name'];
                $tmp[] = date('Y-m-d', $old_data['create_time']);
                $tmp[] = $old_data['abnormal_status'] ? "是" : "否";
*/

                return $tmp;
            }, $result['data']);

            $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name);

            return $excel;
        }


        $this->assign('data', $result['data']);
        return $this->fetch('order_receipt_manage');


    }

//导出订单

    public function exportOrder()
    {

        //读取所有的项目

        $params = Request::instance()->param();
        $data = $params;

        $projectResult = $this->callSoaErp('post', '/source/getProject', $data);
        $this->assign('projectResult', $projectResult['data']);


        // $data['page']=$this->page();
        // $data['page_size']=10;

        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        }
        if (!empty($params['accept_goods_company'])) {
            $data['accept_goods_company'] = $params['accept_goods_company'];
        }
        if (!empty($params['orders_number'])) {
            $data['orders_number'] = $params['orders_number'];
        }


        $result = $this->callSoaErp('post', '/order/getOrder', $data);


        $result = Help::getOrder($result);
        echo json_encode($result, 256);
        exit();
        $this->getPageParams($result);
        return $this->fetch('order_manage');


        $a = [[1, 2, 3], [4, 5, 6]];
        $t = ['a', 'b', 'b'];
        return put_csv($a, $t);
    }


    public function downloadorder()
    {
        $params = Request::instance()->param();
        $data = $params;

        $projectResult = $this->callSoaErp('post', '/source/getProject', $data);
        $this->assign('projectResult', $projectResult['data']);


        $data['page'] = $this->page();
        $data['page_size'] = 10;

        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        }
        if (!empty($params['accept_goods_company'])) {
            $data['accept_goods_company'] = $params['accept_goods_company'];
        }
        if (!empty($params['orders_number'])) {
            $data['orders_number'] = $params['orders_number'];
        }

        unset($data['page']);
        $data['set_page'] = 0;
        $result = $this->callSoaErp('post', '/order/getOrder', $data);
        // echo json_encode($result);exit();


        $result = Help::getOrder($result);
        // echo json_encode($result);

        $put_titile = ["运单ID", "项目名称", "运单编号", "客户单号", "支付方式", "计价方式", "收入", "运输类型", "货物名称", "下单包装单位", "下单件数", "下单数量", "下单重量", "下单体积", "计费件数", "计费数量", "计费包装单位", "计费重量", "计费体积", "发货信息", "发货方", "发货人电话", "收货信息", "收货方", "收货电话", "代收货款", "代收状态", "货值", "运单时间", "送达时间", "备注", "创建人", "创建时间", "当前进度", "是否异常", "当前进度"];
        $put_data = [];

        unset($result['data']['despatch_record']);
        unset($result['data']['despatch_print_record']);
        foreach ($result['data'] as $k => $v) {
            $goods_info = [];
            foreach ($v['orders_goods_info'] as $k1 => $v1) {
                $goods_info['goods_name'] .= $v1['goods_name'] . PHP_EOL;
                $goods_info['estimated_pack_unit'] .= $this->baseConfig['order']['goods_pack'][$v1['estimated_pack_unit']] . PHP_EOL;
                $goods_info['estimated_count'] .= $v1['estimated_count'] . PHP_EOL;
                $goods_info['estimated_pack_count'] .= $v1['estimated_pack_count'] . PHP_EOL;
                $goods_info['estimated_weight'] .= $v1['estimated_weight'] . PHP_EOL;
                $goods_info['estimated_volume'] .= $v1['estimated_volume'] . PHP_EOL;
                $goods_info['realy_count'] .= $v1['realy_count'] . PHP_EOL;
                $goods_info['realy_pack_count'] .= $v1['realy_pack_count'] . PHP_EOL;
                $goods_info['realy_pack_unit'] .= $this->baseConfig['order']['goods_pack'][$v1['realy_pack_unit']] . PHP_EOL;
                $goods_info['realy_weight'] .= $v1['realy_weight'] . PHP_EOL;
                $goods_info['realy_volume'] .= $v1['realy_volume'] . PHP_EOL;
            }


            $tmp = [$v['orders_id'], $v['project_name'], $v['orders_number'], $v['customer_order_number'], $this->baseConfig['order']['pay_type_name'][$v['pay_type']],
                ($v['bargain_type'] == 2) ? "单票" : "合同", $v['bargain_price'], $this->baseConfig['order']['transportation_type_name'][$v['transportation_type']],
                $goods_info['goods_name'], $goods_info['estimated_pack_unit'], $goods_info['estimated_count'], $goods_info['estimated_pack_count'],
                $goods_info['estimated_weight'], $goods_info['estimated_volume'], $goods_info['realy_count'], $goods_info['realy_pack_count'], $goods_info['realy_pack_unit'], $goods_info['realy_weight'], $goods_info['realy_volume']
                , $v['send_province_name'] . $v['send_city_name'] . $v['send_area_name'] . $v['send_address'], $v['send_goods_company'], $v['send_cellphone'], $v['accept_province_name'] . $v['accept_city_name'] . $v['accept_area_name'] . $v['accept_address'],
                $v['accept_goods_company'], $v['accept_cellphone'], $v['replacement_prive'], ($v['replacement_prive_type'] > 0) ? "已收" : "未收", $v['insurance_goods'],
                date('Y-m-d', $v['pickup_time']), date('Y-m-d', $v['send_time']), $v['remark'], $v['create_user_name'], date('Y-m-d', $v['create_time']),
                $this->baseConfig['order']['order_status'][$v['order_status']]['order_status_name'],
                ($v['abnormal_count'] > 0) ? "是" : "否", $this->baseConfig['order']['order_status'][$v['order_status']]['order_status_name']
            ];
            $put_data[] = $tmp;
        }
        //  echo json_encode($result['data'],256);exit();
        return put_csv($put_data, $put_titile);

    }

    /**
     *  订单显示页面
     */
    public function showOrderManage()
    {
        //读取所有的项目

        $params = Request::instance()->param();

        $data = $params;

        //$projectResult = $this->callSoaErp('post', '/source/getProject', $data);
        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        $multi_project_id = input('multi_project_id');
        if (!empty($multi_project_id))
            $projectResult = $this->callSoaErp('post', '/source/getProject', ['multi_project_id' => $multi_project_id]);


        $this->assign('projectResult', $projectResult['data']);

        //获取所有承运商
        $supplierParams['status'] = 1;
        $supplierResult = "";
        if (!empty($params['supplier_uuid'])) {
            $supplierParams['supplier_uuid'] = $params['supplier_uuid'];

            $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);


        }
        $this->assign('supplierResult', $supplierResult['data']);
        $data['page'] = $this->page();
        $data['page_size'] = $params['limit'] ? $params['limit'] : 30;
        if (isset($params['download'])) {
            $data['page_size'] = Contents::PAGE_SIZE_MAX;
        }
        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        } else
            $data['status'] = 1;

        if (!empty($params['accept_goods_company'])) {
            $data['accept_goods_company'] = $params['accept_goods_company'];
        }
        if (!empty($params['orders_number'])) {
            $data['orders_number'] = trim($params['orders_number']);
        }
        if (!empty($params['supplier_uuid'])) {
            $data['supplier_uuid'] = $params['supplier_uuid'];
        }
        if (is_numeric($params['choose_company_id'])) {
            $data['choose_company_id'] = $params['choose_company_id'];
        } else {
            $data['choose_company_id'] = session('user')['company_id'];
        }
        $result = $this->callSoaErp('post', '/order/getOrder', $data);

        $data = $result['data']['list'];

        for ($i = 0; $i < count($data); $i++) {
            if (is_numeric($data[$i]['true_time'])) {
                $data[$i]['true_time_unix'] = $data[$i]['true_time'];
                $data[$i]['true_time'] = date('Y-m-d', $data[$i]['true_time']);

            } else {
                $data[$i]['true_time'] = '&nbsp';
            }
            if (!empty($data[$i]['send_time'])) {
                $data[$i]['send_time'] = date('Y-m-d', $data[$i]['send_time']);
            } else {
                $data[$i]['send_time'] = '';
            }


            $xdjs = 0;
            $xdsl = 0;
            $xdzl = 0;
            $xdtj = 0;
            for ($j = 0; $j < count($data[$i]['orders_goods_info']); $j++) {
                $xdjs += $data[$i]['orders_goods_info'][$j]['realy_count'];
                $xdsl += $data[$i]['orders_goods_info'][$j]['realy_pack_count'];
                $xdzl += $data[$i]['orders_goods_info'][$j]['realy_weight'];
                $xdtj += $data[$i]['orders_goods_info'][$j]['realy_volume'];
            }

            $data[$i]['xdjs'] = $xdjs;
            $data[$i]['xdsl'] = $xdsl;
            $data[$i]['xdzl'] = $xdzl;
            $data[$i]['xdtj'] = $xdtj;
			$data[$i]['xdtj'] = $xdtj;
			$data[$i]['verify_status_name'] =Contents::baseConfig()['order']['verify_status'][$data[$i]['verify_status'] ];
        }
		error_log(print_r($data,1));
        if ($this->request->isAjax()) {

            return json(['code' => 200, 'data' => $data, 'count' => $result['data']['count']]);
        }


        if (isset($params['download'])) {

            $name = "运单导出";
            $xAxis = [];
            $title = "运单导出";
            $header = array('运单编号', '发货方', '运单日期', '货物名称', '件数', '数量', '体积', '重量', '发运件数', '发运数量', '发运体积', '发运重量', '收货方名称', '收货方地址', '联系电话', '收货人', '到站',
                '备注', '承运商', '专线编号', '创建日期', '发货日期', '发车日期', '客户订单号', '要求到货日期', '实际到货日期', '当前进度',
                '发运跟踪', '发货地址', '发站', '包装方式', '货物价值');
            $new_data = [];

            $filter = [
                [
                    "code" => "A",
                    "value" => 30
                ], [
                    "code" => "B",
                    "value" => 20
                ], [
                    "code" => "C",
                    "value" => 20
                ], [
                    "code" => "D",
                    "value" => 20
                ], [
                    "code" => "E",
                    "value" => 20
                ], [
                    "code" => "F",
                    "value" => 20
                ], [
                    "code" => "G",
                    "value" => 20
                ], [
                    "code" => "H",
                    "value" => 20
                ], [
                    "code" => "I",
                    "value" => 20
                ], [
                    "code" => "J",
                    "value" => 40
                ], [
                    "code" => "K",
                    "value" => 20
                ], [
                    "code" => "L",
                    "value" => 20
                ], [
                    "code" => "M",
                    "value" => 20
                ], [
                    "code" => "N",
                    "value" => 50
                ], [
                    "code" => "O",
                    "value" => 20
                ], [
                    "code" => "P",
                    "value" => 20
                ], [
                    "code" => "Q",
                    "value" => 20
                ], [
                    "code" => "R",
                    "value" => 20
                ], [
                    "code" => "S",
                    "value" => 20
                ], [
                    "code" => "T",
                    "value" => 20
                ], [
                    "code" => "U",
                    "value" => 20
                ], [
                    "code" => "V",
                    "value" => 20
                ], [
                    "code" => "W",
                    "value" => 20
                ], [
                    "code" => "X",
                    "value" => 30
                ], [
                    "code" => "Y",
                    "value" => 55
                ], [
                    "code" => "Z",
                    "value" => 20
                ], [
                    "code" => "AA",
                    "value" => 20
                ], [
                    "code" => "AB",
                    "value" => 20
                ]
            ];

            $that = $this;
            $new_data = array_map(function ($old_data) {
                $tmp = [];
                $goods_inf = $old_data['orders_goods_info'];
                $tmp[] = $old_data['orders_number'];
                $tmp[] = trim($old_data['send_goods_company']);
                $tmp[] = $old_data['pickup_time'] ? date('Y-m-d', $old_data['pickup_time']) : "";
                $tmp[] = implode("/", array_column($goods_inf, 'goods_name'));
                $tmp[] = $old_data['xdjs'];
                $tmp[] = $old_data['xdsl'];
                $tmp[] = $old_data['xdtj'];
                $tmp[] = $old_data['xdzl'];

                $tmp[]=$old_data['shipment_count'];
                $tmp[]=$old_data['shipment_pack_count'];
                $tmp[]=$old_data['shipment_volume'];
                $tmp[]=$old_data['shipment_weight'];


                $tmp[] = trim($old_data['accept_goods_company']);
                $tmp[] = $old_data['accept_address'];
                $tmp[] = $old_data['accept_cellphone'];
                $tmp[] = $old_data['accept_name'];
                $tmp[] = $old_data['accept_location_name'];
                $tmp[] = $old_data['remark'];
                $tmp[] = $old_data['supplier_name'];
                $tmp[] = $old_data['shipment_number'];
                $tmp[] = date('Y-m-d', $old_data['create_time']);

                $tmp[] = $old_data['shipment_info'][0]['shipment_time'] ? date('Y-m-d', $old_data['shipment_info'][0]['shipment_time']) : "";
                $tmp[] = $old_data['departure_time'] ? date('Y-m-d', $old_data['departure_time']) : "";
                $tmp[] = $old_data['customer_order_number'];

                $tmp[] = $old_data['send_time'];
                $tmp[] = strtotime($old_data['true_time']) ? $old_data['true_time'] : "";

                $tmp[] = $this->baseConfig['order']['order_status'][$old_data['order_status']]['order_status_name'];

                $tmp[] = implode(',', array_map(function ($e) {
                    return date('Y-m-d', $e['create_time']) . '/' . $e['create_user_name'] . '/' . $this->baseConfig['order']['order_status'][$e['order_status']]['order_status_name'] . '/' . $e['remark'];
                }, $old_data['operating_info']));
                $tmp[] = $old_data['send_address'];
                $tmp[] = $old_data['send_location_name'];
                $tmp[] = implode("/", array_map(function ($e) {
                    return $this->baseConfig['order']['goods_pack'][$e['estimated_pack_unit']];
                }, $goods_inf));
                $tmp[] = $old_data['insurance_goods'];

                return $tmp;
            }, $data);
            //echo json_encode($new_data,256);exit();
            //导出excel
            $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name, $filter);


        }


        // echo json_encode($result);exit();

        $ab_type = $this->callSoaErp('post', '/order/getAbnormalType');


        $ab_type_list = $this->abnormalRecurrence($ab_type['data']['list']);


        $this->assign('abnormalType', json_encode($ab_type_list));

        $result = Help::getOrder($result);

        $this->getPageParams($result);


        return $this->fetch('order_manage');


    }


    /**
     *  订单显示页面
     */
    public function showOrderTrackManage()
    {
        //读取所有的项目

        $params = Request::instance()->param();

        $data = $params;

        //$projectResult = $this->callSoaErp('post', '/source/getProject', $data);
        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        $multi_project_id = input('multi_project_id');
        if (!empty($multi_project_id))
            $projectResult = $this->callSoaErp('post', '/source/getProject', ['multi_project_id' => $multi_project_id]);


        $this->assign('projectResult', $projectResult['data']);

        //获取所有承运商
        $supplierParams['status'] = 1;
        $supplierResult = "";
        if (!empty($params['supplier_uuid'])) {
            $supplierParams['supplier_uuid'] = $params['supplier_uuid'];

            $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);


        }
        $this->assign('supplierResult', $supplierResult['data']);
        $data['page'] = $this->page();
        $data['page_size'] = $params['limit'] ? $params['limit'] : 30;
        if (isset($params['download'])) {
            $data['page_size'] = Contents::PAGE_SIZE_MAX;
        }
        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        } else
            $data['status'] = 1;

        if (!empty($params['accept_goods_company'])) {
            $data['accept_goods_company'] = $params['accept_goods_company'];
        }
        if (!empty($params['orders_number'])) {
            $data['orders_number'] = trim($params['orders_number']);
        }
        if (!empty($params['supplier_uuid'])) {
            $data['supplier_uuid'] = $params['supplier_uuid'];
        }
        if (is_numeric($params['choose_company_id'])) {
            $data['choose_company_id'] = $params['choose_company_id'];
        } else {
            $data['choose_company_id'] = session('user')['company_id'];
        }
        $result = $this->callSoaErp('post', '/order/getOrder', $data);

        $data = $result['data']['list'];

        for ($i = 0; $i < count($data); $i++) {



            $data[$i]['over_time']='';
            if(($data[$i]['send_time']-time()+86399)<0)
                $data[$i]['over_time']='是';
            if(is_numeric($data[$i]['true_time']))
                $data[$i]['over_time']='';



            if (is_numeric($data[$i]['true_time'])) {
                $data[$i]['true_time_unix'] = $data[$i]['true_time'];
                $data[$i]['true_time'] = date('Y-m-d', $data[$i]['true_time']);

            } else {
                $data[$i]['true_time'] = '&nbsp';
            }
            if (!empty($data[$i]['send_time'])) {
                $data[$i]['send_time'] = date('Y-m-d', $data[$i]['send_time']);
            } else {
                $data[$i]['send_time'] = '';
            }


            $xdjs = 0;
            $xdsl = 0;
            $xdzl = 0;
            $xdtj = 0;
            for ($j = 0; $j < count($data[$i]['orders_goods_info']); $j++) {
                $xdjs += $data[$i]['orders_goods_info'][$j]['realy_count'];
                $xdsl += $data[$i]['orders_goods_info'][$j]['realy_pack_count'];
                $xdzl += $data[$i]['orders_goods_info'][$j]['realy_weight'];
                $xdtj += $data[$i]['orders_goods_info'][$j]['realy_volume'];
            }

            $data[$i]['xdjs'] = $xdjs;
            $data[$i]['xdsl'] = $xdsl;
            $data[$i]['xdzl'] = $xdzl;
            $data[$i]['xdtj'] = $xdtj;

        }

        if ($this->request->isAjax()) {

            return json(['code' => 200, 'data' => $data, 'count' => $result['data']['count']]);
        }


        if (isset($params['download'])) {

            $name = "运单导出";
            $xAxis = [];
            $title = "运单导出";
            $header = array('运单编号', '发货方', '运单日期', '货物名称', '件数', '数量', '体积', '重量','发运件数', '发运数量', '发运体积', '发运重量', '收货方名称', '收货方地址', '联系电话', '收货人', '到站',
                '备注', '承运商', '专线编号', '创建日期', '发货日期', '发车日期', '客户订单号', '要求到货日期', '实际到货日期', '当前进度',
                '发运跟踪', '发货地址', '发站', '包装方式', '货物价值');
            $new_data = [];

            $filter = [
                [
                    "code" => "A",
                    "value" => 20
                ], [
                    "code" => "B",
                    "value" => 12
                ], [
                    "code" => "C",
                    "value" => 12,
                    "isDate"=>1
                ], [
                    "code" => "D",
                    "value" => 12
                ], [
                    "code" => "E",
                    "value" => 7
                ], [
                    "code" => "F",
                    "value" => 7
                ], [
                    "code" => "G",
                    "value" => 7
                ], [
                    "code" => "H",
                    "value" => 7
                ], [
                    "code" => "I",
                    "value" => 8
                ], [
                    "code" => "J",
                    "value" => 8
                ], [
                    "code" => "K",
                    "value" => 8
                ], [
                    "code" => "L",
                    "value" => 8
                ], [
                    "code" => "M",
                    "value" => 20
                ], [
                    "code" => "N",
                    "value" => 30
                ], [
                    "code" => "O",
                    "value" => 15
                ], [
                    "code" => "P",
                    "value" => 9
                ], [
                    "code" => "Q",
                    "value" => 9
                ], [
                    "code" => "R",
                    "value" => 16
                ], [
                    "code" => "S",
                    "value" => 20
                ], [
                    "code" => "T",
                    "value" => 20
                ], [
                    "code" => "U",
                    "value" => 16,
                    "isDate"=>1
                ], [
                    "code" => "V",
                    "value" => 16,
                    "isDate"=>1
                ], [
                    "code" => "W",
                    "value" => 16,
                    "isDate"=>1
                ], [
                    "code" => "X",
                    "value" => 16,
                    'format'=>'0'
                ], [
                    "code" => "Y",
                    "value" => 16,
                    "isDate"=>1
                ], [
                    "code" => "Z",
                    "value" => 15
                ], [
                    "code" => "AA",
                    "value" => 15
                ], [
                    "code" => "AB",
                    "value" => 15
                ]
            ];

            $that = $this;
            $new_data = array_map(function ($old_data) {
                $tmp = [];
                $goods_inf = $old_data['orders_goods_info'];
                $tmp[] = $old_data['orders_number'];
                $tmp[] = trim($old_data['send_goods_company']);
                $tmp[] = $old_data['pickup_time'] ? date('Y-m-d', $old_data['pickup_time']) : "";
                $tmp[] = implode("/", array_column($goods_inf, 'goods_name'));
                $tmp[] = $old_data['xdjs'];
                $tmp[] = $old_data['xdsl'];
                $tmp[] = $old_data['xdtj'];
                $tmp[] = $old_data['xdzl'];
                $tmp[]=$old_data['shipment_count'];
                $tmp[]=$old_data['shipment_pack_count'];
                $tmp[]=$old_data['shipment_volume'];
                $tmp[]=$old_data['shipment_weight'];
                $tmp[] = trim($old_data['accept_goods_company']);
                $tmp[] = $old_data['accept_address'];
                $tmp[] = $old_data['accept_cellphone'];
                $tmp[] = $old_data['accept_name'];
                $tmp[] = $old_data['accept_location_name'];
                $tmp[] = $old_data['remark'];
                $tmp[] = $old_data['supplier_name'];
                $tmp[] = $old_data['shipment_number'];
                $tmp[] = date('Y-m-d', $old_data['create_time']);

                $tmp[] = $old_data['shipment_info'][0]['shipment_time'] ? date('Y-m-d', $old_data['shipment_info'][0]['shipment_time']) : "";
                $tmp[] = $old_data['departure_time'] ? date('Y-m-d', $old_data['departure_time']) : "";
                $tmp[] = $old_data['customer_order_number'];

                $tmp[] = $old_data['send_time'];
                $tmp[] = strtotime($old_data['true_time']) ? $old_data['true_time'] : "";

                $tmp[] = $this->baseConfig['order']['order_status'][$old_data['order_status']]['order_status_name'];

                $tmp[] = implode(',', array_map(function ($e) {
                    return date('Y-m-d', $e['create_time']) . '/' . $e['create_user_name'] . '/' . $this->baseConfig['order']['order_status'][$e['order_status']]['order_status_name'] . '/' . $e['remark'];
                }, $old_data['operating_info']));
                $tmp[] = $old_data['send_address'];
                $tmp[] = $old_data['send_location_name'];
                $tmp[] = implode("/", array_map(function ($e) {
                    return $this->baseConfig['order']['goods_pack'][$e['estimated_pack_unit']];
                }, $goods_inf));
                $tmp[] = $old_data['insurance_goods'];

                return $tmp;
            }, $data);
            //echo json_encode($new_data,256);exit();
            //导出excel
           $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name, $filter);


        }


        // echo json_encode($result);exit();

        $ab_type = $this->callSoaErp('post', '/order/getAbnormalType');


        $ab_type_list = $this->abnormalRecurrence($ab_type['data']['list']);


        $this->assign('abnormalType', json_encode($ab_type_list));

        $result = Help::getOrder($result);

        $this->getPageParams($result);


        return $this->fetch('order_track');


    }

//异步添加回单
    public function addReceiptAjax()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/order/addReceipt', $data);
        echo json($result);
    }

    //异常订单图片
    public function orderABnormalUploadView()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/order/getAbnormalUpload', $data);

        $this->assign('data', $result['data']);

        return $this->fetch('order_abnormal_upload_view');
    }

//订单图片
    public function orderUploadView()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/order/getReceipt', $data);
        $this->getPageParams($result);
        return $this->fetch('order_upload_view');
    }

    public function orderUploadDel()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/order/orderUploadDel', $data);
       return json($result);
    }

//添加回单
    public function addReceipt()
    {
        $params = Request::instance()->param();
        $orders_id = $params['orders_id'];

        $rule = ['orders_id' => "require|number"];
        $v = new Validate($rule);


        $data = [
            'page' => $this->page(),
            'page_size' => $this->_page_size,
            //'orders_id_is_like'=>1,
            'orders_id' => $orders_id

        ];
        if (!$v->check($data)) {
            $this->error($v->getError());

        }


        $this->assign('orderStatus', Contents::orderStatus());
        $result = $this->callSoaErp('post', '/order/getOrder', $data);

        $result = Help::getOrder($result);
        $this->getPageParams($result);
        return $this->fetch('add_receipt');
    }


    //获取订单AJAX
    public function getOrderAjax()
    {


        $params = Request::instance()->param();

        $result = $this->callSoaErp('post', '/order/getOrder', $params);


        $result = Help::getOrder($result);
        if (isset($params['page'])) {
            $data['code'] = $result['code'];
            $data['msg'] = $result['code'];
            $data['data'] = $result['data']['list'];
            $data['count'] = $result['data']['count'];
        } else {
            $data = $result;
        }


        return $data;


    }


    //更新订单状态
    public function updateOrderStatus()
    {
        $params = Request::instance()->param();
        $data = [
            'orders_id' => $params['orders_id'],
            'order_status' => $params['order_status']
        ];

        $projectResult = $this->callSoaErp('post', '/order/updateOrderStatus', $data);
        return json($projectResult);
    }


    /**
     * 订单新增页面
     */
    public function showOrderAdd()
    {


        $params = Request::instance()->param();

        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        //获取随车宝

        $equipmentParams['equipment_type'] = 2;
        $equipmentResult = $this->callSoaErp('post', '/equipment/getEquipment', $equipmentParams);

        $this->assign('equipmentResult', $equipmentResult['data']);


        $project_id = input('project_id');
        $orders_id = input('orders_id');
        //获取所有城市
        //获取所有项目

        $productData['status'] = 1;

        $projectResult = $this->callSoaErp('post', '/source/getProject', $productData);

        $sort = array_column($projectResult['data'], 'orders_count');
        array_multisort($sort, SORT_DESC, $projectResult['data']);


        $this->assign('projectResult', $projectResult['data']);

        //如果选择了项目
        if (is_numeric($project_id)) {
            //获取所有的收发货信息
            $project_data['project_id'] = $project_id;
            $project_data['status'] = 1;

            $projectResult = $this->callSoaErp('post', '/source/getProject', $project_data);
            //echo json_encode($projectResult,256);exit();
            $this->assign('projectData', $projectResult['data'][0]);


            $sendResult = $this->callSoaErp('post', '/source/getSendGoods', $project_data);


            $this->assign('sendResult', $sendResult['data']);
            $acceptResult = $this->callSoaErp('post', '/source/getAcceptGoods', $project_data);

            $this->assign('acceptResult', $acceptResult['data']);
            $this->assign('result', $orderResult['data'][0]);


            //获取货物
            $goodsData['project_id'] = $project_id;
            $goodsData['status'] = 1;
            $goodsResult = $this->callSoaErp('post', '/source/getGoods', $goodsData);
            $this->assign('goodsResult', $goodsResult['data']);
        }

        $company_id = Session::get('user')['company_id'];


        if (is_numeric($orders_id)) {
            //那么就获取运单的COMPANY_id
            $orderData['orders_id'] = $orders_id;
            $orderResult = $this->callSoaErp('post', '/order/getOrder', $orderData);

            $company_id = $orderResult['data'][0]['company_id'];
            $this->assign('result', $orderResult['data'][0]);

            $faParams['city_id'] = $orderResult['data'][0]['send_province_id'];

            $faResult = Help::getCityAndArea($faParams['city_id']); //$this->callSoaErp('post', '/source/getCityInfo', $faParams);
            $this->assign('faResult', $faResult);

            $daoParams['city_id'] = $orderResult['data'][0]['accept_province_id'];
            $daoResult = Help::getCityAndArea($daoParams['city_id']);// $this->callSoaErp('post', '/source/getCityInfo', $daoParams);
            $this->assign('daoResult', $daoResult);


            $province_data = Help::getCity(0, 1);
            //获取当前orders_id发站市
            $city_data = Help::getCity($orderResult['data'][0]['send_province_id'], 2);
            //获取当前orders_id发站区
            $area_data = Help::getCity($orderResult['data'][0]['send_city_id'], 3);
            //获取当前orders_id到站市
            $city_data1 = Help::getCity($orderResult['data'][0]['accept_province_id'], 2);
            //获取当前orders_id到站区
            $area_data1 = Help::getCity($orderResult['data'][0]['accept_city_id'], 3);

        } else {
            $province_data = Help::getCity(0, 1);
            //获取所有市
            $city_data = [];// Help::getCity(0,2);
            $area_data = [];// Help::getCity(0, 3);

            $city_data1 = $city_data;
            //获取所有区


            $area_data1 = $area_data;
        }
        // echo json_encode($city_data,256);exit();

        $this->assign('company_id', $company_id);
        $this->assign('provinceResult', $province_data);
        $this->assign('cityResult', $city_data);
        $this->assign('areaResult', $area_data);
        //到站市
        $this->assign('cityResult_accept', $city_data1);
        //到站区
        $this->assign('areaResult_accept', $area_data1);


        return $this->fetch('order_add');
    }

    public function showOrderInfo()
    {

        $orders_number = input('orders_number');
        //获取所有城市


        $orderData['orders_number'] = $orders_number;
        $orderResult = $this->callSoaErp('post', '/order/getOrder', $orderData);

        $this->assign('result', $orderResult['data'][0]);

        $faParams['city_id'] = $orderResult['data'][0]['send_city_id'];


        $faResult = $this->callSoaErp('post', '/source/getCityInfo', $faParams);


        $this->assign('faResult', $faResult['data']);

        $daoParams['city_id'] = $orderResult['data'][0]['accept_city_id'];
        $daoResult = $this->callSoaErp('post', '/source/getCityInfo', $daoParams);
        $this->assign('daoResult', $daoResult['data']);


        return $this->fetch('order_info');

    }

    /**
     * 获取发货方
     */
    public function getSendGoodsNameAjax()
    {
        $keyword = input('keyword');
        $Result = $this->callSoaErp('post', '/order/getSendGoodsName', ['keyword' => $keyword]);
        $outPut = [];
        foreach ($Result['data'] as $k => $v)
            $outPut[] = ['name' => $v['send_goods_company'] . '-' . $v['send_goods_address'], value => $v['send_goods_id']];
        return json(['data' => $outPut, 'code' => 0]);
    }

    /**
     * 获取发货方
     */
    public function getAcceptGoodsNameAjax()
    {
        $keyword = input('keyword');
        $Result = $this->callSoaErp('post', '/order/getAcceptGoodsName', ['keyword' => $keyword]);

        $outPut = [];
        foreach ($Result['data'] as $k => $v)
            $outPut[] = ['name' => $v['accept_goods_company'] . '-' . $v['accept_goods_address'], value => $v['accept_goods_id']];
        return json(['data' => $outPut, 'code' => 0]);
    }


    /**
     * 订单新增AJAX
     */
    public function addOrderAjax()
    {
        $data = Request::instance()->param();

        if (!empty($data['send_time'])) {
            $data['send_time'] = strtotime($data['send_time']);
        }
        if (!empty($data['pickup_time'])) {
            $data['pickup_time'] = strtotime($data['pickup_time']);
        }
        if (empty($data['send_location_id']) || empty($data['accept_location_id'])) {
            return $this->outPutError("发站或者到站错误");
        }

        for ($i = 0; $i < count($data['goods_name']); $i++) {
            $data['goods_name'][$i] = trim($data['goods_name'][$i]);
        }


        if (is_numeric($data['orders_id'])) {

            $result = $this->callSoaErp('post', '/order/updateOrderByOrderId', $data);

        } else {

            $result = $this->callSoaErp('post', '/order/addOrder', $data);

        }

        return $result;//['code' => '400', 'msg' => $data];
    }

    //获取订单跟踪
    public function showOrderFollowManage()
    {
        $data = [
            'page' => $this->page(),
            'page_size' => $this->_page_size,


        ];

        //$result = $this->callSoaErp('post', '/order/getOrderFollow',$data);
        return $this->fetch('order_follow');
    }

    /**
     * 运单跟踪新增页面
     */
    public function orderFollow()
    {
        $data = Request::instance()->param();

        $params['orders_id'] = $data['orders_id'];

        $result = $this->callSoaErp('post', '/order/getOrder', $params);
        $operatingResult = $this->callSoaErp('post', '/source/getOrderOperating', $params);
//echo json_encode($result['data'][0],256);exit();

        $jfjs = 0;
        $jfzl = 0;
        $jftj = 0;
        foreach ($result['data'][0]['orders_goods_info'] as $k => $v) {

            $jfjs += $v['realy_count'];
            $jfzl += $v['realy_weight'];
            $jftj += $v['realy_volume'];

        }


        $this->assign('jfjs', $jfjs);
        $this->assign('jfzl', $jfzl);
        $this->assign('jftj', $jftj);
        $this->assign('result', $result['data'][0]);
        $this->assign('operating', $operatingResult['data']);
        return $this->fetch('order_follow_add');
    }

    public function getOperatingAjax()
    {
        $data = Request::instance()->param();
        $operatingResult = $this->callSoaErp('post', '/source/getOrderOperating', $data);
        // echo json_encode($operatingResult);exit();
        return json(['code' => 0, 'data' => $operatingResult['data'], 'count' => $operatingResult['count']]);
    }


    /**
     * 新增订单跟踪AJAX
     */
    public function addOrderFollowAjax()
    {
        $data = Request::instance()->param();
        if (is_numeric($data['follow_level'])) {
            $data['follow_level'] = 1;
        } else {
            $data['follow_level'] = 0;
        }

        $result = $this->callSoaErp('post', '/order/updateOrderFollow', $data);


        return $result;//['code' => '400', 'msg' => $data];
    }

    //修改发运编号

    public function changeShipmentNumberAjax()
    {
        $data = Request::instance()->param();

        $result = $this->callSoaErp('post', '/order/changeShipmentNumber', $data);


        return $result;//['code' => '400', 'msg' => $data];
    }

//修改操作内容

    public function changeOperatingAjax()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/order/changeOperating', $data);
        return $result;//['code' => '400', 'msg' => $data];
    }


    /**
     *修改运单
     */
    public function updateOrdersByOrdersNumber()
    {
        $data = Request::instance()->param();
        if (!empty($data['true_time'])) {
            $data['true_time'] = strtotime($data['true_time']);
        } else {
            $data['true_time'] = null;
        }

        if (!empty($data['departure_time'])) {
            $data['departure_time'] = strtotime($data['departure_time']);
        } else {
            $data['departure_time'] = null;
        }

        $result = $this->callSoaErp('post', '/order/updateOrderByOrdersNumber', $data);


        return json($result);//['code' => '400', 'msg' => $data];
    }

    /**
     * 新增客户跟踪AJAX
     */
    public function addOrderCustomerFollowAjax()
    {
        $data = Request::instance()->param();

        if (is_numeric($data['orders_customer_follow_id'])) {

            $result = $this->callSoaErp('post', '/order/updateOrderCustomerFollow', $data);
        } else {

            $result = $this->callSoaErp('post', '/order/addOrderCustomerFollow', $data);

        }


        return $result;//['code' => '400', 'msg' => $data];
    }

    /**
     * 送货签收单显示页面
     */
    public function showOrderDeliveryReceiptManage()
    {

        //获取相关数据
        $data = Request::instance()->param();
        $orderResult = $this->callSoaErp('post', '/order/getPrintOrderInfo', $data);
//echo json_encode($orderResult);exit();

        $xdjs = 0;
        $xdsl = 0;
        $xdzl = 0;
        $xdtj = 0;
        foreach ($orderResult['data'][0]['orders_goods_info'] as $key => $val) {
            $xdsl += $val['estimated_count'];
            $xdjs += $val['estimated_pack_count'];
            $xdzl += $val['estimated_weight'];
            $xdtj += $val['estimated_volume'];
        }
//        echo "<pre>";
//        var_dump($orderResult['data'][0]);exit;
//        echo "</pre>";
//        var_dump($nums);exit;
        $this->assign('orderResult', $orderResult['data'][0]);
        $this->assign('xdsl', $xdsl);
        $this->assign('xdjs', $xdjs);
        $this->assign('xdzl', $xdzl);
        $this->assign('xdtj', $xdtj);

        return $this->fetch('order_delivery_receipt_manage');
    }


    /**
     * 送货签收单
     */
    public function sendGoodsReceive()
    {

        return $this->fetch('send_goods_receive');

    }

    /**
     * 客户跟踪
     */
    public function orderCustomerFollow()
    {

        $data = Request::instance()->param();

        $params['orders_number'] = $data['orders_number'];

        $result = $this->callSoaErp('post', '/order/getReceiptInfo', $params);

        $this->assign('result', $result['data'][0]);
        return $this->fetch('order_customer_follow_add');
    }


    public function incomeAccountingImport()
    {

        $importInfo = "";
        if (isset($_FILES["inputExcel"]["tmp_name"])) {


            vendor("phpexcel.PHPExcel");

            vendor("phpexcel.PHPExcel.IOFactory");

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel = \PHPExcel_IOFactory::load($_FILES["inputExcel"]["tmp_name"]);
            $indata = $objPHPExcel->getSheet(0)->toArray();

            $result = $this->callSoaErp('post', '/order/incomeAccountingImport', ['excel_data' => $indata]);

            $importInfo = $result['data'];


        }
        $this->assign('importInfo', $importInfo);
        return $this->fetch('income_accounting_import');
    }

    public function changeAbnormalDescribeAjax()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/order/changeAbnormalDescribe', $data);
        return json($result);
    }

    public function orderTrackEdit()
    {
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/order/orderTrackEdit', $data);
        return json($result);
    }

    public function delOrder(){

        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/order/delOrder', $data);
        return json($result);

    }

    public function delOrderManage(){
        //读取所有的项目

        $params = Request::instance()->param();

        $data = $params;

        //$projectResult = $this->callSoaErp('post', '/source/getProject', $data);
        //获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);

        $multi_project_id = input('multi_project_id');
        if (!empty($multi_project_id))
            $projectResult = $this->callSoaErp('post', '/source/getProject', ['multi_project_id' => $multi_project_id]);


        $this->assign('projectResult', $projectResult['data']);

        //获取所有承运商
        $supplierParams['status'] = 1;
        $supplierResult = "";
        if (!empty($params['supplier_uuid'])) {
            $supplierParams['supplier_uuid'] = $params['supplier_uuid'];

            $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierParams);


        }
        $this->assign('supplierResult', $supplierResult['data']);
        $data['page'] = $this->page();
        $data['page_size'] = $params['limit'] ? $params['limit'] : 30;
        if (isset($params['download'])) {
            $data['page_size'] = Contents::PAGE_SIZE_MAX;
        }
        if (is_numeric(input('project_id'))) {

            $data['project_id'] = input('project_id');
        }
        $status = input('status');
        if (is_numeric($status)) {
            $data['status'] = $status;
        } else
            $data['status'] = 1;

        if (!empty($params['accept_goods_company'])) {
            $data['accept_goods_company'] = $params['accept_goods_company'];
        }
        if (!empty($params['orders_number'])) {
            $data['orders_number'] = trim($params['orders_number']);
        }
        if (!empty($params['supplier_uuid'])) {
            $data['supplier_uuid'] = $params['supplier_uuid'];
        }
        if (is_numeric($params['choose_company_id'])) {
            $data['choose_company_id'] = $params['choose_company_id'];
        } else {
            $data['choose_company_id'] = session('user')['company_id'];
        }
        $result = $this->callSoaErp('post', '/order/getdelOrder', $data);

        $data = $result['data']['list'];

        for ($i = 0; $i < count($data); $i++) {
            if (is_numeric($data[$i]['true_time'])) {
                $data[$i]['true_time_unix'] = $data[$i]['true_time'];
                $data[$i]['true_time'] = date('Y-m-d', $data[$i]['true_time']);

            } else {
                $data[$i]['true_time'] = '&nbsp';
            }
            if (!empty($data[$i]['send_time'])) {
                $data[$i]['send_time'] = date('Y-m-d', $data[$i]['send_time']);
            } else {
                $data[$i]['send_time'] = '';
            }


            $xdjs = 0;
            $xdsl = 0;
            $xdzl = 0;
            $xdtj = 0;
            for ($j = 0; $j < count($data[$i]['orders_goods_info']); $j++) {
                $xdjs += $data[$i]['orders_goods_info'][$j]['realy_count'];
                $xdsl += $data[$i]['orders_goods_info'][$j]['realy_pack_count'];
                $xdzl += $data[$i]['orders_goods_info'][$j]['realy_weight'];
                $xdtj += $data[$i]['orders_goods_info'][$j]['realy_volume'];
            }

            $data[$i]['xdjs'] = $xdjs;
            $data[$i]['xdsl'] = $xdsl;
            $data[$i]['xdzl'] = $xdzl;
            $data[$i]['xdtj'] = $xdtj;

        }

        if ($this->request->isAjax()) {

            return json(['code' => 200, 'data' => $data, 'count' => $result['data']['count']]);
        }


        if (isset($params['download'])) {

            $name = "运单导出";
            $xAxis = [];
            $title = "运单导出";
            $header = array('运单编号', '发货方', '运单日期', '货物名称', '件数', '数量', '体积', '重量', '收货方名称', '收货方地址', '联系电话', '收货人', '到站',
                '备注', '承运商', '专线编号', '创建日期', '发货日期', '发车日期', '客户订单号', '要求到货日期', '实际到货日期', '当前进度',
                '发运跟踪', '发货地址', '发站', '包装方式', '货物价值');
            $new_data = [];

            $filter = [
                [
                    "code" => "A",
                    "value" => 30
                ], [
                    "code" => "B",
                    "value" => 20
                ], [
                    "code" => "C",
                    "value" => 20
                ], [
                    "code" => "D",
                    "value" => 20
                ], [
                    "code" => "E",
                    "value" => 20
                ], [
                    "code" => "F",
                    "value" => 20
                ], [
                    "code" => "G",
                    "value" => 20
                ], [
                    "code" => "H",
                    "value" => 20
                ], [
                    "code" => "I",
                    "value" => 20
                ], [
                    "code" => "J",
                    "value" => 40
                ], [
                    "code" => "K",
                    "value" => 20
                ], [
                    "code" => "L",
                    "value" => 20
                ], [
                    "code" => "M",
                    "value" => 20
                ], [
                    "code" => "N",
                    "value" => 50
                ], [
                    "code" => "O",
                    "value" => 20
                ], [
                    "code" => "P",
                    "value" => 20
                ], [
                    "code" => "Q",
                    "value" => 20
                ], [
                    "code" => "R",
                    "value" => 20
                ], [
                    "code" => "S",
                    "value" => 20
                ], [
                    "code" => "T",
                    "value" => 20
                ], [
                    "code" => "U",
                    "value" => 20
                ], [
                    "code" => "V",
                    "value" => 20
                ], [
                    "code" => "W",
                    "value" => 20
                ], [
                    "code" => "X",
                    "value" => 30
                ], [
                    "code" => "Y",
                    "value" => 55
                ], [
                    "code" => "Z",
                    "value" => 20
                ], [
                    "code" => "AA",
                    "value" => 20
                ], [
                    "code" => "AB",
                    "value" => 20
                ]
            ];

            $that = $this;
            $new_data = array_map(function ($old_data) {
                $tmp = [];
                $goods_inf = $old_data['orders_goods_info'];
                $tmp[] = $old_data['orders_number'];
                $tmp[] = trim($old_data['send_goods_company']);
                $tmp[] = $old_data['pickup_time'] ? date('Y-m-d', $old_data['pickup_time']) : "";
                $tmp[] = implode("/", array_column($goods_inf, 'goods_name'));
                $tmp[] = $old_data['xdjs'];
                $tmp[] = $old_data['xdsl'];
                $tmp[] = $old_data['xdtj'];
                $tmp[] = $old_data['xdzl'];
                $tmp[] = trim($old_data['accept_goods_company']);
                $tmp[] = $old_data['accept_address'];
                $tmp[] = $old_data['accept_cellphone'];
                $tmp[] = $old_data['accept_name'];
                $tmp[] = $old_data['accept_location_name'];
                $tmp[] = $old_data['remark'];
                $tmp[] = $old_data['supplier_name'];
                $tmp[] = $old_data['shipment_number'];
                $tmp[] = date('Y-m-d', $old_data['create_time']);

                $tmp[] = $old_data['shipment_info'][0]['shipment_time'] ? date('Y-m-d', $old_data['shipment_info'][0]['shipment_time']) : "";
                $tmp[] = $old_data['departure_time'] ? date('Y-m-d', $old_data['departure_time']) : "";
                $tmp[] = $old_data['customer_order_number'];

                $tmp[] = $old_data['send_time'];
                $tmp[] = strtotime($old_data['true_time']) ? $old_data['true_time'] : "";

                $tmp[] = $this->baseConfig['order']['order_status'][$old_data['order_status']]['order_status_name'];

                $tmp[] = implode(',', array_map(function ($e) {
                    return date('Y-m-d', $e['create_time']) . '/' . $e['create_user_name'] . '/' . $this->baseConfig['order']['order_status'][$e['order_status']]['order_status_name'] . '/' . $e['remark'];
                }, $old_data['operating_info']));
                $tmp[] = $old_data['send_address'];
                $tmp[] = $old_data['send_location_name'];
                $tmp[] = implode("/", array_map(function ($e) {
                    return $this->baseConfig['order']['goods_pack'][$e['estimated_pack_unit']];
                }, $goods_inf));
                $tmp[] = $old_data['insurance_goods'];

                return $tmp;
            }, $data);
            //echo json_encode($new_data,256);exit();
            //导出excel
            $excel = Demo::export_accept_goods($xAxis, $title, $header, $new_data, $name, $filter);


        }


        // echo json_encode($result);exit();

        $ab_type = $this->callSoaErp('post', '/order/getAbnormalType');


        $ab_type_list = $this->abnormalRecurrence($ab_type['data']['list']);


        $this->assign('abnormalType', json_encode($ab_type_list));

        $result = Help::getOrder($result);

        $this->getPageParams($result);


        return $this->fetch('order_manage');




    }



}