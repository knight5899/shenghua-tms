<?php

namespace app\index\controller;

use think\Controller;
use think\Request;

class Api extends Base1
{
    /**
     * 圣华
     * 添加温湿度数据接口
     */
    public function addTH(Request $request)
    {

        $data = $request->param();

		
		
        $result = $this->callSoaErp2('post', '/Wmsmonitor/addTH', $data);
        return json_encode($result,true);

    }

    public function getOrdersInfo(Request $request){
        $data = $request->param();

        $result = $this->callSoaErp2('post', '/Order/getOrdersInfo', $data);

        return json_encode($result,true);
    }
}