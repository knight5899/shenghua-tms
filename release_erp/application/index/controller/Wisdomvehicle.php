<?php

namespace app\index\controller;
use app\common\help\H;
use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
class Wisdomvehicle extends Base
{



    
    /**
     * 获取停车库列表
     */
    public function showParkList(){
    	
    
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,
    			 
    	];
    	$result = action('haikang/park_list',['postData' => $data]);
		    	
    	$this->assign('data',$result['data']);


    	return $this->fetch('park_list');
    }
    /**
     * 获取出入口列表
     */
    public function showEntranceList(){
    	$params = Request::instance()->param();
		//获取停车库列表
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,
    	
    	];
    	$parkResult = action('haikang/park_list',['postData' => $data]);
 
    	
    	$parkList = $parkResult['data'];
    	if(!empty($params['parkIndexCode'])){
    		$data['parkIndexCodes'] = $params['parkIndexCode'];
    		
    	}else{
    		$data['parkIndexCodes'] = $parkList[0]['parkIndexCode'];
    	}
    
    

    	$this->assign('parkList',$parkList);
    	

    	
    	
    	$result = action('haikang/entrance_list',['postData' => $data]);
    	
    
    	$this->assign('data',$result['data']);
    	
    	
    	return $this->fetch('entrance_list');
    	

    	
    }
    
    /**
     * 获取车道
     */

    public function showRoadwayList(){
    	$params = Request::instance()->param();
  
    	
    	
    	if(!empty($params['entranceIndexCodes'])){
    		$data['entranceIndexCodes'] = $params['entranceIndexCodes'];
    	}else{
    		$data = [];
    	}
    	
    	$result = action('haikang/roadway_list',['postData' => $data]);
    	

    	
    	$this->assign('data',$result['data']);
    
    
    	return $this->fetch('roadway_list');
    }  
    
    /**
     * 车道反控
     */
    public function deviceControl(){
    	$params = Request::instance()->param();
		
		
		
    	$data['roadwaySyscode'] = $params['roadwaySyscode'];
		$data['command'] =  $params['command'];
    	 
    	$result = action('haikang/device_control',['postData' => $data]);  
	
    	return $result;
    }
    
    /**
     * 查询过车记录
     */
    public function showCrossRecords(){
    	$params = Request::instance()->param();
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,    		
    	];
    	if(!empty($params['plateNo'])){
    		$data['plateNo'] = $params['plateNo'];
    	}
    	
    	$result = action('haikang/cross_records',['postData' => $data]);
		
//     	$this->assign('data',$result['data']);
    	
    	$this->getHaikangPageParams($result);
     	return $this->fetch('cross_records');
    	
    }
}

