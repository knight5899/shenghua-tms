<?php
/**
 * Created by PhpStorm.
 * User: Hugh
 * Date: 2019/11/04
 * Time: 13:40
 */

namespace app\index\controller;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;
use app\common\help\Contents;

class MyBooking extends BookingBase
{
    public function index(){
        $b2bBookingStatus = Contents::b2bBookingStatus();
        $this->assign('b2bBookingStatus',$b2bBookingStatus);
        if($_POST['search_submit'] && $_POST['search_submit']=='Search'){
//            var_dump($_POST);exit;

            if($_POST['search_id'])
            {
                $w['b2b_booking_id'] = $_POST['search_id'];
            }
            if($_POST['search_ref']){
                $w['agent_reference_id'] = $_POST['search_ref'];
            }
            if($_POST['search_tourdate']){
                $w['begin_time'] = strtotime($_POST['search_tourdate']);
            }
            if($_POST['search_consultant']){
                $w['consultant'] = $_POST['search_consultant'];
            }
            if($_POST['search_status']){
                $w['booking_status'] = $_POST['search_status'];
            }
        }else{
            unset($_POST);
        }

        $w['agent_id']  = session('booking_user')['distributor_id'];
        $result = $this->callSoaErp('post','/b2b_booking/getB2bBooking',$w);

//        var_dump($result);exit;


        $this->assign('site_title','My Booking');
        $this->assign('all_booking',$result['data']);

        $data['distributor_id'] = session('booking_user')['distributor_id'];
        $distributorData = $this->callSoaErp('post', '/btob/getDistributorConsultant', $data);
        $this->assign('distributorData',$distributorData['data']);

        return $this->fetch('/my_booking/index');
    }


    public function view_booking_summary(){
        $this->assign('site_title','My Booking');

        $bookingId = $_GET['bookingId'];

        $where['b2b_booking_id'] = $bookingId;
        $General = $this->callSoaErp('post','/b2b_booking/getB2bBookingGeneral',$where);
//        var_dump($General);exit;
        $this->assign('general',$General['data']);  unset($where);

        //获取游客信息
        $where['b2b_booking_id'] = $bookingId;
        $Customer = $this->callSoaErp('post','/b2b_booking/getB2bBookingCustomer',$where);
        $this->assign('customer',$Customer['data']);
        $age_group = Arrays::group($Customer['data'],'age_group');
        $this->assign('age_group',$age_group);unset($where);
//        var_dump($age_group);
//        var_dump($Customer);exit;

        //用房
        $where['b2b_booking_id'] = $bookingId;
        $room = $this->callSoaErp('post','/b2b_booking/getB2bBookingRoom',$where);
//        var_dump($room); exit;
        $this->assign('roomData',$room['data']);unset($where);

        //接送机
        $where['b2b_booking_id'] = $bookingId;
        $transfer = $this->callSoaErp('post','/b2b_booking/getB2bBookingTransfer',$where);
        $transfer['data'] = Arrays::group($transfer['data'],'transfer_type');
//        echo '<pre>';print_r($transfer);exit;
        $this->assign('transfer',$transfer['data']);unset($where);

        //前后入住
        $where['b2b_booking_id'] = $bookingId;
        $accommodation = $this->callSoaErp('post','/b2b_booking/getB2bBookingAccommodation/',$where);
        $accommodation['data'] = Arrays::group($accommodation['data'],'accommodation_type');
//        echo '<pre>';print_r($accommodation);exit;
        $this->assign('accommodationAll',$accommodation['data']);unset($where);


        $b2bBookingStatus = Contents::b2bBookingStatus(); //订单状态
        $this->assign('b2bBookingStatus',$b2bBookingStatus);
        return $this->fetch('/my_booking/view_booking_summary');
    }

}