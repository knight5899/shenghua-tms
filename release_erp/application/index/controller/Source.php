<?php

namespace app\index\controller;

use app\common\help\Contents;
use app\common\help\Help;
use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use think\Cache;
class Source extends Base
{

    //供应商类型

  //  $data = Request::instance()->param();
    public function getPricingConfigureAndPrice(){
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post','/source/getPricingConfigureAndPrice',$data);
        return json($result,256);

    }

    



    /**
     * 承运商显示页面
     */
    public function showSupplierManage(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }
		$this->assign('comapnyResult', $comapnyResult);

        $this->getPageParams($result);


        return $this->fetch('supplier_manage');
    }

	//获取供应商AJAX
	public function getSupplierManageAjax(){
		$params = Request::instance()->param();
		
	
		$data = [
				'page' => $params['page'],
				'page_size' => $params['limit'],
				//'orders_id_is_like'=>1,

		
		];		

        $supplier_type = input("supplier_type");
        $supplier_name = input("supplier_name");
        $supplier_uuid= input("supplier_uuid");
        $supplier_id= input("supplier_id");
        if(is_numeric(input('status'))){
        	$data['status'] = input('status');
        }
 

        $linkman = input("linkman");
        if(is_numeric($supplier_type)){
            $data['supplier_type'] = $supplier_type;
        }
        if(!empty($supplier_name)){
            $data['supplier_name'] = $supplier_name;
        }

        if(!empty($supplier_uuid)){
            $data['supplier_uuid'] = $supplier_uuid;
        }
        if(!empty($supplier_id)){
        	$data['supplier_id'] = $supplier_id;
        }       
        if(!empty($linkman)){
            $data['linkman'] = $linkman;
        }
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }else{
			$data['choose_company_id'] =session('user')['company_id'];
		}

        $result = $this->callSoaErp('post','/source/getSupplier',$data);

		for($i=0;$i<count($result['data']['list']);$i++){
			$result['data']['list'][$i]['supplier_type_name']=Contents::baseConfig()['supplier']['supplier_type'][$result['data']['list'][$i]['supplier_type']];
			$result['data']['list'][$i]['create_time']=date('Y-m-d H:i:s',$result['data']['list'][$i]['create_time']);			
		}
		$r = [
			'code'=>200,
			'data'=>$result['data']['list'],
			'count'=>	$result['data']['count'],

		];
		return $r;				
		
		
	}

    public function getSupplierAjax(){
        $data = Request::instance()->param();
        $result = $this->callSoaErp('post','/source/getSupplier',$data);
        return json($result);
    }

    /**
     * 承运商显示新增页面
     */
    public function showSupplierAdd(){

        $params = Request::instance()->param();

        $supplier_id = input('supplier_id');

        //获取供应商类别
        $data = [
            'status'=>1

        ];
		//$supplier_result = $this->callSoaErp('post', '/system/getSupplierType');

        //$this->assign('supplier_data_result',$supplier_result['data']);


        //获取公司信息
        $data = [
            'status'=>1
        ];
        $company_result =  $this->callSoaErp('post', '/system/getCompany',$data);

        //获取城市3级联动信息
//        $data1['status'] = 1;
//        $country_result = $this->callSoaErp('post', '/system/getCountryCity',$data1);

        $this->assign('country_result',$country_result['data']);
        $this->assign('company_result',$company_result['data']);
        $this->assign('supplier_types',$this->types);

        if(is_numeric($supplier_id)){
            $supplierData['supplier_id'] = $supplier_id;
            $supplierResult = $this->callSoaErp('post','/source/getSupplier',$supplierData);
            $this->assign('result',$supplierResult['data'][0]);
//            var_dump($supplierResult);exit;
        }

        if ($params['nomenu'] == 2) {
            return $this->fetch('supplier_add_nomenu2');
        }else{
            return $this->fetch('supplier_add');
        }
    }

    /**
     * 承运商新增数据
     */
    public function addSupplierAjax(){

        $supplier_id = input("supplier_id");
        $supplier_name = input("supplier_name");
        $supplier_type = input('supplier_type');
        $linkman = input("linkman");
        $address = input("address");
        $zip_code = input("zip_code");
        $phone = input("phone");
        $website = input("website");
        $fax = input("fax");  
        $cellphone = input('cellphone');
        $email = input("email");
        $remark = input("remark");
        $account_name = input('account_name');
        $bank_code = input('bank_code');
        $bank_name = input('bank_name');
        $bank_number = input('bank_number');
		$supplier_tax_type = input('supplier_tax_type');
		$supplier_tax = input('supplier_tax');
        $status =1;
        $data = [
            "supplier_id"=>$supplier_id,
            "supplier_name"=>$supplier_name,
            "supplier_type"=>$supplier_type,
            "linkman"=>$linkman,
            "address"=>$address,
            "zip_code"=>$zip_code,
            "phone"=>$phone,
            "website"=>$website,
            "fax"=>$fax,
        	'cellphone'=>$cellphone,
            "email"=>$email,
            "remark"=>$remark,
        	'account_name'=>$account_name,
        	'bank_code'=>$bank_code,
        	'bank_name'=>$bank_name,
        	'bank_number'=>$bank_number,
			'supplier_tax_type'=>$supplier_tax_type,
			'supplier_tax'=>$supplier_tax,
        	"status"=>$status,
          
        ];
		
        $result = $this->callSoaErp('post', '/source/addSupplier',$data);
		
        return   $result;//['code' => '400', 'msg' => $data];
    }

    /**
     * 承运商修改页面
     */
    public function showSupplierEdit(){
        //读取相应供应商信息
        $data=["supplier_id"=>input("supplier_id")];
        $result = $this->callSoaErp('post', '/source/getSupplier',$data);

        $this->assign('data',$result['data'][0]);

   


        $this->assign('supplier_types',$this->types);
        return $this->fetch('supplier_edit');

    }

    /**
     * 承运商修改数据
     */
    public function editSupplierAjax(){
        $supplier_id = input("supplier_id");
        $supplier_type = input("supplier_type");
        $supplier_name = input("supplier_name");
		$supplier_tax = input("supplier_tax");
        $linkman = input("linkman");
        $address = input("address");
        $zip_code = input("zip_code");
        $phone = input("phone");
        $website = input("website");
        $fax = input("fax");
        $email = input("email");
        $remark = input("remark");
        $status = input("status");
		$supplier_tax_type = input('supplier_tax_type');
		$supplier_tax = input('supplier_tax');
        $account_name = input('account_name');
        $bank_code = input('bank_code');
        $bank_name = input('bank_name');
        $bank_number = input('bank_number');
        $cellphone = input('cellphone');
        $data = [
            "supplier_id"=>$supplier_id,
            "supplier_type"=>$supplier_type,
            "supplier_name"=>$supplier_name,
            "linkman"=>$linkman,
            "address"=>$address,
            "zip_code"=>$zip_code,
            "phone"=>$phone,
            "website"=>$website,
            "fax"=>$fax,
            "email"=>$email,
            "remark"=>$remark,
            "status"=>$status,
            "user_id"=>$user_id,
            "pay_message" => $pay_message,
        	'account_name'=>$account_name,
        	'bank_code'=>$bank_code,
        	'bank_name'=>$bank_name,
        	'bank_number'=>$bank_number,
            'cellphone'=>$cellphone,
			'supplier_tax_type'=>$supplier_tax_type,
			'supplier_tax'=>$supplier_tax,
        ];
	
        $result = $this->callSoaErp('post', '/source/updateSupplierBySupplierId',$data);
			
        return   $result;
    }

    /**
     * 承运商显示详情
     */
    public function showSupplierInfo(){
        //获取上一个页面的URl
        $url = $_SERVER["HTTP_REFERER"];
        $url_data = explode("/",$url);
        $this->assign('url',$url_data[4]);

        //读取相应供应商信息
        $data=["supplier_id"=>input("supplier_id")];
        $result = $this->callSoaErp('post', '/source/getSupplier',$data);
        $this->assign('data',$result['data'][0]);

        $supplier_type_id = input("supplier_type_id");
        if($supplier_type_id==5){
            $this->assign('button',$supplier_type_id);
        }

        $this->assign('supplier_types',$this->types);

        return $this->fetch('supplier_info');
    }

    /**
     * 承运商计价配置页面
     */
    public function showPricingConfigureManage(){

        $data=[
            'page'=>$this->page(),
            'page_size'=>1800,

        ];
        $supplier_id= input("supplier_id");

        if(!empty($supplier_id)){
            $data['supplier_id'] = $supplier_id;
        }

        //获取承运商信息
        $result = $this->callSoaErp('post','/source/getSupplier',$data);

        //获取计价配置信息
        $result2 = $this->callSoaErp('post','/source/getPricingConfigure',$data);

        $this->assign("supplierResult", $result['data']['list'][0]);
        $this->getPageParams($result2);

        return $this->fetch('pricing_configure_manage');
    }


    
    /**
     * 项目计价配置页面
     */
    public function showPricingConfigureProjectManage(){
        $data=[
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,

        ];
        $project_id = input("project_id");

        if(!empty($project_id)){
            $data['project_id'] = $project_id;
        }
        $data['supplier_id'] =Request::instance()->param()["supplier_id"];

        $result_Supplier = $this->callSoaErp('post','/source/getSupplier',[]);
        $this->assign("supplierResult", $result_Supplier['data']);

        //获取项目信息
        $result = $this->callSoaErp('post','/source/getProject',$data);

        //获取计价配置信息
        $result2 = $this->callSoaErp('post','/source/getPricingConfigure',$data);
//        echo "<pre>";
//        var_dump($data);exit;
//        echo "</pre>";
        $this->assign("projectResult", $result['data']['list'][0]);
        $this->getPageParams($result2);

        return $this->fetch('pricing_configure_project_manage');
    }

    /**
     * 承运商计价配置新增页面
     */
    public function showPricingConfigureAdd(){

        $supplier_id = Request::instance()->param()['supplier_id'];

        //获取省市区信息
        $city = Help::getCity("",1);
        $this->assign('shengResult',$city);

        //获取货物信息
        $goods = $this->callSoaErp('post', '/source/getGoods',[]);
        $this->assign('goodsResult',$goods['data']);
        $this->assign('supplier_id',$supplier_id);
//        var_dump($goods);exit;
        return $this->fetch('pricing_configure_add');
    }

    /**
     * 承运商计价配置新增页面
     */
    public function showPricingConfigureProjectAdd(){

        $project_id = Request::instance()->param()['project_id'];

        //获取省市区信息
        $city = Help::getCity('',1);
        $this->assign('shengResult',$city);

        //获取承运商信息
        $supplierResult = $this->callSoaErp('post','/source/getSupplier',[]);

        //获取货物信息
        $goods = $this->callSoaErp('post', '/source/getGoods',[]);
        $this->assign('goodsResult',$goods['data']);
        $this->assign('supplierResult',$supplierResult['data']);
        $this->assign('project_id',$project_id);
//        var_dump($goods);exit;
        return $this->fetch('pricing_configure_project_add');
    }

    /**
     * 承运商计价配置修改页面
     */
    public function PricingConfigureEdit(){

        $data['pricing_configure_id'] = Request::instance()->param()['pricing_configure_id'];

        //获取承运商信息
        $result = $this->callSoaErp('post','/source/getPricingConfigure',$data);
        $this->assign("pricingConfigureResult", $result['data'][0]);

        //获取省市区信息
        $city = Help::getCity('',1);
        $this->assign('shengResult',$city);

        //获取省市区信息
        $city2 = Help::getCity('',1);
        $this->assign('shengResult2',$city2);

        //通过拿到的省再去拿市数据
        $cityResult = Help::getCity($result['data'][0]['province_start_id'],2);
        $this->assign('cityResult', $cityResult);

        //通过拿到的省再去拿市数据
        $cityResult2 = Help::getCity($result['data'][0]['province_end_id'],2);
        $this->assign('cityResult2', $cityResult2);

        //通过拿到的市再去区
        $areaResult = Help::getCityAndArea($result['data'][0]['city_start_id'],2);
        $this->assign('areaResult',$areaResult);

        //通过拿到的市再去区
        $areaResult2 = Help::getCityAndArea($result['data'][0]['city_end_id'],2);
        $this->assign('areaResult2',$areaResult2);

        return $this->fetch('pricing_configure_edit');
    }

    /**
     * 项目计价配置修改页面
     */
    public function PricingConfigureProjectEdit(){

        $data['pricing_configure_id'] = Request::instance()->param()['pricing_configure_id'];

        //获取承运商信息
        $result22 = $this->callSoaErp('post','/source/getSupplier',[]);
        $this->assign("supplierResult", $result22['data']);

        //获取承运商信息
        $result = $this->callSoaErp('post','/source/getPricingConfigure',$data);
        $this->assign("pricingConfigureResult", $result['data'][0]);

        //获取省市区信息
        $city = Help::getCity('',1);
        $this->assign('shengResult',$city);

        //获取省市区信息
        $city2 = Help::getCity('',1);
        $this->assign('shengResult2',$city2);

        $data2['city_id'] = $result['data'][0]['departure_id'];
        //通过省市获取选中省市区信息
        $result2 = $this->callSoaErp('post','/city/areaGetProvinceAndcityId',$data2);
        $this->assign("a1Result", $result2['data']);

        $data3['city_id'] = $result['data'][0]['arrival_id'];
        //通过省市获取选中省市区信息
        $result3 = $this->callSoaErp('post','/city/areaGetProvinceAndcityId',$data3);
        $this->assign("a2Result", $result3['data']);

        //通过拿到的省再去拿市数据
        $cityResult = Help::getCity($result2['data']['province_id'],2);
        $this->assign('cityResult',$cityResult);
        //通过拿到的市再去区
        $areaResult = Help::getCity($result2['data']['city_id'],3);
        $this->assign('areaResult',$areaResult);

        //通过拿到的省再去拿市数据
        $cityResult2 = Help::getCity($result3['data']['province_id'],2);
        $this->assign('cityResult2',$cityResult2);
        //通过拿到的市再去区
        $areaResult2 = Help::getCity($result3['data']['city_id'],3);
        $this->assign('areaResult2',$areaResult2);

        //获取货物信息
        $goods = $this->callSoaErp('post', '/source/getGoods',[]);
        $this->assign('goodsResult',$goods['data']);

        return $this->fetch('pricing_configure_project_edit');
    }

    /**
     * 承运商计价配置修改AJAX
     */
    public function editPricingConfigureAjax(){

        $data = Request::instance()->param();

        $result = $this->callSoaErp('post', '/source/editPricingConfigure',$data);

        return   $result;//['code' => '400', 'msg' => $data];
    }

    /**
     * 承运商计价配置区间值批量修改页面
     */
    public function PricingConfigureBatchEdit(){


        $data=[
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,

        ];
        $supplier_id= input("supplier_id");

        if(!empty($supplier_id)){
            $data['supplier_id'] = $supplier_id;
        }

        $pricing_configure_id = Request::instance()->param()['pricing_configure_id'];

        //获取计价配置信息
        $result = $this->callSoaErp('post','/source/getPricingConfigure',$data);
        $this->getPageParams($result);

        return $this->fetch('pricing_configure_batch_edit');
    }

    /**
     * 承运商计价配置区间值批量修改AJAX
     */
    public function editPricingConfigureBatchAjax(){

        $data = Request::instance()->param();
        $result = $this->callSoaErp('post', '/source/updatePricingConfigureBatchByPricingConfigureTypeId',$data);

        return $result;
    }

    /**
     * 承运商计价配置区间验证
     */
    public function intervalCheckAjax(){

        $data = Request::instance()->param();

        $result = $this->callSoaErp('post', '/source/IntervalCheck',$data);

        return   $result;//['code' => '400', 'msg' => $data];
    }


    /**
     * 项目计价配置添加
     */
    public function addPricingConfigureProjectAjax(){

        $data = Request::instance()->param();

        $result = $this->callSoaErp('post', '/source/addPricingConfigureProject',$data);

        return   $result;//['code' => '400', 'msg' => $data];

    }

    /**
     * 项目计价配置修改
     */
    public function editPricingConfigureProjectAjax(){

        $data = Request::instance()->param();

        $result = $this->callSoaErp('post', '/source/editPricingConfigureProject',$data);

        return   $result;//['code' => '400', 'msg' => $data];

    }

    /**
     * 承运商计价配置批量导入
     */
    public function PricingConfigureImport(){

        $user_id = session("user")['user_id'];
        $this->assign('user_id',$user_id);

        $data = Request::instance()->param();
        $this->assign('supplier_id',$data['supplier_id']);

        return $this->fetch('pricing_configure_import');
    }

    /**
     * 項目计价配置批量导入
     */
    public function PricingConfigureProjectImport(){

        $user_id = session("user_id");
        $this->assign('user_id',$user_id);

        $data = Request::instance()->param();
        $this->assign('project_id',$data['project_id']);

        return $this->fetch('pricing_configure_project_import');
    }

    /**
     * 车辆显示页面
     */
    public function showVehicleTypeManage(){
    
    	$data=[
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    	];

    	$vehicle_type_uuid = input("vehicle_type_uuid");
    	$vehicle_type_id = input("vehicle_type_id");
    	$vehicle_type_name = input("vehicle_type_name");
    	if(is_numeric(input('status'))){
    		$data['status'] = input('status');
    	}

    
    	if(!empty($vehicle_type_uuid)){
    		$data['vehicle_type_uuid'] =$vehicle_type_uuid;
    	}
    	if(!empty($vehicle_type_id)){
    		$data['vehicle_type_id'] = $vehicle_type_id;
    	}
    	
    	if(!empty($vehicle_type_name)){
    		$data['vehicle_type_name'] = $vehicle_type_name;
    	}
    
    
    
    	$result = $this->callSoaErp('post','/source/getVehicleType',$data);
    
    	$this->getPageParams($result);
    
    
    	return $this->fetch('vehicle_type_manage');
    }

    /**
     * 车辆类型显示新增页面
     */
    public function showVehicleTypeAdd(){
    
    	return $this->fetch('vehicle_type_add');
    }

    /**
     * 车辆类型新增AJAX
     */
    public function addVehicleTypeAjax(){
    
		$data = Request::instance()->param();
		
		
    	$result = $this->callSoaErp('post', '/source/addVehicleType',$data);
 
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 车辆类型修改页面
     */
    public function showVehicleTypeEdit(){
    	//读取相应供应商信息
    	$data=["vehicle_type_id"=>input("vehicle_type_id")];

    	$result = $this->callSoaErp('post', '/source/getVehicleType',$data);
    	
    	$this->assign('data',$result['data'][0]);

    	return $this->fetch('vehicle_type_edit');
    
    }    
    /**
     * 车辆类型修改AJAX
     */
    public function editVehicleTypeAjax(){

    	$data = Request::instance()->param();
    
   
    
    	$result = $this->callSoaErp('post', '/source/updateVehicleTypeByVehicleTypeId',$data);
    	return   $result;
    }    
    /**
     * 车辆显示页面
     */
    public function showVehicleManage(){
    
    	$data=[
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    	];
    	$supplier_uuid = input("supplier_uuid");
    	$number_plate= input("number_plate");
    	$vehicle_id= input("vehicle_id");
    	$vehicle_type_id= input("vehicle_type_id");
    
    	if(is_numeric(input('status'))){
    		$data['status'] = input('status');
    	}
    	if(is_numeric(input('supplier_uuid'))){
    		$data['supplier_uuid'] = input('supplier_uuid');
    	}    
    	if(!empty(input('number_plate'))){
    		$data['number_plate'] = input('number_plate');
    	}
    	if(!empty(input('number_plate'))){
    		$data['number_plate'] = input('number_plate');
    	}
    	if(!empty(input('max_load'))){
    		$data['max_load'] = input('max_load');
    	}    	
    	if(is_numeric(input('vehicle_type_id'))){
    		$data['vehicle_type_id'] = input('vehicle_type_id');
    	}
   
    	$supplie_data['status'] = 1;
    	$supplier_result = $this->callSoaErp('post','/source/getSupplier',$supplie_data);
		
	
    	$vehicle_type_data['status'] = 1;
    	$vehicle_type_result = $this->callSoaErp('post','/source/getVehicleType',$vehicle_type_data);
    	
    
    	$result = $this->callSoaErp('post','/source/getVehicle',$data);
    	
    	$this->getPageParams($result);
    
    	$this->assign('supplier_result',$supplier_result['data']);
    	$this->assign('vehicle_type_result',$vehicle_type_result['data']);
    
    	return $this->fetch('vehicle_manage');
    }    
    /**
     * 车辆显示新增页面
     */
    public function showVehicleAdd(){
    	$supplie_data['status'] = 1;
    	$supplier_result = $this->callSoaErp('post','/source/getSupplier',$supplie_data);
    	$vehicle_type_data['status'] = 1;
    	$vehicle_type_result = $this->callSoaErp('post','/source/getVehicleType',$vehicle_type_data);
    	     	
    	//如果是修改页面
    	$vehicle_id = input('vehicle_id');
		
    	if(is_numeric($vehicle_id)){
    		$vehicle_data['vehicle_id'] = $vehicle_id;
    		$vehicle_result =  $this->callSoaErp('post','/source/getVehicle',$vehicle_data);
    		$this->assign('vehicle_result',$vehicle_result['data'][0]);
    		
    	}
    	
    	
    	$this->assign('supplier_result',$supplier_result['data']);
    	$this->assign('vehicle_type_result',$vehicle_type_result['data']);    
    	return $this->fetch('vehicle_add');
    }
    /**
     * 车辆新增AJAX
     */
    public function addVehicleAjax(){
    
    	$data = Request::instance()->param();
    
    	
    	$result = $this->callSoaErp('post', '/source/addVehicle',$data);
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 车辆修改AJAX
     */
    public function editVehicleAjax(){
    
    	$data = Request::instance()->param();
    
    	
    	$result = $this->callSoaErp('post', '/source/updateVehicleByVehicleId',$data);

    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 车辆显示AJAX
     */
    public function showVehicleAjax(){
    
    	$data = Request::instance()->param();
    
    	 
    	$result = $this->callSoaErp('post', '/source/getVehicle',$data);
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 司机显示页面
     */
    public function showDriverManage(){
    
    	$data=[
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    	];


    	if(!empty(input('number_plate'))){
    		$data['number_plate'] = input('number_plate');
    	}
    	if(!empty(input('driver_name'))){
    		$data['driver_name'] = input('driver_name');
    	}
    	if(is_numeric(input('supplier_id'))){
    		$data['supplier_id'] = input('supplier_id');
    	}

    	if(is_numeric(input('vehicle_id'))){
    		$data['vehicle_id'] = input('vehicle_id');
    	}
    	 
    	$supplie_data['status'] = 1;
    	$supplier_result = $this->callSoaErp('post','/source/getSupplier',$supplie_data);
    	$vehicle_data['status'] = 1;
    	$vehicle_result = $this->callSoaErp('post','/source/getVehicle',$vehicle_data);
    	 

    	$result = $this->callSoaErp('post','/source/getDriver',$data);

    	$this->getPageParams($result);
    
    	$this->assign('supplier_result',$supplier_result['data']);
    	$this->assign('$vehicle_result',$vehicle_result['data']);
    
    	return $this->fetch('driver_manage');
    }    
    /**
     * 司机显示新增页面
     */
    public function showDriverAdd(){


    	$supplier_data['status'] = 1;
    	$supplier_result =  $this->callSoaErp('post','/source/getSupplier',$supplier_data);
    	 
    	$this->assign('supplier_result',$supplier_result['data']);
        //var_dump(Contents::getDriverLicenseType());exit();
        $this->assign('driver_license_type',Contents::getDriverLicenseType());
    	
    	//如果是修改页面
    	$driver_id = input('driver_id');
    	
    	if(is_numeric($driver_id)){
    		
  

    		
    		$driver_data['driver_id'] = $driver_id;
    		$driver_result =  $this->callSoaErp('post','/source/getDriver',$driver_data);
    		
    		$this->assign('driver_result',$driver_result['data'][0]);


    		$vehicle_data['supplier_id'] = $driver_result['data'][0]['supplier_id'];
    	
    		$vehicle_result =  $this->callSoaErp('post','/source/getVehicle',$vehicle_data);

    		$this->assign('vehicle_result',$vehicle_result['data']);
    	
    	}
    	
    	return $this->fetch('driver_add');
    }    
    
    /**
     * 司机新增AJAX
     */
    public function addDriverAjax(){
   
    	$data = Request::instance()->param();
    	

    	 
    	$result = $this->callSoaErp('post', '/source/addDriver',$data);

    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     * 司机修改AJAX
     */
    public function editDriverAjax(){

    	$data = Request::instance()->param();
    

    	$result = $this->callSoaErp('post', '/source/updateDriverByDriverId',$data);
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }    
    /**
     *  圣华
     *  客户管理显示页面
     */
    public function showCustomerManage(){
    	//读取所有的地接信息
    
    	$company_name = input("company_name");
    
    	$status = input("status");
    	$data = [
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    
    	];
    	if(!empty($company_name)){
    		$data['company_name'] = $company_name;
    	}
    	if(is_numeric($status)){
    		$data['status'] = $status;
    	}
    
    	$result = $this->callSoaErp('post','/source/getCustomer',$data);

    	$this->getPageParams($result);
    	return $this->fetch('customer_manage');
    		
    		
    }
    
    /**
     * 客户管理显示新增页面
     */
    public function showCustomerAdd(){
    
    	$customer_id = input('customer_id');
    	if(is_numeric($customer_id)){
    			
    		$data['customer_id'] = $customer_id;
    		$result = $this->callSoaErp('post','/source/getCustomer',$data);
    		$this->assign('result',$result['data'][0]);
    			
    	}
    
    
    
    	return $this->fetch('customer_add');
    }
    
    /**
     * 客户新增数据
     */
    public function addCustomerAjax(){
    	$data = Request::instance()->param();
    
    
    	if(is_numeric($data['customer_id'])){
    
    
    		$result = $this->callSoaErp('post', '/source/updateCustomerByCustomerId',$data);
    	}else{
    
    		$result = $this->callSoaErp('post', '/source/addCustomer',$data);
    	}
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     *  圣华
     *  项目管理显示页面
     */
    public function showProjectManage(){
    	//读取所有的地接信息
    	$zjm = input("zjm");
    	$project_name = input("project_name");
    
    	$status = input("status");
    
    	$data = [
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    
    	];
    
    	if(is_numeric($status)){
    		$data['status'] = $status;
    	}
    	if(!empty($zjm)){
    		$data['zjm'] = $zjm;
    	}
    	if(!empty($project_name)){
    		$data['project_name'] = $project_name;
    	}
    
    	$result = $this->callSoaErp('post','/source/getProject',$data);
	
    
    	$this->getPageParams($result);
    	return $this->fetch('project_manage');
    		
    		
    }

    /**
     * 项目新增页面
     */
    public function showProjectAdd(){
    
    	$project_id = input('project_id');
    	$customerData['status'] = 1;
    	$customerResult = $this->callSoaErp('post','/source/getCustomer',$customerData);
    
    	$this->assign('customerResult',$customerResult['data']);
    
    
    	if(is_numeric($project_id)){
    		$projectData['project_id'] = $project_id;
    		$customerResult = $this->callSoaErp('post','/source/getProject',$projectData);
    
    		$this->assign('result',$customerResult['data'][0]);

            //获取项目下的发货方信息
            $sendGoodsResult = $this->callSoaErp('post','/source/getSendGoods',$projectData);
//            echo "<pre>";
//            var_dump($sendGoodsResult);exit;
//            echo "</pre>";
            $this->assign('sendGoodsResult',$sendGoodsResult['data']);
    	}
    
    
    
    
    	return $this->fetch('project_add');
    }
    
    /**
     * 项目新增AJAX
     */
    public function addProjectAjax(){
    	$data = Request::instance()->param();
    
    
    	if(is_numeric($data['project_id'])){
    
    
    		$result = $this->callSoaErp('post', '/source/updateProjectByProjectId',$data);
    	}else{
    
    		$result = $this->callSoaErp('post', '/source/addProject',$data);
    	}
	
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     *  圣华
     *  收货管理显示页面
     */
    public function showAcceptGoodsManage(){
    	//读取所有的项目
		$data = [];

        $projectResult = $this->callSoaErp('post','/source/getProject',[]);
    	$data = [
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    	];

        $this->assign('projectResult',$projectResult['data']);

    	if(is_numeric(input('project_id'))){
    
    		$data['project_id'] = input('project_id');
    	}
    	$status = input('status');
    	if(is_numeric($status)){
    		$data['status'] = $status;
    	}
    	if(is_numeric(input('accept_goods_cellphone'))){
    		$data['accept_goods_cellphone'] = input('accept_goods_cellphone');
    	}
    	if(!empty(input('accept_goods_name'))){
    		$data['accept_goods_name'] = input('accept_goods_name');
    	}
        if(!empty(input('accept_goods_company'))){
            $data['accept_goods_company'] = input('accept_goods_company');
        }

    	$result = $this->callSoaErp('post','/source/getAcceptGoods',$data);

        $this->assign("acceptResult", $result['data']['list']);
    	$this->getPageParams($result);
    	return $this->fetch('accept_goods_manage');
    		
    		
    }
    /**
     *  圣华
     *  获取收货信息
     */
    public function showAcceptGoodsAjax(){
    	$data = Request::instance()->param();
    	$goods['accept_goods_id'] = $data['accept_goods_id'];
    	$result = $this->callSoaErp('post','/source/getAcceptGoods',$data);
    		
    	return $result;
    }
    /**
     *  圣华
     *  获取发货信息
     */
    public function showSendGoodsAjax(){
    	$data = Request::instance()->param();
    
    	$goods['send_goods_id'] = $data['send_goods_id'];
    	$result = $this->callSoaErp('post','/source/getSendGoods',$data);
    
    	return $result;
    }
    /**
     * 收货新增页面
     */
    public function showAcceptGoodsAdd(){

        $params = Request::instance()->param();

    	$project_id = input('project_id');
    	$accept_goods_id = input('accept_goods_id');
    	//获取所有城市
    
    
    	//获取所有城市
    
    	$cityParams['stauts'] =1;
    	$cityParams['level'] = 1;
    	$city =  $this->callSoaErp('post','/source/getCity',$cityParams);
    
    
   
    	$this->assign('sheng',$city['data']);
    
    	//获取所有项目
    
    
    	$projectResult = $this->callSoaErp('post','/source/getProject',[]);
    
    
    
    	$this->assign('projectResult',$projectResult['data']);
    
    
    
    
    	if(is_numeric($accept_goods_id)){
    		$projectData['accept_goods_id'] = $accept_goods_id;
    		$customerResult = $this->callSoaErp('post','/source/getAcceptGoods',$projectData);
    		$this->assign('result',$customerResult['data'][0]);
    	
    		//通过拿到的省再去拿市数据
    		$shiParams['parent_id'] = $customerResult['data'][0]['province_id'];
    		
    		$cityResult =  $this->callSoaErp('post','/source/getCity',$shiParams);
    		$this->assign('cityResult',$cityResult['data']);
    		
    		
    		
//     		//通过拿到的市再去区
    		$quParams['parent_id'] = $customerResult['data'][0]['city_id'];

    		$areaResult =  $this->callSoaErp('post','/source/getCity',$quParams);
    		$this->assign('areaResult',$areaResult['data']);
    		
//     		//拿到站
     		$daoParams['city_id'] = $customerResult['data'][0]['city_id'];
    	
     		$daoResult =  $this->callSoaErp('post','/source/getCityInfo',$daoParams);
     		

    		$this->assign('daozhanResult',$daoResult['data']); 
  
    	}
    
        if(isset($params['nomenu'])) {
            if ($params['nomenu'] == 2) {
                return $this->fetch('accept_goods_add_nomenu2');
            }
            return $this->fetch('accept_goods_add_nomenu');
        }else {
            return $this->fetch('accept_goods_add');
        }
    }
    
    /**
     * 收货新增AJAX
     */
    public function addAcceptGoodsAjax(){
    	$data = Request::instance()->param();
    
    	//echo json_encode($data,256);exit();

    	if(is_numeric($data['accept_goods_id'])){
    		$result = $this->callSoaErp('post', '/source/updateAcceptGoodsByAcceptGoodsId',$data);
    	}else{
    
    		$result = $this->callSoaErp('post', '/source/addAcceptGoods',$data);
    	}

    
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     *  发货管理显示页面
     */
    public function showSendGoodsManage(){
    	//读取所有的项目
    
    
    	$projectResult = $this->callSoaErp('post','/source/getProject',$data);
    	$this->assign('projectResult',$projectResult['data']);
    
    	$data = [
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    
    	];
    	if(is_numeric(input('project_id'))){
    
    		$data['project_id'] = input('project_id');
    	}
    	$status = input('status');
    	if(is_numeric($status)){
    		$data['status'] = $status;
    	}
    	if(is_numeric(input('send_goods_cellphone'))){
    		$data['send_goods_cellphone'] = input('send_goods_cellphone');
    	}
    	if(is_numeric(input('send_goods_name'))){
    		$data['send_goods_name'] = input('send_goods_name');
    	}
    	
    	$result = $this->callSoaErp('post','/source/getSendGoods',$data);

        $this->assign("sendResult", $result['data']['list']);
    	$this->getPageParams($result);
    	return $this->fetch('send_goods_manage');
    		
    		
    }
    /**
     * 发货新增页面
     */
    public function showSendGoodsAdd(){
        $params = Request::instance()->param();
    
    
    	$project_id = input('project_id');
    	$send_goods_id = input('send_goods_id');
    	//获取所有城市
    
    	$cityParams['stauts'] =1;
    	$cityParams['level'] = 1;
    	$city =  $this->callSoaErp('post','/source/getCity',$cityParams);
    
    
    
    	$this->assign('sheng',$city['data']);
    
    	//获取所有项目
    
    
    	$projectResult = $this->callSoaErp('post','/source/getProject',[]);
    
    	$this->assign('projectResult',$projectResult['data']);
    
    
    
    
    	if(is_numeric($send_goods_id)){
    		$projectData['send_goods_id'] = $send_goods_id;
    		$customerResult = $this->callSoaErp('post','/source/getSendGoods',$projectData);
			
    		$this->assign('result',$customerResult['data'][0]);
    	
    		
    
    		//通过拿到的省再去拿市数据
    		$shiParams['parent_id'] = $customerResult['data'][0]['province_id'];

    		$cityResult =  $this->callSoaErp('post','/source/getCity',$shiParams);
    		$this->assign('cityResult',$cityResult['data']);
    		
    		
    		
    		//通过拿到的市再去区
    		$quParams['parent_id'] = $customerResult['data'][0]['city_id'];

    		$areaResult =  $this->callSoaErp('post','/source/getCity',$quParams);
		
    		$this->assign('areaResult',$areaResult['data']);
    		
    		//拿发站
    		$faParams['city_id'] = $customerResult['data'][0]['city_id'];
    		
    		$faResult =  $this->callSoaErp('post','/source/getCityInfo',$faParams);
    		$this->assign('fazhanResult',$faResult['data']);    
//     		$this->assign('areaResult',$areaResult);
    	}
    
    
    if(isset($params['nomenu']))
        return $this->fetch('send_goods_add_nomenu');
        else
    	return $this->fetch('send_goods_add');
    }
    
    /**
     * 发货新增AJAX
     */
    public function addSendGoodsAjax(){
    	$data = Request::instance()->param();
    
    
    	if(is_numeric($data['send_goods_id'])){
    
    
    		$result = $this->callSoaErp('post', '/source/updateSendGoodsBySendGoodsId',$data);
    	}else{
    
    		$result = $this->callSoaErp('post', '/source/addSendGoods',$data);
    	}
    
    

    	return   $result;//['code' => '400', 'msg' => $data];
    }
    /**
     *  货物显示页面
     */
    public function showGoodsManage(){
    	//读取所有的项目
    
    
    	$projectResult = $this->callSoaErp('post','/source/getProject',$data);
		
		
    	$this->assign('projectResult',$projectResult['data']);
    
    	$data = [
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    
    	];
    	if(is_numeric(input('project_id'))){
    
    		$data['project_id'] = input('project_id');
    	}
    	$status = input('status');
    	if(is_numeric($status)){
    		$data['status'] = $status;
    	}
    
    	if(!empty(input('goods_name'))){
    		$data['goods_name'] = input('goods_name');
    	}
    	if(is_numeric(input('goods_packaging_type'))){
    		$data['goods_packaging_type'] = input('goods_packaging_type');
    	}
    	$result = $this->callSoaErp('post','/source/getGoods',$data);
		for($i=0;$i<count($result['data']['list']);$i++){
			$result['data']['list'][$i]['goods_packaging_type_name']=Contents::baseConfig()['order']['goods_pack'][$result['data']['list'][$i]['goods_packaging_type']];	
			
		}
		

    	$this->getPageParams($result);
    	return $this->fetch('goods_manage');
    		
    		
    }

    public function addOrderOperatingAjax(){
        $params = Request::instance()->param();
        $Result = $this->callSoaErp('post', '/source/addOrderOperating', $params);
        return json($Result);
    }

    //获取商品
    public  function getGoodsAjax(){
        $params = Request::instance()->param();
        $goodsResult="";
        if(is_numeric($params['goods_id'])) {
            $projectData['goods_id'] = $params['goods_id'];
            $goodsResult = $this->callSoaErp('post', '/source/getGoods', $projectData);
            return json($goodsResult['data'][0]); exit();
        }
        else
            return false;
    }

    /**
     * 货物新增页面
     */
    public function showGoodsAdd(){
    
    
    
    	$project_id = input('project_id');
    	$goods_id = input('goods_id');
        $nomenu = input('nomenu');
    	//获取所有城市
    
    
    
    
    	//获取所有项目
    
    
    	$projectResult = $this->callSoaErp('post','/source/getProject',[]);
    
    	$this->assign('projectResult',$projectResult['data']);
    
    
    
    
    	if(is_numeric($goods_id)){
    		$projectData['goods_id'] = $goods_id;
    		$goodsResult = $this->callSoaErp('post','/source/getGoods',$projectData);
    
    		$this->assign('result',$goodsResult['data'][0]);
    
    
    	}

        if($nomenu==1){
            return $this->fetch('goods_add_nomenu');
        }else{
            return $this->fetch('goods_add');
        }
    }
    
    /**
     * 货物新增AJAX
     */
    public function addGoodsAjax(){
    	$data = Request::instance()->param();
    
    
    	if(is_numeric($data['goods_id'])){
    
    
    		$result = $this->callSoaErp('post', '/source/updateGoodsByGoodsId',$data);
    	}else{
    
    		$result = $this->callSoaErp('post', '/source/addGoods',$data);
    	}
    
    
    	return   $result;//['code' => '400', 'msg' => $data];
    }    
    /**
     *  圣华
     *  承运商线路
     */
    public function showSupplierLine(){
  

    	$supplier_uuid = input("supplier_uuid");
    	
    	$supplierData['status'] = 1;

			
    	$supplierResult = $this->callSoaErp('post','/source/getSupplier',$supplierData);
    	
    	
    	 
    	$this->assign('supplierResult',$supplierResult['data']);
    	
    	
    	
    	    
    	$status = input("status");
    
    	$data = [
    			'page'=>$this->page(),
    			'page_size'=>$this->_page_size,
    
    
    	];
    

    	if(is_numeric($status)){
    		$data['status'] = $status;
    	}
    	if(!empty($supplier_uuid)){
			$data['supplier_uuid'] = $supplier_uuid;
		}
 
    	$result = $this->callSoaErp('post','/source/getSupplierLine',$data);
	
    	$this->getPageParams($result);
    	return $this->fetch('supplier_line_manage');
    
    
    }
    /**
     * 承运商线路新增
     */
    public function showSupplierLineAdd(){
    

    	
    	$supplierData['status'] = 1;

    	$supplierResult = $this->callSoaErp('post','/source/getSupplier',$supplierData);
    
	
    	
    	$this->assign('supplierResult',$supplierResult['data']);
    
    	//获取省市
    	
    	$cityData['level'] = 1;
    	
    	$cityResult = $this->callSoaErp('post','/source/getCity',$cityData);
    	
    	$this->assign('cityResult',$cityResult['data']);
    
    	
    	$supplier_line_uuid = input('supplier_line_uuid');

    	if(!empty($supplier_line_uuid)){
    		$supplierLineData['supplier_line_uuid'] = $supplier_line_uuid;

    		$supplierLineResult = $this->callSoaErp('post','/source/getSupplierLine',$supplierLineData);
		
    		$this->assign('result',$supplierLineResult['data'][0]);
    		
    		$startCityData['parent_id'] = $supplierLineResult['data'][0]['start_province_id'];
    		
    		$startCityResult = $this->callSoaErp('post','/source/getCity',$startCityData);
    		
    		$this->assign('startCityResult',$startCityResult['data']);
    		
			$faParams['city_id']  = $supplierLineResult['data'][0]['start_city_id'];
			
		
    		$faResult = $this->callSoaErp('post', '/source/getCityInfo',$faParams);
				
    		$this->assign('faResult',$faResult['data']);
    		
    		
    	
    		
    		$endCityData['parent_id'] = $supplierLineResult['data'][0]['end_province_id'];
    		
    		$endCityResult = $this->callSoaErp('post','/source/getCity',$endCityData);
    		
    		$this->assign('endCityResult',$endCityResult['data']);

    		$daoParams['city_id']  = $supplierLineResult['data'][0]['end_city_id'];
    		$daoResult = $this->callSoaErp('post', '/source/getCityInfo',$daoParams);
    		$this->assign('daoResult',$daoResult['data']);
    	
    	}
    
    	
    	//
    
    
    
    	return $this->fetch('supplier_line_add');
    }
    
    /**
     * 承运商线路AJAX
     */
    public function addSupplierLineAjax(){
    	$data = Request::instance()->param();

    	
	
 
    	if(!empty($data['supplier_line_uuid'])){
    
    
    		$result = $this->callSoaErp('post', '/source/updateSupplierLine',$data);
    	}else{
    
    		$result = $this->callSoaErp('post', '/source/addSupplierLine',$data);
    	}
    
		
    	return   $result;//['code' => '400', 'msg' => $data];
    }
    public function getSupplierLineAjax(){
	
        $data = Request::instance()->param();

        $result = $this->callSoaErp('post', '/source/getSupplierLine',$data);

        return   json($result);
    }
    
    //获取城市的站点
    public function getCityInfo(){
    	
    	$data = Request::instance()->param();

    	$result = $this->callSoaErp('post', '/source/getCityInfo',$data);
    	return $result['data'];
    }

    //客服用承运商
    public function showCustomerSupplierManage(){
        return $this->fetch('customer_supplier_manage');
    }

    //客服用发货客户
    public function showCustomerSendGoodsManage(){
        return $this->fetch('customer_send_goods_manage');
    }

    //获取客服用承运商动态数据
    public function getCustomerSupplierManageAjax(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit']
        ];

        $supplier_name = input("supplier_name");
        if(is_numeric(input('status'))){
            $data['status'] = input('status');
        }

        if(!empty($supplier_name)){
            $data['supplier_name'] = $supplier_name;
        }

        $result = $this->callSoaErp('post','/source/getCustomerSupplier',$data);

        $new = [
            "code"=> 200,
            "count"=>$result['data']['count'],
            "data"=>$result['data']['list']

        ];
        return $new;

    }

    //获取客服用发货客户动态数据
    public function getCustomerSendGoodsManageAjax(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit']
        ];

        $send_goods_name = input("send_goods_name");
        if(is_numeric(input('status'))){
            $data['status'] = input('status');
        }

        if(!empty($send_goods_name)){
            $data['send_goods_name'] = $send_goods_name;
        }

        $result = $this->callSoaErp('post','/source/getCustomerSendGoods',$data);

        $new = [
            "code"=> 200,
            "count"=>$result['data']['count'],
            "data"=>$result['data']['list']

        ];
        return $new;
    }

    //客服承运商页面
    public function showCustomerSupplierAdd(){

        return $this->fetch('customer_supplier_add');

    }

    //获取客服用发货客户页面
    public function showCustomerSendGoodsAdd(){

        return $this->fetch('customer_send_goods_add');

    }

    //添加客服承运商
    public function addCustomerSupplierAjax(){
        $supplier_name = input("supplier_name");
        $remark = input("remark");
        $status = input("status");
        $data = [
            "supplier_name"=>$supplier_name,
            "remark"=>$remark,
            "status"=>$status,
        ];

        $result = $this->callSoaErp('post', '/source/addCustomerSupplier',$data);
        return $result;
    }

    //添加发货客户
    public function addCustomerSendGoodsAjax(){
        $send_goods_name = input("send_goods_name");
        $send_name = input("send_name");
        $phone = input("phone");
        $address = input("address");
        $remark = input("remark");
        $status = input("status");
        $data = [
            "send_goods_name"=>$send_goods_name,
            "send_name"=>$send_name,
            "phone"=>$phone,
            "address"=>$address,
            "remark"=>$remark,
            "status"=>$status,
        ];

        $result = $this->callSoaErp('post', '/source/addCustomerSendGoods',$data);
        return $result;
    }

    //修改客服承运商界面
    public function showCustomerSupplierEdit(){

        //读取相应供应商信息
        $data=[
            "supplier_id"=>input("supplier_id")
        ];
        $result = $this->callSoaErp('post', '/source/getCustomerSupplier',$data);

        $this->assign('data',$result['data'][0]);

        return $this->fetch('customer_supplier_edit');

    }

    //修改发货客户界面
    public function showCustomerSendGoodsEdit(){

        //读取相应供应商信息
        $data=[
            "customer_send_goods_id"=>input("customer_send_goods_id")
        ];
        $result = $this->callSoaErp('post', '/source/getCustomerSendGoods',$data);

        $this->assign('data',$result['data'][0]);

        return $this->fetch('customer_send_goods_edit');
    }

    //修改客服承运
    public function editCustomerSupplierAjax(){
        $supplier_id = input("supplier_id");
        $supplier_name = input("supplier_name");
        $remark = input("remark");

        $data = [
            "supplier_id"=>$supplier_id,
            "supplier_name"=>$supplier_name,
            "remark"=>$remark,
        ];

        $result = $this->callSoaErp('post', '/source/updateCustomerSupplierByCustomerSupplierId',$data);

        return   $result;
    }

    //修改发货客户
    public function editCustomerSendGoodsAjax(){
        $customer_send_goods_id = input("customer_send_goods_id");
        $send_goods_name = input("send_goods_name");
        $send_name = input("send_name");
        $phone = input("phone");
        $address = input("address");
        $remark = input("remark");

        $data = [
            "customer_send_goods_id"=>$customer_send_goods_id,
            "send_goods_name"=>$send_goods_name,
            "send_name"=>$send_name,
            "phone"=>$phone,
            "address"=>$address,
            "remark"=>$remark,
        ];

        $result = $this->callSoaErp('post', '/source/updateCustomerSendGoodsByCustomerSendGoodsId',$data);

        return   $result;
    }

    //项目计价配置
    public function showProjectPricingConfigureManage(){

        $data=[
            'page'=>$this->page(),
            'page_size'=>1800,

        ];
        $project_id= input("project_id");

        if(!empty($project_id)){
            $data['project_id'] = $project_id;
        }

        //获取项目信息
        $result = $this->callSoaErp('post','/source/getProject',$data);

        //获取项目计价配置信息
        $result2 = $this->callSoaErp('post','/source/getProjectPricingConfigure',$data);

        $data['type'] = 1;
        //获取项目配置信息
        $result3 = $this->callSoaErp('post','/source/getProjectRule',$data);

        $this->assign("projectResult", $result['data']['list'][0]);
        $this->getPageParams($result2);
        $this->assign("projectruleResult", $result3['data']['list']);
        $this->assign("count", count($result3['data']['list']));

        return $this->fetch('project_pricing_configure_manage');
    }

    //导出
    public function export(){

        $project_data = Request::instance()->param();
        $projectReturn = $this->callSoaErp('post','/source/getProject',$project_data);
        $project_name = $projectReturn['data'][0]['project_name'];

        $name = $project_name."-收货人信息";
        $xAxis = [];
        $title = $project_name."-收货人信息";
        $header = array('收货方ID', '项目名称', '收货方', '省', '市', '区', '到站', '地址', '收货人', '联系电话', '备注', '创建人', '创建时间', '状态');

        $data = $this->callSoaErp('post','/source/getAcceptGoodsExport',$project_data);
//        echo "<pre>";
//        var_dump($data['data']);exit;
//        echo "</pre>";
        //导出excel
        $excel = Demo::export_accept_goods($xAxis, $title, $header, $data['data'], $name);

    }
}

