<?php

namespace app\index\controller;

use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;
class Wisdomcarpark extends Base
{


  //  $data = Request::instance()->param();
    

	 /**
	  * 获取预约
	  */
	 public function showAppointment(){
	 
	 	$data=[
	 			'pageNo'=>$this->page(),
	 			'pageSize'=>$this->_page_size,
	 
	 	];
	 	$result = action('haikang/get_person_list',['postData' => $data]);
	 	 

	
	 	 
	 
	 	return $this->fetch('appointment');
	 }
	   
	 
	 /**
	  * 停车位信息
	  */
	 public function showParkingSpace(){
	 	$data=[
	 			'pageNo'=>$this->page(),
	 			'pageSize'=>$this->_page_size,
	 	
	 	];
	 	$result = action('haikang/parking_space',['postData' => $data]);
	 	$list = $result['data']['list'];
	 	
	 	
	 	foreach ($list as $key => $row) {
	 		$begin[$key] = $row['spaceNo'];
	 		 
	 	}
	
	 	//升序处理行程时间
	 	array_multisort($begin,SORT_ASC,$list);
	 	


	 	$result['data']['list'] = $list;
	 	$this->getHaikangPageParams($result);
	 	 
	 	
	 	return $this->fetch('parking_space');	 
	 	
	 }
    /**
     * 新增预约
     */
    public function showAppointmentAdd(){

		//获取车位
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,
    			 
    	];
    	$result = action('haikang/parking_space',['postData' => $data]);
    	$list = $result['data']['list'];
    	 
    	 
    	foreach ($list as $key => $row) {
    		$begin[$key] = $row['spaceNo'];
    			
    	}
    	
    	//升序处理行程时间
    	array_multisort($begin,SORT_ASC,$list);
    	 
		$this->assign('spaceNo',$list);

       return $this->fetch('appointment_add');
    }    
    
    public function appointmentAddAjax(){
    	$data = Request::instance()->param();
    	$data['space_syscode'] = substr($data['space_info'],0,strpos($data['space_info'],','));
    	$data['space_no'] = substr($data['space_info'],strpos($data['space_info'],',')+1);
    	$data['start_time'] = strtotime($data['start_time']);
    	$data['end_time'] =  strtotime($data['end_time']);
    	unset($data['space_info']);
 
    	$result = $this->callSoaErp('post', '/dispatch/uploadDispatchChild', $data);    	
    	
    	
    }
    
    
}

