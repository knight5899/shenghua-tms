<?php

/**
 * 助手函数类，主要就是解决一些公用的函数所用
 */

namespace app\common\help;


use think\Db;
use Underscore\Types\Arrays;
use think\Cache;
use phpmailer\PHPMailer;

/**
 * Description of Helper
 *
 * @author 胡
 */
class Help
{
    public static function http($method, $url, $data = null)
    {

        //1初始化，创建一个新cURL资源

        $ch = curl_init();
        if ($method == 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
            // Accept:application/json; charset=utf-8;Content-Type:application/x-www-form-urlencoded;charset=utf-8
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept:application/json'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    // https请求 不验证证书和hosts
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));//http_build_query

        }
        //2设置URL和相应的选项
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        //3抓取URL并把它传递给浏览器

        $output = curl_exec($ch);

        return $output;
        //4关闭cURL资源，并且释放系统资源

        curl_close($ch);
        exit();

    }

    public static function callSoaErp($method, $function, $data = [])
    {
        $soaerp_config = config('soaerp');
        $soa_erp_url = $soaerp_config['ip'] . ':' . $soaerp_config['port'];
        $data['appKey'] = 'nexus';
        $data['appSecret'] = 'nexusIt';

        $result = Help::http($method, $soa_erp_url . $function, $data);

        //dump($result);exit();
        $result = json_decode($result, true);

        if ($result['code'] == 200) {
            //$this->outPut($result['data']);
            $result = ['code' => '200', 'msg' => 'success', 'data' => $result['data']];

        } else {
            //$this->outPutError($result);
            $result = ['code' => '400', 'msg' => json_encode($result, true)];
        }
        return $result;
    }


    //把URL中的参数page删除
    public static function delUrlPage($url)
    {
        $array = explode('&', $url);
        $new_array = [];
        foreach ($array as $key => $v) {

            if (substr($v, 0, strpos($v, '=')) != 'page') {
                $new_array[] = $v;
            }
        }
        return implode('&', $new_array);
    }

    public static function toData($array, $route_type_id)
    {
        //声明静态数组,避免递归调用时,多次声明导致数组覆盖
        static $list = [];
        foreach ($array as $key => $value) {
            //第一次遍历,找到父节点为根节点的节点 也就是pid=0的节点
            if ($value['pid'] == $route_type_id) {
                //把数组放到list中
                $list[] = $value;
                //把这个节点从数组中移除,减少后续递归消耗
                //unset($array[$key]);
                //开始递归,查找父ID为该节点ID的节点,级别则为原级别+1
                Help::toData($array, $value['route_type_id']);
            }
        }
        foreach ($array as $k => $v) {
            foreach ($list as $k1 => $v1) {
                if ($v['route_type_id'] == $v1['route_type_id']) {
                    unset($array[$k]);
                }
            }
        }
        foreach ($array as $k => $v) {
            if ($v['route_type_id'] == $route_type_id) {
                unset($array[$k]);
            }
        }
        return $array;
    }


    public static function toArrData($array, $tour_type_id)
    {
        //声明静态数组,避免递归调用时,多次声明导致数组覆盖
        static $list = [];
        foreach ($array as $key => $value) {
            //第一次遍历,找到父节点为根节点的节点 也就是pid=0的节点
            if ($value['pid'] == $tour_type_id) {
                //把数组放到list中
                $list[] = $value;
                //把这个节点从数组中移除,减少后续递归消耗
                //unset($array[$key]);
                //开始递归,查找父ID为该节点ID的节点,级别则为原级别+1
                Help::toData($array, $value['tour_type_id']);
            }
        }
        foreach ($array as $k => $v) {
            foreach ($list as $k1 => $v1) {
                if ($v['tour_type_id'] == $v1['tour_type_id']) {
                    unset($array[$k]);
                }
            }
        }
        foreach ($array as $k => $v) {
            if ($v['tour_type_id'] == $tour_type_id) {
                unset($array[$k]);
            }
        }
        return $array;
    }

    /**
     * 模型数据转数组
     * @param $data
     * @return mixed
     */
    public static function modelDataToArr($data)
    {
        return json_decode(json_encode($data), true);
    }

    /**
     * 二维数组根据某字段进行排序
     * sort 1升序 2倒序
     */
    public static function arraySort($array, $string, $sort = 1)
    {
        if ($sort == 1) {
            $sort_sting = 'SORT_ASC';
        } else {
            $sort_sting = 'SORT_DESC';
        }

        return array_multisort($array, $sort_sting, array_column($array, $string));
    }

    //curl 上传图片
    public static function curlImages($path, $url)
    {

        $curl = curl_init();
        if (class_exists('\CURLFile')) {
            curl_setopt($curl, CURLOPT_SAFE_UPLOAD, true);
            $data = array('file' => new \CURLFile(realpath($path)));//>=5.5
        } else {
            if (defined('CURLOPT_SAFE_UPLOAD')) {
                curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
            }
            $data = array('file' => '@' . realpath($path));//<=5.5
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "TEST");

        $result = curl_exec($curl);
        $error = curl_error($curl);
        return $result;
    }


    /*
     *
     *  多语言替换
     *  @author yyy
     *
     *  @param array $data    所有语言标签
     *  @param int $language_id  当前用户的语言id
     *  @return mix $data    返回替换后的标签 | 原数据
     */
    public static function languageReplace($data, $language_id, $user_id)
    {

        if (!is_array($data)) {
            return $data;
        }

        $result = Arrays::group($data, 'language_id');

        $arr = [];
        foreach ($result['1'] as $v) {
            $arr[$v['code_name']] = $v['tag_name'];
        }

        foreach ($result[$language_id] as $value) {
            $arr[$value['code_name']] = $value['tag_name'];
        }

        Cache::set('tag_language' . $user_id, json_encode($arr));

    }

    public static function toTree($array, $pid = 0, $level = 0, $id = 'route_type_id')
    {
        $array = json_decode(json_encode($array), true);

        //声明静态数组,避免递归调用时,多次声明导致数组覆盖
        static $list = [];
        foreach ($array as $key => $value) {
            //第一次遍历,找到父节点为根节点的节点 也就是pid=0的节点
            if ($value['pid'] == $pid) {
                //父节点为根节点的节点,级别为0，也就是第一级
                $value['level'] = $level;
                //把数组放到list中
                $list[] = $value;
                //把这个节点从数组中移除,减少后续递归消耗
                unset($array[$key]);
                //开始递归,查找父ID为该节点ID的节点,级别则为原级别+1
                Help::toTree($array, $value[$id], $level + 1, $id);
            }
        }
        return $list;

    }

    /**
     * 获取上月最后一天
     */
    public static function getLastMonthDay()
    {

        return date('Ymd', strtotime(date('Y-m-01') . ' -1 day'));
    }
    //转换时区

    /**
     *
     * @param unknown $date_time 请传2019-11-10 22:10:10 格式不要传时间戳
     * @param string $format
     * @param string $to 转换后的时区格式 Europe/Rome
     * @param string $from 转换前的时区格式 Asia/Shanghai
     */
    public static function changeTimeZone($date_time, $format = 'Y-m-d H:i:s', $from, $to = 'Asia/Shanghai')
    {

        $datetime = new \DateTime($date_time, new \DateTimeZone($from));
        $datetime->setTimezone(new \DateTimeZone($to));
        return $datetime->format($format);
    }

    /**
     * 获取该城市下的所有城市
     */
    public static function city2data($city_name, $level)
    {
        $cityString = file_get_contents('map.json');
        $cityResult = json_decode($cityString, true);
        if (!$cityString) {


            $cityData = self::callSoaErp('post', '/city/getCity', "")['msg'];
            file_put_contents('map.json', $cityData);
            $cityResult = $cityData;
        }

        foreach ($cityResult as $k => $v) {
            if (($city_name == $v['city_name'] || $city_name == $v['city_short_name']) && $level == $v['level'])
                return $v;

        }
        return false;

    }


    /**
     * 获取该城市下的所有城市
     */
    public static function getCity($city_id, $level = 0)
    {
        $file_name = "map.json";
        if (!file_exists($file_name)) {
            $cityData = self::callSoaErp('post', '/city/getCity', [])['msg'];
            $cityString = $cityData;
            $file = fopen($file_name, "w");
            fwrite($file, $cityData);
            fclose($file);
        } else {
            $file = fopen($file_name, "r");
            $cityString = fread($file, filesize($file_name));
            fclose($file);
        }


        $cityResult = json_decode($cityString, true);
        if (count($cityResult) < 10) {
            echo "错误日志:" . $cityString . "<br>";
            echo "地图数据获取失败或者错误,请删除" . $file_name . "再试";
            exit();
            return;
        }
        if (!$cityResult) return [];
        $cityContent = [];

        if ($level <= 1 || $city_id < 1) {

            for ($i = 0; $i < count($cityResult); $i++) {
                if ($cityResult[$i]['level'] == $level) {
                    $cityContent[] = $cityResult[$i];
                }
            }
        } else {
            for ($i = 0; $i < count($cityResult); $i++) {
                if ($cityResult[$i]['parent_id'] == $city_id) {
                    $cityContent[] = $cityResult[$i];
                }
            }
        }


        return array_values($cityContent);
    }

    /**
     * 获取该城市下的所有城市
     */
    public static function getCityAndArea($city_id, $level = 0)
    {
        $data = [
            "city_id" => $city_id,
            "level" => $level
        ];
        $cityData = self::callSoaErp('post', '/city/getCityAndArea', $data);
        return $cityData['data'];
    }

    //获取支付类型
    public static function getPayType($id)
    {
        $data = Contents::payTypeData();

        for ($i = 0; $i < count($data); $i++) {
            if ($id == $data[$i]['id']) {
                return $data[$i]['pay_type'];
                break;
            }
        }
    }

    //获取运输类型
    public static function getTransportationType($id)
    {

        $data = Contents::transportationTypeData();

        for ($i = 0; $i < count($data); $i++) {
            if ($id == $data[$i]['id']) {
                return $data[$i]['transportation_type'];
                break;
            }
        }
    }

    //获取包装类型
    public static function getGoodsPackagingType($id)
    {
        $data = Contents::goodsPackagingType();

        for ($i = 0; $i < count($data); $i++) {
            if ($id == $data[$i]['id']) {
                return $data[$i]['goods_packaging_type_name'];
                break;
            }
        }
    }

    //获取状态
    public static function getOrderStatus($id)
    {
        $data = Contents::orderStatus();

        for ($i = 0; $i < count($data); $i++) {
            if ($id == $data[$i]['id']) {
                return $data[$i]['order_status_name'];
                break;
            }
        }
    }


    public static function getOrder($result)
    {

        if (isset($result['code'])) {

            for ($i = 0; $i < count($result['data']['list']); $i++) {

                $result['data']['list'][$i]['pay_type_name'] = Help::getPayType($result['data']['list'][$i]['pay_type']);
                $result['data']['list'][$i]['transportation_type_name'] = Help::getTransportationType($result['data']['list'][$i]['transportation_type']);
                //$result['data']['list'][$i]['goods_packaging_type_name'] = Help::getGoodsPackagingType($result['data']['list'][$i]['goods_packaging_type']);
                $result['data']['list'][$i]['order_status_name'] = Help::getOrderStatus($result['data']['list'][$i]['order_status']);
                $k = $result['data']['list'][$i];

                for ($j = 0; $j < count($k['orders_goods_info']); $j++) {


                    $result['data']['list'][$i]['orders_goods_info'][$j]['goods_packaging_type_name'] = Help::getGoodsPackagingType($k['orders_goods_info'][$j]['goods_packaging_type']);

                }
            }
            return $result;
        }


    }

    /**
     * 获取仓库公司信息
     */
    public static function getCompany($ware_id)
    {
        $cityData = Db::name('wms_warehouse')->
        join("company", 'company.company_id= wms_warehouse.company_id')->
        where(['ware_id' => $ware_id])->select();
        return $cityData;
    }

    /**
     * 获取会员信息
     */
    public static function getUser($user_id)
    {
        $cityData = Db::name('user')->where(['user_id' => $user_id])->select();
        return $cityData;
    }

    /**
     * 获取15位随机数包含大小写数字-对账编号
     */
    public static function getReconciliation()
    {

        $head = "F";
        $date = date("Ymdhis");
        $shuzi = array_merge(range(0, 9));
        $xiaoxie = array_merge(range('a', 'z'));
        $daxie = array_merge(range('A', 'Z'));
        $str = $head . $date . $xiaoxie[rand(0, 25)] . $xiaoxie[rand(0, 25)];

        return $str;
    }

    /**
     * 获取15位随机数包含大小写数字-承运商对账编号
     */
    public static function getSupplierReconciliation()
    {

        $head = "S";
        $date = date("Ymdhis");
        $shuzi = array_merge(range(0, 9));
        $xiaoxie = array_merge(range('a', 'z'));
        $daxie = array_merge(range('A', 'Z'));
        $str = $head . $date . $xiaoxie[rand(0, 25)] . $xiaoxie[rand(0, 25)];

        return $str;
    }

    /**
     * 获取19位随机数包含大小写数字-WMS仓库代码
     */
    public static function getWmsWarehouseCode()
    {
        $head = "WMS";
        $date = date("Ymdhis");
        $shuzi = array_merge(range(0, 9));
        $xiaoxie = array_merge(range('a', 'z'));
        $daxie = array_merge(range('A', 'Z'));
        $str = $head . $date . $xiaoxie[rand(0, 25)] . $xiaoxie[rand(0, 25)];

        return $str;
    }

    /**
     * 获取19位随机数包含大小写数字-WMS仓库区域代码
     */
    public static function getRegionCode()
    {
        $head = "qy";
        $date = date("Ymdhis");
        $shuzi = array_merge(range(0, 9));
        $xiaoxie = array_merge(range('a', 'z'));
        $daxie = array_merge(range('A', 'Z'));
        $str = $head . $date . $xiaoxie[rand(0, 25)] . $xiaoxie[rand(0, 25)];

        return $str;
    }
	
	
	//发送邮件
	public static function sendEmail($params){
		$mail = new \Mail;
            //调用消息发送
        $tags = $mail->sendMail($params['send_email'], $params['send_name'],$params['send_title'], $params['send_content']);		
		return $tags;
	}
}
