<?php

/**
 * 助手函数类，主要就是解决一些公用的函数所用
 */

namespace app\common\help;

use think\config;

/**
 * Description of Helper
 *
 * @author 胡
 */
class Contents
{
    private $_lianyi_email;
    /**
     * 分页显示数量
     */
    const PAGE_SIZE = 10;
    const PAGE_SIZE_MAX = 10000000;

    public function baseConfig()
    {//表名->字段名
        return [
            'source'=>[
                'billing_unit'=>[1=>'重量',2=>'体积',3=>'件数',4=>'数量',5=>'按单',6=>'包车',7=>'按瓶']
            ],
            'short_barge' => [
                'share_type' => [0 => '自定义', 1 => '按运单', 2 => '按件数', 3 => "按重量", 4 => "按体积"]

            ],
            'supplier' => [
                'supplier_type' => [1 => "自营", 2 => "第三方", 3 => "个体户"]
            ],


            'despatch' => [
                'billing_unit' => [1 => '重量', 2 => '体积', 3 => '件数', 4 => '数量', 5 => '按单']
                , 'pay_type' => [1 => '现付', 2 => '未付', 3 => '油卡', 4 => '现付油卡', 5 => '现付未付', 6 => '油卡未付', 7 => '现付未付油卡', 8 => '免运费']
            ],
			'goods_pack' => [
            '0' => ['id' => 1, 'goods_packaging_type_name' => '纸箱'],
            '1' => ['id' => 2, 'goods_packaging_type_name' => '托盘'],
            '2' => ['id' => 3, 'goods_packaging_type_name' => '桶'],
            '3' => ['id' => 4, 'goods_packaging_type_name' => '袋装'],
            '3' => ['id' => 5, 'goods_packaging_type_name' => '编织袋'],


			],
            'order' => [
                'receipt_status'=>[0=>"未回",1=>"已回",2=>"已送",3=>"其他"]
                ,
                'bargain_type'=>[
                    1=>"合同",
                    2=>"单票"
                ],
                'abnormal_type' => [
                    1 => "理赔",
                    2 => "无需理赔",
                    3 => "返还",
                    4 => "无需返还"
                ],

                'goods_pack' => [
                    1 => "纸箱",
                    2 => "托盘",
                    3 => "桶",
                    4 => "袋装",
                    5 => "编织袋"
                ],
                'delivery_method' => [ 1 => '送货',2=> '自提',],
                'pay_type_name' => [1 => "现付", 2 => "到付", 3 => "月结"],
                'order_status' => [
                    1 => ['id' => 1, 'order_status_name' => '库存'],
                    2 => ['id' => 2, 'order_status_name' => '已出库'],
					3 => ['id' => 3, 'order_status_name' => '在途'],
					4 => ['id' => 4, 'order_status_name' => '到站'],
                    5 => ['id' => 5, 'order_status_name' => '配送中'],
                    6 => ['id' => 6, 'order_status_name' => '已签收'],
			
                   /* 8=>['id'=>8,'order_status_name'=>'异常签收']*/

                ],

				'verify_status' => [1=>'未核算',2=>'已核算',3=>'已对账',4=>'已开票',5=>'已收款'],
                'transportation_type_name' => ['', '公路', '铁路', '水运', '航空', '快递'],
                'goods_cost_unit' => [1 => '重量', 2 => '体积', 3 => '件数', 4 => '数量', 5 => '按单', 6 => '包车'],

            ],

			'shipment'=>[
					
				'pay_type'=>[1=>'现金',2=>'油卡',3=>'转账'],
				'goods_cost_unit'=>[1=>'重量',2=>'体积',3=>'件数',4=>'数量',5=>'按单',6=>'包车'],
				'short_barge_distribution_type'=>[1=>'自定义',2=>'按单',3=>'件数',4=>'数量',5=>'重量',6=>'体积'],
				'shipment_type'=>[1=>'包车',2=>'干线',3=>'短驳',4=>'配送'],
				'shipment_finance_status'=>[1=>'未审核',2=>'已审核',3=>'已对账',4=>'已确认',5=>'已付款'],
				'shipment_invoice_status'=>[1=>'未开票',2=>'已开票',3=>'财务已收票',4=>'已上传付款通知',5=>'已获取付款通知']
			],

			'abnormal'=>[
				'solve_type' => [1 => "理赔",2 => "无需理赔", 3 => "返还",4 => "无需返还" ],
                'abnormal_status'=>[0=>'已受理',1=>'已审核',2=>'已发放'],
                'responsible_party'=>[1=>'圣华',2=>'发货方',3=>'收货方',4=>'承运商',5=>'无法判定'],
                'damaged'=>[1=>'否',2=>'是'],
                'indemnity_type'=>[1=>'需理赔',2=>'无需理赔',3=>'待确定'],
                'return_goods_type'=>[1=>'需退货',2=>'无需退货',3=>'退货已到库']
			],
        	'bill'=>[
        			'supplier_bill_status'=>[1=>'已对账',2=>'已确认',3=>'已驳回',4=>'已关账'],
                    'customer_invoice_status'=>[1=>'未开票',2=>'已开票',3=>'已送出',4=>'不开票']
        	],
        	
			'shebei'=>[
				'type'=>[1=>'温湿度监控',2=>'随车宝',3=>'周转箱模组'],
			],

            'project_pricing_configure'=>[
                'billing_unit'=>[1=>'重量',2=>'体积',3=>'件数',4=>'数量',5=>'按单',6=>'包车',7=>'按瓶']
            ]
        		
        ];

    }


    public static function getDriverLicenseType()
    {
        return ["B1", "B2", "A1", "A2", "C1"];
    }

    //商品货物包装
    public static function goodsPackagingType()
    {

        $data = [
            '0' => ['id' => 1, 'goods_packaging_type_name' => '纸箱'],
            '1' => ['id' => 2, 'goods_packaging_type_name' => '托盘'],
            '2' => ['id' => 3, 'goods_packaging_type_name' => '桶'],
            '3' => ['id' => 4, 'goods_packaging_type_name' => '袋装'],
            '3' => ['id' => 5, 'goods_packaging_type_name' => '编织袋'],


        ];

        return $data;

    }

    //商品状态名称
    public static function orderStatus()
    {
        $data = Contents::baseConfig()['order']['order_status'];

        return $data;

    }

    //获取支付类型
    public static function payTypeData()
    {
        $data = [
            '0' => ['id' => 1, 'pay_type' => '现付'],
            '1' => ['id' => 2, 'pay_type' => '月结'],
            '2' => ['id' => 3, 'pay_type' => '到付'],
        ];

        return $data;

    }

    //获取支付类型运输类型 1公路2铁路3水运4航空5快递
    public static function transportationTypeData()
    {
        $data = [
            '0' => ['id' => 1, 'transportation_type' => '公路', 'parent_id' => 0],
            '1' => ['id' => 2, 'transportation_type' => '铁路', 'parent_id' => 0],
            '2' => ['id' => 3, 'transportation_type' => '水运', 'parent_id' => 0],
            '3' => ['id' => 3, 'transportation_type' => '航空', 'parent_id' => 0],
            '4' => ['id' => 4, 'transportation_type' => '快递', 'parent_id' => 0],
            '5' => ['id' => 100, 'transportation_type' => '整车', 'parent_id' => 1],
            '6' => ['id' => 101, 'transportation_type' => '零担', 'parent_id' => 1],

        ];
        //   $data=Contents::baseConfig()['order']['transportation_type_name'];
        return $data;

    }

    //商品货物包装
    public static function HikReleaseMode()
    {
        $data = [
            '0' => ['id' => 0, 'release_mode_name' => '禁止放行'],
            '1' => ['id' => 1, 'release_mode_name' => '固定车包期'],
            '2' => ['id' => 2, 'release_mode_name' => '临时车入场'],
            '3' => ['id' => 3, 'release_mode_name' => '预约车入场'],
            '4' => ['id' => 10, 'release_mode_name' => '离线出场'],
            '5' => ['id' => 11, 'release_mode_name' => '缴费出场'],
            '6' => ['id' => 12, 'release_mode_name' => '预付费出场'],
            '7' => ['id' => 13, 'release_mode_name' => '免费出场'],
            '8' => ['id' => 30, 'release_mode_name' => '非法卡不放行'],
            '9' => ['id' => 31, 'release_mode_name' => '手动放行'],
            '10' => ['id' => 32, 'release_mode_name' => '特殊车辆放行'],
            '11' => ['id' => 33, 'release_mode_name' => '节假日放行'],
            '12' => ['id' => 35, 'release_mode_name' => '群组放行'],
            '13' => ['id' => 36, 'release_mode_name' => '遥控器开闸'],


        ];
        return $data;
    }

    /**
     * 获取车辆类型
     */
    public static function HikVehicleType()
    {
        $data = [
            '0' => ['id' => 0, 'vehicleTypeName' => '其他车'],
            '1' => ['id' => 1, 'vehicleTypeName' => '小型车'],
            '2' => ['id' => 2, 'vehicleTypeName' => '大型车'],
            '3' => ['id' => 3, 'vehicleTypeName' => '摩托车'],
        ];
        return $data;

    }

    /**
     * 获取税率
     */
    public static function getFax()
    {
        $data = [
            '0' => ['id' => 0, 'fax' => '1'],
            '1' => ['id' => 1, 'fax' => '3'],
            '2' => ['id' => 2, 'fax' => '6'],
            '3' => ['id' => 3, 'fax' => '9'],
            '4' => ['id' => 4, 'fax' => '13'],
            '5' => ['id' => 5, 'fax' => '0'],
            '6' => ['id' => 6, 'fax' => '收据'],
            '7' => ['id' => 7, 'fax' => '无'],
        ];
        return $data;

    }

    //海康API
    public static function hikApiUrl()
    {
        $envi = config('environment');
        if ($envi == 1) {
            $url = 'https://192.168.1.14';
        } else {
            $url = 'https://sunhua01.tpddns.cn:1443';
        }
        return $url;
    }

    /**
     * 钉钉webhook
     */
    public static function dingdingWebHook()
    {
        $envi = config('environment');
        if ($envi == 1) {
            $url = 'https://oapi.dingtalk.com/robot/send?access_token=1da1399a0d599f84037e0a887c0dca7c3cdf34cdd7691632cb899a614833bcbf';
        } else {
            $url = 'https://sunhua01.tpddns.cn:1443';
        }
        return $url;
    }

}
