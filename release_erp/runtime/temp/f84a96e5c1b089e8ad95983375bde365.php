<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:86:"/var/www/html/test_erp/public/../application/index/view/shipment/short_barge_info.html";i:1659350983;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>

										短驳详情
		
	
	</title>
<style>
	td{overflow: inherit!important;}
.addGoods{
	    color: blue;
	    display: block;
	    margin: 0 auto;
	    text-align: center;
	    font-size: 39px;
}
.delGoods{
	    color: red;
	    display: block;
	    margin: 0 auto;
	    text-align: center;
	    font-size: 39px;
}
.layui-form-item .layui-inline .layui-input-inline{

	margin-right:0px;

}	
.layui-form-item .layui-inline {

  margin-right: 0px;
  margin-bottom:0px;
}
.layui-inline{
float:left
}

.layui-form .layui-form-label {
  width: 100px;
}
.layui-table td, .layui-table th {
    
    padding: 0px; 

}
</style>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">




		<!-- 内容主体区域 -->
	


			<form class="layui-form layui-form-pane"  id="form1" onSubmit="return shortBargeAdd()">
	
<fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>基本信息</legend>
</fieldset>	
		<div class="layui-form-item" style='margin-bottom:2px;'>
						<div class="layui-inline">
							<label class="layui-form-label">短驳日期:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  name="shipment_time" value="<?php if($shipmentResult['shipment_time'] != ''): ?><?php echo date('Y-m-d',$shipmentResult['shipment_time']); endif; ?>"  id='short_barge_time'  placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>
  		</div>			
					
	
		
<fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>发运安排</legend>
</fieldset>						

 <div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label input-required">承运商:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								<select name='supplier_uuid'  lay-filter='supplier_choose'  lay-verify="required"  lay-search>
										<option value=''>请选择</option>
									<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
										<option value='<?php echo $vo['supplier_uuid']; ?>' <?php if($vo['supplier_uuid'] == $shipmentResult['supplier_uuid']): ?>selected<?php endif; ?>><?php echo $vo['supplier_name']; if($vo['supplier_type'] == 1): ?>
										-自营
										<?php elseif($vo['supplier_type'] == 2): ?>
										-第三方
										<?php else: ?>
										-个体户
										<?php endif; ?>
										</option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						<div class="layui-inline">
							<label class="layui-form-label">车辆:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								<select name='supplier_vehicle_id' id='vehicle_id' lay-filter='vehicle_choose' >
										<?php if(is_array($vehicleResult) || $vehicleResult instanceof \think\Collection || $vehicleResult instanceof \think\Paginator): if( count($vehicleResult)==0 ) : echo "" ;else: foreach($vehicleResult as $key=>$vr): ?>
										<option value="<?php echo $vr['vehicle_id']; ?>"><?php echo $vr['number_plate']; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>								
						</div>	


						<div class="layui-inline  clear">
							<label class="layui-form-label">长:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  id="vehicle_lenght"  value="<?php echo $nowVehicleResult['vehicle_lenght']; ?>"  placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
							<div class="layui-inline">
							<label class="layui-form-label">高:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input id='vehicle_height'  placeholder="<?php echo $nowVehicleResult['vehicle_width']; ?>" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
							<div class="layui-inline">
							<label class="layui-form-label">载重:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  id="max_load"   value="<?php echo $nowVehicleResult['max_load']; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
						<div class="layui-inline ">
							<label class="layui-form-label">车型:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  id="vehicle_type_name" value="<?php echo $nowVehicleResult['vehicle_type_name']; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>																								
  </div> 
  <fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>选择短驳</legend>
</fieldset>			
 <div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label input-required">运单号:</label>
							<div class="layui-input-inline "   style='width:300px;'>
 							<select name="choose_orders_number"  xm-select="choose_orders_number" xm-select-search="" xm-select-search-type="dl" xm-select-show-count="1">
                                	<?php if(is_array($ordersResult) || $ordersResult instanceof \think\Collection || $ordersResult instanceof \think\Paginator): if( count($ordersResult)==0 ) : echo "" ;else: foreach($ordersResult as $key=>$v): ?>
                                	     <option value="<?php echo $v['orders_number']; ?>"  selected><?php echo $v['orders_number']; ?>-<?php echo $v['send_location_name']; ?>-<?php echo $v['accept_location_name']; ?></option>

                                	
                                	<?php endforeach; endif; else: echo "" ;endif; if(is_array($shortBargeResult) || $shortBargeResult instanceof \think\Collection || $shortBargeResult instanceof \think\Paginator): if( count($shortBargeResult)==0 ) : echo "" ;else: foreach($shortBargeResult as $key=>$sbr): ?>
                                <option value="<?php echo $sbr['orders_number']; ?>" ><?php echo $sbr['orders_number']; ?>-<?php echo $sbr['send_location_name']; ?>-<?php echo $sbr['accept_location_name']; ?></option>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
							</div>
						</div>	
</div>							
	
  <div class="table-nont user-manage ">
    <table class="layui-table  layui-form" >

      <thead>
        <tr>
         <th>选择</th>
          <th>运单编号</th>
          <th>运单日期</th>
          <th>发货方</th>
          <th>发货人</th>
          <th>收货方</th>
          <th>货物</th>
          <th>下单件数</th>
          <th>下单数量</th>
          <th>包装单位 </th>
          <th>下单重量</th>
           <th>下单体积</th>
           
          <th>承运商</th>
          <th>费用</th>



        </tr> 
      </thead>
      <tbody class="goods-contents-body">
			<?php if(is_array($ordersResult) || $ordersResult instanceof \think\Collection || $ordersResult instanceof \think\Paginator): if( count($ordersResult)==0 ) : echo "" ;else: foreach($ordersResult as $key=>$v): ?>
        <tr class='goods_info'>
        <td class='<?php echo $v['orders_number']; ?>'></td>
          <td>
         	<div style='width:120px;'>			
          		<input type='text' value=' <?php echo $v['orders_number']; ?>' name='orders_number[]' class="layui-input" />
			</div>
          </td>
          <td>									  				
          		<?php echo $v['pickup_time']; ?>
          </td>
       <td><?php echo $v['send_goods_company']; ?></td>
	   <td>
				<?php echo $v['send_name']; ?>
	   </td>
		<td><?php echo $v['accept_goods_company']; ?>	</td>	
		<td>
			<?php echo $v['goodsName']; ?>
		</td>
          <td>	
			<?php echo $v['estimatedCount']; ?>
          </td>
       <td>
	       	<?php echo $v['estimatedPackCount']; ?>
       </td>
        <td>
	       	<?php echo $v['estimatedPackUnit']; ?>
       </td>
       <td>
			<?php echo $v['estimatedWeight']; ?>	       
       </td>             

	   <td>
			<?php echo $v['estimatedVolume']; ?>	       
	   </td>
		<td>
			<?php echo $v['supplierName']; ?>
		</td>
		<td >
		<div style='width:50px;'>			
			<input type='text' class="layui-input xiadanfei" name='cost_money[]' value='<?php echo $v['short_barge_money']; ?>'/>
		</div>
		</td>		
        </tr>
		<?php endforeach; endif; else: echo "" ;endif; ?>




        <tr id='heji'>
          <td>
    
			
				合计
	

          </td>
          <td id=''>			
  				
          </td>
       <td id=''>
       
       </td>
       	<td>	</td>
	   <td id=''></td> 
		<td id=''></td>

		<td id=''></td>
		<td id='xiadanjianshu'></td>	
		<td id='xiadanshuliang'></td>
			<td>	</td>
		<td id='xiadanzhongliang'></td>
		
		<td id='xiadantiji'></td>
		    	<td>	</td>
		<td id='xiadanfei'></td>
        </tr>		
      </tbody>
    </table>
    </div>
 <fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>费用明细 </legend>
</fieldset>			         
 <div class="layui-form-item" id='yunfei'>
						<div class="layui-inline">
							<label class="layui-form-label">短驳费</label>
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="pay_all_money" id='duanbofei' value="<?php echo $shipmentResult['pay_all_money']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input yunfei" type="text">
							</div>
						</div>	

						<div class="layui-inline">
							<label class="layui-form-label">分摊方式</label>
							<div class="layui-input-inline "  style='width:110px;' >
								<select name="distribution_type" id='distribution_type'>
									   	<?php if(is_array($baseConfig['shipment']['short_barge_distribution_type']) || $baseConfig['shipment']['short_barge_distribution_type'] instanceof \think\Collection || $baseConfig['shipment']['short_barge_distribution_type'] instanceof \think\Paginator): if( count($baseConfig['shipment']['short_barge_distribution_type'])==0 ) : echo "" ;else: foreach($baseConfig['shipment']['short_barge_distribution_type'] as $key=>$v2): ?>
									<option value='<?php echo $key; ?>' <?php if($key == $shipmentResult['distribution_type']): ?>selected<?php endif; ?>><?php echo $v2; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>										
						
						
						<div class="layui-inline" id='fentan'>
							<a href="javascript:void(0)"><label class="layui-form-label">分摊</label></a>

						</div>						
  </div>						
							    																																																					
 <fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>备注</legend>
</fieldset>			         

<div class="layui-form-item">

	<div class="layui-inline">
		<label class="layui-form-label">备注:</label>
		<div class="layui-input-inline " >
			 <input  name="shipment_remark"  style='width:600px;' value="<?php echo $shipmentResult['shipment_remark']; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
		</div>
	</div>						
  </div>				
				

	
			</form>
		
		<hr/>
		
		

				
				<div id='cost_info' style='display:none'>
						<div class="layui-inline" style='position:revalite'>
							<div class="layui-input-inline" style='width:100px;;float:left'  >
							<select  name='cost_id[]'>
								<?php if(is_array($costResult) || $costResult instanceof \think\Collection || $costResult instanceof \think\Paginator): if( count($costResult)==0 ) : echo "" ;else: foreach($costResult as $key=>$vo): ?>
								<option value='<?php echo $vo['cost_id']; ?>'><?php echo $vo['cost_name']; ?></option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
							</div>
							<div class="layui-input-inline "  style='width:110px;float:left' >
								 <input  name="cost_money[]" value=""  lay-verify="required" placeholder="" autocomplete="off" class="layui-input yunfei" type="text">
							</div>
								<div style='position:absolute;left:190px;top:-15px;color:red;width:25px;height:25px;font-size:20px;z-index:999'><a href='javascript:void(0)' class='cost_info'><i class="layui-icon" style='font-size:20px;'></i></a></div>	
						</div>
						
				</div>
					<div id='pay_info' style='display:none'>
						<div class="layui-inline" style='position:revalite'>
							<div class="layui-input-inline" style='width:100px;;float:left'  >
							<select  name='pay_type[]'>
							
								<option value='1'>现金</option>
								<option value='2'>油卡</option>
								<option value='3'>转账</option>
							</select>
							</div>
							<div class="layui-input-inline "  style='width:110px;float:left' >
								 <input  name='pay_money[]'  value=""  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
								<div style='position:absolute;left:190px;top:-15px;color:red;width:25px;height:25px;font-size:20px;z-index:999'><a href='javascript:void(0)' class='pay_info'><i class="layui-icon" style='font-size:20px;'></i></a></div>	
						</div>
						
				</div>					

</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/shipment/shipment.js'></script>

</body>
</html>
<script>

    var laydate = layui.laydate;
    var element = layui.element;
    var formSelects = layui.formSelects;
    var upload=layui.upload;

    laydate.render({
        elem: '#short_barge_time',
        trigger: 'click',
        lang: 'zn'
    });
    laydate.render({
        elem: '#need_time',
        trigger: 'click',//解决一闪而过的问题
        lang: 'zn'
    });
  
//获取车辆

    form.on('select(supplier_choose)', function (obj) {

    	var supplier_uuid =obj.value
    	
    	$('#vehicle_id').html("<option value=''>请选择</option>");
	    data = {supplier_uuid:supplier_uuid}
		//开始获得价格
	    $.ajax({
	        type: "post",
	        url: "/source/showVehicleAjax",
	        data: data,
	        dataType: "json",
	        success: function(data){
	            $.each(data.data,function(index,key){	    			
	            	$('#vehicle_id').append("<option value='"+key.vehicle_id+"'>"+key.number_plate+"</option>")	
	            })
	            form.render('select');
	        	
				
	        },
	        error:function(XMLHttpRequest, textStatus, errorThrown){
	            layer.msg('验证失败')
	        }
	    });		
		
    	
    	
  
    	
    })	
    
    
    
		layui.formSelects.on('choose_orders_number', function(id, vals, val, isAdd, isDisabled){
            //id:           点击select的id
            //vals:         当前select已选中的值
            //val:          当前select点击的值
            //isAdd:        当前操作选中or取消
            //isDisabled:   当前选项是否是disabled
            if(isAdd==true){
    		    $.ajax({
    		        type: "post",
    		        url: "/shipment/getShortBrageNeedOrderAjax",
    		        data: {orders_number:val.value},
    		        dataType: "json",
    		        success: function(data){
    		        	
    		        	var h ="<tr class='goods_info'>"+
    					
    					"<td class='"+data.orders_number+"'></td>"+
    					" <td><input type='text' value='"+data.orders_number+"' name='orders_number[]' class='layui-input' /> </td>"+
    					" <td>"+data.pickup_time+"</td>"+
    					" <td>"+data.send_goods_company+"</td>"+
    					" <td>"+data.send_name+"</td>"+
    					" <td>"+data.accept_goods_company+"</td>"+
    					" <td>"+data.goodsName+"</td>"+
    					" <td>"+data.estimatedCount+"</td>"+
    					" <td>"+data.estimatedPackCount+"</td>"+
    					" <td>"+data.estimatedPackUnit+"</td>"+
    					" <td>"+data.estimatedWeight+"</td>"+
    					" <td>"+data.estimatedVolume+"</td>"+
    					" <td>"+data.supplierName+"</td>"+
    					" <td><input type='text'   class='layui-input xiadanfei' value='' name='cost_money[]' class='layui-input' /> </td>"+

    					"</tr>";
    					if($('.goods_info').length=='' || $('.goods_info').length ==0){
    						$('#heji').parent().prepend(h)
    					}else{
    						$('#heji').prev().after(h)
    					}
    					
    					

    		        },
    		        error:function(XMLHttpRequest, textStatus, errorThrown){
    		            layer.msg('验证失败')
    		        }
    		    });           	
            }else{
            	$("."+val.value).parent().remove()
            }
		

	});
  //获取车辆详情

    form.on('select(vehicle_choose)', function (obj) {

    	var vehilce_id=obj.value
    	
	    data = {vehicle_id:vehilce_id}
		//开始获得价格
	    $.ajax({
	        type: "post",
	        url: "/source/showVehicleAjax",
	        data: data,
	        dataType: "json",
	        success: function(data){
				console.log(data)
	
	           //长宽高载重 车型
	           $('#vehicle_lenght').val(data.data[0].vehicle_lenght)
	           $('#vehicle_width').val(data.data[0].vehicle_width)
	           $('#max_load').val(data.data[0].max_load)
	           $('#vehicle_type_name').val(data.data[0].vehicle_type_name)
	           
	         
				
	        },
	        error:function(XMLHttpRequest, textStatus, errorThrown){
	            layer.msg('验证失败')
	        }
	    });		
    })	
	function checkGoodInfo(){

		var goods_info = $('.goods_info');
		var xiadanjianshu = 0;
		var xiadanshuliang = 0;
		var xiadanzhongliang =0;
		var xiadantiji = 0;
		var  xiadanfei=0

		for(var i=0;i<goods_info.length;i++){
			var xdjs = goods_info.eq(i).find('td').eq(7).html();
			xiadanjianshu+=parseFloat(xdjs)
			var xdsl = goods_info.eq(i).find('td').eq(8).html();
			xiadanshuliang+=parseFloat(xdsl)	
			var xdzl = goods_info.eq(i).find('td').eq(10).html();
			xiadanzhongliang+=parseFloat(xdzl)
			var xdtj = goods_info.eq(i).find('td').eq(11).html();
			xiadantiji+=parseFloat(xdtj)
			var xdf = goods_info.eq(i).find('td').eq(13).find('input').val();
			if(xdf==null || xdf==''){
				xdf=0
			}
			xiadanfei+=parseInt(xdf)
		}
		$('#xiadanjianshu').html(xiadanjianshu)
		$('#xiadanshuliang').html(xiadanshuliang)
		$('#xiadanzhongliang').html(xiadanzhongliang)
		$('#xiadantiji').html(xiadantiji)
		$('#xiadanfei').html(xiadanfei)

	}    
    checkGoodInfo();
	$('#fentan').click(function(){
		var v = $('#distribution_type').val();
		var duanbofei = $('#duanbofei').val();
		var goods_info = $('.goods_info')
		var yue = duanbofei;
		
		var xiadanjianshu = $('#xiadanjianshu').html()
		var xiadanshuliang  = $('#xiadanshuliang').html()
		var xiadanzhongliang = $('#xiadanzhongliang').html()

		var xiadantiji = $('#xiadantiji').html()
		//代表按单
		if(v==2){
			for(var i=0;i<goods_info.length;i++){				
				if(i==(goods_info.length-1)){
					$('.goods_info').eq(i).find('td').eq(13).find('input').val(yue);
					break;
					return false;
				}
				var feiyong = Math.ceil(duanbofei/goods_info.length)				
				 $('.goods_info').eq(i).find('td').eq(13).find('input').val(feiyong)
				yue = yue-feiyong

			}
			//按件数
			
		}else if(v==3){
			for(var i=0;i<goods_info.length;i++){				
				if(i==(goods_info.length-1)){
					$('.goods_info').eq(i).find('td').eq(13).find('input').val(yue);
					break;
					return false;
				}
				var v = $('.goods_info').eq(i).find('td').eq(7).html();
				v = parseInt(v)/xiadanjianshu;
				
				var feiyong = Math.ceil(duanbofei*v)				
				 $('.goods_info').eq(i).find('td').eq(13).find('input').val(feiyong)
				yue = yue-feiyong

			}			
		}else if(v==4){
			for(var i=0;i<goods_info.length;i++){				
				if(i==(goods_info.length-1)){
					$('.goods_info').eq(i).find('td').eq(13).find('input').val(yue);
					break;
					return false;
				}
				var v = $('.goods_info').eq(i).find('td').eq(8).html();
				v = parseInt(v)/xiadanshuliang;
				
				var feiyong = Math.ceil(duanbofei*v)				
				 $('.goods_info').eq(i).find('td').eq(13).find('input').val(feiyong)
				yue = yue-feiyong

			}			
		}else if(v==5){
			for(var i=0;i<goods_info.length;i++){				
				if(i==(goods_info.length-1)){
					$('.goods_info').eq(i).find('td').eq(13).find('input').val(yue);
					break;
					return false;
				}
				var v = $('.goods_info').eq(i).find('td').eq(10).html();
				alert(v)
				v = parseInt(v)/xiadanzhongliang;
				
				var feiyong = Math.ceil(duanbofei*v)				
				 $('.goods_info').eq(i).find('td').eq(13).find('input').val(feiyong)
				yue = yue-feiyong

			}			
		}else if(v==6){
			for(var i=0;i<goods_info.length;i++){				
				if(i==(goods_info.length-1)){
					$('.goods_info').eq(i).find('td').eq(13).find('input').val(yue);
					break;
					return false;
				}
				var v = $('.goods_info').eq(i).find('td').eq(11).html();
				v = parseInt(v)/xiadantiji;
				
				var feiyong = Math.ceil(duanbofei*v)				
				 $('.goods_info').eq(i).find('td').eq(13).find('input').val(feiyong)
				yue = yue-feiyong

			}			
		}	
		 checkGoodInfo();
	})
    //结束
    
   $(document).on('keyup', '.xiadanfei', function() {
	
    		checkGoodInfo()
	});


</script>



