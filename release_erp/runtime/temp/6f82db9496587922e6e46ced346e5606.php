<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:96:"/var/www/html/test_erp/public/../application/index/view/order/order_delivery_receipt_manage.html";i:1662617077;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	 <script src="/static/javascript/system/lodop.js"></script>
    <title>签收单</title>
    <style>
        *{
            padding: 0;
            margin: 0;
        }
        .layui-layout-body .title{
            font-size: 28px;
            text-align: center;
            margin-top: 30px;
        }
        .layui-layout-body .title .logo{

        }
        .layui-layout-body .company{
            font-size: 18px;
            margin-left: 10px;
            margin-top: 20px;
        }
        .layui-layout-body .info .u1{
            font-size: 18px;
            width: 788px;
            margin-left: 10px;
        }
        .layui-layout-body .info .u1 li{
            font-size: 17px;
            float: left;
            width: 260px;
            border: 0px solid red;
            margin-top:5px
        }
        .layui-layout-body .table1{
            margin-left: 5px;
            width: 780px;
            height: 730px;
            border: 1px solid black;
            table-layout:fixed;
        }
        .layui-layout-body .table1 tr td{
            font-size: 17px;
        }
		.print-button{
			    position: absolute;
    z-index: 100000;
    width: 500px;
    top: 10px;
		}
		.print-button a{
		    padding: 5px;
    border: 1px solid #333;
    border-radius: 3px;
		
		}
		
    </style>
</head>
<body class="layui-layout-body">
<div style="width:794px;height:1064px;border:1px solid #000000;">
    <div class="title"><image class="logo" src="/static/image/login/login-logo.png" width="40px" height="auto"></image>&nbsp;&nbsp;送 货 签 收 单</div>
    <div class="company">上海圣华国际物流股份有限公司</div>
    <div class="info">
        <ul class="u1">
            <li>地址: 上海市嘉定区思义路1625号 </li>
            <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电话: 021-69151620</li>
          
        </ul>
    </div>
    <br/>
    <br/>
    <table class="table1" border="1" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td colspan="2">&nbsp;运单编号:</td>
                <td colspan="4">&nbsp;<?php echo $orderResult['orders_number']; ?></td>
                <td colspan="2">&nbsp;客户编号:</td>
                <td colspan="7">&nbsp;<?php echo $orderResult['customer_order_number']; ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;送货日期:</td>
                <td colspan="4">&nbsp;<?php if($orderResult['send_time']): ?> <?php echo date('Y-m-d',$orderResult['send_time']); endif; ?></td>
                <td colspan="2">&nbsp;订货日期:</td>
                <td colspan="7">&nbsp;<?php echo date('Y-m-d',$orderResult['create_time']); ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;客户名称:</td>
                <td colspan="13">&nbsp;<?php echo $orderResult['accept_goods_company']; ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;送货地址:</td>
                <td colspan="13">&nbsp;<?php echo $orderResult['accept_address']; ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;收货人:</td>
                <td colspan="4">&nbsp;<?php echo $orderResult['accept_name']; ?></td>
                <td colspan="2">&nbsp;联系电话:</td>
                <td colspan="7">&nbsp;<?php echo $orderResult['accept_cellphone']; ?></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;货物名称</td>
                <td colspan="2">&nbsp;包装</td>
				<td colspan="2">&nbsp;件数</td>
                <td colspan="2">&nbsp;数量</td>   
                <td colspan="3">&nbsp;重量KG</td>
                <td colspan="3">&nbsp;体积m3</td>
            </tr>
            <?php if(is_array($orderResult['orders_goods_info']) || $orderResult['orders_goods_info'] instanceof \think\Collection || $orderResult['orders_goods_info'] instanceof \think\Paginator): if( count($orderResult['orders_goods_info'])==0 ) : echo "" ;else: foreach($orderResult['orders_goods_info'] as $k1=>$vo): ?>
                
                <tr>
                    <td colspan="3">&nbsp;<?php echo $vo['goods_name']; ?><?php echo $orderResult['orders_goods_info']['length']; ?></td>
                    <td colspan="2">&nbsp;<?php if($vo['realy_pack_unit'] == 1): ?>纸箱<?php elseif($vo['realy_pack_unit'] == 2): ?>托盘<?php elseif($vo['realy_pack_unit'] == 3): ?>桶<?php elseif($vo['realy_pack_unit'] == 4): ?>袋装<?php elseif($vo['realy_pack_unit'] == 5): ?>编织袋<?php endif; ?></td>
                    <td colspan="2">&nbsp;<?php echo $vo['estimated_count']; ?></td>
					<td colspan="2">&nbsp;<?php echo $vo['estimated_pack_count']; ?></td>
                    <td colspan="3">&nbsp;<?php echo $vo['estimated_weight']; ?></td>
                    <td colspan="3">&nbsp;<?php echo $vo['estimated_volume']; ?></td>
                </tr>
               
            <?php endforeach; endif; else: echo "" ;endif; ?>

            <tr>
                <td colspan="5" style="text-align: right;">合计:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				 <td colspan="2">&nbsp;<?php echo $xdsl; ?></td>
    <td colspan="2">&nbsp;<?php echo $xdjs; ?></td>               
			  
            
                <td colspan="3">&nbsp;<?php echo $xdzl; ?></td>
                <td colspan="3">&nbsp;<?php echo $xdtj; ?></td>
            </tr>
            <tr>
            <tr style="height:80px;">
                <td colspan="2">&nbsp;备注:</td>
                <td colspan="13">&nbsp;<?php echo $orderResult['remark']; ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;送货单位:</td>
                <td colspan="13">&nbsp;<?php echo $orderResult['send_goods_company']; ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;仓库地址:</td>
                <td colspan="13">&nbsp;<?php echo $orderResult['send_address']; ?></td>
            </tr>
            <tr style="height:120px;line-height:30px">
                <td colspan="8">
                    <p>&nbsp;&nbsp;上列所购货物确认无误 多谢选购本公司产品!</p>
                    <p>&nbsp;&nbsp;如有破损或其它疑问请及时联系: 021-69151895</p>
                    <p>&nbsp;&nbsp;送货员注意: 节假日送货需提前联系收货人!</p>
                </td>
                <td colspan="7">
                    <p>&nbsp;&nbsp;实收数量:</p>
                    <p>&nbsp;&nbsp;收货人签名或盖章:</p>
                    <p>&nbsp;&nbsp;签收日期:</p>
                </td>
            </tr>
        </tbody>
    </table>
	<div class="print-button" onclick="disable_view()">
	<a href="javascript:void(0);" onclick="old_print();" >打印签收单</a>
	<!--<a href="javascript:void(0);" onclick="print_now();" >直接打印</a>-->
	<a href="javascript:void(0);" onclick="print_chose();" >打印签收单1</a>
	<a href="javascript:void(0);" onclick="print_view();" >打印预览签收单1</a>
	<a href="/uploads/l.rar"  >下载控件</a>
	<!--<a href="/shipment/showLineOverManage" >返 回</a>-->
	</div>
</div>
<script>
    function disable_view(){
      
		document.querySelector(".print-button").style.display="none"
		
		setTimeout(function(){
		document.querySelector(".print-button").style.display="block"
		
		
		},3000);
		
		  
		
    }
	function old_print(){
	disable_view()
	window.print();
	
	}
	function CreateOneFormPage(){
		LODOP=getLodop();  
		LODOP.PRINT_INIT("打印");
		var base_width=0;
		var base_height=-3;
		var goods_height=0;
		var pay_money,paid_oil,pay_collect,songhuo_fee,xiehuo_fee,shanglou_fee;
		
	    LODOP.SET_PRINT_PAGESIZE(0,"250mm","140mm","");
		
		
		goods_height=49+base_height;
		LODOP.NewPage();
		LODOP.SET_PRINT_STYLE("FontSize",12);
		LODOP.SET_PRINT_STYLE("font-family","宋体");
		LODOP.ADD_PRINT_TEXT(base_height+21+"mm",base_width+25+"mm",270,21,"<?php echo $orderResult['accept_goods_company']; ?>");
		LODOP.ADD_PRINT_TEXT(base_height+30+"mm",base_width+25+"mm",270,21,"<?php echo $orderResult['accept_name']; ?>");
		LODOP.ADD_PRINT_TEXT(base_height+39+"mm",base_width+25+"mm",300,21,"<?php echo $orderResult['accept_address']; ?>");
		
		
		
		 <?php if(is_array($orderResult['orders_goods_info']) || $orderResult['orders_goods_info'] instanceof \think\Collection || $orderResult['orders_goods_info'] instanceof \think\Paginator): if( count($orderResult['orders_goods_info'])==0 ) : echo "" ;else: foreach($orderResult['orders_goods_info'] as $k1=>$vo): ?>
                goods_height+=8.5;
				LODOP.ADD_PRINT_TEXT(goods_height+"mm",base_width+6+"mm",40,21,"<?php echo str_pad($k1+1,3,0,STR_PAD_LEFT); ?>");
				LODOP.ADD_PRINT_TEXT(goods_height+"mm",base_width+43+"mm",270,21,"<?php echo $vo['goods_name']; ?>");
				LODOP.ADD_PRINT_TEXT(goods_height+"mm",base_width+111+"mm",100,21,"<?php echo intval($vo['goods_config']['goods_weight']); ?>kg*<?php echo $vo['estimated_count']; ?>");
				LODOP.ADD_PRINT_TEXT(goods_height+"mm",base_width+132+"mm",100,21,"<?php echo intval($vo['estimated_weight']); ?>");
         <?php endforeach; endif; else: echo "" ;endif; ?>
		
		LODOP.ADD_PRINT_TEXT(base_height+108+"mm",base_width+21.5+"mm",149,21,"圣华");
		LODOP.ADD_PRINT_TEXT(base_height+116+"mm",base_width+21.5+"mm",820,21,"<?php echo $orderResult['customer_remark']; ?>");
		LODOP.ADD_PRINT_TEXT(base_height+21+"mm",base_width+176+"mm",200,21,"<?php echo $orderResult['khydh']; ?>");
		LODOP.ADD_PRINT_TEXT(base_height+32+"mm",base_width+176+"mm",150,21,"<?php echo date('Y/m/d',$orderResult['pickup_time']); ?>");
		LODOP.ADD_PRINT_TEXT(base_height+38.5+"mm",base_width+176+"mm",200,21,"<?php echo $orderResult['customer_order_number']; ?>");
		
	};
	function print_now(){
	CreateOneFormPage();
	LODOP.PRINT(); 
	}
	function print_chose(){
	CreateOneFormPage();
	LODOP.PRINTA(); 
	}
	function print_view(){
	CreateOneFormPage();
	LODOP.PREVIEW();
	}
	
</script>
</body>
</html>