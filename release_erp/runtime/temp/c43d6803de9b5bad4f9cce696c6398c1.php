<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:87:"/var/www/html/test_erp/public/../application/index/view/bill/customer_finance_bill.html";i:1665281101;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






  <title>账单管理</title>
  <link href="/static/css/source.css" rel="stylesheet">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
  <div class="layui-header">
        <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
  </div>

  <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
    </div>
  </div>

  <div class="layui-body layui-body-bg">

    <!-- 内容主体区域 -->
    <div class="content_body">
      <div class="body-top">
        <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>账单管理</a>
                        <a><cite>对账单</cite></a>
                    </span>
        </div>
        <div class='layui-block all-search-bg'>
          <form class="layui-form" method='get' action='/source/showSupplierManage'>
            <div class="layui-row">
              <div class="layui-col-md14">
                <div class='layui-input-inline'>
                  <label class="layui-form-label">对账编号:</label>
                  <div class="layui-input-block">
                    <input type="text" name="customer_bill_number" id="customer_bill_number" class="layui-input" placeholder="对账编号" value="<?php echo $customerBillNumber; ?>" class="layui-input" readonly>
                  </div>
                </div>
                <div class='layui-input-inline'>
                  <label class="layui-form-label">发货方:</label>
                  <div class="layui-input-block">
                    <input type="text" name="send_name" id="send_name" class="layui-input" placeholder="发货方" value="<?php echo $send_name; ?>" class="layui-input" readonly>
                  </div>
                </div>
                <div class='layui-input-inline'>
                  <label class="layui-form-label">对账名称:</label>
                  <div class="layui-input-block">
                    <input type="text" name="customer_bill_name" id="customer_bill_name" placeholder="对账名称" value="<?php echo $customer_bill_name; ?>" class="layui-input">
                  </div>
                </div>
                <div class='layui-input-inline'>
                  <label class="layui-form-label">创建人:</label>
                  <div class="layui-input-block">
                    <input type="text" name="user_name" id="user_name" placeholder="创建人" value="<?php echo $user_name; ?>" class="layui-input" readonly>
                  </div>
                </div>
                  <div class="layui-input-inline" >
                      <label class="layui-form-label">分公司:</label>
                      <div class="layui-input-block" style="width: 158px;">
                          <select id='company_id' name='choose_company_id' >
                              <option value=''>--全部--</option>
                              <?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$v): ?>
                              <option value="<?php echo $v['company_id']; ?>"

                                      <?php if(\think\Request::instance()->get('choose_company_id') != ''): if(\think\Request::instance()->get('choose_company_id') == $v['company_id']): ?>selected <?php endif; else: if(\think\Session::get('user.company_id') == $v['company_id']): ?>selected <?php endif; endif; ?>
                              >

                              <?php echo $v['company_name']; ?></option>
                              <?php endforeach; endif; else: echo "" ;endif; ?>
                          </select>
                      </div>
                  </div>
                  <div class='layui-input-inline'>
                      <label class="layui-form-label">对账日期:</label>
                      <div class="layui-input-block">
                          <input type="text" name="customer_bill_date" id="customer_bill_date" placeholder="对账日期" value="<?php echo $now; ?>" class="layui-input" readonly>
                      </div>
                  </div>
              </div>
              <br/>
              <div class="layui-col-md14">
                <div class='layui-input-inline'>
                  <label class="layui-form-label">运单总计:</label>
                  <div class="layui-input-block">
                    <input type="text" name="money" id="money" placeholder="运单总计" value="<?php echo $money; ?>" class="layui-input" readonly>
                  </div>
                </div>
                <div class='layui-input-inline'>
                  <label class="layui-form-label">赔款:</label>
                  <div class="layui-input-block">
                    <input type="text" name="abnormal_money" id="abnormal_money" placeholder="赔款" value="<?php echo $abnormal_money; ?>" class="layui-input" readonly>
                  </div>
                </div>
                  <div class='layui-input-inline'>
                      <label class="layui-form-label">开票金额:</label>
                      <div class="layui-input-block">
                          <input type="text" name="customer_bill_money" id="customer_bill_money" placeholder="开票金额" value="<?php echo $customer_bill_money; ?>" class="layui-input" readonly>
                      </div>
                  </div>
                  <div class='layui-input-inline' style="width:12%;">
                      <label class="layui-form-label">是否含税:</label>
                      <div class="layui-input-block">
                          <select name='customer_bill_tax_type' id="customer_bill_tax_type" lay-filter="customer_bill_tax_type" lay-search>
                              <option value='1' <?php if($customer_bill_tax_type == 1 || $customer_bill_tax_type == ''): ?>selected<?php endif; ?>>是</option>
                              <option value='2' <?php if($customer_bill_tax_type == 2): ?>selected<?php endif; ?>>否</option>
                          </select>
                      </div>
                  </div>
                  <div class='layui-input-inline' style="width:15%;">
                    <label class="layui-form-label">税率:</label>
                    <div class="layui-input-block">
                        <select name='customer_bill_tax' id="customer_bill_tax" lay-filter="customer_bill_tax" lay-search>
<!--                            <option value='999999' <?php if($tax_rate == 999999): ?>selected<?php endif; ?>>不开票</option>-->
                            <option value='0' <?php if($tax_rate == 0): ?>selected<?php endif; ?>>0%</option>
                            <option value='3' <?php if($tax_rate == 3): ?>selected<?php endif; ?>>3%</option>
                            <option value='6' <?php if($tax_rate == 6): ?>selected<?php endif; ?>>6%</option>
                            <option value='9' <?php if($tax_rate == 9): ?>selected<?php endif; ?>>9%</option>
                        </select>
                    </div>
                  </div>
                  <div class='layui-input-inline'>
                      <label class="layui-form-label">开票类型:</label>
                      <div class="layui-input-block">
                          <select name='customer_bill_type' id="customer_bill_type"  lay-filter="customer_bill_type" lay-search >
                              <option value=''>--全部--</option>
                              <option value='1' selected >专用增值税</option>
                              <option value='2'>普通增值税</option>
                              <option value='3'>不开票</option>
                              <option value='4'>免税</option>
                          </select>
                      </div>
                  </div>
                  <div class='layui-input-inline'>
                      <label class="layui-form-label">付款方式:</label>
                      <div class="layui-input-block">
                          <select name='pay_type' id="pay_type" lay-search >
                              <option value=''>--全部--</option>
                              <option value='1'>现付</option>
                              <option value='2'>到付</option>
                              <option value='3' selected >月结</option>
                          </select>
                      </div>
                  </div>
              </div>
            </div>
            <br/>
            <div class="layui-row">
                <label class="layui-form-label">备注:</label>
                <div class="layui-input-block">
                  <input type="text" name="remark" id="remark" placeholder="备注" value="" class="layui-input" style="width:100%">
                </div>
            </div>
          </form>
        </div>
      </div>
      <div class="content-bg">
        <div class="layui-col-md14">
          <div class='layui-input-inline'><h3>对账明细</h3></div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <div class='layui-input-inline'>
            <a href="javascript:void(0);">
              <button class="layui-btn layui-btn-sm layui-btn-normal" name="btn_sb">提 交</button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/bill/showCustomerBillManage">
              <button class="layui-btn layui-btn-sm layui-btn-warm" name="btn">返 回</button>
            </a>
          </div>
        </div>
        <hr>
        <div class="table-nont user-manage company-pageHeight">
          <table class="layui-table layui-form" id="test3">
            <thead>
            <tr>
<!--              <th lay-data="{type:'checkbox',fixed:'left'}"></th>-->
              <th>序号</th>
              <th>运单编号</th>
              <th>开票状态</th>
              <th>项目</th>
              <th>发货方</th>
              <th>发站</th>
              <th>到站</th>
              <th>收货方</th>
              <th>货物名称</th>
              <th>件数</th>
              <th>数量</th>
              <th>重量</th>
              <th>体积</th>
              <th>送货方式</th>
              <th>异常</th>
              <th>总费用</th>
              <th>赔款</th>
            </tr>
            </thead>

            <tbody>
            <?php if(is_array($customerBillResult) || $customerBillResult instanceof \think\Collection || $customerBillResult instanceof \think\Paginator): if( count($customerBillResult)==0 ) : echo "" ;else: foreach($customerBillResult as $l=>$vo): ?>
            <tr>
<!--              <th><input type="checkbox" name="layTableCheckbox" lay-skin="primary"></th>-->
			
              <th class="a1"><?php echo $vo['finance_id']; ?></th>
              <th><?php echo $vo['orders_number']; ?></th>
              <th><?php if($vo['invoice_status'] == 0): ?>未开票<?php elseif($vo['invoice_status'] == 1): ?>已开票<?php endif; ?></th>
              <th class="a2"><?php echo $vo['project_name']; ?></th>
              <th><?php echo $vo['send_goods_name']; ?></th>
              <th><?php echo $vo['send_city_name']; ?></th>
              <th><?php echo $vo['accept_city_name']; ?></th>
              <th><?php echo $vo['accept_goods_name']; ?></th>
              <th><?php echo $vo['goods_name']; ?></th>
              <th><?php echo $vo['realy_count']; ?></th>
              <th><?php echo $vo['realy_pack_count']; ?></th>
              <th><?php echo $vo['realy_weight']; ?></th>
              <th><?php echo $vo['realy_volume']; ?></th>
              <th><?php if($vo['delivery_method'] == 1): ?>送货<?php elseif($vo['delivery_method'] == 2): ?>自提<?php endif; ?></th>
              <th><?php if($vo['abnormal_status'] == 0): ?>否<?php elseif($vo['abnormal_status'] == 1): ?>是<?php endif; ?></th>
              <th><?php echo $vo['money2']; ?></th>
              <th><?php echo $vo['abnormal_money']; ?></th>
              <th style="display:none"><?php echo $vo['orders_id']; ?></th>
              <th style="display:none"><?php echo $vo['project_id']; ?></th>
            </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>

          </table>
        </div>

      </div>
        <input type="hidden" id="project_id_hidden" value="<?php echo $project_id; ?>">
        <input type="hidden" id="user_id_hidden" value="<?php echo $user_id; ?>">
        <input type="hidden" id="create_time_hidden" value="<?php echo $now; ?>">
    </div>
  </div>

<!--  <div class="layui-footer">-->
<!--    &lt;!&ndash; 底部固定区域 &ndash;&gt;-->
<!--    © layui.com - 底部固定区域-->
<!--  </div>-->
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/source.js'></script>
<script>
  layui.use(['layer','table','form','laydate'], function() {
    var table = layui.table //表格
            , layer = layui.layer //弹层
            , form = layui.form
            , laydate = layui.laydate

      //默认时间
      laydate.render({
          elem: '#customer_bill_date',
          trigger: 'click',
          type: 'date',
          format: 'yyyy-M-d',
          lang: 'zn',
      });

      form.on('select(customer_bill_tax)', function (data) {
          if($("#customer_bill_tax").val()==999999){
              $("#customer_bill_tax_type").val(2);
              form.render('select');
          }else{
              $("#customer_bill_tax_type").val(1);
              form.render('select');
          }
      });

      form.on('select(customer_bill_type)', function (data) {
          if($("#customer_bill_type").val()==3 || $("#customer_bill_type").val()==4){
              $("#customer_bill_tax_type").val(2);
              $("#customer_bill_tax").val(0);
              form.render('select');
          }else{
              $("#customer_bill_tax_type").val(1);
              $("#customer_bill_tax").val(9);
              form.render('select');
          }
      });

    $('button[name="btn_sb"]').bind('click', function () {

        var customer_bill_name_val = $("#customer_bill_name").val();
        if(customer_bill_name_val==undefined || customer_bill_name_val==''){
            layer.open({
                title: '警 告'
                ,content: '对账名称不得为空!'
            });
            return false;
        }

        var milasUrl = {};//新建对象，用来存储所有数据
        var subMilasUrlArr = {};//存储每一行数据
        var tableData = {};
        $("#test3 tbody tr").each(function(trindex, tritem){
            tableData[trindex] = new Array();
			var kk=1;
            $(tritem).find("th").each(function (tdindex, tditem) {
				if(kk<3){
					tableData[trindex][tdindex] = $(tditem).html();//遍历每一个数据，并存入
					subMilasUrlArr[trindex] = tableData[trindex];//将每一行的数据存入				  
				}

				kk++
            });

        });

		
        for (var key in subMilasUrlArr) {
            milasUrl[key] = subMilasUrlArr[key];//将每一行存入对象
        }

        //ajax提交
        var customer_bill_number = $("#customer_bill_number").val();
        var send_name = $("#project_id_hidden").val();
        var customer_bill_name = $("#customer_bill_name").val();
        var user_id = $("#user_id_hidden").val();
        var money = $("#money").val();
        var abnormal_money = $("#abnormal_money").val();
        var create_time = $("#create_time_hidden").val();
        var remark = $("#remark").val();
        var customer_bill_date = $("#customer_bill_date").val();
        var customer_bill_tax = $("#customer_bill_tax").val();
        var customer_bill_tax_type = $("#customer_bill_tax_type").val();
        var pay_type = $("#pay_type").val();
        var company_id = $("#company_id").val();
        var customer_bill_money = $("#customer_bill_money").val();
        var customer_bill_type = $("#customer_bill_type").val();

        var myarray = [];

        myarray.push([customer_bill_number,send_name,customer_bill_name,user_id,money,abnormal_money,create_time,remark,customer_bill_date,customer_bill_tax,customer_bill_tax_type,pay_type,company_id,customer_bill_money,customer_bill_type]);
        milasUrl[-1] = myarray[0];
        // console.log(milasUrl);return false;

        $.ajax({
            type: "POST",
            url: '/bill/addCustomerBillAjax',
            data: milasUrl,
            success: function(data){
                // console.log(data);
                if(data.code!=200){
                    layer.msg("添加失败!");
                    return false;
                }else if(data.code==200) {
                    layer.msg('操作成功',{time:1,end : function(layero, index){
                            location.href='/bill/showCustomerBillManage';
                        }
                    });
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                layer.msg('验证失败')
            }
        });
        return false;

    });

  });

</script>
</body>
</html>