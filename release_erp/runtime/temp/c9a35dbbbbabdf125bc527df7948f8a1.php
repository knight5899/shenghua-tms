<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:79:"/var/www/html/test_erp/public/../application/index/view/oaperson/user_edit.html";i:1659350323;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:70:"/var/www/html/test_erp/application/index/view/public/left_oa_menu.html";i:1642647257;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






  	<title>layout 后台大布局 - Layui</title>
 
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
	
	<div class="layui-header">
	    <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
	</div>
  
	 <div class="layui-side layui-bg-black">
	   <div class="layui-side-scroll">
	     <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
		      <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="iconfont img-homepage_fill"></i><em>控制面板</em></a></li>
        <li <?php if($controller_name == 'oaorganization'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>组织管理</em></a>
          <dl class="layui-nav-child">
                       <dd <?php if(in_array(($function_name), explode(',',"showcompanyedit,showcompanyadd,showcompanymanage"))): ?> class="layui-this"<?php endif; ?>><a href="/oaorganization/showCompanyManage">公司</a></dd>
             <dd <?php if(in_array(($function_name), explode(',',"showdepartmentedit,showdepartmentadd,showdepartmentmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/oaorganization/showDepartmentManage">部门</a></dd>



          </dl>
        </li>
        <li <?php if($controller_name == 'oaperson'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>人员管理</em></a>
          <dl class="layui-nav-child">
                       <dd <?php if(in_array(($function_name), explode(',',"showuseredit,showuseradd,showusermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/oaperson/showUserManage">用户</a></dd>




          </dl>
        </li>

        <li <?php if($controller_name == 'oarole'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>角色管理</em></a>
          <dl class="layui-nav-child">
                       <dd <?php if(in_array(($function_name), explode(',',"showroleedit,showroleadd,showrolemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/oarole/showRoleManage">角色</a></dd>
             <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/showAuthManage">权限</a></dd>



          </dl>
        </li>

          <li <?php if($controller_name == 'oadata'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>资料管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/oadata/showDataGroupManage">资料分类</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/oadata/showDataManage">资料</a></dd>



          </dl>
          </li>

          <li <?php if($controller_name == 'examination'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>考试管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/examination/showSubjectManage">考试题目</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/examination/showExaminationManage">考试题库</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/examination/showOnlineExaminationManage">在线考试</a></dd>



          </dl>
          </li>
    
          <!--<li <?php if($controller_name == 'otaarticle' or $controller_name == 'enquirty' or $controller_name == 'otasystem' or $controller_name == 'otaslide' or $controller_name == 'otaproduct' or $controller_name == 'otamember'): ?>-->
              <!--class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>-->
          <!--<a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>官网管理</em></a>-->
          <!--<dl class="layui-nav-child">-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showotasystemmanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showOtaSystemManage">网站设置</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showotamenumanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showOtaMenuManage?status=1">菜单</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showarticletypemanage,showarticletypeadd,showarticletypeedit,showarticletypeinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_article/showArticleTypeManage?status=1">文章分类</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showarticlemanage,showarticleadd,showarticleedit,showarticleinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_article/showArticleManage?status=1">文章</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showslidemanage,showslideadd,showslideedit,showslideinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showSlideManage?status=1">幻灯片</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showadvertmanage,showadvertadd,showadvertedit,showadvertinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showAdvertManage?status=1">友情链接</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showadvertisingmanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showAdvertisingManage?status=1">广告位</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showcompanywebsitemanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showCompanyWebsiteManage?status=1">域名管理</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"index,addenquirty,editenquirty"))): ?> class="layui-this" <?php endif; ?> ><a href="/enquirty/index">需求定制</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"lst,add,edit"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_member/lst?status=1">账号管理</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"types,addtype,edittype,gettypeinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/types?status=1">旅游产品分类</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"productlists,addlist,editlist"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/productLists?status=1">旅游产品列表</a></dd>-->
              <!--&lt;!&ndash;<dd <?php if(in_array(($function_name), explode(',',"products,addlist,editlist"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/products?status=1">旅游产品</a></dd>&ndash;&gt;-->
          <!--</dl>-->

          <!--</li>-->
      </ul>
	   </div>
	 </div>
  
	<div class="layui-body">
	 
		<!-- 内容主体区域 -->
		<div class="content_body">
			<div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>人员管理</a>
					<a>用户</a>
					<a><cite>修改用户</cite></a>
				</span>
			</div>
			<br><br>
			<div class="layui-row">
				<form class="layui-form layui-col-md4"  onSubmit="return userEdit()">
					<div class="layui-form-item">
						<label class="layui-form-label input-required">公司名称 ：</label>
						<div class="layui-input-block">
							<select name="company_id" id='company_id' lay-verify="required" lay-filter="system_company_id">
								<option value="">- - <?php echo $language_tag['index_public_select']; ?> - -</option>
								<?php if(is_array($company_data_result) || $company_data_result instanceof \think\Collection || $company_data_result instanceof \think\Paginator): if( count($company_data_result)==0 ) : echo "" ;else: foreach($company_data_result as $key=>$vo): ?>
								<option value="<?php echo $vo['company_id']; ?>" <?php if($vo['company_id'] == $data['company_id']): ?>selected='selected'<?php endif; ?>><?php echo $vo['company_name']; ?></option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label input-required">部门：</label>
						<div class="layui-input-block">
							<select name="department_id" id='department_id' >
								<?php if(is_array($department_data_result) || $department_data_result instanceof \think\Collection || $department_data_result instanceof \think\Paginator): if( count($department_data_result)==0 ) : echo "" ;else: foreach($department_data_result as $key=>$v): ?>
								<option value="<?php echo $v['department_id']; ?>" <?php if($v['department_id'] == $data['department_id']): ?>selected='selected'<?php endif; ?>><?php echo $v['department_name']; ?></option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
						</div>
						<input type="hidden" id="hidden_father_id" value="<?php echo $data2['department_id']; ?>" >
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label input-required">上级领导：</label>
						<div class="layui-input-block">
							<select name="superior_leader_id" id='superior_leader_id' lay-search>
								<?php if(is_array($user_data_result) || $user_data_result instanceof \think\Collection || $user_data_result instanceof \think\Paginator): if( count($user_data_result)==0 ) : echo "" ;else: foreach($user_data_result as $key=>$vo): ?>
								<option  value="<?php echo $vo['user_id']; ?>" <?php if($vo['user_id'] == $data['superior_leader_id2']): ?>selected='selected'<?php endif; ?> ><?php echo $vo['nickname']; ?></option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label ">职位：</label>
						<div class="layui-input-block">
							<input name="job_name" id='job_name' class="layui-input" value='<?php echo $data['job_name']; ?>' >
						</div>
					</div>

					<div class="layui-form-item">
						<label class="layui-form-label input-required">姓名：</label>
						<div class="layui-input-block">
							<input name="nickname" value='<?php echo $data['nickname']; ?>'  placeholder="<?php echo $language_tag['index_system_showUserManage_input_user_name']; ?>" autocomplete="off" class="layui-input" type="text">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label input-required">性别:</label>
						<div class="layui-input-block">
							<select name="gender" id='gender' >
								<option value="1" <?php if($data['gender'] == '1'): ?>selected='selected'<?php endif; ?>>男</option>
								<option value="2" <?php if($data['gender'] == '2'): ?>selected='selected'<?php endif; ?>>女</option>
								<option value="0" <?php if($data['gender'] == '0'): ?>selected='selected'<?php endif; ?>>未知</option>
							</select>
						</div>
					</div>
<!--					<div class="layui-form-item">-->
<!--						<label class="layui-form-label">密码：</label>-->
<!--						<div class="layui-input-block">-->
<!--							<input name="password" placeholder="<?php echo $language_tag['index_public_placeHolder']; ?>" autocomplete="off" class="layui-input" type="password">-->
<!--						</div>-->
<!--					</div>-->
<!--					<div class="layui-form-item">-->
<!--						<label class="layui-form-label">确认密码：</label>-->
<!--						<div class="layui-input-block">-->
<!--							<input name="password_q" placeholder="<?php echo $language_tag['index_public_placeHolder']; ?>" autocomplete="off" class="layui-input" type="password">-->
<!--						</div>-->
<!--					</div>-->
				
					<div class="layui-form-item">
						<label class="layui-form-label">联系方式：</label>
						<div class="layui-input-block">
							<input name="phone" value='<?php echo $data['phone']; ?>'  placeholder="<?php echo $language_tag['index_system_showDepartmentManage_input_phone']; ?>" autocomplete="off" class="layui-input" type="text">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">座机号：</label>
						<div class="layui-input-block">
							<input name="landline" value='<?php echo $data['landline']; ?>'  placeholder="" autocomplete="off" class="layui-input" type="text">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">邮件：</label>
						<div class="layui-input-block">
							<input name="email" value='<?php echo $data['email']; ?>'  placeholder="<?php echo $language_tag['index_public_placeHolder']; ?><?php echo $language_tag['index_system_showUserManage_email']; ?>" autocomplete="off" class="layui-input" type="text">
						</div>

					</div>

					<div class="layui-form-item">
						<label class="layui-form-label input-required">角色：</label>
						<div class="layui-input-block">
							<select name="role_id" id="role_id" lay-verify="required" >
							
								<?php if(is_array($role_data_result) || $role_data_result instanceof \think\Collection || $role_data_result instanceof \think\Paginator): if( count($role_data_result)==0 ) : echo "" ;else: foreach($role_data_result as $key=>$vo): ?>
								<option value="<?php echo $vo['role_id']; ?>" <?php if($vo['role_id'] == $data['role_id']): ?> selected<?php endif; ?>><?php echo $vo['role_name']; ?></option>

								<?php endforeach; endif; else: echo "" ;endif; ?>

							</select>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">状态：</label>
						<div class="layui-input-block">
							<select name="status" id='status' lay-verify="required">
								<option value="1" <?php if(1 == $data['status']): ?>selected="seleted"<?php endif; ?>>启用</option>
								<option value="0" <?php if(2 == $data['status']): ?>selected="seleted"<?php endif; ?>>禁用</option>
								<option value="3" <?php if(3 == $data['status']): ?>selected="seleted"<?php endif; ?>>锁定</option>
							</select>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn nav-submit" lay-submit="" lay-filter="formDemo" >提交</button>
							<a href='/oaperson/showUserManage'><button type="button" class="layui-btn layui-btn-primary">返回</button></a>
							<input type='hidden' id='user_id' value="<?php echo $data['user_id']; ?>" />
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php if(is_array($lianyi_email) || $lianyi_email instanceof \think\Collection || $lianyi_email instanceof \think\Paginator): if( count($lianyi_email)==0 ) : echo "" ;else: foreach($lianyi_email as $key=>$a): ?>
<input type='hidden' class='lianyi_email' value="<?php echo $a['email']; ?>">
<?php endforeach; endif; else: echo "" ;endif; ?>
  <input type='hidden' id='language_id' value="<?php echo \think\Session::get('user.language_id'); ?>" />
  <div class="layui-footer">
    <!-- 底部固定区域 -->
    © layui.com - 底部固定区域
  </div>
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>

<script src='/static/javascript/oa/oaperson.js'></script>
<script>
	var formSelects = layui.formSelects;



</script>
</body>
</html>