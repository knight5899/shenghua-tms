<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:88:"/var/www/html/test_erp/public/../application/index/view/order/order_abnormal_manage.html";i:1665292419;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>异常运单</title>
<style>
	
	.xm-select{min-height:25px!important;}
	.xm-select-title{min-height:25px!important;}
	.xm-select--suffix .xm-input{height:25px!important;}
	
</style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    
	<?php if(!\think\Request::instance()->get('hide_menu')): ?>
	<div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <div title="菜单收缩" class="kit-side-fold"><i class="layui-icon layui-icon-spread-left" aria-hidden="true"></i></div>
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
        </div>
    </div>
<?php endif; ?>
    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>运单管理</a>
                        <a><cite>异常运单</cite></a>
                    </span>
                </div>
				
				
				
				<?php if(!\think\Request::instance()->get('hide_menu')): ?>
                <div class='layui-block all-search-bg'>
				
				<div class="layui-tab">
  <ul class="layui-tab-title">
    <li   <?php if(\think\Request::instance()->get('handle') == 1): ?> class="layui-this"  <?php endif; ?> ><a href='/order/showOrderAbnormalManage?handle=1'>未处理</a></li>
    <li <?php if(\think\Request::instance()->get('handle') == 2): ?> class="layui-this"  <?php endif; ?>><a href='/order/showOrderAbnormalManage?handle=2'>已处理</a></li>
    <li <?php if(\think\Request::instance()->get('handle') == 3): ?> class="layui-this"  <?php endif; ?>><a href='/order/showOrderAbnormalManage?handle=3'>全部</a></li>


  </ul>

</div> 
				
				
                    <form class="layui-form" method='get' action=''>
                     
                                <div class="layui-input-inline">
                                     <label class="layui-form-label">项目名称:</label>
                                    <div class="layui-input-inline">
                                    		<select name="multi_project_id" lay-verify="required" id='department_id' lay-verify="required" xm-select="select1"  xm-select-direction="down" xm-select-search="/order/getProjectAjax" xm-select-search-type="dl">
													<?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$vo): ?>									  					
													<option value="<?php echo $vo['project_id']; ?>" selected><?php echo $vo['project_name']; ?></option>	
														<?php endforeach; endif; else: echo "" ;endif; ?>
									  					
											</select>
                                    </div>
                                </div>
                         	
                                <div class="layui-input-inline">
                                     <label class="layui-form-label">异常类型:</label>
                                    <div class="layui-input-inline">
                                    		<select name="multi_abnormal_type_id" lay-verify="required" id='multi_abnormal_type_id' lay-verify="required" xm-select="select2"  xm-select-direction="down" xm-select-search="/order/getAbnormalTypeAjax" xm-select-search-type="dl">
													<?php if(is_array($abnormalTypeResult) || $abnormalTypeResult instanceof \think\Collection || $abnormalTypeResult instanceof \think\Paginator): if( count($abnormalTypeResult)==0 ) : echo "" ;else: foreach($abnormalTypeResult as $key=>$vo): ?>									  					
													<option value="<?php echo $vo['abnormal_type_id']; ?>" selected><?php echo $vo['abnormal_type_name']; ?></option>	
														<?php endforeach; endif; else: echo "" ;endif; ?>
									  					
											</select>
                                    </div>
                                </div>

                        
                    
						    <div class="layui-input-inline">
						        <label class="layui-form-label">运单编号:</label>
						        <div class="layui-input-inline" style="width:100px">
						        	
											
										<input type="text" class="layui-input" value="<?php echo \think\Request::instance()->get('orders_number'); ?>" name="orders_number" id="" value="" />
											
						        			
						        		
						        </div>
						    </div>
							
							<div class="layui-input-inline">
                                    <label class="layui-form-label">承运商:</label>
                                    <div class="layui-input-inline" style='width:119px'>

                                        <select name="supplier_uuid" id='supplier_uuid' xm-select="select3"
                                                xm-select-direction="down" xm-select-search="/order/getsupplierAjax"
                                                xm-select-search-type="dl">
                                            <?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
                                            <option value="<?php echo $vo['supplier_uuid']; ?>" selected><?php echo $vo['supplier_name']; ?></option>
                                            <?php endforeach; endif; else: echo "" ;endif; ?>

                                        </select>


                                    </div>
                                </div>
				
						    <div class="layui-input-inline">
						        <label class="layui-form-label">处理方式:</label>
						        <div class="layui-input-inline">
						        		<select id='abnormal_status' name='indemnity_type'>
                                             <option value="">请选择</option>
											<?php if(is_array($baseConfig['order']['abnormal_type']) || $baseConfig['order']['abnormal_type'] instanceof \think\Collection || $baseConfig['order']['abnormal_type'] instanceof \think\Paginator): if( count($baseConfig['order']['abnormal_type'])==0 ) : echo "" ;else: foreach($baseConfig['order']['abnormal_type'] as $key3=>$vo3): ?>
											<option <?php if(\think\Request::instance()->get('indemnity_type')==$key3): ?>selected="selected"<?php endif; ?> value="<?php echo $key3; ?>"><?php echo $vo3; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>
						        			
						        			
						        		</select>
						        </div>
						    </div>
					
                             <div class="layui-input-inline">
                                    <label class="layui-form-label">运单时间:</label>
                                    <div class="layui-input-inline">
										<div class="layui-input-inline" style="width: 85px;">
											
                                        <input type="text" id="pickup_time"  name="pickup_time" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('pickup_time'); ?>"  placeholder="开始时间" class="layui-input pickup_time">
										</div>
									
										<div style="width: 85px;" class="layui-input-inline">
										<input type="text" name="pickup_time_end" type="text" class="pickup_time layui-input" value="<?php echo \think\Request::instance()->get('pickup_time_end'); ?>" placeholder="结束时间">
                                 </div>
									</div>
                                </div> 	

                                <div class="layui-input-inline" >
                                    <label class="layui-form-label">分公司:</label>
                                    <div class="layui-input-inline" style="width: 87px;">
                                         <select id='company_id' name='choose_company_id' >
                                            <option value=''>--全部--</option>
											<?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$v): ?>
												<option value="<?php echo $v['company_id']; ?>" 
												
												<?php if(\think\Request::instance()->get('choose_company_id') != ''): if(\think\Request::instance()->get('choose_company_id') == $v['company_id']): ?>selected <?php endif; else: if(\think\Session::get('user.company_id') == $v['company_id']): ?>selected <?php endif; endif; ?>
												>

												<?php echo $v['company_name']; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>                                           
                                        </select>
                                    </div>
                                </div>									
                                <button class="layui-btn nav-search search_button" >搜索</button>
                                <a href="/order/showOrderAbnormalManage"><button type="button" class="layui-btn layui-btn-primary swiper-right-form-reset search_button" >重置</button></a>
                        <!--<div class='layui-input-inline'>-->
                        <!--<input type="text" id="" name=""  placeholder="电话、联系人、操作人" class="layui-input">-->
                        <!--</div>-->
          
                    </form>

                </div>
				<?php endif; ?>
            </div>
           <div class="content-bg" >
                        
                         
                           <div class="table-nont user-manage " >
                               <table id="language-table"  lay-filter="language-table">
                          
                               </table>
                             <script type="text/html" id="toolbarDemo" lay-filter="toolbar1">
                       			<div class="layui-inline" lay-event="add"> <i class="layui-icon layui-icon-add-1"></i> </div>
                       
                       	</script>
                           </div>
                       
                       </div>

        </div>
		
		
		
    </div>





 
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/base.js'></script>
<script src='/static/javascript/system/source.js'></script>
<script type="text/html" id="tool_bar">
<?php if(!\think\Request::instance()->get('hide_menu')): ?>

 <span ><a href='javascript:openNewLayer("/order/showOrderAbnormalAdd","新建异常运单") '><button class="layui-btn nav-add layui-btn-sm">新建异常运单</button></a></span>
 <span ><button onclick="delorders()" class="layui-btn nav-add layui-btn-sm">删除</button></span>
<span ><a target="blank" href='showOrderAbnormalManage?download=1&<?php echo http_build_query($_GET); ?>'><button onclick="" class="layui-btn nav-add layui-btn-sm">下载</button></a></span>
  
	<?php endif; ?>
	<span ><a href='javascript:void(0);'><button onclick="signPaper()" lay-event="signPaper" class="layui-btn nav-add layui-btn-sm">理赔核查表</button></a></span>
	<span ><a href='javascript:void(0);'><button onclick="detailPaper()" lay-event="signPaper" class="layui-btn nav-add layui-btn-sm">赔款明细表</button></a></span>
 <a class="layui-btn layui-btn-sm layui-btn-normal" id="clear"><i class="layui-icon layui-icon-delete"></i>清除缓存</a>
    <a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="save" ><i class="layui-icon "></i>保存设置</a>	
</script>
<script>

  layui.config({}).extend({
        soulTable: '/static/layui-soul-table/ext/soulTable',
        tableChild: '/static/layui-soul-table/ext/tableChild',
        tableMerge: '/static/layui-soul-table/ext/tableMerge',
        tableFilter: '/static/layui-soul-table/ext/tableFilter',
        excel: '/static/layui-soul-table/ext/excel'
    });


layui.use(['form', 'laydate', 'table', 'soulTable', 'element'], function(){
    var table = layui.table;
	var soulTable = layui.soulTable;
    var   laytable =layui.laytable

    layui.use('element', function(){
        var element = layui.element;
    });
    var isShow = true;  //定义⼀个标志位
    $('.kit-side-fold').click(function(){
        //选择出所有的span，并判断是不是hidden
        $('.layui-nav-item span').each(function(){
            if($(this).is(':hidden')){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
        //判断isshow的状态
        if(isShow){
            $('.layui-side.layui-bg-black').width(50); //设置宽度
            $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
            //将footer和body的宽度修改
            $('.layui-body').css('left', 60+'px');
            $('.layui-footer').css('left', 60+'px');
            //将⼆级导航栏隐藏
            $('dd span').each(function(){
                $(this).hide();
            });
            //修改标志位
            isShow =false;
        }else{
            $('.layui-side.layui-bg-black').width(200);
            $('.kit-side-fold i').css('margin-right', '10%');
            $('.layui-body').css('left', 200+'px');
            $('.layui-footer').css('left', 200+'px');
            $('dd span').each(function(){
                $(this).show();
            });
            isShow =true;
        }
    });

	 global_table= table.render({
		    elem: '#language-table'
		    ,height: tableHeight
		    ,url: '/order/showOrderAbnormalManage?<?php echo http_build_query($_GET); ?>'///shipment/showLineAjax?orders_number=<?php echo \think\Request::instance()->get('orders_number'); ?>&start_pickup_time=<?php echo \think\Request::instance()->get('start_pickup_time'); ?>&end_pickup_time=<?php echo \think\Request::instance()->get('end_pickup_time'); ?>' //数据接口
		
		    ,response: {

		           statusCode: 200 //规定成功的状态码，默认：0

		          } 
		    ,page: true //开启分页
			,limits:[100,200,300,400,500]
		    ,limit:100
		    ,toolbar: '#tool_bar' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
		    ,totalRow: true //开启合计行
		    ,cols: [[ //表头
                        {type: 'checkbox', fixed: 'left',field: 'checkbox'}
		             	,{field: 'abnormal_id2', title: '序号',width:80,type:'numbers'}
						,{field: 'send_goods_company', title: '发货方',width:90,
						templet:function(e){
						
						if(!e.order_info)
							return ""
							else
						return e.order_info.send_goods_company
						
						}
						}
		             	, {field: 'orders_number', title: '运单编号',width:90,sort:true}
						,{field: 'goods_name', title: '货物名称',width:100,sort:true,templet:function(e){
							var info="";if(!e.goods_info)
							return ""
							else
							e.goods_info.map(function(i){
								info=info+i.goods_name+"/";
							})
							return info.substring(0,info.length-1)
						}}
						,{field: 'accept_name', title: '下单件数',width:60,sort:true,templet:function(e){
							var info="";if(!e.goods_info)
							return ""
							else
							e.goods_info.map(function(i){
								info=info+i.estimated_count+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'accept_cellphone', title: '下单数量',width:60,sort:true,templet:function(e){
							var info="";if(!e.goods_info)
							return ""
							else
							e.goods_info.map(function(i){
								info=info+i.estimated_pack_count+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'accept_all_address', title: '下单重量',width:60,sort:true,templet:function(e){
							var info="";if(!e.goods_info)
							return ""
							else
							e.goods_info.map(function(i){
								info=info+i.estimated_weight+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'goods_name', title: '下单体积',width:60,sort:true,templet:function(e){
							var info="";if(!e.goods_info)
							return ""
							else
							e.goods_info.map(function(i){
								info=info+i.estimated_volume+"/";
							})
							return info.substring(0,info.length-1)
						}}
						, {
                    field: 'shipment_count', title: '发运件数', width: 60, sort: true

                }, {
                    field: 'shipment_pack_count', title: '发运数量', width: 60, sort: true

                }
                , {
                    field: 'shipment_volume', title: '发运体积', width: 60, sort: true
                }
                , {
                    field: 'shipment_weight', title: '发运重量', width: 60, sort: true
                }
						,{field: 'accept_goods_company', title: '收货方',width:90,sort:true,
						templet:function(e){
						
						if(!e.accept_goods_info)
							return ""
							else
						return e.accept_goods_info.accept_goods_company
						
						}
						}
						,{field: 'remark6', title: '收货方地址',width:90,sort:true,templet:function(e){
							return  e.order_info.accept_address
						}}
		      			
		      			,{field: 'accept_cellphone', title: '联系电话',width:90,sort:true,
						templet:function(e){return e.order_info.accept_cellphone}}
						,{field: 'accept_name', title: '收货人',width:90,sort:true,
						templet:function(e){return e.order_info.accept_name}}
						,{field: 'supplier_name', title: '承运商',width:150,sort:true }
						,{field: 'indemnity_event_description', title: '理赔事件描述',width:150,sort:true }
						,{title: '责任方',width:120,sort:true,
						templet:function(e){
						if(e.responsible_party)
							return baseConfig['abnormal']['responsible_party'][e.responsible_party];
							else
							return '';
							}
						}
						,{field: 'abnormal_type_name', title: '异常类型',width:110,sort:true}
						,{field: 'damaged', title: '是否破损',width:110,sort:true,templet:function(e){
						if(e.damaged)
						return baseConfig['abnormal']['damaged'][e.damaged];
						else
						return '';
						}}
		      			,{field: 'abnormal_count', title: '货损数量',width:110,sort:true}
						,{field: 'return_goods_type', title: '退货处理方式',width:110,sort:true,templet:function(e){
						if(e.return_goods_type)
						return baseConfig['abnormal']['return_goods_type'][e.return_goods_type];
						else
						return '';
						}}
						,{field: 'indemnity_type', title: '理赔处理方式',width:110,sort:true,templet:function(e){
						if(e.indemnity_type)
						return baseConfig['abnormal']['indemnity_type'][e.indemnity_type];
						else
						return '';
						}}
		      			,{field: 'abnormal_money', title: '理赔金额(发货方)',width:110,sort:true}
		      			,{field: 'supplier_abnormal_money', title: '理赔金额(承运商)',width:110,sort:true}
		      			
		      			,{field: 'pickup_time', title: '运单日期',width:110,templet:function(e){
						
						if(e.order_info.pickup_time)
						return php_date('Y-m-d',e.order_info.pickup_time);
						return '';
						
						}},{field: 'true_time', title: '实际收货日期',width:110,templet:function(e){
						
						if(e.order_info.true_time)
						return php_date('Y-m-d',e.order_info.true_time);
						return '';
						
						}},{field: 'customer_order_number', title: '客户编号',width:110,templet:function(e){
						
						if(e.order_info.customer_order_number)
						return e.order_info.customer_order_number;
						return '';
						
						}}
						
		      			,{field: 'abnormal_number', title: '异常编号',width:110,sort:true}
						,{field: 'estimated_pack_unit', title: '包装单位',width:90,sort:true,templet:function(e){
							var info="";if(!e.goods_info)
							return ""
							else
							e.goods_info.map(function(i){
								info=info+ baseConfig['order']['goods_pack'][i.estimated_pack_unit]+"/";
							})
							return info.substring(0,info.length-1)
						}}
						
						
		      			
		      			,{field: 'remark', title: '备注',width:90}
		      			
		      			

		      			,{field: 'remark3', title: '异常图片',width:90,templet:function(e){
						if(e.upload.length>0)
						return `<a href="javascript:void(0);" onclick="openlayer('/order/orderABnormalUploadView?abnormal_id=${e.abnormal_id}','异常图片')" style="color: #0f0">点击查看</a>`
						return '';
						}},{field: 'remark4',fixed: 'right', title: '操作',width:60,templet:function(e){
						return `<span><a href='javascript:openNewLayer("/order/showOrderAbnormalAdd?abnormal_id=${e.abnormal_id}&orders_id=${e.orders_id}","修改异常运单")  '><button class="layui-btn layui-btn-sm hover-edit layui-btn-primary">修改</button></a></span>`
						}
						
						}
		      			
		     

		    ]],
			 filter: {
                items: ['column', 'data', 'condition', 'editCondition', 'excel', 'clearCache'],
                cache: true
            },
			 done: function (res, curr, count) {
			  soulTable.render(this)
			    //如果是异步请求数据方式，res即为你接口返回的信息。
			    //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
			    console.log(res);
			  
			    //得到当前页码
			    console.log(curr);
			  var that = this.elem.next();
                    
			    //得到数据总量
			    console.log(count);
			  res.data.forEach(function(item,index){
				  if(item.follow_level)
				  if(item.follow_level==1)
				  {
					  var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']");
                            tr.css("background-color", "#FF5722");
                            tr.css("color", "white");

				  }
				  
				  
			  })
			
			  }
		  });
	
  $(document).on('click', '#clear', function () {
            layui.soulTable.clearCache(global_table.config.id)
            layer.msg('已还原！', {icon: 1, time: 1000})
        })


//多选事件
    table.on('checkbox(language-table)', function(obj){
        if(obj.type=="all"){ //全选
            if(obj.checked==true){
                $(".layui-table-body table.layui-table tbody tr").css('background','#99CCFF');
            }else{
                $(".layui-table-body table.layui-table tbody tr").map(function(i,d){
				$(d).css('background','#fff');
				})	
            }
        }else{ //单选
            if(obj.checked==true){
                obj.tr.css('background','#99CCFF');
            }else{
                obj.tr.css('background','#fff');
            }
        }

    });
    //监听头工具栏事件
    table.on('toolbar(language-table)', function(obj){
		console.log(obj)
		console.log(obj.event)
		var myTable=obj.config;
 var storeKey = location.pathname + location.hash + myTable.id;
 var colsStr=JSON.stringify(obj.config.cols)
      var checkStatus = table.checkStatus(obj.config.id)
      ,data = checkStatus.data; //获取选中的数据
      switch(obj.event){
	         case 'signPaper':console.log(obj);break;
	  
	        case 'save':
			   layer.msg("保存成功");
			   localStorage.setItem( storeKey, colsStr);
			break;
		  case 'addImportant':
		  if(data.length>0)
		 data.forEach(function(e){
		 	$.ajax({
		 		url:"/index/changeStatus",
		 		data:{table_id:e.orders_id,
				table_id_name:'orders_id',
				table_name:"orders",
				status:1,field:"follow_level"},
		 		success:function(e){
		 			alert("标记成功");
		 			window.location.reload();
		 		}
		 		
		 	})
		 	
		 })
		 else
		 layer.msg("请选择一行");
		  
		  break;
        
      };
    });
})
	  //第一个实例

</script>




<script>
    function openlayer(url,title){
        layer.open({
            type:2,
            title:title,
            content:url,
            area:["500px","600px"]
        })
    }

    function delorders(){
        var checkdata= layui.table.checkStatus("language-table")['data'];
        if(checkdata.length!=1)
            layer.msg("请选择一条数据");
        else
        {
            layer.open({
                title: '信息'
                ,content: '真的删除吗？'
                ,btn: ['确定', '取消']
                ,yes: function(index, layero){
                    //按钮【按钮一】的回调
                    $.ajax({
                        url:"/index/changeStatus",
                        data:{table_id:checkdata[0]['abnormal_id'],table_id_name:'abnormal_id',table_name:"orders_abnormal",status:0,field:"status"},
                        success:function(e){
                            window.location.reload();
                            // languagetable_global.reload();
                        }

                    })
                }
            });


        }
    }



    var layer = layui.layer;

    var form = layui.form;
	
	form.on('select(replacement_prive_type)',function(data){
		var table_id = $(data.elem).parents("tr").children(".order_id").html();
		
		var table_id_name = "orders_id";
		var table_name = "orders";
		status=data.value;
		
		$.ajax({
		    type: "POST",
		    url: '/index/changeStatus',
		    data: {
		        table_id:table_id,
		        table_id_name:table_id_name,
		        table_name:table_name,
		        status:status,
				field:"replacement_prive_type"
		    },
		    success: function(data){
		        //console.log(data);
		        if(data.code!=200){
		            layer.msg(data.msg);
		            return false;
		        }else if(data.code==200) {
		            layer.msg('操作成功',{time:1,end : function(layero, index){
		
		                }
		            });
		        }
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
		        layer.msg('验证失败')
		    }
		});
		
		
		
		console.log(data)
	})
	
	
    form.on('switch(switchTest)', function(data){
        var table_id = $(data.elem).parents("tr").children(".order_id").html();

        var table_id_name = "order_id";
        var table_name = "order";
        if(this.checked == true){
            var status = 1;
        }else{
            var status = 0;
        }

        $.ajax({
            type: "POST",
            url: '/index/changeStatus',
            data: {
                table_id:table_id,
                table_id_name:table_id_name,
                table_name:table_name,
                status:status,
            },
            success: function(data){
                //console.log(data);
                if(data.code!=200){
                    layer.msg(data.msg);
                    return false;
                }else if(data.code==200) {
                    layer.msg('操作成功',{time:1,end : function(layero, index){

                        }
                    });
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                layer.msg('验证失败')
            }
        });
    });

 function openNewLayer(url,title){
  layer.open({
            type: 2,
			maxmin: true,
            title: title,
            content: url,
            area: ["100%", "100%"]
        })
  
  }
  function signPaper(){
 var data = layui.table.checkStatus('language-table');
 if(data.data.length==1)
window.open("/order/orderAbnormalReview?abnormal_id="+data.data[0]['abnormal_id'],"_blank"); 
 else
 layer.msg("请选择一条数据")
 
 
 } function detailPaper(){
 var data = layui.table.checkStatus('language-table');
 if(data.data.length==1)
window.open("/order/orderAbnormalSupplierReview?abnormal_id="+data.data[0]['abnormal_id'],"_blank"); 
 else
 layer.msg("请选择一条数据")
 
 
 }

</script>
</body>
</html>


