<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:86:"/var/www/html/test_erp/public/../application/index/view/despatch/pickup_order_add.html";i:1655951487;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>

				新增安排

	
	</title>
<style type="text/css">
.layui-inline{
margin-right:0px;
}
#pickup_order_lingdan > div{
	border: 1px solid #a9a9a9;
	padding: 7px;
	}
.box-warp{
	margin-bottom: 5px;
	line-height: 35px;
	display: flex;
	align-items: flex-start;
}
   .huowuOpt{
	   display: flex;
	   justify-content: space-around;
	   min-width: 70px;
   }

	.addGoods{
		font-size: 34px;
		color: #7777ff;
		cursor: pointer;
	}
	.delHuowu{
		cursor: pointer;
		font-size: 34px;
		color: #e50000;
	}
	.tihuo-t-01{
		font-size: 17px;
		padding-bottom: 8px;
		display: block;
		color: #383838;
		padding-top: 0px;
	}
.addTihuoSupplier{
	cursor: pointer;
	font-size: 38px;
	padding-left: 5px;
	font-weight: bold;
	color: #525252;
	padding-top: 0;
	line-height: 0;
	padding-bottom: 15px;
}
	.closeTihuolist{
		position: absolute;
		right: 5px;
		top: 0;
		color: #525252;
		border: 1px solid #525252;
		border-radius: 20px;
		font-size: 22px;
		display: flex;
		justify-content: center;
		align-items: center;
		width: 20px;
		height: 20px;
		cursor: pointer;
	}
	.fee-item{
		display: flex;justify-content: flex-start;flex-wrap: nowrap;
		    border-bottom: 1px #eee dashed;
		    margin-top: 6px;
			    padding-top: 8px;
				padding-left: 6px;
				background-color: #fef;
	}
	.fee-item .layui-form-label {
		width: 70px;
	}
	.fee-content{
		display: flex;
	}
	.fee-add{
		    font-size: 50px;
		       width: 90px;
		       text-align: center;
		    color: #f00;
			cursor: pointer;
	}
	.fee-subtract{
		font-size: 50px;
		    width: 90px;
		    text-align: center;
		color: #00f;
		cursor: pointer;
	}
	.shuilv,.zonge{
		pointer-events: none;
	}
	
</style>
<link rel="stylesheet" type="text/css" href="/static/css/easyui.css"/>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

	<div class="layui-header">
		    <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
	</div>

	<div class="layui-side layui-bg-black">
		<div class="layui-side-scroll">
			<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
			      <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
		</div>
	</div>

	<div class="layui-body">

		<!-- 内容主体区域 -->
		<div class="content_body">
			<div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>调度管理</a>
					<a >发运安排</a>	
					<a><cite>
					
						新增发运
					
					</cite></a>
				</span>
			</div>
			<br/>
		

			<br/>
			
	<hr/>	
	

							<div class="layui-tab" style="margin: 0 auto;max-width: 1200px;">
					
							  <div class="layui-tab-content">
							    <div class="layui-tab-item layui-show">
										<form class="layui-form layui-form-pane"   id="form_zhengche" onSubmit="return pickupOrderZhengcheAdd()">
												
												
												<div class="layui-form-item">
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" >结束安排:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															
																<input name="jiesu" class=" layui-form-checkbox" lay-skin="primary" type="checkbox" checked>
															
																							
														</div>
													</div><div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label " >需短驳:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															
																<input name="short_barge" class=" layui-form-checkbox" lay-skin="primary" type="checkbox" >
															
																							
														</div>
													</div><div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label " >需配送:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															
																<input name="delivery" class=" layui-form-checkbox" lay-skin="primary" type="checkbox" >
															
																							
														</div>
													</div>
												</div>
												
												
												
												<fieldset class="easyui-fieldset">
												                    <legend>
												                        服务信息
												                    </legend>
												                    <table class="panel-body" border="1" cellspacing="0" cellpadding="1" style="border-collapse: collapse;">
												
												                        <tbody><tr>
												                            <td align="right" style="width: 85px;" class="datagrid-td-rownumber">
												                                承运商：
												                            </td>
												                            <td>
																			<select lay-verify="required" name="supplier_line_id" lay-filter="checksupplier" class="layui-input">
																				<option disabled="disabled" selected="selected">请选择</option>
																				<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
																					
																					<option value="<?php echo $vo['supplier_line_id']; ?>"  > <?php echo $vo['supplier_name']; ?></option>
																				<?php endforeach; endif; else: echo "" ;endif; ?>
																			</select>
																					 </td>
												                            <input type="hidden" name="supplier_id" id="supplier_id" value="" />
												                            <input type="hidden" name="orders_id"  value="<?php echo $or['orders_id']; ?>" />
												                            <td align="right" style="width: 65px;" class="datagrid-td-rownumber">
												                                专线编号：
												                            </td>
												                            <td>
												                                <input style="width: 85px;" id="special_number" name="special_number" type="text" class="easyui-textbox textbox-f" textboxname="v_si_car_code" >
																				
																				 </td><td align="right" style="width: 85px;" class="datagrid-td-rownumber">
												                                车牌号：
												                            </td>
												                            <td>
												                                <input style="width: 85px;"  id="v_si_car_code" name="car_id" type="text" class="easyui-textbox textbox-f" textboxname="v_si_car_code" >
																				
																				 </td>
												                      
												                           
												                            <td align="right" style="width: 85px;" class="datagrid-td-rownumber">
												                                发站电话：
												                            </td>
												                            <td>
												                                <input id="v_si_send_phone" type="text" class="easyui-textbox textbox-f" textboxname="v_si_send_phone" > 
																					</td>
																			
																		
																			
												                            <td align="right" style="width: 85px;" class="datagrid-td-rownumber">
												                                到站联系人：
												                            </td>
												                            <td>
												                                <input style="width: 45px;" id="v_si_arrive_phone" type="text" class="easyui-textbox textbox-f" textboxname="v_si_arrive_phone" > </td>
												                            <td align="right" style="width: 85px;" class="datagrid-td-rownumber">
												                                到站电话：
												                            </td>
												                            <td>
												                                <input id="v_si_arrive_man" type="text" class="easyui-textbox textbox-f" textboxname="v_si_arrive_man" >
																				</td>
												                        </tr>
												
												                  
												                    </tbody></table>
												                </fieldset>
												<fieldset id="">
													<legend>
													    货物信息
													</legend>
													<table class="layui-table" id="goods_info">
													  <colgroup>
													    <col width="100">
													    <col width="90">
													    <col width="90">
													    <col width="90">
													    <col width="90">
													  </colgroup>
													  <thead>
													    <tr>
													
													      <th>货物</th>					      
													      <th>数量</th>	
													      <th>包装数量</th>	
													      <th>重量</th>	
													      <th>体积</th>	
													      <th>发运件数</th>
													      <th>发运数量</th>
													      <th>发运重量</th>
													      <th>发运体积</th>
													      <th>计价单位</th>
													      <th>单价</th>
													      <th>发运费</th>
													      	
													    </tr> 
													  </thead>
													  <tbody>
													  	<?php if(is_array($orderGoodsResult) || $orderGoodsResult instanceof \think\Collection || $orderGoodsResult instanceof \think\Paginator): if( count($orderGoodsResult)==0 ) : echo "" ;else: foreach($orderGoodsResult as $key1=>$v): ?>
													    <tr class="goods_list">
													     
														   <td><?php echo $v['goods_name']; ?></td>
													      <td><?php echo $v['realy_weight']; ?></td>
													      <td><?php echo $v['realy_volume']; ?></td>
													      <td><?php echo $v['realy_count']; ?></td>
													      <td><?php echo $v['realy_pack_count']; ?></td>
													      <input type="hidden" name="goods_name[<?php echo $key1; ?>]" id="" value="<?php echo $v['goods_name']; ?>" />
													      <input type="hidden" name="goods_id[<?php echo $key1; ?>]" id="" value="<?php echo $v['goods_id']; ?>" />
														  <td><input type="text" name="package_number[<?php echo $key1; ?>]" class="layui-input package_number" id="" value="0" /></td>
														  <td><input type="text" name="despatch_number[<?php echo $key1; ?>]" class="layui-input despatch_number" id="" value="0" /></td>
														  <td><input type="text" name="despatch_weight[<?php echo $key1; ?>]" class="layui-input despatch_weight" id="" value="0" /></td>
														  <td><input type="text" name="despatch_volume[<?php echo $key1; ?>]" class="layui-input despatch_volume" id="" value="0" /></td>
														  <td style="overflow:inherit ;"><select lay-filter="changeBilingUnit" name="billing_unit[<?php echo $key1; ?>]" class="layui-input billing_unit" id="">
																								<option value="0" disabled="disabled" selected="selected">请选择</option>
																							<?php if(is_array($baseConfig['despatch']['billing_unit']) || $baseConfig['despatch']['billing_unit'] instanceof \think\Collection || $baseConfig['despatch']['billing_unit'] instanceof \think\Paginator): if( count($baseConfig['despatch']['billing_unit'])==0 ) : echo "" ;else: foreach($baseConfig['despatch']['billing_unit'] as $key2=>$vo2): ?>
																													 
																								<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
																														 <?php endforeach; endif; else: echo "" ;endif; ?>
																							</select></td>
														  <td><input type="text"  class="layui-input despatch_unit" name="despatch_unit[<?php echo $key1; ?>]" id="" value="0" /></td>
														  <td><input type="text" lay-verify="required|number" class="layui-input input-required  despatch_money" name="despatch_money[<?php echo $key1; ?>]" id="" value="0" /></td>
													      
													    </tr>
													    <?php endforeach; endif; else: echo "" ;endif; ?>
													
													
													<tr>
													 
													   <td>合计</td>
													  <td></td>
													  <td></td>
													  <td></td>
													  <td></td>
													  
													  <td><div class="despatch_number_all" id="despatch_number_all">
													  	0
													  </div></td>
													  <td><div class="despatch_weight_all"   id="despatch_weight_all" >
													  	0
													  </div></td>
													  <td><div class="despatch_volume_all"   id="despatch_volume_all" >
													  	0
													  </div></td>
													  <td></td>
													  <td></td>
													  <td><div class="despatch_money_all"   id="despatch_money_all" >
													  	0
													  </div></td>
													  
													</tr>
													
													
													  </tbody>
													</table>
													
												</fieldset>
												
												
												<fieldset id="">
													<legend>
													    支付信息
													</legend>
													<div class="layui-form-item">
														<div class="layui-inline">
															<label class="layui-form-label input-required" >付款方式:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="pay_type">
																	<?php if(is_array($baseConfig['despatch']['pay_type']) || $baseConfig['despatch']['pay_type'] instanceof \think\Collection || $baseConfig['despatch']['pay_type'] instanceof \think\Paginator): if( count($baseConfig['despatch']['pay_type'])==0 ) : echo "" ;else: foreach($baseConfig['despatch']['pay_type'] as $key2=>$vo2): ?>
																							 
																		<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
																								 <?php endforeach; endif; else: echo "" ;endif; ?>
																</select>									
															</div>
														</div><div class="layui-inline">
															<label class="layui-form-label input-required" >现付:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<input id="pay" name="pay" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
																									
															</div>
														</div><div class="layui-inline">
															<label class="layui-form-label input-required" >未付:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<input id="Unpaid" name="Unpaid" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
																									
															</div>
														</div><div class="layui-inline">
															<label class="layui-form-label input-required" >油卡:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<input id="oil_card" name="oil_card" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
																									
															</div>
														</div>
													</div>
													</fieldset>
													<br>
													
													<fieldset id="">
														<legend>成本信息</legend>
														<div class="layui-form-item">
															<div  class="fee-content">
															
															<div class="fee-add" data-id="zhengche" data-index="0">
																+
															</div>
															
															<div class="fee-item" style="">
																
															
															<div class="layui-inline layui-col-md3" >
																<label class="layui-form-label " style="width: 100px;">成本:</label>
																<div class=" layui-col-md5">
																<select name="fee_id_zhengche[0]" class="fee_select" lay-filter="fee_select">
																	<option value="">请选择</option>
																	<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
																	<option value="<?php echo $vo4['cost_id']; ?>"><?php echo $vo4['cost_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>
																</div>
															</div><div class="layui-inline " >
																<label class="layui-form-label ">金额:</label>
																<div class="layui-col-md5">
																	<input name="money_zhengche[0]" value="<?php echo $or['money']; ?>"  placeholder="" autocomplete="off" class="layui-input money" type="text">
																											
																</div>
															</div>	<div class="layui-inline " >
																<label class="layui-form-label ">备注:</label>
																<div class="layui-col-md5">
																	<input  name="remark_zhengche[0]" value=""  placeholder="" autocomplete="off" class="layui-input remark" type="text">
																											
																</div>
															</div>
															
																	</div>			
															
																
															</div>	
														</div>
														
													</fieldset>
													
													<div class="">
														
													</div>
													
												
												<fieldset id="">
													<legend>
													    订单信息
													</legend>
												<div class="layui-form-item"  id='pickup_order_zhengche' style="pointer-events: none;" >	

													<input type="hidden" name="money"  id="all_money" value="" />
													<div class="layui-form-item" style="border: 1px solid #bbb; padding: 10px 5px;">
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" >订单号:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
											  				<input id="all_weight" name="orders_number" value="<?php echo $or['orders_number']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
										
														</div>
													</div>


													
												
													
													
							
										
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" style='width:140px;'>预计提货日期:</label>
														<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
															 <input id='zc_hb_pickup_time_<?php echo $or['orders_id']; ?>'  orders_id="<?php echo $or['orders_id']; ?>" name="pickup_time" value="<?php if($or['pickup_time'] != ''): ?><?php echo date('Y-m-d',$or['pickup_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
											
														</div>
													</div>		
														<div class="layui-inline">
														<label class="layui-form-label input-required" style='width:140px;'>预计送货日期:</label>
														<div class="layui-input-inline" style='width:120px;'>
															 <input id='zc_hb_send_time_<?php echo $or['orders_id']; ?>' orders_id="<?php echo $or['orders_id']; ?>"  name="send_time" value="<?php if($or['send_time'] != ''): ?><?php echo date('Y-m-d',$or['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
											
														</div>
													</div>	
													<br />	
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required">发货地:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="send_province_id" class='province_id'  lay-verify="required" lay-filter="sheng-shi">
																<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>				
													<div class="layui-inline" style='margin-right:0px;'>

														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="send_city_id" class='city_id'  lay-verify="required" lay-filter="shi-qu">
																<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>

														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="send_area_id" class='area_id'  lay-verify="required" lay-filter="qu">
																<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>		
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" >发货地址:</label>
														<div class="layui-input-inline" style='margin-right:0px;'>
											  				<input id="all_weight" name="send_address" value="<?php echo $or['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
										
														</div>
													</div>	
													<br />
							
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required">收货地:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="accept_province_id" id='send_city_id' class="province_id"  lay-verify="required" lay-filter="sheng-shi">
																<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>				
													<div class="layui-inline" style='margin-right:0px;'>

														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="accept_city_id" id='send_city_id' class="city_id"  lay-verify="required" lay-filter="shi-qu">
																<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>

														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="accept_area_id" id='send_city_id' class="area_id"  lay-verify="required" lay-filter="qu">
																<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>		
													<div class="layui-inline" >
														<label class="layui-form-label input-required">收货地址:</label>
														<div class="layui-input-inline">
											  				<input id="all_weight" name="accept_address" value="<?php echo $or['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
												
														</div>
													</div>	
													<div class="layui-inline" >
														<label class="layui-form-label ">备注:</label>
														<div class="layui-input-inline">
											  				<input id="all_weight" name="remark" value="<?php echo $or['remark']; ?>"  placeholder="" autocomplete="off" class="layui-input" type="text">
												
														</div>
													</div>
													
																	
													</div>
													
													

											</div>	
																					</fieldset>
												<div class="layui-form-item">
												<div class="layui-input-block all-button-center">
													
													<input type='hidden' name='pickup_order_add_type' value='1' />
													<button class="layui-btn nav-submit" lay-submit="" lay-filter="despatchAdd" id="dining_add_button">提交</button>
													<a href="/dispatch/showPickupOrderManage"><button type="button" class="layui-btn layui-btn-primary">返回</button></a>
												</div>
											</div>											
										
										</form>
							    </div>
							    

							 
							    <!-- 整车开始 -->
					    	
					<!-- 整车结束 -->
					<!-- 零担开始 -->

					<!-- 零担结束 -->
					
					

			</div>
			
		</div>




		
		
	</div>


		
			
			
			
	<!-- 模板结束 -->	
	
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>

<script src='/static/javascript/despatch/despatch.js'></script>
<script type="text/javascript">
	FEE_TYPE=<?php echo json_encode($fee); ?>
	
	fee_index=1000;
	project_index=1000;
	
	$(document).on('click',".fee-add",function(){
		
		fee_index++;
		
		var p_name=$(this).attr("data-id");
		var data_index=$(this).attr("data-index");
		
		
		if(p_name == 'zhongzhuan' || p_name == 'tihuo'|| p_name == 'zhengche')
		{
			p_name=p_name;
			
		}
		var dom=$(this).parent();
			
		$(dom).after(`<div class="fee-content">
														
														<div class="fee-subtract" data-id="zhengche">
															-
														</div>
														
														<div class="fee-item" style="">
															
														
														<div class="layui-inline layui-col-md3">
															<label class="layui-form-label " style="width: 100px;">成本:</label>
															<div class=" layui-col-md5">
												  			<select name="fee_id_${p_name}[${fee_index}]" class="fee_select" lay-filter="fee_select">
															<option value="">请选择</option>
															<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
															<option value="<?php echo $vo4['cost_id']; ?>"><?php echo $vo4['cost_name']; ?></option>
															<?php endforeach; endif; else: echo "" ;endif; ?>
																											  			
																											  			</select>
															</div>
														</div><div class="layui-inline ">
															<label class="layui-form-label money">金额:</label>
															<div class="layui-col-md5">
												  				<input  name="money_${p_name}[${fee_index}]" value="" placeholder="" autocomplete="off" class="layui-input money" type="text">
													
															</div>
														</div>	<div class="layui-inline ">
															<label class="layui-form-label ">备注:</label>
															<div class="layui-col-md5">
												  				<input  name="remark_${p_name}[${fee_index}]" value="" placeholder="" autocomplete="off" class="layui-input" type="text">
													
															</div>
														</div>
														
																</div>			
														
															
														</div>`);
														form.render()
	})

	form.on('select(checksupplier)', function(e){
		
		$.ajax({
		url:"/source/getSupplierLineAjax",
		dataType:"json",
		data:{supplier_line_id:e.value},
		success:function(i){
			//console.log(i);
			//$("#v_si_car_code").val(i.data[0].start_site_name);
			$("#v_si_send_phone").val(i.data[0].start_site_phone);
			$("#v_si_arrive_phone").val(i.data[0].end_site_phone);
			$("#v_si_arrive_man").val(i.data[0].end_site_name);
			$("#supplier_id").val(i.data[0].supplier_id);
	
			
		}
		})
		
	})
	
	function updateGoodsInfo(elem){
		var count=0;
		$(".despatch_number").map(function(d,i){
			
			count+=parseInt($(i).val());
			
		})
		$(".despatch_number_all")[0].innerText=count;
		count=0;
		$(".despatch_weight").map(function(d,i){
			
			count+=parseInt($(i).val());
			
		})
		$(".despatch_weight_all")[0].innerText=count;
		count=0;
		$(".despatch_volume").map(function(d,i){
			
			count+=parseInt($(i).val());
			
		})
		$(".despatch_volume_all")[0].innerText=count;
		
		
		
		var parentsVar=$(elem).parents("tr").context?$(elem).parents("tr"):$(elem.target).parents("tr");
		
		postData={};
		postData.supplier_id=$("#supplier_id").val();
		if(!$("#supplier_id").val())
		return alert("请先选择供应商");
		postData.billing_unit=parentsVar.find(".billing_unit  option:selected").val();
		if(postData.billing_unit=="0")
        return alert("请先选择计价单位");		
		postData.status=1;
		postData.departure_id=1;
		postData.arrival_id=1;
		
			//console.log(parentsVar.find(".billing_unit  option:selected"));
		
		switch(postData.billing_unit){
			case "1":postData.interval=parentsVar.find(".despatch_weight").val();break;
			case "2":postData.interval=parentsVar.find(".despatch_volume").val();break;
			case "3":postData.interval=parentsVar.find(".package_number").val();break;
			case "4":postData.interval=parentsVar.find(".despatch_number").val();break;
			default:postData.interval=0;break;
			
			
		}
		
		postData.departure_id=<?php echo $orderResult[0]['send_city_id']; ?>;
		postData.arrival_id=<?php echo $orderResult[0]['accept_city_id']; ?>;
		
		
		//console.log(postData);
		
		$.ajax({
			url:"/source/getPricingConfigureAndPrice",
			data:postData,
			success:function(e){
				if(e.data)
				{
					parentsVar.find(".despatch_unit").val(e.data.unit_price);
					parentsVar.find(".despatch_money").val(e.data.total);
				}
			},
			complete:function(e){
				count=0;
				$(".despatch_money").map(function(d,i){
					
					count+=parseFloat($(i).val());
					
				})
				$(".despatch_money_all")[0].innerText=count.toFixed(3);
				$("#all_money").val(count.toFixed(3));
			}
			
			
		})
		
		
		
		
		
	}
	
	
	$("#goods_info input,select").change(function(e){
		updateGoodsInfo(e)
	})
	
	
	form.on("select(changeBilingUnit)",function(e){
	
		updateGoodsInfo(e.elem)
		
		
	})
	
	form.on("submit(despatchAdd)",function(e){
		console.log(e);
		layer.load()
		$.ajax({
			url:"/despatch/despatchAddAjax",
			data:$("form").serializeArray(),
			success:function(e){
				if(e.code==200)
				{
					layer.msg("");
				
				window.location.href="/dispatch/showPickupOrderManage";
				}
				else
				 layer.msg(e.msg)
			},
			complete:function(e){
				layer.closeAll()
			}
		})
		
	})
	
</script>

</body>
</html>


