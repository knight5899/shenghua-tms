<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:86:"/var/www/html/test_erp/public/../application/index/view/dispatch/pickup_order_add.html";i:1655951488;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>

				新增揽单

	
	</title>
<style type="text/css">
.layui-inline{
margin-right:0px;
}
#pickup_order_lingdan > div{
	border: 1px solid #a9a9a9;
	padding: 7px;
	}
.box-warp{
	margin-bottom: 5px;
	line-height: 35px;
	display: flex;
	align-items: flex-start;
}
   .huowuOpt{
	   display: flex;
	   justify-content: space-around;
	   min-width: 70px;
   }

	.addGoods{
		font-size: 34px;
		color: #7777ff;
		cursor: pointer;
	}
	.delHuowu{
		cursor: pointer;
		font-size: 34px;
		color: #e50000;
	}
	.tihuo-t-01{
		font-size: 17px;
		padding-bottom: 8px;
		display: block;
		color: #383838;
		padding-top: 0px;
	}
.addTihuoSupplier{
	cursor: pointer;
	font-size: 38px;
	padding-left: 5px;
	font-weight: bold;
	color: #525252;
	padding-top: 0;
	line-height: 0;
	padding-bottom: 15px;
}
	.closeTihuolist{
		position: absolute;
		right: 5px;
		top: 0;
		color: #525252;
		border: 1px solid #525252;
		border-radius: 20px;
		font-size: 22px;
		display: flex;
		justify-content: center;
		align-items: center;
		width: 20px;
		height: 20px;
		cursor: pointer;
	}
	.fee-item{
		display: flex;justify-content: flex-start;flex-wrap: nowrap;
		    border-bottom: 1px #eee dashed;
		    margin-top: 6px;
			    padding-top: 8px;
				padding-left: 6px;
				background-color: #fef;
	}
	.fee-item .layui-form-label {
		width: 70px;
	}
	.fee-content{
		display: flex;
	}
	.fee-add{
		    font-size: 50px;
		       width: 90px;
		       text-align: center;
		    color: #f00;
			cursor: pointer;
	}
	.fee-subtract{
		font-size: 50px;
		    width: 90px;
		    text-align: center;
		color: #00f;
		cursor: pointer;
	}
	.shuilv,.zonge{
		pointer-events: none;
	}
	
</style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

	<div class="layui-header">
		    <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
	</div>

	<div class="layui-side layui-bg-black">
		<div class="layui-side-scroll">
			<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
			      <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
		</div>
	</div>

	<div class="layui-body">

		<!-- 内容主体区域 -->
		<div class="content_body">
			<div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>调度管理</a>
					<a >揽单</a>	
					<a><cite>
					
						新增揽单
					
					</cite></a>
				</span>
			</div>
			<br/>
		

			<br/>
			<form class="layui-form layui-form-pane"  >
			<div class="layui-form-item">		
					<table class="layui-table">
					  <colgroup>
					    <col width="100">
					    <col width="190">
					    <col width="190">
					    <col width="120">
					    <col width="120">
					  </colgroup>
					  <thead>
					    <tr>
					      <th>订单号</th>
					      <th>发货地址</th>	
					      <th>收货地址</th>	
					      <th>预计收货时间</th>
					      <th>预计发货时间</th>
					      <th>货物</th>					      
					      <th>数量</th>	
					      <th>包装数量</th>	
					      <th>重量</th>	
					      <th>体积</th>	
					    </tr> 
					  </thead>
					  <tbody>
					  	<?php if(is_array($orderGoodsResult) || $orderGoodsResult instanceof \think\Collection || $orderGoodsResult instanceof \think\Paginator): if( count($orderGoodsResult)==0 ) : echo "" ;else: foreach($orderGoodsResult as $key=>$v): ?>
					    <tr>
					      <td><?php echo $v['orders_number']; ?></td>
					      <td><?php echo $v['send_province_name']; ?><?php echo $v['send_city_name']; ?><?php echo $v['send_area_name']; ?><?php echo $v['send_address']; ?></td>
					      <td><?php echo $v['accept_province_name']; ?><?php echo $v['accept_city_name']; ?><?php echo $v['accept_area_name']; ?><?php echo $v['accept_address']; ?></td>
					      <td><?php if($v['pickup_time'] != ''): ?><?php echo date('Y-m-d',$v['pickup_time']); endif; ?></td>
					      <td><?php if($v['send_time'] != ''): ?><?php echo date('Y-m-d',$v['send_time']); endif; ?></td>
					      <td><?php echo $v['goods_name']; ?></td>
					      <td><?php echo $v['realy_weight']; ?></td>
					      <td><?php echo $v['realy_volume']; ?></td>
					      <td><?php echo $v['realy_count']; ?></td>
					      <td><?php echo $v['realy_pack_count']; ?></td>
					    </tr>
					    <?php endforeach; endif; else: echo "" ;endif; ?>

					  </tbody>
					</table>
			
				    				    							
			</div>	
			</form>
	<hr/>	
	
	<!-- 选择运输类型 -->
							<div class="layui-tab" style="margin: 0 auto;max-width: 1200px;">
							  <ul class="layui-tab-title">
							    <li class="layui-this">整车</li>
							    <li>零担</li>
							
							  </ul>
							  <div class="layui-tab-content">
							    <div class="layui-tab-item layui-show">
										<form class="layui-form layui-form-pane"   id="form_zhengche" onSubmit="return pickupOrderZhengcheAdd()">
												<div class="layui-form-item"  id='pickup_order_zhengche' >	

													<?php if(is_array($orderResult) || $orderResult instanceof \think\Collection || $orderResult instanceof \think\Paginator): $key_zhengche = 0; $__LIST__ = $orderResult;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$or): $mod = ($key_zhengche % 2 );++$key_zhengche;?>		
													<div class="layui-form-item" style="border: 1px solid #bbb; padding: 10px 5px;">
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" >订单号:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
											  				<input id="all_weight" name="orders_number[]" value="<?php echo $or['orders_number']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
										
														</div>
													</div>


													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required">供应商:</label>
														<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
															<select name="supplier_id[]"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
																	<option value=""  >--请选择--</option>
																<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
																	
																	<option value="<?php echo $vo['supplier_id']; ?>"  > <?php echo $vo['supplier_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>
							
										
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" style='width:140px;'>预计提货日期:</label>
														<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
															 <input id='zc_hb_pickup_time_<?php echo $or['orders_id']; ?>'  orders_id="<?php echo $or['orders_id']; ?>" name="pickup_time[]" value="<?php if($or['pickup_time'] != ''): ?><?php echo date('Y-m-d',$or['pickup_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
											
														</div>
													</div>		
														<div class="layui-inline">
														<label class="layui-form-label input-required" style='width:140px;'>预计送货日期:</label>
														<div class="layui-input-inline" style='width:120px;'>
															 <input id='zc_hb_send_time_<?php echo $or['orders_id']; ?>' orders_id="<?php echo $or['orders_id']; ?>"  name="send_time[]" value="<?php if($or['send_time'] != ''): ?><?php echo date('Y-m-d',$or['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
											
														</div>
													</div>	
													<br />	
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required">发货地:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="send_province_id[]" class='province_id'  lay-verify="required" lay-filter="sheng-shi">
																<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>				
													<div class="layui-inline" style='margin-right:0px;'>

														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="send_city_id[]" class='city_id'  lay-verify="required" lay-filter="shi-qu">
																<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>

														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="send_area_id[]" class='area_id'  lay-verify="required" lay-filter="qu">
																<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>		
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" >发货地址:</label>
														<div class="layui-input-inline" style='margin-right:0px;'>
											  				<input id="all_weight" name="send_address[]" value="<?php echo $or['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
										
														</div>
													</div>	
													<br />
							
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required">收货地:</label>
														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="accept_province_id[]" id='send_city_id' class="province_id"  lay-verify="required" lay-filter="sheng-shi">
																<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>				
													<div class="layui-inline" style='margin-right:0px;'>

														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="accept_city_id[]" id='send_city_id' class="city_id"  lay-verify="required" lay-filter="shi-qu">
																<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>

														<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
															<select name="accept_area_id[]" id='send_city_id' class="area_id"  lay-verify="required" lay-filter="qu">
																<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																	<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $or['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select>							
														</div>
													</div>		
													<div class="layui-inline" >
														<label class="layui-form-label input-required">收货地址:</label>
														<div class="layui-input-inline">
											  				<input id="all_weight" name="accept_address[]" value="<?php echo $or['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
												
														</div>
													</div>	
													<div class="layui-inline" >
														<label class="layui-form-label ">备注:</label>
														<div class="layui-input-inline">
											  				<input id="all_weight" name="remark[]" value="<?php echo $or['remark']; ?>"  placeholder="" autocomplete="off" class="layui-input" type="text">
												
														</div>
													</div>
													
													<div  class="fee-content">
													
													<div class="fee-add" data-id="zhengche" data-index="<?php echo $key_zhengche-1; ?>">
														+
													</div>
													
													<div class="fee-item" style="">
														
													
													<div class="layui-inline layui-col-md3" >
														<label class="layui-form-label " style="width: 100px;">成本:</label>
														<div class=" layui-col-md5">
											  			<select name="fee_id_zhengche[<?php echo $key_zhengche-1; ?>][0]" class="fee_select" lay-filter="fee_select">
															<option value="">请选择</option>
															<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
											  				<option value="<?php echo $vo4['fee_id']; ?>"><?php echo $vo4['fee_name']; ?></option>
															<?php endforeach; endif; else: echo "" ;endif; ?>
											  			</select>
														</div>
													</div><div class="layui-inline " >
														<label class="layui-form-label ">金额:</label>
														<div class="layui-col-md5">
											  				<input name="money_zhengche[<?php echo $key_zhengche-1; ?>][0]" value="<?php echo $or['money']; ?>"  placeholder="" autocomplete="off" class="layui-input money" type="text">
												
														</div>
													</div>	<div class="layui-inline " >
														<label class="layui-form-label " style="width: 70px;">税率:</label>
														<div class="layui-col-md5">
											  				<input  name="tax_rate_zhengche[<?php echo $key_zhengche-1; ?>][0]" value="<?php echo $or['tax_rate']; ?>"  placeholder="" autocomplete="off" class="layui-input shuilv" type="text">
												
														</div>
													</div>
													<div class="layui-inline " >
														<label class="layui-form-label ">总额:</label>
														<div class="layui-col-md5">
											  				<input  name="true_money_zhengche[<?php echo $key_zhengche-1; ?>][0]" value="<?php echo $or['true_money']; ?>"  placeholder="" autocomplete="off" class="layui-input zonge" type="text">
												
														</div>
													</div><div class="layui-inline " >
														<label class="layui-form-label ">备注:</label>
														<div class="layui-col-md5">
											  				<input  name="remark_zhengche[<?php echo $key_zhengche-1; ?>][0]" value=""  placeholder="" autocomplete="off" class="layui-input remark" type="text">
												
														</div>
													</div>
													
															</div>			
													
														
													</div>					
													</div>
													
													<?php endforeach; endif; else: echo "" ;endif; ?>

											</div>											
												<div class="layui-form-item">
												<div class="layui-input-block all-button-center">
													<input type="hidden" name="orders_id" id="" value="<?php echo \think\Request::instance()->get('orders_id'); ?>" />
													<input type='hidden' name='pickup_order_add_type' value='1' />
													<button class="layui-btn nav-submit" lay-submit="" lay-filter="orderAdd" id="dining_add_button">提交</button>
													<a href="/dispatch/showPickupOrderManage"><button type="button" class="layui-btn layui-btn-primary">返回</button></a>
												</div>
											</div>											
										
										</form>
							    </div>
							    <div class="layui-tab-item">
							    	<form class="layui-form layui-form-pane"   id="form1" onSubmit="return Add()">
										<div class="layui-inline box-warp">
											<label for="">是否中转</label>
											<input type="checkbox"  name="openzhongzhuan" class="layui-input" id="openzhongzhuan" lay-filter="switchTest" lay-skin="switch" lay-text="开启|关闭">
										</div>



											<div class="layui-form-item"  id='pickup_order_lingdan' >

												
                                                <br>
												<label class="tihuo-t-01" >提货:</label>
												<div class="layui-form-item"  id='tihuo' '>						
													<div class="layui-inline" style='margin-right:0px;'>
</div>
</br>
													<div class="layui-form-item" id='tihuo_info'  style="margin-bottom: 0">
														<div class="layui-form-item" style="margin-bottom: 0">
																
													
																	<div class="layui-inline" style='margin-right:0px;'>
																	<label class="layui-form-label input-required">供应商:</label>
																	<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
																		<select name="tihuo_supplier[]"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
																				<option value=""  >--请选择--</option>
																			<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
																				
																				<option value="<?php echo $vo['supplier_id']; ?>"  > <?php echo $vo['supplier_name']; ?></option>
																			<?php endforeach; endif; else: echo "" ;endif; ?>
																		</select>							
																	</div>
																</div>	
																<div class="layui-inline" style='margin-right:0px;'>
																	<label class="layui-form-label input-required" style='width:140px;'>预计提货时间:</label>
																	<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
																		 <input   orders_id="<?php echo $vv['orders_id']; ?>" name="tihuo_pickup_date[]" value="<?php if($vv['pickup_time'] != ''): ?><?php echo date('Y-m-d',$vv['pickup_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
														
																	</div>
																</div>		
																	<div class="layui-inline">
																	<label class="layui-form-label input-required" style='width:140px;'>预计送货时间:</label>
																	<div class="layui-input-inline" style='width:120px;'>
																		 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="tihuo_send_date[]" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
														
																	</div>
																</div>	<div class="layui-inline">
																	<label class="layui-form-label " style='width:140px;'>备注:</label>
																	<div class="layui-input-inline" style='width:120px;'>
																		 <input  name="tihuo_remark[]" value=""  lay-verify="" placeholder="" autocomplete="off" class="layui-input " type="text">
														
																	</div>
																</div>															
																	<br />			
															<div class='tihuo_supplier'>							
																	<div class="huowu" >
																				<div class="layui-inline" style='margin-right:0px;'>
																					<label class="layui-form-label input-required">货物:</label>
																						<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
																					
																						<select name="tihuo_huowu_name[0][0]" class="layui-input"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
																								<option value=""  >--请选择--</option>
																							<?php if(is_array($orderGoodsResult) || $orderGoodsResult instanceof \think\Collection || $orderGoodsResult instanceof \think\Paginator): if( count($orderGoodsResult)==0 ) : echo "" ;else: foreach($orderGoodsResult as $key=>$vo): ?>
																								
																								<option value="<?php echo $vo['orders_id']; ?>-<?php echo $vo['goods_id']; ?>"  > <?php echo $vo['goods_name']; ?></option>
																							<?php endforeach; endif; else: echo "" ;endif; ?>
																						</select>	
																				</div>
																			</div>			
																			<div class="layui-inline" style='margin-right:0px;'>
																				<label class="layui-form-label input-required" style='width:80px' >数量:</label>
																				<div class="layui-input-inline" style='width:80px;margin-right:0px;'>
																					 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="tihuo_huowu_number[0][0]" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="text">
																	
																				</div>
																			</div>	
																			<div class="layui-inline" style='margin-right:0px;'>
																				<label class="layui-form-label input-required" >包装数量:</label>
																				<div class="layui-input-inline" style='width:80px;margin-right:0px;'>
																					 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="tihuo_huowu_pack_number[0][0]" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="text">
																	
																				</div>
																			</div>	
																			<div class="layui-inline" style='margin-right:0px;'>
																				<label class="layui-form-label input-required" style='width:80px;'>重量:</label>
																				<div class="layui-input-inline" style='width:100px;margin-right:0px;'>
																					 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="tihuo_huowu_weight[0][0]" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="text">
																	
																				</div>
																			</div>	
																			<div class="layui-inline" style='margin-right:0px;'>
																				<label class="layui-form-label input-required" style='width:80px;'>体积:</label>
																				<div class="layui-input-inline" style='width:100px;'>
																					 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="tihuo_huowu_volume[0][0]" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="text">
																	
																				</div>
																			</div>	
																			<div class="more-box layui-inline addGoods">	+	</div>
																			
																			
							
																</div>
															
															
															</div>


															<div class="layui-inline" style='margin-right:0px;'>
																<label class="layui-form-label input-required">发货地:</label>
																<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																	<select name="tihuo_send_province_id[]" id='send_city_id' class="province_id"  lay-verify="required" lay-filter="sheng-shi">
																		<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																			<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																		<?php endforeach; endif; else: echo "" ;endif; ?>
																	</select>							
																</div>
															</div>				
															<div class="layui-inline" style='margin-right:0px;'>

																<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																	<select name="tihuo_send_city_id[]" id='send_city_id' class="city_id"  lay-verify="required" lay-filter="shi-qu">
																		<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																			<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																		<?php endforeach; endif; else: echo "" ;endif; ?>
																	</select>							
																</div>
															</div>	
															<div class="layui-inline" style='margin-right:0px;'>

																<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																	<select name="tihuo_send_area_id[]" id='send_city_id' class="area_id" lay-verify="required" lay-filter="qu">
																		<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																			<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																		<?php endforeach; endif; else: echo "" ;endif; ?>
																	</select>							
																</div>
															</div>		
															<div class="layui-inline" style='margin-right:0px;'>
																<label class="layui-form-label input-required" >发货地址:</label>
																<div class="layui-input-inline" style='margin-right:0px;'>
													  				<input id="all_weight" name="tihuo_send_address[]" value="<?php echo $vv['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
												
																</div>
															</div>		
																						
														
															
														</div>
															
													</div>											
						
													<div class='layui-form-item'>
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">收货地:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="tihuo_accept_province_id[]" id='send_city_id' class="province_id" lay-verify="required" lay-filter="sheng-shi">
																	<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>				
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="tihuo_accept_city_id[]" id='send_city_id' class="city_id"  lay-verify="required" lay-filter="shi-qu">
																	<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>	
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="tihuo_accept_area_id[]" id='send_city_id' class="area_id" lay-verify="required" lay-filter="qu">
																	<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>		
														<div class="layui-inline" >
															<label class="layui-form-label input-required">收货地址:</label>
															<div class="layui-input-inline">
												  				<input id="all_weight" name="tihuo_accept_address[]" value="<?php echo $vv['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
													
															</div>
														</div>
															<div  class="fee-content">
															
															<div class="fee-add" data-id="tihuo"  data-index="0">
																+
															</div>
															
															<div class="fee-item" style="">
																
															
															<div class="layui-inline layui-col-md3" >
																<label class="layui-form-label " style="width: 100px;">成本:</label>
																<div class=" layui-col-md5">
																<select name="fee_id_tihuo[0][0]" class="fee_select" lay-filter="fee_select">
																	<option value="">请选择</option>
																	<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
																	<option value="<?php echo $vo4['fee_id']; ?>"><?php echo $vo4['fee_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>
																</div>
															</div><div class="layui-inline " >
																<label class="layui-form-label ">金额:</label>
																<div class="layui-col-md5">
																	<input name="money_tihuo[0][0]" value="<?php echo $or['money']; ?>"  placeholder="" autocomplete="off" class="layui-input money" type="text">
																											
																</div>
															</div>	<div class="layui-inline " >
																<label class="layui-form-label " style="width: 70px;">税率:</label>
																<div class="layui-col-md5">
																	<input  name="tax_rate_tihuo[0][0]" value="<?php echo $or['tax_rate']; ?>"  placeholder="" autocomplete="off" class="layui-input shuilv" type="text">
																											
																</div>
															</div>
															<div class="layui-inline " >
																<label class="layui-form-label ">总额:</label>
																<div class="layui-col-md5">
																	<input  name="true_money_tihuo[0][0]" value="<?php echo $or['true_money']; ?>"  placeholder="" autocomplete="off" class="layui-input zonge" type="text">
																											
																</div>
															</div><div class="layui-inline " >
																<label class="layui-form-label ">备注:</label>
																<div class="layui-col-md5">
																	<input  name="remark_tihuo[0][0]" value=""  placeholder="" autocomplete="off" class="layui-input remark" type="text">
																											
																</div>
															</div>
															
																	</div>			
															
																
															</div>
													</div>
												<hr />
												<div class="layui-inline addTihuoSupplier" style='margin-right:0px;' >
													...

												</div>
						
												
																		
						
											</div>
											<hr />
										<label class="tihuo-t-01" >干线:</label>
											<div class='layui-form-item' id='zhongzhuan' >
															<div class="layui-inline" style='margin-right:0px;'>

						
														</div>
														<br />
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">订单号:</label>
															<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
										  							<input id="all_weight" name="ganxian_order_ids" value="<?php if(is_array($orderResult) || $orderResult instanceof \think\Collection || $orderResult instanceof \think\Paginator): $i = 0; $__LIST__ = $orderResult;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($i % 2 );++$i;if($key==0): ?><?php echo $vv['orders_number']; else: ?>,<?php echo $vv['orders_number']; endif; endforeach; endif; else: echo "" ;endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
											
															</div>
														</div>
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">供应商:</label>
															<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
																<select name="ganxian_supplier"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
																		<option value=""  >--请选择--</option>
																	<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
																		
																		<option value="<?php echo $vo['supplier_id']; ?>"  > <?php echo $vo['supplier_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>
								
											
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required" style='width:140px;'>预计提货时间:</label>
															<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
																 <input   orders_id="<?php echo $vv['orders_id']; ?>" name="ganxian_pickup_date" value="<?php if($vv['pickup_time'] != ''): ?><?php echo date('Y-m-d',$vv['pickup_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time " type="text">
												
															</div>
														</div>		
															<div class="layui-inline">
															<label class="layui-form-label input-required" style='width:140px;'>预计送货时间:</label>
															<div class="layui-input-inline" style='width:120px;'>
																 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="ganxian_send_date" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input  pickup_time" type="text">
												
															</div>
														</div>	<div class="layui-inline">
															<label class="layui-form-label " style='width:140px;'>备注:</label>
															<div class="layui-input-inline" style='width:120px;'>
																 <input   name="ganxian_remark" value=""  lay-verify="" placeholder="" autocomplete="off" class="layui-input  " type="text">
												
															</div>
														</div>	
														<br />	
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">发货地:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="ganxian_send_province_id" id='send_city_id'  lay-verify="required" class="province_id" lay-filter="sheng-shi">
																	<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>				
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="ganxian_send_city_id" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
																	<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>	
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="ganxian_send_area_id" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
																	<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>		
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required" >发货地址:</label>
															<div class="layui-input-inline" style='margin-right:0px;'>
												  				<input id="all_weight" name="ganxian_send_address" value="<?php echo $vv['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
											
															</div>
														</div>	<br>
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">收货地:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="ganxian_accept_province_id" id='send_city_id' class="city_id" lay-verify="required" lay-filter="sheng-shi">
																	<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>				
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="ganxian_accept_city_id" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
																	<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>	
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="ganxian_accept_area_id" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
																	<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>		
														<div class="layui-inline" >
															<label class="layui-form-label input-required">收货地址:</label>
															<div class="layui-input-inline">
												  				<input id="all_weight" name="ganxian_accept_address" value="<?php echo $vv['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
													
															</div>
														</div>								
											
											<div  class="fee-content">
											
											<div class="fee-add" data-id="ganxian">
												+
											</div>
											
											<div class="fee-item" style="">
												
											
											<div class="layui-inline layui-col-md3" >
												<label class="layui-form-label " style="width: 100px;">成本:</label>
												<div class=" layui-col-md5">
												<select name="fee_id_ganxian[0]" class="fee_select" lay-filter="fee_select">
													<option value="">请选择</option>
													<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
													<option value="<?php echo $vo4['fee_id']; ?>"><?php echo $vo4['fee_name']; ?></option>
													<?php endforeach; endif; else: echo "" ;endif; ?>
												</select>
												</div>
											</div><div class="layui-inline " >
												<label class="layui-form-label ">金额:</label>
												<div class="layui-col-md5">
													<input name="money_ganxian[0]" value="<?php echo $or['money']; ?>"  placeholder="" autocomplete="off" class="layui-input money" type="text">
																							
												</div>
											</div>	<div class="layui-inline " >
												<label class="layui-form-label " style="width: 70px;">税率:</label>
												<div class="layui-col-md5">
													<input  name="tax_rate_ganxian[0]" value="<?php echo $or['tax_rate']; ?>"  placeholder="" autocomplete="off" class="layui-input shuilv" type="text">
																							
												</div>
											</div>
											<div class="layui-inline " >
												<label class="layui-form-label ">总额:</label>
												<div class="layui-col-md5">
													<input  name="true_money_ganxian[0]" value="<?php echo $or['true_money']; ?>"  placeholder="" autocomplete="off" class="layui-input zonge" type="text">
																							
												</div>
											</div><div class="layui-inline " >
												<label class="layui-form-label ">备注:</label>
												<div class="layui-col-md5">
													<input  name="remark_ganxian[0]" value=""  placeholder="" autocomplete="off" class="layui-input remark" type="text">
																							
												</div>
											</div>
											
													</div>			
											
												
											</div>
											
											 </div>
											 <hr />
										<label class="tihuo-t-01" >配送:</label>
											 <div id='peisong'>
													<div class="layui-inline" style='margin-right:0px;'>

						
														</div>

												
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">订单号:</label>
															<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
										  							<input id="all_weight" name="peisong_order_ids" value="<?php if(is_array($orderResult) || $orderResult instanceof \think\Collection || $orderResult instanceof \think\Paginator): $i = 0; $__LIST__ = $orderResult;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($i % 2 );++$i;if($key==0): ?><?php echo $vv['orders_number']; else: ?>,<?php echo $vv['orders_number']; endif; endforeach; endif; else: echo "" ;endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
											
															</div>
														</div>
													
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">供应商:</label>
															<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
																<select name="peisong_supplier"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
																		<option value=""  >--请选择--</option>
																	<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
																		
																		<option value="<?php echo $vo['supplier_id']; ?>"  > <?php echo $vo['supplier_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>
								
											
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required" style='width:140px;'>预计提货时间:</label>
															<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
																 <input   orders_id="<?php echo $vv['orders_id']; ?>" name="peisong_pickup_date" value="<?php if($vv['pickup_time'] != ''): ?><?php echo date('Y-m-d',$vv['pickup_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time " type="text">
												
															</div>
														</div>		
															<div class="layui-inline">
															<label class="layui-form-label input-required" style='width:140px;'>预计送货时间:</label>
															<div class="layui-input-inline" style='width:120px;'>
																 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="peisong_send_date" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time " type="text">
												
															</div>
														</div>	<div class="layui-inline">
															<label class="layui-form-label " style='width:140px;'>备注:</label>
															<div class="layui-input-inline" style='width:120px;'>
																 <input   name="peisong_remark" value=""   placeholder="" autocomplete="off" class="layui-input  " type="text">
												
															</div>
														</div>	
														
														
														
														<br />	
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">发货地:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="peisong_send_province_id" id='send_city_id'  lay-verify="required" class="province_id" lay-filter="sheng-shi">
																	<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>				
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="peisong_send_city_id" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
																	<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>	
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="peisong_send_area_id" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
																	<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>		
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required" >发货地址:</label>
															<div class="layui-input-inline" style='margin-right:0px;'>
												  				<input id="all_weight" name="peisong_send_address" value="<?php echo $vv['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
											
															</div>
														</div>		<br>
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">收货地:</label>
															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="peisong_accept_province_id" id='send_city_id' class="province_id" lay-verify="required" lay-filter="sheng-shi">
																	<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>				
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="peisong_accept_city_id" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
																	<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>	
														<div class="layui-inline" style='margin-right:0px;'>

															<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
																<select name="peisong_accept_area_id" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
																	<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
																		<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>							
															</div>
														</div>		
														<div class="layui-inline" >
															<label class="layui-form-label input-required">收货地址:</label>
															<div class="layui-input-inline">
												  				<input id="all_weight" name="peisong_accept_address" value="<?php echo $vv['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
													
															</div>
														</div>									
												 <div  class="fee-content">
												 
												 <div class="fee-add" data-id="ganxian">
												 	+
												 </div>
												 
												 <div class="fee-item" style="">
												 	
												 
												 <div class="layui-inline layui-col-md3" >
												 	<label class="layui-form-label " style="width: 100px;">成本:</label>
												 	<div class=" layui-col-md5">
												 	<select name="fee_id_peisong[0]" class="fee_select" lay-filter="fee_select">
												 		<option value="">请选择</option>
												 		<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
												 		<option value="<?php echo $vo4['fee_id']; ?>"><?php echo $vo4['fee_name']; ?></option>
												 		<?php endforeach; endif; else: echo "" ;endif; ?>
												 	</select>
												 	</div>
												 </div><div class="layui-inline " >
												 	<label class="layui-form-label ">金额:</label>
												 	<div class="layui-col-md5">
												 		<input name="money_peisong[0]" value="<?php echo $or['money']; ?>"  placeholder="" autocomplete="off" class="layui-input money" type="text">
												 												
												 	</div>
												 </div>	<div class="layui-inline " >
												 	<label class="layui-form-label " style="width: 70px;">税率:</label>
												 	<div class="layui-col-md5">
												 		<input  name="tax_rate_peisong[0]" value="<?php echo $or['tax_rate']; ?>"  placeholder="" autocomplete="off" class="layui-input shuilv" type="text">
												 												
												 	</div>
												 </div>
												 <div class="layui-inline " >
												 	<label class="layui-form-label ">总额:</label>
												 	<div class="layui-col-md5">
												 		<input  name="true_money_peisong[0]" value="<?php echo $or['true_money']; ?>"  placeholder="" autocomplete="off" class="layui-input zonge" type="text">
												 												
												 	</div>
												 </div><div class="layui-inline " >
												 	<label class="layui-form-label ">备注:</label>
												 	<div class="layui-col-md5">
												 		<input  name="remark_peisong[0]" value=""  placeholder="" autocomplete="off" class="layui-input remark" type="text">
												 												
												 	</div>
												 </div>
												 
												 		</div>			
												 
												 	
												 </div>
											 
											 </div>					
											
											</div>		
												    	
												<div class="layui-form-item">
												<div class="layui-input-block all-button-center">
													<input type="hidden" name="orders_id" id="" value="<?php echo \think\Request::instance()->get('orders_id'); ?>" />
													<button class="layui-btn nav-submit" lay-submit="" lay-filter="orderAdd" id="dining_add_button">提交</button>
													<a href="/dispatch/showPickupOrderManage"><button type="button" class="layui-btn layui-btn-primary">返回</button></a>
												</div>
											</div>								    	
							    	</form>
							    
							    
							    </div>
							
							  </div>
							</div>

							    <!-- 
							    <div class="layui-input-inline" id='lingdan_button'  style="display:none">
							    			<label class="layui-form-label input-required">运输模式:</label>
							    			<div class="layui-input-inline " >
												<select name="zhengche_supplier"    lay-filter="peisong_type"  lay-search>
													<option value=""  >-请选择--</option>
											
													
													<option value="1"  >门-门</option>
													<option value="2"  >门-站</option>
													<option value="3"  >站-门</option>
													<option value="4"  >站-站</option>
											</select>		
											</div>
							    </div>	
							    --> 
							    <!-- 整车开始 -->
					    	
					<!-- 整车结束 -->
					<!-- 零担开始 -->

					<!-- 零担结束 -->
					
					

			</div>
			
		</div>




		
		
	</div>

	<div class="layui-footer">
		<!-- 底部固定区域 -->
		© layui.com - 底部固定区域

	<!-- 模板开始 -->	
		<div id='zhengche_html' style='display:none'>
						<div class="layui-inline" style='margin-right:0px;'>
							<label class="layui-form-label input-required">供应商:</label>
							<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
								<select name="zhengche_supplier"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
										<option value=""  >--请选择--</option>
									<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
										
										<option value="<?php echo $vo['supplier_id']; ?>"  > <?php echo $vo['supplier_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>

			
						<div class="layui-inline" style='margin-right:0px;'>
							<label class="layui-form-label input-required" style='width:140px;'>预计提货时间:</label>
							<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
								 <input id='zc_hb_pickup_time_<?php echo $vv['orders_id']; ?>'  orders_id="<?php echo $vv['orders_id']; ?>" name="send_cellphone" value="<?php if($vv['pickup_time'] != ''): ?><?php echo date('Y-m-d',$vv['pickup_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input zc_hb_pickup_time" type="text">
				
							</div>
						</div>		
							<div class="layui-inline">
							<label class="layui-form-label input-required" style='width:140px;'>预计送货时间:</label>
							<div class="layui-input-inline" style='width:120px;'>
								 <input id='zc_hb_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="send_cellphone" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input zc_hb_send_time" type="text">
				
							</div>
						</div>	
						<br />	
						<div class="layui-inline" style='margin-right:0px;'>
							<label class="layui-form-label input-required">发货地:</label>
							<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
								<select name="send_city_id" id='send_city_id'  lay-verify="required" class="province_id" lay-filter="sheng-shi">
									<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>				
						<div class="layui-inline" style='margin-right:0px;'>

							<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
								<select name="send_city_id" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
									<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>	
						<div class="layui-inline" style='margin-right:0px;'>

							<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
								<select name="send_city_id" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
									<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>		
						<div class="layui-inline" style='margin-right:0px;'>
							<label class="layui-form-label input-required" >发货地址:</label>
							<div class="layui-input-inline" style='margin-right:0px;'>
				  				<input id="all_weight" name="all_weight" value="<?php echo $vv['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
			
							</div>
						</div>	
						<br />

						<div class="layui-inline" style='margin-right:0px;'>
							<label class="layui-form-label input-required">收货地:</label>
							<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
								<select name="send_city_id" id='send_city_id'  lay-verify="required" class="province_id" lay-filter="sheng-shi">
									<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>				
						<div class="layui-inline" style='margin-right:0px;'>

							<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
								<select name="send_city_id" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
									<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>	
						<div class="layui-inline" style='margin-right:0px;'>

							<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
								<select name="send_city_id" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
									<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>		
						<div class="layui-inline" >
							<label class="layui-form-label input-required">收货地址:</label>
							<div class="layui-input-inline">
				  				<input id="all_weight" name="all_weight" value="<?php echo $vv['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
					
							</div>
						</div>				
		</div>
		
		
		
		
		<div id='huowu' style='display:none'>
			<div class="huowu" >
													<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">货物:</label>
																<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
															
																<select name="_huowu_name"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
																		<option value=""  >--请选择--</option>
																	<?php if(is_array($orderGoodsResult) || $orderGoodsResult instanceof \think\Collection || $orderGoodsResult instanceof \think\Paginator): if( count($orderGoodsResult)==0 ) : echo "" ;else: foreach($orderGoodsResult as $key=>$vo): ?>
																		
																		<option value="<?php echo $vo['orders_id']; ?>-<?php echo $vo['goods_id']; ?>"  > <?php echo $vo['goods_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>	
														</div>
													</div>			
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" style='width:80px' >数量:</label>
														<div class="layui-input-inline" style='width:80px;margin-right:0px;'>
															 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_huowu_number" value=""  lay-verify="required|number" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="number">
											
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" >包装数量:</label>
														<div class="layui-input-inline" style='width:80px;margin-right:0px;'>
															 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_huowu_pack_number" value=""  lay-verify="required|number" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="number">
											
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" style='width:80px;'>重量:</label>
														<div class="layui-input-inline" style='width:100px;margin-right:0px;'>
															 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_huowu_weight" value=""  lay-verify="required|number" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="number">
											
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" style='width:80px;'>体积:</label>
														<div class="layui-input-inline" style='width:100px;'>
															 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_huowu_volume" value=""  lay-verify="required|number" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="number">
											
														</div>
													</div>	
													<div class="layui-inline">
														<div class="huowuOpt">


														<div class=" delHuowu" style='margin-right:0px;'>-</div>
														<div class="addGoods">+</div>
														</div>
													</div>

					</div>	
			</div>		
		
		<div id="zhongzhuan-start-model" style="display:none">
		<div class="layui-form-item zhongzhuan-start" id="zhongzhuan-start">
		
		
		
			<div class="layui-inline" style='margin-right:0px;' id='addTihuoSupplier'>
		
		
			</div>
			<br />
			<div class="layui-form-item" id='tihuo_info' style="margin-bottom: 0" >
				<div class="layui-form-item" style="margin-bottom: 0">
		
		
					<div class="layui-inline" style='margin-right:0px;'>
						<label class="layui-form-label input-required" style=" width: max-content;">供应商:</label>
						<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
							<select name="zhongzhuan_supplier[]"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
								<option value=""  >--请选择--</option>
								<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
		
								<option value="<?php echo $vo['supplier_id']; ?>"  > <?php echo $vo['supplier_name']; ?></option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
						</div>
					</div>
					<div class="layui-inline" style='margin-right:0px;'>
						<label class="layui-form-label input-required" style='width:140px;'>预计提货时间:</label>
						<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
							<input   orders_id="<?php echo $vv['orders_id']; ?>" name="zhongzhuan_pickup_date[]" value="<?php if($vv['pickup_time'] != ''): ?><?php echo date('Y-m-d',$vv['pickup_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
		
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label input-required" style='width:140px;'>预计送货时间:</label>
						<div class="layui-input-inline" style='width:120px;'>
							<input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="zhongzhuan_send_date[]" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
		
						</div>
					</div>
					
					<div class="layui-inline">
						<label class="layui-form-label " style='width:140px;'>备注:</label>
						<div class="layui-input-inline" style='width:120px;'>
							<input  name="zhongzhuan_remark[]" value=""  lay-verify="" placeholder="" autocomplete="off" class="layui-input" type="text">
		
						</div>
					</div>
		
		
					<br />
					<div class='tihuo_supplier'>
						<div class="huowu" >
							<div class="layui-inline" style='margin-right:0px;'>
								<label class="layui-form-label input-required">货物:</label>
								<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
		
									<select name="zhongzhuan_huowu_name[0][0]"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
										<option value=""  >--请选择--</option>
										<?php if(is_array($orderGoodsResult) || $orderGoodsResult instanceof \think\Collection || $orderGoodsResult instanceof \think\Paginator): if( count($orderGoodsResult)==0 ) : echo "" ;else: foreach($orderGoodsResult as $key=>$vo): ?>
		
										<option value="<?php echo $vo['orders_id']; ?>-<?php echo $vo['goods_id']; ?>"  > <?php echo $vo['goods_name']; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
									</select>
								</div>
							</div>
							<div class="layui-inline" style='margin-right:0px;'>
								<label class="layui-form-label input-required" style='width:80px' >数量:</label>
								<div class="layui-input-inline" style='width:80px;margin-right:0px;'>
									<input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="zhongzhuan_huowu_number[0][0]" value=""  lay-verify="required|number" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="number">
		
								</div>
							</div>
							<div class="layui-inline" style='margin-right:0px;'>
								<label class="layui-form-label input-required" >包装数量:</label>
								<div class="layui-input-inline" style='width:80px;margin-right:0px;'>
									<input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="zhongzhuan_huowu_pack_number[0][0]" value=""  lay-verify="required|number" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="number">
		
								</div>
							</div>
							<div class="layui-inline" style='margin-right:0px;'>
								<label class="layui-form-label input-required" style='width:80px;'>重量:</label>
								<div class="layui-input-inline" style='width:100px;margin-right:0px;'>
									<input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="zhongzhuan_huowu_weight[0][0]" value=""  lay-verify="required|number" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="number">
		
								</div>
							</div>
							<div class="layui-inline" style='margin-right:0px;'>
								<label class="layui-form-label input-required" style='width:80px;'>体积:</label>
								<div class="layui-input-inline" style='width:100px;'>
									<input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="zhongzhuan_huowu_volume[0][0]" value=""  lay-verify="required|number" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="number">
		
								</div>
							</div>
		
							<div class="more-box layui-inline addGoods">	+	</div>
		
						</div>
		
		
					</div>
		
					<br />
					<div class="layui-inline" style='margin-right:0px;'>
						<label class="layui-form-label input-required">发货地:</label>
						<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
							<select name="zhongzhuan_send_province_id[]" id='send_city_id'  lay-verify="required" class="province_id" lay-filter="sheng-shi">
								<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
								<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
						</div>
					</div>
					<div class="layui-inline" style='margin-right:0px;'>
		
						<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
							<select name="zhongzhuan_send_city_id[]" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
								<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
								<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
						</div>
					</div>
					<div class="layui-inline" style='margin-right:0px;'>
		
						<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
							<select name="zhongzhuan_send_area_id[]" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
								<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
								<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
						</div>
					</div>
					<div class="layui-inline" style='margin-right:0px;'>
						<label class="layui-form-label input-required" >发货地址:</label>
						<div class="layui-input-inline" style='margin-right:0px;'>
							<input id="all_weight" name="zhongzhuan_send_address[]" value="<?php echo $vv['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
		
						</div>
					</div>
		
		
		
				</div>
		
			</div>
		
			<div class='layui-form-item'>
				<div class="layui-inline" style='margin-right:0px;'>
					<label class="layui-form-label input-required">收货地:</label>
					<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
						<select name="zhongzhuan_accept_province_id[]" id='send_city_id'  lay-verify="required" class="province_id" lay-filter="sheng-shi">
							<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
							<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
							<?php endforeach; endif; else: echo "" ;endif; ?>
						</select>
					</div>
				</div>
				<div class="layui-inline" style='margin-right:0px;'>
		
					<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
						<select name="zhongzhuan_accept_city_id[]" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
							<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
							<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
							<?php endforeach; endif; else: echo "" ;endif; ?>
						</select>
					</div>
				</div>
				<div class="layui-inline" style='margin-right:0px;'>
		
					<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
						<select name="zhongzhuan_accept_area_id[]" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
							<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
							<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
							<?php endforeach; endif; else: echo "" ;endif; ?>
						</select>
					</div>
				</div>
				<div class="layui-inline" >
					<label class="layui-form-label input-required">收货地址:</label>
					<div class="layui-input-inline">
						<input id="all_weight" name="zhongzhuan_accept_address[]" value="<?php echo $vv['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
		
					</div>
				</div>
			<div  class="fee-content">
			
			<div class="fee-add" data-id="zhongzhuan" data-index="0">
				+
			</div>
			
			<div class="fee-item" style="">
				
			
			<div class="layui-inline layui-col-md3" >
				<label class="layui-form-label " style="width: 100px;">成本:</label>
				<div class=" layui-col-md5">
				<select name="fee_id_zhongzhuan[0][0]" class="fee_select" lay-filter="fee_select">
					<option value="">请选择</option>
					<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
					<option value="<?php echo $vo4['fee_id']; ?>"><?php echo $vo4['fee_name']; ?></option>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</select>
				</div>
			</div><div class="layui-inline " >
				<label class="layui-form-label ">金额:</label>
				<div class="layui-col-md5">
					<input name="money_zhongzhuan[0][0]" value="<?php echo $or['money']; ?>"  placeholder="" autocomplete="off" class="layui-input money" type="text">
															
				</div>
			</div>	<div class="layui-inline " >
				<label class="layui-form-label " style="width: 70px;">税率:</label>
				<div class="layui-col-md5">
					<input  name="tax_rate_zhongzhuan[0][0]" value="<?php echo $or['tax_rate']; ?>"  placeholder="" autocomplete="off" class="layui-input shuilv" type="text">
															
				</div>
			</div>
			<div class="layui-inline " >
				<label class="layui-form-label ">总额:</label>
				<div class="layui-col-md5">
					<input  name="true_money_zhongzhuan[0][0]" value="<?php echo $or['true_money']; ?>"  placeholder="" autocomplete="off" class="layui-input zonge" type="text">
															
				</div>
			</div><div class="layui-inline " >
				<label class="layui-form-label ">备注:</label>
				<div class="layui-col-md5">
					<input  name="remark_zhongzhuan[0][0]" value=""  placeholder="" autocomplete="off" class="layui-input remark" type="text">
															
				</div>
			</div>
			
					</div>			
			
				
			</div>	
				
			</div>
		    <hr>
			<div class="layui-inline addTihuoSupplier" style="margin-right:0px;">
				...
		
			</div>
		
		
		
		</div>
		</div>
		
			<div id='addTihuoSupplierHtml' style='display:none;'>
				<div style="position: relative" class="tihuosupplierlist">
				                <div class="closeTihuolist">x</div>
								<div class="layui-form-item">
										
							
											<div class="layui-inline" style='margin-right:0px;'>
											<label class="layui-form-label input-required">供应商:</label>
											<div class="layui-input-inline"  style='width:140px;margin-right:0px;'>
												<select name="_supplier"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
														<option value=""  >--请选择--</option>
													<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
														
														<option value="<?php echo $vo['supplier_id']; ?>"  > <?php echo $vo['supplier_name']; ?></option>
													<?php endforeach; endif; else: echo "" ;endif; ?>
												</select>							
											</div>
										</div>	
										<div class="layui-inline" style='margin-right:0px;'>
											<label class="layui-form-label input-required" style='width:140px;'>预计提货时间:</label>
											<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
												 <input   orders_id="<?php echo $vv['orders_id']; ?>" name="_pickup_date" value="<?php if($vv['pickup_time'] != ''): ?><?php echo date('Y-m-d',$vv['pickup_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
								
											</div>
										</div>		
											<div class="layui-inline">
											<label class="layui-form-label input-required" style='width:140px;'>预计送货时间:</label>
											<div class="layui-input-inline" style='width:120px;'>
												 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_send_date" value="<?php if($vv['send_time'] != ''): ?><?php echo date('Y-m-d',$vv['send_time']); endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input pickup_time" type="text">
								
											</div>
										</div>															
											<br />			
									<div class='tihuo_supplier'>							
											<div class="huowu" >
														<div class="layui-inline" style='margin-right:0px;'>
															<label class="layui-form-label input-required">货物:</label>
																<div class="layui-input-inline" style='width:120px;margin-right:0px;'>
															
																<select name="_huowu_name"  lay-verify="required"   lay-filter="orderProvince"  lay-search>
																		<option value=""  >--请选择--</option>
																	<?php if(is_array($orderGoodsResult) || $orderGoodsResult instanceof \think\Collection || $orderGoodsResult instanceof \think\Paginator): if( count($orderGoodsResult)==0 ) : echo "" ;else: foreach($orderGoodsResult as $key=>$vo): ?>
																		
																		<option value="<?php echo $vo['orders_id']; ?>-<?php echo $vo['goods_id']; ?>"  > <?php echo $vo['goods_name']; ?></option>
																	<?php endforeach; endif; else: echo "" ;endif; ?>
																</select>	
														</div>
													</div>			
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" style='width:80px' >数量:</label>
														<div class="layui-input-inline" style='width:80px;margin-right:0px;'>
															 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_huowu_number" value=""  lay-verify="required" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="text">
											
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" >包装数量:</label>
														<div class="layui-input-inline" style='width:80px;margin-right:0px;'>
															 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_huowu_pack_number" value=""  lay-verify="required" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="text">
											
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" style='width:80px;'>重量:</label>
														<div class="layui-input-inline" style='width:100px;margin-right:0px;'>
															 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_huowu_weight" value=""  lay-verify="required" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="text">
											
														</div>
													</div>	
													<div class="layui-inline" style='margin-right:0px;'>
														<label class="layui-form-label input-required" style='width:80px;'>体积:</label>
														<div class="layui-input-inline" style='width:100px;'>
															 <input id='ld_send_time_<?php echo $vv['orders_id']; ?>' orders_id="<?php echo $vv['orders_id']; ?>"  name="_huowu_volume" value=""  lay-verify="required" placeholder="" autocomplete="off" class="layui-input ld_send_time" type="text">
											
														</div>
													</div>

												<div class="more-box layui-inline addGoods">	+	</div>
													
	
										</div>
									
									
									</div>


									<div class="layui-inline" style='margin-right:0px;'>
										<label class="layui-form-label input-required">发货地:</label>
										<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
											<select name="_send_province_id" id='send_city_id'  lay-verify="required" class="province_id" lay-filter="sheng-shi">
												<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
													<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
												<?php endforeach; endif; else: echo "" ;endif; ?>
											</select>							
										</div>
									</div>				
									<div class="layui-inline" style='margin-right:0px;'>

										<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
											<select name="_send_city_id" id='send_city_id'  lay-verify="required" class="city_id" lay-filter="shi-qu">
												<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
													<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
												<?php endforeach; endif; else: echo "" ;endif; ?>
											</select>							
										</div>
									</div>	
									<div class="layui-inline" style='margin-right:0px;'>

										<div class="layui-input-inline" style='width:105px;margin-right:0px;'>
											<select name="_send_area_id" id='send_city_id'  lay-verify="required" class="area_id" lay-filter="qu">
												<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
													<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $vv['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
												<?php endforeach; endif; else: echo "" ;endif; ?>
											</select>							
										</div>
									</div>		
									<div class="layui-inline" style='margin-right:0px;'>
										<label class="layui-form-label input-required" >发货地址:</label>
										<div class="layui-input-inline" style='margin-right:0px;'>
							  				<input id="all_weight" name="_send_address" value="<?php echo $vv['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
						
										</div>
									</div>		
																
								
									
								</div>	
										
								<div  class="fee-content">
								
								<div class="fee-add" data-id="tihuo"  data-index="0">
									+
								</div>
								
								<div class="fee-item" style="">
									
								
								<div class="layui-inline layui-col-md3" >
									<label class="layui-form-label " style="width: 100px;">成本:</label>
									<div class=" layui-col-md5">
									<select name="fee_id" class="fee_select" lay-filter="fee_select">
										<option value="">请选择</option>
										<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
										<option value="<?php echo $vo4['fee_id']; ?>"><?php echo $vo4['fee_name']; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
									</select>
									</div>
								</div><div class="layui-inline " >
									<label class="layui-form-label ">金额:</label>
									<div class="layui-col-md5">
										<input name="money" value="<?php echo $or['money']; ?>"  placeholder="" autocomplete="off" class="layui-input money" type="text">
																				
									</div>
								</div>	<div class="layui-inline " >
									<label class="layui-form-label " style="width: 70px;">税率:</label>
									<div class="layui-col-md5">
										<input  name="tax_rate" value="<?php echo $or['tax_rate']; ?>"  placeholder="" autocomplete="off" class="layui-input shuilv" type="text">
																				
									</div>
								</div>
								<div class="layui-inline " >
									<label class="layui-form-label ">总额:</label>
									<div class="layui-col-md5">
										<input  name="true_money" value="<?php echo $or['true_money']; ?>"  placeholder="" autocomplete="off" class="layui-input zonge" type="text">
																				
									</div>
								</div><div class="layui-inline " >
									<label class="layui-form-label ">备注:</label>
									<div class="layui-col-md5">
										<input  name="remark" value=""  placeholder="" autocomplete="off" class="layui-input remark" type="text">
																				
									</div>
								</div>
								
										</div>			
								
									
								</div>
										
			            <hr>
			</div>
			</div>
			
			
			
			
	<!-- 模板结束 -->	
	</div>
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/order/transport.js'></script>

<script type="text/javascript">
	FEE_TYPE=<?php echo json_encode($fee); ?>
	
	fee_index=1000;
	project_index=1000;
	
	$(document).on('click',".fee-add",function(){
		
		fee_index++;
		
		var p_name=$(this).attr("data-id");
		var data_index=$(this).attr("data-index");
		
		
		if(p_name == 'zhongzhuan' || p_name == 'tihuo'|| p_name == 'zhengche')
		{
			p_name=p_name+"["+data_index+"]";
			
		}
		var dom=$(this).parent();
			
		$(dom).after(`<div class="fee-content">
														
														<div class="fee-subtract" data-id="zhengche">
															-
														</div>
														
														<div class="fee-item" style="">
															
														
														<div class="layui-inline layui-col-md3">
															<label class="layui-form-label " style="width: 100px;">成本:</label>
															<div class=" layui-col-md5">
												  			<select name="fee_id_${p_name}[${fee_index}]" class="fee_select" lay-filter="fee_select">
															<option value="">请选择</option>
															<?php if(is_array($fee) || $fee instanceof \think\Collection || $fee instanceof \think\Paginator): if( count($fee)==0 ) : echo "" ;else: foreach($fee as $key=>$vo4): ?>
															<option value="<?php echo $vo4['fee_id']; ?>"><?php echo $vo4['fee_name']; ?></option>
															<?php endforeach; endif; else: echo "" ;endif; ?>
																											  			
																											  			</select>
															</div>
														</div><div class="layui-inline ">
															<label class="layui-form-label money">金额:</label>
															<div class="layui-col-md5">
												  				<input  name="money_${p_name}[${fee_index}]" value="" placeholder="" autocomplete="off" class="layui-input money" type="text">
													
															</div>
														</div>	<div class="layui-inline ">
															<label class="layui-form-label " style="width: 70px;">税率:</label>
															<div class="layui-col-md5">
												  				<input  name="tax_rate_${p_name}[${fee_index}]" value="" placeholder="" autocomplete="off" class="layui-input shuilv" type="text">
													
															</div>
														</div>
														<div class="layui-inline ">
															<label class="layui-form-label ">总额:</label>
															<div class="layui-col-md5">
												  				<input name="true_money_${p_name}[${fee_index}]" value="" placeholder="" autocomplete="off" class="layui-input zonge" type="text">
													
															</div>
														</div><div class="layui-inline ">
															<label class="layui-form-label ">备注:</label>
															<div class="layui-col-md5">
												  				<input  name="remark_${p_name}[${fee_index}]" value="" placeholder="" autocomplete="off" class="layui-input" type="text">
													
															</div>
														</div>
														
																</div>			
														
															
														</div>`);
														form.render()
	})

	
</script>>

</body>
</html>


