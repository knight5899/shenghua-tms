<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:79:"/var/www/html/test_erp/public/../application/index/view/shipment/line_info.html";i:1652246787;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>
  			发运详情
	
	</title>
<style>
	td{overflow: inherit!important;}
.addGoods{
	    color: blue;
	    display: block;
	    margin: 0 auto;
	    text-align: center;
	    font-size: 39px;
}
.delGoods{
	    color: red;
	    display: block;
	    margin: 0 auto;
	    text-align: center;
	    font-size: 39px;
}
.layui-form-item .layui-inline .layui-input-inline{

	margin-right:0px;

}	
.layui-form-item .layui-inline {

  margin-right: 0px;
  margin-bottom:0px;
}
.layui-inline{
float:left
}

.layui-form .layui-form-label {
  width: 100px;
}
</style>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">







			<form class="layui-form layui-form-pane"  id="form1" onSubmit="return shipmentAdd()">
	
<fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>运单信息</legend>
</fieldset>	
		<div class="layui-form-item" style='margin-bottom:2px;'>
						<div class="layui-inline">
							<label class="layui-form-label">运单号:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  name="" value="<?php echo $result['orders_number']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>
				
						<?php if(\think\Request::instance()->get('shipment_uuid')): ?>
							<div class="layui-inline">
								<label class="layui-form-label">发运日期:</label>
								<div class="layui-input-inline "  style='width:110px;' >
									 <input   name="shipment_time"  id='shipment_time' value="<?php if($shipmentResult['shipment_time'] != ''): ?><?php echo date('Y-m-d',$shipmentResult['shipment_time']); endif; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
					
								</div>
							</div>	
							<div class="layui-inline">
								<label class="layui-form-label">到货日期:</label>
								<div class="layui-input-inline "  style='width:110px;' >
									 <input name="need_time" id='need_time'  value="<?php if($shipmentResult['need_time'] != ''): ?><?php echo date('Y-m-d',$shipmentResult['need_time']); endif; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
					
								</div>
							</div>							
						<?php else: ?>
							<div class="layui-inline">
								<label class="layui-form-label">发运日期:</label>
								<div class="layui-input-inline "  style='width:110px;' >
									 <input   name="shipment_time"  id='shipment_time' value="<?php if($result['pickup_time'] != ''): ?><?php echo date('Y-m-d',$result['pickup_time']); endif; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
					
								</div>
							</div>	
							<div class="layui-inline">
								<label class="layui-form-label">到货日期:</label>
								<div class="layui-input-inline "  style='width:110px;' >
									 <input name="need_time" id='need_time'  value="<?php if($result['send_time'] != ''): ?><?php echo date('Y-m-d',$result['send_time']); endif; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
					
								</div>
							</div>																
						<?php endif; ?>

						<div class="layui-inline">
							<label class="layui-form-label">创建人:</label>
							<div class="layui-input-inline " style='width:110px;' >
								 <input  name="" value="<?php echo $result['create_user_name']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>
				
			
						<div class="layui-inline  clear">
							<label class="layui-form-label">创建时间:</label>
							<div class="layui-input-inline " style='width:130px;'  >
								 <input name="" value="<?php echo date('Y-m-d',$result['create_time']); ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>	
						<div class="layui-inline">
							<label class="layui-form-label">送货方式:</label>
							<div class="layui-input-inline " style='width:110px;'>
								 <input   name="" value="<?php if($result['delivery_method'] == 1): ?>送货<?php else: ?>自提<?php endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>	
						<div class="layui-inline">
							<label class="layui-form-label">备注:</label>
							<div class="layui-input-inline "  style='width:460px;'>
								 <input   name="" value="<?php echo $result['remark']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>		
  		</div>			
					
	
			
						
    <div class="layui-form-item" >
						<div class="layui-inline">
							<label class="layui-form-label">发货方:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  name="" value="<?php echo $result['send_goods_company']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>   
						<div class="layui-inline">
							<label class="layui-form-label">发货人:</label>
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="" value="<?php echo $result['send_name']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>  
						<div class="layui-inline">
							<label class="layui-form-label">联系电话:</label>
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="" value="<?php echo $result['send_cellphone']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
						<div class="layui-inline">
							<label class="layui-form-label">发货地址:</label>
							<div class="layui-input-inline "  style='width:252px;' >
								 <input  name="" value="<?php echo $result['send_province_name']; ?><?php echo $result['send_city_name']; ?><?php echo $result['send_area_name']; ?><?php echo $result['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
						<div class="layui-inline clear">
							<label class="layui-form-label">收货方:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  name="" value="<?php echo $result['accept_goods_company']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>   
						<div class="layui-inline">
							<label class="layui-form-label">收货人:</label>
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="" value="<?php echo $result['accept_name']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>  
						<div class="layui-inline">
							<label class="layui-form-label">联系电话:</label>
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="" value="<?php echo $result['accept_cellphone']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
						<div class="layui-inline">
							<label class="layui-form-label">收货地址:</label>
							<div class="layui-input-inline "  style='width:252px;' >
								 <input  name="" value="<?php echo $result['accept_province_name']; ?><?php echo $result['accept_city_name']; ?><?php echo $result['accept_area_name']; ?><?php echo $results['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>																									 
    </div>			
<fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>发运安排</legend>
</fieldset>						
<div class="layui-form-item">
    <label class="layui-form-label">发运类型</label>
    <div class="layui-input-block" id='fayuncheckbox'>
      <input type="radio" name="shipment_type" lay-filter='switchFayun' value="1" title="包车"  <?php if($shipmentResult['shipment_type'] == 1): ?>checked <?php endif; ?>>
      <input type="radio" name="shipment_type" lay-filter='switchFayun' value="2" title="干线"  <?php if($shipmentResult['shipment_type'] == 2): ?>checked <?php endif; ?>>
      <?php if($shipmentResult['shipment_type'] == 1): ?>
                        <span style='display:none' id='checkduanbo'><input type="checkbox"  lay-skin="primary" name="is_short_barge" title="短驳" value='1' ></span>
      
      
      <?php else: ?>
      	
                        <span id='checkduanbo'><input type="checkbox"  lay-skin="primary" name="is_short_barge" title="短驳" value='1'  <?php if($result['is_short_barge'] == 1): ?> checked<?php endif; ?>></span>
      
      <?php endif; ?>
    </div>
  </div>
  
 <div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">承运商:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								<select name='supplier_uuid'  lay-filter='supplier_choose' >
										<option value=''>请选择</option>
									<?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
										<option value='<?php echo $vo['supplier_uuid']; ?>' <?php if($vo['supplier_uuid'] == $shipmentResult['supplier_uuid']): ?> selected<?php endif; ?>><?php echo $vo['supplier_name']; if($vo['supplier_type'] == 1): ?>
										-自营
										<?php elseif($vo['supplier_type'] == 2): ?>
										-第三方
										<?php else: ?>
										-个体户
										<?php endif; ?>
										</option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>	
						<div class="layui-inline">
							<label class="layui-form-label">线路:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								<select name='supplier_line_uuid' id='choose_xianlu' lay-filter='choose_xianlu' >
										<?php if(is_array($supplierLineResult) || $supplierLineResult instanceof \think\Collection || $supplierLineResult instanceof \think\Paginator): if( count($supplierLineResult)==0 ) : echo "" ;else: foreach($supplierLineResult as $key=>$slr): ?>
											<option value='<?php echo $slr['supplier_line_uuid']; ?>' <?php if($slr['supplier_line_uuid'] == $shipmentResult['supplier_line_uuid']): ?>selected<?php endif; ?>><?php echo $slr['start_location_name']; ?>-<?php echo $slr['end_location_name']; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>
						</div>		
						<div class="layui-inline">
							<label class="layui-form-label">专线编号:</label>
							<div class="layui-input-inline "  style='width:250px;' >
								 <input  name="supplier_shipment_number" value="<?php echo $shipmentResult['supplier_shipment_number']; ?>"  placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>		
						<div class="layui-inline  clear">
							<label class="layui-form-label">发站人:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  id="send_name" value="<?php echo $shipmentSupplierLineResult['start_site_name']; ?>"  placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
							<div class="layui-inline">
							<label class="layui-form-label">发站电话:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input id='send_cellphone'  value="<?php echo $shipmentSupplierLineResult['start_site_phone']; ?>"placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
							<div class="layui-inline">
							<label class="layui-form-label">发站地址:</label>
							<div class="layui-input-inline "  style='width:250px;' >
								 <input  id="send_address" value="<?php echo $shipmentSupplierLineResult['start_province_name']; ?><?php echo $shipmentSupplierLineResult['start_city_name']; ?><?php echo $shipmentSupplierLineResult['start_site_address']; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
						<div class="layui-inline clear">
							<label class="layui-form-label">到站人:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input  id="accept_name" value="<?php echo $shipmentSupplierLineResult['end_site_name']; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
							<div class="layui-inline">
							<label class="layui-form-label">到站电话:</label>
							<div class="layui-input-inline "  style='width:130px;' >
								 <input   id="accept_cellphone" value="<?php echo $shipmentSupplierLineResult['end_site_phone']; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
							<div class="layui-inline">
							<label class="layui-form-label">到站地址:</label>
							<div class="layui-input-inline "  style='width:250px;' >
								 <input   id="accept_address" value="<?php echo $shipmentSupplierLineResult['end_province_name']; ?><?php echo $shipmentSupplierLineResult['end_city_name']; ?><?php echo $shipmentSupplierLineResult['end_site_address']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>																									
  </div> 
  
    <table class="layui-table">
      <colgroup>
        <col width="150">
        <col width="100">
        <col>
      </colgroup>
      <thead>
        <tr>
          <th>货物信息</th>
          <th>下单件数</th>
          <th>下单数量</th>
          <th>包装单位</th>
          <th>下单重量</th>
           <th>下单体积</th>
           
          <th>发运件数</th>
          <th>发运数量</th>
         <th>发运重量</th>
          <th>发运体积</th>
           <th>计价方式</th>           
            <th>&nbsp&nbsp单价&nbsp&nbsp</th>    
            <th>发运费</th> 

        </tr> 
      </thead>
      <tbody class="goods-contents-body">
			<?php if(is_array($result[orders_goods_info]) || $result[orders_goods_info] instanceof \think\Collection || $result[orders_goods_info] instanceof \think\Paginator): if( count($result[orders_goods_info])==0 ) : echo "" ;else: foreach($result[orders_goods_info] as $key=>$v): ?>
        <tr class='goods_info'>
          <td>
          		<select name='goods_id[]'>
          			<option value='<?php echo $v['goods_id']; ?>'><?php echo $v['goods_name']; ?></option>
          		</select>

					
		
					
          </td>
          <td>									  				
          		<?php echo $v['estimated_count']; ?>
          </td>
       <td><?php echo $v['estimated_pack_count']; ?></td>
	   <td>
	   <select name='shipment_pack_unit[]'>
	   	<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key=>$v2): ?>
	   		<option value='<?php echo $key; ?>' <?php if($v['estimated_pack_unit'] == $key): ?> selected<?php endif; ?>><?php echo $v2; ?></option>
	   	<?php endforeach; endif; else: echo "" ;endif; ?>
	   	</select>
	   </td>
		<td><?php echo $v['estimated_weight']; ?>	</td>	
		<td><?php echo $v['estimated_volume']; ?></td>
          <td>	
          	<?php if(\think\Request::instance()->get('shipment_uuid') != ''): ?>
       			<input   id="shipment_count"  name='shipment_count[]' value="<?php echo $v['shipment_count']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
			 <?php else: ?>
       			<input   id="shipment_count"  name='shipment_count[]'  value="<?php echo $v['estimated_count']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
			 
			 <?php endif; ?>				
          		
          </td>
       <td>
          	<?php if(\think\Request::instance()->get('shipment_uuid') != ''): ?>
       			<input   id="shipment_pack_count"  name='shipment_pack_count[]' value="<?php echo $v['shipment_pack_count']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
			 <?php else: ?>
       			<input   id="shipment_pack_count"  name='shipment_pack_count[]'  value="<?php echo $v['estimated_pack_count']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
			 
			 <?php endif; ?>			       
       </td>
        <td>
          	<?php if(\think\Request::instance()->get('shipment_uuid') != ''): ?>
       			<input   id="shipment_weight"  name='shipment_weight[]' value="<?php echo $v['shipment_weight']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
			 <?php else: ?>
       			<input   id="shipment_weight"  name='shipment_weight[]'  value="<?php echo $v['estimated_weight']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
			 
			 <?php endif; ?>			       
       </td>
       <td>
          	<?php if(\think\Request::instance()->get('shipment_uuid') != ''): ?>
       			<input   id="shipment_volume"  name='shipment_volume[]' value="<?php echo $v['shipment_volume']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
			 <?php else: ?>
       			<input   id="shipment_volume"  name='shipment_volume[]'  value="<?php echo $v['estimated_volume']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
			 
			 <?php endif; ?>			       
       </td>             

	   <td>
	   		<select name='shipment_charge_type[]'  lay-filter='choose_billing_unit'>

	   		<?php if(is_array($baseConfig['order']['goods_cost_unit']) || $baseConfig['order']['goods_cost_unit'] instanceof \think\Collection || $baseConfig['order']['goods_cost_unit'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_cost_unit'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_cost_unit'] as $key2=>$vo): ?>
	   			<option value='<?php echo $key2; ?>' <?php if($v['shipment_charge_type'] == $key2): ?> selected<?php endif; ?>><?php echo $vo; ?></option>
			<?php endforeach; endif; else: echo "" ;endif; ?>
	   		</select>
	   </td>
		<td>
			<input    name='unit_price[]' value="<?php echo $v['unit_price']; ?>"  class="layui-input unit_price" type="text">
		</td>	
		<td>
		<input     name='shipment_money[]' value="<?php echo $v['shipment_money']; ?>"  class="layui-input shipment_money" type="text">
		</td>										
										
        </tr>
		<?php endforeach; endif; else: echo "" ;endif; ?>




        <tr>
          <td>
    
			
				合计
	

          </td>
          <td id='xiadanjianshu'>			
  				
          </td>
       <td id='xiadanshuliang'>
       
       </td>
       	<td>	</td>
	   <td id='xiadanzhongliang'></td> 
		<td id='xiadantiji'></td>

		<td id='fayunjianshu'></td>
		<td id='fayunshuliang'></td>	
		<td id='fayunzhongliang'></td>
		<td id='fayuntiji'></td>
			<td>	</td>
		<td id='danjia'></td>
		<td id='fayunfei'></td>
        </tr>		
      </tbody>
    </table>
 <fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>费用明细 总计:<span id='zongji'><?php echo $shipmentResult['pay_all_money']; ?></span></legend>
</fieldset>			         
 <div class="layui-form-item" id='yunfei'>
						<div class="layui-inline">
							<div class="layui-input-inline" style='width:100px;;float:left'  >
								<select  name='cost_id[]' >
									<?php if(is_array($costResult) || $costResult instanceof \think\Collection || $costResult instanceof \think\Paginator): if( count($costResult)==0 ) : echo "" ;else: foreach($costResult as $key=>$vo): ?>
									<option value='<?php echo $vo['cost_id']; ?>' <?php if($vo['cost_id'] == 5): ?> selected<?php endif; ?>><?php echo $vo['cost_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>	
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="cost_money[]" id='fyms_yunfei' value="<?php if(is_array($shipmentResult['shipment_cost']) || $shipmentResult['shipment_cost'] instanceof \think\Collection || $shipmentResult['shipment_cost'] instanceof \think\Paginator): if( count($shipmentResult['shipment_cost'])==0 ) : echo "" ;else: foreach($shipmentResult['shipment_cost'] as $key=>$sc): if($sc['cost_id'] == 5): ?><?php echo $sc['cost_money']; endif; endforeach; endif; else: echo "" ;endif; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input yunfei" type="text">
							</div>
						</div>	
						<div class="layui-inline">
							<div class="layui-input-inline" style='width:100px;;float:left'  >
								<select  name='cost_id[]'  >
									<?php if(is_array($costResult) || $costResult instanceof \think\Collection || $costResult instanceof \think\Paginator): if( count($costResult)==0 ) : echo "" ;else: foreach($costResult as $key=>$vo): ?>
									<option value='<?php echo $vo['cost_id']; ?>' <?php if($vo['cost_id'] == 7): ?> selected<?php endif; ?>><?php echo $vo['cost_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>	
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="cost_money[]" id='fyms_songhuofei'  value="<?php if(is_array($shipmentResult['shipment_cost']) || $shipmentResult['shipment_cost'] instanceof \think\Collection || $shipmentResult['shipment_cost'] instanceof \think\Paginator): if( count($shipmentResult['shipment_cost'])==0 ) : echo "" ;else: foreach($shipmentResult['shipment_cost'] as $key=>$sc): if($sc['cost_id'] == 7): ?><?php echo $sc['cost_money']; endif; endforeach; endif; else: echo "" ;endif; ?>"  placeholder="" autocomplete="off" class="layui-input yunfei" type="text">
							</div>
						</div>	
						<div class="layui-inline">
							<div class="layui-input-inline" style='width:100px;;float:left'  >
								<select  name='cost_id[]'  >
									<?php if(is_array($costResult) || $costResult instanceof \think\Collection || $costResult instanceof \think\Paginator): if( count($costResult)==0 ) : echo "" ;else: foreach($costResult as $key=>$vo): ?>
									<option value='<?php echo $vo['cost_id']; ?>' <?php if($vo['cost_id'] == 8): ?> selected<?php endif; ?>><?php echo $vo['cost_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
							</div>	
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="cost_money[]"  id='fyms_zhuangxiefei'  value="<?php if(is_array($shipmentResult['shipment_cost']) || $shipmentResult['shipment_cost'] instanceof \think\Collection || $shipmentResult['shipment_cost'] instanceof \think\Paginator): if( count($shipmentResult['shipment_cost'])==0 ) : echo "" ;else: foreach($shipmentResult['shipment_cost'] as $key=>$sc): if($sc['cost_id'] == 8): ?><?php echo $sc['cost_money']; endif; endforeach; endif; else: echo "" ;endif; ?>"  placeholder="" autocomplete="off" class="layui-input yunfei" type="text">
							</div>
						</div>													
						<?php if(is_array($shipmentResult['shipment_cost']) || $shipmentResult['shipment_cost'] instanceof \think\Collection || $shipmentResult['shipment_cost'] instanceof \think\Paginator): if( count($shipmentResult['shipment_cost'])==0 ) : echo "" ;else: foreach($shipmentResult['shipment_cost'] as $key=>$ssc): if($ssc['cost_id'] != 5 && $ssc['cost_id'] != 7 && $ssc['cost_id'] != 8): ?>
							<div class="layui-inline">
								<div class="layui-input-inline" style='width:100px;;float:left'  >
									<select  name='cost_id[]'  >
										<?php if(is_array($costResult) || $costResult instanceof \think\Collection || $costResult instanceof \think\Paginator): if( count($costResult)==0 ) : echo "" ;else: foreach($costResult as $key=>$vo): ?>
										<option value='<?php echo $vo['cost_id']; ?>' <?php if($vo['cost_id'] == $ssc['cost_id']): ?> selected<?php endif; ?>><?php echo $vo['cost_name']; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
									</select>
								</div>	
								<div class="layui-input-inline "  style='width:110px;' >
									 <input  name="cost_money[]"  id='fyms_zhuangxiefei'  value="<?php echo $ssc['cost_money']; ?>"  placeholder="" autocomplete="off" class="layui-input yunfei" type="text">
								</div>
								<div style='position:absolute;left:190px;top:-15px;color:red;width:25px;height:25px;font-size:20px;z-index:999'><a href='javascript:void(0)' class='cost_info'><i class="layui-icon" style='font-size:20px;'></i></a></div>	
							</div>		
							<?php endif; endforeach; endif; else: echo "" ;endif; ?>

						
						<div class="layui-inline" id='addCostType'>
							<label class="layui-form-label">添加</label>

						</div>						
  </div>						
 <fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>付款信息</legend>
</fieldset>			         
 <div class="layui-form-item" id='fukuanxinxi'>
 						<?php if(is_array($shipmentResult['shipment_pay']) || $shipmentResult['shipment_pay'] instanceof \think\Collection || $shipmentResult['shipment_pay'] instanceof \think\Paginator): if( count($shipmentResult['shipment_pay'])==0 ) : echo "" ;else: foreach($shipmentResult['shipment_pay'] as $key=>$ssp): ?>
						<div class="layui-inline">
								<div class="layui-input-inline" style='width:100px;;float:left'  >
							<select name='pay_type[]'>
								<option value='1' <?php if($ssp['pay_type'] == 1): ?>selected<?php endif; ?>>现金</option>
								<option value='2' <?php if($ssp['pay_type'] == 2): ?>selected<?php endif; ?>>油卡</option>
								<option value='3' <?php if($ssp['pay_type'] == 3): ?>selected<?php endif; ?>>转账</option>
							</select>
							</div>
							<div class="layui-input-inline "  style='width:110px;' >
								 <input  name="pay_money[]" value="<?php echo $ssp['pay_money']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">

							</div>
							<div style='position:absolute;left:190px;top:-15px;color:red;width:25px;height:25px;font-size:20px;z-index:999'><a href='javascript:void(0)' class='pay_info'><i class="layui-icon" style='font-size:20px;'></i></a></div>
						</div>	
						<?php endforeach; endif; else: echo "" ;endif; ?>
						
						<div class="layui-inline"  id='addPayInfo'>
							<label class="layui-form-label">添加</label>

						</div>							
  </div>								    																																																					
 <fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>设置</legend>
</fieldset>			         

<div class="layui-form-item">
	<div class="layui-inline">
	    <label class="layui-form-label">结束安排</label>
	    <div class="layui-input-inline" style='width:50px;'>
	        <span ><input type="checkbox"  lay-skin="primary" name="shipment_status"  value='1'  <?php if($result['shipment_status'] == 1): ?>checked<?php endif; ?>></span>
	    </div>
	</div>
	<div class="layui-inline">
		<label class="layui-form-label">备注:</label>
		<div class="layui-input-inline " >
			 <input  name="shipment_remark"  style='width:600px;' value="<?php echo $shipmentResult['shipment_remark']; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
		</div>
	</div>						
  </div>				
				

		
			</form>
	
		
		

				

</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/shipment/shipment.js'></script>

</body>
</html>
<script>
layui.use(['laydate','element','form'], function(){
    var laydate = layui.laydate;
    var element = layui.element;
    var formSelects = layui.formSelects;
    var upload=layui.upload;
    var form = layui.form
    laydate.render({
        elem: '#shipment_time',
        trigger: 'click',
        lang: 'zn'
    });
    laydate.render({
        elem: '#need_time',
        trigger: 'click',//解决一闪而过的问题
        lang: 'zn'
    });
  
 
    
    
    form.on('radio(switchFayun)', function (data) {
    			var value = data.value;   
 				if(value==1){
 					$('#checkduanbo').hide();
 					var goods_info = $('.goods_info');
 					for(var i =0;i<goods_info.length;i++){
 					
 						var goods_input = goods_info.eq(i).find('td').eq(10).find('select');
 					
 						goods_input.find("option[value=6]").attr("selected", "selected");
 					}
 					
 				}else{
 					$('#checkduanbo').show();		
 				}
		
    	　　 form.render();
    	});
    	
    //choose_xianlu

    form.on('select(supplier_choose)', function (data) {
    			var value = data.value;   
    			$('#choose_xianlu').html("<option value=''>请选择</option>");
			    data = {supplier_uuid:value,status:1}
			    $.ajax({
			        type: "post",
			        url: "/source/getSupplierLineAjax",
			        data: data,
			        dataType: "json",
			        success: function(data){
			        
			            $.each(data.data,function(index,key){
			
			            	$('#choose_xianlu').append("<option value='"+key.supplier_line_uuid+"'>"+key.start_location_name+'-'+key.end_location_name+"</option>")
			
			            })

			            form.render('select');
			
			        },
			        error:function(XMLHttpRequest, textStatus, errorThrown){
			            layer.msg('验证失败')
			        }
			    });				
				
    	　　
    	});
    
    //选择线路 开始计算价格
    form.on('select(choose_xianlu)', function (data) {
		var value = data.value;   
		
		var xianluxuanze = $("input[name='shipment_type']:checked").val()

	    data = {supplier_line_uuid:value}
	    $.ajax({
	        type: "post",
	        url: "/source/getSupplierLineAjax",
	        data: data,
	        dataType: "json",
	        success: function(data){


				$('#send_name').val(data.data[0].start_site_name)
				$('#send_cellphone').val(data.data[0].start_site_phone)
				$('#send_address').val(data.data[0].start_province_name+data.data[0].start_city_name+data.data[0].start_site_address)
				$('#accept_name').val(data.data[0].end_site_name)
				$('#accept_cellphone').val(data.data[0].end_site_phone)
				$('#accept_address').val(data.data[0].end_province_name+data.data[0].end_city_name+data.data[0].end_site_address)
				
	        },
	        error:function(XMLHttpRequest, textStatus, errorThrown){
	            layer.msg('验证失败')
	        }
	    });				

	    //假如包车 等于价格清空
		if(xianluxuanze ==1){
			var goods_info = $('.goods_info');
			for(var i=0;i<goods_info.length;i++){
				goods_info.eq(i).find('td').eq(11).find('input').val('');

				goods_info.eq(i).find('td').eq(12).find('input').val('');
			}			
			
			return false;
		}
	    data = {supplier_line_uuid:value,shipment_charge_type:1}
		//开始获得价格
	    $.ajax({
	        type: "post",
	        url: "/shipment/getSupplierPriceAjax",
	        data: data,
	        dataType: "json",
	        success: function(data){
	        	//代表有数据
				
	
					$('#fyms_songhuofei').val(data[0].delivery_price)

					var goods_info = $('.goods_info');
					for(var i=0;i<goods_info.length;i++){
						

						
						//代表 重量
						if(goods_info.eq(i).find('td').eq(8).find('input').val() !=''){
							
							for(var j=0;j<data.length;j++){
								if(data[j].billing_unit ==1){

									if(goods_info.eq(i).find('td').eq(8).find('input').val() >= data[j].interval_start && goods_info.eq(i).find('td').eq(8).find('input').val() <= data[j].interval_end ){
									
										var key = goods_info.eq(i).find('td').eq(8).find('input').val()
										goods_info.eq(i).find('td').eq(11).find('input').val(data[j].unit_price);
										var unit_price = data[j].unit_price;
										 goods_info.eq(i).find('td').eq(10).find('select').eq(0).find('option').eq(0).attr("selected",'selected');

									}
								}
							}
						}else if(goods_info.eq(i).find('td').eq(9).find('input').val() !=''){
							for(var j=0;j<data.length;j++){
								if(data[j].billing_unit ==2){
									if(goods_info.eq(i).find('td').eq(9).find('input').val() >= data[j].interval_start && goods_info.eq(i).find('td').eq(9).find('input').val() <= data[j].interval_start ){
										var key = goods_info.eq(i).find('td').eq(9).find('input').val()
										goods_info.eq(i).find('td').eq(11).find('input').val(data[j].unit_price);
										var unit_price = data[j].unit_price;
										 goods_info.eq(i).find('td').eq(10).find('select').eq(0).find('option').eq(1).attr("selected",'selected');

									}
								}
							}							
						}else if(goods_info.eq(i).find('td').eq(6).find('input').val() !=''){
							for(var j=0;j<data.length;j++){
								if(data[j].billing_unit ==3){
									if(goods_info.eq(i).find('td').eq(6).find('input').val() >= data[j].interval_start && goods_info.eq(i).find('td').eq(6).find('input').val() <= data[j].interval_start ){
										var key = goods_info.eq(i).find('td').eq(6).find('input').val()
										goods_info.eq(i).find('td').eq(11).find('input').val(data[j].unit_price);
										var unit_price = data[j].unit_price;
										 goods_info.eq(i).find('td').eq(10).find('select').eq(0).find('option').eq(2).attr("selected",'selected');

									}
								}
							}									
						}

						
						var yf = parseFloat(key)*unit_price
						goods_info.eq(i).find('td').eq(12).find('input').val(yf);
						/*
						if(data[j].billing_unit == 1){
	

						*/
					}		
		
	
					

					
		
				form.render('select');
				checkGoodInfo();
	        	zongji();
	        },
	        error:function(XMLHttpRequest, textStatus, errorThrown){
	            layer.msg('验证失败')
	        }
	    });		
		
	    
	    
	    
　　
	});   	
    
	$('#addCostType').click(function(){

		var h =$('#cost_info').html();
		$(this).prev().append(h)
  		form.render('select');
	})    
	

$(document).on('click', '.cost_info', function() {
	$(this).parent().parent().remove()
		zongji()

});
	$('#addPayInfo').click(function(){
		var l =$('#fukuanxinxi').find('div').length
		

		var h =$('#pay_info').html();
		if(l==1){
			$('#fukuanxinxi').prepend(h)
		}else{
			$(this).prev().append(h)
		}
		
		
  		form.render('select');
	})    
	

$(document).on('click', '.pay_info', function() {
	$(this).parent().parent().remove()
	

});
	
	function checkGoodInfo(){

		var goods_info = $('.goods_info');
		var xiadanjianshu = 0;
		var xiadanshuliang = 0;
		var xiadanzhongliang =0;
		var xiadantiji = 0;
		var fayunjianshu = 0;
		var fayunshuliang = 0;
		var fayunzhongliang = 0;
		var fayuntiji = 0;
		var danjia = 0;
		var fayunfei = 0;
		for(var i=0;i<goods_info.length;i++){
			var xdjs = goods_info.eq(i).find('td').eq(1).html();
			xiadanjianshu+=parseFloat(xdjs)
			var xdsl = goods_info.eq(i).find('td').eq(2).html();
			xiadanshuliang+=parseFloat(xdsl)	
			var xdzl = goods_info.eq(i).find('td').eq(4).html();
			xiadanzhongliang+=parseFloat(xdzl)
			var xdtj = goods_info.eq(i).find('td').eq(5).html();
			xiadantiji+=parseFloat(xdtj)
			var fyjs = goods_info.eq(i).find('td').eq(6).find('input').val();
			fayunjianshu+=parseFloat(fyjs)
			var fysl = goods_info.eq(i).find('td').eq(7).find('input').val();
			fayunshuliang+=parseFloat(fysl)
			var fyzl = goods_info.eq(i).find('td').eq(8).find('input').val();
			fayunzhongliang+=parseFloat(fyzl)
			var fytj = goods_info.eq(i).find('td').eq(9).find('input').val();
			fayuntiji+=parseFloat(fytj)
			var dj = goods_info.eq(i).find('td').eq(11).find('input').val();

			if(parseFloat(dj)>0){
			
				danjia+=parseFloat(dj)
			}

			var fyf = goods_info.eq(i).find('td').eq(12).find('input').val();
			if(parseFloat(fyf)>0){
				
				fayunfei+=parseFloat(fyf)
			}
		
		}
		$('#xiadanjianshu').html(xiadanjianshu)
		$('#xiadanshuliang').html(xiadanshuliang)
		$('#xiadanzhongliang').html(xiadanzhongliang)
		$('#xiadantiji').html(xiadantiji)
		$('#fayunjianshu').html(fayunjianshu)
		$('#fayunshuliang').html(fayunshuliang)
		$('#fayunzhongliang').html(fayunzhongliang)
		$('#fayuntiji').html(fayuntiji)		
		$('#danjia').html(danjia)
		$('#fayunfei').html(fayunfei)	
		
	//	if($('#shipment_uuid')=='' || $('#shipment_uuid')==null){
			$('#fyms_yunfei').val(fayunfei)		
	//	}
		
		var shipment_uuid = $('#shipment_uuid').val()


		zongji()

	}

	$(document).on('keyup', '.yunfei', function() {
	
		zongji()
	});
	//计算总费用
	function zongji(){
		var yunfei = $('#yunfei').find('.yunfei')

		var zongji = 0;
		for(var i=0;i<yunfei.length;i++){
			
			var value =yunfei.eq(i).val()
			if(parseFloat(value)>0){
				zongji+=parseFloat(value)
			}
			
		}
	
		//if($('#shipment_uuid') =='' || $('#shipment_uuid') == null){
		$('#zongji').html(zongji)	
		//}
		
		$('#pay_all_money').val(zongji)		
	}
    form.on('select(choose_billing_unit)', function (obj) {

    	var uuid = $('#choose_xianlu').val()
    	

	    data = {supplier_line_uuid:uuid,shipment_charge_type:obj.value}
		//开始获得价格
	    $.ajax({
	        type: "post",
	        url: "/shipment/getSupplierPriceAjax",
	        data: data,
	        dataType: "json",
	        success: function(data){

	        	//代表有数据
				if(data.pricing_configure_id > 0){
				
				
					$(obj.elem).parent().parent().find('td').eq(11).find('input').val(data.unit_price);
						//代表 重量
						if(obj.value == 1){
							var key = $(obj.elem).parent().parent().find('td').eq(8).find('input').val()
				
							//代表体积
						}else if(obj.value==2){
							var key = $(obj.elem).parent().parent().find('td').eq(9).find('input').val()
							
						}

						var yf = parseFloat(key)*data.unit_price
						$(obj.elem).parent().parent().find('td').eq(12).find('input').val(yf);

				}else{
					$(obj.elem).parent().parent().find('td').eq(11).find('input').val('');
					$(obj.elem).parent().parent().find('td').eq(12).find('input').val('');
				}
	        	
	        	
				checkGoodInfo();
	        },
	        error:function(XMLHttpRequest, textStatus, errorThrown){
	            layer.msg('验证失败')
	        }
	    });		

    })	
    
	$('.unit_price').keyup(function(){
		var nowPrice = $(this).val();
		var value = $(this).parent().parent().find('td').eq(10).find('select').val();
		if(value==3){
			var price = $(this).parent().parent().find('td').eq(6).find('input').val();
		}else if(value==4){
			var price = $(this).parent().parent().find('td').eq(7).find('input').val();
		}else if(value==5){
			var price =0;
		}else if(value==1){
			var price = $(this).parent().parent().find('td').eq(8).find('input').val();
		}else if(value==2){
			var price = $(this).parent().parent().find('td').eq(9).find('input').val();
		}else{
			var price = 1;
		}

		var allmoney = price*nowPrice

		$(this).parent().parent().find('td').eq(12).find('input').val(allmoney);
		checkGoodInfo();
	})
    
	
	$('.shipment_money').keyup(function(){
		checkGoodInfo();
	})
	checkGoodInfo();




})

</script>



