<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:74:"/var/www/html/test_erp/public/../application/index/view/contact/index.html";i:1639618343;s:76:"/var/www/html/test_erp/application/index/view/book_tour/includes/header.html";i:1639618343;s:77:"/var/www/html/test_erp/application/index/view/book_tour/includes/top_nav.html";i:1639618343;s:76:"/var/www/html/test_erp/application/index/view/book_tour/includes/footer.html";i:1639618343;}*/ ?>
<!--
	This is view/templates/header.php
-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href='/assets/css/style.css' type="text/css" media="screen"/>
    <link rel="shortcut icon" href="http://www.nexusholidays.com.au/favicon.ico" type="image/x-icon"/>
    <title><?php echo $site_title; ?> - Nexus Holidays Booking System</title>
    <link type="text/css" href='/assets/css/jquery-ui.css' rel="stylesheet" />
    <link rel="stylesheet" href="/assets/css/icons.css" type="text/css" media="screen"/>
    <script src="/assets/js/jquery-1.9.0.min.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>
    <script src="/assets/js/jquery.validate.js"></script>
    <script>
        <?php
                $date_format=config('date_format');
        ?>
        default_date_format='<?php echo to_JS_date_format($date_format,1); ?>';
        default_date_mask='<?php echo to_JS_date_format($date_format,2); ?>';
        default_date_js_format='<?php echo to_JS_date_format($date_format,3); ?>';
        $(function(){
            $("#uername_icon_wrapper").tooltip();
        });
    </script>
</head>

<body>

<div id="wapper">
    <div id="main">
        <header>
            <div id="header">
                <h1 class="header_h1">
                    <a href="" title="Nexus Holidays Booking System">
                        <img class="header_logo" src="/assets/images/nexus_logo.png" alt="Nexus Holidays Logo" />
                    </a>
                </h1>
                <p class="header_banner">
                    <img src="/assets/images/banner_img1.png" width="550" alt="Nexus Holidays Header Banner">
                </p><!-- header banner -->
                <ul style="text-align: right;padding-right: 40px;">
                    <li class="header_login">Welcome,
                        <?php echo \think\Session::get('user.nickname'); ?>
                        <a id="uername_icon_wrapper" title="" href="/b2b_myinfo">
                            <span class="header_icon user_icon nh-user-alt-2 nh-icon-blue-base nh-icon"></span>My Account
                        </a> |
                        <a title="Logout" href="/booking_login/loginOut">
                            <span class="header_icon nh-logout nh-icon-blue-base nh-icon"></span>Logout
                        </a>

                    </li> <!-- header login details -->
                    <li class="header_contact">
                        <!--                        Need help?-->
                        <br/>
                        <p class="header_call_text" style="margin-top: -20px;">
                            <img src="/assets/images/phone_icon_25.png" alt="Nexus Holidays Help Desk"/>Call <span style="padding-left:7px;"> </span>
                        </p>
                        <p class="header_call_text" style="padding-left:-20px; padding-top:3px;"><span style="padding-top:10px; padding-left: 25px;"> </span></p>
                    </li> <!-- header contact -->
                </ul>
            </div>
        </header>
<!--
	This is view/includes/top_nav.php
-->
<script>
    $(function() {
        //$( document ).tooltip();
        $( "a#hotels" ).mouseover(function() {
            $("div#hotel-pop-up").css("display","block");
        });
        $( "div#hotel-pop-up" ).mouseover(function() {
            $("div#hotel-pop-up").css("display","block");
        });
        $( "div#hotel-pop-up" ).mouseout(function() {
            $("div#hotel-pop-up").css("display","none");
        });
        $.ajax({
            type: "POST",
            url: $("input#hotel-login").val(),
            dataType: "json",
            async: false
        }).done(function(data){
            console.log(data);
            if(data.status) {
                $("div#hotel-pop-up p").html(
                        "<p>username: " + data.data.username + "<br/> password: " + data.data.password +"</p>"
                );
            }
        });
    });
</script>
<style>
    a#hotels label {
        display: inline-block;
        width: 5em;
    }
    div#hotel-pop-up {
        display: none;
        position: absolute;
        z-index: 10;
        width: 230px;
        padding: 10px;
        background: #eeeeee;
        color: #000000;
        border: 1px solid #1a1a1a;
        font-size: 80%;
        left: 450px;
    }
    /*.animation-img {*/
    /*-webkit-animation: bounce 2s infinite ease-in-out;*/
    /*}*/
    /*@-webkit-keyframes bounce {*/
    /*0%, 20%, 60%, 100%  { -webkit-transform: translateY(0); }*/
    /*40%  { -webkit-transform: translateY(-5px); }*/
    /*80% { -webkit-transform: translateY(-0px); }*/
    /*}*/
</style>
<nav>
    <div id="top_nav"> <!-- top nav -->
        <ul>
            <li <?php if($controller_name == 'b2bnews'): ?>class="active"<?php endif; ?>><a href="/b2b_news" ><span class="nav_icon" title="News"><img src="/assets/images/latestnews.png"/></span>Latest News</a></li>
            <li <?php if($controller_name == 'booktour'): ?>class="active"<?php endif; ?>><a href="/book_tour/select_tour" title="Booing Form" class=""><span class="nav_icon"><img src="/assets/images/booktour.png"/></span>Book Tour</a></li>
            <li <?php if($controller_name == 'mybooking'): ?>class="active"<?php endif; ?>><a href="/my_booking/index" title="Booking History" class=""><span class="nav_icon"><img src="/assets/images/managebooking.png" /></span>My Bookings</a></li>
            <!--<li class=""><a href="" title="Nexus Holidays Day Tour Availability" class="" target="_blank" ><span class="nav_icon"><img class="animation-img" src="/assets/images/bus.png" /></span>Day Tours</a></li>-->
             <li <?php if($controller_name == 'commissiontable'): ?>class="active"<?php endif; ?>><a href="/commission_table" title="Agent Commission Table" class=""><span class="nav_icon"><img src="/assets/images/commissiontable.png" /></span>Commission</a></li>
            <li class=""><a href="" target="_blank" title="Official Website" class=""><span class="nav_icon"><img src="/assets/images/latestnews.png" /></span>Official Website</a></li>
            <li <?php if($controller_name == 'contact'): ?>class="active"<?php endif; ?>><a href="/contact" title="Nexus Holidays Contact Information" class=""><span class="nav_icon"><img src="/assets/images/contactus.png" /></span>Contact</a></li>
          </ul>
    </div> <!-- end top nav -->
    <div class="triangle-l"></div>
    <div class="triangle-r"></div>
    <div id="hotel-pop-up">
        <input type="hidden" id="hotel-login" value="">
        <h3>Your hotel login details: </h3>
        <p>
            Please contact us if you can't see hotel login details here.
        </p>
    </div>
</nav>

<style>
    #contact_div {
        width: 1230px;
    }

    #contact_div table#contact_table {
        width: 63%;
        height: 450px;
        font-size: 20px;
        margin: 10px 0 10px 0;
        padding: 5px 20px 5px 10px;
        float: left;

    }

    #contact_div table#contact_table tr td.title {
        vertical-align: top;
        font-weight: bold;
    }

    #contact_div table#contact_table tr td {
        padding: 5px 0;
        vertical-align: top;
        font-weight: 200;
    }

    #trading-hours {
        border:2px solid #9C9A95;
        background-color: #fff;
        -moz-border-radius: 7px;
        border-radius: 7px;
        padding: 10px;

        font-size: 18px;
        width: 32%;
        float: left;
        margin-top: 25px;
        margin-left: 10px;
    }

    .clear{
        clear: both;
    }

    .my-row:before, .my-row:after{
        display: table;
        content: " ";
    }

    .my-row:after{
        clear: both;
    }

    .my-row>div{
        float:left;
    }

    #trading-hours .icon{
        vertical-align: top;
        display: inline-block;
    }

    #trading-hours .header{
        font-weight: bold;
        color: #2f75bd;
    }

    #trading-hours h3{
        margin-bottom: 10px;
        font-size: 22px;
    }

    #trading-hours .my-row{
        border-top:1px dashed #ababad;
        padding: 8px 8px 8px 30px;
    }

</style>


    <section id="contact">
        <article>
            <h1 style="margin-top: 40px;margin-left: 10px;font-size: 24px;"><img src="http://127.0.0.1:9986/assets/images/Contactus_blue.png"> Contact Us</h1>

            <div id="contact_div">
                <table id="contact_table">
                    <tbody><tr>
                        <td class="title" style="width:30%; ">Address</td>
                        <td>
                            Suite 203 Level 2 Manning Building<br>                        451 Pitt Street Sydney NSW 2000                        </td>
                    </tr>
                    <tr>
                        <td class="title">Outbound Tours</td>
                        <td class="title">International</td>
                    </tr>
                    <tr>
                        <td class="title">Phone</td>
                        <td>
                            <b>USA, Canada, Europe, Cruise, Hong Kong &amp; Macau, Bali Tours</b><br>02 9281 8028 (8 lines)<br><b>China Tours</b><br>02 9281 8028<br><b>Asia Tours</b> <br>02 9281 8028<br><b>Emergency - After Hours Support (24 hours/7 days)</b><br>0432 629 087<br><!--<b>Sales Manager - Angel Yan (24 hours/7 days)</b> --><br><!--0448 803 262-->                         </td>
                    </tr>
                    <tr>
                        <td class="title">Email</td>
                        <td>agents@nexusholidays.com - For International Tours</td>
                    </tr>
                    <tr>
                        <td class="title">Inbound Tours</td>
                        <td class="title">Domestic</td>
                    </tr>
                    <tr>
                        <td class="title">Phone</td>
                        <td>
                            <b>Australia, New Zealand &amp; Fiji</b><br>02 9211 6080 (8 lines)<br><b>Emergency - After Hours Support (24 hours/7 days)</b><br>0401 328 978<br><b>Inbound Operations Manager - Kinson Wong</b> <br>0452 233 868<br><b>Inbound Manager - Sandra Liu</b> <br>0448 323 546<br><!--<b>Sales Manager - Angel Yan (24 hours/7 days)</b> --><br><!--0448 803 262-->                        </td>
                    </tr>
                    <tr>
                        <td class="title">Email</td>
                        <td>inbound@nexusholidays.com - For Domestic Tours</td>
                    </tr>
                    <tr>
                        <td class="title">Fax</td>
                        <td>02 8226 7788</td>
                    </tr>
                    <tr>
                        <td class="title">Worldwide offices</td>
                        <td>
                            London,
                            New York,
                            Washington,
                            Chicago,
                            San Francisco,
                            Los Angeles,
                            Houston,
                            Toronto,
                            Vancouver,
                            Shanghai,
                            Wuxi,
                            Beijing,
                            Sydney
                        </td>
                    </tr>
                    </tbody></table>
                <div id="trading-hours">
                    <div class="trading_hours">
                        <h3 class="header" style="text-align: center">Trading Hours</h3>

                        <div class="my-row">
                            <div class="header" style="width: 50%">Monday:</div>
                            <div style="width: 50%">9:00am to 6:00pm</div>
                        </div>


                        <div class="my-row">
                            <div class="header" style="width: 50%">Tuesday:</div>
                            <div style="width: 50%">9:00am to 6:00pm</div>
                        </div>


                        <div class="my-row">
                            <div class="header" style="width: 50%">Wednesday:</div>
                            <div style="width: 50%">9:00am to 6:00pm</div>
                        </div>


                        <div class="my-row">
                            <div class="header" style="width: 50%">Thursday:</div>
                            <div style="width: 50%">9:00am to 6:00pm</div>
                        </div>


                        <div class="my-row">
                            <div class="header" style="width: 50%">Friday:</div>
                            <div style="width: 50%">9:00am to 6:00pm</div>
                        </div>


                        <div class="my-row">
                            <div class="header" style="width: 50%">Saturday:</div>
                            <div style="width: 50%">10am to 3pm</div>
                        </div>


                        <div class="my-row">
                            <div class="header" style="width: 50%">Sunday:</div>
                            <div style="width: 50%">10am to 3pm</div>
                        </div>

                    </div>
                </div>
                <div class="clear"></div>
            </div>

        </article>
    </section>
    <!-- end contact section -->



<div class="modal"></div>
<br class="clear" />
<br class="clear" />

<!--
	This is view/includes/footer.php
-->
</div><!-- end main -->

<div id="footer">
    <div id="footer_text">
        <p>
            <?php
                    $arr=array();
                    $a = '';
            $b = 'ABN 34 124 885 356';
            if (!empty($a)) {
            $arr[]=$a;
            }
            if (!empty($b)) {
            $arr[]=$b;
            }
            echo implode(" | ",$arr);
            if(!empty($arr)){
            echo " |";
            }
            ?>
            &copy; <?php echo date('Y') ?>
            <a href="">
               <?php echo $booking_system_website; ?>
            </a> <?php echo $company_name; ?> | Site Map | <a href="">FAQ</a>
            | Nexus Holidays Booking System Pro v<?php echo $booking_system_version; ?></p>
    </div>
</div>
</div><!-- end wapper -->
<script src="https://use.fontawesome.com/690ab28302.js"></script>
<script>
    var checked = "<?php echo \think\Session::get('systemPopupChecked'); ?>";

    var systemModalPopup = function(){
        $.get("/info/getSystemModal", function(data) {
            if(data) {
                var modalData = $.parseJSON(data);
                $(modalData[0].content).appendTo('body').modal();
            }
        });
    };

    if(!checked) {
        systemModalPopup();
    }

</script>
</body>
</html>