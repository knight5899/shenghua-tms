<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:88:"/var/www/html/test_erp/public/../application/index/view/bill/customer_yes_bill_info.html";i:1664430026;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






  	<title>修改承运商账单</title>

</head>
<body class="layui-layout-body">
	<div class="table-nont user-manage company-pageHeight" style="scrollbar-width: none;-ms-overflow-style: none;overflow-x: hidden;overflow-y: auto;">
		<table class="layui-table layui-form" id="t1" lay-filter="demo2">
			<div class="layui-table-tool">
				<button id="aaa" class="layui-btn layui-btn-normal" data-type="parseTable">转换总计表</button>
				<span><a href='/bill/export?customer_bill_number=<?php echo $customer_bill_number; ?>&export=1'><button class="layui-btn">导 出</button></a></span>
			</div>
			<thead>
			<tr>
				<!--              <th lay-data="{type:'checkbox',fixed:'left'}"></th>-->
				<th lay-data="{field:'', title:'序号', type:'numbers', totalRowText:'合计', width:70}">序号</th>
				<th lay-data="{field:'customer_bill_number', width:180}">对账编号</th>
				<th lay-data="{field:'orders_number', width:180}">运单编号</th>
				<th lay-data="{field:'pickup_time',templet:function (d) {
                      return layui.util.toDateString(d.pickup_time*1000, 'yyyy-MM-dd');
                  },width:100}">运单日期</th>
				<th lay-data="{field:'verify_status',templet: function(d){
                      if(d.verify_status==1){
                          return '未核算';
                      }else if(d.verify_status==2){
                          return '已核算';
                      }else if(d.verify_status==3){
                          return '已对账';
                      }else if(d.verify_status==4){
                          return '已开票';
                      }else if(d.verify_status==5){
                          return '已收款';
                      }
                  }, width:90}">开票状态</th>
				<th lay-data="{field:'project_name', width:120}">项目</th>
				<th lay-data="{field:'send_goods_name', width:120}">发货方</th>
				<th lay-data="{field:'send_city_name', width:120}">发站</th>
				<th lay-data="{field:'accept_city_name', width:120}">到站</th>
				<th lay-data="{field:'accept_goods_name', width:120}">收货方</th>
				<th lay-data="{field:'goods_name', width:120}">货物名称</th>
				<th lay-data="{field:'realy_count', totalRow: true, width:120}">件数</th>
				<th lay-data="{field:'realy_pack_count', totalRow: true, width:120}">数量</th>
				<th lay-data="{field:'realy_weight', totalRow: true, width:120}">重量</th>
				<th lay-data="{field:'realy_volume', totalRow: true, width:120}">体积</th>
				<th lay-data="{field:'delivery_method', templet: function(d){
                      if(d.delivery_method==1){
                          return '送货';
                      }else if(d.delivery_method==2){
                          return '自提';
                      }
                  },
				width:120}">送货方式</th>
				<th lay-data="{field:'abnormal_status', templet: function(d){
                      if(d.abnormal_status==0){
                          return '否';
                      }else if(d.abnormal_status==1){
                          return '是';
                      }
                  },
				width:120}">异常</th>
				<th lay-data="{field:'money', totalRow: true, width:120}">总费用</th>
				<th lay-data="{field:'abnormal_money', totalRow: true, width:120}">赔款</th>
			</tr>
			</thead>

			<tbody>
			<?php if(is_array($customerYesBillInfoResult) || $customerYesBillInfoResult instanceof \think\Collection || $customerYesBillInfoResult instanceof \think\Paginator): if( count($customerYesBillInfoResult)==0 ) : echo "" ;else: foreach($customerYesBillInfoResult as $key=>$vo): ?>
			<tr>
				<!--              <th><input type="checkbox" name="layTableCheckbox" lay-skin="primary"></th>-->
				<th><?php echo $key+1; ?></th>
				<th class="a1" style="display:none"><?php echo $vo['finance_id']; ?></th>
				<th><?php echo $customer_bill_number; ?></th>
				<th><?php echo $vo['orders_number']; ?></th>
				<th><?php echo date('Y-m-d',$vo['pickup_time']); ?></th>
				<th><?php if($vo['verify_status'] == 1): ?>未核算<?php elseif($vo['verify_status'] == 2): ?>已核算<?php elseif($vo['verify_status'] == 3): ?>已对账<?php elseif($vo['verify_status'] == 4): ?>已开票<?php elseif($vo['verify_status'] == 5): ?>已收款<?php endif; ?></th>
				<th><?php echo $vo['project_name']; ?></th>
				<th class="a2"><?php echo $vo['send_goods_name']; ?></th>
				<th><?php echo $vo['send_city_name']; ?></th>
				<th><?php echo $vo['accept_city_name']; ?></th>
				<th><?php echo $vo['accept_goods_name']; ?></th>
				<th><?php echo $vo['goods_name']; ?></th>
				<th><?php echo $vo['realy_count']; ?></th>
				<th><?php echo $vo['realy_pack_count']; ?></th>
				<th><?php echo $vo['realy_weight']; ?></th>
				<th><?php echo $vo['realy_volume']; ?></th>
				<th><?php if($vo['delivery_method'] == 1): ?>送货<?php elseif($vo['delivery_method'] == 2): ?>自提<?php endif; ?></th>
				<th><?php if($vo['abnormal_status'] == 0): ?>否<?php elseif($vo['abnormal_status'] == 1): ?>是<?php endif; ?></th>
				<th><?php echo $vo['money']; ?></th>
				<th><?php echo $vo['abnormal_money']; ?></th>
				<th style="display:none"><?php echo $vo['orders_id']; ?></th>
			</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>

		</table>
		<input type="hidden" id="customer_bill_number_hidden" value="<?php echo $customer_bill_number; ?>">
	</div>
</div>
	<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
	<script>
		layui.use(['layer','table','form'], function(){
			var table = layui.table //表格
				,layer = layui.layer //弹层
				,form = layui.form

			var customer_bill_number = $("#customer_bill_number_hidden").val();
			$('#aaa').on('click', function(){
				table.init('demo2', {
					limit: 10
					,height: '500'
					,totalRow: true
					,url:'/bill/getCustomerYesBillAjax?customer_bill_number='+customer_bill_number+'&customer_bill_number='+customer_bill_number
					,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
					,defaultToolbar: ['filter', 'exports', 'print']
					,page: true
					,response: {
						statusCode: 200 //重新规定成功的状态码为 200，table 组件默认为 0
					}
					,parseData: function(res){ //将原始数据解析成 table 组件所规定的数据
						return {
							"code": res.status, //解析接口状态
							"msg": res.message, //解析提示文本
							"count": res.total, //解析数据长度
							"data": res.rows.item //解析数据列表
						};
					}
				});
			});

			//点击修改值按钮
			$('button[name="btn"]').bind('click', function (e) {
				var interval_start = $(this).parents("tr").find(".a1>input").val();
				var interval_end = $(this).parents("tr").find(".a2>input").val();
				var pricing_configure_id = $(this).parents("tr").find(".pricing_configure_id").html();

				$.ajax({
					type: "post",
					url: "/source/editPricingConfigureBatchAjax",
					data: {
						pricing_configure_id:pricing_configure_id,
						interval_start:interval_start,
						interval_end:interval_end
					},
					dataType: "json",
					success: function(data){
						// console.log(data);return false;
						if(data.code!=200){
							layer.msg(data.msg);
						}else if(data.code==200) {
							if(data.data==2){
								layer.msg("操作失败!");
							}else {
								layer.msg('操作成功', {
									time: 1, end: function (layero, index) {
										//当你在iframe页面关闭自身时
										var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
										parent.layer.close(index);
										parent.location.href = "/source/PricingConfigureEdit?pricing_configure_id=" + pricing_configure_id;
									}
								});
							}
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						layer.msg('验证失败')
					}
				});
			});
		});
	</script>
</body>
</html>
