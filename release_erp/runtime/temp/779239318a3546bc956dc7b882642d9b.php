<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:88:"/var/www/html/test_erp/public/../application/index/view/order/order_abnormal_review.html";i:1665191762;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	 <script src="/static/javascript/system/lodop.js"></script>
    <title>签收单</title>
	<style>
		.table1 tr{
			text-align: center;
		}
		.xl145{
			font-size: 18px;
			    font-weight: bold;
		}
		.xl102{
		position: absolute;
			bottom: 5px;
			left: 5px;
		}
		.xl103{
			position: absolute;
				bottom: 5px;
				right:130px ;
		}
		*{
		font-family:"宋体"
		}
		
	</style>
	</head>
	<body class="layui-layout-body">
	
	<div>
		
			
			<table class="table0" align="center" border="0" cellpadding="0" cellspacing="0" width="670" style="width:502pt;" >
				<tr>
					<td colspan="7" align="center"  height="30" class="xl145" width="670" style="font-size:24px;font-weight:bold;font-family:auto;">
						圣华国际理赔核查表
					</td>
				</tr>
				
				<tr align="left">
					<td height="20" class="xl64">
						QR－&nbsp;
					</td>
					<td class="xl64">
					</td>
					<td class="xl65">
						填表日期：
					</td>
					<td colspan="2" class="xl148">
						
					</td>
					<td class="xl66">
						编号：
					</td>
					<td class="xl64">
						
					</td>
				</tr>
				
			</table>
			
			<table class="table1" align="center" border="1" cellpadding="0" cellspacing="0" width="670" style="width:502pt;" class="ke-zeroborder">
				<tbody>
					
					<tr>
						<td height="38" class="xl67">
							发货方：
						</td>
						<td class="xl67">
							<?php echo $abnormalResult['orders_info']['send_goods_company']; ?>
						</td>
						<td class="xl68" width="91">
							委托单号：
						</td>
						<td colspan="2" class="xl149" width="160">
							<?php echo $abnormalResult['orders_info']['orders_number']; ?>
						</td>
						<td class="xl67">
							委托日期：
						</td>
						<td class="xl69">
							<?php echo date('Y-m-d',$abnormalResult['orders_info']['pickup_time']); ?>
						</td>
					</tr>
					<tr>
						<td height="36" class="xl68" width="91">
							收货方：
						</td>
						<td class="xl70" width="91">
							<?php echo $abnormalResult['orders_info']['accept_goods_company']; ?>
						</td>
						<td class="xl67">
							发货日期：
						</td>
						<td colspan="2" class="xl150" width="160">
						
						<?php if($abnormalResult['orders_info']['send_time']): ?>
							<?php echo date('Y-m-d',$abnormalResult['shipment_info'][0]['shipment_time'].send_time); endif; ?>
						</td>
						<td class="xl67">
							到货日期：
						</td>
						<td class="xl69">
						<?php if($abnormalResult['orders_info']['true_time']): ?>
							<?php echo date('Y-m-d',$abnormalResult['orders_info']['true_time']); endif; ?>
						</td>
					</tr>
					<tr>
						<td height="40" class="xl68" width="91">
							<span class="font0">业务员：</span>
						</td>
						<td class="xl68" width="91">
							<?php echo \think\Session::get('user.username'); ?>
						</td>
						<td class="xl67">
							理赔原因：
						</td>
						<td colspan="2" class="xl71">
							<?php echo $abnormalResult['abnormal_type_name']; ?>
						</td>
						<td class="xl67">
							责任方：
						</td>
						<td class="xl72" width="146">
						<?php if($abnormalResult['responsible_party']==4): ?>
					    <?php echo mb_substr($abnormalResult['supplier_name'],0,10); else: ?>
							<?php echo $baseConfig['abnormal']['responsible_party'][$abnormalResult['responsible_party']]; endif; ?>
							
						</td>
					</tr>
					
					<tr>
						<td colspan="7" height="30" class="xl141">
							<span class="font5">责任方：&nbsp;&nbsp; </span><span class="font0">&nbsp;(&nbsp;
		  )车辆&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; （ ）专线&nbsp; </span><span class="font5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="font0">（&nbsp; ）员工&nbsp;&nbsp;&nbsp;&nbsp;
		  （&nbsp; ）其他，注明：</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="30" class="xl71">
							品名
						</td>
						<td class="xl73">
							单位
						</td>
						<td class="xl73">
							数量
						</td>
						<td class="xl67">
							单价
						</td>
						<td class="xl67">
							未税价格
						</td>
						<td class="xl67">
							理赔金额（元）
						</td>
					</tr>
					
					<tr>
						<td colspan="2" height="29" class="xl70" width="182">
						<?php echo implode("/",array_column($abnormalResult['orders_goods'],'goods_name'));  ?>
						
						</td>
						<td class="xl68" width="91">
							
						</td>
						<td class="xl68" width="81">
							<?php echo $abnormalResult['abnormal_count']; ?>
						</td>
						<td class="xl64">
							
						</td>
						<td class="xl74">
							
						</td>
						<td class="xl74">
						
						<?php echo $abnormalResult['supplier_abnormal_money']; ?>
						
						</td>
					</tr>
				
					
				
					<tr>
						<td colspan="2" height="29" class="xl132">
							合计：
						</td>
						<td class="xl83" width="91">
							
						</td>
						<td class="xl84" width="81">
							
						</td>
						<td class="xl85" width="79">
							
						</td>
						<td class="xl86">
							
						</td>
						<td class="xl87">
							
						</td>
					</tr>
					<tr>
						<td height="22" class="xl71">
							实际扣款明细
						</td>
						<td class="xl88">
							
						</td>
						<td class="xl89" width="91">
							
						</td>
						<td class="xl90" width="81">
							
						</td>
						<td class="xl91" width="79">
							
						</td>
						<td class="xl92">
							
						</td>
						<td class="xl93">
							
						</td>
					</tr>
					<tr>
						<td height="29" class="xl94">
							发货方：
						</td>
						<td class="xl94" colspan="3">
							<?php echo $abnormalResult['orders_info']['send_goods_company']; ?>
						</td>
						
						<td class="xl94">
							差额部分：
						</td>
						<td class="xl116">
							
						</td>
						<td class="xl95">
							
						</td>
					</tr>
					
					
					<tr>
						<td colspan="4" style="position: relative;" height="150" class="xl134" width="354">
							<div style="position: absolute;top: 3px;left: 3px;height:128px;overflow:hidden">事件描述：<?php echo $abnormalResult['indemnity_event_description']; ?></div>
							<div height="22" class="xl102" width="91">
								填表人:<?php echo \think\Session::get('user.username'); ?>
							</div>
							<div class="xl103" width="91">
								日期:<?php echo date('Y-m-d'); ?>
							</div>
						</td>
						<td colspan="3" class="xl137" width="316" style="position: relative;">
							<div style="position: absolute;top: 3px;left: 3px;">事件核实：</div>
							<div height="22" class="xl102" width="91">
								填表人:
							</div>
							<div class="xl103" width="91">
								日期:
							</div>
						</td>
					</tr>
					
					
					<tr>
						<td colspan="7" height="100" class="xl127" style="position:relative">
							<div style="position:absolute;top:5px;left:5px">客服部/业务员处理意见：</div>
							
							<div style="position:absolute;bottom:5px;right:268px">签字：</div>
							<div style="position:absolute;bottom:5px;right:128px">日期：</div>
						</td>
					</tr><tr>
						<td colspan="7" height="100" class="xl127" style="position:relative">
							<div style="position:absolute;top:5px;left:5px">运营部/采购部处理意见：</div>
							
							<div style="position:absolute;bottom:5px;right:268px">签字：</div>
							<div style="position:absolute;bottom:5px;right:128px">日期：</div>
						</td>
					</tr>
					<tr>
						<td colspan="7" height="100" class="xl127" style="position:relative">
							<div style="position:absolute;top:5px;left:5px">总经办处理意见：</div>
							
							<div style="position:absolute;bottom:5px;right:268px">签字：</div>
							<div style="position:absolute;bottom:5px;right:128px">日期：</div>
						</td>
					</tr>
					<tr>
						<td colspan="7" height="100" class="xl127" style="position:relative">
							<div style="position:absolute;top:5px;left:5px">财务部意见：</div>
							
							<div style="position:absolute;bottom:5px;right:268px">签字：</div>
							<div style="position:absolute;bottom:5px;right:128px">日期：</div>
						</td>
					</tr>
					
					
					
					
					<tr>
						<td colspan="7" height="64" align="left" class="xl122">
							备用：
						</td>
					</tr>
				</tbody>
			</table>
			
	
	</div>
	
	</body>
</html>