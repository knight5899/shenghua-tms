<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:76:"/var/www/html/test_erp/public/../application/index/view/equipment/index.html";i:1658299220;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:77:"/var/www/html/test_erp/application/index/view/public/left_equipment_menu.html";i:1658299220;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>

<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo $language_tag['index_public_homepage']; ?></title>
    <link rel="stylesheet" href="/static/css/index.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">

    <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
    <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
    <dl class="layui-nav-child">
        <dd <?php if(in_array(($function_name), explode(',',"equipmentManage"))): ?> class="layui-this"<?php endif; ?>><a href="/equipment/equipmentManage">智能设备</a></dd>
    </dl>
    </li>
</ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a><?php echo $language_tag['index_public_homepage']; ?></a>
                        <a><cite><?php echo $language_tag['index_index_controlPanel']; ?></cite></a>
                    </span>
                </div>
            </div>
            <div class="table-nont">
                <div class="control-top">
                    <div class="layui-row layui-col-space10">
                        <div class="layui-col-md6">
                            <div class="layui-row layui-col-space10">
                                <div class="layui-col-md3" >
                                    <div class="bg-one bg-one-img">
                                        <a href="/product/ShowPlanTour">
                                            <div><?php echo $language_tag['index_index_receiveVisitorsToday']; ?>(<?php echo $language_tag['index_index_people']; ?>)</div>
                                            <h4 id="today_customer_count">0</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="layui-col-md3">
                                    <div class="bg-two bg-one-img">
                                        <a href="/product/ShowPlanTour">
                                            <div><?php echo $language_tag['index_index_numberOfGroupsOutToday']; ?></div>
                                            <h4 id="today_team_product_count">0</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="layui-col-md3">
                                    <div class="bg-three bg-img-edu">
                                        <a href="/branchcompany/showCompanyOrderManage">
                                            <div><?php echo $language_tag['index_index_turnoverToday']; ?>(RMB)</div>
                                            <h4 id="today_receivable">0</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="layui-col-md3">
                                    <div class="bg-four bg-one-img">
                                        <a href="/product/ShowPlanTour">
                                            <div><?php echo $language_tag['index_index_receiveVisitorsThisMonth']; ?>(<?php echo $language_tag['index_index_people']; ?>)</div>
                                            <h4 id="month_customer_count">0</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md6">
                            <div class="layui-row layui-col-space10">
                                <div class="layui-col-md3 bg-img-tuan" >
                                    <div class="bg-one bg-one-img">
                                        <a href="product/ShowPlanTour">
                                            <div><?php echo $language_tag['index_index_numberOfToursThisMonth']; ?></div>
                                            <h4 id="month_team_product_count">0</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="layui-col-md3">
                                    <div class="bg-two bg-img-edu">
                                        <a href="/branchcompany/showCompanyOrderManage">
                                            <div><?php echo $language_tag['index_index_turnoverThisMonth']; ?>(RMB)</div>
                                            <h4 id="month_receivable">0</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="layui-col-md3">
                                    <div class="bg-three bg-img-ying">
                                        <a href="finance/showReceivableManage">
                                            <div><?php echo $language_tag['index_index_accountsReceivable']; ?>(RMB)</div>
                                            <h4 id="all_receivable">0</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="layui-col-md3">
                                    <div class="bg-four bg-img-yishou">
                                        <a href="finance/showReceivableManage">
                                            <div><?php echo $language_tag['index_index_accountsReceived']; ?>(RMB)</div>
                                            <h4 id="all_receivable_info">0</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--top-->
                <div class="control-chart-one">
                    <div class="layui-row layui-col-space10">
                        <div class="layui-col-md8 control-oneChart-left">
                            <div>
                                <h4 class="clear">
                                    <div class="fl"><?php echo $language_tag['index_index_passengerStatisticsTable']; ?></div>
                                    <div class="fr layui-btn-group one-button">
                                        <button class="layui-btn layui-btn-xs layui-btn-primary index-one-button" onclick="chartList('7','chartOne',this)"><?php echo $language_tag['index_index_last_7_days']; ?></button>
                                        <button class="layui-btn layui-btn-xs layui-btn-primary" onclick="chartList('30','chartOne',this)"><?php echo $language_tag['index_index_last_30_days']; ?></button>
                                        <button class="layui-btn layui-btn-xs layui-btn-primary" onclick="chartList('90','chartOne',this)"><?php echo $language_tag['index_index_last_90_days']; ?></button>
                                    </div>
                                </h4>
                                <div id="chartOne" style="width: 100%;height:340px;margin-top:25px">

                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md4 control-oneChart-right ">
                            <div class="contorl-boards">
                                <div class="contorl-boards-title"><?php echo $language_tag['index_index_systemAnnouncement']; ?></div>
                                <div class="contorl-boards-box" id="contorl-boards-box">
                                    <?php if(is_array($InStationLetterList) || $InStationLetterList instanceof \think\Collection || $InStationLetterList instanceof \think\Paginator): $i = 0;$__LIST__ = is_array($InStationLetterList) ? array_slice($InStationLetterList,0,5, true) : $InStationLetterList->slice(0,5, true); if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                    <div class="clear">
                                        <div class="fl"><a href='<?php echo $v['url']; ?>'><?php echo $v['content']; ?></a></div>
                                        <div class="fr"><?php echo date('Y-m-d H:i:s',$v['create_time']); ?></div>
                                    </div>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--收客统计////系统公告-->
                <div class="control-chart-one">
                    <div class="layui-row layui-col-space10">
                        <div class="layui-col-md8 control-oneChart-left">
                            <div>
                                <h4 class="clear">
                                    <div class="fl"><?php echo $language_tag['index_index_statisticsDorecastOfSales']; ?></div>
                                    <div class="fr layui-btn-group two-button">
                                        <button class="layui-btn layui-btn-xs layui-btn-primary" onclick="chartList('7-2','chartTwo',this)"><?php echo $language_tag['index_index_last_7_days']; ?></button>
                                        <button class="layui-btn layui-btn-xs layui-btn-primary index-two-button" onclick="chartList('30-2','chartTwo',this)"><?php echo $language_tag['index_index_last_30_days']; ?></button>
                                        <button class="layui-btn layui-btn-xs layui-btn-primary" onclick="chartList('90-2','chartTwo',this)"><?php echo $language_tag['index_index_last_90_days']; ?></button>
                                    </div>
                                </h4>
                                <div id="chartTwo" style="width: 100%;height:340px;margin-top:25px">

                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md4 control-oneChart-right ">
                            <div class="contorl-boards">
                                <div class="contorl-boards-title"><?php echo $language_tag['index_index_myApproval']; ?></div>
                                <div class="contorl-boards-box" id="contorl-boards-box2">
                                    <div class="contorl-boards-list clear">
                                        <div class="fl"><span class="list-bg-one"></span><?php echo $language_tag['index_index_pendingMyApproval']; ?></div>
                                        <div class="fr"><i>0</i>(<?php echo $language_tag['index_index_number']; ?>)</div>
                                    </div>
                                    <div class="contorl-boards-list clear">
                                        <div class="fl"><span class="list-bg-two"></span><?php echo $language_tag['index_index_iStartedIt']; ?></div>
                                        <div class="fr"><i>0</i>(<?php echo $language_tag['index_index_number']; ?>)</div>
                                    </div>
                                    <div class="contorl-boards-list clear">
                                        <div class="fl"><span class="list-bg-three"></span><?php echo $language_tag['index_index_approved']; ?></div>
                                        <div class="fr"><i>0</i>(<?php echo $language_tag['index_index_number']; ?>)</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--销售额///审批-->
                <div class="control-last">
                    <div class="layui-row layui-col-space10">
                        <div class="layui-col-md8 control-last-list">
                            <div>
                                <div class="contorl-boards-title clear"><span class="fl"><?php echo $language_tag['index_index_serviceReminder']; ?></span><span class="fr layui-icon layui-icon-more-vertical"></span></div>
                                <!--<div class="contorl-boards-box contorl-last-scroll">-->
                                <div id="service-reminder" class="table-nont" style="height:365px;overflow: auto;padding-top:10px;">
                                    <!--<div class="clear">
                                        <a href="#">
                                            <div class="fl">aaaaaaaaaaaa</div>
                                            <div class="fr"><?php echo $language_tag['index_reminderManagement_allInStationLetter_unread']; ?></div>
                                            <div class="fr">2018-06-01</div>
                                        </a>
                                    </div>-->
                                    <!--  <?php if(empty($service_reminder) || (($service_reminder instanceof \think\Collection || $service_reminder instanceof \think\Paginator ) && $service_reminder->isEmpty())): ?>
                                          <div class="index-none"><?php echo $language_tag['index_public_noData']; ?></div>
                                      <?php else: ?>
                                      <table class="layui-table">
                                          <thead>
                                          <tr>
                                              <th><?php echo $language_tag['index_public_finish']; ?></th>
                                              <th><?php echo $language_tag['index_finance_showReceivableManage_order_number']; ?></th>
                                              <th>事件</th>
                                              <th><?php echo $language_tag['index_index_email_template']; ?></th>
                                              <th><?php echo $language_tag['index_public_send']; ?></th>
                                              <th><?php echo $language_tag['index_index_remind_date']; ?></th>
                                              <th><?php echo $language_tag['index_index_remind_object']; ?></th>
                                              <th><?php echo $language_tag['index_product_showRouteTemplateManage_resources_enclosure']; ?></th>
                                              <th><?php echo $language_tag['index_public_mark']; ?></th>
                                          </tr>
                                          </thead>

                                          <?php foreach($service_reminder as $value): ?>
                                          <tr>
                                              <th><input type="checkbox" value="<?php echo $value['id']; ?>" title=""></th>
                                              <th><?php echo $value['company_order_number']; ?></th>
                                              <th><?php echo $value['operation_name']; ?></th>
                                              <th><?php echo $value['email_template_id']; ?></th>
                                              <th>
                                                  <?php if($value['is_email_sent'] == 1): ?>
                                                  <button class="layui-btn layui-btn-sm hover-edit layui-btn-primary">已发送</button>
                                                  <?php elseif($value['is_email_sent'] == 0): ?>
                                                  <a href=''><button class="layui-btn layui-btn-sm hover-details layui-btn-primary"><?php echo $language_tag['index_public_send']; ?></button></a>
                                                  <?php endif; ?>
                                              </th>
                                              <th><?php echo $value['remind_at']; ?></th>
                                              <th><?php echo $value['remind_to_nickname']; ?></th>
                                              <th><?php echo $value['remind_to']; ?></th>
                                              <th><?php echo $value['remark']; ?></th>
                                          </tr>
                                          <?php endforeach; endif; ?>
                                          </tbody>

                                      </table>-->
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md4 control-last-list">
                            <div>
                                <div class="contorl-boards-title clear"><span class="fl"><?php echo $language_tag['index_index_uncollectedAccountsRanking']; ?></span><span class="fr layui-icon layui-icon-more-vertical"></span></div>
                                <div class="contorl-boards-box contorl-last-scroll" id="miss_payment_company">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>

</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>

<script>

</script>

</body>
</html>