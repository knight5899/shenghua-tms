<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:72:"/var/www/html/test_erp/public/../application/index/view/index/index.html";i:1661510456;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>

<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo $language_tag['index_public_homepage']; ?></title>
    <link rel="stylesheet" href="/static/css/index.css">
    <style>
        .layui-tab .layui-this{
            border: 0px solid #00b7ee;
            border-bottom: 3px solid black;
            background: #00b7ee;
            color: white;
        }
		.clearStorage{
		    position: fixed;
    color: wheat;
    background-color: red;
    z-index: 100000;
    right: 10px;
    top: 44px;
    padding: 5px;
    border-radius: 5px;
		}
    </style>
</head>
<body class="layui-layout-body">

<a href="javascript:;" onclick="clearStorage()" class="clearStorage">清除缓存</a>

<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <div title="菜单收缩" class="kit-side-fold"><i class="layui-icon layui-icon-spread-left" aria-hidden="true"></i></div>
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">
        <div class="content_body">

            <div class="layui-tab" lay-filter="demo" lay-allowclose="true">
<!--                <ul class="layui-tab-title">-->
<!--                    <li class="layui-this" lay-id="11">控制面板</li>-->
<!--                </ul>-->
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <div class="body-top">
                            <div class='layui-form-item'>
                            </div>
                        </div>
                        <div class="table-nont">
                            <div class="control-top">
                                <div class="layui-row layui-col-space10">
                                    <div class="layui-col-md6">
                                        <div class="layui-row layui-col-space10">
                                            <div class="layui-col-md3" >
                                                <div class="bg-one">
                                                    <a href="javscript:void(0)">
                                                        <div>预估当月收入</div>
                                                        <h4 id="today_customer_count"><?php echo $result['now_month_shouru']; ?></h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="layui-col-md3">
                                                <div class="bg-two ">
                                                    <a href="javscript:void(0)">
                                                        <div>预估当月成本</div>
                                                        <h4 id="today_team_product_count"><?php echo $result['now_month_chengben']; ?></h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="layui-col-md3">
                                                <div class="bg-three ">
                                                    <a href="javscript:void(0)">
                                                        <div>预估当月净收入</div>
                                                        <h4 id="today_receivable"><?php echo $result['now_month_jingshouru']; ?></h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="layui-col-md3">
                                                <div class="bg-four">
                                                    <a href="javscript:void(0)">
                                                        <div>预估当年收入</div>
                                                        <h4 id="month_customer_count"><?php echo $result['shouru']; ?></h4>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-col-md6">
                                        <div class="layui-row layui-col-space10">
                                            <!--
                                                <div class="layui-col-md3 bg-img-tuan" >
                                                    <div class="bg-one ">
                                                        <a href="javscript:void(0)">
                                                            <div>当年订单数</div>
                                                            <h4 id="month_team_product_count"><?php echo $orderNowYearCount; ?></h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="layui-col-md3">
                                                    <div class="bg-two ">
                                                        <a href="javscript:void(0)">
                                                            <div>当年发运数</div>
                                                            <h4 id="month_receivable"><?php echo $shipmentNowYearCount; ?></h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                -->
                                            <div class="layui-col-md3">
                                                <div class="bg-three ">
                                                    <a href="javscript:void(0)">
                                                        <div>预估当年成本</div>
                                                        <h4 id="all_receivable"><?php echo $result['chengben']; ?></h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="layui-col-md3">
                                                <div class="bg-four">
                                                    <a href="javscript:void(0)">
                                                        <div>预估当年净利润</div>
                                                        <h4 id="all_receivable_info"><?php echo $result['jingshouru']; ?></h4>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--top-->
                            <div class="control-chart-one">
                                <div class="layui-row layui-col-space10">
                                    <div class="layui-col-md8 control-oneChart-left">
                                        <div>
                                            <h4 class="clear">
                                                <div class="fl"><?php echo $language_tag['index_index_passengerStatisticsTable']; ?></div>
                                                <div class="fr layui-btn-group one-button">
                                                    <button class="layui-btn layui-btn-xs layui-btn-primary index-one-button" onclick="chartList('7','chartOne',this)"><?php echo $language_tag['index_index_last_7_days']; ?></button>
                                                    <button class="layui-btn layui-btn-xs layui-btn-primary" onclick="chartList('30','chartOne',this)"><?php echo $language_tag['index_index_last_30_days']; ?></button>
                                                    <button class="layui-btn layui-btn-xs layui-btn-primary" onclick="chartList('90','chartOne',this)"><?php echo $language_tag['index_index_last_90_days']; ?></button>
                                                </div>
                                            </h4>
                                            <div id="chartOne" style="width: 100%;height:340px;margin-top:25px">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-col-md4 control-oneChart-right ">
                                        <div class="contorl-boards">
                                            <div class="contorl-boards-title">系统公告</div>
                                            <div class="contorl-boards-box" id="contorl-boards-box">

                                                <div class="clear">
                                                    <div class="fl"><a href=''></a></div>
                                                    <div class="fr"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--收客统计////系统公告-->
                            <div class="control-chart-one">
                                <div class="layui-row layui-col-space10">
                                    <div class="layui-col-md8 control-oneChart-left">
                                        <div>
                                            <h4 class="clear">
                                                <div class="fl"><?php echo $language_tag['index_index_statisticsDorecastOfSales']; ?></div>
                                                <div class="fr layui-btn-group two-button">
                                                    <button class="layui-btn layui-btn-xs layui-btn-primary" onclick="chartList('7-2','chartTwo',this)"><?php echo $language_tag['index_index_last_7_days']; ?></button>
                                                    <button class="layui-btn layui-btn-xs layui-btn-primary index-two-button" onclick="chartList('30-2','chartTwo',this)"><?php echo $language_tag['index_index_last_30_days']; ?></button>
                                                    <button class="layui-btn layui-btn-xs layui-btn-primary" onclick="chartList('90-2','chartTwo',this)"><?php echo $language_tag['index_index_last_90_days']; ?></button>
                                                </div>
                                            </h4>
                                            <div id="chartTwo" style="width: 100%;height:340px;margin-top:25px">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-col-md4 control-oneChart-right ">
                                        <div class="contorl-boards">
                                            <div class="contorl-boards-title"><?php echo $language_tag['index_index_myApproval']; ?></div>
                                            <div class="contorl-boards-box" id="contorl-boards-box2">
                                                <div class="contorl-boards-list clear">
                                                    <div class="fl"><span class="list-bg-one"></span><?php echo $language_tag['index_index_pendingMyApproval']; ?></div>
                                                    <div class="fr"><i>0</i>(<?php echo $language_tag['index_index_number']; ?>)</div>
                                                </div>
                                                <div class="contorl-boards-list clear">
                                                    <div class="fl"><span class="list-bg-two"></span><?php echo $language_tag['index_index_iStartedIt']; ?></div>
                                                    <div class="fr"><i>0</i>(<?php echo $language_tag['index_index_number']; ?>)</div>
                                                </div>
                                                <div class="contorl-boards-list clear">
                                                    <div class="fl"><span class="list-bg-three"></span><?php echo $language_tag['index_index_approved']; ?></div>
                                                    <div class="fr"><i>0</i>(<?php echo $language_tag['index_index_number']; ?>)</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--销售额///审批-->
                            <div class="control-last">
                                <div class="layui-row layui-col-space10">
                                    <div class="layui-col-md8 control-last-list">
                                        <div>
                                            <div class="contorl-boards-title clear"><span class="fl"><?php echo $language_tag['index_index_serviceReminder']; ?></span><span class="fr layui-icon layui-icon-more-vertical"></span></div>
                                            <!--<div class="contorl-boards-box contorl-last-scroll">-->
                                            <div id="service-reminder" class="table-nont" style="height:365px;overflow: auto;padding-top:10px;">
                                                <!--<div class="clear">
                                                    <a href="#">
                                                        <div class="fl">aaaaaaaaaaaa</div>
                                                        <div class="fr"><?php echo $language_tag['index_reminderManagement_allInStationLetter_unread']; ?></div>
                                                        <div class="fr">2018-06-01</div>
                                                    </a>
                                                </div>-->
                                                <!--  <?php if(empty($service_reminder) || (($service_reminder instanceof \think\Collection || $service_reminder instanceof \think\Paginator ) && $service_reminder->isEmpty())): ?>
                                                      <div class="index-none"><?php echo $language_tag['index_public_noData']; ?></div>
                                                  <?php else: ?>
                                                  <table class="layui-table">
                                                      <thead>
                                                      <tr>
                                                          <th><?php echo $language_tag['index_public_finish']; ?></th>
                                                          <th><?php echo $language_tag['index_finance_showReceivableManage_order_number']; ?></th>
                                                          <th>事件</th>
                                                          <th><?php echo $language_tag['index_index_email_template']; ?></th>
                                                          <th><?php echo $language_tag['index_public_send']; ?></th>
                                                          <th><?php echo $language_tag['index_index_remind_date']; ?></th>
                                                          <th><?php echo $language_tag['index_index_remind_object']; ?></th>
                                                          <th><?php echo $language_tag['index_product_showRouteTemplateManage_resources_enclosure']; ?></th>
                                                          <th><?php echo $language_tag['index_public_mark']; ?></th>
                                                      </tr>
                                                      </thead>

                                                      <?php foreach($service_reminder as $value): ?>
                                                      <tr>
                                                          <th><input type="checkbox" value="<?php echo $value['id']; ?>" title=""></th>
                                                          <th><?php echo $value['company_order_number']; ?></th>
                                                          <th><?php echo $value['operation_name']; ?></th>
                                                          <th><?php echo $value['email_template_id']; ?></th>
                                                          <th>
                                                              <?php if($value['is_email_sent'] == 1): ?>
                                                              <button class="layui-btn layui-btn-sm hover-edit layui-btn-primary">已发送</button>
                                                              <?php elseif($value['is_email_sent'] == 0): ?>
                                                              <a href=''><button class="layui-btn layui-btn-sm hover-details layui-btn-primary"><?php echo $language_tag['index_public_send']; ?></button></a>
                                                              <?php endif; ?>
                                                          </th>
                                                          <th><?php echo $value['remind_at']; ?></th>
                                                          <th><?php echo $value['remind_to_nickname']; ?></th>
                                                          <th><?php echo $value['remind_to']; ?></th>
                                                          <th><?php echo $value['remark']; ?></th>
                                                      </tr>
                                                      <?php endforeach; endif; ?>
                                                      </tbody>

                                                  </table>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-col-md4 control-last-list">
                                        <div>
                                            <div class="contorl-boards-title clear"><span class="fl"><?php echo $language_tag['index_index_uncollectedAccountsRanking']; ?></span><span class="fr layui-icon layui-icon-more-vertical"></span></div>
                                            <div class="contorl-boards-box contorl-last-scroll" id="miss_payment_company">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
    <script id="demo" type="text/html">
        {{#  layui.each(d, function(index, item){ }}
        <div class="clear">
            <div class="fl">{{item.content}}</div>
            <div class="fr">{{item.date}}</div>
        </div>
        {{#  }); }}
    </script>
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script type="text/javascript" src="/static/javascript/index/echarts.js"></script>
<script type="text/javascript" src="/static/javascript/index/index.js"></script>
<!--<script>-->
<!--    layui.use('element', function() {-->
<!--        var $ = layui.jquery-->
<!--            , element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块-->

<!--        $('#left-nav').find('dd').each(function(index){-->
<!--            $(this).click(function () {-->
<!--                var title = $(this).attr('title');-->
<!--                var url = $(this).attr('url');-->

<!--                //新增一个Tab项-->
<!--                element.tabAdd('demo', {-->
<!--                    title: title //用于演示-->
<!--                    , content: '<iframe data-frameid="'+index+'" scrolling="auto" frameborder="0" src="'+url+'" style="width:100%;height:780px;"></iframe>'-->
<!--                    , id: new Date().getTime() //实际使用一般是规定好的id，这里以时间戳模拟下-->
<!--                })-->
<!--            });-->
<!--        });-->

<!--    });-->
<!--</script>-->
<script>
    var layer = layui.layer;
    var form = layui.form;
    var upload = layui.upload;

	function clearStorage(){
	
	localStorage.clear()
	layer.msg("清除成功")
	}


    $.ajax({
        type : 'post',
        url : '/operations/getgetServiceReminderListAjax',
        data:{},
        dataType: 'json',
        success:function (data) {
            if (data.code == 200 && data.data.length != 0) {
                var user_arr = data.data[0].user_arr;
                console.log(data.data);
                /*业务提醒*/
                var html = "";
                html += '<table class="layui-table layui-form" style="margin:0px 10px;table-layout:fixed;"><colgroup><col width="60px" /><col width="150px" /><col width="150px" /><col width="150px" /><col width="150px" /><col width="150px" /><col width="150px" /><col width="200px" /><col width="150px" /><col width="150px" /></colgroup><thead><tr><th><?php echo $language_tag['index_public_finish']; ?></th><th><?php echo $language_tag['index_finance_showReceivableManage_order_number']; ?></th><th><?php echo $language_tag['index_index_incident']; ?></th><th><?php echo $language_tag['index_index_email_template']; ?></th><th><?php echo $language_tag['index_public_send']; ?></th><th><?php echo $language_tag['index_index_remind_date']; ?></th><th><?php echo $language_tag['index_index_remind_object']; ?></th><th><?php echo $language_tag['index_product_showRouteTemplateManage_resources_enclosure']; ?></th><th><?php echo $language_tag['index_product_loadFlight_upload_accessory']; ?></th><th><?php echo $language_tag['index_public_mark']; ?></th></tr></thead><tbody>';

                for(var i=0;i<data.data.length;i++) {
                    //完成
                    html +='<th><input type="checkbox" lay-skin="primary" lay-filter="company_order_operations_status" value="' + data.data[i].id + '" title=""></th>';

                    html +='<th>'+ data.data[i].company_order_number + '</th>';
                    //事件
                    html +='<th>'+data.data[i].operation_name+ '</th><th>';

                    //邮件模板
                    if (data.data[i].operations_email_templates.length != 0)
                    {
                        var email_template_id = data.data[i].email_template_id;
                        html += '<select lay-filter="operations-email_template_id" data-info="' + data.data[i].id + '" >';
                        for(var t=0;t<data.data[i].operations_email_templates.length;t++) {
                            var select_email_template_id = data.data[i].operations_email_templates[t].id;
                            var selected  = select_email_template_id == email_template_id ? 'selected = "selected"' : '';
                            html += '<option value="' + select_email_template_id + '" '+ selected +'>' + data.data[i].operations_email_templates[t].name + '</option>'
                        }
                        html += '</select>';
                    }
                    html += '</th><th>';

                    //发送
                    if (data.data[i].is_email_sent == 1) {
                        html += '<button class="layui-btn layui-btn-sm hover-edit layui-btn-primary dispatch-operationsEmail" data-info="' + data.data[i].id + '"><?php echo $language_tag['index_public_send']; ?></button>';
                    }


                    //提醒日期
                    html +='</th><th>' + data.data[i].remind_at + '</th><th>';

                    //提醒对象
                    if (user_arr.length != 0)
                    {
                        var remind_to_id = data.data[i].remind_to;

                        html += '<select lay-filter="operations-user_id" data-info="' + data.data[i].id + '" >';
                        for(var u = 0 ; u < user_arr.length ; u++) {
                            var select_remind_to_id = user_arr[u].user_id;
                            var selected  = remind_to_id == select_remind_to_id ? 'selected = "selected"' : '';
                            html += '<option value="' + select_remind_to_id + '" '+ selected +'>' + user_arr[u].nickname + '</option>'
                        }
                        html += '</select>';
                    }

                    //附件
                    html +='</th><th id="upload-' + data.data[i].id + '">';
                    if (data.data[i].attachments.length != 0) {
                        for(var j=0;j<data.data[i].attachments.length;j++) {
                            html += '<div><a href="' + data.data[i].attachments[j].savepath + '">'+ data.data[i].attachments[j].name +'</a><a class="layui-btn layui-btn-xs layui-btn-primary del-attachments" data-info="'+ data.data[i].attachments[j].id +'"><?php echo $language_tag['index_public_del']; ?></a></div>'
                        }
                    }

                    //上传附件
                    html += '</th><th><button class="layui-btn layui-btn-sm hover-details layui-btn-primary upload" data-info=' + data.data[i].id  + ' data-order_id='+ data.data[i].company_order_id +'><?php echo $language_tag['index_public_upload']; ?></button></th>';

                    //备注
                    html +='<th><textarea class="layui-textarea  operations-remark" data-info="' + data.data[i].id + '">' + data.data[i].remark + '</textarea></th>';

                    html +='</tr>';
                }
                html += '</tbody></table>';
                $("#service-reminder").html(html);
                upStatus(); //修改订单状态
                uploadReminder();//上传附件
                del_company_order_operations_attachments(); //附件删除
                edit_email_template_id(); //修改待办邮件模板
                edit_remark();  //修改代办备注
                editRemindTo(); //修改订单提醒谁
                dispatchOperationsEmail(); //待办邮件发送
                form.render();
            }
            else {
                $("#service-reminder").html('<div class="index-none"><?php echo $language_tag['index_public_noData']; ?></div>')
            }
        }
    });

    layui.use('element', function(){
        var element = layui.element;


    });

    var isShow = true;  //定义⼀个标志位
    $('.kit-side-fold').click(function(){
        //选择出所有的span，并判断是不是hidden
        $('.layui-nav-item span').each(function(){
            if($(this).is(':hidden')){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
        //判断isshow的状态
        if(isShow){
            $('.layui-side.layui-bg-black').width(50); //设置宽度
            $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
            //将footer和body的宽度修改
            $('.layui-body').css('left', 60+'px');
            $('.layui-footer').css('left', 60+'px');
            //将⼆级导航栏隐藏
            $('dd span').each(function(){
                $(this).hide();
            });
            //修改标志位
            isShow =false;
        }else{
            $('.layui-side.layui-bg-black').width(200);
            $('.kit-side-fold i').css('margin-right', '10%');
            $('.layui-body').css('left', 200+'px');
            $('.layui-footer').css('left', 200+'px');
            $('dd span').each(function(){
                $(this).show();
            });
            isShow =true;
        }
    });

    //修改新单待办状态
    function upStatus(){
        form.on('checkbox(company_order_operations_status)', function(data){
            var s = 1;
            if(data.elem.checked){
                var s = 2;
            }
            var company_order_operations_id = data.value;
            $.post('/operations/upStatus',{company_order_operations_id:company_order_operations_id,status:s},function(a){
                layer.closeAll('loading');
            });

        });
    }

    function uploadReminder() {
        $('.upload').each(function(){
            alert
            upload.render({
                elem: this
                ,url: '/operations/upload_operations_attachments/?company_order_operations_id='+$(this).data('info')+'&company_order_id='+$(this).data('order_id')
                ,accept: 'file'
                ,done: function(res){
                    console.log(res)
                    if(res.code == 200){
                        var h = '<div><a href="'+res.data+'" target="_blank">'+res.image_name+'</a>&nbsp;<a class="layui-btn layui-btn-xs layui-btn-primary del-attachments" data-info="'+res.data+'"><?php echo $language_tag['index_public_del']; ?></a></div>';
                        console.log('#upload-'+res.get.company_order_operations_id);
                        $('#upload-'+res.get.company_order_operations_id).append(h);
                        del_company_order_operations_attachments(); //附件删除
                    }
                }
            });
        });
    }


    var d_attachments;
    //附件删除
    function del_company_order_operations_attachments(){
        $('.del-attachments').on('click',function(){

            layer.load(2);
            var company_order_operations_attachments_id = $(this).data('info');
            console.log(company_order_operations_attachments_id);
            d_attachments  = $(this);
            $.post('/operations/delCompanyOrderOperationsAttachmentsAjax',{company_order_operations_attachments_id:company_order_operations_attachments_id},function(a){
                d_attachments.parent().remove();
                layer.closeAll('loading');
            });
            form.render();

        });
    }

    //修改待办邮件模板
    function edit_email_template_id(){
        form.on('select(operations-email_template_id)', function(data){

            var company_order_operations_id = $(data.elem).data('info');

            var v = data.value;
            layer.load(2);
            $.post('/operations/upEmailTemplateIdAjax',{company_order_operations_id:company_order_operations_id,email_template_id:v},function(a){
                layer.closeAll('loading');
            });

        });
    }

    //修改待办备注
    function edit_remark(){
        $('.operations-remark').on('blur',function(){
            var remark = $(this).val();
            var company_order_operations_id = $(this).data('info');
            layer.load(2);
            $.post('/operations/upRemarkAjax',{company_order_operations_id:company_order_operations_id,remark:remark},function(a){
                layer.closeAll('loading');
            });

        });
    }


    //修改订单提醒谁
    function editRemindTo(){
        form.on('select(operations-user_id)', function (data) {
            var remind_to = data.value;
            var company_order_operations_id = $(data.elem).data('info');
            layer.load(2);
            $.post('/operations/upRemindTo',{company_order_operations_id:company_order_operations_id,remind_to:remind_to},function(a){
                layer.closeAll('loading');
            });

        });
    }



    //待办邮件发送
    function dispatchOperationsEmail(){
        $('.dispatch-operationsEmail').on('click',function(){
            var company_order_operations_id = $(this).data('info');
            layer.open({
                title: '发送邮件'
                ,type: 2
                ,content: '/operations/dispatchOperationsEmail?company_order_operations_id='+company_order_operations_id+'&company_order_number=<?php echo $_GET["company_order_number"]; ?>'
                ,area: ['800px', '500px']
            });
        });

    }

    //关闭发送邮件窗口
    function close_emali(){
        layer.closeAll();
    }
var user_value={};
    <?php if(is_array($userconfig) || $userconfig instanceof \think\Collection || $userconfig instanceof \think\Paginator): if( count($userconfig)==0 ) : echo "" ;else: foreach($userconfig as $key=>$vo): ?>
	user_value=<?php echo $vo['value']; ?>;
	localStorage.setItem('<?php echo $vo['key']; ?>',JSON.stringify(user_value));
	<?php endforeach; endif; else: echo "" ;endif; ?>
	

	
</script>

</body>
</html>