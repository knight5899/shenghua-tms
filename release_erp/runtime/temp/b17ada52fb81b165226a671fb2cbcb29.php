<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:79:"/var/www/html/test_erp/public/../application/index/view/order/order_manage.html";i:1665643657;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>运单</title>
    <style>

        .layui-table-fixed-r .layui-table-cell {
            overflow: visible;
        }

      

        .xm-select {
            min-height: 25px !important;
        }

        .xm-select-title {
            min-height: 25px !important;
        }

        .xm-select--suffix .xm-input {
            height: 25px !important;
        }

        .pickup_time {
            padding-left: 0;
            width: 65px;
        }


    </style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <div title="菜单收缩" class="kit-side-fold"><i class="layui-icon layui-icon-spread-left" aria-hidden="true"></i>
            </div>
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>运单管理</a>
                        <a><cite>运单</cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
					<div class="layui-tab">
  <ul class="layui-tab-title">
    <li   <?php if(\think\Request::instance()->get('status') == null): ?> class="layui-this"  <?php endif; ?> ><a href='/order/showOrderManage?multi_order_status=1'>正常运单</a></li>
    <li <?php if(\think\Request::instance()->get('status') != null): ?> class="layui-this"  <?php endif; ?>><a href='/order/delOrderManage?status=0'>已删除运单</a></li>


  </ul>

</div> 
				
                    <form class="layui-form" method='get' action='/order/showOrderManage'>
					<?php if(\think\Request::instance()->get('status') != null): ?>  <input type="hidden" name="status" value="0"></input> <?php endif; ?>
					
                        <div class="layui-row" style="    margin: 5px;">
                            <div class="layui-col-md2">
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">项目名称:</label>
                                    <div class="layui-input-inline" style="width:119px">
                                        <select name="multi_project_id" lay-verify="required" id='department_id'
                                                lay-verify="required" xm-select="select1" xm-select-direction="down"
                                                xm-select-search="/order/getProjectAjax" xm-select-search-type="dl">
                                            <?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$vo): ?>
                                            <option value="<?php echo $vo['project_id']; ?>" selected><?php echo $vo['project_name']; ?></option>
                                            <?php endforeach; endif; else: echo "" ;endif; ?>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="layui-col-md2">
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">运单号:</label>
                                    <div class="layui-input-inline" style="width:119px">
                                        <input type="text" id="" name="orders_number" maxlength="300" autocomplete="off"
                                               value="<?php echo \think\Request::instance()->get('orders_number'); ?>" placeholder="" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md2">
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">运单状态:</label>
                                    <div class="layui-input-inline" style="width:119px">
                                        <select id='order_status' name='multi_order_status' xm-select="select2">
                                            <option value=''>--全部--</option>
                                            <?php if(is_array($baseConfig['order']['order_status']) || $baseConfig['order']['order_status'] instanceof \think\Collection || $baseConfig['order']['order_status'] instanceof \think\Paginator): if( count($baseConfig['order']['order_status'])==0 ) : echo "" ;else: foreach($baseConfig['order']['order_status'] as $key=>$vo1): ?>
                                            <option value='<?php echo $vo1['id']; ?>'
                                            <?php if(isset($_GET['multi_order_status']))if( in_array($vo1['id'],explode(",",$_GET['multi_order_status'])) )echo "selected=\"selected\"";  ?>
                                            ><?php echo $vo1['order_status_name']; ?></option>

                                            <?php endforeach; endif; else: echo "" ;endif; ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <!--						<div class="layui-col-md2">-->
                            <!--						    <div class="layui-input-inline">-->
                            <!--						      -->
                            <!--							      		<label class="layui-form-label">免运费:</label>-->
                            <!--							      			<div class="layui-input-inline" style="width:85px">-->
                            <!--								  			<select name="is_free" id='is_free'  lay-verify="required"  lay-filter="is_free">-->
                            <!--											<option value="" >全部</option>-->
                            <!--										      <option value="0" <?php if(\think\Request::instance()->get('is_free') == '0'): ?>selected<?php endif; ?>>否</option>-->
                            <!--											  <option value="1" <?php if(\think\Request::instance()->get('is_free')==1): ?>selected<?php endif; ?>>是</option>-->
                            <!--												-->
                            <!--										</select>	 </div>-->
                            <!--							    	-->
                            <!--						    </div>-->
                            <!--					    -->
                            <!--						 </div>-->
                            <div class="layui-col-md2">
                                <div class="layui-input-inline">

                                    <label class="layui-form-label">客户编号:</label>
                                    <div class="layui-input-inline" style="width:119px">
                                        <input type="text" id="" name="customer_order_number" maxlength="300"
                                               autocomplete="off" value="<?php echo \think\Request::instance()->get('customer_order_number'); ?>"
                                               placeholder="" class="layui-input">
                                    </div>

                                </div>

                            </div>
                            <div class="layui-col-md2">

                                <div class="layui-input-inline">
                                    <label class="layui-form-label">运单日期:</label>
                                    <div class="layui-input-inline">
                                        <div class="layui-input-inline" style="width: 65px;">

                                            <input type="text" id="pickup_time" name="pickup_time" maxlength="300"
                                                   autocomplete="off" value="<?php echo \think\Request::instance()->get('pickup_time'); ?>"
                                                   placeholder="开始日期" class="layui-input pickup_time">
                                        </div>

                                        <div style="width: 65px;" class="layui-input-inline">
                                            <input type="text" name="pickup_time_end" type="text"
                                                   class="pickup_time layui-input" value="<?php echo \think\Request::instance()->get('pickup_time_end'); ?>"
                                                   placeholder="结束日期">
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="layui-col-md2">

                                <div class="layui-input-inline" >
                                    <label class="layui-form-label">分公司:</label>
                                    <div class="layui-input-inline" style="width: 87px;">
                                         <select id='company_id' name='choose_company_id' >
                                            <option value=''>--全部--</option>
											<?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$v): ?>
												<option value="<?php echo $v['company_id']; ?>" 
												
												<?php if(\think\Request::instance()->get('choose_company_id') != ''): if(\think\Request::instance()->get('choose_company_id') == $v['company_id']): ?>selected <?php endif; else: if(\think\Session::get('user.company_id') == $v['company_id']): ?>selected <?php endif; endif; ?>
												>

												<?php echo $v['company_name']; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>                                           
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="layui-row" style="    margin: 5px;">
                           
                            <div class="layui-col-md2">
                                <div class="layui-input-inline">

                                    <label class="layui-form-label">收货方:</label>
                                    <div class="layui-input-inline">


                                        <input style="width:85px" type="text" autocomplete="off" class="layui-input"
                                               value="<?php echo \think\Request::instance()->get('accept_goods_company'); ?>" name="accept_goods_company"
                                               id="" value=""/>


                                    </div>

                                </div>
                            </div>
							
							<div class="layui-col-md2">

                                <div class="layui-input-inline" >
                                    <label class="layui-form-label">是否重要:</label>
                                    <div class="layui-input-inline" style="width: 87px;">
                                         <select id='follow_level' name='follow_level' >
                                            <option value=''>--全部--</option>
											
 <option value="0" <?php if(\think\Request::instance()->get('follow_level') == 0  && is_numeric(\think\Request::instance()->get('follow_level'))): ?>selected <?php endif; ?> >否</option>
 <option value="1" <?php if(\think\Request::instance()->get('follow_level') == 1): ?>selected <?php endif; ?> >是</option>
											                                          
                                        </select>
                                    </div>
                                </div>
                            </div>
                         

                            <div class="layui-col-md2" style="display:flex;justify-content: center;">
                                <button class="layui-btn nav-search search_button">搜索</button>
                                <a type="reset" href="/order/showOrderManage"
                                   class="layui-btn layui-btn-primary search_button">重置</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>


            <div class="content-bg">


                <div class="table-nont user-manage ">
                    <table id="language-table" lay-filter="language-table">

                    </table>
                    <script type="text/html" id="toolbarDemo" lay-filter="toolbar1">
                        <div class="layui-inline" lay-event="add"><i class="layui-icon layui-icon-add-1"></i></div>

                    </script>
                </div>

            </div>


        </div>
    </div>


</div>
<?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>
<script type="text/html" id="tool_bar">

<?php if(!is_numeric(\think\Request::instance()->get('status'))): ?>
    <span><a href='javascript:openAddOrder("/order/showOrderAdd");'><button class="layui-btn nav-add layui-btn-sm">新建运单</button></a></span>
    <span><a href='javascript:void();' lay-event="addImportant"><button
            class="layui-btn nav-add layui-btn-sm">重要数据</button></a></span>
    <span><a target="blank" href='/order/showOrderManage?download&<?php echo http_build_query($_GET); ?>'><button
            onclick="" class="layui-btn nav-add layui-btn-sm">下载</button></a></span>
    <span><button onclick="qianshou()" class="layui-btn nav-add layui-btn-sm">打印送货单</button></span>
    <!--<span><button onclick="addphoto()" class="layui-btn nav-add layui-btn-sm">上传拍照件</button></span>-->
    <span><button onclick="delorders()" class="layui-btn nav-add layui-btn-sm">删除</button></span>
    
<?php endif; ?>	
	<a class="layui-btn layui-btn-sm layui-btn-normal" id="clear"><i class="layui-icon layui-icon-delete"></i>清除缓存</a>
    <a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="save" >保存设置</a>
		  <a href="javascript:void(0);">已选中&nbsp;<span class="layui-badge" id="nums">0</span></a>
    <a href="javascript:void(0);" style='height: 30px;position: absolute;right: 230px;'>注:紫色为重要数据 红色字体为超时到达</a>


</script>
<?php endif; if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>

<script src='/static/javascript/system/base.js'></script>
<script src='/static/javascript/system/source.js'></script>
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script>

       function zhuanhuan(string) {
            var f = string.split(' ', 2);
            var d = (f[0] ? f[0] : '').split('-', 3);
            var t = (f[1] ? f[1] : '').split(':', 3);
            return (new Date(
                parseInt(d[0], 10) || null,
                (parseInt(d[1], 10) || 1) - 1,
                parseInt(d[2], 10) || null,
                parseInt(t[0], 10) || null,
                parseInt(t[1], 10) || null,
                parseInt(t[2], 10) || null
            )).getTime() / 1000;
        }
function timestampToTime(timestamp) {
        var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
        var Y = date.getFullYear()
        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1)
        var D = (date.getDate() < 10 ? '0'+(date.getDate()) : date.getDate())
       // var h = date.getHours() + ':';
       // var m = date.getMinutes() + ':';
       // var s = date.getSeconds();
        return Y+M+D;
    }
        function aa() {

            var tt = $('.true_time')
            for (var i = 0; i < tt.length; i++) {


                sv = tt.eq(i).parent().parent().prev().prev().find('div').eq(0).html();
				
                if (sv != '' && sv != null) {
                    var tv = $.trim(tt.eq(i).val())
				
                    if (tv != '' && tv != null) {

                        sv = tt.eq(i).parent().parent().prev().find('div').eq(0).html();
					
                        if (sv != '' && sv != null) {
					
								
                            if (timestampToTime(zhuanhuan(tv)) < timestampToTime(zhuanhuan(sv))) {

                                tt.eq(i).parent().parent().parent().css("color", "red");
                            } else {
                                tt.eq(i).parent().parent().parent().css("border-bottom", "0px solid white");
                            }


                        }
                    } else {
						
						var kkkk= Date.parse(new Date()) / 1000;
			
					
                        if (timestampToTime(kkkk) >timestampToTime(zhuanhuan(sv))) {
                            tt.eq(i).parent().parent().parent().css("color", "red");
                        } else {
                            tt.eq(i).parent().parent().parent().css("border-bottom", "0px solid white");
                        }
                    }


                }


            }
			
			
			
			lay('.true_time').each(function () {
                   var ins1= laydate.render({
                        elem: this
                        , trigger: 'click',
						
						done: function (value, date, endDate) { //监
                            var obj = $(this.elem[0])
                            var orders_number = $(obj).attr('key')
                            if ($(obj).attr('order_status') == 1) {
                                layer.msg("库存状态不能修改")
								$(obj).val("")
                                return false;
                            }
                            $.ajax({
                                url: '/order/updateOrdersByOrdersNumber',
                                data: {
                                    orders_number: orders_number,
                                    true_time: $(obj).val(),
									orderStatus:6
                                },
                                success: function (e) {

                                    //获取签一个时间


                                    layer.msg('成功');

                                    aa();
                                }


                            })
                        }

                    });


                })
				
				 lay('.departure_time').each(function () {
                    laydate.render({
                        elem: this
                        , trigger: 'click'
                        , done: function (value, date, endDate) { //监
                            var obj = $(this.elem[0])
                            var orders_number = $(obj).attr('key')
                            if ($(obj).attr('order_status') == 1) {
                                layer.msg("库存状态不能修改")
                                return false;
                            }
                            $.ajax({
                                url: '/order/updateOrdersByOrdersNumber',
                                data: {
                                    orders_number: orders_number,
                                    departure_time: $(obj).val(),
                                },
                                success: function (e) {

                                    //获取签一个时间


                                    layer.msg('成功');

                                    aa();
                                }


                            })
                        }

                    });


                })
			
        }


    languagetable_global = {};
    layui.config({}).extend({
        soulTable: '/static/layui-soul-table/ext/soulTable',
        tableChild: '/static/layui-soul-table/ext/tableChild',
        tableMerge: '/static/layui-soul-table/ext/tableMerge',
        tableFilter: '/static/layui-soul-table/ext/tableFilter',
        excel: '/static/layui-soul-table/ext/excel'
    });

    layui.use(['form', 'laydate', 'table', 'soulTable', 'element'], function () {
        var table = layui.table;
        // var laytable =layui.laytable;
        var layer = layui.layer;
        var form = layui.form;
        var soulTable = layui.soulTable;

        //加载日期控件
        layui.laydate.render({
            elem: '#follow_time',//input的id

        });

        layui.laydate.render({
            elem: '#send_time',//input的id

        });
        layui.laydate.render({
            elem: '#accept_time',//input的id

        });

        var isShow = true;  //定义⼀个标志位
        $('.kit-side-fold').click(function () {
            //选择出所有的span，并判断是不是hidden
            $('.layui-nav-item span').each(function () {
                if ($(this).is(':hidden')) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
            //判断isshow的状态
            if (isShow) {
                $('.layui-side.layui-bg-black').width(48); //设置宽度
                $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
                //将footer和body的宽度修改
                $('.layui-body').css('left', 60 + 'px');
                $('.layui-footer').css('left', 60 + 'px');
                //将⼆级导航栏隐藏
                $('dd span').each(function () {
                    $(this).hide();
                });
                //修改标志位
                isShow = false;
            } else {
                $('.layui-side.layui-bg-black').width(200);
                $('.kit-side-fold i').css('margin-right', '10%');
                $('.layui-body').css('left', 200 + 'px');
                $('.layui-footer').css('left', 200 + 'px');
                $('dd span').each(function () {
                    $(this).show();
                });
                isShow = true;
            }
        });

 

        var languagetable = languagetable_global = table.render({
            elem: '#language-table'
            , height: tableHeight
            , url: window.location.pathname+'?<?php echo http_build_query($_GET); ?>'///shipment/showLineAjax?orders_number=<?php echo \think\Request::instance()->get('orders_number'); ?>&start_pickup_time=<?php echo \think\Request::instance()->get('start_pickup_time'); ?>&end_pickup_time=<?php echo \think\Request::instance()->get('end_pickup_time'); ?>' //数据接口

            , response: {

                statusCode: 200 //规定成功的状态码，默认：0

            }
            , page: true //开启分页
            , limit: 90
            , toolbar: '#tool_bar' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
            , defaultToolbar: ['filter', 'exports']
            , totalRow: true //开启合计行
            , cols: [[ //表头
                {type: 'checkbox',field:'checkbox', fixed: 'left'}
                , {field: 'index_number', title: '序号', width: 40, type: 'numbers'}
                , {
                    field: 'orders_number', title: '运单编号', width: 110, sort: true, templet: function (e) {
                        style = "";
                        if (e.follow_level == 1)
                            style = "style='color:#b416e3'"
                        return "<span class='openOrders' orders_id='" + e.orders_id + "' project_id='" + e.project_id + "'  orders_number='" + e.orders_number + "' ><a " + style + " href='javascript:void(0);'>" + e.orders_number + "</a></span>"
                    }
                }
				
				
                , {field: 'send_goods_company', title: '发货方', width: 90, sort: true}
                , {
                    field: 'pickup_time', title: '运单日期', width: 70, sort: true, templet: function (e) {
                        return php_date('Y-m-d', parseInt(e.pickup_time));
                    }
                }
                , {
                    field: 'goods_name', title: '货物名称', width: 100, sort: true, templet: function (e) {
                        var info = "";
                        if (!e.orders_goods_info)
                            return ""
                        else
                            e.orders_goods_info.map(function (i) {
                                info = info + i.goods_name + "/";
                            })
                        return info.substring(0, info.length - 1)
                    }
                }
                , {
                    field: 'xdjs', title: '下单件数', width: 60, sort: true, totalRow: true

                }, {
                    field: 'xdsl', title: '下单数量', width: 60, sort: true, totalRow: true

                }
                , {
                    field: 'xdtj', title: '下单体积', width: 60, sort: true, totalRow: true
                }
                , {
                    field: 'xdzl', title: '下单重量', width: 60, sort: true, totalRow: true
                }, {
                    field: 'shipment_count', title: '发运件数', width: 60, sort: true, totalRow: true

                }, {
                    field: 'shipment_pack_count', title: '发运数量', width: 60, sort: true, totalRow: true

                }
                , {
                    field: 'shipment_volume', title: '发运体积', width: 60, sort: true, totalRow: true
                }
                , {
                    field: 'shipment_weight', title: '发运重量', width: 60, sort: true, totalRow: true
                }
                , {field: 'accept_goods_company', title: '收货方名称', width: 90, sort: true}
                , {field: 'accept_address', title: '收货方地址', width: 90}
                , {field: 'accept_cellphone', title: '联系电话', width: 90, sort: true}
                , {field: 'accept_name', title: '收货人', width: 80, sort: true}
                , {field: 'accept_location_name', title: '到站', width: 90, sort: true}
                , {field: 'remark', title: '备注', width: 90}
             
                , {
                    field: 'create_time', hide: false, title: '创建时间', width: 150, sort: true, templet: function (e) {
                        return php_date('Y-m-d H:i:s', e.create_time);
                    }
                }
                , {
                    field: 'shipment_time', title: '发货时间', width: 110, sort: true, templet: function (e) {
                        if (e.shipment_info.length > 0)
                            return php_date('Y-m-d', e.shipment_info[0].shipment_time);
                        else
                            return "";
                    }
                }
                , {
                    field: 'departure_time', title: '发车时间', width: 90, sort: true, templet: function (e) {

                   return php_date('Y-m-d', e.departure_time);
				   }
                }
                , {field: 'customer_order_number', title: '客户订单号', width: 120, sort: true}
                , {field: 'send_time', title: '要求到货日期', width: 110, sort: true}

                , {
                    field: 'true_time', title: '实际到货日期', width: 90, sort: true
                }


                , {field: 'send_address', title: '发货地址', width: 90, sort: true}
                , {field: 'send_location_name', title: '发站', width: 90, sort: true}
                , {
                    field: 'estimated_pack_unit',
                    hide: false,
                    title: '包装方式',
                    width: 90,
                    sort: true,
                    templet: function (e) {
                        var info = "";
                        if (!e.orders_goods_info)
                            return ""
                        else
                            e.orders_goods_info.map(function (i) {
                                info = info + baseConfig['order']['goods_pack'][i.realy_pack_unit] + "/";
                            })
                        return info.substring(0, info.length - 1)
                    }
                }

                , {field: 'insurance_goods', hide: false, title: '货物价值', width: 90, sort: true}



                 , {
                    field: 'send_location_name',
                    hide: true,
                    title: '支付方式',
                    width: 90,
                    sort: true,
                    templet: function (e) {
                        return baseConfig['order']['pay_type_name'][e.pay_type]
                    }
                }
                , {
                    field: 'bargain_type', hide: true, title: '计价方式', width: 90, sort: true, templet: function (e) {
                        return baseConfig['order']['bargain_type'][e.bargain_type]
                    }
                }
                , {field: 'bargain_price', hide: true, title: '收入', width: 90, sort: true}
                , {
                    field: 'send_cellphone', hide: true, title: '运输类型', width: 130, sort: true, templet: function (e) {
                        return baseConfig['order']['transportation_type_name'][e.transportation_type]
                    }
                }

                , {
                    field: 'accept_goods_company',
                    hide: true,
                    title: '计费包装单位',
                    width: 90,
                    sort: true,
                    templet: function (e) {
                        var info = 0;
                        if (!e.orders_goods_info)
                            return ""
                        else
                            e.orders_goods_info.map(function (i) {
                                info = info + baseConfig['order']['goods_pack'][i.realy_pack_unit] + "/";
                            })
                        return info;
                    }
                }
                , {
                    field: 'accept_name', hide: true, title: '计费件数', width: 90, sort: true, templet: function (e) {
                        var info = 0;
                        if (!e.orders_goods_info)
                            return ""
                        else
                            e.orders_goods_info.map(function (i) {
                                info = info + parseInt(i.realy_count);
                            })
                        return info;
                    }
                }
                , {
                    field: 'accept_cellphone', hide: true, title: '计费数量', width: 90, sort: true, templet: function (e) {
                        var info = 0;
                        if (!e.orders_goods_info)
                            return ""
                        else
                            e.orders_goods_info.map(function (i) {

                                info = info + parseInt(i.realy_pack_count);
                            })
                        return info;
                    }
                }
                , {
                    field: 'accept_all_address',
                    hide: true,
                    title: '计费重量',
                    width: 190,
                    sort: true,
                    templet: function (e) {
                        var info = 0;
                        if (!e.orders_goods_info)
                            return ""
                        else
                            e.orders_goods_info.map(function (i) {
                                info = info + parseFloat(i.realy_weight);
                            })
                        return info;
                    }
                }
                , {
                    field: 'goods_name', hide: true, title: '计费体积', width: 130, sort: true, templet: function (e) {
                        var info = 0;
                        if (!e.orders_goods_info)
                            return ""
                        else
                            e.orders_goods_info.map(function (i) {
                                info = info + parseFloat(i.realy_volume);
                            })
                        return info;
                    }
                }


                , {
                    field: 'bargain_type', hide: true, title: '发货信息', width: 90, sort: true, templet: function (e) {
                        return e.send_province_name + e.send_city_name + e.send_area_name + e.send_address
                    }
                }

                , {field: 'send_cellphone', hide: true, title: '发货方电话', width: 90, sort: true}


                , {field: 'replacement_prive', hide: true, title: '代收货款', width: 90, sort: true}
                , {
                    field: 'remark', hide: true, title: '代收状态', width: 90, sort: true, templet: function (e) {
                        return e.replacement_prive ? "已收" : "未收"
                    }
                }


                , {field: 'create_user_name', hide: true, title: '创建人', width: 90, sort: true}

               
                , {
                    field: 'remark', hide: true, title: '回单状态', width: 90, sort: true, templet: function (e) {
                        if (e.order_status >= 7) {

                            if (e.receipt_status == 0)
                                return ` <a href="javascript:void(0);" onclick="openlayer('/order/addReceipt?orders_id=${e.orders_id}','运单回执')" style="color: #f00">未回</a>`
                            else if (e.receipt_status == 1)
                                return `<a href="javascript:void(0);" onclick="openlayer('/order/orderUploadView?orders_id=${e.orders_id}','运单回执')" style="color: #0f0">已回</a>`
                            else if (e.receipt_status == 2)
                                return `<a href="javascript:void(0);" onclick="openlayer('/order/orderUploadView?orders_id=${e.orders_id}','运单回执')" style="color: #0f0">已送</a>`
                            else if (e.receipt_status == 3)
                                return `<a href="javascript:void(0);" onclick="openlayer('/order/orderUploadView?orders_id=${e.orders_id}','运单回执')" style="color: #0f0">其他</a>`

                        }
                        return ""
                    }
                }
                , {
                    field: 'abnormal_count', hide: true, title: '是否异常', width: 90, sort: true, templet: function (e) {
                        return e.abnormal_count > 0 ? "是" : "否"
                    }
                }
				
               
                ,{field: 'follow_remark', title: '发运跟踪',width:90,templet:function(e){
            return "<a href='javascript:void(0);' onclick=\"openlayer('/order/orderFollow?orders_number="+e.orders_number+"&orders_id="+e.orders_id+"&hide_form=1')\">点击查看<a>";
                    return e.follow_remark;
                    //return "<a href='/order/orderFollow?orders_id="+e.orders_id+"&orders_number="+e.orders_number+"'  class='orderFollow' follow_remark='"+e.follow_remark+"'  follow_time='"+e.follow_time+"'>查看</a>"
                }}
               
            
				<?php if(is_numeric(\think\Request::instance()->get('status'))): ?>
				,{field: 'del_remark', title: '删除原因',width:90}
				<?php endif; if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>
                , {field:'set',
                    fixed: 'right', title: '操作', width: 50, templet: function (e) {
                        


                        if (e.order_status == 1) {
                            return "<a href=\"javascript:openAddOrder('/order/showOrderAdd?project_id="+e.project_id+"&orders_id="+e.orders_id+"');  \"><button class=\"layui-btn layui-btn-sm hover-edit layui-btn-primary\">修改</button></a>"
                                
                        } else {
                            return ''


                        }


                    }
                }
                <?php endif; ?>

            ]],
            filter: {
                items: ['column', 'data', 'condition', 'editCondition', 'excel', 'clearCache'],
                cache: true
            },
            done: function (res, curr, count) {
		
		 line_counts = res.data.length;
	
			
                soulTable.render(this)
             

            }
        });

        $(document).on('click', '#clear', function () {
            soulTable.clearCache(languagetable.config.id)
            layer.msg('已还原！', {icon: 1, time: 1000})
        })


        table.on('toolbar(test)', function (obj) {
            var checkStatus = table.checkStatus(obj.config.id)
                , data = checkStatus.data; //获取选中的数据

         
            switch (obj.event) {
                case 'addImportant':

                    break;
                case 'add':
                    layer.msg('添加');
                    break;
                case 'update':
                    if (data.length === 0) {
                        layer.msg('请选择一行');
                    } else if (data.length > 1) {
                        layer.msg('只能同时编辑一个');
                    } else {
                        layer.alert('编辑 [id]：' + checkStatus.data[0].id);
                    }
                    break;
                case 'delete':
                    if (data.length === 0) {
                        layer.msg('请选择一行');
                    } else {
                        layer.msg('删除');
                    }
                    break;
            }
            ;
        });

        //监听头工具栏事件
        table.on('toolbar(language-table)', function (obj) {
var myTable=obj.config;
 var storeKey = location.pathname + location.hash + myTable.id;
 var colsStr=JSON.stringify(obj.config.cols);
            var checkStatus = table.checkStatus(obj.config.id)
                , data = checkStatus.data; //获取选中的数据
            switch (obj.event) {
			    case 'save':
				layer.msg("保存成功");
			    localStorage.setItem( storeKey, colsStr);
				break;
                case 'addImportant':
                    if (data.length > 0)
                        data.forEach(function (e) {
                            $.ajax({
                                url: "/index/changeStatus",
                                data: {
                                    table_id: e.orders_id,
                                    table_id_name: 'orders_id',
                                    table_name: "orders",
                                    status: e.follow_level?0:1, field: "follow_level"
                                },
                                success: function (e) {
								layer.msg("标记成功");
								languagetable_global.reload(); 
                                }

                            })

                        })
                    else
                        layer.msg("请选择一行");

                    break;

            }
            ;
        });

        table.on('sort(language-table)', function (obj,d,i) {
		console.log(obj)



var checkCell=$(".layui-form-checked I");
checkCell.click();
$("#nums").html(0)


            aa();


        });

        form.on('checkbox(checkAll)', function (e) {

            $(".income_select").map(function (i, d) {

                d.checked = e.elem.checked;
            })
            form.render();

        })

        form.on('select(orderStatus)', function (e) {

            var order_id = $(e.elem).attr("order-id");
            var order_number = $(e.elem).attr("order-number");
            var order_status = e.value;
            /*

           var order_status_old=$(e.elem).attr("order-status");

           if(parseInt(order_status_old)==1)
            {layer.msg("未出库不能修改状态",{time:1000},function(){window.location.reload()});return;}


           if(order_status==1)
           { layer.msg("已出库不能修改为未出库",{time:1000},function(){window.location.reload()}); return;}

           */

            $.ajax({
                url: "/source/addOrderOperatingAjax",
                data: {
                    orders_id: order_id, order_status: order_status

                },
                success: function (e) {

                },
                async: false


            });

            if (order_status == 6) {
                $.ajax(
                    {
                        url: "/index/changeStatus"
                        ,
                        data: {
                            table_id: order_id,
                            table_id_name: 'orders_id',
                            table_name: "orders",
                            status: parseInt((new Date).getTime() / 1000),
                            field: "true_time"
                        }
                        ,
                        success: function (e) {
                        }
                        ,
                        async: false
                    }
                )

            }


            $.ajax({
                url: "/index/changeStatus",
                data: {
                    table_id: order_id,
                    table_id_name: 'orders_id',
                    table_name: "orders",
                    status: order_status,
                    field: "order_status"
                },
                success: function (e) {
                    layer.msg("修改成功");
                    
                   
                },
                error: function (e) {
                    layer.msg("修改失败");
                }


            })

        });

        form.on('select(replacement_prive_type)', function (data) {
            var table_id = $(data.elem).parents("tr").children(".order_id").html();

            var table_id_name = "orders_id";
            var table_name = "orders";
            status = data.value;

            $.ajax({
                type: "POST",
                url: '/index/changeStatus',
                data: {
                    table_id: table_id,
                    table_id_name: table_id_name,
                    table_name: table_name,
                    status: status,
                    field: "replacement_prive_type"
                },
                success: function (data) {
                 
                    if (data.code != 200) {
                        layer.msg(data.msg);
                        return false;
                    } else if (data.code == 200) {
                        layer.msg('操作成功', {
                            time: 1, end: function (layero, index) {

                            }
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.msg('验证失败')
                }
            });


            console.log(data)
        })

        form.on('switch(switchTest)', function (data) {
            var table_id = $(data.elem).parents("tr").children(".order_id").html();

            var table_id_name = "order_id";
            var table_name = "order";
            if (this.checked == true) {
                var status = 1;
            } else {
                var status = 0;
            }

            $.ajax({
                type: "POST",
                url: '/index/changeStatus',
                data: {
                    table_id: table_id,
                    table_id_name: table_id_name,
                    table_name: table_name,
                    status: status,
                },
                success: function (data) {
                    //console.log(data);
                    if (data.code != 200) {
                        layer.msg(data.msg);
                        return false;
                    } else if (data.code == 200) {
                        layer.msg('操作成功', {
                            time: 1, end: function (layero, index) {

                            }
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.msg('验证失败')
                }
            });
        });

        $(document).on('click', '.sdsj', function () {

            layer.confirm('确认删除该数据？', function (index) {
                $.ajax({
                    url: "/shipment/delShipment",
                    data: {orders_number: data[0].orders_number, finance_number: data[0].finance_number},
                    success: function (e) {
                        location.reload()
                    },


                })
                layer.close(index);
            })

        })
		
		
		
	
		
		var nums = 0;
    //多选事件
    table.on('checkbox(language-table)', function(obj){
        if(obj.type=="all"){ //全选
            if(obj.checked==true){
                $(".layui-table-body table.layui-table tbody tr").css('background','#99CCFF');
                $("#nums").html(line_counts);
            }else{
                $(".layui-table-body table.layui-table tbody tr").map(function(i,d){
				$(d).css('background','#fff');
				})
				
				
				
                $("#nums").html(0);
            }
        }else{ //单选
            if(obj.checked==true){
                obj.tr.css('background','#99CCFF');
                nums = parseInt($("#nums").html())+1;
                $("#nums").html(nums);
            }else{
                obj.tr.css('background','#fff');
                nums = parseInt($("#nums").html())-1;
                $("#nums").html(nums);
            }
        }

    });
		
		
		
    })



    function signStatus(id) {
        $.ajax({
            url: "/index/changeStatus",
            data: {table_id: id, table_id_name: 'orders_id', table_name: "orders", status: "7", field: "order_status"},
            success: function (e) {
                window.location.reload();
            }

        })

    }

    function qianshou() {

        var checkdata = layui.table.checkStatus("language-table")['data'];
        if (checkdata.length != 1)
            layer.msg("请选择一条数据");
        else {

            window.location.href = "/order/showOrderDeliveryReceiptManage?orders_id=" + checkdata[0]['orders_id'];
        }
    }

    function delorders() {
        var checkdata = layui.table.checkStatus("language-table")['data'];
        if (checkdata.length != 1)
            layer.msg("请选择一条数据");
        else {
            if (checkdata[0]['order_status'] != 1)
                layer.msg("只能删除库存运单");
            else
			
			layer.prompt({
                            formType: 0,
                            value: '',
                            title: '删除原因',
                            area: ['80px', '80px'] //自定义文本域宽高
                        }, function (value, index, elem) {
						
						$.ajax({
                                    url: "/order/delOrder",
                                    data: {orders_id:checkdata[0]['orders_id'],
									status:0,
									del_remark:value
									},
                                    success: function (e) {
                                        layer.msg("修改成功");
										layer.close(index);
										languagetable_global.reload();
                                    }

                                })
						
						
						
						

                        });
			
              


        }
    }


    function changeTrueTime(obj) {


        console.log(obj)

    }

    $(document).on('click', '.openOrders', function () {
        layer.open({
            type: 2,
            title: '预览订单',
            maxmin: true,
			area: ["100%", "600px"],
            success: function (layero, index) {
                //在回调方法中的第2个参数“index”表示的是当前弹窗的索引。
                //通过layer.full方法将窗口放大。
                //layer.full(index);
            },
            content: '/order/showOrderAdd?view=1&project_id='+$(this).attr('project_id')+'&orders_id=' + $(this).attr('orders_id')


        });
    })

    form.on('select(orderabnormalStatus)', function (e) {
        var order_id = $(e.elem).attr("order-id");
        var order_abnormal_describe_id = e.value;


        $.ajax(
            {
                url: "/order/changeAbnormalDescribeAjax"
                , data: {orders_id: order_id, order_abnormal_describe_id: order_abnormal_describe_id}
                , success: function (e) {
                    layer.msg("修改成功");

                }
                , async: false
            }
        )


    })

    function changeShipmentNumber(orders_id, obj) {

        var shipment_number = $(obj).val()
        $.ajax({
            url: "/order/changeShipmentNumberAjax",
            data: {orders_id: orders_id, shipment_number: shipment_number},
            success: function (e) {
                layer.msg("修改成功")
                //window.location.reload();
                //languagetable_global.reload();
            }

        })

    }

    function addphoto() {
        var data_array = layui.table.checkStatus("language-table")
        data_array.data.map(function (e, i) {

            openlayer("/order/addReceipt?type=2&orders_id=" + e.orders_id, '上传拍照件')

        })

    }

  function openAddOrder(url){
  layer.open({
            type: 2,
			 maxmin: true,
            title: "运单",
            content: url,
            area: ["100%", "100%"]
        })
  
  }
  
   
</script>

</body>
</html>


