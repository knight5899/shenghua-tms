<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:95:"/var/www/html/test_erp/public/../application/index/view/branchcompany/company_order_manage.html";i:1639618343;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title><?php echo $language_tag['index_product_showRouteTemplateManage_receiptTemplate']; ?></title>
    <link href="/static/css/product.css" rel="stylesheet">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg" style="overflow-y: auto">

        <!-- 内容主体区域 -->
        <div style="padding: 15px 15px 0px;">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a><?php echo $language_tag['index_public_homepage']; ?></a>
                        <a><?php echo $language_tag['index_branchcompany_showBranchProductManage_branchManagement']; ?></a>
                        <a><cite><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_orderManagement']; ?></cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg layui-form demoTable'>

                    <div class="layui-row">
                        <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_finance_showReceivableManage_order_number']; ?>:</label>
                                <div class="layui-input-block">
                                    <input type="text" id="one_id_search" value="<?php echo \think\Request::instance()->get('one_id_search'); ?>" name="one_id_search"
                                           placeholder="<?php echo $language_tag['index_finance_showReceivableManage_order_number']; ?>" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_branchcompany_showBranchProductManage_productName']; ?>:</label>
                                <div class="layui-input-block">
                                    <input type="order_name" placeholder="<?php echo $language_tag['index_branchcompany_showBranchProductManage_productName']; ?>" id="order_name" autocomplete="off" class="layui-input" />
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_channel']; ?>:</label>
                                <div class="layui-input-block">
                                    <select name="" lay-search="" id="distributor_name">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-row">
                      <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_product_visitor_goTime']; ?><?php echo $language_tag['index_system_showCountryManage_time_zone']; ?>:</label>
                                <div class="layui-input-block">
                                     <select  id="begin_begin_time_time_zone">
                                     	<?php if(is_array(\think\Session::get('time_zone')) || \think\Session::get('time_zone') instanceof \think\Collection || \think\Session::get('time_zone') instanceof \think\Paginator): if( count(\think\Session::get('time_zone'))==0 ) : echo "" ;else: foreach(\think\Session::get('time_zone') as $key=>$v1): ?>
                                        <option value="<?php echo $v1['time_zone_id']; ?>"><?php echo $v1['name']; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>   
                    
                        <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_product_visitor_goTime']; ?>:</label>
                                <div class="layui-input-block">
                                    <div class="layui-input-inline date-width">
                                        <input type="text" name="" placeholder="<?php echo $language_tag['index_branchcompany_showDistributorManage_pleaseSelectTheDate']; ?>" autocomplete="off" id="begin_begin_time" class="layui-input sales-date">
                                    </div>
                                    <div class="date-and">-</div>
                                    <div class="layui-input-inline date-width">
                                        <input type="text" name="" placeholder="<?php echo $language_tag['index_branchcompany_showDistributorManage_pleaseSelectTheDate']; ?>" autocomplete="off" id="end_begin_time" class="layui-input sales-date">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_branchcompany_companyOrderManage_visitor']; ?>:</label>
                                <div class="layui-input-block">
                                    <input type="text" id="customer_name" value="<?php echo \think\Request::instance()->get('one_content_search'); ?>" name="one_content_search"
                                           placeholder="" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="layui-row">
                        <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label">订单状态:</label>
                                <div class="layui-input-block">
                                    <select name="" lay-search="" id="company_order_status">
                                        <option value=""></option>
                                        <option value="1"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_unconfirmed']; ?></option>
                                        <option value="2"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_confirmed']; ?></option>
                                        <option value="3"><?php echo $language_tag['index_public_cancel']; ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_orderTime']; ?><?php echo $language_tag['index_system_showCountryManage_time_zone']; ?>:</label>
                                <div class="layui-input-block">
                                     <select  id="begin_create_time_time_zone">
                                     	<?php if(is_array(\think\Session::get('time_zone')) || \think\Session::get('time_zone') instanceof \think\Collection || \think\Session::get('time_zone') instanceof \think\Paginator): if( count(\think\Session::get('time_zone'))==0 ) : echo "" ;else: foreach(\think\Session::get('time_zone') as $key=>$v1): ?>
                                        <option value="<?php echo $v1['time_zone_id']; ?>"><?php echo $v1['name']; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>                        
                        <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_orderTime']; ?>:</label>
                                <div class="layui-input-block">
                                    <div class="layui-input-inline date-width">
                                        <input type="text" name="" placeholder="<?php echo $language_tag['index_branchcompany_showDistributorManage_pleaseSelectTheDate']; ?>" autocomplete="off" id="begin_create_time" class="layui-input sales-date">
                                    </div>
                                    <div class="date-and">-</div>
                                    <div class="layui-input-inline date-width">
                                        <input type="text" name="" placeholder="<?php echo $language_tag['index_branchcompany_showDistributorManage_pleaseSelectTheDate']; ?>" autocomplete="off" id="end_create_time" class="layui-input sales-date">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class='layui-row'>
                                            <div class="layui-col-md4">
                            <div class="layui-form-item">
                                <label class="layui-form-label"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_creater']; ?>:</label>
                                <div class="layui-input-block">
                                    <input type="text" id="create_user_name" value="<?php echo \think\Request::instance()->get('one_agent_search'); ?>" name="one_agent_search"  placeholder="<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_creater']; ?>"
                                           autocomplete="off" class="layui-input">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="layui-row">
                        <div class="layui-col-md4 layui-col-md-offset4">
                            <div class='input-inline all-button-center pages-search-margin'>
                                <button class="layui-btn nav-search" data-type="reload"><?php echo $language_tag['index_public_search']; ?></button>
                            </div>
                        </div>
                    </div>

                    <!--<div class='layui-button-inline'>

                         <a href='#'><button class="layui-btn nav-details">打印订单</button></a>
                    </div>-->
                </div>
            </div>
            <div class="content-bg" style="padding-bottom: 5px!important;">
                <span style="<?php echo action('index/auth/check_auth',['role_id'=>'248']); ?>"><a href='/branchcompany/companyOrderManage'><button class="layui-btn nav-add layui-btn-sm"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_newOrder']; ?></button></a></span>
                <button class="layui-btn nav-add layui-btn-sm" onclick="exportApiDemo('/companyorder/getCompanyOrderAjax?is_like=1')"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_printOrder']; ?></button><!--打印订单-->
                <hr>
                <div class="company-pageHeight">
                    <table id="orderTable" lay-filter="orderTable"></table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/html" id="barDemo">
        <span style="<?php echo action('index/auth/check_auth',['role_id'=>'249']); ?>"><a href="javascript:void(0)"><button class="layui-btn  layui-btn-xs hover-edit layui-btn-primary" lay-event="edit"><?php echo $language_tag['index_public_update']; ?></button></a></span>
        <span style="<?php echo action('index/auth/check_auth',['role_id'=>'250']); ?>"><a class="layui-btn layui-btn-xs hover-edit layui-btn-primary" href="javascript:void(0)" lay-event="receipt"><?php echo $language_tag['index_branchcompany_showCompanyOrderManage_touristItinerary']; ?></a></span>
        {{#  if(d.company_order_status ==3){ }}
            <span><a href="javascript:void(0)" ><button company_order_number="<?php echo $vo['company_order_number']; ?>" class="del_company_order layui-btn layui-btn-xs nav-polylingual layui-btn-primary" style="color: #fff">已取消</button></a></span>
        {{#  } else { }}
        <span style="<?php echo action('index/auth/check_auth',['role_id'=>'251']); ?>"><a href="javascript:void(0)" ><button company_order_number="<?php echo $vo['company_order_number']; ?>" class="del_company_order layui-btn layui-btn-xs nav-del layui-btn-primary" lay-event="del"><?php echo $language_tag['index_public_cancel']; ?></button></a></span>
        {{#  } }}
    </script>
    <!-- 	 取消 暂时隐藏
    	<span style="<?php echo action('index/auth/check_auth',['role_id'=>'251']); ?>"><a href="javascript:void(0)" ><button company_order_number="<?php echo $vo['company_order_number']; ?>" class="del_company_order layui-btn layui-btn-xs nav-del layui-btn-primary" lay-event="del"><?php echo $language_tag['index_public_cancel']; ?></button></a></span>
     -->
    
    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/product/all.js'></script>
<script src='/static/javascript/data.js'></script>
<script type="text/javascript" src="/static/layui/layui.js"></script>

<script type="text/javascript">
    layui.config({
        base: '/static/javascript/layui_exts/',
    }).extend({
        excel: 'excel',
    });
</script>
<script type="text/javascript">
    layui.use(['table','jquery','laydate'], function(){
        var table = layui.table;
        var $ = layui.jquery;
        var laydate = layui.laydate;
        var dataThat={};
        var urls='';
        var pages=1;
        dataThat=JSON.parse(localStorage.getItem("loc"));
  
        if(dataThat != null){
            pages=dataThat.page;
            $('#one_id_search').val(dataThat.company_order_number);
            $('#order_name').val(dataThat.order_name);
            $('#begin_create_time').val(dataThat.begin_create_time);
            $('#end_create_time').val(dataThat.end_create_time);
            $('#begin_begin_time').val(dataThat.begin_begin_time);

            
            $('#end_begin_time').val(dataThat.end_begin_time);
            $('#distributor_name').val(dataThat.distributor_name);
            $('#customer_name').val(dataThat.customer_name);
            $('#create_user_name').val(dataThat.create_user_name);
            $('#company_order_status').val(dataThat.company_order_status);
            $('#begin_create_time_time_zone').val(dataThat.begin_create_time_time_zone);
            $('#begin_begin_time_time_zone').val(dataThat.begin_begin_time_time_zone);
            
            var distributor_name=dataThat.distributor_name==null?'':dataThat.distributor_name;
            urls='/companyorder/getCompanyOrderAjax/status/1?is_like=1&company_order_number='+dataThat.company_order_number+'&order_name='+dataThat.order_name+'&begin_create_time='+dataThat.begin_create_time+'&end_create_time='+dataThat.end_create_time+'&begin_begin_time='+dataThat.begin_begin_time+'&end_begin_time='+dataThat.end_begin_time+'&distributor_name='+distributor_name+'&customer_name='+dataThat.customer_name+'&create_user_name='+dataThat.create_user_name+'&company_order_status='+dataThat.company_order_status+"&begin_create_time_time_zone="+dataThat.begin_create_time_time_zone+"&begin_begin_time_time_zone="+dataThat.begin_begin_time_time_zone;
        }else{
            urls='/companyorder/getCompanyOrderAjax/status/1?is_like=1';
        }
        form.render();
        lay('.sales-date').each(function(){
            laydate.render({
                elem: this
                ,trigger: 'click'
                ,done: function(value, date){
                    console.log(date)
                }
            });
        });
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var company=bodyH-bodyTopH-15-115;
        layer.load(2);
        var ab=table.render({
            elem: '#orderTable'
            ,url:urls
            ,parseData: function(res){
                console.log(res)
                return {
                    "code": res.code,
                    "msg": res.msg,
                    "count": res.data_count,
                    "data": res.data
                };
            }
            ,response: {
                statusCode: 200 //规定成功的状态码，默认：0
            }
            ,cols: [[ //表头
                {field: 'company_order_number', title: '<?php echo $language_tag['index_finance_showReceivableManage_order_number']; ?>', width:170, sort: true, fixed: 'left',templet:'<div><a href="/branchcompany/companyOrderManage?company_order_number={{d.company_order_number}}" class="layui-table-link">{{d.company_order_number}}</a></div>'}
                ,{field: 'order_name', title: '<?php echo $language_tag['index_branchcompany_showBranchProductManage_productName']; ?>', sort: true, fixed: 'left',width:150}
                ,{field: 'team_product_number', title: '<?php echo $language_tag['index_finance_showReceivableManage_team_product_number']; ?>', sort: true, fixed: 'left',width:150,templet:'<div>{{d.team_product_number==""?"":d.team_product_number==null?"":d.team_product_number}}</div>'}
                ,{field:'company_order_status',title:'订单状态',sort:true,width:120,templet:'<div>{{d.company_order_status==1?"<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_unconfirmed']; ?>":d.company_order_status==2?"<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_confirmed']; ?>":"<?php echo $language_tag['index_public_cancel']; ?>"}}</div>'}
                ,{field: 'begin_time', title: '<?php echo $language_tag['index_branchcompany_showBranchProductManage_startDate']; ?>', sort: true, width:120,templet: '<div>{{d.begin_time!=null?d.begin_time:""}} {{d.begin_time_time_zone_name}}</div>'}
                ,{field: 'customer_count', title: '<?php echo $language_tag['index_product_PlanBooking_number']; ?>', sort: true, width:100}
                ,{field: 'create_user_name', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_creater']; ?>', sort: true, width:140}
                ,{field: 'wr', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_retail']; ?>/<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_wholeSale']; ?>', sort: true, width:150,templet: '<div>{{d.wr==1?"retail":"wholesale"}}</div>'}
                ,{field: 'clientsource', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_customerSource']; ?>', sort: true, width:120,templet: '<div>{{clientsource(d.clientsource)}}</div>'}
                ,{field: 'channel_type', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_channelOf_distribution']; ?>', sort: true, width:120,templet: '<div>{{d.channel_type==1 ? "<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_distributor']; ?>" : "<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_straightGuest']; ?>" }}</div>'}
                ,{field: 'buy_order_time', title: '<?php echo $language_tag['index_branchcompany_showCustomerOrder_order_time']; ?>', sort: true, width:120,templet: '<div>{{d.buy_order_time!=null?d.buy_order_time:""}} {{d.buy_order_time_time_zone_name}}</div>'}
                ,{field: 'end_time', title: '<?php echo $language_tag['index_branchcompany_showBranchProductManage_endDate']; ?>', sort: true, width:120,templet: '<div>{{d.end_time!=null?d.end_time:""}} {{d.end_time!=null?d.end_time_time_zone_name:""}}</div>'}
                ,{field: 'begin_city', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_startCity']; ?>', sort: true, width:150}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_channel']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].distributor_name:""}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showDistributorManage_associateType']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?associate_type(d.distributor_info[0].associate_type):""}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showBranchProductManage_language']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].language_name!=null?d.distributor_info[0].language_name:"":""}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showDistributorManage_city']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].city_name!=null?d.distributor_info[0].city_name:"":""}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_provincialState']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].province_name!=null?d.distributor_info[0].province_name:"":""}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_country']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].country_name!=null?d.distributor_info[0].country_name:"":""}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showDistributorManage_address']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].address!=null?d.distributor_info[0].address:"":""}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showDistributorManage_zipCode']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].zip_code!=null?d.distributor_info[0].zip_code:"":""}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showBranchProductManage_email']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].email!=null?d.distributor_info[0].email:"":d.email==null?"":d.email}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showDistributorManage_contacts']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].contect!=null?d.distributor_info[0].contect:"":d.contect_name==null?"":d.contect_name}}</div>'}
                ,{field: 'distributor_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_tel']; ?>', sort: true, width:160,templet: '<div>{{d.distributor_info?d.distributor_info[0].tel!=null?d.distributor_info[0].tel:"":d.tel==null?"":d.tel}}</div>'}
                ,{field: 'remark', title: '<?php echo $language_tag['index_public_mark']; ?>', sort: true, width:120}
                ,{field: 'receivable_money', title: '<?php echo $language_tag['index_finance_showReceivableManage_receivable_money']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.total:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_quotedPrice']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.price:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_tax']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.tax:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_sales_outstanding']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.miss_sale_receivable:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_sales_received']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.sale_receivable:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_financial_uncollected']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.miss_finance_receivable:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_financialReceived']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.finance_receivable:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_primeCost']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.cost:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_finance_showMustPayManage_true_receipt']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.yifu:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_finance_showMustPayManage_unpaid']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.weifu:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_exchange']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.huiduisunyi:""}}</div>'}
                ,{field: 'company_order_receivable_info', title: '<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_gross_profit']; ?>', sort: true, width:120,templet: '<div>{{d.company_order_receivable_info?d.company_order_receivable_info.maoli:""}}</div>'}
                ,{fixed: 'right',title: '<?php echo $language_tag['index_source_operation']; ?>', width:230, align:'center', toolbar: '#barDemo'}
            ]]
            ,page: true //开启分页
            ,limits: [10,20,30,40,50,60,70,80,90]
            ,limit: 20 //每页默认显示的数量
            ,id: 'testReload',
            page: {
                curr: pages //重新从第 1 页开始
            },
            done: function(res, curr, count){
                res.data.forEach(function (item, index) {

                    if(item.company_order_status==3){
                        $(".layui-table-box tbody tr[data-index='" + index + "']").css("text-decoration", "line-through");
                        $(".layui-table-box .layui-table-fixed-r tbody tr[data-index='" + index + "']").css("text-decoration", "none");
                    }

                })


                var one_id_search = $('#one_id_search').val();
                var order_name = $('#order_name').val();
                var begin_create_time_time_zone = $('#begin_create_time_time_zone').val();
                var begin_create_time = $('#begin_create_time').val();
                var begin_begin_time_time_zone  = $('#begin_begin_time_time_zone').val();
                var end_create_time = $('#end_create_time').val();
                var begin_begin_time = $('#begin_begin_time').val();
                var end_begin_time = $('#end_begin_time').val();
                var distributor_name = $('#distributor_name').val();
                var customer_name = $('#customer_name').val();
                var create_user_name = $('#create_user_name').val();
                var company_order_status=$('#company_order_status').val();
                var datasLis={
                    page:curr,
                    is_company_order_search:1,
                    company_order_number: one_id_search,
                    order_name:order_name,
                    begin_create_time:begin_create_time,
                    end_create_time:end_create_time,
                    begin_begin_time:begin_begin_time,
                    end_begin_time:end_begin_time,
                    distributor_name:distributor_name,
                    customer_name:customer_name,
                    create_user_name:create_user_name,
                    company_order_status:company_order_status,
                    status:1,
                    is_like:1,
                    begin_create_time_time_zone:begin_create_time_time_zone,
                    begin_begin_time_time_zone:begin_begin_time_time_zone
                }
                localStorage.setItem("loc", JSON.stringify(datasLis));
                layer.closeAll('loading');

            }
        });
        var $ = layui.$, active = {
            reload: function(){
                var one_id_search = $('#one_id_search').val();
                var order_name = $('#order_name').val();
                var begin_create_time = $('#begin_create_time').val();

                var end_create_time = $('#end_create_time').val();
                var begin_begin_time = $('#begin_begin_time').val();
                var end_begin_time = $('#end_begin_time').val();
                var distributor_name = $('#distributor_name').val();
                var customer_name = $('#customer_name').val();
                var create_user_name = $('#create_user_name').val();
                var company_order_status=$('#company_order_status').val();
                var begin_create_time_time_zone=$('#begin_create_time_time_zone').val();
                var begin_begin_time_time_zone = $('#begin_begin_time_time_zone').val();
                //执行重载
                table.reload('testReload', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    ,where: {
                        is_company_order_search:1,
                        company_order_number: one_id_search,
                        order_name:order_name,
                        begin_create_time:begin_create_time,
                        end_create_time:end_create_time,
                        begin_begin_time:begin_begin_time,
                        end_begin_time:end_begin_time,
                        distributor_name:distributor_name,
                        customer_name:customer_name,
                        create_user_name:create_user_name,
                        company_order_status:company_order_status,
                        status:1,
                        is_like:1,
                        begin_create_time_time_zone:begin_create_time_time_zone,
                        begin_begin_time_time_zone:begin_begin_time_time_zone
                    }
                    , url: '/companyorder/getCompanyOrderAjax'
                    , method: 'post'
                });
            }
        };
        $('.demoTable .layui-btn').on('click', function(){
            var type = $(this).data('type');
            layer.load(2);
            active[type] ? active[type].call(this) : '';
        });
        //取消
        table.on('tool(orderTable)', function(obj){
            var data = obj.data;
            if(obj.event === 'del'){
                layer.confirm('<?php echo $language_tag['index_bookings_showBookingList_is_confirm']; ?>？', function(index){
                    $.ajax({
                        type: "POST",
                        url: '/companyorder/updateCompanyOrderStatusByCompanyOrderNumberAjax',
                        data: {
                            "company_order_number":data.company_order_number,
                            "company_order_status":3
                        },
                        success: function(data){
                            layer.load(2);
                            if(data.code!=200){
                                layer.msg(data.msg);
                                return false;
                            }else if(data.code==200) {
                                layer.closeAll();
                                layer.msg('<?php echo $language_tag['index_public_success']; ?>');
                                location.reload();
                            }
                        }
                    });
                });
            }else if(obj.event==='edit'){
                location.href="/branchcompany/companyOrderManage?company_order_number="+data.company_order_number;
            }else if(obj.event==='receipt'){
                location.href="/branchcompany/showCompanyOrderCustomerGuideReceipt?company_order_number="+data.company_order_number+'&company_order_id='+data.company_order_id;
            }
        });
        function dates(obj) {
            if(obj!=''){
                return Math.round(new Date(obj).getTime()/1000).toString();
            }else{
                return '';
            }
        }
    });
</script>
<script src='/static/javascript/product/company_order.js'></script>
<script type="text/javascript">
    function exportApiDemo(url) {
        layui.use(['jquery', 'excel', 'layer'], function() {
            var $ = layui.jquery;
            var layer = layui.layer;
            var excel = layui.excel;


            $.ajax({
                url: url,
                data:{is_like:1}
                ,dataType: 'json'
                ,success: function(res) {
                    var data = res.data;
                    data = excel.filterExportData(data, {
                        company_order_number: 'company_order_number',
                        order_name: 'order_name',
                        begin_time:function (value,line,data) {
                            return line.begin_time!=null?date('Y-m-d',line.begin_time):'';
                        },
                        company_order_status:function(value,line,data){
                            return line.company_order_status==1?"<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_unconfirmed']; ?>":line.company_order_status==2?"<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_confirmed']; ?>":"<?php echo $language_tag['index_public_cancel']; ?>"
                        },
                        customer_count:'customer_count',
                        create_user_name:'create_user_name',
                        wr:function (value,line,data) {
                            return line.wr==1?"retail":"wholesale"
                        },
                        clientsource:function (value,line,data) {
                            return clientsource(line.clientsource);
                        },
                        channel_type:function (value,line,data) {
                            return line.channel_type==1?"<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_agent']; ?>":"<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_straightGuest']; ?>";
                        },
                        buy_order_time: function (value,line,data) {
                            return line.buy_order_time!=null?date('Y-m-d',line.buy_order_time):'';
                        },
                        end_time:function (value,line,data) {
                            return line.end_time!=null?date('Y-m-d',line.end_time):'';
                        },
                        begin_city:'begin_city',
                        distributor_name:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].distributor_name:'';
                        },
                        associate_type:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].associate_type!=null?associate_type(line.distributor_info[0].associate_type):'':'';
                        },
                        language_name:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].language_name!=null?line.distributor_info[0].language_name:'':'';
                        },
                        city_name:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].city_name!=null?line.distributor_info[0].city_name:'':'';
                        },
                        province_name:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].province_name!=null?line.distributor_info[0].province_name:'':'';
                        },
                        country_name:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].country_name!=null?line.distributor_info[0].country_name:'':'';
                        },
                        address:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].address!=null?line.distributor_info[0].address:'':'';
                        },
                        zip_code:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].zip_code!=null?line.distributor_info[0].zip_code:'':'';
                        },
                        email:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].email!=null?line.distributor_info[0].email:'':line.email!=null?line.email:'';
                        },
                        contect:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].contect!=null?line.distributor_info[0].contect:'':line.contect_name!=null?line.contect_name:'';
                        },
                        tel:function (value,line,data) {
                            return line.distributor_info?line.distributor_info[0].tel!=null?line.distributor_info[0].tel:'':line.tel!=null?line.tel:'';
                        },
                        remark:function (value,line,data) {
                            return line.remark!=null?line.remark:'';
                        },
                        receivable_money:function (value,line,data) {
                            return line.receivable_money==0?'0.00':line.receivable_money;
                        },
                        price:function (value,line,data) {
                            return line.company_order_receivable_info.price==0?'0.00':line.company_order_receivable_info.price;
                        },
                        tax:function (value,line,data) {
                            return line.company_order_receivable_info.tax==0?'0.00':line.company_order_receivable_info.tax;
                        },
                        miss_sale_receivable:function (value,line,data) {
                            return line.company_order_receivable_info.miss_sale_receivable==0?'0.00':line.company_order_receivable_info.miss_sale_receivable;
                        },
                        sale_receivable:function (value,line,data) {
                            return line.company_order_receivable_info.sale_receivable==0?'0.00':line.company_order_receivable_info.sale_receivable;
                        },
                        miss_finance_receivable:function (value,line,data) {
                            return line.company_order_receivable_info.miss_finance_receivable==0?'0.00':line.company_order_receivable_info.miss_finance_receivable;
                        },
                        finance_receivable:function (value,line,data) {
                            return line.company_order_receivable_info.finance_receivable==0?'0.00':line.company_order_receivable_info.finance_receivable;
                        },
                        cost:function (value,line,data) {
                            return line.company_order_receivable_info.cost==0?'0.00':line.company_order_receivable_info.cost;
                        },
                        yifu:function (value,line,data) {
                            return line.company_order_receivable_info==0?'0.00':line.company_order_receivable_info.yifu;
                        },
                        weifu:function (value,line,data) {
                            return line.company_order_receivable_info.weifu==0?'0.00':line.company_order_receivable_info.weifu;
                        },
                        huiduisunyi:function (value,line,data) {
                            return line.company_order_receivable_info.huiduisunyi==0?'0.00':line.company_order_receivable_info.huiduisunyi;
                        },
                        maoli:function (value,line,data) {
                            return line.company_order_receivable_info.maoli==0?'0.00':line.company_order_receivable_info.maoli;
                        },
                    });//梳理数据
                    data.unshift({
                        company_order_number: '<?php echo $language_tag['index_finance_showReceivableManage_order_number']; ?>',
                        order_name: "<?php echo $language_tag['index_branchcompany_showBranchProductManage_productName']; ?>",
                        company_order_status:"订单状态",
                        begin_time:'<?php echo $language_tag['index_branchcompany_showBranchProductManage_startDate']; ?>',
                        customer_count:'<?php echo $language_tag['index_product_PlanBooking_number']; ?>',
                        create_user_name:'<?php echo $language_tag['index_system_showCompanyManage_create_user']; ?>',
                        wr:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_retail']; ?>/<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_wholeSale']; ?>',
                        clientsource:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_customerSource']; ?>',
                        channel_type:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_channelOf_distribution']; ?>',
                        buy_order_time: "<?php echo $language_tag['index_branchcompany_showCustomerOrder_order_time']; ?>",
                        end_time:'<?php echo $language_tag['index_branchcompany_showBranchProductManage_endDate']; ?>',
                        begin_city:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_startCity']; ?>',
                        distributor_name:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_channel']; ?>',
                        associate_type:'<?php echo $language_tag['index_branchcompany_showDistributorManage_associateType']; ?>',
                        language_name:'<?php echo $language_tag['index_branchcompany_showBranchProductManage_language']; ?>',
                        city_name:'<?php echo $language_tag['index_branchcompany_showDistributorManage_city']; ?>',
                        province_name:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_provincialState']; ?>',
                        country_name:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_country']; ?>',
                        address:'<?php echo $language_tag['index_branchcompany_showDistributorManage_address']; ?>',
                        zip_code:'<?php echo $language_tag['index_branchcompany_showDistributorManage_zipCode']; ?>',
                        email:'<?php echo $language_tag['index_branchcompany_showBranchProductManage_email']; ?>',
                        contect:'<?php echo $language_tag['index_branchcompany_showDistributorManage_contacts']; ?>',
                        tel:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_tel']; ?>',
                        remark:'<?php echo $language_tag['index_public_mark']; ?>',
                        receivable_money:'<?php echo $language_tag['index_finance_showReceivableManage_receivable_money']; ?>',
                        price:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_quotedPrice']; ?>',
                        tax:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_tax']; ?>',
                        miss_sale_receivable:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_sales_outstanding']; ?>',
                        sale_receivable:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_sales_received']; ?>',
                        miss_finance_receivable:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_financial_uncollected']; ?>',
                        finance_receivable:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_financialReceived']; ?>',
                        cost:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_addEdit_primeCost']; ?>',
                        yifu:'<?php echo $language_tag['index_finance_showMustPayManage_true_receipt']; ?>',
                        weifu:'<?php echo $language_tag['index_finance_showMustPayManage_unpaid']; ?>',
                        huiduisunyi:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_exchange']; ?>',
                        maoli:'<?php echo $language_tag['index_branchcompany_showCompanyOrderManage_gross_profit']; ?>'
                    });

                    var timestart = Date.now();
                    excel.exportExcel({
                        sheet1: data
                    }, '<?php echo $language_tag['index_product_PlanBooking_order']; ?>.xlsx', 'xlsx');
                }
                ,error: function() {
                    layer.alert('获取数据失败');
                }
            });
        });
    }
</script>
</body>
</html>