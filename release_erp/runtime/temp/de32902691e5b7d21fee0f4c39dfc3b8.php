<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:85:"/var/www/html/test_erp/public/../application/index/view/bill/customer_bill_check.html";i:1663202604;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>TMS收入账单修改</title>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">



    <div class="layui-body">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
                    <a>首页</a>
					<a>收入管理</a>
					<a>TMS收入开票</a>
					<a><cite>修改</cite></a>
				</span>
            </div>
            <br/>
            <br/>
            <form class="layui-form"  id='fax_table'  onSubmit="return kaipiaoEdit()">
                <div class="layui-row layui-col-space10">

                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label ">对账编号:</label>
                            <div class="layui-input-block">
                                <input style="padding-right: 45px" disabled maxlength="300" value="<?php echo $result['customer_bill_number']; ?>" lay-verify="required"  autocomplete="off" class="layui-input" type="text">

                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">对账日期:</label>
                            <div class="layui-input-block">
                                <input style="padding-right: 45px" disabled maxlength="300" value="<?php echo date('Y-m-d',$result['create_time']); ?>" lay-verify="required"  autocomplete="off" class="layui-input" type="text">

                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">账单名称:</label>
                            <div class="layui-input-block">
                                <input name="linkman"  disabled value="<?php echo $result['customer_bill_name']; ?>" autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div>   
                                 
                </div>
                <div class="layui-row layui-col-space10">
                     <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">项目名称:</label>
                            <div class="layui-input-block">
                                <input name="address" value="<?php echo $result['project_name']; ?>"  disabled placeholder="<?php echo $language_tag['index_source_showSupplier_input_address']; ?>" autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div> 
                   <div class="layui-col-md4">
                       <div class="layui-form-item">
                           <label class="layui-form-label">运费总计:</label>
                           <div class="layui-input-block">
                               <input name="zip_code"  value="<?php echo $result['money']; ?>"  disabled placeholder="<?php echo $language_tag['index_source_showSupplier_input_zip_code']; ?>" autocomplete="off" class="layui-input" type="text">
                           </div>
                       </div>
                   </div>                 
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">总赔款:</label>
                            <div class="layui-input-block">
                                <input name="phone" value="<?php echo $result['abnormal_money']; ?>"  disabled placeholder="<?php echo $language_tag['index_source_showSupplier_input_phone']; ?>" autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div>           
                  
                    
                           
                </div>
                <div class="layui-row layui-col-space10">
                     <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">开票金额:</label>
                            <div class="layui-input-block">
                                <input name="phone" value="<?php echo $result['customer_bill_money']; ?>"  disabled  autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div>      				
                     <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">税率:</label>
                            <div class="layui-input-block">
                                <input name="address" value="<?php echo $result['customer_bill_tax']; ?>"  disabled autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div> 
                      <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">是否含税:</label>
                            <div class="layui-input-block">
                                <input name="address" value="<?php if($result['customer_bill_tax_type'] == 1): ?>否<?php else: ?>是<?php endif; ?>"  disabled autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div> 
                      <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">支付方式:</label>
                            <div class="layui-input-block">
                                <input name="address" value="<?php if($result['pay_type'] == 1): ?>现付<?php elseif($result['pay_type'] == 2): ?>到付<?php else: ?>月结<?php endif; ?>"  disabled autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div>   					
                </div>	
          
 
    
               					
				<div class='layui-form-item'>
	<table class="layui-table">
  <colgroup>
    <col width="150">
    <col width="200">
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>运单号</th>
      <th>收入</th>
      <th>赔款</th>
    </tr> 
  </thead>
  <tbody>
  <?php if(is_array($result['info']) || $result['info'] instanceof \think\Collection || $result['info'] instanceof \think\Paginator): if( count($result['info'])==0 ) : echo "" ;else: foreach($result['info'] as $key=>$v): ?>
    <tr>
      <td><?php echo $v['orders_number']; ?></td>
      <td><?php echo $v['money']; ?></td>
      <td><?php echo $v['abnormal_info'][0]['abnormal_money']; ?></td>
    </tr>
<?php endforeach; endif; else: echo "" ;endif; ?>
  </tbody>
</table>			
				
				
				</div>
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">是否确认:</label>
                            <div class="layui-input-block">
	                   			<select name="is_back"  lay-filter="is_back" >
									<option value="1"  >确认</option>
									<option value="2"  >驳回</option>
							</select>		                            </div>
                        </div>
                    </div>      

         		<div id='bohui_fou_html'>
        
					<!-- 
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">发票类型:</label>
                            <div class="layui-input-block">
	                   			<select name="invoice_type" >
									<option value="1"  >增值税专票</option>
									<option value="2"  >增值税普票</option>
							</select>		
                            </div>
                        </div>
                    </div>                       
    -->
	
                   
   
                   </div>                     
                    <div id='bohui_shi_html' style='display:none'>
                      <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label">备注:</label>
                            <div class="layui-input-block">
                                <input name="back_remark" value="<?php echo $result['back_remark']; ?>"  id='fax_money'  placeholder="<?php echo $language_tag['index_source_showSupplier_input_phone']; ?>" autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div>                  
                
                </div>              
                <div class="layui-form-item">
                    <div class="all-button-center">
					<input type='hidden' value='<?php echo $result['customer_bill_number']; ?>' name='customer_bill_number' />
                        <button class="layui-btn nav-submit" lay-submit="" lay-filter="formDemo" id="language_add_button">提交</button>
                        <a href='javascript:history.back(-1)'><button type="button" class="layui-btn layui-btn-primary">返回</button></a>
                        <input type="hidden" name="customer_bill_id" value="<?php echo $result['customer_bill_id']; ?>">
                    </div>
                </div>
          
                
                

                
            </form>
        </div>
    </div>


</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script >

layer = layui.layer
form = layui.form;



form.on('select(is_back)', function(data){
	value = data.value
	if(value==1){
		$('#bohui_shi_html').hide();
		$('#bohui_fou_html').show();	
	}else{
		$('#bohui_shi_html').show();
		$('#bohui_fou_html').hide();
	}

});

//点击供应商显示车牌号AJAX
form.on('select(fax)', function(data){
	value = data.value
	
	if(value=='0' || value=='收据' || value=='无'){
		$('#fax_money').val('0')
		$('#real_money').val($('#kaipiao').val())
	}else{
		var zong = $('#kaipiao').val();
		var ruzhang_money = zong/(1+value*0.01);
		ruzhang_money =Math.round(ruzhang_money * 100) / 100
		$('#real_money').val(ruzhang_money);
		$('#fax_money').val((zong-ruzhang_money).toFixed(2))
	}

});


function kaipiaoEdit(){
	
    $.ajax({
        type: "post",
        url: "/financetmsprice/updatepricereceiptAjax",
        data: $('#fax_table').serializeArray(),
        dataType: "json",
        success: function(data){
        
        	if(data.code!=200){
        		layer.msg(data.msg)
        		return false;
        	}else if(data.code==200){
        		location.href = '/bill/showCustomerBillManage/status/2';
        	}
       },
       error:function(XMLHttpRequest, textStatus, errorThrown){
    	   layer.msg('验证失败')
       }
    });	
	
	return false;
}
</script>

</body>
</html>
