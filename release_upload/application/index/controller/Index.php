<?php
namespace app\index\controller;
use app\index\controller\Base;
use think\Request;
use think\config;
class Index extends Base
{

	//上传图片
    public function uploadImages()
    {
	
    	$file = request()->file('file');
		
		$request = Request::instance();
    	if($file){
    	
    		$info = $file->move(ROOT_PATH . 'public' . DS . 'static'.DS.'uploads'.DS.'images');
    		
    		if($info){
    			$domain = config('domain')['domain_name'].":".config('domain')['port'];
    			$img_name = $domain. DS . 'static'.DS.'uploads'.DS.'images'.DS.$info->getSaveName();
				
    			$this->outPut($img_name);
    		}else{
    		
    			$this->outPut([],'保存图片失败');
    		}
    		//  echo $info->getExtension();
    		// 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
    		// echo $info->getSaveName();
    		// 输出 42a79759f284b767dfcb2a0197904287.jpg
    		//  echo $info->getFilename();
    	}else{
    		// 上传失败获取错误信息
    		echo $this->outPut([],$file->getError());
 
    	}    	
    	
    }

    //导游回执单上传文件
    public function uploadImages2()
    {
    	
        $file = request()->file('file');
        
        $request = Request::instance();
        if($file){
        
            $info = $file->move(ROOT_PATH . 'public' . DS . 'static'.DS.'uploads'.DS.'file'.DS.'team_product_tour_guide_jounery');
            
            if($info){
                $domain = config('domain')['domain_name'].":".config('domain')['port'];
                $img_name = $domain. DS . 'static'.DS.'uploads'.DS.'file'.DS.'team_product_tour_guide_jounery'.DS.$info->getSaveName();
                
                $this->outPut($img_name);
            }else{
            
                $this->outPut([],'保存文件失败');
            }
            //  echo $info->getExtension();
            // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
            // echo $info->getSaveName();
            // 输出 42a79759f284b767dfcb2a0197904287.jpg
            //  echo $info->getFilename();
        }else{
            // 上传失败获取错误信息
            echo $this->outPut([],$file->getError());
 
        }       
        
    }


    //游客回执单上传文件
    public function uploadImages3()
    {
    
        $file = request()->file('file');
        
        $request = Request::instance();
        if($file){
        
            $info = $file->move(ROOT_PATH . 'public' . DS . 'static'.DS.'uploads'.DS.'file'.DS.'company_order_customer_tour_guide_jounery');
            
            if($info){
                $domain = config('domain')['domain_name'].":".config('domain')['port'];
                $img_name = $domain. DS . 'static'.DS.'uploads'.DS.'file'.DS.'company_order_customer_tour_guide_jounery'.DS.$info->getSaveName();
                
                $this->outPut($img_name);
            }else{
            
                $this->outPut([],'保存文件失败');
            }
            //  echo $info->getExtension();
            // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
            // echo $info->getSaveName();
            // 输出 42a79759f284b767dfcb2a0197904287.jpg
            //  echo $info->getFilename();
        }else{
            // 上传失败获取错误信息
            echo $this->outPut([],$file->getError());
 
        }       
        
    }
	
	
	//上传文件包含所有
	
	//上传图片
    public function uploadAll()
    {
	
    	$file = request()->file('file');
		
		$request = Request::instance();
    	if($file){
    	
    		$info = $file->move(ROOT_PATH . 'public' . DS . 'static'.DS.'uploads'.DS.'file');
    		
    		if($info){
    			$domain = config('domain')['domain_name'].":".config('domain')['port'];
    			$img_name = $domain. DS . 'static'.DS.'uploads'.DS.'file'.DS.$info->getSaveName();
				
    			$this->outPut($img_name);
    		}else{
    		
    			$this->outPut([],'保存图片失败');
    		}
    		//  echo $info->getExtension();
    		// 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
    		// echo $info->getSaveName();
    		// 输出 42a79759f284b767dfcb2a0197904287.jpg
    		//  echo $info->getFilename();
    	}else{
    		// 上传失败获取错误信息
    		echo $this->outPut([],$file->getError());
 
    	}    	
    	
    }	
	
}
