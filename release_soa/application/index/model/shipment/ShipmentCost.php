<?php

namespace app\index\model\shipment;
use think\Model;
use app\common\help\Help;

use think\config;
use think\Db;
class ShipmentCost extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'shipment_cost';

    public function initialize()
    {
    

    	parent::initialize();
    
    }


    /**
     * 获取发运
     * 胡
     */
    public function getShipmentCost($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "shipment_cost.status =1 ";

    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment_cost.shipment_uuid= '".$params['shipment_uuid']."'";
    	}   

  
        if($is_count==true){
            $result = $this->table("shipment_cost")->alias("shipment_cost")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("shipment_cost")->alias('shipment_cost')->
				join("cost","cost.cost_id= shipment_cost.cost_id")->


                where($data)->limit($page, $page_size)->order('shipment_cost.create_time desc')->
                field([
                		'shipment_cost.*','cost.cost_name'

		                ])->select();
            }else{
                $result = $this->table("shipment_cost")->alias('shipment_cost')->
				join("cost","cost.cost_id= shipment_cost.cost_id")->

                where($data)->order('shipment_cost.create_time desc')->
                field([
						'shipment_cost.*','cost.cost_name'
                		
                ])->select();
            }
        }
    	 
 
    
    	return $result;
    
    }



}