<?php

namespace app\index\model\shipment;
use think\Model;
use app\common\help\Help;

use think\config;
use think\Db;
class ShipmentAbnormal extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'shipment_abnormal';

    public function initialize()
    {
    

    	parent::initialize();
    
    }

    /**
     * 添加发运异常
     * 胡
     */
    public function addShipmentAbnormal($params){
    	$t = time();
    	
    	//$data['shipment_uuid'] = $params['shipment_uuid'];

   

    	//$data['shipment_abnormal_money'] = $params['shipment_abnormal_money'];    		


    	

    	
    	$data['create_time'] = $t;
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = 1;


	
    	Db::startTrans();
    	try{
    		$abnormal_money = 0;
			for($i=0;$i<count($params['data']);$i++){
				if($params['data'][$i]['is_type']==1){//代表干线
					//开始计算合同号
					//先计算当天的合同数量
					$dayCountParams = " FROM_UNIXTIME(create_time,'%Y%m') =".date('Ym');
					$dayCount = Db::name('shipment_abnormal')->where($dayCountParams)->count();
					$data['shipment_abnormal_number'] = "LP".date("Ym").str_pad(($dayCount+1),4,0,STR_PAD_LEFT);
					$data['shipment_uuid'] = $params['data'][$i]['shipment_uuid'];
					$data['shipment_abnormal_money'] =$params['data'][$i]['shipment_abnormal_money']; 
					$abnormal_money+=$data['shipment_abnormal_money'];
					$data['orders_abnormal_number'] = $params['abnormal_number'];				
					$pk_id = Db::name('shipment_abnormal')->insertGetId($data);		

				
					
				}else{
					//首先判断shipment_uuid 是否存在shipment_abnormal 
					
					//如果存在则进行数据的修改。。如果不存在则添加
					$shipmentAbnormalResult = Db::name('shipment_abnormal')->where("shipment_uuid ='".$params['data'][$i]['shipment_uuid']."'")->find();	
					if(!empty($shipmentAbnormalResult)){
						//第一先进行修改赔款金额
						//再修改短驳里的赔款金额
						$shortBargeWhere['shipment_uuid'] =$params['data'][$i]['shipment_uuid'];
						$shortBargeWhere['orders_number'] =$params['orders_number'];
						$shortBargeParams['short_barge_abnormal_money']=$params['data'][$i]['shipment_abnormal_money'];  
						$abnormal_money+=$shortBargeParams['short_barge_abnormal_money'];
						
						Db::name('shipment_short_barge')->where($shortBargeWhere)->update($shortBargeParams);	
						
						
						//首先计算该发运下总共有多少赔款
						
						$abnormal_money = Db::name('shipment_short_barge')->where("status = 1 and shipment_uuid ='".$params['data'][$i]['shipment_uuid']."'")->field(['sum(short_barge_abnormal_money) as abnormal_money'])->find();
						//然后再更新
						$update['shipment_abnormal_money'] = $abnormal_money['abnormal_money'];
						Db::name('shipment_abnormal')->where("shipment_uuid ='".$params['data'][$i]['shipment_uuid']."'")->update($update);
							
						//第二shipment_abnormal 的总金额进行修改
					}else{
						//添加数据
						$dayCountParams = " FROM_UNIXTIME(create_time,'%Y%m') =".date('Ym');
						$dayCount = Db::name('shipment_abnormal')->where($dayCountParams)->count();
						$data['shipment_abnormal_number'] = "LP".date("Ym").str_pad(($dayCount+1),4,0,STR_PAD_LEFT);
						$data['shipment_uuid'] = $params['data'][$i]['shipment_uuid'];
						$data['shipment_abnormal_money'] =$params['data'][$i]['shipment_abnormal_money'];   
						$data['orders_abnormal_number'] = $params['abnormal_number'];		
						$pk_id = Db::name('shipment_abnormal')->insertGetId($data);		
						//再修改短驳里的赔款金额
						$shortBargeWhere['shipment_uuid'] =$params['data'][$i]['shipment_uuid'];
						$shortBargeWhere['orders_number'] =$params['orders_number'];
						$shortBargeParams['short_barge_abnormal_money']=$params['data'][$i]['shipment_abnormal_money']; 
						$abnormal_money+=$shortBargeParams['short_barge_abnormal_money'];					
						Db::name('shipment_short_barge')->where($shortBargeWhere)->update($shortBargeParams);						
					}
					
				}
			
			}
			
						
			$orders_abnormal_where['supplier_abnormal_money'] = $abnormal_money;
			$orders_abnormal_where['shipment_status'] = 2;
			$orders_abnormal_params['abnormal_number'] = $params['abnormal_number'];
			
	
			Db::name('orders_abnormal')->where($orders_abnormal_params)->update($orders_abnormal_where);
	
			$shipment_abnormal_result = Db::name('shipment_abnormal')->where("status = 1 and orders_abnormal_number ='".$params['abnormal_number']."'")->select();
			 for($i=0;$i<count($shipment_abnormal_result);$i++){
				$shipment_abnormal_money = Db::name('shipment_abnormal')->where("status = 1 and shipment_uuid ='".$shipment_abnormal_result[$i]['shipment_uuid']."'")->field(['sum(shipment_abnormal_money) as abnormal_money'])->find();
				 
				//获取shipmen信息 判断是否是短驳，如果是短驳则不需要填充
				$shipment_result = Db::name('shipment')->where("status = 1 and shipment_uuid ='".$shipment_abnormal_result[$i]['shipment_uuid']."'")->find();
			
				if($shipment_result['shipment_type']!=3){
					$shipment_update['shipment_abnormal_money'] = $shipment_abnormal_money['abnormal_money'];
					$shipment_where['shipment_uuid'] = $shipment_abnormal_result[$i]['shipment_uuid'];
		
					Db::name('shipment')->where($shipment_where)->update($shipment_update);	
					
				
					
				}
			}

    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}

    	return $result;
    }
    
    /**
     * 获取发运异常
     * 胡
     */
    public function getShipmentAbnormal($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "1=1 and shipment_abnormal.status = 1 ";

    	if(!empty($params['orders_abnormal_number'])){
    		$data.= " and shipment_abnormal.orders_abnormal_number = '".$params['orders_abnormal_number']."'";
    	}   
    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment_abnormal.shipment_uuid = '".$params['shipment_uuid']."'";
    	}   		
    	if(is_numeric($params['shipment_type_is_not_3'])){
    		$data.= " and shipment.shipment_type != 3";
    	}   	
		if(is_numeric($params['shipment_type'])){
    		$data.= " and shipment.shipment_type = ".$params['shipment_type'];
    	}   
	
        if($is_count==true){
            $result = $this->table("shipment_abnormal")->alias("shipment_abnormal")->
                join("shipment","shipment.shipment_uuid = shipment_abnormal.shipment_uuid")->
                  join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
            where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("shipment_abnormal")->alias('shipment_abnormal')->
				
                join("shipment","shipment.shipment_uuid = shipment_abnormal.shipment_uuid",'left')->
                join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
            
                where($data)->limit($page, $page_size)->order('shipment.create_time desc')->
                field([
                		'shipment_abnormal.*',
                		'shipment.finance_number','shipment.shipment_type','shipment.orders_number',
                		'shipment.shipment_time','shipment.pay_all_money',
						'shipment.supplier_uuid',
                		'supplier.supplier_name','shipment.shipment_type',
                		
                		'abnormal_type.abnormal_type_name',
                		"(select nickname  from user where user.user_id = shipment_abnormal.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = shipment_abnormal.update_user_id)"=> 'update_user_name'
                		
		                ])->select();
            }else{
                $result = $this->table("shipment_abnormal")->alias('shipment_abnormal')->
                join("shipment","shipment.shipment_uuid = shipment_abnormal.shipment_uuid")->
                  join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
               
                  
                where($data)->order('shipment_abnormal.create_time desc')->
                field([
                		'shipment_abnormal.*',
                		'shipment.finance_number','shipment.shipment_type','shipment.orders_number',
                		'shipment.shipment_time','shipment.pay_all_money',
						'shipment.supplier_uuid',
                		'supplier.supplier_name','shipment.shipment_type',
                		
                		"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
                    	])->select();
            }
        }
    	 
 	
    
    	return $result;
    
    }
    
    /**
     * 修改异常
     */
    public function updateShipmentAbnormal($params){
    
    	$t = time();
    	

    	$data['update_time'] = $t;

    	$data['update_user_id'] = $params['user_id'];

    	
    	$data['update_time'] = $t;

    	$data['update_user_id'] = $params['user_id'];



    	Db::startTrans();
    	try{
			if($params['shipment_status']==3){
				$abnormal_number_where['abnormal_number'] = $params['orders_abnormal_number'];
				$abnormal_number_params['shipment_status'] = 3;
				Db::name('orders_abnormal')->where($abnormal_number_where)->update($abnormal_number_params);
				$shipment_abnormal_where['orders_abnormal_number'] = $params['orders_abnormal_number'];
				$shipment_abnormal_where['status'] = 1;
				$shipment_abnormal_params['shipment_abnormal_status'] = 2;
				Db::name('shipment_abnormal')->where($shipment_abnormal_where)->update($shipment_abnormal_params);
			
				

			}else{
				$abnormal_money =0;
				//修改干线
			
				for($i=0;$i<count($params['shipmentData']);$i++){
					$shipment_abnormalWhere['shipment_abnormal_id'] = $params['shipmentData'][$i]['shipment_abnormal_id'];
					$shipment_abnormalParams['shipment_abnormal_money'] = $params['shipmentData'][$i]['shipment_abnormal_money'];
					$abnormal_money+=$params['shipmentData'][$i]['shipment_abnormal_money'];
					Db::name('shipment_abnormal')->where($shipment_abnormalWhere)->update($shipment_abnormalParams);
					
					//$this->query("update shipment set shipment.shipment_abnormal_money = (select sum(shipment_abnormal_money) from supplier_abnormal where supplier_abnormal.shipment_uuid = '".$params['shipmentData'][$i]['shipment_uuid']."')");
				}
				//修改短驳
				for($i=0;$i<count($params['shortBargeData']);$i++){
					$shortBargeWhere['shipment_short_barge_id'] = $params['shortBargeData'][$i]['shipment_short_barge_id'];
					$shortBargeParams['short_barge_abnormal_money'] = $params['shortBargeData'][$i]['shipment_abnormal_money'];
					$abnormal_money+=$params['shortBargeData'][$i]['shipment_abnormal_money'];
					Db::name('shipment_short_barge')->where($shortBargeWhere)->update($shortBargeParams);			
				}
				
				$orders_number_where['abnormal_number']=$params['abnormal_number'];
				$orders_number_params['supplier_abnormal_money'] = $abnormal_money;
				Db::name('orders_abnormal')->where($orders_number_where)->update($orders_number_params);
				
				//通过运单号查询 shipment_uuid
				$shortBargeResult = Db::name('shipment_short_barge')->where("status = 1 and orders_number ='".$params['orders_number']."'")->select();
				for($i=0;$i<count($shortBargeResult);$i++){
					//通过shipment_uuid 查询所有赔款数据
					$abnormal_money = Db::name('shipment_short_barge')->where("status = 1 and shipment_uuid ='".$shortBargeResult[$i]['shipment_uuid']."'")->field(['sum(short_barge_abnormal_money) as abnormal_money'])->find();
					$update['shipment_abnormal_money'] = $abnormal_money['abnormal_money'];
					Db::name('shipment_abnormal')->where("shipment_uuid ='".$shortBargeResult[$i]['shipment_uuid']."'")->update($update);	
				}				
				
				//通过运单异常编号把所有发运异常金额填充
				
				 $shipment_abnormal_result = Db::name('shipment_abnormal')->where("status = 1 and orders_abnormal_number ='".$params['abnormal_number']."'")->select();
				 for($i=0;$i<count($shipment_abnormal_result);$i++){
					$shipment_abnormal_money = Db::name('shipment_abnormal')->where("status = 1 and shipment_uuid ='".$shipment_abnormal_result[$i]['shipment_uuid']."'")->field(['sum(shipment_abnormal_money) as abnormal_money'])->find();
					 
					//获取shipmen信息 判断是否是短驳，如果是短驳则不需要填充
					$shipment_result = Db::name('shipment')->where("status = 1 and shipment_uuid ='".$shipment_abnormal_result[$i]['shipment_uuid']."'")->find();
				
					if($shipment_result['shipment_type']!=3){
						$shipment_update['shipment_abnormal_money'] = $shipment_abnormal_money['abnormal_money'];
						$shipment_where['shipment_uuid'] = $shipment_abnormal_result[$i]['shipment_uuid'];
			
						Db::name('shipment')->where($shipment_where)->update($shipment_update);	
						
					
						
					}
				}
			
				
			}


			$result=1;
			Db::commit();
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
		
    	return $result;
    }
	//删除异常
	public function delShipmentAbnormal($params){
    	Db::startTrans();
    	try{
			
		
			
			$shipmentAbnormalResult = Db::name('shipment_abnormal')->join("shipment",'shipment.shipment_uuid = shipment_abnormal.shipment_uuid')->where("shipment_abnormal.status = 1 and shipment_abnormal.orders_abnormal_number='".$params['orders_abnormal_number']."'")->field(["shipment_abnormal.*",'shipment.shipment_type'])->select();
			
			for($i=0;$i<count($shipmentAbnormalResult);$i++){
				if($shipmentAbnormalResult[$i]['shipment_type']==3){//代表短驳
						//第一先进行修改赔款金额
						//再修改短驳里的赔款金额
						$shortBargeWhere['shipment_uuid'] =$shipmentAbnormalResult[$i]['shipment_uuid'];
						$shortBargeWhere['orders_number'] =$params['orders_number'];
						$shortBargeParams['short_barge_abnormal_money']=0;  
						Db::name('shipment_short_barge')->where($shortBargeWhere)->update($shortBargeParams);	
						
						//首先计算该发运下总共有多少赔款
						
						$abnormal_money = Db::name('shipment_short_barge')->where("status = 1 and shipment_uuid ='".$shipmentAbnormalResult[$i]['shipment_uuid']."'")->field(['sum(short_barge_abnormal_money) as abnormal_money'])->find();
						//然后再更新
						$update['shipment_abnormal_money'] = $abnormal_money['abnormal_money'];
						Db::name('shipment_abnormal')->where("shipment_uuid ='".$shipmentAbnormalResult[$i]['shipment_uuid']."'")->update($update);				
					
					
					
				}else{
					$shipmentWhere['shipment_abnormal_id'] = $shipmentAbnormalResult[$i]['shipment_abnormal_id'];
					$shipmentUpdate['status'] =0;
					Db::name('shipment_abnormal')->where($shipmentWhere)->update($shipmentUpdate);
				}
				
			}
			
			$ordersParams['shipment_status'] = 1;
			$ordersWhere['orders_number'] = $params['orders_number'];
			
			Db::name('orders_abnormal')->where($ordersWhere)->update($ordersParams);
	
    		$result = 1;
    		// 提交事务
			

    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;		
		
		
	}
	//驾驶舱
	public function getIndexShipmentAbnormal($params){
		$data = '1=1 and shipment_abnormal.status =1';
		if(is_numeric($params['month'])){
			$data.= " and FROM_UNIXTIME(shipment.shipment_time,'%Y%m') = FROM_UNIXTIME(".$params['month'].",'%Y%m')";
	
		}				
		if(is_numeric($params['year'])){
			$data.= " and FROM_UNIXTIME(shipment.shipment_time,'%Y') = FROM_UNIXTIME(".$params['year'].",'%Y')";
	
		}										
		$result =  $this->table('shipment_abnormal')->join("shipment",'shipment.shipment_uuid = shipment_abnormal.shipment_uuid and shipment.status = 1')->where($data)->field([
				"sum(shipment_abnormal.shipment_abnormal_money) as sum_shipment_abnormal_money",
		
                ])->select();
	
		return $result;
		
	}		
}