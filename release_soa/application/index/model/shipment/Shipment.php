<?php

namespace app\index\model\shipment;
use think\Model;
use app\common\help\Help;

use think\config;
use think\Db;
class Shipment extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'supplier_line';

    public function initialize()
    {
    

    	parent::initialize();
    
    }

    /**
     * 添加发运
     * 胡
     */
    public function addShipment($params){
    	$t = time();
    	if(is_numeric($params['shipment_time'])){
    		$data['shipment_time'] = $params['shipment_time'];
    	}
    	if(is_numeric($params['need_time'])){
    		$data['need_time'] = $params['need_time'];
    	}
    	if(isset($params['supplier_uuid'])){
    		$data['supplier_uuid'] = $params['supplier_uuid'];
    	}
    	if(isset($params['supplier_line_uuid'])){
    		$data['supplier_line_uuid'] = $params['supplier_line_uuid'];
    	}
    	
    	if(isset($params['supplier_line_uuid'])){
    		$data['supplier_line_uuid'] = $params['supplier_line_uuid'];
    	}
    	
    	$data['shipment_type'] = $params['shipment_type'];
    	
    	if(!empty($params['orders_number'])){
    		$data['orders_number'] = $params['orders_number'];    		
    	}
		if(!empty($params['supplier_shipment_number'])){
			$data['supplier_shipment_number'] = $params['supplier_shipment_number'];
		}
    	
    	$data['pay_all_money'] = $params['pay_all_money'];
    	$data['shipment_remark'] = $params['shipment_remark'];
		$data['shipment_pay_type'] = $params['shipment_pay_type'];
    	$data['create_time'] = $t;
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = 1;


    	$data['shipment_uuid']= Help::getUuid();

    	Db::startTrans();
    	try{
    		//$data['finance_number']
    		
    		//开始计算合同号
    		//先计算当天的合同数量
    		$dayCountParams = "shipment_type in (1,2) and FROM_UNIXTIME(create_time,'%Y%m') =".date('Ym');
    		$dayCount = Db::name('shipment')->where($dayCountParams)->count();

			
			//error_log(print_r($dayCount,1);
			
    		$data['finance_number'] = "AFY".date("ym").str_pad(($dayCount+1),4,0,STR_PAD_LEFT);
    		//开始遍历
					
			$bianliData['finance_number'] = $data['finance_number'];
			while(Db::name('shipment')->where($bianliData)->count()){
				$dayCount++;
				$data['finance_number'] = "AFY".date("ym").str_pad(($dayCount+1),4,0,STR_PAD_LEFT);
				$bianliData['finance_number'] = $data['finance_number'];
			}
			 
			
    		$pk_id = Db::name('shipment')->insertGetId($data);
			

			//开始插入成本
    		$shipmentCost = $params['shipment_cost'];
    		for($i=0;$i<count($shipmentCost);$i++){
    			$shipmentCostParams['shipment_uuid'] = $data['shipment_uuid'];
    			$shipmentCostParams['shipment_cost_uuid'] = Help::getUuid();
    			$shipmentCostParams['cost_id'] = $shipmentCost[$i]['cost_id'];
    			$shipmentCostParams['cost_money'] = $shipmentCost[$i]['cost_money'];
    			$shipmentCostParams['create_time'] = $t;
    			$shipmentCostParams['update_time'] = $t;
    			$shipmentCostParams['create_user_id'] = $params['user_id'];
    			$shipmentCostParams['update_user_id'] = $params['user_id'];
    			$shipmentCostParams['status'] = 1;
    			Db::name('shipment_cost')->insertGetId($shipmentCostParams);
    		}
    		//开始插入商品
    		$shipmentGoods = $params['shipment_goods'];
    		for($i=0;$i<count($shipmentGoods);$i++){
    			$shipmentGoodsParams['shipment_uuid'] = $data['shipment_uuid'];
    			$shipmentGoodsParams['shipment_goods_uuid'] = Help::getUuid();
    			$shipmentGoodsParams['goods_id'] = $shipmentGoods[$i]['goods_id'];
    			$shipmentGoodsParams['shipment_count'] = $shipmentGoods[$i]['shipment_count'];
    			$shipmentGoodsParams['shipment_pack_unit'] = $shipmentGoods[$i]['shipment_pack_unit'];
    			$shipmentGoodsParams['shipment_pack_count'] = $shipmentGoods[$i]['shipment_pack_count'];    	
    			$shipmentGoodsParams['shipment_weight'] = $shipmentGoods[$i]['shipment_weight'];
    			$shipmentGoodsParams['shipment_volume'] = $shipmentGoods[$i]['shipment_volume'];   			
    			$shipmentGoodsParams['shipment_charge_type'] = $shipmentGoods[$i]['shipment_charge_type'];
    			$shipmentGoodsParams['unit_price'] = $shipmentGoods[$i]['unit_price'];
    			$shipmentGoodsParams['shipment_money'] = $shipmentGoods[$i]['shipment_money'];    			
    			$shipmentGoodsParams['create_time'] = $t;
    			$shipmentGoodsParams['update_time'] = $t;
    			$shipmentGoodsParams['create_user_id'] = $params['user_id'];
    			$shipmentGoodsParams['update_user_id'] = $params['user_id'];
    			$shipmentGoodsParams['status'] = 1;
    			Db::name('shipment_goods')->insertGetId($shipmentGoodsParams);
    		}   		
    		//开始插入支付方式
    		$shipmentPay = $params['shipment_pay'];
    		for($i=0;$i<count($shipmentPay);$i++){
    			$shipmentPayParams['shipment_uuid'] = $data['shipment_uuid'];
    			$shipmentPayParams['shipment_pay_uuid'] = Help::getUuid();
    			$shipmentPayParams['pay_type'] = $shipmentPay[$i]['pay_type'];
    			$shipmentPayParams['pay_money'] = $shipmentPay[$i]['pay_money'];
    			$shipmentPayParams['create_time'] = $t;
    			$shipmentPayParams['update_time'] = $t;
    			$shipmentPayParams['create_user_id'] = $params['user_id'];
    			$shipmentPayParams['update_user_id'] = $params['user_id'];
    			$shipmentPayParams['status'] = 1;
    			Db::name('shipment_pay')->insertGetId($shipmentPayParams);
    		}    		
    		

    		$ordersParams = [];
    		//假如shipment_status = 1说明结束安排
    		if($params['shipment_status'] ==1){

   
    			$ordersParams['shipment_status'] = 1;
    		}
    		if($params['is_short_barge']==1){
    
    			$ordersParams['is_short_barge'] = 1;
    		}   		
    		$ordersWhere['orders_number'] = $params['orders_number'];
			$ordersParams['order_status'] = 2;
    		Db::name('orders')->where($ordersWhere)->update($ordersParams);
    		
    		
    		
			
			$updateSupplier['orders_number'] = $params['orders_number'];
			$this->updateOrdersSupplier($updateSupplier);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
	
    	return $result;
    }
    //修改订单的表把承运商UUID插入进去
	public function updateOrdersSupplier($params){
	
		$where['orders_number'] = $params['orders_number'];
		$where['status'] = 1;
		$result = Db::name('shipment')->where($where)->select();
		$key ='';
		$shipment_number='';
		for($i=0;$i<count($result);$i++){
			if($i==0){
				$key=$result[$i]['supplier_uuid'];
				$shipment_number=$result[$i]['supplier_shipment_number'];
			}else{
				if(!empty($result[$i]['supplier_shipment_number'])){
							$shipment_number.=','.$result[$i]['supplier_shipment_number'];
				}
				$key.=','.$result[$i]['supplier_uuid'];
		
			}
		}
		$update['supplier_uuid'] = $key;
		$update['shipment_number'] = $shipment_number;
		Db::name('orders')->where($where)->update($update);
		
	}
	
    /**
     * 获取发运
     * 胡
     */
    public function getShipment($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "1=1 ";

    	if(is_numeric($params['status'])){
    		$data.= " and shipment.status= ".$params['status'];
    	}
    	if(is_numeric($params['project_id'])){
    		$data.= " and project.project_id= ".$params['project_id'];
    	}
    	if(!empty($params['orders_number'])){
    		$data.= " and shipment.orders_number like '%".$params['orders_number']."%'";
    	}
    	if(!empty($params['finance_number'])){
    		$data.= " and shipment.finance_number like '%".$params['finance_number']."%'";
    	}

    	if(is_numeric($params['start_shipment_time'])){
    		$data.= " and shipment.shipment_time >= ".$params['start_shipment_time'];
    	}
    	if(is_numeric($params['end_shipment_time'])){
    		$data.= " and shipment.shipment_time <= ".$params['end_shipment_time'];
    	}
		if(is_numeric($params['start_pickup_time'])){
    		$data.=" and orders.pickup_time >= ".$params['start_pickup_time'];
    	}
    	if(is_numeric($params['end_pickup_time'])){
    		$data.=" and orders.pickup_time <= ".$params['end_pickup_time'];
    	}	
    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment.shipment_uuid= '".$params['shipment_uuid']."'";
    	}

        if(!empty($params['supplier_shipment_number'])){ //专线编号
            $data.= " and shipment.supplier_shipment_number= '".$params['supplier_shipment_number']."'";
        }
        if(!empty($params['supplier_name'])){ //承运商
            $data.= " and supplier.supplier_name like '%".$params['supplier_name']."%'";
        }
        if(!empty($params['send_goods_company'])){ //发货方
            $data.=' and orders.send_goods_company like "%'.$params['send_goods_company'].'%"';
        }
        if(!empty($params['accept_goods_company'])){ //收货方
            $data.=' and orders.accept_goods_company like"%'.$params['accept_goods_company'].'%"';
        }
        if(!empty($params['send_location_name'])){ //发站
            $data.=' and city2.city_name like "%'.$params['send_location_name'].'%"';
        }
        if(!empty($params['accept_location_name'])){ //到站
            $data.=' and city.city_name like"%'.$params['accept_location_name'].'%"';
        }
    	if(is_numeric($params['choose_company_id'])){
    		$data.=" and shipment.company_id = ".$params['choose_company_id'];
    	}	


        if($is_count==true){
            $result = $this->table("shipment")->alias("shipment")->
            	join("orders","shipment.orders_number = orders.orders_number")->
            	join("project","project.project_id= orders.project_id")->
                join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
                join("city city", 'city.city_id = orders.accept_location_id')->
                join("city city2", 'city2.city_id = orders.send_location_id')->
 
            where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("shipment")->alias('shipment')->
				join("orders","shipment.orders_number = orders.orders_number and orders.status = 1")->
				join("project","project.project_id= orders.project_id")->
                join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
                join("city city", 'city.city_id = orders.accept_location_id')->
                join("city city2", 'city2.city_id = orders.send_location_id')->

                where($data)->limit($page, $page_size)->order('shipment.create_time desc')->
                field([
                		'shipment.shipment_uuid','shipment.finance_number','shipment.orders_number',
                		'shipment.shipment_time','shipment.need_time','shipment.supplier_uuid','shipment.supplier_line_uuid',
                		'shipment.supplier_shipment_number','shipment.shipment_type','shipment.pay_all_money','shipment.shipment_remark','shipment.company_id',
                		'shipment.create_time','shipment.create_user_id','shipment.update_time','shipment.update_user_id',
                		'shipment.status','shipment.is_print','shipment.shipment_finance_status','shipment.shipment_pay_type',
						
                		'orders.accept_province_id','orders.accept_city_id','orders.accept_area_id','orders.accept_address',
                		'orders.send_goods_company','orders.accept_goods_company',
						'orders.is_short_barge','orders.pickup_time',
                		'project.project_name','city.city_name'=>'accept_location_name','city2.city_name'=>'send_location_name',
						"supplier.supplier_name","supplier.cellphone","supplier.linkman","supplier.phone",
 
	                	"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
                		
		                ])->select();
            }else{
                $result = $this->table("shipment")->alias('shipment')->
                join("orders","shipment.orders_number = orders.orders_number and orders.status = 1")->
                join("project","project.project_id= orders.project_id")->
                join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
                join("city city", 'city.city_id = orders.accept_location_id')->
                join("city city2", 'city2.city_id = orders.send_location_id')->
                join("accept_goods accept_goods", 'accept_goods.accept_goods_id= orders.accept_goods_id','left')->
                join("send_goods send_goods", 'send_goods.send_goods_id= orders.send_goods_id','left')->
                where($data)->order('create_time desc')->
                field([
                		'shipment.shipment_uuid','shipment.finance_number','shipment.orders_number',
                		'shipment.shipment_time','shipment.need_time','shipment.supplier_uuid','shipment.supplier_line_uuid',
                		'shipment.supplier_shipment_number','shipment.shipment_type','shipment.pay_all_money','shipment.shipment_remark','shipment.company_id',
                		'shipment.create_time','shipment.create_user_id','shipment.update_time','shipment.update_user_id',
                		'shipment.status','shipment.is_print','shipment.shipment_finance_status','shipment.shipment_pay_type',
                		'orders.accept_province_id','orders.accept_city_id','orders.accept_area_id','orders.accept_address',
						'orders.send_goods_company','orders.accept_goods_company',
                		'orders.is_short_barge','orders.pickup_time',
                		'project.project_name','city.city_name'=>'accept_location_name','city2.city_name'=>'send_location_name',
                		"supplier.supplier_name","supplier.cellphone","supplier.linkman","supplier.phone",

	                	"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
                    	])->select();
            }
        }
    	 
 
    
    	return $result;
    
    }
    /**
     * 获取短驳
     * 胡
     */
    public function getShipmentShortBarge($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "shipment_type=3 ";
    
    	if(is_numeric($params['status'])){
    		$data.= " and shipment.status= ".$params['status'];
    	}
    	if(!empty($params['orders_number'])){
    		$data.= " and shipment.orders_number= '".$params['orders_number']."'";
    	}
    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment.shipment_uuid= '".$params['shipment_uuid']."'";
    	}
		
    	if($is_count==true){
    		$result = $this->table("shipment")->alias("shipment")->where($data)->count();
    	}else {
    		if ($is_page == true) {
    			$result = $this->table("shipment")->alias('shipment')->
 
    			join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
    			where($data)->limit($page, $page_size)->order('shipment.create_time desc')->
    			field([
    					'shipment.shipment_uuid','shipment.finance_number','shipment.distribution_type',
    					'shipment.shipment_time','shipment.need_time','shipment.supplier_uuid','shipment.supplier_line_uuid',
    					'shipment.supplier_shipment_number','shipment.shipment_type','shipment.pay_all_money','shipment.shipment_remark',
    					'shipment.create_time','shipment.create_user_id','shipment.update_time','shipment.update_user_id',
    					'shipment.status',

    
    					"supplier.supplier_name",
    					//"(select accept_goods_company from accept_goods where accept_goods_id = orders.accept_goods_id)"=>"accept_goods_company",
    					//"(select send_goods_company from send_goods where send_goods_id = orders.send_goods_id)"=>"send_goods_company",
    					"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
    
    			])->select();
    		}else{
    			$result = $this->table("shipment")->alias('shipment')->

    			join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
    			where($data)->order('create_time desc')->
    			field([
    					'shipment.shipment_uuid','shipment.finance_number','shipment.distribution_type',
    					'shipment.shipment_time','shipment.need_time','shipment.supplier_uuid','shipment.supplier_line_uuid',
    					'shipment.supplier_shipment_number','shipment.shipment_type','shipment.pay_all_money','shipment.shipment_remark',
    					'shipment.create_time','shipment.create_user_id','shipment.update_time','shipment.update_user_id',
    					'shipment.status',

    					"supplier.supplier_name",
    			//	"(select accept_goods_company from accept_goods where accept_goods_id = orders.accept_goods_id)"=>"accept_goods_company",
    				//	"(select send_goods_company from send_goods where send_goods_id = orders.send_goods_id)"=>"send_goods_company",
    					"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
    			])->select();
    		}
    	}
    
    
    
    	return $result;
    
    }
    /**
     * 获取发运
     * 胡
     */
    public function getShipmentInfo($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "1=1 ";
    
    	if(is_numeric($params['status'])){
    		$data.= " and shipment.status= ".$params['status'];
    	}
    	if(is_numeric($params['shipment_finance_status'])){
    		$data.= " and shipment.shipment_finance_status= ".$params['shipment_finance_status'];
    	}
		if(is_numeric($params['shipment_finance_status'])){
    		$data.= " and shipment.shipment_finance_status= ".$params['shipment_finance_status'];
    	}
    	if(is_numeric($params['project_id'])){
    		$data.= " and project.project_id= ".$params['project_id'];
    	}	
    	if(is_numeric($params['shipment_abnormal_status'])){
    		$data.= " and shipment.shipment_abnormal_status= ".$params['shipment_abnormal_status'];
    	}			
    	if(!empty($params['finance_number'])){
    		$data.= " and shipment.finance_number like '%".$params['finance_number']."%'";
    	}
    	if(!empty($params['orders_number'])){
    		$data.= " and orders.orders_number like '%".$params['orders_number']."%'";
    	}		
    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment.shipment_uuid= '".$params['shipment_uuid']."'";
    	}
    	if(!empty($params['supplier_uuid'])){
    		$data.= " and shipment.supplier_uuid= '".$params['supplier_uuid']."'";
    	}
    	if(!empty($params['supplier_name'])){
    		$data.= " and supplier.supplier_name like '%".$params['supplier_name']."%'";
    	}		
    	if(!empty($params['start_shipment_time'])){
    		$data.= " and shipment.shipment_time >= ".$params['start_shipment_time'];
    	}
    	if(!empty($params['end_shipment_time'])){
    		$data.= " and shipment.shipment_time <=".$params['end_shipment_time'];
    	}    	
    	if(!empty($params['shipment_uuid_string'])){
    		$data.= " and shipment.shipment_uuid in (".$params['shipment_uuid_string'].")";
    	}
    	if(!empty($params['shipment_id_string'])){
    		$data.= " and shipment.shipment_id in (".$params['shipment_id_string'].")";
    	}	
		if(is_numeric($params['start_pickup_time'])){
    		$data.= " and orders.pickup_time>= ".$params['start_pickup_time'];
    	}	
    	if(is_numeric($params['end_pickup_time'])){
    		$data.= " and orders.pickup_time<= ".$params['end_pickup_time'];
    	}			
    	if(is_numeric($params['choose_company_id'])){
    		$data.= " and shipment.company_id= ".$params['choose_company_id'];
    	}	
		if(is_numeric($params['shipment_finance_status_not_miss'])){
    		$data.=" and shipment.shipment_finance_status >1";
    	}	
		
    	if($is_count==true){
    		$result = $this->table("shipment")->alias("shipment")->
				join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
				join("orders","orders.orders_number = shipment.orders_number",'left')->
				join("project","project.project_id = orders.project_id",'left')->			
			where($data)->count();
    	}else {
    		if ($is_page == true) {
				
    			$result = $this->table("shipment")->alias('shipment')->  	
    			join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
				join("orders","orders.orders_number = shipment.orders_number",'left')->
				join("project","project.project_id = orders.project_id",'left')->
    			where($data)->limit($page, $page_size)->order('shipment.create_time desc')->
    			field([
    					'shipment.shipment_uuid','shipment.finance_number','shipment.orders_number',
    					'shipment.shipment_time','shipment.need_time','shipment.supplier_uuid','shipment.supplier_line_uuid',
    					'shipment.supplier_shipment_number','shipment.shipment_type','shipment.pay_all_money','shipment.shipment_remark',
    					'shipment.create_time','shipment.create_user_id','shipment.update_time','shipment.update_user_id',
    					'shipment.status',	'shipment.shipment_finance_status',
						'project.project_name','orders.pickup_time','shipment.shipment_id',
    
    					"supplier.supplier_name","orders.dfk",

    					"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
    
    			])->select();
				
    		}else{
    			$result = $this->table("shipment")->alias('shipment')->

    			join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
				join("orders","orders.orders_number = shipment.orders_number",'left')->
				join("project","project.project_id = orders.project_id",'left')->
    			where($data)->order('create_time desc')->
    			field([
    					'shipment.shipment_uuid','shipment.finance_number','shipment.orders_number',
    					'shipment.shipment_time','shipment.need_time','shipment.supplier_uuid','shipment.supplier_line_uuid',
    					'shipment.supplier_shipment_number','shipment.shipment_type','shipment.pay_all_money','shipment.shipment_remark',
    					'shipment.create_time','shipment.create_user_id','shipment.update_time','shipment.update_user_id',
    					'shipment.status',	'shipment.shipment_finance_status',
						'project.project_name','orders.pickup_time','shipment.shipment_id',
    					"supplier.supplier_name","orders.dfk",

    					"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
    			])->select();
    		}
    	}
    
    
    
    	return $result;
    
    }
    /**
     * 修改发运
     */
    public function updateShipment($params){
    
    	$t = time();



    	
    	if(is_numeric($params['shipment_time'])){
    		$data['shipment_time'] = $params['shipment_time'];
    	}
    	if(is_numeric($params['need_time'])){
    		$data['need_time'] = $params['need_time'];
    	}
    	if(isset($params['supplier_uuid'])){
    		$data['supplier_uuid'] = $params['supplier_uuid'];
    	}
    	if(isset($params['supplier_line_uuid'])){
    		$data['supplier_line_uuid'] = $params['supplier_line_uuid'];
    	}
    	$data['shipment_type'] = $params['shipment_type'];
    	$data['orders_number'] = $params['orders_number'];
    	$data['supplier_shipment_number'] = $params['supplier_shipment_number'];
    	$data['pay_all_money'] = $params['pay_all_money'];
    	$data['shipment_remark'] = $params['shipment_remark'];
		$data['shipment_pay_type'] = $params['shipment_pay_type'];
    	$data['update_time'] = $t;

    	$data['update_user_id'] = $params['user_id'];



 
    	Db::startTrans();
    	try{

    		Db::name('shipment')->where("shipment_uuid='".$params['shipment_uuid']."'")->update($data);
			$statusParams['status'] = 0;
			//开始插入成本
			
    		Db::name('shipment_cost')->where("shipment_uuid='".$params['shipment_uuid']."'")->update($statusParams);
    		
    		$shipmentCost = $params['shipment_cost'];
    		
    		for($i=0;$i<count($shipmentCost);$i++){
    			$shipmentCostParams['shipment_uuid'] = $params['shipment_uuid'];
    			$shipmentCostParams['shipment_cost_uuid'] = Help::getUuid();
    			$shipmentCostParams['cost_id'] = $shipmentCost[$i]['cost_id'];
    			$shipmentCostParams['cost_money'] = $shipmentCost[$i]['cost_money'];
    			$shipmentCostParams['create_time'] = $t;
    			$shipmentCostParams['update_time'] = $t;
    			$shipmentCostParams['create_user_id'] = $params['user_id'];
    			$shipmentCostParams['update_user_id'] = $params['user_id'];
    			$shipmentCostParams['status'] = 1;
    			Db::name('shipment_cost')->insertGetId($shipmentCostParams);
    		}
    		//开始插入商品
    		Db::name('shipment_goods')->where("shipment_uuid='".$params['shipment_uuid']."'")->update($statusParams);
    		$shipmentGoods = $params['shipment_goods'];
    		for($i=0;$i<count($shipmentGoods);$i++){
    			$shipmentGoodsParams['shipment_uuid'] = $params['shipment_uuid'];
    			$shipmentGoodsParams['shipment_goods_uuid'] = Help::getUuid();
    			$shipmentGoodsParams['goods_id'] = $shipmentGoods[$i]['goods_id'];
    			$shipmentGoodsParams['shipment_count'] = $shipmentGoods[$i]['shipment_count'];
    			$shipmentGoodsParams['shipment_pack_unit'] = $shipmentGoods[$i]['shipment_pack_unit'];
    			$shipmentGoodsParams['shipment_pack_count'] = $shipmentGoods[$i]['shipment_pack_count'];    	
    			$shipmentGoodsParams['shipment_weight'] = $shipmentGoods[$i]['shipment_weight'];
    			$shipmentGoodsParams['shipment_volume'] = $shipmentGoods[$i]['shipment_volume'];
    			
    			$shipmentGoodsParams['shipment_charge_type'] = $shipmentGoods[$i]['shipment_charge_type'];
    			$shipmentGoodsParams['unit_price'] = $shipmentGoods[$i]['unit_price'];
    			$shipmentGoodsParams['shipment_money'] = $shipmentGoods[$i]['shipment_money'];    			
    			$shipmentGoodsParams['create_time'] = $t;
    			$shipmentGoodsParams['update_time'] = $t;
    			$shipmentGoodsParams['create_user_id'] = $params['user_id'];
    			$shipmentGoodsParams['update_user_id'] = $params['user_id'];
    			$shipmentGoodsParams['status'] = 1;
    			Db::name('shipment_goods')->insertGetId($shipmentGoodsParams);
    		}   		
    		//开始插入支付方式
    		Db::name('shipment_pay')->where("shipment_uuid='".$params['shipment_uuid']."'")->update($statusParams);
    		$shipmentPay = $params['shipment_pay'];
    		for($i=0;$i<count($shipmentPay);$i++){
    			$shipmentPayParams['shipment_uuid'] = $params['shipment_uuid'];
    			$shipmentPayParams['shipment_pay_uuid'] = Help::getUuid();
    			$shipmentPayParams['pay_type'] = $shipmentPay[$i]['pay_type'];
    			$shipmentPayParams['pay_money'] = $shipmentPay[$i]['pay_money'];
    			$shipmentPayParams['create_time'] = $t;
    			$shipmentPayParams['update_time'] = $t;
    			$shipmentPayParams['create_user_id'] = $params['user_id'];
    			$shipmentPayParams['update_user_id'] = $params['user_id'];
    			$shipmentPayParams['status'] = 1;
    			Db::name('shipment_pay')->insertGetId($shipmentPayParams);
    		}    		
    		

    		$ordersParams = [];
    		//假如shipment_status = 1说明结束安排
    		if($params['shipment_status'] ==1){

   
    			$ordersParams['shipment_status'] = 1;
    		}
    		if($params['is_short_barge']==1){
    
    			$ordersParams['is_short_barge'] = 1;
    		}else{//如果不需要短驳
    			$ordersParams['is_short_barge']=0;
    			Db::name('shipment')->where("shipment_type = 3 and shipment_uuid='".$params['shipment_uuid']."'")->update($statusParams);
    			//Db::name('orders')->where("orders_number='".$params['orders_number']."'")->update($ordersParams);
    		} 		
    		$ordersWhere['orders_number'] = $params['orders_number'];

			$updateSupplier['orders_number'] = $params['orders_number'];
			$this->updateOrdersSupplier($updateSupplier);
			
			//首先判断是否原来就有短驳，如果有就不加没有就加
			$ordersResult = Db::name('orders')->where($ordersWhere)->find();
			if($ordersResult['is_short_barge']==0 && $params['is_short_barge']==1){
				Db::name('orders')->where($ordersWhere)->update($ordersParams);
				
			}else if($ordersResult['is_short_barge']>0 && $params['is_short_barge']==0){
				Db::name('orders')->where($ordersWhere)->update($ordersParams);
			}
    		
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }

    /**
     * 添加短驳
     * 胡
     */
    public function addShortBarge($params){
    	$t = time();
    	if(is_numeric($params['shipment_time'])){
    		$data['shipment_time'] = $params['shipment_time'];
    	}

    	if(isset($params['supplier_uuid'])){
    		$data['supplier_uuid'] = $params['supplier_uuid'];
    	}
    	if(isset($params['supplier_vehicle_id'])){
    		$data['supplier_vehicle_id'] = $params['supplier_vehicle_id'];
    	}
    	 

    	$data['shipment_type'] = 3;
    	 
    	if(!empty($params['pay_all_money'])){
    		$data['pay_all_money'] = $params['pay_all_money'];
    	}
    	if(is_numeric($params['distribution_type'])){
    		$data['distribution_type'] = $params['distribution_type'];
    	}
    	
    	if(!empty($params['shipment_remark'])){
    		$data['shipment_remark'] = $params['shipment_remark'];
    	}
    	 
    	$data['create_time'] = $t;
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = 1;
    
    
    	$data['shipment_uuid']= Help::getUuid();
    
    	Db::startTrans();
    	try{
    		//$data['finance_number']
    
    		//开始计算合同号
    		//先计算当天的合同数量
    		$dayCountParams = "shipment_type =3 and FROM_UNIXTIME(create_time,'%Y%m') =".date('Ym');
    		$dayCount = Db::name('shipment')->where($dayCountParams)->count();
    		$data['finance_number'] = "ADB".date("ym").str_pad(($dayCount+1),4,0,STR_PAD_LEFT);
    		 
    		$pk_id = Db::name('shipment')->insertGetId($data);
    		//插入运单明细
    		$shipmentShortBarge = $params['shipment_short_barge'];
    		for($i=0;$i<count($shipmentShortBarge);$i++){
    			$shipmentShortBargeParams['shipment_uuid'] = $data['shipment_uuid'];
    			$shipmentShortBargeParams['shipment_short_barge_uuid'] = Help::getUuid();
    			$shipmentShortBargeParams['orders_number'] = $shipmentShortBarge[$i]['orders_number'];
    		//orders表的短驳状态进行变更

    			$ordersParams['is_short_barge'] = 2;
    			
    			$ordersWhere['orders_number'] = $shipmentShortBarge[$i]['orders_number'];
    			
    			Db::name('orders')->where($ordersWhere)->update($ordersParams);
    	
    			$shipmentShortBargeParams['short_barge_money'] = $shipmentShortBarge[$i]['short_barge_money'];
    			$shipmentShortBargeParams['create_time'] = $t;
    			$shipmentShortBargeParams['update_time'] = $t;
    			$shipmentShortBargeParams['create_user_id'] = $params['user_id'];
    			$shipmentShortBargeParams['update_user_id'] = $params['user_id'];
    			$shipmentShortBargeParams['status'] = 1;
    			Db::name('shipment_short_barge')->insertGetId($shipmentShortBargeParams);
    		}
    		
    		$shipmentCostParams['shipment_uuid'] =$data['shipment_uuid'];
			$shipmentCostParams['shipment_cost_uuid'] = Help::getUuid();
			$shipmentCostParams['cost_id'] =9;
			$shipmentCostParams['cost_money'] =$params['pay_all_money'];
			$shipmentCostParams['create_time'] = $t;
			$shipmentCostParams['update_time'] = $t;
			$shipmentCostParams['create_user_id'] = $params['user_id'];
			$shipmentCostParams['update_user_id'] = $params['user_id'];
			$shipmentCostParams['status'] = 1;
			Db::name('shipment_cost')->insertGetId($shipmentCostParams);
			
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    	 
    	return $result;
    }
	
	public function delShipment($params){
  
    	Db::startTrans();
    	try{

			$ordersWhere['orders_number']=$params['orders_number'];
			$ordersUpdate['shipment_status'] = 0;
			$ordersUpdate['is_short_barge'] = 0;
			
			$shipmentWhere['finance_number']=$params['finance_number'];
			$shipmentUpdate['status'] = 0;

			Db::name('orders')->where($ordersWhere)->update($ordersUpdate);
			Db::name('shipment')->where($shipmentWhere)->update($shipmentUpdate);
			//这里开始判断是否有发运，只要有一条发运就不改变运单状态，如果一条发运都没有那么就要变更为库存状态
			$shipmentParams['orders_number']=$params['orders_number'];
			$shipmentParams['status']=1;
			$s_count = Db::name('shipment')->where($shipmentParams)->count();
		
			if($s_count == 0){
						
				$ordersWhere['orders_number']=$params['orders_number'];
				$ordersUpdate1['order_status'] = 1;
				
				Db::name('orders')->where($ordersWhere)->update($ordersUpdate1);
			}		
		
			$updateSupplier['orders_number'] = $params['orders_number'];
			$this->updateOrdersSupplier($updateSupplier);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;		
		
		
	}
	
	public function delShortBarge($params){
	
    	Db::startTrans();
    	try{
			
			$shipmentWhere['shipment_uuid']=$params['shipment_uuid'];
			$shipmentUpdate['status'] = 0;
			Db::name('shipment')->where($shipmentWhere)->update($shipmentUpdate);
			
			//获取所有的运单号
			
			
			
			$shipmentShortBargeWhere['shipment_uuid']=$params['shipment_uuid'];
			
			
			$result=Db::name('shipment_short_barge')->where($shipmentShortBargeWhere)->select();
			for($i=0;$i<count($result);$i++){
				$orderWhere['orders_number'] = $result[$i]['orders_number'];
				$orderUpdate['is_short_barge'] = 1;
				Db::name('orders')->where($orderWhere)->update($orderUpdate);	
				
			}
			
			$shipmentShortBargeUpdate['status'] = 0;
			Db::name('shipment_short_barge')->where($shipmentShortBargeWhere)->update($shipmentShortBargeUpdate);		

	
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
		
    	return $result;		
		
		
	}	
    /**
     * 修改短驳
     * 胡
     */
    public function updateShortBarge($params){
    	$t = time();
    	if(is_numeric($params['shipment_time'])){
    		$data['shipment_time'] = $params['shipment_time'];
    	}
    
    	if(isset($params['supplier_uuid'])){
    		$data['supplier_uuid'] = $params['supplier_uuid'];
    	}
    	if(isset($params['supplier_vehicle_id'])){
    		$data['supplier_vehicle_id'] = $params['supplier_vehicle_id'];
    	}
    
    
    
    
    	if(isset($params['pay_all_money'])){
    		$data['pay_all_money'] = $params['pay_all_money'];
    	}
    	if(is_numeric($params['distribution_type'])){
    		$data['distribution_type'] = $params['distribution_type'];
    	}
    	 
    	if(isset($params['shipment_remark'])){
    		$data['shipment_remark'] = $params['shipment_remark'];
    	}
    
    
    	$data['update_time'] = $t;

    	$data['update_user_id'] = $params['user_id'];

    
    
    //	$data['shipment_uuid']= Help::getUuid();
    
    	Db::startTrans();
    	try{

    		$where['shipment_uuid'] = $params['shipment_uuid'];
    		$pk_id = Db::name('shipment')->where($where)->update($data);
    		
    		//首先先让他下面运单的短驳 信息变更为需要短驳
    		$shipment_short_barge_params['status'] = 1;
    		$shipment_short_barge_params['shipment_uuid'] = $params['shipment_uuid'];
    		$shipment_short_barge_result = Db::name('shipment_short_barge')->where($shipment_short_barge_params)->select();
    		for($i=0;$i<count($shipment_short_barge_result);$i++){
    			$order_params['is_short_barge']=1;
    			$order_where_params['orders_number'] = $shipment_short_barge_result[$i]['orders_number'];
    			Db::name('orders')->where($order_where_params)->update($order_params);
    		}
    		
    		//插入运单明细
    		//先把状态变更为0
    		$statusParams['status'] = 0;
    		$statusWhereParams['shipment_uuid'] = $params['shipment_uuid'];
    		Db::name('shipment_short_barge')->where($statusWhereParams)->update($statusParams);
    		$shipmentShortBarge = $params['shipment_short_barge'];
    		for($i=0;$i<count($shipmentShortBarge);$i++){
    			$shipmentShortBargeParams['shipment_uuid'] = $params['shipment_uuid'];
    			$shipmentShortBargeParams['shipment_short_barge_uuid'] = Help::getUuid();
    			$shipmentShortBargeParams['orders_number'] = $shipmentShortBarge[$i]['orders_number'];
    			//orders表的短驳状态进行变更
    
    			$ordersParams['is_short_barge'] = 2;
    			 
    			$ordersWhere['orders_number'] = $shipmentShortBarge[$i]['orders_number'];
    			 
    			Db::name('orders')->where($ordersWhere)->update($ordersParams);
    			 
    			$shipmentShortBargeParams['short_barge_money'] = $shipmentShortBarge[$i]['short_barge_money'];
    			$shipmentShortBargeParams['create_time'] = $t;
    			$shipmentShortBargeParams['update_time'] = $t;
    			$shipmentShortBargeParams['create_user_id'] = $params['user_id'];
    			$shipmentShortBargeParams['update_user_id'] = $params['user_id'];
    			$shipmentShortBargeParams['status'] = 1;
    			Db::name('shipment_short_barge')->insertGetId($shipmentShortBargeParams);
    		}
    		Db::name('shipment_cost')->where($statusWhereParams)->update($statusParams);
    		$shipmentCostParams['shipment_uuid'] =$params['shipment_uuid'];
    		$shipmentCostParams['shipment_cost_uuid'] = Help::getUuid();
    		$shipmentCostParams['cost_id'] =9;
    		$shipmentCostParams['cost_money'] =$params['pay_all_money'];
    		$shipmentCostParams['create_time'] = $t;
    		$shipmentCostParams['update_time'] = $t;
    		$shipmentCostParams['create_user_id'] = $params['user_id'];
    		$shipmentCostParams['update_user_id'] = $params['user_id'];
    		$shipmentCostParams['status'] = 1;
    		Db::name('shipment_cost')->insertGetId($shipmentCostParams);
    			
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }    
    //修改财务状态
    public function updateShipmentFinanceStatus($params){
    	$t = time();
		$shipment_uuid = $params['shipment_uuid_array'];
    	$data['update_time'] = $t;
    
    	$data['update_user_id'] = $params['user_id'];
    	$data['shipment_finance_status'] = $params['shipment_finance_status'];
    
    
    	//	$data['shipment_uuid']= Help::getUuid();
    
    	Db::startTrans();
    	try{
    		for($i=0;$i<count($shipment_uuid);$i++){
    			$where['shipment_uuid'] = $shipment_uuid[$i];
    			 Db::name('shipment')->where($where)->update($data);
    		}
    		
    	

    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }   

	/*获取发运数据*/
	public function getShipmentEasy($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "1=1 ";

    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment.shipment_uuid= '".$params['shipment_uuid']."'";
    	}


   
        if($is_count==true){
            $result = $this->table("shipment")->alias("shipment")->
            where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("shipment")->alias('shipment')->

                where($data)->limit($page, $page_size)->order('shipment.create_time desc')->
                field([
                		'shipment.*',
                		
		                ])->select();
            }else{
                $result = $this->table("shipment")->alias('shipment')->

                where($data)->order('shipment.create_time desc')->
                field([
                		'shipment.*'
                    	])->select();
            }
        }
    	 
 
    
    	return $result;
    
    }	
	//驾驶舱
	public function getIndexShipment($params){
		$data = '1=1 and shipment.status =1';
		if(is_numeric($params['month'])){
			$data.= " and FROM_UNIXTIME(shipment_time,'%Y%m') = FROM_UNIXTIME(".$params['month'].",'%Y%m')";
	
		}				
		if(is_numeric($params['year'])){
			$data.= " and FROM_UNIXTIME(shipment_time,'%Y') = FROM_UNIXTIME(".$params['year'].",'%Y')";
	
		}					
		
		$result =  $this->table('shipment')->where($data)->field([
				"sum(shipment.pay_all_money) as sum_shipment_money",
		
                ])->select();
	
		return $result;
		
	}	
}