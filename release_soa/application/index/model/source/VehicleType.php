<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;

use think\config;
use think\Db;
class VehicleType extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'vehicle_type';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {

    	parent::initialize();
    
    }

    /**
     * 添加车辆
     * 胡
     */
    public function addVehicleType($params){
    	$t = time();

    	
    	$data['vehicle_type_name'] = $params['vehicle_type_name'];

    	$data['vehicle_type_uuid'] = Help::getUuid();
    	$data['create_time'] = $t;  	
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];

    	
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('vehicle_type')->insertGetId($data);

    		$result =1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取车辆
     * 胡
     */
    public function getVehicleType($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";


    	if(is_numeric($params['status'])){
    		$data.= " and status = ".$params['status'];
    	}
    	if(!empty($params['vehicle_type_id'])){
    		$data.= " and vehicle_type_id = '".$params['vehicle_type_id']."'";
    	}
    	if(!empty($params['vehicle_type_uuid'])){
    		$data.= " and vehicle_type_uuid = '".$params['vehicle_type_uuid']."'";
    	}

    	if(!empty($params['vehicle_type_name'])){
    		$data.= ' and vehicle_type_name like "%'.$params['vehicle_type_name'].'%"';
    		
    	}

        if($is_count==true){
            $result = $this->table("vehicle_type")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("vehicle_type")->

                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['vehicle_type_id', "vehicle_type_uuid", 'vehicle_type_name',

                    "(select nickname  from user where user.user_id = vehicle_type.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = vehicle_type.update_user_id)"=> 'update_user_name',
                    'vehicle_type.update_time', 'vehicle_type.create_time', "vehicle_type.status",
                ])->select();
            }else{
                $result = $this->table("vehicle_type")->alias('vehicle_type')->

                where($data)->order('create_time desc')->
                field(['vehicle_type_id', "vehicle_type_uuid", 'vehicle_type.vehicle_type_name',

                    "(select nickname  from user where user.user_id = vehicle_type.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = vehicle_type.update_user_id)"=> 'update_user_name',
                    'vehicle_type.update_time', 'vehicle_type.create_time', "vehicle_type.status",
                ])->select();
            }
        }

        return $result;
    
    }

    
    /**
     * 修改车辆
     */
    public function updateVehicleTypeByVehicleTypeId($params){
    
    	$t = time();
    	
    	if(!empty($params['vehicle_type_name'])){
    		$data['vehicle_type_name'] = $params['vehicle_type_name'];
    	
    	}


    	if(is_numeric($params['status'])){
    		$data['status'] = $params['status'];
    		 
    	}



    	$data['update_user_id'] = $params['user_id'];   
    	$data['update_time'] = $t;

    
    
    	$source_price=[];
    	Db::startTrans();
    	try{
    		Db::name('vehicle_type')->where("vehicle_type_id = ".$params['vehicle_type_id'])->update($data);

    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }

    /**
     * getOneVehicle
     *
     * 获取一条车辆信息
     * @author shj
     *
     * @param $vehicle_id
     *
     * @return void
     * Date: 2019/2/27
     * Time: 17:00
     */
    public function getOneVehicle($vehicle_id){
        $result = $this->table("vehicle")->where(['vehicle_id' => $vehicle_id])->find();
        return $result;
    }
}