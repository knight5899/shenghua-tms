<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class SupplierLine extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'supplier_line';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {
    	$this->_languageList = config('systom_setting')['language_list'];
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    /**
     * 添加线路
     * 胡
     */
    public function addSupplierLine($params){
    	$t = time();


    	$data['supplier_uuid'] = $params['supplier_uuid'];
    	if(isset($params['start_province_id'])){
    		$data['start_province_id'] = $params['start_province_id'];
    	}
    	if(isset($params['start_city_id'])){
    		$data['start_city_id'] = $params['start_city_id'];
    	}
    	if(isset($params['start_location_id'])){
    		$data['start_location_id'] = $params['start_location_id'];
    	}
    	if(isset($params['end_province_id'])){
    		$data['end_province_id'] = $params['end_province_id'];
    	}    	
    	if(isset($params['end_city_id'])){
    		$data['end_city_id'] = $params['end_city_id'];
    	}
    	if(isset($params['end_location_id'])){
    		$data['end_location_id'] = $params['end_location_id'];
    	}    	
    	if(isset($params['cost_hour'])){
    		$data['cost_hour'] = $params['cost_hour'];
    	}
    	if(isset($params['line_type'])){
    		$data['line_type'] = $params['line_type'];
    	}
    	if(isset($params['start_site_name'])){
    		$data['start_site_name'] = $params['start_site_name'];
    	}
    	if(isset($params['start_site_phone'])){
    		$data['start_site_phone'] = $params['start_site_phone'];
    	}
    	if(isset($params['start_site_address'])){
    		$data['start_site_address'] = $params['start_site_address'];
    	}
    	 
    	if(isset($params['end_site_name'])){
    		$data['end_site_name'] = $params['end_site_name'];
    	}
    	if(isset($params['end_site_phone'])){
    		$data['end_site_phone'] = $params['end_site_phone'];
    	}
    	if(isset($params['end_site_address'])){
    		$data['end_site_address'] = $params['end_site_address'];
    	}


    	$data['create_time'] = $t;
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];


    	$data['supplier_line_uuid']= Help::getUuid();
 
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('supplier_line')->insertGetId($data);

    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    	
    	return $result;
    }
    
    /**
     * 获取线路
     * 胡
     */
    public function getSupplierLine($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    

    	$data = "1=1 ";

    	if(isset($params['supplier_line_uuid'])){
    		$data.= " and supplier_line_uuid='".$params['supplier_line_uuid']."'";
    	}
    	if(!empty($params['supplier_uuid'])){
    		$data.= " and supplier_line.supplier_uuid='".$params['supplier_uuid']."'";
    	}

    	if(isset($params['start_city_id'])){
    		$data.= " and start_city_id= ".$params['start_city_id'];
    	}
    	if(isset($params['end_city_id'])){
    		$data.= " and end_city_id= ".$params['end_city_id'];
    	}

    	if(is_numeric($params['status'])){
    		$data.= " and status= ".$params['status'];
    	}   
		

        if($is_count==true){
            $result = $this->table("supplier_line")->alias("supplier_line")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("supplier_line")->alias('supplier_line')->
			
            
                where($data)->limit($page, $page_size)->order('supplier_line.create_time desc')->
                field(['supplier_line_id',"supplier_line_uuid", "supplier_uuid","start_province_id","start_city_id","start_location_id",
                		"end_province_id","end_city_id","end_location_id",
                     'cost_hour', 'line_type', 'start_site_name','start_site_phone',
                    'end_site_name', 'end_site_phone', 'end_site_address',  'start_site_address', 
                		"(select supplier_id from supplier where supplier.supplier_uuid = supplier_line.supplier_uuid)"=>'supplier_id',
                		"(select supplier_name  from supplier where supplier.supplier_uuid= supplier_line.supplier_uuid)"=> 'supplier_name',
                		"(select city_name  from city where supplier_line.start_province_id = city.city_id)"=> 'start_province_name',
                		"(select city_name  from city where supplier_line.start_city_id = city.city_id)"=> 'start_city_name',
                		"(select city_name  from city where supplier_line.start_location_id = city.city_id)"=> 'start_location_name',
                		"(select city_name  from city where supplier_line.end_province_id = city.city_id)"=> 'end_province_name',
                		"(select city_name  from city where supplier_line.end_city_id = city.city_id)"=> 'end_city_name',
                		"(select city_name  from city where supplier_line.end_location_id = city.city_id)"=> 'end_location_name',
                		"(select nickname  from user where user.user_id = supplier_line.create_user_id)"=> 'create_user_name',
                   	 "(select nickname  from user where user.user_id = supplier_line.update_user_id)"=> 'update_user_name',
                		
                    'update_time', 'create_time', "status"])->select();
					error_log(print_r($this->getlastsql(),1));
            }else{
                $result = $this->table("supplier_line")->alias('supplier_line')->
                where($data)->order('create_time desc')->
                field(['supplier_line_id',"supplier_line_uuid", "supplier_uuid","start_province_id","start_city_id","start_location_id",
                		"end_province_id","end_city_id","end_location_id",
                     'cost_hour', 'line_type', 'start_site_name','start_site_phone',
                    'end_site_name', 'end_site_phone', 'end_site_address',  'start_site_address', 
                		"(select supplier_id from supplier where supplier.supplier_uuid = supplier_line.supplier_uuid)"=>'supplier_id',
                		"(select supplier_name  from supplier where supplier.supplier_uuid= supplier_line.supplier_uuid)"=> 'supplier_name',
                		"(select city_name  from city where supplier_line.start_province_id = city.city_id)"=> 'start_province_name',
                		"(select city_name  from city where supplier_line.start_city_id = city.city_id)"=> 'start_city_name',
                		"(select city_name  from city where supplier_line.start_location_id = city.city_id)"=> 'start_location_name',
                		"(select city_name  from city where supplier_line.end_province_id = city.city_id)"=> 'end_province_name',
                		"(select city_name  from city where supplier_line.end_city_id = city.city_id)"=> 'end_city_name',
                		"(select city_name  from city where supplier_line.end_location_id = city.city_id)"=> 'end_location_name',
                		"(select nickname  from user where user.user_id = supplier_line.create_user_id)"=> 'create_user_name',
                   	 "(select nickname  from user where user.user_id = supplier_line.update_user_id)"=> 'update_user_name',

                    'update_time', 'create_time', "status"])->select();
            }
        }
    	 
	
    
    	return $result;
    
    }


    /**
     * 修改线路 根据supplier_id
     */
    public function updateSupplierLine($params){
    
    	$t = time();

    
    	$data['supplier_uuid'] = $params['supplier_uuid'];

        	if(isset($params['start_province_id'])){
    		$data['start_province_id'] = $params['start_province_id'];
    	}
    	if(isset($params['start_city_id'])){
    		$data['start_city_id'] = $params['start_city_id'];
    	}
    	if(isset($params['start_location_id'])){
    		$data['start_location_id'] = $params['start_location_id'];
    	}
    	if(isset($params['end_province_id'])){
    		$data['end_province_id'] = $params['end_province_id'];
    	}    	
    	if(isset($params['end_city_id'])){
    		$data['end_city_id'] = $params['end_city_id'];
    	}
    	if(isset($params['end_location_id'])){
    		$data['end_location_id'] = $params['end_location_id'];
    	}    	
    	if(isset($params['cost_hour'])){
    		$data['cost_hour'] = $params['cost_hour'];
    	}
    	if(isset($params['line_type'])){
    		$data['line_type'] = $params['line_type'];
    	}
    	if(isset($params['start_site_name'])){
    		$data['start_site_name'] = $params['start_site_name'];
    	}
    	if(isset($params['start_site_phone'])){
    		$data['start_site_phone'] = $params['start_site_phone'];
    	}
    	if(isset($params['end_site_name'])){
    		$data['end_site_name'] = $params['end_site_name'];
    	}
    	if(isset($params['end_site_phone'])){
    		$data['end_site_phone'] = $params['end_site_phone'];
    	}
		if(isset($params['start_site_address'])){
    		$data['start_site_address'] = $params['start_site_address'];
    	}
    	if(isset($params['end_site_address'])){
    		$data['end_site_address'] = $params['end_site_address'];
    	}

 
    	Db::startTrans();
    	try{
    		Db::name('supplier_line')->where("supplier_line_uuid = '".$params['supplier_line_uuid']."'")->update($data);

    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
		
			
    	return $result;
    }


}