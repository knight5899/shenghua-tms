<?php

namespace app\index\model\source;

use app\index\model\Base;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;

class AcceptGoods extends Base
{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'accept_goods';
    private $_public_service;

    public function initialize()
    {
        $this->_public_service = new PublicService();
        parent::initialize();

    }

    public function parseAcceptGoods($params)
    {
        $where['project_id'] = $params['project_id'];
        $where['province_id'] = $params['accept_province_id'];
        $where['city_id'] = $params['accept_city_id'];
        $where['area_id'] = $params['accept_area_id'];
        $where['accept_location_id'] = $params['accept_location_id'];
        $where['accept_goods_company'] = $params['accept_goods_company'];
        $where['accept_goods_address'] = $params['accept_address'];
        $where['accept_goods_name'] = $params['accept_name'];
        $where['accept_goods_cellphone'] = $params['accept_cellphone'];
        $res = $this->where($where)->find();
        if ($res){

         //   $res->remark=$params['remark'];$res->save();
            return ['accept_goods_id' => $res->accept_goods_id];
        }

        else{
            $where['status']=1;
            $where['create_user_id']=$params['user_id'];
            $where['update_user_id']=$params['user_id'];
            $where['create_time']=time();
            $where['update_time']=time();
            $where['remark']=$params['remark'];


            return ['accept_goods_id' => $this->pubInsert($where)];
        }

    }

    public function getAcceptgoodsname($params){
        if(!empty($params['keyword']))
            return $this->where('accept_goods_name','like',"%".$params['keyword']."%")->select();
        else
            return $this->limit(0,10)->select();

    }

    /**
     * 添加客户
     * 胡
     */
    public function addAcceptGoods($params)
    {
        $t = time();

        $data['project_id'] = $params['project_id'];
        $data['province_id'] = $params['province_id'];
        $data['area_id'] = $params['area_id'];
        $data['city_id'] = $params['city_id'];
        $data['accept_location_id'] = $params['accept_location_id'];
        if (!empty($params['accept_goods_company'])) {
        	$data['accept_goods_company'] = $params['accept_goods_company'];
        }
        if (!empty($params['accept_goods_address'])) {
            $data['accept_goods_address'] = $params['accept_goods_address'];
        }


        if (!empty($params['accept_goods_name'])) {
            $data['accept_goods_name'] = $params['accept_goods_name'][$params['lianxiren-select']];

        }

        if (!empty($params['accept_goods_cellphone'])) {
            $data['accept_goods_cellphone'] = $params['accept_goods_cellphone'][$params['lianxiren-select']];

        }
        if (!empty($params['remark'])) {
            $data['remark'] = $params['remark'];

        }


        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];


        Db::startTrans();
        try {
            $pk_id = Db::name('accept_goods')->insertGetId($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }
        $contactArray = [];
        foreach ($params['accept_goods_name'] as $k => $v) {
            $contactArray[] = ['accept_goods_id' => $pk_id,
                'contact_name' => $v,
                'contact_cellphone' => $params['accept_goods_cellphone'][$k],
                'create_time' => time(),
                'status'=>1
            ];
        }
        $goodsContact = new AcceptGoodsContact();
		
		
		  Db::startTrans();
        try {
	        $goodsContact->saveAll($contactArray);
			 Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }
        return $result;
    }

    /**
     * 获取客户
     * 胡
     */
    public function getAcceptGoods($params, $is_count = false, $is_page = false, $page = null, $page_size = 20)
    {
        $data = "1=1 ";
        if (is_numeric($params['accept_goods_id'])) {
            $data .= " and accept_goods.accept_goods_id = " . $params['accept_goods_id'];
        }
        if (is_numeric($params['project_id'])) {
            $data .= " and accept_goods.project_id = " . $params['project_id'];
        }
        if (is_numeric($params['province_id'])) {
            $data .= " and accept_goods.province_id = " . $params['province_id'];
        }
        if (is_numeric($params['city_id'])) {
            $data .= " and accept_goods.city_id = " . $params['city_id'];
        }
        if (is_numeric($params['area_id'])) {
            $data .= " and accept_goods.area_id = " . $params['area_id'];
        }
        if (is_numeric($params['status'])) {
            $data .= " and accept_goods.status = " . $params['status'];
        }

        if (!empty($params['accept_goods_name'])) {
            $data .= ' and accept_goods.accept_goods_name like "%' . $params['accept_goods_name'] . '%"';
        }

        if (!empty($params['accept_goods_company'])) {
            $data .= ' and accept_goods.accept_goods_company like "%' . $params['accept_goods_company'] . '%"';
        }

        if (!empty($params['accept_goods_cellphone'])) {
            $data .= ' and accept_goods.accept_goods_cellphone like "%' . $params['accept_goods_cellphone'] . '%"';
        }

        if ($is_count == true) {
            $result = $this->table("accept_goods")->where($data)->
            join("project", 'project.project_id = accept_goods.project_id')->count();
        } else {
            if ($is_page == true) {
                $result = $this->table("accept_goods")->
                join("project", 'project.project_id = accept_goods.project_id')->
                where($data)->limit($page, $page_size)->order('accept_goods.accept_goods_id desc')->
                field(['accept_goods.*', 'project.project_name','accept_goods_company',
                	"(select city_name  from city where city.city_id= accept_goods.accept_location_id)"=> 'accept_location_name',
                    "(select city_name  from city where city.city_id= accept_goods.province_id)" => 'province_name',
                    "(select city_name  from city where city.city_id= accept_goods.city_id)" => 'city_name',
                    "(select city_name  from city where city.city_id= accept_goods.area_id)" => 'area_name',
                    "(select nickname  from user where user.user_id = accept_goods.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = accept_goods.update_user_id)" => 'update_user_name',
                    'accept_goods.update_time', 'accept_goods.create_time', "accept_goods.status",
                ])->select();


            } else {
                $result = $this->table("accept_goods")->alias('accept_goods')->
                join("project", 'project.project_id = accept_goods.project_id')->

                where($data)->order('accept_goods.accept_goods_id desc')->
                field(['accept_goods.*', 'project.project_name','accept_goods_company',
                    "(select city_name  from city where city.city_id= accept_goods.accept_location_id)"=> 'accept_location_name',
                    "(select city_name  from city where city.city_id= accept_goods.province_id)" => 'province_name',
                    "(select city_name  from city where city.city_id= accept_goods.city_id)" => 'city_name',
                    "(select city_name  from city where city.city_id= accept_goods.area_id)" => 'area_name',
                    "(select nickname  from user where user.user_id = accept_goods.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = accept_goods.update_user_id)" => 'update_user_name',
                    'accept_goods.update_time', 'accept_goods.create_time', "accept_goods.status",
                ])->select();
            }
        }
        if ($result)
            $result[0]['contact'] = AcceptGoodsContact::where('accept_goods_id', $result[0]['accept_goods_id'])
                ->where('status',1)
                ->select();
        return $result;

    }

    public function getAcceptGoodsExport($params){

        $data = "1=1 ";

        if (is_numeric($params['project_id'])) {
            $data .= " and accept_goods.project_id = " . $params['project_id'];
        }

        $result = $this->table("accept_goods")->alias('accept_goods')->
        join("project", 'project.project_id = accept_goods.project_id')->

        where($data)->order('accept_goods.accept_goods_id desc')->
        field(['accept_goods.accept_goods_id','project.project_name','accept_goods_company',
            "(select city_name  from city where city.city_id= accept_goods.accept_location_id)"=> 'accept_location_name',
            "(select city_name  from city where city.city_id= accept_goods.province_id)" => 'province_name',
            "(select city_name  from city where city.city_id= accept_goods.city_id)" => 'city_name',
            "(select city_name  from city where city.city_id= accept_goods.area_id)" => 'area_name',
            'accept_goods.accept_goods_address','accept_goods.accept_goods_name','accept_goods.accept_goods_cellphone','accept_goods.remark',
            "(select nickname  from user where user.user_id = accept_goods.create_user_id)" => 'create_user_name',
            'FROM_UNIXTIME(accept_goods.create_time)'=>'create_time', "accept_goods.status"
        ])->select();

        return $result;
    }

    /**
     * 修改收货方
     */
    public function updateAcceptGoodsByAcceptGoodsId($params)
    {

        $t = time();

        $contactArray = [];
        foreach ($params['accept_goods_name'] as $k => $v) {
                $contactArray[] = [
                    'accept_goods_id'=>$params['accept_goods_id'],

                    'contact_name' => $v,
                    'contact_cellphone' => $params['accept_goods_cellphone'][$k],
                    'create_time' => time(),
                    'status'=>1
                ];
        }
        $contact_model=new AcceptGoodsContact();
        $contact_model->where('accept_goods_id', $params['accept_goods_id'])->update(['status'=>0]);

		Db::startTrans();
        try {

        $contact_model->saveAll($contactArray);

		 Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
			return $result;
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }
		
		
		
        $data['project_id'] = $params['project_id'];
        $data['province_id'] = $params['province_id'];
        $data['area_id'] = $params['area_id'];
        $data['city_id'] = $params['city_id'];
        $data['accept_location_id'] = $params['accept_location_id'];
        if (!empty($params['accept_goods_address'])) {
            $data['accept_goods_address'] = $params['accept_goods_address'];
        }
        if (!empty($params['accept_goods_company'])) {
        	$data['accept_goods_company'] = $params['accept_goods_company'];
        }
        
        if (!empty($params['accept_goods_name'])) {
            $data['accept_goods_name'] = $params['accept_goods_name'][$params['lianxiren-select']];

        }
        
        if (!empty($params['accept_goods_cellphone'])) {
            $data['accept_goods_cellphone'] = $params['accept_goods_cellphone'][$params['lianxiren-select']];

        }
//        if (!empty($params['remark'])) {
//            $data['remark'] = $params['remark'];
//
//        }
        $data['remark'] = $params['remark'];

        $data['status'] = $params['status'];
        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;


        $source_price = [];
        Db::startTrans();
        try {
            Db::name('accept_goods')->where("accept_goods_id = " . $params['accept_goods_id'])->update($data);
            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;
    }


}