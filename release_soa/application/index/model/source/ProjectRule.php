<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class ProjectRule extends Model{

    protected $table = 'project_rule';

    /**
     * 获取承运商计价配置
     * 韩
     */
    public function getProjectRule($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

    	$data = "1=1 ";

        if(isset($params['project_id'])){ //项目ID
            $data.= " and project_rule.project_id = ".$params['project_id'];
        }

        if(is_numeric($params['type'])){ //类型
            $data.= " and project_rule.type = ".$params['type'];
        }

        if(is_numeric($params['status'])){ //状态
            $data.= " and project_rule.status = ".$params['status'];
        }

        if($is_count==true){
            $result = $this->table("project_rule")->alias("project_rule")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("project_rule")->alias('project_rule')->
                where($data)->limit($page, $page_size)->order('project_rule.sort asc')->
                field(['project_rule.*'])->select();
            }else{
                $result = $this->table("project_rule")->alias('project_rule')->
                where($data)->order('project_rule.sort asc')->
                field(['project_rule.*'])->select();
            }
        }
    
    	return $result;
    
    }

}