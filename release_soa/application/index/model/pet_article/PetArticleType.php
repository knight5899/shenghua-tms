<?php

namespace app\index\model\pet_article;
use think\Model;
use app\common\help\Help;
use think\config;
use think\Db;
class PetArticleType extends Model{

    public function initialize()
    {
        parent::initialize();
    }

    public function addPetArticleType($params){

        $t = time();

       $data['pet_article_type_name'] = $params['pet_article_type_name'];

        if($params['image']){
            $data['image'] = $params['image'];
        }
        if($params['content']){
            $data['content'] = $params['content'];
        }
        if($params['title']){
            $data['title'] = $params['title'];
        }
        if($params['description']){
            $data['description'] = $params['description'];
        }
        if($params['keywords']){
            $data['keywords'] = $params['keywords'];
        }
        $data['pid'] = $params['pid'];
        $a = $this->getPetArticleType(['pet_article_type_id' => $params['pid']]);
		$a = $a[0];
        $data['level'] = $a['level'] ? $a['level']+1 : 1;

        $data['status'] = $params['status'];
        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['create_time'] = $t;

        Db::startTrans();
        try{
            Db::name('pet_article_type')->insertGetId($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }

        return $result;
    }


    public function getPetArticleType($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";
        if(is_numeric($params['status'])){
            $data.= " and pet_article_type.status= ".$params['status'];
        }
        if(is_numeric($params['pet_article_type_id'])){
        	$data.= " and pet_article_type.pet_article_type_id= ".$params['pet_article_type_id'];
        }
        if(!empty($params['pet_article_type_name'])){
            $data.= " and pet_article_type_name like'%".$params['pet_article_type_name']."%'";
        }

        if($is_count==true){
            $result = $this->table("pet_article_type")->where($data)->count();
        }else {
            if ($is_page == true) {

                $result = $this->table("pet_article_type")
                    ->where($data)
                    ->limit($page, $page_size)
                    ->field(['pet_article_type.*',
                    "(select nickname  from user where user.user_id = pet_article_type.create_user_id)"=>'create_user_name',
            		"(select nickname  from user where user.user_id = pet_article_type.update_user_id)"=>'update_user_name',
            		])
                    ->select();

            } else {

                $result = $this->table("pet_article_type")
                    ->where($data)
                    ->field(['pet_article_type.*',
                    "(select nickname  from user where user.user_id = pet_article_type.create_user_id)"=>'create_user_name',
            		"(select nickname  from user where user.user_id = pet_article_type.update_user_id)"=>'update_user_name',
            		
                    		
                    		
                    ])
                    ->select();

            }
        }

        return $result;
    }


    public function updatePetArticleTypeByPetArticleTypeId($params){
        $t = time();

        if($params['pet_article_type_name']){
            $data['pet_article_type_name'] = $params['pet_article_type_name'];
        }

        if($params['pid']){
            $data['pid'] = $params['pid'];
            
            
   
            $a = $this->getPetArticleType(['pet_article_type_id' => $data['pid']]);
            $a = $a[0];
            $data['level'] = $a['level'] ? $a['level']+1 : 1;
            
            
        }
        
        //通过PID查找LEVEL
        
        
        
//        if(isset($params['image'])){
//            $data['image'] = $params['image'];
//        }
        if(isset($params['content'])) {
            $data['content'] = $params['content'];
        }
        $data['image'] = isset($params['image']) ? $params['image']: '';
        $data['title'] = isset($params['title']) ? $params['title']: '';
        $data['description'] = isset($params['description']) ? $params['description']: '';
        $data['keywords'] = isset($params['keywords']) ? $params['keywords']: '';

   	
        $data['status'] = $params['status'];
        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        
     
        Db::startTrans();
        try{
            Db::name('pet_article_type')->where("pet_article_type_id = ".$params['pet_article_type_id'])->update($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;


    }

}