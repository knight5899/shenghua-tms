<?php 
namespace app\index\model\b2b_operate; 
use think\Exception;
use think\Model;
use think\Db;
class B2bOperate extends Model{
	public function initialize()
	{
	    	$this->_languageList = config('systom_setting')['language_list'];
	    	parent::initialize();
	    
	}

	public function approvePlan($params){

		$where = '1=1';

		if($params['is_inbound']){ //1.入境 2.出境
			$is_inbound = $params['is_inbound']==1?1:0;
			$where .= " and b2b_tour.is_inbound={$is_inbound}";
		}

		if($params['user_company_id']){ 
			$where .= " and company_order.company_id={$params['user_company_id']}";
		}

		if($params['page']){
			$slimit = ($params['page']-1)*$params['page_size'];
		}

		if($params['s_tour']){
			$where .= " and b2b_booking.btb_tour_id={$params['s_tour']}";
		}
		if($params['s_agentCode']){
			$where .= " and distributor.distributor_code like '%{$params['s_agentCode']}%'";
		}
		if($params['s_createdFrom']){
			$s_createdFrom = strtotime($params['s_createdFrom']);
			$where .= " and company_order.create_time>=$s_createdFrom";	
		}
		if($params['s_createdTo']){
			$s_createdTo = strtotime($params['s_createdTo']);
			$where .= " and company_order.create_time<=$s_createdTo"; 
		}
		if($params['s_bkId']){
			$where .= " and b2b_booking.b2b_booking_id={$params['s_bkId']}";
		}
		if($params['s_office_status']){
			$where .= " and b2b_booking.office_status={$params['s_office_status']}";	
		}
		if($params['s_booking_status']){
			$where .= " and b2b_booking.booking_status={$params['s_booking_status']}";	
		}
		// return $where;
		$query =  $this->table("b2b_booking")->field([
			'b2b_booking.b2b_booking_id as BKID','company_order.create_time','b2b_booking_invoice.invoice_number as invoice','b2b_booking_invoice.balance_due as Sales','b2b_tour.tour_name','b2b_tour.tour_name_ch','company_order.begin_time as tourDate','b2b_booking.bus_no','language.language_name','b2b_booking.consultant','b2b_booking.agent_reference_id','b2b_booking.is_pre_paid','b2b_booking.special_requests','b2b_booking.emergency_phone','b2b_booking.lead_pax_mobile','b2b_booking.tour_guide','b2b_booking.tour_contact','b2b_booking.booking_status','b2b_booking.office_status','b2b_booking.company_order_number','b2b_tour.is_inbound','distributor.distributor_code',
			"(select GROUP_CONCAT(hotel_name_cn,' ',hotel_name_en) from b2b_hotel where b2b_hotel.b2b_hotel_id=b2b_booking.hotel_first_day) as hotel_first_day",
			"(select GROUP_CONCAT(hotel_name_cn,' ',hotel_name_en) from b2b_hotel where b2b_hotel.b2b_hotel_id=b2b_booking.hotel_last_day) as hotel_last_day",
			"(select GROUP_CONCAT(hotel_name_cn,' ',hotel_name_en) from b2b_hotel where b2b_hotel.b2b_hotel_id=b2b_booking.hotel_both_day) as hotel_both_day"
		])
		->join('company_order','company_order.company_order_number=b2b_booking.company_order_number')
		->join('b2b_booking_invoice','b2b_booking.b2b_booking_id=b2b_booking_invoice.b2b_booking_id')
		->join('b2b_tour','b2b_tour.btb_tour_id=b2b_booking.btb_tour_id')
		->join('language','language.language_id=b2b_booking.language_id')
		->join('distributor','distributor.distributor_id=b2b_booking.agent_id')
		->where($where)->order('BKID desc');
		if($params['page']){
			$query->limit($slimit,$params['page_size']);
		}
		$dd = $query->select();

 		foreach ($dd as $key => $value) {
 			//获取游客信息
 			$dd[$key]['customer'] = $this->table('company_order_customer')->field([
 				'customer.customer_first_name','customer.customer_last_name','customer.gender','customer.passport_number','customer.card_number','customer.birthday','customer.age_group','company_order_customer.temp_id','company_order_customer.seat_no'
 			])->join('customer','customer.customer_id=company_order_customer.customer_id')->
 			where("company_order_customer.company_order_number='{$value['company_order_number']}' and company_order_customer.status=1 and company_order_customer.type=2")->select();

 			//用房
 			$dd[$key]['room'] = $this->table('b2b_booking_room')->
 			field(['b2b_booking_room.temp_group','b2b_booking_room.seat_group','b2b_booking_room.room_type'])->where("b2b_booking_room.b2b_booking_id={$value['BKID']} and b2b_booking_room.status=1")->select();
 					
 			//接送机		
 			$dd[$key]['transfer'] = $this->table('b2b_booking_transfer')
 			->field(['temp_group','seat_group','airport','transfer_type','flight','bus_pax','pickup_location','date','time'])->where("b2b_booking_transfer.b2b_booking_id={$value['BKID']} and b2b_booking_transfer.status=1")->select();	

 			//前后
 			$dd[$key]['accommodation'] = $this->table('b2b_booking_accommodation')->field(['temp_group','seat_group','accommodation_type','room_type','nights'])->where("b2b_booking_accommodation.b2b_booking_id=1 and b2b_booking_accommodation.status={$value['BKID']}")->select(); 	
 		}

 		return $dd;

	}


	public function updateApprovePlan($params){
		try{
			foreach ($params as $key => $value) {
						if($value['BKID']){
							$where['b2b_booking_id'] = $value['BKID']; 
							$up['bus_no'] = $value['bus_no'];
							$up['booking_status'] = $value['booking_status'];
							$up['office_status'] = $value['office_status'];
							$this->table('b2b_booking')->where($where)->update($up);

							//company_order_number
							$b2b_booking = $this->table('b2b_booking')->where($where)->find();
							$w['company_order_number'] = $b2b_booking['company_order_number'];
							$company_order_customer = $this->table('company_order_customer')->where($w)->select();	
							foreach ($company_order_customer as $kk => $vv) {
								$uw['company_order_customer_id'] = $vv['company_order_customer_id'];
								if($value['seat'][$kk]){
									$u['seat_no'] = $value['seat'][$kk];
								}else{
									$u['seat_no'] = NULL;
								}
								$this->table('company_order_customer')->where($uw)->update($u); 		
							}	
						}
					}
					return 1;
		}catch (\Exception $e) {
			$result = $e->getMessage();
			\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
		}
		
				
	}


	public function top10AgentsByState($params){

		$where = 'company_order_customer.status=1  and b2b_booking.booking_status<>6';

		if($params['user_company_id']){ 
			$where .= " and company_order.company_id={$params['user_company_id']}";
		}	
		if($params['created_from']){
			$s_createdFrom = strtotime($params['created_from']);
			$where .= " and company_order.create_time>=$s_createdFrom";	
		}
		if($params['created_to']){
			$s_createdTo = strtotime($params['created_to']);
			$where .= " and company_order.create_time<=$s_createdTo"; 
		}

		

		$return = $this->table('b2b_booking')->field(
			['company_order_customer.customer_id','distributor.state','b2b_booking.agent_id',
'distributor.distributor_name','distributor.distributor_name_chinese','b2b_tour.is_inbound']
		)
		->join('company_order_customer','company_order_customer.company_order_number=b2b_booking.company_order_number')
		->join('distributor','distributor.distributor_id=b2b_booking.agent_id')
		->join('b2b_tour','b2b_tour.btb_tour_id=b2b_booking.btb_tour_id')
		->join('company_order','company_order.company_order_number=b2b_booking.company_order_number')->where($where)
		->select();

		return $return;
	}

	public function reportBookings($params){
		try{
			$where = '1=1  and b2b_booking.booking_status<>6';
		 
			if($params['user_company_id']){ 
				$where .= " and company_order.company_id={$params['user_company_id']}";
			} 

			//订单创建日期
			if($params['BookingCreatedFrom']){
				$BookingCreatedFrom = strtotime($params['BookingCreatedFrom']);
				$where .= " and company_order.create_time>={$BookingCreatedFrom}";		
			}
			if($params['BookingCreatedTo']){
				$BookingCreatedTo = strtotime($params['BookingCreatedTo']);
				$where .= " and company_order.create_time<={$BookingCreatedTo}";	
			}

			//出团日期
			if($params['TourDateFrom']){
				$TourDateFrom = strtotime($params['TourDateFrom']);
				$where .= " and company_order.begin_time>={$TourDateFrom}";	
			}
			if($params['TourDateTo']){
				$TourDateFrom = strtotime($params['TourDateTo']);
				$where .= " and company_order.TourDateTo<={$TourDateTo}";	
			}

			if($params['TourName']){
				$where .= " and b2b_tour.tour_name like '%{$params['TourName']}%'";	
			}

			if($params['TourType']){
				$TourType = $params['TourType']==1?0:1;	
				$where .= " and b2b_tour.is_inbound={$TourType}";
			}
			if($params['AgentName']){
				$where .= " and distributor.distributor_name like '%{$params['AgentName']}%'";	
			}
			if($params['State']){
				$where .= " and distributor.state={$params['State']}";	
			}

			

			$return = $this->table('b2b_booking')->field([
				'company_order.create_time as BookingDate',
				'b2b_booking.b2b_booking_id as BookingId',
				'b2b_booking.bus_no as Bus',
				'b2b_booking_invoice.balance_due as sales',
				'distributor.distributor_code as AgentCode',
				'distributor.distributor_name',
				'distributor.distributor_name_chinese',
				'b2b_tour.tour_name',
				'b2b_tour.tour_name_ch',
				'company_order.begin_time as TourDate',
				'b2b_tour.tour_type2',
				'b2b_tour.tour_type1',
				"(select b2b_tour_type.tour_type_name from b2b_tour_type where b2b_tour_type.tour_type_id=b2b_tour.tour_type2) as Category",			
	"(select sum(b2b_booking_sales.net_total) from b2b_booking_sales where b2b_booking_sales.b2b_booking_id=b2b_booking.b2b_booking_id and b2b_booking_sales.is_gst=1 and b2b_booking_sales.status='enable') as net_total_gst",'company_order.company_order_number','b2b_tour.is_inbound'])
			->join('company_order','b2b_booking.company_order_number=company_order.company_order_number')
			->join('b2b_booking_invoice','b2b_booking_invoice.b2b_booking_id=b2b_booking.b2b_booking_id')
			->join('distributor','distributor.distributor_id=b2b_booking.agent_id')
			->join('b2b_tour','b2b_tour.btb_tour_id=b2b_booking.btb_tour_id')
			->where($where)->select(); 

			foreach ($return as $key => $value) {
				//获取游客信息
	 			$return[$key]['customer'] = $this->table('company_order_customer')->field([
	 				'customer.customer_first_name','customer.customer_last_name','customer.gender','customer.passport_number','customer.card_number','customer.birthday','customer.age_group','company_order_customer.temp_id','company_order_customer.seat_no'
	 			])->join('customer','customer.customer_id=company_order_customer.customer_id')->
	 			where("company_order_customer.company_order_number='{$value['company_order_number']}' and company_order_customer.status=1 and company_order_customer.type=2")->select(); 
			} 
		}catch (\Exception $e) {
			// return $this->table('b2b_booking')->getLastSql();	
			$result = $e->getMessage();
			\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
		} 

		return $return;

	}

	public function getDistributorState(){
		// select state from distributor where part=2  GROUP BY state
		$where = "part=2 and state<>''";
		if($params['user_company_id']){ 
				$where .= " and company_order.company_id={$params['user_company_id']}";
		}  
		return $this->table('distributor')->field(['state'])->where($where)->group('state')->select();

	}

	public function reportTours($params){
		$where = '1=1';
		if($params['user_company_id']){ 
			$where .= " and branch_product.company_id={$params['user_company_id']}";
		}

		//订单创建日期
		$w1 = ' and 1=1  and b2b_booking.booking_status<>6';
		if($params['BookingCreatedFrom']){
			$BookingCreatedFrom = strtotime($params['BookingCreatedFrom']);
			$w1 .= " and company_order.create_time>={$BookingCreatedFrom}";  
		}
		if($params['BookingCreatedTo']){
			$BookingCreatedTo = strtotime($params['BookingCreatedTo']);
			$w1 .= " and company_order.create_time<={$BookingCreatedTo}";
		}
		//代理信息
		if($params['AgentName']){
			$w1 .= " and distributor.distributor_name like '%{$params['AgentName']}%'";
		}
		if($params['State']){
			$w1 .= " and distributor.state='{$params['State']}'";
		}

		//出团日期
		if($params['TourDateFrom']){
			$TourDateFrom = strtotime($params['TourDateFrom']);	
			$w1 .= " and company_order.begin_time>={$TourDateFrom}";
		}
		if($params['TourDateTo']){
			$TourDateTo = strtotime($params['TourDateTo']);	
			$w1 .= " and company_order.begin_time<={$TourDateTo}";
		}

		//B2B产品	
		if($params['TourName']){
			$where .= " and b2b_tour.tour_name like '%{$params['TourName']}%'";
		}
		if($params['TourType']){
			$TourType = $params['TourType']==1?0:1;
			$where .= " and b2b_tour.is_inbound={$TourType}";
		}

  
  		

		$return = $this->table('b2b_tour')->field([
			'b2b_tour.btb_tour_id as tourId','b2b_tour.tour_name','b2b_tour.tour_name_ch'
			,"(select b2b_tour_type.tour_type_name from b2b_tour_type where b2b_tour_type.tour_type_id=b2b_tour.tour_type2) as TourCategory"
			,"(select count(b2b_booking.b2b_booking_id) from b2b_booking INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number INNER JOIN distributor on distributor.distributor_id=b2b_booking.agent_id where b2b_booking.btb_tour_id=b2b_tour.btb_tour_id {$w1}) as bookings"
			,"(select count(company_order_customer.customer_id) from company_order_customer where  company_order_customer.b2b_booking_id in ((select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number INNER JOIN distributor on distributor.distributor_id=b2b_booking.agent_id where b2b_booking.btb_tour_id=b2b_tour.btb_tour_id {$w1}))) as pax"
			,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  company_order_customer.b2b_booking_id in ((select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number INNER JOIN distributor on distributor.distributor_id=b2b_booking.agent_id where b2b_booking.btb_tour_id=b2b_tour.btb_tour_id {$w1})) and customer.age_group='Adult') as Adult"
			,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  company_order_customer.b2b_booking_id in ((select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number INNER JOIN distributor on distributor.distributor_id=b2b_booking.agent_id where b2b_booking.btb_tour_id=b2b_tour.btb_tour_id {$w1})) and customer.age_group='Child') as Child"
			,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  company_order_customer.b2b_booking_id in ((select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number INNER JOIN distributor on distributor.distributor_id=b2b_booking.agent_id where b2b_booking.btb_tour_id=b2b_tour.btb_tour_id {$w1})) and customer.age_group='Infant') as Infant"
			,"(select SUM(balance_due) from b2b_booking_invoice where b2b_booking_invoice.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number INNER JOIN distributor on distributor.distributor_id=b2b_booking.agent_id where b2b_booking.btb_tour_id=b2b_tour.btb_tour_id {$w1})) as Sales"
			,"(select sum(net_total) from b2b_booking_sales where b2b_booking_sales.is_gst=1 and b2b_booking_sales.status='enable' and b2b_booking_sales.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number INNER JOIN distributor on distributor.distributor_id=b2b_booking.agent_id where b2b_booking.btb_tour_id=b2b_tour.btb_tour_id {$w1})) as GST"
		])
		->join('branch_product','branch_product.branch_product_id=b2b_tour.supplier_tour')
		->where($where)->select();

		return $return;
	}


	public function reportAgents($params){
		//B2B供应商
		$where = "distributor.part=2 and distributor.status=1";
		if($params['user_company_id']){ 
			$where .= " and distributor.company_id={$params['user_company_id']}";
		}

		$w1 = " and 1=1 and b2b_booking.booking_status<>6";
		if($params['BookingCreatedFrom']){
			$BookingCreatedFrom = strtotime($params['BookingCreatedFrom']);
			$w1 .= " and company_order.create_time>={$BookingCreatedFrom}";
		}	
		if($params['BookingCreatedTo']){
			$BookingCreatedTo = strtotime($params['BookingCreatedTo']);
			$w1 .= " and company_order.create_time<={$BookingCreatedTo}";
		}

		if($params['TourDateFrom']){
			$TourDateFrom = strtotime($params['TourDateFrom']);
			$w1 .= " and company_order.begin_time>={$TourDateFrom}";
		}
		if($params['TourDateTo']){
			$TourDateTo = strtotime($params['TourDateTo']);
			$w1 .= " and company_order.begin_time<={$TourDateTo}";
		}

		if($params['TourName']){
			$w1 .= " and b2b_tour.tour_name like '%{$params['TourName']}%'";	
		}

		if($params['State']){
			$where .= " and distributor.state='{$params['State']}'";	
		}

		$return = $this->table("distributor")->field([
			'distributor.distributor_id'
			,'distributor.distributor_code as agentCode'
			,'distributor.distributor_name as agentName'
			,'distributor.state'
			,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  customer.age_group='Adult' and  company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=1  {$w1})) as InboundAdult"
			,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  customer.age_group='Child' and  company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=1 {$w1})) as InboundChild"
			,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  customer.age_group='Infant' and  company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=1 {$w1})) as InboundInfant"
			,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=1 {$w1})) as InboundPax"
			,"(select sum(b2b_booking_invoice.balance_due) from b2b_booking_invoice where b2b_booking_invoice.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=1 {$w1})) as InboundSales"
,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  customer.age_group='Adult' and  company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=0 {$w1})) as OutboundAdult"
,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  customer.age_group='Child' and  company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=0 {$w1})) as OutboundChild"
,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where  customer.age_group='Infant' and  company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=0 {$w1})) as OutboundInfant"
,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=0 {$w1})) as OutboundPax"
,"(select sum(b2b_booking_invoice.balance_due) from b2b_booking_invoice where b2b_booking_invoice.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.is_inbound=0 {$w1})) as OutboundSales"
,"(select count(customer.customer_id) from company_order_customer left JOIN  customer ON customer.customer_id=company_order_customer.customer_id where company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.supplier_tour>0 {$w1})) as supplierPax"
,"(select sum(b2b_receivable_cope.total_price) from b2b_receivable_cope where  b2b_receivable_cope.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN b2b_tour on b2b_booking.btb_tour_id=b2b_tour.btb_tour_id INNER JOIN company_order on company_order.company_order_number=b2b_booking.company_order_number where b2b_booking.agent_id=distributor.distributor_id and b2b_tour.supplier_tour>0 {$w1})) as total_price"
		])->where($where)->select();

		return $return;

	}

	public function noOfPax($params){
		$form = strtotime($params['date'].'00:00:00');
		$to = strtotime($params['date'].'23:59:59');
		$where = "begin_time>={$form} and begin_time<={$to} and btb_tour_id={$params['btb_tour_id']} and b2b_booking.booking_status<>6";
		if($params['user_company_id']){ 
				$where .= " and company_order.company_id={$params['user_company_id']}";
		}

		return $this->table('company_order_customer')->field([
			'count(customer.customer_id) as pax'
		])->join('customer','customer.customer_id=company_order_customer.customer_id')
		->where("company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order ON company_order.company_order_number=b2b_booking.company_order_number where {$where})")->select(); 
	}


	public function dashboard($params){

	

		//本周的订单数
		$start = strtotime($params['start'].' 00:00:00');
		$end = strtotime($params['end'].' 23:59:59');

		$w = "company_order.create_time>={$start} and company_order.create_time<={$end} and b2b_booking.booking_status<>6";
		if($params['user_company_id']){ 
				$w .= " and company_order.company_id={$params['user_company_id']}";
		}

		$d['thisWeekCbooking'] = $this->table('b2b_booking')->field([
			'count(b2b_booking.b2b_booking_id) as cBooking'
		])
		->join('company_order','b2b_booking.company_order_number=company_order.company_order_number')
		->where($w)->find()['cBooking'];

		//本周收客人数
		$d['thisWeekPax'] = $this->table('company_order_customer')->field([
			'count(customer.customer_id) as pax'
		])->join('customer','customer.customer_id=company_order_customer.customer_id')
		->where("company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order ON company_order.company_order_number=b2b_booking.company_order_number where {$w})")->find()['pax']; 


		//上周订单
		$lw_start = strtotime($params['lw_start'].' 00:00:00');
		$lw_end = strtotime($params['lw_end'].' 23:59:59');

		$w2 = "company_order.create_time>={$lw_start} and company_order.create_time<={$lw_end} and b2b_booking.booking_status<>6";
		if($params['user_company_id']){ 
				$w2 .= " and company_order.company_id={$params['user_company_id']}";
		}
		$d['lastWeekCbooking'] = $this->table('b2b_booking')->field([
			'count(b2b_booking.b2b_booking_id) as cBooking'
		])
		->join('company_order','b2b_booking.company_order_number=company_order.company_order_number')
		->where($w2)->find()['cBooking'];
		//上周收客
		$d['lastWeekPax'] = $this->table('company_order_customer')->field([
			'count(customer.customer_id) as pax'
		])->join('customer','customer.customer_id=company_order_customer.customer_id')
		->where("company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order ON company_order.company_order_number=b2b_booking.company_order_number where {$w2})")->find()['pax']; 

		//本周的收客人数
		//周1
		for($i=1;$i<=7;$i++){
		  $d['this_tpax'][$i] = $this->f($params,$i,1);
		}

		//上周
		for($i=1;$i<=7;$i++){
		  $d['last_tpax'][$i] = $this->f($params,$i,2);
		}

		return $d;
	}

	public function f($params,$i,$type){
		if($type==1){ //本周
			$start_w1 = strtotime($params['start'].' 00:00:00')+(($i-1) * 24*60*60);
			$end_w1 = strtotime($params['start'].' 23:59:59')+(($i-1) * 24*60*60);
		}else{ //上周
			$start_w1 = strtotime($params['lw_start'].' 00:00:00')+(($i-1) * 24*60*60);
			$end_w1 = strtotime($params['lw_start'].' 23:59:59')+(($i-1) * 24*60*60);
		}
		
		$weer1 = "company_order.create_time>={$start_w1} and company_order.create_time<={$end_w1}  and b2b_booking.booking_status<>6";
		if($params['user_company_id']){ 
				$weer1 .= " and company_order.company_id={$params['user_company_id']}";
		}
		return  $this->table('company_order_customer')->field([
			'count(customer.customer_id) as pax'
		])->join('customer','customer.customer_id=company_order_customer.customer_id')
		->where("company_order_customer.b2b_booking_id in (select b2b_booking.b2b_booking_id from b2b_booking INNER JOIN company_order ON company_order.company_order_number=b2b_booking.company_order_number where {$weer1})")->find()['pax'];
	}


	public function export_to_msword($params){
	
		$b2b_booking_id = $params['b2b_booking_id'];

		$where['b2b_booking.b2b_booking_id'] = $b2b_booking_id;
		$ar['booking_customer'] = $this->table('b2b_booking')->field([
			'b2b_booking.voucher_number','company_order.create_time as bookedDate','company_order.begin_time as tourDate','b2b_tour.tour_name','b2b_tour.tour_name_ch',
			'distributor.distributor_name as agentName','distributor_consultant.consultant_name as agentConsultant','b2b_booking.agent_reference_id',
			'language.language_name','b2b_booking.special_requests','b2b_booking.is_pre_paid'
		])
		->join('distributor','distributor.distributor_id=b2b_booking.agent_id','left')
		->join('company_order','company_order.company_order_number=b2b_booking.company_order_number','left')
		->join('b2b_tour','b2b_tour.btb_tour_id=b2b_booking.btb_tour_id','left')
		->join('distributor_consultant','distributor_consultant.distributor_consultant_id=b2b_booking.consultant','left')
		->join('language','b2b_booking.language_id=language.language_id','left')
		->where($where)->find();

		$sql = "select company_order_customer.temp_id,customer.customer_first_name,
			customer.customer_last_name,customer.gender,customer.nationality,customer.birthday,customer.passport_number,term_of_validity 
			 from company_order_customer
			 LEFT JOIN customer on company_order_customer.customer_id = customer.customer_id
			 LEFT JOIN `language` on customer.language_id=language.language_id 
			 where company_order_customer.b2b_booking_id={$b2b_booking_id} and company_order_customer.status=1  ORDER BY temp_id asc";

		$ar['customer'] = Db::query($sql);

		$sql = "select * from b2b_booking_room where b2b_booking_room.b2b_booking_id={$b2b_booking_id} and b2b_booking_room.`status`=1";
		$ar['home'] = Db::query($sql);

		$sql = "select * from b2b_booking_transfer where b2b_booking_transfer.b2b_booking_id={$b2b_booking_id} and b2b_booking_transfer.`status`=1";
		$ar['transfer'] = Db::query($sql);

		$sql = "select * from b2b_booking_accommodation where b2b_booking_id={$b2b_booking_id} and status=1  ORDER BY accommodation_type asc";
		$ar['accommodation'] = Db::query($sql);

		

		return $ar;

	}


	public function invoice($params){
		$where = '1=1';

		if($params['agent_code']){
			$where .= " and b.agent_id={$params['agent_code']}";
		}
		if($params['select_a_tour']){
			$where .= " and b.btb_tour_id={$params['select_a_tour']}";
		}
		if($params['invoiceNo']){
			$where .= " and a.invoice_number like'%{$params['invoiceNo']}%'";
		}
		if($params['sTourDepartureDate']){
			$where .= " and FROM_UNIXTIME(c.begin_time,'%Y-%m-%d')>='{$params['sTourDepartureDate']}'";
		}
		if($params['eTourDepartureDate']){
			$where .= " and FROM_UNIXTIME(c.begin_time,'%Y-%m-%d')<='{$params['eTourDepartureDate']}'";
		}
		if($params['accoutCode']){
			$where .= " and a.a.account_code like '%{$params['accoutCode']}%'";
		}
		if($params['tour_type']){
			$where .= " and d.is_inbound={$params['tour_type']}";
		}
		if($params['is_invoice_ready']){
			$where .= " and a.is_ready={$params['is_invoice_ready']}";
		}
		if($params['cost_approval']){
			$where .= " and a.approval={$params['cost_approval']}";
		}

	

		$sql = "select a.b2b_booking_invoice_id,a.b2b_booking_id,a.invoice_number,a.account_code,a.myob_sales as ms,a.myob_cost as mc,
a.balance_due,a.due_date,a.invoice_type,a.status as invoice_status,a.is_ready,a.total_cost,a.total_sales,a.approval,
c.begin_time,d.tour_name,d.tour_name_ch,d.is_inbound,e.distributor_code,
(select count(company_order_customer.company_order_customer_id) from company_order_customer where company_order_customer.b2b_booking_id=b.b2b_booking_id and company_order_customer.`status`<>0) as pax
,(select sum(b2b_booking_sales.net_total) from b2b_booking_sales where b2b_booking_sales.b2b_booking_id=b.b2b_booking_id and b2b_booking_sales.`status`=1 and b2b_booking_sales.is_gst=1)  as gst_net_total
 from b2b_booking_invoice as a
 join b2b_booking as b on a.b2b_booking_id=b.b2b_booking_id
 join company_order as c on b.company_order_number=c.company_order_number
 join b2b_tour as d on d.btb_tour_id=b.btb_tour_id
 join distributor as e on e.distributor_id=b.agent_id
 where {$where}";
 		$ar = Db::query($sql);
		return $ar;
	}


	public function upInvoice($params){ 
		foreach ($params as $key => $value) {
			$where['b2b_booking_invoice_id'] = $value['b2b_booking_invoice_id']; 
			$up['approval'] = $value['approval'];
			$this->table('b2b_booking_invoice')->where($where)->update($up);
		}
		return 1;
	}


}