<?php


namespace app\index\model\dispatch;


use app\common\help\Help;
use app\index\model\source\Supplier;
use app\index\model\system\User;
use think\Db;
use think\Model;

class TH extends Model
{
    protected $table = 'wms_th';

    public function initialize()
    {
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    /**
     * 获取客户
     * 胡
     */
    public function getTH($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";
    

    
    
    	if($is_count==true){
    		$result = $this->table("wms_th")->where($data)->count();
    	}else {
    		if ($is_page == true) {
    			$result = $this->table("wms_th")->
    
    			where($data)->limit($page, $page_size)->order('create_time desc')->
    			field(['wms_th_point_uuid','temperature','humidity','create_time',
    

    			])->select();
    			 
    			 
    		}else{
    			$result = $this->table("wms_th")->alias('wms_th')->
    
    			where($data)->order('create_time desc')->
    			field(['wms_th_point_uuid','temperature','humidity','create_time',

    			])->select();
    		}
    	}
    
    	return $result;
    
    }    
    
    
    
}