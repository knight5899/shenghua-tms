<?php


namespace app\index\model\despatch;


use app\index\model\Base;

class DespatchGoods extends Base
{
    protected $table = "despatch_goods";

//添加短驳商品
    public function addDespatchGoods($params)
    {



        foreach ($params['goods_id'] as $k => $v) {
            $data['goods_id'] = $v;
            $data['goods_name'] = $params['goods_name'][$k];
            $data['package_number'] = $params['package_number'][$k];
            $data['despatch_number'] = $params['despatch_number'][$k];
            $data['despatch_weight'] = $params['despatch_weight'][$k];
            $data['despatch_volume'] = $params['despatch_volume'][$k];
            $data['billing_unit'] = $params['billing_unit'][$k];
            $data['despatch_unit'] = $params['despatch_unit'][$k];
            $data['despatch_money'] = $params['despatch_money'][$k];
            $data['despatch_id'] = $v['despatch_id'][$k];

            return $this->pubInsert($data);

        }


    }

}