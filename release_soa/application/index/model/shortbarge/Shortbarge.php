<?php


namespace app\index\model\shortbarge;


use app\index\model\Base;
use app\index\model\despatch\Despatch;
use app\index\model\source\Vehicle;
use app\index\model\system\User;

class Shortbarge extends Base
{
    protected $table = "short_barge";

    public function getNumber($params)
    {

        $data = $this->where('contract_number', 'like', '%' . $params['keyword'] . '%')->limit(0, 10)
            ->field(['contract_number' => 'name', 'short_barge_id' => 'value'])
            ->select();

        return $data;
    }

    public function getCount(){

        $data['short_barge_record'] = $this->count();
        $data['short_barge_print_record'] = $this->where('print', 1)->count();
        return $data;


    }


    public function getShortBarge($params)
    {
        $usermodel = new User();
        $where = [];

        if(isset($params['short_barge_id']))
            $where['short_barge_id']=$params['short_barge_id'];
        if (isset($params['print']))
            $where['print'] = 1;
        $where['status'] = $params['status'] ? $params['status'] : 1;
        $data = $this->pubGetData($where, true, ['short_barge_id' => 'desc']);
        $data['short_barge_record'] = $this->count();
        $data['short_barge_print_record'] = $this->where('print', 1)->count();


        foreach ($data['list'] as $k => $v) {
            $user_info = $usermodel->where('user_id', $v['create_user_id'])->find();
            $data['list'][$k]['create_user_name'] = $user_info->nickname;
            $user_info = $usermodel->where('user_id', $v['update_user_id'])->find();
            $data['list'][$k]['update_user_name'] = $user_info->nickname;


        }

        return $data;
    }


    public function addShortBarge($params)
    {
        $data['short_barge_time'] = strtotime($params['short_barge_time']);
        $vehicle_info = (new Vehicle())->where('vehicle_id', $params['vehicle_id'])->find();
        $data['car_number'] = $vehicle_info['number_plate'];
        $data['vehicle_id'] = $params['vehicle_id'];
        $data['car_type'] = $params['car_type'];
        $data["accept_province_id"] = $params["accept_province_id"];
        $data["accept_city_id"] = $params["accept_city_id"];
        $data["accept_area_id"] = $params["accept_area_id"];
        $data["accept_address"] = $params["accept_address"];
        $data["remark"] = $params["remark"];
        $data["cost_all"] = $params["cost_all"];
        $data["market_price_all"] = $params["market_price_all"];
        $data["share_type"] = $params["share_type"];
        $data["supplier_id"] = $params["supplier_id"];
        $data["supplier_type"] = $params["supplier_type"];
        $data["create_time"] = time();
        $data["update_time"] = time();
        $data["create_user_id"] = $params["user_id"];
        $data["update_user_id"] = $params["user_id"];

//error_log(json_encode($params));
        $id = $this->pubInsert($data);
        if ($id) {
            $short_barge_info = $this->where('short_barge_id', $id)->find();
            $short_barge_info->contract_number = "DB" . $id;
            $short_barge_info->orders_id = implode(',', $params['orders_id']);
            $short_barge_info->despatch_id = implode(',', $params['despatch_id']);

            $short_barge_info->save();

        }
        $params['short_barge_id'] = $id;
        $short_barge_model = new ShortbargeGoods();
        $short_barge_model->insertGoods($params);

        $despatch_model = new Despatch();
        $despatch_model->whereIn('despatch_id', $params['despatch_id'])->update(['short_barge' => 2]);


        return $id;
    }


}