<?php


namespace app\index\model\shortbarge;


use app\index\model\Base;

class ShortbargeGoods extends Base
{
    protected $table = "short_barge_goods";

    public function getShortBargeGoods($params){
     //$where['status']=$params['status']?$params['status']:1;
     $where['short_barge_id']=$params['short_barge_id'];
     $data=$this->pubGetData($where);
     return $data;
    }

    public function insertGoods($params)
    {
        $data = [];
        foreach ($params['despatch_id'] as $k => $v) {
            $tmp = [];
            $tmp['short_barge_id'] = $params['short_barge_id'];
            $tmp['despatch_id'] = $v;
            $tmp['cost'] = $params['cost'][$k];
            $tmp['goods_name'] = $params['goods_name'][$k];
            $tmp['goods_packaging_type_id'] = $params['goods_packaging_type_id'][$k];
            $tmp['goods_packaging_type_name'] = $params['goods_packaging_type_name'][$k];
            $tmp['market_price'] = $params['market_price'][$k];
            $tmp['orders_id'] = $params['orders_id'][$k];
            $tmp['send_address'] = $params['short_barge_send_address'][$k];
            $data[] = $tmp;

        }
        return $this->pubInsertAll($data);
    }

}