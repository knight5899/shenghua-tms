<?php

namespace app\index\model\equipment;
use think\Model;
use app\common\help\Help;
use think\config;
use think\Db;
class Equipment extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'equipment';

    public function initialize()
    {
    

    	parent::initialize();
    
    }

    /**
     * 添加设备
     * 胡
     */
    public function addEquipment($params){
    	$t = time();


    	if(isset($params['equipment_number_in'])){
    		$data['equipment_number_in'] = $params['equipment_number_in'];
    	}
    	if(isset($params['equipment_number_out'])){
    		$data['equipment_number_out'] = $params['equipment_number_out'];
    	}
    	
    	if(isset($params['equipment_type'])){
    		$data['equipment_type'] = $params['equipment_type'];
    	}

        	
    	if(!empty($params['remark'])){
    		$data['remark'] = $params['remark'];    		
    	}
    	$data['create_time'] = $t;
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = 1;


    
 
    	Db::startTrans();
    	try{
 
    		$pk_id = Db::name('equipment')->insertGetId($data);
			

    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    	
    	return $result;
    }
	
    /**
     * 获取
     * 胡
     */
    public function getEquipment($params,$is_count=false,$is_page=false,$page=null,$page_size=20){


    	$data = "1=1 ";

		if(is_numeric($params['equipment_status'])){
			//$data.=' and equipment.equipment_status' = $params['equipment_status'];
			$data.= " and equipment.equipment_status= ".$params['equipment_status'];
		}

        if($is_count==true){
            $result = $this->table("equipment")->alias("equipment")->

            where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("equipment")->alias('equipment')->

                where($data)->limit($page, $page_size)->order('equipment.create_time desc')->
                field([
                		'equipment.*',
	                	"(select nickname  from user where user.user_id = equipment.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = equipment.update_user_id)"=> 'update_user_name'

		                ])->select();
            }else{
                $result = $this->table("equipment")->alias('equipment')->

                where($data)->order('create_time desc')->
                field([
                		'equipment.*',
	                	"(select nickname  from user where user.user_id = equipment.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = equipment.update_user_id)"=> 'update_user_name'
                    	])->select();
            }
        }



    	return $result;
    
    }

    public function getEquipmentHistory($params){

        $data = "1=1 ";

        if(is_numeric($params['imei_status'])){
            //$data.=' and equipment.equipment_status' = $params['equipment_status'];
            $data.= " and equipment_history.imei_status= ".$params['imei_status'];
        }

        $result = $this->table("equipment_history")->alias('equipment_history')->

        where($data)->order('gps_time desc')->
        field([
            'equipment_history.*'
        ])->select();

        return $result;

    }

    /**
     * 修改设备添加token
     * 韩
     */
    public function editEquipment($params){
        $t = time();

        if(!empty($params['token'])){
            $data['token'] = $params['token'];
        }
        if(!empty($params['expiresin'])){
            $data['expiresin'] = $params['expiresin'];
        }
        if(!empty($params['time'])){
            $data['time'] = $params['time'];
        }
        if(!empty($params['mc_type_use_scope'])){
            $data['mc_type_use_scope'] = $params['mc_type_use_scope'];
        }
        if(!empty($params['equip_ype'])){
            $data['equip_ype'] = $params['equip_ype'];
        }
        if(!empty($params['sim'])){
            $data['sim'] = $params['sim'];
        }
        if(!empty($params['activation_time'])){
            $data['activation_time'] = $params['activation_time'];
        }
        if(is_numeric($params['imei_status'])){ //设备状态
            $data['imei_status'] = $params['imei_status'];
        }
        if(is_numeric($params['lng'])){ //设备状态
            $data['lng'] = $params['lng'];
        }
        if(is_numeric($params['lat'])){ //设备状态
            $data['lat'] = $params['lat'];
        }

        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];

        Db::startTrans();
        try {
            Db::name('equipment')->where("equipment_id = '" . $params['equipment_id']."'")->update($data);
            $result = 200;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 添加设备
     * 胡
     */
    public function addEquipmentAll($params){
        $t = time();

        $data['equipment_id'] = $params['equipment_id'];
        $data['imei'] = $params['imei'];
        $data['device_name'] = $params['device_name'];
        $data['icon'] = $params['icon'];
        $data['imei_status'] = $params['imei_status'];
        $data['lng'] = $params['lng'];
        $data['lat'] = $params['lat'];
        $data['expire_flag'] = $params['expire_flag'];
        $data['activation_flag'] = $params['activation_flag'];
        $data['pos_type'] = $params['pos_type'];
        $data['gps_time'] = $params['gps_time'];
        $data['hb_time'] = $params['hb_time'];
        $data['speed'] = $params['speed'];
        $data['acc_status'] = $params['acc_status'];
        $data['elect_quantity'] = $params['elect_quantity'];
        $data['power_value'] = $params['power_value'];
        $data['gps_num'] = $params['gps_num'];
        $data['direction'] = $params['direction'];
        $data['mileage'] = $params['mileage'];

//        $data['create_time'] = $t;
//        $data['update_time'] = $t;
//        $data['create_user_id'] = $params['user_id'];
//        $data['update_user_id'] = $params['user_id'];
//        $data['status'] = 1;


        Db::startTrans();
        try{

            $equipment_history_id = Db::name('equipment_history')->insertGetId($data);


            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }

        return $result;
    }
	
}