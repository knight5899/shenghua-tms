<?php

namespace app\index\model\position;

use think\Db;
use think\Model;

class Position extends Model
{
    protected $table = 'wms_position';

    /**
     * 获取货区
     * 韩
     */
    public function getPosition($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(!empty($params['position_id'])){ //货区id
            $data.=" and wms_position.position_id =".$params['position_id'];
        }

        if(!empty($params['position_name'])){ //货区名称
            $data.=' and wms_position.position_name ="'.$params['position_name'].'"';
        }

        if(!empty($params['company_id'])){ //公司id
            $data.=" and wms_position.company_id =".$params['company_id'];
        }

        if(!empty($params['company_name'])){ //公司名称
            $data.=' and company.company_name ="'.$params['company_name'].'"';
        }

        if(!empty($params['ware_id'])){ //仓库id
            $data.=" and wms_position.ware_id =".$params['ware_id'];
        }

        if(!empty($params['ware_name'])){ //仓库名称
            $data.=' and wms_position.ware_name ="'.$params['ware_name'].'"';
        }

        if(is_numeric($params['status'])){ //状态
            $data.=" and wms_position.status =".$params['status'];
        }

        if($is_count==true){
            $result = $this->table("wms_position")->
                join("company",'company.company_id=wms_position.company_id')->
                join("wms_warehouse",'wms_warehouse.ware_id=wms_position.ware_id')->
                where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("wms_position")->
                join("company",'company.company_id=wms_position.company_id')->
                join("wms_warehouse",'wms_warehouse.ware_id=wms_position.ware_id')->
                where($data)->limit($page, $page_size)->order('wms_position.create_time desc')->
                field(['wms_position.position_id','wms_position.position_name','wms_position.company_id',
                    'company.company_name','wms_position.ware_id','wms_warehouse.ware_name',
                    "(select nickname from user where user.user_id = wms_position.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = wms_position.update_user_id)"=> 'update_user_name',
                    'wms_position.status',
                    'from_unixtime(wms_position.create_time)'=> 'create_time',
                    'from_unixtime(wms_position.update_time)'=> 'update_time'
                    ])->select();
            }else{
                $result = $this->table("wms_position")->
                join("company",'company.company_id=wms_position.company_id')->
                join("wms_warehouse",'wms_warehouse.ware_id=wms_position.ware_id')->
                where($data)->
                field(['wms_position.position_id','wms_position.position_name','wms_position.company_id',
                    'company.company_name','wms_position.ware_id','wms_warehouse.ware_name',
                    "(select nickname from user where user.user_id = wms_position.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = wms_position.update_user_id)"=> 'update_user_name',
                    'wms_position.status',
                    'from_unixtime(wms_position.create_time)'=> 'create_time',
                    'from_unixtime(wms_position.update_time)'=> 'update_time'
                ])->select();
            }
        }
        return $result;
    }

    /**
     * 添加货区
     * 韩
     */
    public function addPosition($params){
        $t = time();

        $data['ware_id'] = $params['ware_id'];
        $data['company_id'] = $params['company_id'];
        $data['position_name'] = $params['position_name'];
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("wms_position")->insertGetId($data);
            $result = 1;
            // 提交事务
    		Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 修改货区
     * 韩
     */
    public function updatePositionByPositionId($params){
        $t = time();

        $data['ware_id'] = $params['ware_id'];
        $data['company_id'] = $params['company_id'];
        $data['position_name'] = $params['position_name'];
        $data['status'] = $params['status'];
//        $data['create_time'] = $t;
//        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try {
            Db::name('wms_position')->where("position_id = " . $params['position_id'])->update($data);
            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }
}