<?php

namespace app\index\model\turnoverbox;

use think\Db;
use think\Model;

class Turnoverbox extends Model
{
    protected $table = 'device_box';

    /**
     * 获取周转箱数据
     * 韩
     */
    public function getTurnoverbox($params){
        $data = "1=1 ";

        if(is_numeric($params['status'])){ //状态
            $data.=" and device_box.status =".$params['status'];
        }

        $result = $this->table("device_box")->
        where($data)->order('device_box.create_time desc')->
        field(['device_box.device_number','device_box.bd_location','device_box.gps_location','device_box.voltage'
            ])->select();

        return $result;
    }
}