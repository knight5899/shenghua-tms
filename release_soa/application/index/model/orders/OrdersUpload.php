<?php


namespace app\index\model\orders;


use think\Db;
use think\Model;

class OrdersUpload extends Model
{

    protected  $table="orders_upload";

public function getUploadByOrdersId($params){
   $uploads= $this->where('orders_id',$params['orders_id'])->select();

return $uploads;
}

public  function  delUploadById($params){
    $upload_info=$this->find($params['id']);
    if($upload_info)
    {   $type=$upload_info['type'];
        $orders_id=$upload_info->orders_id;
        $upload_info->delete();
        $count=$this->where(['orders_id'=>$orders_id,'type'=>$type])->count();
        if($count==0)
        {
            $order_model=new Orders();
            $order_info=$order_model->where('orders_id',$orders_id)->find();
           if($order_info)
           {
               $order_info->is_upload=(3-$type);
               $order_info->save();
           }

        }


        return 1;
    }
    return 0;
}

}