<?php

namespace app\index\model\orders;

use app\index\model\Base;
use app\index\model\source\Goods;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;

class Ordersgoods extends Base
{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'orders_goods';
    protected $append = ['goods_name'];
    private $_public_service;

    public function initialize()
    {
        $this->_public_service = new PublicService();
        parent::initialize();

    }




    public function getGoodsNameAttr($v, $d)
    {
        $goodsInfo = Goods::get($d['goods_id']);
        return $goodsInfo->goods_name;
    }

    /**
     * 添加订单
     * 胡
     */
    public function addOrders($params)
    {
        $t = time();

        $data['project_id'] = $params['project_id'];
        $data['pay_type'] = $params['pay_type'];//支付方式
        $data['bargain_type'] = $params['bargain_type'];
        if ($data['bargain_type'] == 2) {
            $data['bargain_price'] = $params['bargain_price'];
        }


        $data['transportation_type'] = $params['transportation_type'];

        $data['send_province_id'] = $params['send_province_id'];
        $data['send_city_id'] = $params['send_city_id'];
        $data['send_area_id'] = $params['send_area_id'];
        $data['send_address'] = $params['send_address'];
        $data['send_name'] = $params['send_name'];
        $data['send_cellphone'] = $params['send_cellphone'];

        $data['accept_province_id'] = $params['accept_province_id'];
        $data['accept_city_id'] = $params['accept_city_id'];
        $data['accept_area_id'] = $params['accept_area_id'];
        $data['accept_address'] = $params['accept_address'];
        $data['accept_name'] = $params['accept_name'];
        $data['accept_cellphone'] = $params['accept_cellphone'];
        //客户单号
        if (!empty($params['customer_order_number'])) {
            $data['customer_order_number'] = $params['customer_order_number'];
        }


        //代收货款
        if (!empty($params['replacement_prive'])) {
            $data['replacement_prive'] = $params['replacement_prive'];

        }

        //保险货值
        if (!empty($params['insurance_goods'])) {
            $data['insurance_goods'] = $params['insurance_goods'];

        }
        //提货时间
        if (!empty($params['pickup_time'])) {
            $data['pickup_time'] = $params['pickup_time'];

        }
        //送货时间
        if (!empty($params['send_time'])) {
            $data['send_time'] = $params['send_time'];

        }
        if (!empty($params['remark'])) {
            $data['remark'] = $params['remark'];

        }


        $data['orders_number'] = $params['orders_number'];
        $data['order_status'] = 1;
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = 1;


        Db::startTrans();
        try {
            $pk_id = Db::name('orders')->insertGetId($data);

            $this->_public_service->setNumber('orders', 'orders_id', $pk_id, 'orders_number', $data['orders_number'], $pk_id);

            //添加完成后再添加到订单商品表中
            for ($i = 0; $i < count($params['goods_id']); $i++) {
                $orders_goods = [];
                $orders_goods['orders_id'] = $pk_id;
                $orders_goods['goods_id'] = $params['goods_id'][$i];
                $orders_goods['estimated_count'] = $params['estimated_count'][$i];
                $orders_goods['estimated_pack_count'] = $params['estimated_pack_count'][$i];
                $orders_goods['estimated_weight'] = $params['estimated_weight'][$i];
                $orders_goods['estimated_volume'] = $params['estimated_volume'][$i];
                $orders_goods['realy_count'] = $params['realy_count'][$i];
                $orders_goods['realy_pack_count'] = $params['realy_pack_count'][$i];
                $orders_goods['realy_weight'] = $params['realy_weight'][$i];
                $orders_goods['realy_volume'] = $params['realy_volume'][$i];
                $orders_goods['create_time'] = $t;
                $orders_goods['create_user_id'] = $params['user_id'];
                $orders_goods['update_time'] = $t;
                $orders_goods['update_user_id'] = $params['user_id'];
                $orders_goods['status'] = 1;

                Db::name('orders_goods')->insertGetId($orders_goods);
            }
            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }

        return $result;
    }

    /**
     * 获取订单商品信息
     * 胡
     */
    public function getOrdersGoods($params, $is_count = false, $is_page = false, $page = null, $page_size = 20)
    {
        $data = "1=1 ";


        if (is_numeric($params['orders_goods_id_is_like']) && $params['orders_goods_id_is_like'] == 1) {
            $data .= " and orders_goods.orders_id in (" . $params['orders_id'] . ")";

        } else {
            if (is_numeric($params['orders_id'])) {
                $data .= " and orders_goods.orders_id =" . $params['orders_id'];
            }
        }

        if ($is_count == true) {
            $result = $this->table("orders_goods")->where($data)->count();
        } else {
            if ($is_page == true) {
                $result = $this->table("orders_goods")->
                join("goods", 'goods.goods_id= orders_goods.goods_id')->
                join("orders", 'orders.orders_id= orders_goods.orders_id')->
                where($data)->limit($page, $page_size)->order('orders_goods.create_time desc')->
                field(['orders_goods.*', 'goods.goods_name', 'goods.goods_packaging_type', 'goods.goods_specification',
                    'goods.goods_model', 'goods.goods_weight', 'goods_volume', 'goods.goods_length','goods.goods_specification',
                    'goods.goods_width', 'goods.goods_height', 'orders.send_name', 'orders.send_cellphone', 'orders.orders_number', 'orders.pickup_time', 'orders.send_time',
                    'orders.accept_address', 'orders.send_address',
                    "(select city_name  from city where city.city_id= orders.send_province_id)" => 'send_province_name',
                    "(select city_name  from city where city.city_id= orders.send_city_id)" => 'send_city_name',
                    "(select city_name  from city where city.city_id= orders.send_area_id)" => 'send_area_name',
                    "(select city_name  from city where city.city_id= orders.accept_province_id)" => 'accept_province_name',
                    "(select city_name  from city where city.city_id= orders.accept_city_id)" => 'accept_city_name',
                    "(select city_name  from city where city.city_id= orders.accept_area_id)" => 'accept_area_name',
                ])->select();


            } else {
                $result = $this->table("orders_goods")->alias('orders_goods')->
                join("goods", 'goods.goods_id= orders_goods.goods_id')->
                join("orders", 'orders.orders_id= orders_goods.orders_id')->
                where($data)->order('orders_goods.create_time desc')->
                field(['orders_goods.*', 'goods.goods_name', 'goods.goods_packaging_type', 'goods.goods_specification',
                    'goods.goods_model', 'goods.goods_weight', 'goods_volume', 'goods.goods_length','goods.goods_specification',
                    'goods.goods_width', 'goods.goods_height', 'orders.send_name', 'orders.send_cellphone', 'orders.orders_number', 'orders.pickup_time', 'orders.send_time',
                    'orders.accept_address', 'orders.send_address',
                    "(select city_name  from city where city.city_id= orders.send_province_id)" => 'send_province_name',
                    "(select city_name  from city where city.city_id= orders.send_city_id)" => 'send_city_name',
                    "(select city_name  from city where city.city_id= orders.send_area_id)" => 'send_area_name',
                    "(select city_name  from city where city.city_id= orders.accept_province_id)" => 'accept_province_name',
                    "(select city_name  from city where city.city_id= orders.accept_city_id)" => 'accept_city_name',
                    "(select city_name  from city where city.city_id= orders.accept_area_id)" => 'accept_area_name',

                ])->select();
            }
        }


        return $result;

    }

    /**
     * 获取订单商品信息不包含订单
     * 胡
     */
    public function getShipmentNeedOrder($params, $is_count = false, $is_page = false, $page = null, $page_size = 20)
    {
        $data = "1=1 ";


        if (is_numeric($params['orders_goods_id_is_like']) && $params['orders_goods_id_is_like'] == 1) {
            $data .= " and orders_goods.orders_id in (" . $params['orders_id'] . ")";

        } else {
            if (is_numeric($params['orders_id'])) {
                $data .= " and orders_goods.orders_id =" . $params['orders_id'];
            }
        }

        if ($is_count == true) {
            $result = $this->table("orders_goods")->where($data)->count();
        } else {
            if ($is_page == true) {
                $result = $this->table("orders_goods")->
                join("goods", 'goods.goods_id= orders_goods.goods_id')->

                where($data)->limit($page, $page_size)->order('orders_goods.create_time desc')->
                field(['orders_goods.*', 'goods.goods_name', 'goods.goods_packaging_type', 'goods.goods_specification',
                    'goods.goods_model', 'goods.goods_weight', 'goods_volume', 'goods.goods_length',
                    'goods.goods_width', 'goods.goods_height',

                ])->select();


            } else {
                $result = $this->table("orders_goods")->alias('orders_goods')->
                join("goods", 'goods.goods_id= orders_goods.goods_id')->

                where($data)->order('orders_goods.create_time desc')->
                field(['orders_goods.*', 'goods.goods_name', 'goods.goods_packaging_type', 'goods.goods_specification',
                    'goods.goods_model', 'goods.goods_weight', 'goods_volume', 'goods.goods_length',
                    'goods.goods_width', 'goods.goods_height',


                ])->select();
            }
        }


        return $result;

    }

    /**
     * 修改发货方
     */
    public function updateOrderByOrderId($params)
    {

        $t = time();
        $data['project_id'] = $params['project_id'];
        $data['pay_type'] = $params['pay_type'];//支付方式
        $data['bargain_type'] = $params['bargain_type'];
        if ($data['bargain_type'] == 2) {
            $data['bargain_price'] = $params['bargain_price'];
        } else {
            $data['bargain_price'] = 0;
        }


        $data['transportation_type'] = $params['transportation_type'];

        $data['goods_id'] = $params['goods_id'];
        $data['send_province_id'] = $params['send_province_id'];
        $data['send_city_id'] = $params['send_city_id'];
        $data['send_area_id'] = $params['send_area_id'];
        $data['send_address'] = $params['send_address'];
        $data['send_name'] = $params['send_name'];
        $data['send_cellphone'] = $params['send_cellphone'];

        $data['accept_province_id'] = $params['accept_province_id'];
        $data['accept_city_id'] = $params['accept_city_id'];
        $data['accept_area_id'] = $params['accept_area_id'];
        $data['accept_address'] = $params['accept_address'];
        $data['accept_name'] = $params['accept_name'];
        $data['accept_cellphone'] = $params['accept_cellphone'];
        //客户单号

        $data['customer_order_number'] = $params['customer_order_number'];


        $data['estimated_count'] = $params['estimated_count'];
        $data['realy_count'] = $params['realy_count'];

        $data['estimated_pack_count'] = $params['estimated_pack_count'];
        $data['realy_pack_count'] = $params['realy_pack_count'];


        $data['estimated_weight'] = $params['estimated_weight'];
        $data['realy_weight'] = $params['realy_weight'];

        $data['estimated_volume'] = $params['estimated_volume'];
        $data['realy_volume'] = $params['realy_volume'];


        //代收货款

        $data['replacement_prive'] = $params['replacement_prive'];


        //保险货值

        $data['insurance_goods'] = $params['insurance_goods'];


        //提货时间
        if ($params['pickup_time'] == '') {
            $data['pickup_time'] = null;

        }


        //送货时间
        if ($params['send_time'] == '') {
            $data['send_time'] = null;

        }


        $data['remark'] = $params['remark'];


        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;


        $source_price = [];
        Db::startTrans();
        try {
            Db::name('orders')->where("orders_id = " . $params['orders_id'])->update($data);
            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;
    }


}