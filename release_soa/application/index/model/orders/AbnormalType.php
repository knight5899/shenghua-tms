<?php


namespace app\index\model\orders;


use app\index\model\Base;

class AbnormalType extends Base
{
	protected $table="abnormal_type";

	public function getAbnormalList($params){
		$where=[];
		if (isset($params['keyword'])) {
			$where['abnormal_type_name'] = ['like', "%" . $params['keyword'] . "%"];
			$where['abnormal_type_pid']=['>',0];
		}
		if(isset($params['multi_abnormal_type_id']))
		if(!empty($params['multi_abnormal_type_id']))
		{
			$where['abnormal_type_id']=['in',$params['multi_abnormal_type_id']];
		}

		$data=$this->pubGetData($where,false);
		return $data;


	}
}