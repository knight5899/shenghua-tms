<?php


namespace app\index\model\orders;


use app\index\model\Base;

use think\Db;
class OrdersFollow extends Base
{
    protected $table="orders_follow";


    /**
     * 获取客户财务信息
     * 胡
     */
    public function getOrdersFollow($params,$is_count=false,$is_page=true,$page=null,$page_size=20){

        $data = "1=1 ";

        if(is_numeric($params['status'])){ //状态
            $data.= " and orders_follow.status = ".$params['status'];
        }
        
        if(isset($params['orders_number'])){ //状态
        	$data.= " and orders_follow.orders_number = ".$params['orders_number'];
        }
        
        
        if($is_count==true){
            $result = $this->table("orders_follow")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("orders_follow")->alias('orders_follow')->
                join('orders','orders.orders_id = orders_follow.orders_id','left')->
//                join('orders_goods','orders.orders_id = orders_goods.orders_id','left')->
                where($data)->limit($page, $page_size)->order('orders_follow.create_time desc')->
                field(['orders.orders_id','orders_finance.finance_id','orders_finance.money','orders.orders_number','orders_finance.invoice_status',
                    'orders.send_name','orders.send_city_id','orders.project_id',
                    "(select project_name from project where project_id=orders.project_id)"=> 'project_name',
                    "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
                    "(select sum(orders_income.money) from orders_income where orders_income.finance_id=orders_finance.finance_id and orders_income.status =1)"=> 'money2',
                    'orders.accept_name','orders.accept_city_id',
                    "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
//                    'orders_goods.realy_pack_count','orders_goods.realy_weight','orders_goods.realy_volume','orders_goods.realy_count',
                    'orders.delivery_method','orders.abnormal_status'
                    ])->select();

                //获取赔款总金额
                if($result){
                    foreach($result as $key=>$val){
                        $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
                    }
                }
            }else{
                $result = $this->table("orders_finance")->alias('orders_finance')->
                join('orders','orders.orders_id = orders_finance.orders_id','left')->
//                join('orders_goods','orders.orders_id = orders_goods.orders_id','left')->
                where($data)->order('orders_finance.create_time desc')->
                field(['orders.orders_id','orders_finance.finance_id','orders_finance.money','orders.orders_number','orders_finance.invoice_status',
                    'orders.send_name','orders.send_city_id','orders.project_id',
                    "(select project_name from project where project_id=orders.project_id)"=> 'project_name',
                    "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
                    "(select sum(orders_income.money) from orders_income where orders_income.finance_id=orders_finance.finance_id and orders_income.status =1)"=> 'money2',
                    'orders.accept_name','orders.accept_city_id',
                    "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
//                    'orders_goods.realy_count','orders_goods.realy_pack_count','orders_goods.realy_weight','orders_goods.realy_volume','orders_goods.realy_count',
                    'orders.delivery_method','orders.abnormal_status'
                ])->select();

                //获取赔款总金额
                if($result){
                    foreach($result as $key=>$val){
                        $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
                    }

                }
            }
        }
        return  $result;
    }

    public function getCustomerBillInData($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $str = "";
        foreach($params as $key=>$val){
            if($key!=count($params)){
                $str.=$val['finance_id'].",";
            }
        }
        $data = "1=1 and orders_finance.finance_id in (".substr($str,0,strlen($str)-1).")";

        if($is_count==true){
            $result = $this->table("orders_finance")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("orders_finance")->alias('orders_finance')->
                join('orders','orders.orders_id = orders_finance.orders_id','left')->
//                join('orders_goods','orders.orders_id = orders_goods.orders_id','left')->
                where($data)->limit($page, $page_size)->order('orders_finance.create_time desc')->
                field(['orders.orders_id','orders_finance.finance_id','orders_finance.money','orders.orders_number','orders_finance.invoice_status',
                    'orders.send_name','orders.send_city_id','orders.project_id',
                    "(select project_name from project where project_id=orders.project_id)"=> 'project_name',
                    "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
                    'orders.accept_name','orders.accept_city_id',
                    "(select sum(orders_income.money) from orders_income where orders_income.finance_id=orders_finance.finance_id and orders_income.status =1)"=> 'money2',
                    "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
//                    'orders_goods.realy_pack_count','orders_goods.realy_weight','orders_goods.realy_volume','orders_goods.realy_count',
                    'orders.delivery_method','orders.abnormal_status'
                ])->select();

                //获取赔款总金额
                if($result){
                    foreach($result as $key=>$val){
                        $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
                    }
                }
            }else{
                $result = $this->table("orders_finance")->alias('orders_finance')->
                join('orders','orders.orders_id = orders_finance.orders_id','left')->
//                join('orders_goods','orders.orders_id = orders_goods.orders_id','left')->
                where($data)->order('orders_finance.create_time desc')->
                field(['orders.orders_id','orders_finance.finance_id','orders_finance.money','orders.orders_number','orders_finance.invoice_status',
                    'orders.send_name','orders.send_city_id','orders.project_id',
                    "(select project_name from project where project_id=orders.project_id)"=> 'project_name',
                    "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
                    'orders.accept_name','orders.accept_city_id',
                    "(select sum(orders_income.money) from orders_income where orders_income.finance_id=orders_finance.finance_id and orders_income.status =1)"=> 'money2',
                    "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
//                    'orders_goods.realy_count','orders_goods.realy_pack_count','orders_goods.realy_weight','orders_goods.realy_volume',
                    'orders.delivery_method','orders.abnormal_status'
                ])->select();

                //获取赔款总金额
                if($result){
                    foreach($result as $key=>$val){
                        $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
                    }

                }
            }
        }
        return $result;
    }

    public function getCustomerYesBillInData($params){

        $data = "1=1 ";

        if(!empty($params['customer_bill_number'])){ //备注
            $data.=' and customer_bill.customer_bill_number ="'.$params['customer_bill_number'].'"';
        }

        $result = $this->table("orders_finance")->alias('orders_finance')->
        join('orders','orders.orders_id = orders_finance.orders_id','left')->
        join('orders_goods','orders.orders_id = orders_goods.orders_id','left')->
        join('customer_bill_info','customer_bill_info.orders_id = orders.orders_id','left')->
        join('customer_bill','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
        where($data)->order('orders_finance.create_time desc')->
        field(['orders.orders_id','orders_finance.finance_id','orders_finance.money','orders.orders_number','orders_finance.invoice_status',
            'orders.send_name','orders.send_city_id',
            "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
            'orders.accept_name','orders.accept_city_id','orders_goods.realy_count',
            "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
            'orders_goods.realy_count','orders_goods.realy_pack_count','orders_goods.realy_weight','orders_goods.realy_volume',
            'orders.delivery_method','orders.abnormal_status','customer_bill_info.customer_bill_info_id'
        ])->select();

        //获取赔款总金额
        if($result){
            foreach($result as $key=>$val){
                $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
            }

        }
        return $result;
    }
    //修改
    public function updateOrdersFinance($params){
    	$t = time();
    		
    		
    	if(isset($params['invoice_status'])){
    		$data['invoice_status'] = $params['invoice_status'];
    	}
    
    		
    	Db::startTrans();
    	try {
    		Db::name('orders_finance')->where("finance_id = " . $params['finance_id'])->update($data);
    
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    	}
    	return $result;
    		
    	 
    
    }
}