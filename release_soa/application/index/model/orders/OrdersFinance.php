<?php


namespace app\index\model\orders;


use app\index\model\Base;

use think\Db;
class OrdersFinance extends Base
{
    protected $table="orders_finance";

    
    
    public function sub($params){
    $orderIncome=new OrderIncome();
    foreach ($params['orders_id'] as $k=>$v){
        $this->where('orders_id',$v)->delete();
        $tmp=[];
        $tmp['money']=$orderIncome->where('orders_id',$v)->sum('money');
        $tmp['orders_id']=$v;
        $tmp['create_time']=time();
        $tmp['update_time']=time();
        $tmp['create_user_id']=$params['user_id'];
        $tmp['update_user_id']=$params['user_id'];
        $this->pubInsert($tmp);
    }
return "success";
}

    /**
     * 获取客户财务信息
     * 胡
     */
    public function getOrdersFinance($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 and orders.status=1";

        if(!empty($params['orders_number'])){ //运单编号
            $data.= ' and orders.orders_number like "%'.$params['orders_number'].'%"';
        }

        if(!empty($params['multi_project_id'])){ //项目id
            $data .= " and orders.project_id in (".$params['multi_project_id'].")";
//            $data .= ' and orders.project_id ="'.$params['multi_project_id'].'"';
        }

        if(is_numeric($params['verify_status'])){ //状态
            $data.= " and orders.verify_status = ".$params['verify_status'];
        }

        if(!empty($params['customer_order_number'])){ //客户编号
            $data.= ' and orders.customer_order_number like "%'.$params['customer_order_number'].'%"';
        }

        if(!empty($params['send_goods_name'])){ //发货方
            $data.=' and orders.send_goods_company like "%'.$params['send_goods_name'].'%"';
        }

        if(is_numeric($params['accept_goods_name'])){ //收货方
            $data.=' and orders.accept_goods_company like "%'.$params['accept_goods_name'].'%"';
        }

        if(!empty($params['accept_address'])){ //收货地址
            $data.=' and orders.accept_address like "%'.$params['accept_address'].'%"';
        }

        if(!empty($params['accept_name'])){  //收货人
            $data.=' and orders.accept_name ="'.$params['accept_name'].'"';
        }

        if(!empty($params['accept_cellphone'])){  //收货电话
            $data.=' and orders.accept_cellphone ="'.$params['accept_cellphone'].'"';
        }

        if(is_numeric($params['pay_type'])){ //支付方式
            $data.= " and orders.pay_type = ".$params['pay_type'];
        }

        if(!empty($params['start_order_time'])){
            $data.= " and orders.pickup_time >= ".$params['start_order_time'];
        }
        if(!empty($params['end_order_time'])){
            $data.= " and orders.pickup_time <=".$params['end_order_time'];
        }

        if(is_numeric($params['choose_company_id'])){ //状态
            $data.= " and orders.company_id = ".$params['choose_company_id'];
        }

        if($is_count == true){
            $result = $this->table("orders")->
            join('send_goods','orders.send_goods_id = send_goods.send_goods_id','left')->
            join('accept_goods','orders.accept_goods_id = accept_goods.accept_goods_id','left')->
            where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("orders")->alias('orders')->
                join('send_goods','orders.send_goods_id = send_goods.send_goods_id','left')->
                join('accept_goods','orders.accept_goods_id = accept_goods.accept_goods_id','left')->
                where($data)->limit($page, $page_size)->order('orders.create_time desc')->
                field(['orders.orders_id','orders.orders_id'=>'finance_id','orders.orders_number','orders.pay_type',
                    'orders.send_name','orders.send_city_id','orders.project_id','orders.pickup_time','orders.send_goods_company'=>'send_goods_company','orders.accept_goods_company'=>'accept_goods_company',
                    "(select project_name from project where project_id=orders.project_id)"=> 'project_name',
                    "(select send_goods_company from send_goods where send_goods_id=orders.send_goods_id)"=> 'send_goods_name',
                    "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
                    'orders.money' => 'money2',
                    'orders.accept_name','orders.accept_city_id',
                    "(select accept_goods_company from accept_goods where accept_goods_id=orders.accept_goods_id)"=> 'accept_goods_name',
                    "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
                    'orders.delivery_method','orders.abnormal_status'
                    ])->select();

                //获取赔款总金额
                if($result){
                    foreach($result as $key=>$val){
                        if(!empty($result[$key]['orders_id'])){
                            $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("status = 1 and orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
                        }
                        $goods = $this->table("orders_goods")
                            ->join('goods','orders_goods.goods_id = goods.goods_id','left')
                            ->where("orders_id = ".$result[$key]['orders_id'])->select();
						$goods_name='';	
						$realy_count=0;
						$realy_pack_count =0;
						$realy_weight=0;
						$realy_volume=0;	
                        foreach ($goods as $k1=>$v1){
                            if($k1!=count($goods)-1){
                                $goods_name .= $v1['goods_name'].",";
                            }else{
                                $goods_name .= $v1['goods_name'];
                            }
                            $realy_count += $v1['realy_count'];
                            $realy_pack_count += $v1['realy_pack_count'];
                            $realy_weight += $v1['realy_weight'];
                            $realy_volume += $v1['realy_volume'];
                        }
                        $result[$key]["goods_name"] = $goods_name;
                        $result[$key]["realy_count"] = $realy_count; //件数
                        $result[$key]["realy_pack_count"] = $realy_pack_count; //数量
                        $result[$key]["realy_weight"] = number_format($realy_weight, 3, '.', ''); //计费重量
                        $result[$key]["realy_volume"] = number_format($realy_volume, 3, '.', ''); //计费体积

                    }
                }
            }else{
                $result = $this->table("orders")->alias('orders')->
                join('send_goods','orders.send_goods_id = send_goods.send_goods_id','left')->
                join('accept_goods','orders.accept_goods_id = accept_goods.accept_goods_id','left')->
                where($data)->order('orders.create_time desc')->
                field(['orders.orders_id','orders.orders_id'=>'finance_id','orders.orders_number','orders.pay_type',
                    'orders.send_name','orders.send_city_id','orders.project_id','orders.pickup_time','orders.send_goods_company'=>'send_goods_company','orders.accept_goods_company'=>'accept_goods_company',
                    "(select project_name from project where project_id=orders.project_id)"=> 'project_name',
                    "(select send_goods_company from send_goods where send_goods_id=orders.send_goods_id)"=> 'send_goods_name',
                    "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
                    'orders.money' => 'money2',
                    'orders.accept_name','orders.accept_city_id',
                    "(select accept_goods_company from accept_goods where accept_goods_id=orders.accept_goods_id)"=> 'accept_goods_name',
                    "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
                    'orders.delivery_method','orders.abnormal_status'
                ])->select();

                //获取赔款总金额
                if($result){
                    foreach($result as $key=>$val){
                        if(!empty($result[$key]['orders_id'])){
                            $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("status = 1 and orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
                        }
                        $goods = $this->table("orders_goods")
                            ->join('goods','orders_goods.goods_id = goods.goods_id','left')
                            ->where("orders_id = ".$result[$key]['orders_id'])->select();
						$goods_name='';	
						$realy_count=0;
						$realy_pack_count =0;
						$realy_weight=0;
						$realy_volume=0;
                        foreach ($goods as $k1=>$v1){
                            if($k1!=count($goods)-1){
                                $goods_name .= $v1['goods_name'].",";
                            }else{
                                $goods_name .= $v1['goods_name'];
                            }
                            $realy_count += $v1['realy_count'];
                            $realy_pack_count += $v1['realy_pack_count'];
                            $realy_weight += $v1['realy_weight'];
                            $realy_volume += $v1['realy_volume'];
                        }
                        $result[$key]["goods_name"] = $goods;
                        $result[$key]["realy_count"] = $realy_count; //件数
                        $result[$key]["realy_pack_count"] = $realy_pack_count; //数量
                        $result[$key]["realy_weight"] = number_format($realy_weight, 3, '.', ''); //计费重量
                        $result[$key]["realy_volume"] = number_format($realy_volume, 3, '.', ''); //计费体积

                    }
                 }
            }
        }
        return  $result;
    }

    public function getCustomerBillInData($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $str = "";
        foreach($params as $key=>$val){
            if($key!=count($params)){
                $str.=$val['finance_id'].",";
            }
        }
        $data = "1=1 and orders.orders_id in (".substr($str,0,strlen($str)-1).")";

        if($is_count==true){
            $result = $this->table("orders")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("orders")->alias('orders')->
                where($data)->limit($page, $page_size)->order('orders.create_time desc')->
                field(['orders.orders_id','orders.orders_id'=>'finance_id','orders.money','orders.orders_number',
                    'orders.send_name','orders.send_city_id','orders.project_id',
                    "(select project_name from project where project_id=orders.project_id)"=> 'project_name',
                    "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
                    'orders.accept_name','orders.accept_city_id',
                    "(select send_goods_company from send_goods where send_goods_id=orders.send_goods_id)"=> 'send_goods_name',
                    'orders.money' => 'money2',
                    "(select accept_goods_company from accept_goods where accept_goods_id=orders.accept_goods_id)"=> 'accept_goods_name',
                    "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
                    "(select tax_rate from project where project_id=orders.project_id)"=> 'tax_rate',
                    "(select tax_type from project where project_id=orders.project_id)"=> 'tax_type',
                    'orders.delivery_method','orders.abnormal_status'
                ])->select();

                //获取赔款总金额
                if($result){
                    foreach($result as $key=>$val){
                        $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("status = 1 and orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
                    }
                    $goods = $this->table("orders_goods")
                        ->join('goods','orders_goods.goods_id = goods.goods_id','left')
                        ->where("orders_id = ".$result[$key]['orders_id'])->select();
						$goods_name='';	
						$realy_count=0;
						$realy_pack_count =0;
						$realy_weight=0;
						$realy_volume=0;						
                    foreach ($goods as $k1=>$v1){
                        if($k1!=count($goods)-1){
                            $goods_name .= $v1['goods_name'].",";
                        }else{
                            $goods_name .= $v1['goods_name'];
                        }
                        $realy_count += $v1['realy_count'];
                        $realy_pack_count += $v1['realy_pack_count'];
                        $realy_weight += $v1['realy_weight'];
                        $realy_volume += $v1['realy_volume'];
                    }
                    $result[$key]["goods_name"] = $goods_name;
                    $result[$key]["realy_count"] = $realy_count; //件数
                    $result[$key]["realy_pack_count"] = $realy_pack_count; //数量
                    $result[$key]["realy_weight"] = number_format($realy_weight, 3, '.', ''); //计费重量
                    $result[$key]["realy_volume"] = number_format($realy_volume, 3, '.', ''); //计费体积
                }
            }else{
                $result = $this->table("orders")->alias('orders')->
                where($data)->order('orders.create_time desc')->
                field(['orders.orders_id','orders.orders_id'=>'finance_id','orders.money','orders.orders_number',
                    'orders.send_name','orders.send_city_id','orders.project_id',
                    "(select project_name from project where project_id=orders.project_id)"=> 'project_name',
                    "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
                    'orders.accept_name','orders.accept_city_id',
                    "(select send_goods_company from send_goods where send_goods_id=orders.send_goods_id)"=> 'send_goods_name',
                    'orders.money' => 'money2',
                    "(select accept_goods_company from accept_goods where accept_goods_id=orders.accept_goods_id)"=> 'accept_goods_name',
                    "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
                    "(select tax_rate from project where project_id=orders.project_id)"=> 'tax_rate',
                    "(select tax_type from project where project_id=orders.project_id)"=> 'tax_type',
                    'orders.delivery_method','orders.abnormal_status'
                ])->select();

                //获取赔款总金额
                if($result){
                    foreach($result as $key=>$val){
                        $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("status = 1 and orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');
                  
						$goods = $this->table("orders_goods")
							->join('goods','orders_goods.goods_id = goods.goods_id','left')
							->where("orders_id = ".$result[$key]['orders_id'])->select();
						$goods_name='';	
						$realy_count=0;
						$realy_pack_count =0;
						$realy_weight=0;
						$realy_volume=0;
						foreach ($goods as $k1=>$v1){
							if($k1!=count($goods)-1){
								$goods_name .= $v1['goods_name'].",";
							}else{
								$goods_name .= $v1['goods_name'];
							}
							$realy_count += $v1['realy_count'];
							$realy_pack_count += $v1['realy_pack_count'];
							$realy_weight += $v1['realy_weight'];
							$realy_volume += $v1['realy_volume'];
						}
						$result[$key]["goods_name"] = $goods_name;
						$result[$key]["realy_count"] = $realy_count; //件数
						$result[$key]["realy_pack_count"] = $realy_pack_count; //数量
						$result[$key]["realy_weight"] = number_format($realy_weight, 3, '.', ''); //计费重量
						$result[$key]["realy_volume"] = number_format($realy_volume, 3, '.', ''); //计费体积
					}
                }
            }
        }
        return $result;
    }

    public function getCustomerYesBillInData($params){

        $data = "1=1 ";

        if(!empty($params['customer_bill_number'])){ //备注
            $data.=' and customer_bill.customer_bill_number ="'.$params['customer_bill_number'].'"';
        }

        $result = $this->table("orders")->alias('orders')->
//        join('orders_goods','orders.orders_id = orders_goods.orders_id','left')->
        join('customer_bill_info','customer_bill_info.orders_id = orders.orders_id','left')->
        join('customer_bill','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
        where($data)->order('orders.create_time desc')->
        field(['orders.orders_id','orders.orders_id'=>'finance_id','orders.money','orders.orders_number',
            'orders.send_name','orders.send_city_id','orders.verify_status','orders.pickup_time',
            "(select project_name from project where project_id=customer_bill.project_id)"=> 'project_name',
            "(select send_goods_company from send_goods where send_goods_id=orders.send_goods_id)"=> 'send_goods_name',
            "(select accept_goods_company from accept_goods where accept_goods_id=orders.accept_goods_id)"=> 'accept_goods_name',
            "(select city_name from city where city_id=orders.send_city_id)"=> 'send_city_name',
            'orders.accept_name','orders.accept_city_id',
            "(select city_name from city where city_id=orders.accept_city_id)"=> 'accept_city_name',
            'orders.delivery_method','orders.abnormal_status','customer_bill_info.customer_bill_info_id'
        ])->select();

        //获取赔款总金额
        if($result){
            foreach($result as $key=>$val){
                $result[$key]["abnormal_money"] = number_format($this->table("orders_abnormal")->where("orders_id = ".$result[$key]['orders_id'])->sum("abnormal_money"), 3, '.', '');

                $goods = $this->table("orders_goods")
                    ->join('goods','orders_goods.goods_id = goods.goods_id','left')
                    ->where("orders_id = ".$result[$key]['orders_id'])->select();
                $goods_name='';
                $realy_count=0;
                $realy_pack_count =0;
                $realy_weight=0;
                $realy_volume=0;
                foreach ($goods as $k1=>$v1){
                    if($k1!=count($goods)-1){
                        $goods_name .= $v1['goods_name'].",";
                    }else{
                        $goods_name .= $v1['goods_name'];
                    }
                    $realy_count += $v1['realy_count'];
                    $realy_pack_count += $v1['realy_pack_count'];
                    $realy_weight += $v1['realy_weight'];
                    $realy_volume += $v1['realy_volume'];
                }
                $result[$key]["goods_name"] = $goods_name;
                $result[$key]["realy_count"] = $realy_count; //件数
                $result[$key]["realy_pack_count"] = $realy_pack_count; //数量
                $result[$key]["realy_weight"] = number_format($realy_weight, 3, '.', ''); //计费重量
                $result[$key]["realy_volume"] = number_format($realy_volume, 3, '.', ''); //计费体积
            }
        }
        return $result;
    }

    //修改
    public function updateOrdersFinance($params){
    	$t = time();
    		
    		
    	if(isset($params['invoice_status'])){
    		$data['invoice_status'] = $params['invoice_status'];
    	}
    
    		
    	Db::startTrans();
    	try {
    		Db::name('orders_finance')->where("finance_id = " . $params['finance_id'])->update($data);
    
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    	}
    	return $result;
    		
    	 
    
    }
}