<?php


namespace app\index\model\orders;


use app\common\help\Contents;
use app\index\controller\Shipment;
use app\index\model\Base;
use app\index\model\shipment\ShipmentAbnormal;
use app\index\model\shipment\ShipmentGoods;
use app\index\model\shipment\ShipmentShortBarge;
use app\index\model\source\AcceptGoods;
use app\index\model\source\Goods;
use app\index\model\source\SendGoods;
use app\index\model\source\Supplier;
use Cassandra\Date;
use think\Db;
use think\db\Where;
use think\Exception;
use think\Model;

class OrdersAbnormal extends Base
{

    protected $table = "orders_abnormal";


    public function add($params)
    {
        if (isset($params['orders_id'])) {
            $data = Orders::where('orders_id', $params['orders_id'])->find();
            $data1['abnormal_count'] = $params['abnormal_count'];
            $data1['supplier_abnormal_money'] = is_numeric($params['supplier_abnormal_money']) ?$params['supplier_abnormal_money']:null;
            $data1['responsible_party'] = $params['responsible_party'];
            $data1['damaged'] = $params['damaged'];
            $data1['indemnity_type'] = $params['indemnity_type'];
            $data1['return_goods_type'] = $params['return_goods_type'];
            $data1['indemnity_event_description'] = $params['indemnity_event_description'];
            $data1['remark'] = $params['remark'];
            $data1['abnormal_money'] = is_numeric($params['abnormal_money'])?$params['abnormal_money']:null;
            $data1['remark'] = $params['remark'];
            $data1['abnormal_type_id'] = $params['abnormal_type_id'];
            $data1['solve_way'] = isset($params['solve_way']) ? $params['solve_way'] : "1";
            $data1['orders_id'] = $data['orders_id'];
            $data1['orders_number'] = $data['orders_number'];

            $data1['customer_order_number'] = $data['customer_order_number'];
            $data1['customer_order_number'] = $data['customer_order_number'];

            $data1['transportation_type'] = $data['transportation_type'];
            $data1['accept_name'] = $data['accept_name'];
            $data1['accept_address'] = $data['accept_address'];
            $data1['accept_cellphone'] = $data['accept_cellphone'];
            $data1['insurance_goods'] = $data['insurance_goods'];
            $data1['pickup_time'] = $data['pickup_time'];
            $data1['send_time'] = $data['send_time'];
            $data1['abnormal_status'] = 0;
            $data1['create_user_id'] = $params['user_id'];
            $data1['create_time'] = time();
            $data1['update_user_id'] = $params['user_id'];
            $data1['update_time'] = time();
            $data1['status'] = 1;
            $data1['pickup_time'] = $data['pickup_time'];

            $project_info = Db::name('project')->where('project_id', $data['project_id'])->find();


            $data1['project_name'] = $project_info['project_name'];
            $data1['project_id'] = $project_info['project_id'];
            $transport_info = Db::name("transport")->where('order_number', $data['orders_number'])->find();
            $data1['transport_number'] = $transport_info['transport_number'];
            $goods_info = Ordersgoods::where('orders_id', $data['orders_id'])->select();
            $goods_name = [];
            foreach ($goods_info as $k => $v) {
                $goods_name[] = $v['goods_name'];
            }

            $data1['goods_name'] = implode(',', $goods_name);
            if (isset($params['abnormal_id'])) {
                $data2['supplier_abnormal_money'] = $params['supplier_abnormal_money'];
                $data2['abnormal_count'] = $params['abnormal_count'];
                $data2['responsible_party'] = $params['responsible_party'];
                $data2['damaged'] = $params['damaged'];
                $data2['indemnity_type'] = $params['indemnity_type'];
                $data2['return_goods_type'] = $params['return_goods_type'];
                $data2['indemnity_event_description'] = $params['indemnity_event_description'];

                $data2['remark'] = $params['remark'];
                $data2['solve_way'] = $params['solve_way'];
                $data2['abnormal_money'] = $params['abnormal_money'];
                $data2['abnormal_type_id'] = $params['abnormal_type_id'];


                if (isset($params['image'])) {
                    Db::name("orders_abnormal_upload")->where('abnormal_id', $params['abnormal_id'])->delete();
                    foreach ($params['image'] as $k1 => $v1) {
                        Db::name("orders_abnormal_upload")->insert(['abnormal_id' => $params['abnormal_id'], 'url' => $v1, 'create_time' => time()]);
                    }
                }

                $this->where('abnormal_id', $params['abnormal_id'])->update($data2);

                return true;
            } else {


                // return $data1;
                Db:
                $this->startTrans();
                try {
                    $id = $this->strict(false)->insertGetId($data1);
                    $ordersData['abnormal_status'] = 1;
                    Db::name('orders')->where("orders_id = " . $params['orders_id'])->update($ordersData);

                    // $this->allowField(true)->save($data);
                    Db::commit();
                } catch (Exception $e) {
                    // echo json_encode(['code'=>400,'msg'=>$e->getMessage()]);exit();
                    return $e->getMessage();
                    Db::rollback();
                }

                Db::name("orders_abnormal_upload")->where('abnormal_id', $id)->delete();
                if (isset($params['image']))
                    foreach ($params['image'] as $k1 => $v1) {
                        Db::name("orders_abnormal_upload")->insert(['abnormal_id' => $id, 'url' => $v1, 'create_time' => time()]);
                    }


                $times = $this->whereTime("create_time", 'month')->count() + 1;
                $times = sprintf('%04d', $times);

                $thism = $this->get($id);
                $thism->abnormal_number = 'PK' . date('Ym', time()) . $times;
                $thism->save();

            }
            return true;
        }
    }

    public function getAbnormalReview($params){
        $data=[];
        if(is_numeric($params['abnormal_id']) || isset($params['orders_number'])){
            if(is_numeric($params['abnormal_id']))
            {$data=$this->where('abnormal_id',$params['abnormal_id'])->find();

                $data['abnormal_type_name']  =AbnormalType::get($data->abnormal_type_id)->abnormal_type_name;
            }
            if(isset($params['orders_number']))
                {$data=$this->where('orders_number',$params['orders_number'])->find();

                    $data['abnormal_type_name']  =AbnormalType::get($data->abnormal_type_id)->abnormal_type_name;
                }


        if($data)
            $data['orders_info']=(new Orders())->where('orders_id',$data['orders_id'])->find();
            $data['supplier_info']=(new Supplier)->where('supplier_uuid','in',$data['orders_info']['supplier_uuid'])->select();
            $data['supplier_name'] = array_column($data['supplier_info'],'supplier_name');
            $data['supplier_name']=implode(',',$data['supplier_name']);

            $abnormal_shipment_uuids=(new ShipmentAbnormal())->where('orders_abnormal_number',$data['abnormal_number'])->column('shipment_uuid');



            $data['abnormal_supplier_name']="";
            $data['shipment_info']=Db::name('shipment')->whereIn('orders_number',$data['orders_number'])->select();

            if($abnormal_shipment_uuids) {
                $abnormal_supplier_uuids=Db::name('shipment')->whereIn('shipment_uuid',$abnormal_shipment_uuids)->column('supplier_uuid');


                $abnormal_supplier_name=(new Supplier())->whereIn('supplier_uuid',$abnormal_supplier_uuids)->column('supplier_name');
                $data['abnormal_supplier_name']=implode('/',$abnormal_supplier_name);
            }


            $goods_info=(new Ordersgoods())->where('orders_id',$data['orders_id'])->select();
            array_walk($goods_info,function (&$item){
                $item['goods_base']=Goods::get($item['goods_id']);

            });

            $data['orders_goods']=$goods_info;
        }

return $data;
    }


    public function getAbnormalByid($params)
    {
        $info = $this->get($params['abnormal_id']);
        $upload = Db::name('orders_abnormal_upload')->where('abnormal_id', $params['abnormal_id'])->select();
        $info['upload'] = $upload;
        return $info;

    }

    public function getAbnormalList($params,$is_page=true,$is_count=false)
    {
        $pageSize = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
        $page = isset($params['page']) ? $params['page'] : 1;
        $new_where="1=1 ";

        if(is_numeric($params['handle'])){
            switch ($params['handle'])
            {
                case 1:$new_where.=" and supplier_abnormal_money is null and abnormal_money is null"; break;
                case 2:$new_where.=" and (supplier_abnormal_money >= 0 or abnormal_money >= 0)";break;
                case 3:break;
            }


        }

        if (!empty($params['multi_project_id'])) {
            $new_where.=" and orders_abnormal.project_id in (".$params['multi_project_id'].")";
        }
        if (is_numeric($params['shipment_status'])) {
            $new_where.=" and orders_abnormal.shipment_status=".$params['shipment_status'];
        }		
        if (is_numeric($params['shipment_status_not3'])) {
            $new_where.=" and orders_abnormal.shipment_status<3";
        }			
        if(is_numeric($params['abnormal_money']))
         $new_where.=" and orders_abnormal.abnormal_money = ".$params['abnormal_money'];


        if(!empty($params['multi_abnormal_type_id']))
        { $new_where.=" and orders_abnormal.project_id in (".$params['multi_abnormal_type_id'].")";
        }
        if (!empty($params['pickup_time']))
            $new_where.=" and orders_abnormal.pickup_time>=".strtotime($params['pickup_time']);

        if (!empty($params['pickup_time_end']))
            $new_where.=" and orders_abnormal.pickup_time<=".strtotime($params['pickup_time_end']);
        if (!empty($params['orders_number']))
            $new_where.=" and orders_abnormal.orders_number like '%".$params['orders_number']."%' ";
        if (!empty($params['solve_way']))
            $new_where.=" and orders_abnormal.solve_way = ".$params['solve_way'];
        if (!empty($params['indemnity_type']))
            $new_where.=" and orders_abnormal.indemnity_type = ".$params['indemnity_type'];
        if (!empty($params['abnormal_id']))
            $new_where.=" and orders_abnormal.abnormal_id = ".$params['abnormal_id'];

		if(is_numeric($params['status'])){
			$new_where.=" and orders_abnormal.status = ".$params['status'];
			
		}
		if(is_numeric($params['choose_company_id'])){
			$new_where.=" and orders.company_id = ".$params['choose_company_id'];
			
		}
        if (!empty($params['supplier_uuid'])) {
            $supplier_array = explode(",", $params['supplier_uuid']);
            $supplier_str = "";
            foreach ($supplier_array as $k1 => $v1) {
                $supplier_array[$k1] = "'" . $v1 . "'";
            }
            $supplier_str = implode(',', $supplier_array);

            $new_where .= ' and orders.supplier_uuid in (' . $supplier_str . ')';
        }
		

        $data['count'] = $this->join("orders","orders.orders_id=orders_abnormal.orders_id")->where($new_where)->count();
        $data['page_count'] = ceil($data['count'] / $pageSize);
        if($is_count)
            return  $this->join("orders","orders.orders_id=orders_abnormal.orders_id")->where($new_where)->count();

        if($is_page)

        $data['list'] = $this->alias("orders_abnormal")->where($new_where)->page($page, $pageSize)
            ->field(["orders_abnormal.*"])->
             join("orders","orders.orders_id=orders_abnormal.orders_id",'left')
          //  ->join("shipment","shipment.orders_number=orders_abnormal.orders_number",'left')
            ->order('orders_abnormal.abnormal_id', 'desc')->select();
        else
        $data['list'] = $this->alias("orders_abnormal")->where($new_where)
            ->field(["orders_abnormal.*,orders.company_id"])->
            join("orders","orders.orders_id=orders_abnormal.orders_id",'left')
       //     ->join("shipment","shipment.orders_number=orders_abnormal.orders_number",'left')
            ->order('orders_abnormal.abnormal_id', 'desc')->select();
		
        $accept_goods_model = new AcceptGoods();
        $goodsModel=new Ordersgoods();
        $orders_model=new Orders();



        foreach ($data['list'] as $k => $v) {
            $data['list'][$k]['index']=$k+1;
            $order_info = $orders_model->where('orders_id',$data['list'][$k]['orders_id'])->find();
            $data['list'][$k]['order_info']=$order_info;
            $data['list'][$k]['supplier_name']=(new Supplier())->where('supplier_uuid',$order_info->supplier_uuid)->find()['supplier_name'];


            $data['list'][$k]['shipment_info'] = Db::name("shipment")->where('orders_number', $v['orders_number'])->select();
            if($data['list'][$k]['shipment_info'])
            {   $shipmen_uuids=array_column($data['list'][$k]['shipment_info'],'shipment_uuid');
                $shipment_goods=(new ShipmentGoods())->whereIn('shipment_uuid',$shipmen_uuids)->select();
                $shipment_count=array_sum(array_column($shipment_goods,'shipment_count'));
                $shipment_pack_count=array_sum(array_column($shipment_goods,'shipment_pack_count'));
                $shipment_weight=array_sum(array_column($shipment_goods,'shipment_weight'));
                $shipment_volume=array_sum(array_column($shipment_goods,'shipment_volume'));

            }
            $data['list'][$k]['shipment_count']=$shipment_count?$shipment_count:0;
            $data['list'][$k]['shipment_pack_count']=$shipment_pack_count?$shipment_pack_count:0;
            $data['list'][$k]['shipment_weight']=$shipment_weight?$shipment_weight:0;
            $data['list'][$k]['shipment_volume']=$shipment_volume?$shipment_volume:0;



           // $data['list'][$k]['supplier_abnormal_money']=0;
          //  if($data['list'][$k]['add_type']==0)
          //  $data['list'][$k]['supplier_abnormal_money']=Db::name('shipment')->where(['orders_number'=>$order_info->orders_number,'status'=>1])->sum('shipment_abnormal_money')+(new ShipmentShortBarge())->where(['orders_number'=>$order_info->orders_number,'status'=>1])->sum('short_barge_abnormal_money');
            $data['list'][$k]['goods_info']=$goodsModel->where('orders_id',$data['list'][$k]['orders_id'])->select();
            $accept_goods_info = $accept_goods_model->where('accept_goods_id', $order_info['accept_goods_id'])->find();
            $data['list'][$k]['accept_goods_info'] = $accept_goods_info;
            $data['list'][$k]['upload'] = Db::name("orders_abnormal_upload")->where('abnormal_id', $v['abnormal_id'])->select();
            $abnormal_type_info = Db::name('abnormal_type')->where('abnormal_type_id', $data['list'][$k]['abnormal_type_id'])->find();
            $data['list'][$k]['abnormal_type_name'] = $abnormal_type_info ? $abnormal_type_info['abnormal_type_name'] : "无";

        }

        return $data;
    }
	public function getShipmentNeedOrderAbnormal($params){
		$where='1=1';
		if(!empty($params['abnormal_number'])){
			$where.=" and orders_abnormal.abnormal_number like '".$params['abnormal_number']."'";
		}
		$result = Db::name("orders_abnormal")->where($where)->field([
				"orders_abnormal.*",
				"(select abnormal_type.abnormal_type_name from abnormal_type where orders_abnormal.abnormal_type_id = abnormal_type.abnormal_type_id)"=>'abnormal_type_name'
	
                ])->select();
		for($i=0;$i<count($result);$i++){
			$result[$i]['upload'] = Db::name("orders_abnormal_upload")->where('abnormal_id='.$result[$i]['abnormal_id'])->select();
		}		
				// 
		return $result;		
	}
	
	//获取常规的运单异常
	public function getOrdersAbnormal($params){
		$where="1=1 and orders_abnormal.status =1 ";
        if (!empty($params['orders_number'])){
			
			$where.=" and orders_abnormal.orders_number = '".$params['orders_number']."'";
		}
          
		$result = Db::name("orders_abnormal")->where($where)->field([
				"orders_abnormal.*",
				"(select abnormal_type.abnormal_type_name from abnormal_type where orders_abnormal.abnormal_type_id = abnormal_type.abnormal_type_id)"=>'abnormal_type_name'
                ])->select();
				

		return $result;
		
	}
	//驾驶舱
	public function getIndexOrderAbnormal($params){
		$data = '1=1 and orders.status =1';
		if(is_numeric($params['month'])){
			$data.= " and FROM_UNIXTIME(orders.pickup_time,'%Y%m') = FROM_UNIXTIME(".$params['month'].",'%Y%m')";
	
		}
		if(is_numeric($params['year'])){
			$data.= " and FROM_UNIXTIME(orders.pickup_time,'%Y') = FROM_UNIXTIME(".$params['year'].",'%Y')";
	
		}		
		$result =  $this->table('orders_abnormal')->join("orders",'orders.orders_id = orders_abnormal.orders_id and orders.status = 1')->where("orders_abnormal.status =1 ")->field([
				"sum(abnormal_money) as sum_orders_abnormal_money",
		
                ])->select();
	
		return $result;
		
	}
}