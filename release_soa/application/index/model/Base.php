<?php


namespace app\index\model;

use think\Db;
use think\Exception;
use think\Model;
use app\common\help\Contents;
use think\Request;


class Base extends Model
{

    public function user()
    {
        return $this->hasOne('app\index\model\system\user', 'user_id', 'user_id')->field("username,user_id,phone");
    }

    public function project()
    {
        return $this->hasOne('\app\index\model\source\Project', 'project_id', 'project_id')->field("project_id,project_name");
    }

    public function parseWhere($where)
    {
        $data = " ";
        foreach ($where as $k => $v) {
            if (is_array($v)) {
                switch ($v[0]) {
                    case 'in':
                        $data .= " and " . $k . " in " . "(" . $v[1] . ")";
                        break;

                    default:
                        $data .= " and " . $k . " " . $v[0] . " " . $v[1];
                }

            } else {
                $data .= " and $k =$v";
            }

        }
        return $data;
    }

    public function pubInsert($data)
    {
        $id = 0;
        Db::startTrans();
        try {
            $id = $this->insertGetId($data);

            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

            \think\Response::create(['code' => '400', 'msg' => $result], 'json')->send();
            exit();

        }
        return $id;
    }

    public function pubInsertAll($data)
    {

        Db::startTrans();
        try {

            $this->insertAll($data);
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

            \think\Response::create(['code' => '400', 'msg' => $result], 'json')->send();
            exit();

        }
        return 1;
    }

    public function pubGetData($where = [], $is_page = true,$is_count=false, $order = null, $with = null)
    {
        $contents = file_get_contents("php://input");

        $params = json_decode($contents, TRUE);

        if ($is_count == true) {
            $result = $this->table("accept_goods")->where($where)->
            join("project", 'project.project_id = accept_goods.project_id')->count();
            return $result;
        }

        if ($is_page) {
            $page = isset($params['page']) ? $params['page'] : 1;
            $pageSize = isset($params['$pageSize']) ? $params['$pageSize'] : Contents::PAGE_SIZE;
            $data['count'] = $this->where($where)->count();
            $data['page_count'] = ceil($data['count'] / $pageSize);
            if ($with)
                $this->with($with);
            $tinfo = $this->where($where)->page($page, $pageSize)->order($order)->select();

        } else
            $tinfo = $this->where($where)->order($order)->select();
        $data['list'] = $tinfo;
        return $data;
    }


}