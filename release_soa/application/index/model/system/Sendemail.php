<?php

namespace app\index\model\system;

use think\Model;
use app\common\help\Help;
use think\config;
use think\Db;
class Sendemail extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'send_email';



    /**
     * 添加
     * 胡
     */
    public function addSendEmail($params){

        $t = time();
       
        $data['send_email'] = $params['send_email'];
		$data['send_name'] = $params['send_name'];
		$data['send_title'] = $params['send_title'];
		$data['send_content'] = $params['send_content'];
		$data['status'] = 1;
        $data['create_time'] = $t;
  

        Db::startTrans();
        try{
            $id = Db::name('send_email')->insertGetId($data);


            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
	
        return $result;
    }

    /**
     * 获取费用
     * 胡
     */
    public function getSendEmail($params){//第一个为参数，第二个为是否要获取 总数

 
    	$data = "1=1";

    	if(is_numeric($params['status'])){
    		$data.= " and status = ".$params['status'];
    	}



 
	    $result= $this->table("send_email")->alias("send_email")->

	        where($data)->field(["send_email.*"])->order("send_email.create_time asc")->
	            select();
    			 
    		
 

        return $result;

    }


    /**
     * 修改公司 根据company_id
     */
    public function updateSendemail($params){

        $t = time();
        
      
        $data['status'] = 2;
      

      
        $data['send_time'] = $t;
		
        Db::startTrans();
        try{
            Db::name('send_email')->where("send_email_id = ".$params['send_email_id'])->update($data);


            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;
    }

    /**
     * getOneCompany
     *
     * 获取一条公司信息
     * @author shj
     *
     * @param $company_id
     *
     * @return void
     * Date: 2019/2/28
     * Time: 10:59
     */
    public function getOneCompany($company_id){
        $result = $this->table("company")->where(['company_id' => $company_id])->find();
        return $result;
    }
}