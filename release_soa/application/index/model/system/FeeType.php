<?php


namespace app\index\model\system;


use app\common\help\Contents;
use app\common\help\Help;
use app\index\model\Base;
use app\index\model\source\Project;
use think\Db;
use think\Model;

class FeeType extends Base
{

    protected $table = "cost";
    //新增费用
    public function addFeeType($params){

        $params['cost_uuid'] = Help::getUuid(); //获取uuid
        $params['create_time'] = time();
        $params['create_user_id'] = $params['user_id'];
        $params['update_time'] = time();
        $params['update_user_id'] = $params['user_id'];
        $params['status'] = intval($params['status']);
        $params['update_time'] = time();
        $params['create_user_id'] = $params['user_id'];
        $params['update_user_id'] = $params['user_id'];

        Db::startTrans();
        try{
            $pk_id = $this->allowField(true)->save($params);
        	$result = 1;
            // 提交事务
            Db::commit();
        
        } catch (\Exception $e) {
        	$result = $e->getMessage();
        	// 回滚事务
        	Db::rollback();
        	//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
        	//exit();
        
        }

        //$result=$this->allowField(true)->save($params);
        return $result;
    }
    //更新税率
    public function updateFeeType($params){
        $params['create_time'] = time();
//        $params['project_id'] = $params['project_id'];
        $params['cost_name'] = $params['cost_name'];
        $params['cost_type'] = intval($params['cost_type']);
        $params['status'] = intval($params['status']);
        $params['remark'] = isset($params['remark'])?$params['remark']:"";
        $info=$this->where('cost_id',$params['cost_id'])->find();
		
		
		Db::startTrans();
        try {
        $result=$info->allowField(true)->save($params);
		 Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
			//return $result;
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }
        return $result;
    }

    public function getFeeType($params)
    {

        $pageSize = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
        $page = isset($params['page']) ? $params['page'] : 1;
        $where=[];
        if(isset($params['status']))
        {
            $where['status']=$params['status'];
        }if(isset($params['cost_type']))
        {
            $where['cost_type']=['=',$params['cost_type']];
        }
        if(isset($params['cost_id']))
        {
            $where['cost_id']=$params['cost_id'];
            $data['project']=(new Project())->field('project_name,project_id')->select();
        }
        if(isset($params['cost_name']))
        {
            $where['cost_name']=$params['cost_name'];
        }

        $data['count'] = $this->where($where)->count();
        $data['page_count'] = ceil($data['count'] / $pageSize);
        $data['list']=$this->where($where)->select();
        /*$data['list'] = $this->alias("fee")->field(['fee.*',
            "(select nickname from user where user.user_id = fee.create_user_id)"=> 'create_user_name'
        ])->where($where)->with(['user','project'])->page($page, $pageSize)->order('cost_id', 'desc')->select();
       */ return $data;


    }

}
