<?php

namespace app\index\model\system;
use app\index\model\source\Supplier;
use think\Model;
use app\common\help\Help;
use think\config;
use think\Db;
class Company extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'company';
    private $_languageList;
    public function initialize()
    {
        $this->_languageList = config('systom_setting')['language_list'];
        parent::initialize();

    }

    /**
     * 添加公司
     * 胡
     */
    public function addCompany($params){

        $t = time();
       
        $data['company_name'] = $params['company_name'];
        if(isset($params['linkman'])){
            $data['linkman'] = $params['linkman'];
        }
        if(isset($params['phone'])){
            $data['phone'] = $params['phone'];
        }
        if(isset($params['address'])){
            $data['address'] = $params['address'];
        }

        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = 1;

        Db::startTrans();
        try{
            $id = Db::name('company')->insertGetId($data);


            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
		
        return $result;
    }

    /**
     * 获取公司数据
     * 胡
     */
    public function getCompany($params,$is_count=false){//第一个为参数，第二个为是否要获取 总数

 
    	$data = "1=1";
    	if(isset($params['company_id'])){
    		$data.= " and company.company_id = ".$params['company_id'];
    	}
    	if(is_numeric($params['status'])){
    		$data.= " and company.status = ".$params['status'];
    	}
    	if(isset($params['company_name'])){
    		$data.= " and company.company_name like'%".$params['company_name']."%'";
    	}

    	if(isset($params['linkman'])){
    		$data.= " and company.linkman = '".$params['linkman']."'";
    	}   
    	if(isset($params['phone'])){
    		$data.= " and company.phone = '".$params['phone']."'";
    	}
    
    	if($is_count==true){
    		$result = $this->where($data)->count();
    		 
    	}else{
            if(isset($params['page'])){
                $result= $this->table("company")->alias("company")->

                where($data)->order('create_time desc')->limit($params['page'],$params['page_size'])->
                field(["company.*"])
                    ->order("company.create_time desc")->

                    select();

    		}else{
	            $result= $this->table("company")->alias("company")->

	            where($data)->
	            field(["company.*"])->order("company.create_time desc")->
	            select();
    			 
    		}
    		 
    	}

        return $result;

    }

	/**
	 * 获取公司根据公司多个ID
	 */
    public function getCompanyByCompanysId($params){
    	$data['company_id'] = array('in',$params['use_company_id']);
    	$result = $this->where($data)->field(['company.company_id','company.company_name','company.phone','company.linkman','company.country_id','company.timezone','company.language_id','company.currency_id','company.create_user_id','company.create_time','company.update_user_id','company.update_time','company.status','company.is_supplier'])
            ->select();
   
    	return $result;
    	
    	
    	
    }
    /**
     * 修改公司 根据company_id
     */
    public function updateCompanyByCompanyId($params){

        $t = time();
        
        if(!empty($params['company_name'])){
        	$data['company_name'] = $params['company_name'];
        }
        if(isset($params['linkman'])){
            $data['linkman'] = $params['linkman'];
        }
        if(isset($params['phone'])){
            $data['phone'] = $params['phone'];
        }

        if(is_numeric($params['status'])){
            $data['status'] = $params['status'];
        }

        if(isset($params['address'])){
        	$data['address'] = $params['address'];
        }


        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
		
        Db::startTrans();
        try{
            Db::name('company')->where("company_id = ".$params['company_id'])->update($data);


            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;
    }

    /**
     * getOneCompany
     *
     * 获取一条公司信息
     * @author shj
     *
     * @param $company_id
     *
     * @return void
     * Date: 2019/2/28
     * Time: 10:59
     */
    public function getOneCompany($company_id){
        $result = $this->table("company")->where(['company_id' => $company_id])->find();
        return $result;
    }
}