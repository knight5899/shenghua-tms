<?php

namespace app\index\model\bill;

use app\index\model\bill\CustomerBillInfo;
use think\Db;
use think\Model;
use app\common\help\Help;
use app\index\model\orders\Orders;
use app\index\model\orders\OrdersFinance;
use app\index\model\system\Sendemail;
class CustomerBill extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'customer_bill';

    /**
     * 获取客户账单
     * 韩
     */
    public function getCustomerBillOnly($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(is_numeric($params['customer_bill_status'])){ //1对账 2开票 3驳回
            $data.= " and customer_bill.customer_bill_status = ".$params['customer_bill_status'];
        }

        if(is_numeric($params['status'])){ //状态 0禁用 1启用
            $data.= " and customer_bill.status = ".$params['status'];
        }

        if(is_numeric($params['customer_bill_id'])) {//客户账单ID
            $data.= " and customer_bill.customer_bill_id = ".$params['customer_bill_id'];
        }

        if(is_numeric($params['project_id'])){ //项目ID
            $data.= " and customer_bill.project_id = ".$params['project_id'];
        }

        if(!empty($params['customer_bill_number'])){ //客户账单编号
            $data.=' and customer_bill.customer_bill_number ="'.$params['customer_bill_number'].'"';
        }
        if(!empty($params['customer_bill_date'])){
            $data.= " and FROM_UNIXTIME(customer_bill.customer_bill_date,'%Y-%m')  ='".$params['customer_bill_date']."'";
        }
        if($is_count==true){
            $result = $this->table("customer_bill")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("customer_bill")->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(["customer_bill_id,customer_bill_number,customer_bill_name","project_id",
                    'customer_bill_tax','customer_invoice_status','customer_invoice_name',
                    "(select project_name from project where project_id=customer_bill.project_id)"=> 'project_name',
                    "money","abnormal_money","remark","customer_bill_status","back_remark","invoice_type",
                    "invoice_fax","fax_money","invoice_money","real_money","create_invoice_user_id",
                    "(select nickname from user where user.user_id = customer_bill.create_invoice_user_id)"=> 'create_invoice_user_name',
                    "(select nickname from user where user.user_id = customer_bill.back_user_id)"=> 'back_user_name',
                    'back_time','customer_bill.customer_bill_tax_type','customer_bill.customer_bill_money',
                    "create_invoice_time","create_time","create_user_id",
                    "(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "update_time","update_user_id",
                    "(select nickname from user where user.user_id = customer_bill.update_user_id)"=> 'update_user_name',
                    "status"
                ])->select();

                if(count($result)>0){
                    foreach($result as $key=>$val){
                        $result[$key]['info'] = $this->table("customer_bill_info")->
                        join('orders','orders.orders_id = customer_bill_info.orders_id','left')->
                        join('orders_finance','orders.orders_id = orders_finance.orders_id','left')->
                        where("customer_bill_id = ".$result[$key]['customer_bill_id'])->field("customer_bill_info.*,orders.money,orders.orders_number")->select();
                        $info = $result[$key]['info'];
                        for($i=0;$i<count($info);$i++){
                            $info[$i]['abnormal_info'] =  $this->table("orders_abnormal")->where("orders_id = ".$info[$i]['orders_id'])->select();
                        }
//                         foreach($result[$key]['info'] as $kk1=>$vv1){
//                             $result[$key]['info'][$kk1]['abnormal_info'] = $this->table("orders_abnormal")->where("orders_id = ".$vv1['orders_id'])->select();
//                         }
                    }
                }

            }else{
                $result = $this->table("customer_bill")->
                where($data)->order('create_time desc')->
                field(["customer_bill_id,customer_bill_number,customer_bill_name","project_id",
                    'customer_bill_tax','customer_invoice_status','customer_invoice_name',
                    "(select project_name from project where project_id=customer_bill.project_id)"=> 'project_name',
                    "money","abnormal_money","remark","customer_bill_status","back_remark","invoice_type",
                    "invoice_fax","fax_money","invoice_money","real_money","create_invoice_user_id",
                    "(select nickname from user where user.user_id = customer_bill.create_invoice_user_id)"=> 'create_invoice_user_name',
                    "(select nickname from user where user.user_id = customer_bill.back_user_id)"=> 'back_user_name',
                    'back_time','customer_bill.customer_bill_tax_type','customer_bill.customer_bill_money',
                    "create_invoice_time","create_time","create_user_id",
                    "(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "update_time","update_user_id",
                    "(select nickname from user where user.user_id = customer_bill.update_user_id)"=> 'update_user_name',
                    "status"
                ])->select();

                if(count($result)){

                    foreach($result as $key=>$val){

                        $result[$key]['info'] = $this->table("customer_bill_info")->
                        join('orders','orders.orders_id = customer_bill_info.orders_id','left')->
                        join('orders_finance','orders.orders_id = orders_finance.orders_id','left')->
                        where("customer_bill_id = ".$result[$key]['customer_bill_id'])->field("customer_bill_info.*,orders.money,orders.orders_number")->select();

                        $info = $result[$key]['info'];

                        for($i=0;$i<count($info);$i++){

                            $info[$i]['abnormal_info'] =  $this->table("orders_abnormal")->where("orders_id = ".$info[$i]['orders_id'])->select();


                        }

                    }
                }

            }
        }
        return $result;
    }

    /**
     * 获取客户账单
     * 韩
     */
    public function getCustomerBill($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 and customer_bill_status !=3";

        if(is_numeric($params['customer_bill_status'])){ //1对账 2开票 3驳回
            $data.= " and customer_bill.customer_bill_status = ".$params['customer_bill_status'];
        }

        if(is_numeric($params['status'])){ //状态 0禁用 1启用
            $data.= " and customer_bill.status = ".$params['status'];
        }

        if(is_numeric($params['customer_bill_id'])) {//客户账单ID
            $data.= " and customer_bill.customer_bill_id = ".$params['customer_bill_id'];
        }
        if(is_numeric($params['customer_invoice_status'])) {
            $data.= " and customer_bill.customer_invoice_status = ".$params['customer_invoice_status'];
        }
        if(is_numeric($params['project_id'])){ //项目ID
            $data.= " and customer_bill.project_id = ".$params['project_id'];
        }

        if(!empty($params['customer_bill_number'])){ //客户账单编号
            $data.=' and customer_bill.customer_bill_number like "%'.$params['customer_bill_number'].'%"';
        }
        if(!empty($params['customer_bill_date'])){
    		$data.= " and FROM_UNIXTIME(customer_bill.customer_bill_date,'%Y-%m')  ='".$params['customer_bill_date']."'";
    	}	 

		
        if($is_count==true){
            $result = $this->table("customer_bill")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("customer_bill")->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(["customer_bill_id,customer_bill_number,customer_bill_name","project_id",'create_over_time',
					'customer_bill_tax','customer_invoice_status','customer_invoice_name',
                    "(select project_name from project where project_id=customer_bill.project_id)"=> 'project_name',
                    "money","abnormal_money","remark","customer_bill_status","back_remark","invoice_type",
                    "invoice_fax","fax_money","invoice_money","real_money","create_invoice_user_id",
                    "(select nickname from user where user.user_id = customer_bill.create_invoice_user_id)"=> 'create_invoice_user_name',
					 "(select nickname from user where user.user_id = customer_bill.create_over_user_id)"=> 'create_over_user_name',
                	"(select nickname from user where user.user_id = customer_bill.back_user_id)"=> 'back_user_name',
                	'back_time','customer_bill.customer_bill_tax_type','customer_bill.pay_type','customer_bill.customer_bill_money',
                    "create_invoice_time","create_time","create_user_id",
                    "(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "update_time","update_user_id",
                    "(select nickname from user where user.user_id = customer_bill.update_user_id)"=> 'update_user_name',
                    "status"
                ])->select();

                if(count($result)>0){
                    foreach($result as $key=>$val){
                        $result[$key]['info'] = $this->table("customer_bill_info")->
                        join('orders','orders.orders_id = customer_bill_info.orders_id','left')->
                        join('orders_finance','orders.orders_id = orders_finance.orders_id','left')->
                        where("customer_bill_id = ".$result[$key]['customer_bill_id'])->field("customer_bill_info.*,orders.money,orders.orders_number")->select();
                        $info = $result[$key]['info'];
						for($i=0;$i<count($info);$i++){
							$info[$i]['abnormal_info'] =  $this->table("orders_abnormal")->where("orders_id = ".$info[$i]['orders_id'])->select();
						}
//                         foreach($result[$key]['info'] as $kk1=>$vv1){
//                             $result[$key]['info'][$kk1]['abnormal_info'] = $this->table("orders_abnormal")->where("orders_id = ".$vv1['orders_id'])->select();
//                         }
                    }
                }

            }else{
                $result = $this->table("customer_bill")->
                where($data)->order('create_time desc')->
                field(["customer_bill_id,customer_bill_number,customer_bill_name","project_id",'create_over_time',
						'customer_bill_tax','customer_invoice_status','customer_invoice_name',
                    "(select project_name from project where project_id=customer_bill.project_id)"=> 'project_name',
                    "money","abnormal_money","remark","customer_bill_status","back_remark","invoice_type",
                    "invoice_fax","fax_money","invoice_money","real_money","create_invoice_user_id",
                    "(select nickname from user where user.user_id = customer_bill.create_invoice_user_id)"=> 'create_invoice_user_name',
                		"(select nickname from user where user.user_id = customer_bill.back_user_id)"=> 'back_user_name',
                		'back_time','customer_bill.customer_bill_tax_type','customer_bill.pay_type','customer_bill.customer_bill_money',
                		"create_invoice_time","create_time","create_user_id",
                    "(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "update_time","update_user_id",
                    "(select nickname from user where user.user_id = customer_bill.update_user_id)"=> 'update_user_name',
                    "status"
                ])->select();

                if(count($result)){

                    foreach($result as $key=>$val){

                        $result[$key]['info'] = $this->table("customer_bill_info")->
                        join('orders','orders.orders_id = customer_bill_info.orders_id','left')->
                        join('orders_finance','orders.orders_id = orders_finance.orders_id','left')->
                        where("customer_bill_id = ".$result[$key]['customer_bill_id'])->field("customer_bill_info.*,orders.money,orders.orders_number")->select();

                        $info = $result[$key]['info'];

                        for($i=0;$i<count($info);$i++){

							$info[$i]['abnormal_info'] =  $this->table("orders_abnormal")->where("orders_id = ".$info[$i]['orders_id'])->select();


                        }

                    }
                }

            }
        }
        return $result;
    }

    public function getCustomerBillInfoOnlly($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

    }

    /**
     * 添加客户账单
     * 韩
     */
    public function addCustomerBill($params){
//        return $params;exit;
        if($params){
            foreach($params as $key=>$val) {
                if ($key == 0) {
                    //插入主表
                    $dayCountParams = " FROM_UNIXTIME(customer_bill_date,'%Y%m') =".date('Ym',strtotime($val[8]));
                    $dayCount = Db::name('customer_bill')->where($dayCountParams)->count();
                    $data['customer_bill_number'] = "KD".date('Ym',strtotime($val[8])).str_pad(($dayCount+1),3,0,STR_PAD_LEFT);

//                    $data['customer_bill_number'] = $val[0];
                    $data['project_id'] = $val[1];
                    $data['customer_bill_name'] = $val[2];
                    $data['create_user_id'] = $val[3];
                    $data['money'] = $val[4];
                    $data['abnormal_money'] = $val[5];
                    $data['remark'] = $val[7];
                    $data['customer_bill_date'] = strtotime($val[8]);
                    $data['customer_bill_tax'] = $val[9];
                    $data['customer_bill_tax_type'] = $val[10];
                    $data['pay_type'] = $val[11];
                    $data['company_id'] = $val[12];
                    $data['customer_bill_money'] = $val[13];
                    $data['customer_bill_type'] = $val[14];
                    $data['create_user_id'] = $val[3];
                    $data['create_time'] = strtotime($val[6]);
                    $data['update_user_id'] = $val[3];
                    $data['update_time'] = strtotime($val[6]);
                    $data['customer_bill_status'] = 1;

                    Db::startTrans();
                    try{
                        $info = $this->table("customer_bill")->insertGetId($data);
                        $result = 1;
                        // 提交事务
                        Db::commit();
                    }catch (\Exception $e) {
                        $result = $e->getMessage();
                        // 回滚事务
                        Db::rollback();
                    }

                }else{
                    //插入详情表

                    $data2[$key-1]['customer_bill_id'] = $info;
                    $data2[$key-1]['orders_finance_id'] = $val[0];
                    $data2[$key-1]['orders_id'] = $val[0];
					$data2[$key-1]['orders_number'] = $val[1];
                    $data2[$key-1]['create_user_id'] = $data['create_user_id'];
                    $data2[$key-1]['create_time'] = $data['create_time'];
                    $data2[$key-1]['update_user_id'] = $data['update_user_id'];
                    $data2[$key-1]['update_time'] = $data['update_time'];
                    $data2[$key-1]['status'] = 1;
                }
            }
        }
        if($result==1){
            Db::startTrans();
            try{
                $asd = $this->table("customer_bill_info")->insertAll($data2);

                //修改财务表状态
                foreach($data2 as $kk=>$vv){
                    $data4['verify_status'] = 3;
                    $this->table("orders")->where('orders_id = "'.$vv['orders_id'].'"')->update($data4);
                }

                $result = 1;
                // 提交事务
                Db::commit();
            }catch (\Exception $e) {
                $result = $e->getMessage();
                // 回滚事务
                Db::rollback();
            }
        }

        return $result;
    }

    /**
     * 获取客户已对账单
     * 韩
     */
    public function getCustomerYesBill2($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(isset($params['customer_bill_number'])){ //对账编号
            $data.= ' and customer_bill.customer_bill_number like "%'.$params['customer_bill_number'].'%"';
        }
//        if(!empty($params['send_name'])){ //发货方
//            $data.=' and customer_bill.send_name ="'.$params['send_name'].'"';
//        }
        if(isset($params['customer_bill_name'])){ //对账名称
            $data.= ' and customer_bill.customer_bill_name like "%'.$params['customer_bill_name'].'%"';
        }

        if(is_numeric($params['choose_company_id'])){ //分公司
            $data.= " and customer_bill.company_id = ".$params['choose_company_id'];
        }

        if(is_numeric($params['customer_bill_status'])){ //状态
            $data.= " and customer_bill.customer_bill_status = ".$params['customer_bill_status'];
        }

        if(is_numeric($params['customer_invoice_status'])){ //状态
            if($params['customer_invoice_status']==4){
                $data.= " and customer_bill.customer_invoice_status in (2,4)";
            }else {
                $data .= " and customer_bill.customer_invoice_status = " . $params['customer_invoice_status'];
            }
        }

        if($is_count==true){
            $result = $this->table("customer_bill")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("customer_bill")->
//                join('customer_bill_info','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['customer_bill.*',"(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "(select company_name from company where company.company_id = customer_bill.company_id)"=> 'company_name'
                ])->select();

                foreach($result as $key=>$val) {
                    $project_name = $this->table("project")->
                    where("project_id = " . $result[$key]['project_id'])->field("project_name")->find();
                    $result[$key]['project_name'] =  $project_name['project_name'];
                }

            }else{
                $result = $this->table("customer_bill")->
//                join('customer_bill_info','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
                where($data)->order('create_time desc')->
                field(['customer_bill.*',"(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "(select company_name from company where company.company_id = customer_bill.company_id)"=> 'company_name'
                ])->select();

                foreach($result as $key=>$val) {
                    $project_name = $this->table("project")->
                    where("project_id = " . $result[$key]['project_id'])->field("project_name")->find();
                    $result[$key]['project_name'] =  $project_name['project_name'];
                }
            }
        }
        return $result;
    }

    /**
     * 获取客户已对账单
     * 韩
     */
    public function getCustomerYesBill3($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(isset($params['customer_bill_number'])){ //对账编号
            $data.= ' and customer_bill.customer_bill_number like "%'.$params['customer_bill_number'].'%"';
        }
//        if(!empty($params['send_name'])){ //发货方
//            $data.=' and customer_bill.send_name ="'.$params['send_name'].'"';
//        }
        if(isset($params['customer_bill_name'])){ //对账名称
            $data.= ' and customer_bill.customer_bill_name like "%'.$params['customer_bill_name'].'%"';
        }

        if(is_numeric($params['choose_company_id'])){ //分公司
            $data.= " and customer_bill.company_id = ".$params['choose_company_id'];
        }

        if(is_numeric($params['customer_bill_status'])){ //状态
            $data.= " and customer_bill.customer_bill_status = ".$params['customer_bill_status'];
        }

        if(is_numeric($params['customer_invoice_status'])){ //状态
            if($params['customer_invoice_status']==4){
                $data.= " and customer_bill.customer_invoice_status in (2,4)";
            }else {
                $data .= " and customer_bill.customer_invoice_status = " . $params['customer_invoice_status'];
            }
        }

        if($is_count==true){
            $result = $this->table("customer_bill")->
            join('customer_bill_info','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
            where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("customer_bill")->
                join('customer_bill_info','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['customer_bill.*','customer_bill_info.orders_id'=>'finance_id',"(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "(select company_name from company where company.company_id = customer_bill.company_id)"=> 'company_name'
                ])->select();

                foreach($result as $key=>$val) {
                    $project_name = $this->table("project")->
                    where("project_id = " . $result[$key]['project_id'])->field("project_name")->find();
                    $result[$key]['project_name'] =  $project_name['project_name'];
                }

            }else{
                $result = $this->table("customer_bill")->
                join('customer_bill_info','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
                where($data)->order('create_time desc')->
                field(['customer_bill.*','customer_bill_info.orders_id'=>'finance_id',"(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "(select company_name from company where company.company_id = customer_bill.company_id)"=> 'company_name'
                ])->select();

                foreach($result as $key=>$val) {
                    $project_name = $this->table("project")->
                    where("project_id = " . $result[$key]['project_id'])->field("project_name")->find();
                    $result[$key]['project_name'] =  $project_name['project_name'];
                }
            }
        }
        return $result;
    }

    /**
     * 获取客户已对账单
     * 韩
     */
    public function getCustomerYesBill($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(isset($params['customer_bill_number'])){ //对账编号
            $data.= ' and customer_bill.customer_bill_number like "%'.$params['customer_bill_number'].'%"';
        }
//        if(!empty($params['send_name'])){ //发货方
//            $data.=' and customer_bill.send_name ="'.$params['send_name'].'"';
//        }
        if(isset($params['customer_bill_name'])){ //对账名称
            $data.= ' and customer_bill.customer_bill_name like "%'.$params['customer_bill_name'].'%"';
        }

        if(is_numeric($params['choose_company_id'])){ //分公司
            $data.= " and customer_bill.company_id = ".$params['choose_company_id'];
        }

        if(is_numeric($params['customer_bill_status'])){ //状态
            $data.= " and customer_bill.customer_bill_status = ".$params['customer_bill_status'];
        }

        if(is_numeric($params['customer_invoice_status'])){ //状态
            if($params['customer_invoice_status']==4){
                $data.= " and customer_bill.customer_invoice_status in (2,4)";
            }else {
                $data .= " and customer_bill.customer_invoice_status = " . $params['customer_invoice_status'];
            }
        }

        if($is_count==true){
            $result = $this->table("customer_bill")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("customer_bill")->
                join('customer_bill_info','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['customer_bill.*','customer_bill_info.orders_id'=>'finance_id',"(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "(select company_name from company where company.company_id = customer_bill.company_id)"=> 'company_name'
                    ])->select();

                foreach($result as $key=>$val) {
                    $project_name = $this->table("project")->
                    where("project_id = " . $result[$key]['project_id'])->field("project_name")->find();
                    $result[$key]['project_name'] =  $project_name['project_name'];
                }

            }else{
                $result = $this->table("customer_bill")->
                join('customer_bill_info','customer_bill.customer_bill_id = customer_bill_info.customer_bill_id','left')->
                where($data)->order('create_time desc')->
                field(['customer_bill.*','customer_bill_info.orders_id'=>'finance_id',"(select nickname from user where user.user_id = customer_bill.create_user_id)"=> 'create_user_name',
                    "(select company_name from company where company.company_id = customer_bill.company_id)"=> 'company_name'
                ])->select();

                foreach($result as $key=>$val) {
                    $project_name = $this->table("project")->
                    where("project_id = " . $result[$key]['project_id'])->field("project_name")->find();
                    $result[$key]['project_name'] =  $project_name['project_name'];
                }
            }
        }
        return $result;
    }

	
    //修改客户账单
    public function updateCustomerBill($params){
    	$t = time();


    	if(isset($params['back_remark'])){
    		$data['back_remark'] = $params['back_remark'];
    	}

    	if(isset($params['customer_bill_status'])){
    		$data['customer_bill_status'] = $params['customer_bill_status'];
    		$customerBillInfo = new CustomerBillInfo();
    		$customerBillInfoParams['customer_bill_id'] = $params['customer_bill_id'];
    		$customerBillInfoResult = $customerBillInfo->getCustomerBillInfo($customerBillInfoParams);

			if($params['customer_bill_tax']=='不开票'){
				$data['customer_invoice_status'] = 4;
			}

    		if($data['customer_bill_status']==2){//通过后变成2 订单也需要变更
    			$data['create_invoice_user_id'] = $params['user_id'];
    			$data['create_invoice_time'] =  $t;

    		}else if($data['customer_bill_status']==3){ //驳回后finince的inovice_status 变成0
    			$data['back_user_id'] = $params['user_id'];
    			$data['back_time'] =  $t;
    		}else if($data['customer_bill_status']==4){
				$data['create_over_time'] = $params['create_over_time'];
				$data['create_over_user_id'] =  $params['user_id'];
			}
    	}
		if(is_numeric($params['customer_invoice_status'])){
			$data['customer_invoice_status'] = $params['customer_invoice_status'];
			if($params['customer_invoice_status']==2){
				$data['customer_invoice_name'] = $params['customer_invoice_name'];

			}
		}


    	//        $data['create_time'] = $t;
    	//        $data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];

	
    	Db::startTrans();
    	try {
    		Db::name('customer_bill')->where("customer_bill_id = " . $params['customer_bill_id'])->update($data);
			//代表财务开好票了
			$orders = new Orders();
			if($data['customer_invoice_status']==2){
				$customerBillInfo = new CustomerBillInfo();
				$customerBillInfoParams['customer_bill_id'] = $params['customer_bill_id'];
				$customerBillInfoResult = $customerBillInfo->getCustomerBillInfo($customerBillInfoParams);
    			for($i=0;$i<count($customerBillInfoResult);$i++){
    				$ordersParams['orders_id'] = $customerBillInfoResult[$i]['orders_id'];
    				$ordersParams['verify_status'] = 4;
    				$result = $orders->updateOrders($ordersParams);

    			}				
				
			}
    		if($data['customer_bill_status'] ==2){// 确认通过
    			$orders = new Orders();
				$sendEmail= new Sendemail();
						
						$caiwuResult = Db::name('user')->where("department_id = 52" )->select();
						for($c=0;$c<count($caiwuResult);$c++){
							$sendEmailParams['send_email']=$caiwuResult[$c]['email'];
							$sendEmailParams['send_name'] = $caiwuResult[$c]['nickname'];
							$sendEmailParams['send_title'] = '客户账单号'.$params['customer_bill_number'].'客服已确认,可以开票啦';
							$sendEmailParams['send_content'] = '请尽快开票哦';
							$sendEmail->addSendEmail($sendEmailParams);									
						}
				
						$sendEmailParams['send_email']='wcb@sun-hua.com';
						$sendEmailParams['send_name'] = '王传兵';
						$sendEmailParams['send_title'] = '客户账单号'.$params['customer_bill_number'].'客服已确认,运营请尽快提交承运商账单';
						$sendEmailParams['send_content'] = '请尽快提交哦';
						$sendEmail->addSendEmail($sendEmailParams);		
				

				
				/*
				//这里开始 判断哪些运单通过财务确认了但发运和短驳没提交的
				$customerBillInfoResult = Db::name('customer_bill_info')->where("customer_bill_id = " . $params['customer_bill_id'])->select();
				for($ii=0;$ii<count($customerBillInfoResult);$ii++){
					$shipmentResult = Db::name('shipment')->where("status =1 and  orders_number= '" . $customerBillInfoResult[$ii]['orders_number']."'")->select();
					//判断发运是否没提交数据				
					if(count($shipmentResult)==0){
						$sendEmailParams['send_email']='wcb@sun-hua.com';
						$sendEmailParams['send_name'] = '王传兵';
						$sendEmailParams['send_title'] = '运单号'.$customerBillInfoResult[$ii]['orders_number'].'发运数据未提交';
						$sendEmailParams['send_content'] = '请尽快提交哦';
						$sendEmail->addSendEmail($sendEmailParams);
					}else{
						for($j=0;$j<count($shipmentResult);$j++){
							if($shipmentResult[$j]['shipment_finance_status'] <4){
								$sendEmailParams['send_email']='wcb@sun-hua.com';
								$sendEmailParams['send_name'] = '王传兵';
								$sendEmailParams['send_title'] = '运单号'.$customerBillInfoResult[$ii]['orders_number'].'发运数据未提交';
								$sendEmailParams['send_content'] = '请尽快提交哦';
								$sendEmail->addSendEmail($sendEmailParams);								
							}
							
						}
						
					}
					//判断短驳是否没提交数据
					//先获取运单数据
					$ordersResult=Db::name('orders')->where("status =1 and  orders_number= '" . $customerBillInfoResult[$ii]['orders_number']."'")->find();
				
					//代表不需要短驳
					if($ordersResult['is_short_barge'] ==0){
						
						conditune;
					}else{
						
						$shortResult = Db::name('shipment_short_barge')->where("status =1 and  orders_number= '" . $customerBillInfoResult[$ii]['orders_number']."'")->field(["shipment_short_barge.*",
						"(select shipment_finance_status from shipment where shipment.shipment_uuid = shipment_short_barge.shipment_uuid)"=>'shipment_finance_status'
						])->find();
						if($shortResult[$i]['shipment_finance_status']<4){
							$sendEmailParams['send_email']='wcb@sun-hua.com';
							$sendEmailParams['send_name'] = '王传兵';
							$sendEmailParams['send_title'] = '运单号'.$customerBillInfoResult[$ii]['orders_number'].'短驳数据未提交';
							$sendEmailParams['send_content'] = '请尽快提交哦';
							$sendEmail->addSendEmail($sendEmailParams);									
							
						}
					
					}
					
					
				}
				*/
				

    		}
			else if($params['customer_bill_status']==3){
    		 		//要把该状态下的运单状态都变成已审核
    		 		$customer_bill_info_result = Db::name('customer_bill_info')->where('customer_bill_id ='.$params['customer_bill_id'])->select();
    		 		for($i=0;$i<count($customer_bill_info_result);$i++){
    		 			$shipmentParams['verify_status'] = 2;
    		 			$shipmentWhere['orders_id'] = $customer_bill_info_result[$i]['orders_id'];
    		 			Db::name('orders')->where($shipmentWhere)->update($shipmentParams);
    		 		}
    		 }	
			
			
			else if($data['customer_bill_status'] ==4){//变更财务状态的开票状态
    			$orders = new Orders();
    			for($i=0;$i<count($customerBillInfoResult);$i++){
    				$ordersParams['orders_id'] = $customerBillInfoResult[$i]['orders_id'];
    				$ordersParams['verify_status'] = 5;
    				$result = $orders->updateOrders($ordersParams);
    			}
    		}


    		$result = 1;
    		// 提交事务
    		Db::commit();
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    	}

		
    	return $result;


    	
    }

    //修改客户账单
    public function editCustomerInvoiceBill($params){
        $t = time();

        if(isset($params['courier_number'])){
            $data['courier_number'] = $params['courier_number'];
        }
        if(is_numeric($params['customer_invoice_status'])){ //状态
            $data['customer_invoice_status'] = $params['customer_invoice_status'];
        }
        if(is_numeric($params['customer_bill_tax_type'])){
            $data['customer_bill_tax_type'] = $params['customer_bill_tax_type'];
        }
        if(isset($params['customer_bill_tax'])){
            $data['customer_bill_tax'] = $params['customer_bill_tax'];
        }
        if(isset($params['img_url'])){
            $data['img_url'] = $params['img_url'];
        }

        $data['customer_bill_number'] = $params['customer_bill_number'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];

        Db::startTrans();
        try {
            Db::name('customer_bill')->where("customer_bill_number = '" . $params['customer_bill_number']."'")->update($data);
            $result = 200;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $data;

    }
	
	public function getCustomerBillMoney($params){
		
		$data = "1=1 ";
		
		if(is_numeric($params['customer_bill_status'])){
			
			$data.=' and customer_bill_status='.$params['customer_bill_status'];
		}
		if(is_numeric($params['customer_bill_id'])){
			
			$data.=' and customer_bill_id='.$params['customer_bill_id'];
		}
				
		if(is_numeric($params['start_update_time'])){
			
			$data.=' and update_time>='.$params['start_update_time'];
			
		}
		if(is_numeric($params['end_update_time'])){
			
			$data.=' and update<='.$params['end_update_time'];
			
		}		
		
		$result = $this->table("customer_bill")->
      
                        where($data)->field("sum(money) as money,sum(abnormal_money) as abnormal")->select();
		return $result;
	}
}