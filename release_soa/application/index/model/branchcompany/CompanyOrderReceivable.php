<?php

namespace app\index\model\branchcompany;
use think\Model;
use app\index\service\PublicService;
class CompanyOrderReceivable extends Model{

    protected $table = 'company_order_receivable';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {
        $this->_languageList = config('systom_setting')['language_list'];
        $this->_public_service = new PublicService();
        parent::initialize();

    }

    /**
     * 添加 订单应收
     *
     */
    public function addCompanyOrderReceivable($params){

        $data['company_order_number'] = $params['company_order_number'];
        $data['receivable_money'] = $params['receivable_money'];
        $data['tax_money'] = $params['tax_money'];
        $data['currency_id'] = $params['currency_id'];
        $data['receivable_number'] = $params['receivable_number'];
        $data['create_user_id'] = $params['create_user_id'];
        $data['create_time'] = time();
  

        $this->startTrans();
        try{
            $this->insert($data);
            $result = $this->insertGetId($data);

            // 提交事务
           $this->commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }


    public function getCompanyOrderReceivable($params)
    {
        $data = " 1=1 ";
        if(!empty($params['company_order_number'])){
            $data.= " and company_order_number = '".$params['company_order_number']."'";
        }



        try{
            $result = $this->
                where($data)->select();

        } catch (\Exception $e) {
            $result = $e->getMessage();

            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();

        }

        return $result;
    }




    public function updateCompanyOrderReceivable($params)
    {
		
    	$t = time();    	
    	$data['receivable_money'] = $params['receivable_money'];
    	$data['tax_money'] = $params['tax_money'];   	
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['now_user_id'];
    	if(is_numeric($params['company_order_receivable_id'])){
    		$where['company_order_receivable_id'] = $params['company_order_receivable_id'];
    	}

    	$this->startTrans();
   
        try{
            $result =$this->where($where)->update($data);

            
            // 提交事务
            $this->commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }
        error_Log(print_r($result,1));
    	return $result;
    }

}