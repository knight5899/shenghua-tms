<?php

namespace app\index\model\branchcompany;
use think\Exception;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class CompanyOrderCustomer extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'company_order_customer';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {
        $this->_languageList = config('systom_setting')['language_list'];
        $this->_public_service = new PublicService();
        parent::initialize();

    }

    /**
     * 添加分公司 订单-游客
     * 胡
     */
    public function addCompanyOrderCustomer($params){

        $t = time();

        $data['customer_id'] = $params['customer_id'];
        $data['company_order_number'] = $params['company_order_number'];
        $data['since_the_tip'] = $params['since_the_tip'];
        
		if(!empty($params['special_claim'])){
			$data['special_claim'] = $params['special_claim'];
		}
		if(!empty($params['seat_no'])){
			$data['seat_no'] = $params['seat_no'];
		}
		
		if(is_numeric($params['since_the_tip'])){
			$data['since_the_tip'] = $params['since_the_tip'];
		}
		
        $params['type'] = 1;

        $data['create_time'] = $t;
        $data['create_user_id'] = $params['now_user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['now_user_id'];
        $data['status'] = 1;




        $this->startTrans();
        try{

        	
            $result = $this->insertGetId($data);

            
            // 提交事务
           $this->commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }

        return $result;
    }
	/**
	 * 添加占位
	 */
    public function addCustomerOccupy($params){

    	$t = time();
    	$user_id = $params['now_user_id'];
    	$order_number = $params['company_order_number'];
    	//$sql =  "insert into company_order_customer (company_order_number,customer_id,create_time,create_user_id,update_time,update_user_id,status) values";
    	
    	
    	$this->startTrans();
    	try{
    		for($i=0;$i<$params['occupy_count'];$i++){
    			//$sql.="('$order_number',0,$t,$user_id,$t,$user_id,2),";
    			$data = [
    				'company_order_number'=>$order_number,
    				'customer_id'=>0,
    				'create_user_id'=>$user_id,
    				'update_user_id'=>$user_id,
    				'create_time'=>$t,
    				'update_time'=>$t,
    				'status'=>1	
    			];
    			
    			$company_order_customer_id = $this->insertGetId($data);
    			
    			$accommodation_params = [
    				'company_order_number'=>$order_number,
    				'company_order_customer_id'=>$company_order_customer_id,
    				'customer_id'=>0,	
    				'create_user_id'=>$user_id,
    				'update_user_id'=>$user_id,
    				'create_time'=>$t,
    				'update_time'=>$t,
    				'status'=>1	
    			];
    			
    			$this->table("company_order_flight")->insert($accommodation_params);
    			
    		}
    		
    		
    		
    		$this->commit();

    		
    		$result = 1;
    	
    		 
    		// 提交事务
    		$this->commit();
    	
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		$this->rollback();
    	
    	}	
    	error_log(print_r($result,1));
    	return $result;
    }
    
    /**
     * 通过顾客ID获取订单编号
     */
    public function getCompanyOrderNumberByCompanyOrderCustomerId($params){
   
    	if(!empty($params['company_order_customer_id'])){
    		$data= " company_order_customer_id = ".$params['company_order_customer_id'];
    	}

    	$result= $this->
    	where($data)->
    	field([
    			'company_order_number',

    			'status'])->
    	
    			select();

    	return $result;
    }
    /**
     * 获取顾客数据
     * 胡
     */
    public function getCompanyOrderCustomer($params){

 
    	$data = "1=1 and coc.status >0";
    	if(!empty($params['company_order_customer_id'])){
    		$data.= " and coc.company_order_customer_id = ".$params['company_order_customer_id'];
    	}
    	if(!empty($params['customer_id'])){
    		$data.= " and coc.customer_id = ".$params['customer_id'];
    	}
    	if(!empty($params['company_order_number'])){
    		$data.= " and coc.company_order_number = '".$params['company_order_number']."'";
    	}
 
   	
            $result= $this->table("company_order_customer")->alias("coc")->
            join("customer customer","customer.customer_id = coc.customer_id",'left')->
            join("country country","customer.country_id = country.country_id",'left')->
            join("company company","company.company_id = customer.company_id",'left')->
            join("language language","customer.language_id = language.language_id",'left')->
            join("company_order_accommodation coa","coa.company_order_number = coc.company_order_number and coa.company_order_customer_id = coc.company_order_customer_id",'left')->
            join("room_type",'room_type.room_type_id = coa.room_type','left')->
            where($data)->
            field(['coc.customer_id','coc.company_order_number','coc.company_order_customer_id','coc.special_claim',
            		"if(coc.customer_id=0,'占位',concat(customer.customer_first_name,' ',customer.customer_last_name)) customer_name",
            		'company.company_id','company.company_name','coc.seat_no',
            		'customer.customer_number','coc.since_the_tip',
            		'customer.passport_number','customer.emergency_contact','customer.emergency_call','customer.address',
            		'customer.customer_first_name','customer.customer_last_name','customer.middle_name',
            		'customer.english_first_name','customer.english_last_name',
            		'customer.customer_type','customer.gender','customer.phone',
            		'customer.email','customer.card_type','customer.card_number',
            		'customer.issuing_date','customer.term_of_validity','customer.birthday','customer.remark',
            		'customer.language_id','language.language_name',
            		'country.country_id as country_id','country.country_name as country_name',
            		'coa.check_in_hotel','coa.check_on_hotel',
            		"coa.room_code",'coa.room_type','coa.check_in','coa.check_on','coa.check_in_room_type','coa.check_on_room_type',
                   
            		"(select nickname  from user where user.user_id = customer.create_user_id)"=>'create_user_name',
            		"(select nickname  from user where user.user_id = customer.update_user_id)"=>'update_user_name',
            		'coc.create_user_id','coc.create_time','coc.update_user_id','room_type.room_type_name','room_type.accommodate',
            		'coc.update_time','coc.status','customer.status as customer_status','customer.blacklist_company_id'])->
            
            select();
	
        return $result;

    }

    /**
     * 通过团队产品编号获取所有游客信息
     */

    public function getCompanyOrderCustomerByTeamProductId($params){
    
    
    	$data = "1=1 and copt.status >0 and co.status>0 and coc.status>0";
    	if(!empty($params['team_product_id'])){
    		$data.= " and copt.team_product_id = ".$params['team_product_id'];
    	}
    
    	
    	$result= $this->table('company_order_product_team')->alias('copt')->
    	join("company_order co",'co.company_order_number = copt.company_order_number ','left')->
    	join("company_order_customer coc",'coc.company_order_number = co.company_order_number and coc.status>0','right')->
    	join("customer customer","customer.customer_id = coc.customer_id",'left')->
     	join("country country","customer.country_id = country.country_id",'left')->
//     	join("company company","company.company_id = customer.company_id",'left')->
     	join("language language","customer.language_id = language.language_id",'left')->
    	join("company_order_accommodation coa","coa.company_order_customer_id = coc.company_order_customer_id and coa.status =1",'left')->
    	join('`room_type` rt','rt.room_type_id = coa.room_type','left')->
    	where($data)->
    	field(['coc.customer_id','coc.company_order_customer_id','coc.special_claim','co.company_order_number',
    			"if(coc.customer_id=0,'占位',concat(customer.customer_first_name,' ',customer.customer_last_name)) customer_name",
    			//'company.company_id','company.company_name',
    			'customer.customer_number','coa.check_in_room_type','coa.check_on_room_type',
    			'customer.passport_number','customer.emergency_contact','customer.emergency_call','customer.address',
    			'customer.customer_first_name','customer.customer_last_name','customer.middle_name',
    			'customer.english_first_name','customer.english_last_name',
    			'customer.customer_type','customer.gender','customer.phone',
    			'customer.email','customer.card_type','customer.card_number',
    			'customer.issuing_date','customer.term_of_validity','customer.birthday','customer.remark',
    			'customer.language_id','language.language_name',
    			'country.country_id as country_id','country.country_name as country_name',
    			'coa.check_in_hotel','coa.check_on_hotel',
    			"coa.room_code",'coa.room_type','coa.check_in','coa.check_on',
				'rt.room_type_name','coc.since_the_tip','co.company_order_status',
    
    			"(select nickname  from user where user.user_id = customer.create_user_id)"=>'create_user_name',
    			"(select nickname  from user where user.user_id = customer.update_user_id)"=>'update_user_name',
    			'coc.create_user_id','coc.create_time','coc.update_user_id',
    			'coc.update_time','coc.status'])->
    
    			select();
		
    	return $result;
    
    }    
	/**
	 * 获取订单下的顾客信息
	 */
	 public function getCompanyOrderCustomerByCompanyOrderNumber($params){
	 	$data = "1=1  and coc.status>0";

	 	if(isset($params['company_order_number'])){
	 		$data.= " and coc.company_order_number = '".$params['company_order_number']."'";
	 	}

	 	
	 	$result= $this->table("company_order_customer")->alias("coc")->
	 	join("customer customer","customer.customer_id = coc.customer_id",'left')->

	 	where($data)->
	 	field(['company_order_customer.company_order_customer_id','company_order_customer.customer_id',
	 			
	 			'customer.customer_first_name','customer.customer_last_name',
	 			'customer.english_first_name','customer.english_last_name',
	 			"(select nickname  from user where user.user_id = customer.create_user_id)"=>'create_user_name',
	 			"(select nickname  from user where user.user_id = customer.update_user_id)"=>'update_user_name',
	 			'customer.create_user_id','customer.create_time','customer.update_user_id',
	 			'customer.update_time','coc.status'])->
	 	
	 			select();
	 	
	 	
	 	return $result;	 	
	 	
	 }
    /**
     * 修改顾客 根据customer_id
     */
    public function updateCompanyOrderCustomerByCompanyOrderCustomerId($params){

        $t = time();
        

   

        if(isset($params['customer_id'])){
        	$data['customer_id'] = $params['customer_id'];
        	$data['status']=1;
        	
        }


        $data['since_the_tip'] = $params['since_the_tip'];

        if(is_numeric($params['status'])){
            $data['status'] = $params['status'];

        }
		if(isset($params['special_claim'])){
			$data['special_claim'] = $params['special_claim'];
		}
		if(isset($params['seat_no'])){
			$data['seat_no'] = $params['seat_no'];
		}
        $data['update_user_id'] = $params['now_user_id'];

        $data['update_time'] = $t;



        Db::startTrans();
        try{
            Db::name('company_order_customer')->where("company_order_customer_id = ".$params['company_order_customer_id'])->update($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;
    }
	//修改游客状态
	public function updateCompanyOrderCustomerStatusByCompanyOrderCustomerId($params){
		$t = time();
		

		
		
		
		
	
		$data['status'] = $params['status'];

		
		
		$data['update_user_id'] = $params['now_user_id'];
		
		$data['update_time'] = $t;
		
		
	
		
		Db::startTrans();
		try{
			Db::name('company_order_customer')->where("company_order_customer_id = ".$params['company_order_customer_id'])->update($data);
		
			$result = 1;
			// 提交事务
			Db::commit();
		
		} catch (\Exception $e) {
			$result = $e->getMessage();
			// 回滚事务
			Db::rollback();
		
		}
		return $result;		
	}
	
	/**
	 * 获取所有订单的收客人数
	 */
	public function getAllCompanyOrderCustomer($params){
		$data="1=1 and company_order_customer.status>0 and co.status=1";
		if(!empty($params['create_time_day'])){
			$data.=" and from_unixtime(company_order_customer.create_time,'%Y%m%d') = '".$params['create_time_day']."'";
		}
		if(!empty($params['create_time_month'])){
			$data.=" and from_unixtime(company_order_customer.create_time,'%Y%m') = '".$params['create_time_month']."'";
		}
		if(!empty($params['company_id'])){
			$data.=" and co.company_id = ".$params['company_id'];
		}
		if(!empty($params['create_time_diy_day'])){
			$data.=" and from_unixtime(company_order_customer.create_time,'%Y%m%d') >=".$params['create_time_diy_day'];
		}
		
		$result= $this->table("company_order")->alias("co")->
		join("company_order_customer",'company_order_customer.company_order_number = co.company_order_number','left')->
		join("customer customer","customer.customer_id = company_order_customer.customer_id",'left')->
		
		where($data)->
		field(['company_order_customer.company_order_customer_id','company_order_customer.customer_id',
			 	
				'customer.customer_first_name','customer.customer_last_name',
				'customer.english_first_name','customer.english_last_name',
				"(select nickname  from user where user.user_id = customer.create_user_id)"=>'create_user_name',
				"(select nickname  from user where user.user_id = customer.update_user_id)"=>'update_user_name',
				'customer.create_user_id','company_order_customer.create_time','customer.update_user_id',
				"from_unixtime(company_order_customer.create_time,'%Y-%m-%d') as from_unixtime_time",
				'customer.update_time','company_order_customer.status'])->
				 
				select();
		 
		
		return $result;
	}



    public function addB2bOrderCustomer($params){

        $t = time();

        $data['customer_id'] = $params['customer_id'];
        $data['company_order_number'] = $params['company_order_number'];
        $data['b2b_booking_id'] = $params['b2b_booking_id'];
        $data['type'] = 2;
        $data['temp_id'] = $params['seat_no'];
        $data['seat_no'] = $params['seat_no'];
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['now_user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['now_user_id'];
        $data['status'] = 1;

        try{
            $result = $this->insertGetId($data);
        } catch (\Exception $e) {
            $result = $e->getMessage();

            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

	//修改b2b_booking的游客
    public function updateB2bBookingCustomer($params)
    {
        try{
            $this->table("company_order_customer")->startTrans();
            $this->table("company_order_customer")->where(['b2b_booking_id' => $params['b2b_booking_id']])->update(['status' => 0]);
            $customer_model = new Customer();
            $t = time();
            foreach ($params['customer'] as $v)
            {
                $customer_info = $customer_model->getOneCustomerByPassport($v['passport_number']);
                $v['now_user_id'] = $params['now_user_id'];
                $v['birthday'] = date('Y-m-d',strtotime($v['birthday']));
                if ($customer_info)
                {
                    //有 修改
                    $v['customer_id'] = $customer_info['customer_id'];
                    $customer_model->updateCustomerByCustomerId($v);
                }
                else
                {
                    //无 添加
                    $customer_id = $customer_model->addCustomer($v);
                    $v['customer_id'] = $customer_id;
                }
                $data['b2b_booking_id'] = $params['b2b_booking_id'];
                $data['seat_no'] = $v['seat_no'];
                $data['customer_id'] = $v['customer_id'];
                $data['temp_id'] = $v['temp_id'];
                $data['status'] = 1;
                $data['update_time'] = $t;
                $data['update_user_id'] = $params['now_user_id'];
                if ($v['company_order_customer_id'])
                {
                    $where = " type = 2 and company_order_customer_id = ". $v['company_order_customer_id'];
                    $this->table("company_order_customer")->where($where)->update($data);
                }
                else
                {
                    $data['b2b_booking_id'] = $params['b2b_booking_id'];
                    $b2b_booking = $this->table('b2b_booking')->where(['b2b_booking_id' => $params['b2b_booking_id']])->find();
                    $data['company_order_number'] = $b2b_booking['company_order_number'];
                    $data['type'] = 2;
                    $data['create_time'] = $t;
                    $data['create_user_id'] = $params['now_user_id'];
                    $this->table("company_order_customer")->insert($data);
                }
            }
            $this->table("company_order_customer")->commit();
            $result = 1;
        } catch (\Exception $e) {
            $this->table("company_order_customer")->rollback();
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }
        return $result;
    }

    //获取b2b_booking的游客
    public function getB2bBookingCustomerByBtbBookingId($b2b_booking_id)
    {
        try
        {
            $where['b2b_booking_id'] = $b2b_booking_id;
            $where['company_order_customer.status'] = 1;
            $where['type'] = 2;
            $result = $this->table("company_order_customer")->alias('company_order_customer')->
            join("customer", "company_order_customer.customer_id = customer.customer_id")->
            field([
                'company_order_customer.*', 'customer.customer_first_name', 'customer.customer_last_name', 'customer.gender',
                'customer.birthday', 'customer.age_group', 'customer.speaking_fluent', 'customer.ethnicity', 'customer.nationality',
                'customer.passport_number', 'customer.term_of_validity'
            ])->where($where)->select();

        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }
    
    /**
     * 根据 订单的游客信息 根据参数
     */
    public function updateCompanyOrderCustomerByParams($params){
    	

   		$t = time();
    	if(is_numeric($params['company_order_number'])){
    		$where['company_order_number'] = $params['company_order_number'];
    	}
    	

        if(is_numeric($params['now_lineup_number'])){
        	$data['now_lineup_number'] = $params['now_lineup_number'];

        	
        }

        if(is_numeric($params['status'])){
        	$data['status'] = $params['status'];
        
        	 
        }

        $data['update_time'] = $t;



        $this->startTrans();
        try{
           	$this->where($where)->update($data);

            $result = 1;
            // 提交事务
            $this->commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }
        return $result;
    	
    }
}