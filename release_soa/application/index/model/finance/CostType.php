<?php

namespace app\index\model\finance;

use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;

class CostType extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'cost_type';
    private $_languageList;
    private	$_cope;
    private $_public_service;
    public function initialize()
    {
    	
   

        $this->_public_service = new PublicService();
        parent::initialize();

    }

    /**
     * 添加cost type
     * 胡
     */
    public function addCostType($params){
	
	
        $t = time();
		$data['cost_type_name'] = $params['cost_type_name'];
       
		if(!empty($params['remark'])){
			$data['remark'] = $params['remark'];
		}

		$data['status'] = 1;
		$data['cost_type_code'] = $params['cost_type_code'];
		
		
		
	
        $this->startTrans();
        try{
            $pk_id = $this->insertGetId($data);
           
			
            // 提交事务
            $this->commit();
            $result = $pk_id;

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }
		
        return $result;
    }
    /**
     * 修改cost type
     * 胡
     */
    public function updateCostType($params){
	
	
 
		$data['cost_type_name'] = $params['cost_type_name'];
       
		if(!empty($params['remark'])){
			$data['remark'] = $params['remark'];
		}

		$data['status'] = $params['status'];
		
		
		
		
	
        $this->startTrans();
        try{
            $pk_id = $this->where("cost_type_id =".$params['cost_type_id'])->update($data);
           
			
            // 提交事务
            $this->commit();
            $result = $pk_id;

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }
		
        return $result;
    }
    /**
     * 获取cost type
     * 王
     */
    public function getCostType($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
		


    	if(is_numeric($params['status'])){
    		$data['status']= $params['status'];
    	}

		if(!empty($params['cost_type_name'])){
			$data['cost_type_name']= array('like',"%".$params['cost_type_name']."%");
		}
		if(!empty($params['cost_type_id'])){
			$data['cost_type_id']= $params['cost_type_id'];
		}

		
        if($is_count==true){
				$result = $this->
                where($data)->count();
        }else {
            if ($is_page == true) {
                $result= $this->
                where($data)->limit($page, $page_size)->order('cost_type_id desc')->
				select();
            } else {
                $result = $this->where($data)->order('cost_type_id desc')->select();
            }
        }
		
				
		
		return $result;
    }

    
}