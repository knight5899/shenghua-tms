<?php
namespace app\index\controller;
use app\common\help\Help;
use app\index\model\equipment\Equipment as EquipmentModel;
use \app\index\controller\Base;
class Equipment extends Base
{
	private $_equipment;
	public function __construct()
    {
		$this->_equipment = new EquipmentModel;
	}	
	/**
     * 添加设备
     * 胡
     */
    public function addEquipment(){
	
    	$params = $this->input();
  	
    	$paramRule = [
    		'equipment_number_in'=>'string',
    	];
    
    	$this->paramCheckRule($paramRule,$params);
		
		
    	$result = $this->_equipment->addEquipment($params);
    
    	$this->outPut($result);
    }
	/**
     * 获取设备
     * 胡
     */
    public function getEquipment(){
	
    	$params = $this->input();

    	
        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $this->_equipment->getEquipment($params, true);
            $result = $this->_equipment->getEquipment($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];

            return $this->output($data);
        }
    	$result = $this->_equipment->getEquipment($params);

    	$this->outPut($result);
    }

    /**
     * 修改设备添加token
     * 韩
     */
    public function editEquipment(){

        $params = $this->input();

        $result = $this->_equipment->editEquipment($params);

        $this->outPut($result);
    }

    /**
     * 添加最新设备信息
     * 韩
     */
    public function addEquipmentAll(){

        $params = $this->input();

        $result = $this->_equipment->addEquipmentAll($params);

        $this->outPut($result);
    }

    public function getEquipmentHistory(){

        $params = $this->input();

        $result = $this->_equipment->getEquipmentHistory($params);

        $this->outPut($result);
    }
}
