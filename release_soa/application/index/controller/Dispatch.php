<?php

namespace app\index\controller;

use app\common\help\Help;
use app\index\model\dispatch\DispatchFee;
use app\index\model\dispatch\DispathGoods;
use app\index\model\orders\Orders;
use app\index\model\orders\Ordersgoods;
use app\index\model\transport\Transport;
use app\index\model\dispatch\Dispatch as Dispatchmodel;
use app\index\validate\transport\Transport as Transportvalidate;
use think\config;

use app\index\service\SourceService;
use app\index\service\BranchProductService;

use app\index\service\InStationLetterService;
use think\Model;
use think\Controller;
use app\common\help\Contents;
use app\index\service\PublicService;
use app\common\help\Transport as transportHelp;
use think\Validate;

class Dispatch extends Base
{

    private $_orders;
    private $_transport;

    //_lang Base里的属性，
    public function __construct()
    {
        $this->_transportHelp = new transportHelp();
        $this->_transport = new Transport();
        $this->_ordersGoods = new Ordersgoods();
        $this->_dispatchModel = new Dispatchmodel();
        $this->_dispatchGoodsModel = new DispathGoods();
        $this->_transportValidate = new Transportvalidate();
        parent::__construct();
    }


    public function getDispatchById()
    {
        $params = $this->input();
        $orderResult = $this->_dispatchModel->getDispatchById($params);
        $this->outPut($orderResult);
    }

    //获取调度商品
    public function getDispatchGoods()
    {
        $params = $this->input();
        $orderResult = $this->_dispatchGoodsModel->getDispatchGoods($params, true);
        $this->outPut($orderResult);
    }

//获取调度列表
    public function getDispatch()
    {
        $params = $this->input();
        $orderResult = $this->_dispatchModel->getDispatch($params, true);
        $this->outPut($orderResult);
    }

//获取运单
    public function getTransport()
    {
        $params = $this->input();

        $orderResult = $this->_transport->getTransport($params, true);


        $this->outPut($orderResult);
    }

    public function uploadDispatchChild(){
        $params = $this->input();
        $dispatch_id=$params['dispatch_id'];
        $validate=new Validate(['dispatch_id'=>'number|require']);
        if(!$validate->check($params))
            return  $this->outPutError($validate->getError());





        if (!isset($params['pickup_order_add_type'])) {
            $zhongzhuanLists = $this->_transportHelp->filter_zhongzhuan($params);
            foreach ($zhongzhuanLists as $k => $v) {
                 $v['dispatch_id']=$dispatch_id;
                 $this->_dispatchModel->upload($v);
                $this->_dispatchGoodsModel->add($v['goods'], $dispatch_id);
            }
        }

        //添加提货
        $tihuoLists = $this->_transportHelp->filter_tihuo($params);
        foreach ($tihuoLists as $k => $v) {
            $v['dispatch_id']=$dispatch_id;

            $this->_dispatchModel->upload($v);
            $dispatch_ids[] = $dispatch_id;
            $this->_dispatchGoodsModel->add($v['goods'], $dispatch_id);
        }
        if (isset($params['pickup_order_add_type'])) return $this->outPut('success');

        $ganxianLists = $this->_transportHelp->filter_ganxian($params);
        $ganxianLists['dispatch_id']=$dispatch_id;

        $this->_dispatchModel->upload($ganxianLists);
        $this->_dispatchGoodsModel->add($ganxianLists['goods'], $dispatch_id);
        $peisongLists = $this->_transportHelp->filter_peisong($params);
        $peisongLists['dispatch_id']=$dispatch_id;

        $this->_dispatchModel->upload($peisongLists);
        $this->_dispatchGoodsModel->add($peisongLists['goods'], $dispatch_id);

            return  $this->outPut('success');

    }



    /**
     * 添加运单
     * 胡
     */
    public function addTransport()
    {



        $orders_numbers = [];
        $transport_ids = [];
        $dispatch_ids = [];
        $params = $this->input();
        $fee_data=$this->_transportHelp->filter_fee($params);
        $fee_model=new DispatchFee();
        //添加运单
        $transportlists = $this->_transportHelp->filter_transport($params);

        foreach ($transportlists as $k => $v) {

            if (isset($params['pickup_order_add_type']))
                foreach ($transportlists as $k1 => $v1) {
                    if ($v1['send_area_name'] != $v['send_area_name'] || $v1['send_address'] != $v['send_address'])
                        return $this->outPutError("发货地址不一样不能合并订单");
                }

            $res = $this->_transportValidate->check($v);
            if (!$res) {
                $this->outPutError($this->_transportValidate->getError());
            } else {
                $orders_numbers[] = $v['order_number'];


            }
        }
        $transportlists[0]['order_number'] = implode(',', $orders_numbers);
        $id = $this->_transport->addTransport($transportlists[0]);
        $transport_ids[] = 'T-' . $id;


        //添加中转
        if (!isset($params['pickup_order_add_type'])) {
            $zhongzhuanLists = $this->_transportHelp->filter_zhongzhuan($params);
            foreach ($zhongzhuanLists as $k => $v) {
                $v['transport_number'] = implode(',', $transport_ids);
                $dispatch_id = $this->_dispatchModel->add($v);

                $fee_model->add(array_shift($fee_data['zhongzhuan']),$dispatch_id,explode(",",$params['orders_id'])[0],$params);
                $dispatch_ids[] = $dispatch_id;
                $this->_dispatchGoodsModel->add($v['goods'], $dispatch_id);
            }
        }

        //添加提货
        $tihuoLists = $this->_transportHelp->filter_tihuo($params);
        foreach ($tihuoLists as $k => $v) {
            $v['transport_number'] = implode(',', $transport_ids);
            $dispatch_id = $this->_dispatchModel->add($v);
            $dispatch_ids[] = $dispatch_id;
            $this->_dispatchGoodsModel->add($v['goods'], $dispatch_id);
            $type_name=isset($params['pickup_order_add_type'])?"zhengche":"tihuo";
//error_log(json_encode($fee_data[$type_name]));
            $fee_model->add(array_shift($fee_data[$type_name]),$dispatch_id,explode(",",$params['orders_id'])[0],$params);
        }
        if (isset($params['pickup_order_add_type'])) return $this->outPut('success');

        $ganxianLists = $this->_transportHelp->filter_ganxian($params);
        $ganxianLists['transport_number'] = implode(',', $transport_ids);
        $dispatch_id = $this->_dispatchModel->add($ganxianLists);
        $this->_dispatchGoodsModel->add($ganxianLists['goods'], $dispatch_id);
        $fee_model->add($fee_data['ganxian'],$dispatch_id,explode(",",$params['orders_id'])[0],$params);

        $peisongLists = $this->_transportHelp->filter_peisong($params);
        $peisongLists['transport_number'] = implode(',', $transport_ids);
        $dispatch_id = $this->_dispatchModel->add($peisongLists);
        $this->_dispatchGoodsModel->add($peisongLists['goods'], $dispatch_id);
        $fee_model->add($fee_data['peisong'],$dispatch_id,explode(",",$params['orders_id'])[0],$params);
       return  $this->outPutError("测试");
        $this->outPut("success");


    }


    /**
     * 获取订单
     */
    public
    function getOrder()
    {

        $params = $this->input();

        $order = new Orders();

        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getOrder($params, true);
            $result = $this->_orders->getOrder($params, false, 'true', $page, $page_size);

            for ($i = 0; $i < count($result); $i++) {
                $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
                $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);

            }
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
        }
        $orderResult = $this->_orders->getOrder($params);

        for ($i = 0; $i < count($orderResult); $i++) {
            $orderGoodsParams['orders_id'] = $orderResult[$i]['orders_id'];
            $orderResult[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);

        }
        $this->outPut($orderResult);


    }

    /**
     * 修改订单根据订单ID
     * 胡
     */
    public function updateOrderByOrderId()
    {

        $params = $this->input();

        $paramRule = [

            'orders_id' => 'string',
            'project_id' => "string"


        ];
        $this->paramCheckRule($paramRule, $params);


        $result = $this->_orders->updateOrderByOrderId($params);

        $this->outPut($result);
    }

    /**
     * 获取订单货物
     */
    public
    function getOrderGoods()
    {

        $params = $this->input();


        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_ordersGoods->getOrdersGoods($params, true);
            $result = $this->_ordersGoods->getOrdersGoods($params, false, 'true', $page, $page_size);


            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
        }
        $result = $this->_ordersGoods->getOrdersGoods($params);


        $this->outPut($result);


    }

}
