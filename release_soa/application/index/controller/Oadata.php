<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\oadata\OaData as oad;

class Oadata extends Base
{
    /**
     * 获取资料信息
     */
    public function getData(){

        $params = $this->input();

        //实例化模型
        $oadata = new oad();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $oadata->getOaData($params, true);
            $result = $oadata->getOaData($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $dataResult = $oadata->getOaData($params);
        $this->outPut($dataResult);
    }


    /**
     * 获取資料分组信息
     */
    public function getDataGroupAjax(){

        $params = $this->input();

        //实例化模型
        $oadata = new oad();

        $warehouseResult = $oadata->getOaDataAjax($params);
        $this->outPut($warehouseResult);
    }


    /**
     *  添加资料
     */
    public function addData(){

        $params = $this->input();

        $paramRule = [
            'group_id'=>'number',
            'file_name'=>'string'
        ];

        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $oadata = new oad();
        $result = $oadata->addData($params);

        $this->outPut($result);
    }

    /**
     * 修改资料根据资料ID
     * 韩
     */
    public function updateDataByDataId(){

        $params = $this->input();

        $paramRule = [
            'data_id'=>'number'
        ];
        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $oadata = new oad();
        $result = $oadata->updateDataByDataId($params);

        $this->outPut($result);
    }
}