<?php


namespace app\index\controller;


class Despatch extends Base
{



    public function addDespatch(){
        $params = $this->input();
        $despatchmodel=new \app\index\model\despatch\Despatch();
        $orderResult =$despatchmodel->addDespatch ($params);
        $this->outPut($orderResult);
    }

    public function getDespatch(){
        $params = $this->input();
        $despatchmodel=new \app\index\model\despatch\Despatch();
        $orderResult =$despatchmodel->getDespatch($params);
        $this->outPut($orderResult);
    }

}