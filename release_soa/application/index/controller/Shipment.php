<?php

namespace app\index\controller;

use app\common\help\Contents;
use app\common\help\Help;

use app\index\model\index\IndexModel;
use app\index\model\orders\Orders;
use think\config;

use app\index\model\shipment\Shipment as ShipmentModel;
use app\index\model\shipment\ShipmentGoods;
use app\index\model\shipment\ShipmentCost;
use app\index\model\shipment\ShipmentPay;
use app\index\model\shipment\ShipmentShortBarge;
use app\index\model\shipment\ShipmentAbnormal;
use app\index\model\orders\Ordersgoods;
use app\index\model\orders\OrdersAbnormal;
use app\index\model\source\City;
use think\Model;

class Shipment extends Base
{
    private $_shipment;
    private $_shipmentGoods;
    private $_shipmentCost;
    private $_shipmentPay;
    private $_shipmentShortBarge;
    private $_ordersGoods;
    private $_city;
    private $_shipmentAbnormal;
	private $_ordersAbnormal;

    //_lang Base里的属性，
    public function __construct()
    {

        $this->_shipment = new ShipmentModel();
        $this->_shipmentGoods = new ShipmentGoods();
        $this->_shipmentCost = new ShipmentCost();
        $this->_shipmentPay = new ShipmentPay();
        $this->_shipmentShortBarge = new ShipmentShortBarge();
        $this->_shipmentAbnormal = new ShipmentAbnormal();
        $this->_ordersGoods = new Ordersgoods();
		$this->_ordersAbnormal = new OrdersAbnormal();
        $this->_orders = new Orders();
        $this->_index = new IndexModel();
        $this->_city = new City();
        parent::__construct();
    }

    /**
     * 获取发运打印数据
     * 袁
     */
    public function print_all()
    {
        $params = $this->input();
        $result = [];
        foreach ($params['shipment_uuid_array'] as $k => $v) {
            $result_tmp = $this->_shipment->getShipment(['shipment_uuid' => $v], 0, 1, 0, 1)[0];
            if ($result_tmp) ;
            array_push($result, $result_tmp);
        }


        //开始查询省市区 以及 货物 支付 方式 支付类型
        foreach ($result as $i => $v) {


            $provinceResult = help::getCityByCityId($v['accept_province_id']);
            $cityResult = help::getCityByCityId($v['accept_city_id']);
            $areaResult = help::getCityByCityId($v['accept_area_id']);
            $result[$i]['accept_province_name'] = $provinceResult[0]['city_name'];
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];
            $result[$i]['accept_area_name'] = $areaResult[0]['city_name'];
            $shipmentGoodsParams['shipment_uuid'] = $result[$i]['shipment_uuid'];

            $shipmentGoodsResult = $this->_shipmentGoods->getShipmentGoods($shipmentGoodsParams);
            $count_all = 0;
            $weight_all = 0;
            $volume_all = 0;
            foreach ($shipmentGoodsResult as $k => $v) {
                $count_all += $v['shipment_count'];
                $weight_all += $v['shipment_weight'];
                $volume_all += $v['shipment_volume'];
            }
            $result[$i]['count_all'] = $count_all;
            $result[$i]['weight_all'] = $weight_all;
            $result[$i]['volume_all'] = $volume_all;
            $result[$i]['shipment_goods'] = $shipmentGoodsResult;
            $shipmentCostParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentCostParams['status'] = 1;
            $shipmentCostResult = $this->_shipmentCost->getShipmentCost($shipmentCostParams);
            $newShipmentCostResult = [];
            foreach ($shipmentCostResult as $k => $v) {
                $v['cost_money'] = round($v['cost_money']);
                $newShipmentCostResult[] = $v;

            }

            $result[$i]['shipment_cost'] = $newShipmentCostResult;

            $pay_all = 0;
            $pay_other = 0;
            $pay_yun_fee=0;
            $pay_base_fee=0;

            foreach ($shipmentCostResult as $k => $v) {

               if($v['cost_id']==5)
                   $pay_yun_fee+=$v['cost_money'];
                if($v['cost_id']==9)
                    $pay_base_fee+=$v['cost_money'];

                if($v['cost_id']==6 || $v['cost_id']==10)
                    $pay_other += $v['cost_money'];
            }
            $pay_yun_fee=($pay_yun_fee>=$pay_base_fee)?$pay_yun_fee:$pay_base_fee;
            $result[$i]['pay_yun_fee']= $pay_yun_fee;
            $result[$i]['pay_all'] = $result[$i]['pay_all_money'];

            $result[$i]['pay_all_cny']="零元整";
            if($result[$i]['pay_all']>0)
            $result[$i]['pay_all_cny'] = cny($result[$i]['pay_all']) . '元整';
            $result[$i]['pay_other'] = $pay_other;

            $shipmentPayParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentPayResult = $this->_shipmentPay->getShipmentPay($shipmentPayParams);
            $result[$i]['shipment_pay'] = $shipmentPayResult;
            $result[$i]['order_info'] = $this->_orders->getShipmentNeedOrder(['orders_number' => $result[$i]['orders_number']])[0];

        }

        $this->outPut($result);
    }

    /**
     * 获取发运打印数据
     * 袁
     */
    public function getShipmentPrintData()
    {
        $params = $this->input();

        $result = $this->_shipment->getShipment($params, 0, 1, 0, 1);

        $this->_index->updateStatusByTableId(['status' => 1, 'field' => 'is_print', 'table_name' => 'shipment', 'table_id_name' => 'shipment_uuid', 'table_id' => $params['shipment_uuid']]);
        //开始查询省市区 以及 货物 支付 方式 支付类型
        for ($i = 0; $i < count($result); $i++) {


            $provinceResult = help::getCityByCityId($result[$i]['accept_province_id']);
            $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
            $areaResult = help::getCityByCityId($result[$i]['accept_area_id']);
            $result[$i]['accept_province_name'] = $provinceResult[0]['city_name'];
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];
            $result[$i]['accept_area_name'] = $areaResult[0]['city_name'];

            $shipmentGoodsParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentGoodsResult = $this->_shipmentGoods->getShipmentGoods($shipmentGoodsParams);
            $count_all = 0;
            $weight_all = 0;
            $volume_all = 0;
            foreach ($shipmentGoodsResult as $k => $v) {
                $count_all += $v['shipment_count'];
                $weight_all += $v['shipment_weight'];
                $volume_all += $v['shipment_volume'];
            }
            $result[$i]['count_all'] = $count_all;
            $result[$i]['weight_all'] = $weight_all;
            $result[$i]['volume_all'] = $volume_all;

            $result[$i]['shipment_goods'] = $shipmentGoodsResult;
            $shipmentCostParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentCostResult = $this->_shipmentCost->getShipmentCost($shipmentCostParams);
            $newShipmentCostResult = [];
            foreach ($shipmentCostResult as $k => $v) {
                $v['cost_money'] = round($v['cost_money']);
                $newShipmentCostResult[] = $v;

            }
            $result[$i]['shipment_cost'] = $newShipmentCostResult;

            $pay_all = 0;
            $pay_other = 0;
            foreach ($shipmentCostResult as $k => $v) {

                if ($v['cost_name'] == '运费' || $v['cost_name'] == '送货费' || $v['cost_name'] == '卸货费' || $v['cost_name'] == '上楼费') {
                    $pay_all += $v['cost_money'];
                } else
                    $pay_other += $v['cost_money'];
            }
            $result[$i]['pay_all'] = $pay_all;
            $result[$i]['pay_all_cny']="零元整";
            if($pay_all>0)
            $result[$i]['pay_all_cny'] = cny($pay_all) . '元整';
            $result[$i]['pay_other'] = $pay_other;

            $shipmentPayParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentPayResult = $this->_shipmentPay->getShipmentPay($shipmentPayParams);
            $result[$i]['shipment_pay'] = $shipmentPayResult;
            $result[$i]['order_info'] = $this->_orders->getShipmentNeedOrder(['orders_number' => $result[$i]['orders_number']])[0];
        }

        $this->outPut($result);

    }


    /**
     * 获取发运
     * 胡
     */
    public function getShipment()
    {

        $params = $this->input();

        //开始查询省市区
        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_shipment->getShipment($params, true);
            $result = $this->_shipment->getShipment($params, false, 'true', $page, $page_size);

            //开始查询省市区 以及 货物 支付 方式 支付类型
            for ($i = 0; $i < count($result); $i++) {
                $provinceResult = help::getCityByCityId($result[$i]['accept_province_id']);
                $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
                $areaResult = help::getCityByCityId($result[$i]['accept_area_id']);

                $result[$i]['accept_province_name'] = $provinceResult[0]['city_name'];
                $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];
                $result[$i]['accept_area_name'] = $areaResult[0]['city_name'];
                if ($result[$i]['need_time'] > 0)
                    $result[$i]['need_time_txt'] = date('Y-m-d', $result[$i]['need_time']);

                $shipmentGoodsParams['shipment_uuid'] = $result[$i]['shipment_uuid'];

                $shipmentGoodsResult = $this->_shipmentGoods->getShipmentGoods($shipmentGoodsParams);
                $result[$i]['shipment_goods'] = $shipmentGoodsResult;
                $shipmentCostParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
                $shipmentCostResult = $this->_shipmentCost->getShipmentCost($shipmentCostParams);
                $result[$i]['shipment_cost'] = $shipmentCostResult;
                $shipmentPayParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
                $shipmentPayResult = $this->_shipmentPay->getShipmentPay($shipmentPayParams);
                $result[$i]['shipment_pay'] = $shipmentPayResult;
                $orderResult = $this->_orders->getOrder(['orders_number' => $result[$i]['orders_number']]);
                $result[$i]['orders_data'] = $orderResult[0];

                $accept_location = help::getCityByCityId($result[$i]['orders_data']['accept_location_id']);
                $send_location = help::getCityByCityId($result[$i]['orders_data']['send_location_id']);

                $result[$i]['accept_location_name'] = $accept_location[0]['city_name'];
                $result[$i]['send_location_name'] = $send_location[0]['city_name'];
            }
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $result = $this->_shipment->getShipment($params);
        //开始查询省市区 以及 货物 支付 方式 支付类型
        for ($i = 0; $i < count($result); $i++) {
            $provinceResult = help::getCityByCityId($result[$i]['accept_province_id']);
            $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
            $areaResult = help::getCityByCityId($result[$i]['accept_area_id']);
            $result[$i]['accept_province_name'] = $provinceResult[0]['city_name'];
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];
            $result[$i]['accept_area_name'] = $areaResult[0]['city_name'];

            $shipmentGoodsParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentGoodsResult = $this->_shipmentGoods->getShipmentGoods($shipmentGoodsParams);
            $result[$i]['shipment_goods'] = $shipmentGoodsResult;
            $shipmentCostParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentCostResult = $this->_shipmentCost->getShipmentCost($shipmentCostParams);
            $result[$i]['shipment_cost'] = $shipmentCostResult;
            $shipmentPayParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentPayResult = $this->_shipmentPay->getShipmentPay($shipmentPayParams);
            $result[$i]['shipment_pay'] = $shipmentPayResult;
        }

        $this->outPut($result);

    }

    /*删除 发运*/
    public function delShipment()
    {
        $params = $this->input();

        $result = $this->_shipment->delShipment($params);

        $this->outPut($result);

    }

    /*删除 短驳*/
    public function delShortBarge()
    {

        $params = $this->input();

        $result = $this->_shipment->delShortBarge($params);

        $this->outPut($result);

    }

    /**
     * 获取发运里的短驳
     * 胡
     */
    public function getShipmentShortBarge()
    {
        $params = $this->input();
        //开始查询省市区
		

        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_shipment->getShipmentShortBarge($params, true);
            $result = $this->_shipment->getShipmentShortBarge($params, false, 'true', $page, $page_size);
			
            for ($i = 0; $i < count($result); $i++) {
                $shipmentGoodsParams['status'] = 1;
                $shipmentGoodsParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
                $shipmentGoodsResult = $this->_shipmentShortBarge->getShipmentShortBargeInfo($shipmentGoodsParams);
                $result[$i]['shipment_goods'] = $shipmentGoodsResult;
            }
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $result = $this->_shipment->getShipmentShortBarge($params);
		
        for ($i = 0; $i < count($result); $i++) {
            $shipmentGoodsParams['status'] = 1;
            $shipmentGoodsParams['shipment_uuid'] = $result[$i]['shipment_uuid'];

            $shipmentGoodsResult = $this->_shipmentShortBarge->getShipmentShortBargeInfo($shipmentGoodsParams);
            $result[$i]['shipment_goods'] = $shipmentGoodsResult;
        }
        $this->outPut($result);	
    }

    //获取发运所有的信息

    public function getShipmentInfo()
    {
        $params = $this->input();

        //开始查询省市区
        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_shipment->getShipmentInfo($params, true);
            $result = $this->_shipment->getShipmentInfo($params, false, 'true', $page, $page_size);
	
            //开始查询省市区 以及 货物 支付 方式 支付类型
            for ($i = 0; $i < count($result); $i++) {


                $shipmentGoodsParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
				
                $shipmentGoodsResult = $this->_shipmentGoods->getShipmentGoods($shipmentGoodsParams);
                $result[$i]['shipment_goods'] = $shipmentGoodsResult;
                $shipmentCostParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
                $shipmentCostResult = $this->_shipmentCost->getShipmentCost($shipmentCostParams);
                $result[$i]['shipment_cost'] = $shipmentCostResult;
                $shipmentPayParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
                $shipmentPayResult = $this->_shipmentPay->getShipmentPay($shipmentPayParams);
                $result[$i]['shipment_pay'] = $shipmentPayResult;
                $shipmentAbnormalParams['status'] = 1;
                $shipmentAbnormalParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
                $shipmentAbnormalResult = $this->_shipmentAbnormal->getShipmentAbnormal($shipmentAbnormalParams);
                $result[$i]['shipment_abnormal'] = $shipmentAbnormalResult;
                if ($result[$i]['shipment_type'] == 3) {//代表短驳获取短驳的所有数据
                    $shipmentShortBargeParams['status'] = 1;
                    $shipmentShortBargeParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
                    $shipmentShortBargeResult = $this->_shipmentShortBarge->getShipmentShortBarge($shipmentShortBargeParams);
                    $result[$i]['shipment_short_barge_info'] = $shipmentShortBargeResult;
					$result[$i]['dfk'] = 0;
                }
		
				if($result[$i]['shipment_type']!=3){
					$ordersAbnormalParams['orders_number'] = $result[$i]['orders_number'];
					$ordersAbnormalParams['status'] = 1;
					$result[$i]['orders_abnormal'] = $this->_ordersAbnormal->getOrdersAbnormal($ordersAbnormalParams); //这里要获取到付的金额
					
					$shipmentNeedOrder['orders_number'] = $result[$i]['orders_number'];
		
					$shipmentNeedOrderResult = $this->_orders->getEasyOrder($shipmentNeedOrder);
				
					$result[$i]['dfk'] = $shipmentNeedOrderResult[0]['dfk'];
				}
				
				//开始查询到付
            }
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $result = $this->_shipment->getShipmentInfo($params);
        //开始查询省市区 以及 货物 支付 方式 支付类型
        for ($i = 0; $i < count($result); $i++) {

            $shipmentGoodsParams['shipment_uuid'] = $result[$i]['shipment_uuid'];

            $shipmentGoodsResult = $this->_shipmentGoods->getShipmentGoods($shipmentGoodsParams);
            $result[$i]['shipment_goods'] = $shipmentGoodsResult;
            $shipmentCostParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentCostResult = $this->_shipmentCost->getShipmentCost($shipmentCostParams);
            $result[$i]['shipment_cost'] = $shipmentCostResult;
            $shipmentPayParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentPayResult = $this->_shipmentPay->getShipmentPay($shipmentPayParams);
            $result[$i]['shipment_pay'] = $shipmentPayResult;
            $shipmentAbnormalParams['status'] = 1;
            $shipmentAbnormalParams['shipment_uuid'] = $result[$i]['shipment_uuid'];
            $shipmentAbnormalResult = $this->_shipmentAbnormal->getShipmentAbnormal($shipmentAbnormalParams);
            $result[$i]['shipment_abnormal'] = $shipmentAbnormalResult;
            if ($result[$i]['shipment_type'] == 3) {//代表短驳获取短驳的所有数据
                $shipmentShortBargeParams['status'] = 1;
                $shipmentShortBargeParams['shipment_uuid'] = $result[$i]['shipment_uuid'];

                $shipmentShortBargeResult = $this->_shipmentShortBarge->getShipmentShortBarge($shipmentShortBargeParams);
                $result[$i]['shipment_short_barge_info'] = $shipmentShortBargeResult;
				$result[$i]['dfk'] =0;	
            }
			if($result[$i]['shipment_type']!=3){
				$ordersAbnormalParams['orders_number'] = $result[$i]['orders_number'];
				$ordersAbnormalParams['status'] = 1;
				$result[$i]['orders_abnormal'] = $this->_ordersAbnormal->getOrdersAbnormal($ordersAbnormalParams);
				$shipmentNeedOrder['orders_number'] = $result[$i]['orders_number'];
		
				$shipmentNeedOrderResult = $this->_orders->getEasyOrder($shipmentNeedOrder);
				
				$result[$i]['dfk'] = $shipmentNeedOrderResult[0]['dfk'];				
			}			
        }

        $this->outPut($result);
    }

    /**
     * 添加发运
     * 胡
     */
    public function addShipment()
    {

        $params = $this->input();

        $paramRule = [

            'shipment_type' => 'int',
            'supplier_uuid' => 'string',

        ];
			
        $this->paramCheckRule($paramRule, $params);
	

        $customerResult = $this->_shipment->addShipment($params);

        $this->outPut($customerResult);
    }

    /**
     * 修改客户数据
     * 胡
     */
    public function updateShipment()
    {

        $params = $this->input();

        $paramRule = [

            'shipment_uuid' => 'string',

        ];
        $this->paramCheckRule($paramRule, $params);

	
        $customerResult = $this->_shipment->updateShipment($params);

        $this->outPut($customerResult);
    }

    //添加 短驳
    public function addShortBarge()
    {

        $params = $this->input();

        $paramRule = [

            'supplier_vehicle_id' => 'int',


        ];

        $this->paramCheckRule($paramRule, $params);


        $customerResult = $this->_shipment->addShortBarge($params);

        $this->outPut($customerResult);
    }

    //修改短驳
    public function updateShortBarge()
    {

        $params = $this->input();

        $paramRule = [

            'shipment_uuid' => 'string',


        ];

        $this->paramCheckRule($paramRule, $params);


        $result = $this->_shipment->updateShortBarge($params);

        $this->outPut($result);
    }

    //获取短驳
    public function getShortBarge()
    {

        $params = $this->input();


        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_shipmentShortBarge->getShipmentShortBarge($params, true);
            $result = $this->_shipmentShortBarge->getShipmentShortBarge($params, false, 'true', $page, $page_size);


            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $result = $this->_shipmentShortBarge->getShipmentShortBarge($params);

        $this->outPut($result);
    }

    //获取短驳详情
    public function getShortBargeInfo()
    {

        $params = $this->input();


        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_shipmentShortBarge->getShipmentShortBargeInfo($params, true);
            $result = $this->_shipmentShortBarge->getShipmentShortBargeInfo($params, false, 'true', $page, $page_size);


            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $result = $this->_shipmentShortBarge->getShipmentShortBargeInfo($params);

        $this->outPut($result);
    }

    //添加异常管理
    public function addShipmentAbnormal()
    {

        $params = $this->input();




        $customerResult = $this->_shipmentAbnormal->addShipmentAbnormal($params);

        $this->outPut($customerResult);
    }

    //修改异常
    public function updateShipmentAbnormal()
    {

        $params = $this->input();




        $result = $this->_shipmentAbnormal->updateShipmentAbnormal($params);

        $this->outPut($result);
    }

    //删除异常
    public function delShipmentAbnormal()
    {
        $params = $this->input();
        $paramRule = [

            'orders_abnormal_number' => 'string',


        ];

        $this->paramCheckRule($paramRule, $params);

        $result = $this->_shipmentAbnormal->delShipmentAbnormal($params);
        $this->outPut($result);
    }

    //获取发运异常
    public function getShipmentAbnormal()
    {

        $params = $this->input();


        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_shipmentAbnormal->getShipmentAbnormal($params, true);
            $result = $this->_shipmentAbnormal->getShipmentAbnormal($params, false, 'true', $page, $page_size);
			/*
			for($i=0;$i<count($result);$i++){
				if(!empty($result[$i]['orders_number'])){
					$ordersAbnormalParams['orders_number'] = $result[$i]['orders_number'];
					$ordersAbnormalParams['status'] = 1;
				
					$result[$i]['orders_abnormal'] = $this->_ordersAbnormal->getOrdersAbnormal($ordersAbnormalParams);						
				}
				
				
			}
			*/
					
			
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $result = $this->_shipmentAbnormal->getShipmentAbnormal($params);
		/*
		for($i=0;$i<count($result);$i++){
			if(!empty($result[$i]['orders_number'])){
				$ordersAbnormalParams['orders_number'] = $result[$i]['orders_number'];
				$ordersAbnormalParams['status'] = 1;
				
				$result[$i]['orders_abnormal'] = $this->_ordersAbnormal->getOrdersAbnormal($ordersAbnormalParams);						
			}		
				
		}
		*/
        $this->outPut($result);
    }

    //更新财务状态
    public function updateShipmentFinanceStatus()
    {

        $params = $this->input();

        $paramRule = [

            'abnormal_type_id' => 'int',


        ];

        $this->paramCheckRule($paramRule, $params);


        $result = $this->_shipment->updateShipmentFinanceStatus($params);

        $this->outPut($result);
    }

    /*通过shipment_uuid获取订单编号*/
    public function getShipmentShortBargeOrder()
    {
        $params = $this->input();

        $result = $this->_shipment->getShipmentEasy($params);
        if ($result[0]['shipment_type'] == 3) {//代表短驳查询短驳数据
            $shortBarge['shipment_uuid'] = $params['shipment_uuid'];
            $shortBarge['status'] = 1;
            $shortBargeResult = $this->_shipmentShortBarge->getShipmentShortBargeInfo($shortBarge);
            $result[0]['shortBargeInfo'] = $shortBargeResult;
        }
        $this->outPut($result);

    }
	
	//获取驾驶舱成本
	public function getIndexShipment(){
		$params = $this->input();
	
		$result =$this->_shipment->getIndexShipment($params);
		
		 return $this->outPut($result);
	}
	//获取驾驶舱理赔
	public function getIndexShipmentAbnormal(){
		$params = $this->input();
	
		$result =$this->_shipmentAbnormal->getIndexShipmentAbnormal($params);
		
		 return $this->outPut($result);
	}	
	
}
