<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\position\Position as ps;

class Position extends Base
{
    /**
     * 获取货区信息
     */
    public function getPosition(){

        $params = $this->input();

        //实例化模型
        $position = new ps();

        if(isset($params['page'])){
            $page_size = isset($params['page_size'])?$params['page_size']:20;
            $page = ($params['page']-1)*$page_size;
            $count = $position->getPosition($params, true);
            $result = $position->getPosition($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];
            return $this->output($data);
        }

        $positionResult = $position->getPosition($params);
        $this->outPut($positionResult);
    }

    /**
     * 添加货区
     * 韩
     */
    public function addPosition(){

        $params = $this->input();

        $paramRule = [
            'ware_id'=>'number',
            'company_id'=>'number',
            'position_name'=>'string',
            'status'=>'number'
        ];

        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $warehouse = new ps();
        $result = $warehouse->addPosition($params);

        $this->outPut($result);
    }

    /**
     * 修改货区根据货区ID
     * 韩
     */
    public function updatePositionByPositionId(){

        $params = $this->input();

        $paramRule = [
            'position_id'=>'string'
        ];
        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $position = new ps();
        $result = $position->updatePositionByPositionId($params);

        $this->outPut($result);
    }
}