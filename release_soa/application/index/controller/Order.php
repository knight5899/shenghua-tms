<?php

namespace app\index\controller;

use app\common\help\Help;
use app\index\model\despatch\Despatch;
use app\index\model\orders\AbnormalType;
use app\index\model\orders\OrderIncome;
use app\index\model\orders\Orders;
use app\index\model\orders\OrdersAbnormal;
use app\index\model\orders\OrdersFinance;
use app\index\model\orders\Ordersgoods;
use app\index\model\orders\OrdersOperating;
use app\index\model\orders\OrdersRemark;
use app\index\model\orders\OrdersUpload;
use app\index\model\shipment\Shipment as ShipmentModel;
use app\index\model\orders\OrdersCustomerFollow;
use app\index\model\source\AcceptGoods;
use app\index\model\source\Goods;
use app\index\model\source\SendGoods;
use think\config;

use app\index\service\SourceService;
use app\index\service\BranchProductService;

use app\index\service\InStationLetterService;
use think\Db;
use think\Model;
use think\Controller;
use app\common\help\Contents;
use app\index\service\PublicService;
use think\Validate;

class Order extends Base
{

    private $_orders;
    private $_ordersGoods;
	private $_ordersAbnormal;
    //_lang Base里的属性，
    public function __construct()
    {

        $this->_orders = new Orders();
		$this->_ordersAbnormal = new OrdersAbnormal();
        $this->_ordersGoods = new Ordersgoods();
        parent::__construct();
    }

    public function getUploadByOrdersId()
    {
        $params = $this->input();
        $upload_model = new OrdersUpload();
        return $this->outPut($upload_model->getUploadByOrdersId($params));
    }

    public function subFinance()
    {
        $params = $this->input();
        $ordersFinance = new OrdersFinance();
        $res = $ordersFinance->sub($params);
        return $this->outPut($res);
    }

    public function getOrderIncome()
    {

        $params = $this->input();
        $income_model = new OrderIncome();
        $res = $income_model->getinfo($params);
        return $this->outPut($res);
    }

    public function addIncome()
    {
        $params = $this->input();

        $income_model = new OrderIncome();
        $res = $income_model->add($params);
        return $this->outPut($params);
    }




    public function OrderAbnormalAdd()
    {
        $params = $this->input();
        $ordersAbnormalModel = new OrdersAbnormal();
        $res = $ordersAbnormalModel->add($params);
        return $this->outPut($res);
    }


    /**
     * 添加订单
     * 胡
     */
    public function addOrder()
    {

        $params = $this->input();

        $paramRule = [

            'project_id' => 'string',
        ];


        $this->paramCheckRule($paramRule, $params);


        $params['orders_number'] = Help::getNumber(1);
        $result = $this->_orders->addOrders($params);

        $this->outPut($result);
    }

	//获取异常订单列表
    public function getOrderAbnormal()
    {
        $orderAbnormal = new OrdersAbnormal();
        $params = $this->input();
        $result = $orderAbnormal->getAbnormalList($params);
        return $this->outPut($result);

    }
	//获取发运需要的运单异常信息
	public function getShipmentNeedOrderAbnormal(){
		$orderAbnormal = new OrdersAbnormal();
        $params = $this->input();
        $result = $orderAbnormal->getShipmentNeedOrderAbnormal($params);
        return $this->outPut($result);		
		
	}
    public function getAbnormalReview(){
        $orderAbnormal = new OrdersAbnormal();
        $params = $this->input();
        $result = $orderAbnormal->getAbnormalReview($params);
        return $this->outPut($result);
    }


    public function getAbnormalType()
    { $params = $this->input();
        $abtmodel = new AbnormalType();
       $data=$abtmodel->getAbnormalList($params);
        return $this->outPut($data);
    }

    public function getOrderIncomeList()
    {
        $params = $this->input();

        $order = new Orders();
        $income_model = new OrderIncome();
        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getOrder($params, true);

            $result = $this->_orders->getOrder($params, false, 'true', $page, $page_size);
            // return  $this->outPutError($result);
            for ($i = 0; $i < count($result); $i++) {
                $orders_id = $result[$i]['orders_id'];
                $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
                $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
                $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
                $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
                $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
                $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
                $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
                $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
                $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
                $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
                $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];

                $orders_remark_info = Db::name('orders_remark')->where(['orders_id' => $orders_id, 'type' => 3])->find();
                $result[$i]['income_remark'] = $orders_remark_info;
                $result[$i]['yun_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 1])->sum('money');
                $result[$i]['ti_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 2])->sum('money');
                $result[$i]['song_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 3])->sum('money');
                $result[$i]['xie_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 4])->sum('money');
                $result[$i]['pack_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 12])->sum('money');
                $result[$i]['other_money'] = $income_model->where('cost_id', '>', 12)->where('orders_id', $orders_id)->sum('money');

            }

            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
            // return $this->output($result,'',$count);
        }
        $result = $this->_orders->getOrder($params);

        for ($i = 0; $i < count($result); $i++) {
            $orders_id = $result[$i]['orders_id'];
            $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
            $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
            $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
            $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
            $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
            $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
            $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
            $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
            $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
            $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
            $result[$i]['yun_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 1])->sum('money');
            $result[$i]['ti_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 2])->sum('money');
            $result[$i]['song_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 3])->sum('money');
            $result[$i]['xie_money'] = $income_model->where(['orders_id' => $orders_id, 'cost_id' => 4])->sum('money');
            $result[$i]['other_money'] = $income_model->where('cost_id', '>', 4)->where('orders_id', $orders_id)->sum('money');


        }

        return $this->output($result, '');


    }

//获取备注
    public function getOrderRemark()
    {
        $params = $this->input();
        $model = new OrdersRemark();
        return $this->outPut($model->getRemark($params));
    }

    /**
     * 获取回单
     */
    public function getOrderReceipt()
    {

        $params = $this->input();

        $order = new Orders();

        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getOrder($params, true);

            $result = $this->_orders->getOrder($params, false, 'true', $page, $page_size);
            // return  $this->outPutError($result);
            for ($i = 0; $i < count($result); $i++) {
                $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
                $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
                $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
                $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
                $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
                $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
                $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
                $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
                $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
                $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
                $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
                $result[$i]['orders_receipt_remark'] = (new OrdersCustomerFollow())->getOrdersCustomerFollow($result[$i])[0];//  ->where('orders_number',$result[$i]['orders_number'])->find();

            }

            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
            // return $this->output($result,'',$count);
        }
        $result = $this->_orders->getOrder($params);

        for ($i = 0; $i < count($result); $i++) {
            $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
            $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
            $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
            $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
            $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
            $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
            $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
            $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
            $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
            $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
            $result[$i]['orders_receipt_remark'] = (new OrdersCustomerFollow())->where('orders_number',$result[$i]['orders_number'])->select();
        }

        return $this->output($result, '');


    }  /**
     * 获取订单
     */
    public function getOrder()
    {

        $params = $this->input();

        $order = new Orders();

        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getOrder($params, true);

            $result = $this->_orders->getOrder($params, false, 'true', $page, $page_size);
            // return  $this->outPutError($result);
            for ($i = 0; $i < count($result); $i++) {
                $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
                $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
                $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
                $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
                $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
                $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
                $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
                $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
                $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
                $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
                $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
            }

            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
            // return $this->output($result,'',$count);
        }
        $result = $this->_orders->getOrder($params);

        for ($i = 0; $i < count($result); $i++) {
            $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
            $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
            $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
            $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
            $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
            $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
            $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
            $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
            $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
            $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
        }

        return $this->output($result, '');


    }


    public function getSendGoodsName()
    {
        $sendModel = new SendGoods();
        $params = $this->input();
        $data = $sendModel->getSendgoodsname($params);

        return $this->output($data, '');

    }

    public function getAcceptGoodsName()
    {
        $acceptModel = new AcceptGoods();
        $params = $this->input();
        $data = $acceptModel->getAcceptgoodsname($params);

        return $this->output($data, '');
    }


    /**
     * 获取调度需要的订单
     */
    public function getShipmentNeedOrder()
    {

        $params = $this->input();

        $order = new Orders();

        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getShipmentNeedOrder($params, true);
            $result = $this->_orders->getShipmentNeedOrder($params, false, 'true', $page, $page_size);
            // return  $this->outPutError($result);
            for ($i = 0; $i < count($result); $i++) {
                $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
                $result[$i]['orders_goods_info'] = $this->_ordersGoods->getShipmentNeedOrder($orderGoodsParams);
                $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
                $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
                $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
                $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
                $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
                $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
                $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
                $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
                $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];


            }
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
        }
        $orderResult = $this->_orders->getShipmentNeedOrder($params);

        for ($i = 0; $i < count($orderResult); $i++) {
            $orderGoodsParams['orders_id'] = $orderResult[$i]['orders_id'];
            $orderResult[$i]['orders_goods_info'] = $this->_ordersGoods->getShipmentNeedOrder($orderGoodsParams);
            $cityResult = help::getCityByCityId($orderResult[$i]['send_province_id']);
            $orderResult[$i]['send_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($orderResult[$i]['send_city_id']);
            $orderResult[$i]['send_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($orderResult[$i]['send_area_id']);
            $orderResult[$i]['send_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($orderResult[$i]['accept_province_id']);
            $orderResult[$i]['accept_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($orderResult[$i]['accept_city_id']);
            $orderResult[$i]['accept_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($orderResult[$i]['accept_area_id']);
            $orderResult[$i]['accept_area_name'] = $cityResult[0]['city_name'];
            $cityResult = help::getCityByCityId($orderResult[$i]['send_location_id']);
            $orderResult[$i]['send_location_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($orderResult[$i]['accept_location_id']);
            $orderResult[$i]['accept_location_name'] = $cityResult[0]['city_name'];
        }


        $this->outPut($orderResult);


    }

    /**
     * 获取短驳需要的订单
     */
    public function getShortBrageNeedOrder()
    {

        $params = $this->input();

        $order = new Orders();
        $shipment = new ShipmentModel();
        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getShipmentNeedOrder($params, true);
            $result = $this->_orders->getShipmentNeedOrder($params, false, 'true', $page, $page_size);
            // return  $this->outPutError($result);
            for ($i = 0; $i < count($result); $i++) {
                $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
                $result[$i]['orders_goods_info'] = $this->_ordersGoods->getShipmentNeedOrder($orderGoodsParams);
                $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
                $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
                $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
                $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
                $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
                $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
                $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
                $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
                $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
                $shipmentParams['status'] = 1;
                $shipmentParams['orders_number'] = $result[$i]['orders_number'];

                $result[$i]['shipmentInfo'] = $shipment->getShipment($shipmentParams);

            }
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
        }
        $result = $this->_orders->getShipmentNeedOrder($params);

        for ($i = 0; $i < count($result); $i++) {
            $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
            $result[$i]['orders_goods_info'] = $this->_ordersGoods->getShipmentNeedOrder($orderGoodsParams);
            $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
            $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
            $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
            $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
            $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
            $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];
            $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
            $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
            $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
            $shipmentParams['status'] = 1;
            $shipmentParams['orders_number'] = $result[$i]['orders_number'];
            $result[$i]['shipmentInfo'] = $shipment->getShipment($shipmentParams);
        }


        $this->outPut($result);


    }

    public function getAbnormalUpload()
    {
        $params = $this->input();
        $model = new OrdersAbnormal();
        $data = $model->getAbnormalByid($params);
        return $this->outPut($data);
    }

    public function getReceipt()
    {
        $params = $this->input();
        $validate = new Validate(['orders_id' => 'number|require']);
        if (!$validate->check($params))
            return $this->outPutError($validate->getError());
        $data = $this->_orders->getReceipt($params);
        return $this->outPut($data);
    }

    public function addReceipt()
    {
        $params = $this->input();
        $validate = new Validate(['orders_id' => 'number|require']);

        if (!$validate->check($params))
            return $this->outPutError($validate->getError());

        $this->_orders->addReceipt($params);

        return $this->outPut('success');
    }

    public function updateOrderStatus()
    {
        $params = $this->input();
        $this->_orders->updateStatus($params);
        return $this->outPut('success');
    }


    /**
     * 修改订单根据订单ID
     * 胡
     */
    public function updateOrderByOrderId()
    {

        $params = $this->input();

        $paramRule = [

            'orders_id' => 'string',
            'project_id' => "string"


        ];
        $this->paramCheckRule($paramRule, $params);


        $result = $this->_orders->updateOrderByOrderId($params);
        // $this->outPutError(json_encode($result,256));
        $this->outPut($result);
    }

    /**
     * 修改发运跟踪根据订单ID
     * 胡
     */
    public function updateOrderFollow()
    {

        $params = $this->input();

        $paramRule = [

            'orders_id' => 'string',


        ];
        $this->paramCheckRule($paramRule, $params);


        $result = $this->_orders->updateOrders($params);

        $this->outPut($result);
    }

    /**
     * 修改运单根据运单编号
     * 胡
     */
    public function updateOrderByOrdersNumber()
    {

        $params = $this->input();

        $paramRule = [

            'orders_number' => 'string',


        ];
        $this->paramCheckRule($paramRule, $params);


        $result = $this->_orders->updateOrdersByOrdersNumber($params);

        $this->outPut($result);
    }

    public function getOrderStatusList()
    {
        $params = $this->input();
        $data = $this->_orders->getUpdateStatusList($params);
        return $data;
    }

    public function changeShipmentNumber()
    {
        $params = $this->input();

        $data = $this->_orders->changeShipmentNumber($params);
        return $data;
    }

    /**
     * 获取订单货物
     */
    public function getOrderGoods()
    {

        $params = $this->input();


        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_ordersGoods->getOrdersGoods($params, true);
            $result = $this->_ordersGoods->getOrdersGoods($params, false, 'true', $page, $page_size);


            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
        }
        $result = $this->_ordersGoods->getOrdersGoods($params);

        $this->outPut($result);


    }

    public function getPrintOrderInfo(){
        $params = $this->input();
        $result = $this->_orders->getOrder($params);
        for ($i = 0; $i < count($result); $i++) {
            $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
            $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
            for ($j = 0; $j < count($result[$i]['orders_goods_info']); $j++) {
                $result[$i]['orders_goods_info'][$j]['goods_config']=(new Goods())->getGoods(['goods_id'=>$result[$i]['orders_goods_info'][$j]['goods_id']])[0];


            }


            $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
            $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
            $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
            $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
            $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
            $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
            $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
            $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
        }
return $this->outPut($result);
    }

    public function getOrdersInfo()
    {

        $params = $this->input();

        $orders = new Orders();
        $ordersGoods = new Ordersgoods();

        //获取相关运单信息
        $result = $orders->getOrder($params);

        $project_data['project_id'] = $result[0]['project_id'];
        $project_data['orders_id'] = $result[0]['orders_id'];
        $project_data['status'] = 1;

        //获取货物信息
        $ordersGoodsResult = $ordersGoods->getOrdersGoods($project_data);

        //计算合计
        $numbers = 0;
        $weights = 0;
        $volumes = 0;
        foreach ($ordersGoodsResult as $k1 => $v1) {
            $numbers += $v1['realy_count']; //合计数量
            $weights += number_format($v1['realy_weight'], 2, '.', ''); //合计重量
            $volumes += number_format($v1['realy_volume'], 3, '.', ''); //合计体积
        }

        $this->outPut($ordersGoodsResult);

    }

    /**
     * 获取客户跟踪
     * 胡
     */
    public function getOrderCustomerFollow()
    {
		$params = $this->input();
        $ordersOperating = new OrdersOperating();


        $result = $ordersOperating->getFollowInfo($params);

        $this->outPut($result);
    }
    public function getReceiptInfo(){
        $params = $this->input();

        $result =  (new OrdersCustomerFollow())->getOrdersCustomerFollow($params);// OrdersCustomerFollow::getOrdersCustomerFollow($params);

        $this->outPut($result);
    }



    /**
     * 修改客户跟踪
     * 胡
     */
    public function updateOrderCustomerFollow()
    {
        $orderCustomerFollow = new OrdersCustomerFollow();
        $params = $this->input();

        $paramRule = [

            'orders_customer_follow_id' => 'int',
        ];
        $this->paramCheckRule($paramRule, $params);


        $result = $orderCustomerFollow->updateOrdersCustomerFollow($params);

        $this->outPut($result);
    }

    /**
     * 添加客户跟踪
     * 胡
     */
    public function addOrderCustomerFollow()
    {
        $orderCustomerFollow = new OrdersCustomerFollow();
        $params = $this->input();

        $paramRule = [

            'orders_number' => 'string',
        ];
        $this->paramCheckRule($paramRule, $params);


        $result = $orderCustomerFollow->addOrdersCustomerFollow($params);

        $this->outPut($result);
    }

    public function incomeAccountingImport()
    {
        $params = $this->input();
        //$this->outPut(['success'=>json_encode($params)]);
        $model = new OrderIncome();
        $outres = ['success' => "", 'error' => ""];
        $res = $model->incomeAccountingImport($params['excel_data'], $params['user_id']);
        if ($res)
            $outres['error'] = $res;
        else
            $outres['success'] = "导入成功";

        $this->outPut($outres);
    }

    //应收配比表
    public function peibi()
    {

        $params = $this->input();

        $order = new Orders();

        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getPeibi($params, true);
            $result = $this->_orders->getPeibi($params, false, 'true', $page, $page_size);
            // return  $this->outPutError($result);

            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
        }
        $orderResult = $this->_orders->getPeibi($params);


        $this->outPut($orderResult);


    }


    public function changeAbnormalDescribe()
    {
        $params = $this->input();

        $order = new Orders();
        $res = $order->changeAbnormalDescribe($params);
        return $this->outPut($res);
    }

    public function changeOperating()
    {
        $params = $this->input();
        $operating_model = new OrdersOperating();
        $operating_model->updateContent($params);
    }
	
	//获取驾驶舱运单收入
	public function getIndexOrder(){
		$params = $this->input();
	
		$result = $this->_orders->getIndexOrder($params);
		
		 return $this->outPut($result);
	}
	//获取驾驶舱运单理赔
	public function getIndexOrderAbnormal(){
		$params = $this->input();
	
		$result =$this->_ordersAbnormal->getIndexOrderAbnormal($params);
		
		 return $this->outPut($result);
	}
	public function orderTrackEdit(){
        $params = $this->input();
        $result = $this->_orders->orderTrackEdit($params);
        return $this->outPut($result);
    }
    public function orderUploadDel(){
        $params = $this->input();
        $upload_model=new OrdersUpload();
        $result=$upload_model->delUploadById($params);
        return $this->outPut($result);
    }

    public function delOrder(){
        $params = $this->input();
        $result = $this->_orders->delOrder($params);
        return $this->outPut($result);
    }
    public function getdelOrder(){
        $params = $this->input();
        $order = new Orders();
        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getOrder($params, true);

            $result = $this->_orders->getOrder($params, false, 'true', $page, $page_size);
            // return  $this->outPutError($result);
            for ($i = 0; $i < count($result); $i++) {
                $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
                $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
                $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
                $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
                $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
                $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
                $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
                $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
                $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
                $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

                $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
                $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
                $remark_info=(new OrdersRemark())->where(['type'=>4,'orders_id'=>$result[$i]['orders_id']])->find();
                $result[$i]['del_remark'] =$remark_info?$remark_info->content:"";
            }

            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
            // return $this->output($result,'',$count);
        }
        $result = $this->_orders->getOrder($params);

        for ($i = 0; $i < count($result); $i++) {
            $orderGoodsParams['orders_id'] = $result[$i]['orders_id'];
            $result[$i]['orders_goods_info'] = $this->_ordersGoods->getOrdersGoods($orderGoodsParams);
            $cityResult = help::getCityByCityId($result[$i]['send_province_id']);
            $result[$i]['send_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_city_id']);
            $result[$i]['send_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_area_id']);
            $result[$i]['send_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_province_id']);
            $result[$i]['accept_province_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_city_id']);
            $result[$i]['accept_city_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_area_id']);
            $result[$i]['accept_area_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['send_location_id']);
            $result[$i]['send_location_name'] = $cityResult[0]['city_name'];

            $cityResult = help::getCityByCityId($result[$i]['accept_location_id']);
            $result[$i]['accept_location_name'] = $cityResult[0]['city_name'];
            $remark_info=(new OrdersRemark())->where(['type'=>4,'orders_id'=>$result[$i]['orders_id']])->find();
            $result[$i]['del_remark'] =$remark_info?$remark_info->content:"";
        }

        return $this->output($result, '');

    }
}
