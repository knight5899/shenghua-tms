<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\warehouse\Warehouse as wh;
//监测系统
class Monitor extends Base
{
	
	/**
	 * 获取温湿度点位数据
	 */
	
    /**
     * 获取海康温湿度数据
     */
    public function getTh(){

        $params = $this->input();

        //实例化模型
        $warehouse = new wh();

        if(isset($params['page'])){
            $page_size = isset($params['page_size'])?$params['page_size']:20;
            $page = ($params['page']-1)*$page_size;
            $count = $warehouse->getWarehouse($params, true);
            $result = $warehouse->getWarehouse($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];
            return $this->output($data);
        }

        $warehouseResult = $warehouse->getWarehouse($params);
        $this->outPut($warehouseResult);
    }

   
}