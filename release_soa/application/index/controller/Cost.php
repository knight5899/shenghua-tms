<?php


namespace app\index\controller;


class Cost extends Base
{
public function getAll(){
    $cost_model=new \app\index\model\finance\Cost();

  return  $this->outPut($cost_model->getAll());
}

    public function getCostIdToCostName(){
        $params = $this->input();

        $cost_model=new \app\index\model\finance\Cost();
        $costResult = $cost_model->getCostIdToCostName($params);

        $this->outPut($costResult);
    }
}