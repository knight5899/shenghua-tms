<?php


namespace app\index\controller;

use app\index\controller\Base;
class Dispatchfee extends Base
{

    public function getDispatchfee()
    {
        $dispatchfee=new \app\index\model\dispatch\DispatchFee();

        $params = $this->input();
        $orderResult = $dispatchfee->getFeeList($params, true);//get->getDispatch($params, true);
        $this->outPut($orderResult);
    }
}