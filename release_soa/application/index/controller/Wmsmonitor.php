<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\wmsmonitor\THPoint as thp;
use app\index\model\wmsmonitor\THCalc as thc;
use app\index\model\wmsmonitor\TH as th;

class Wmsmonitor extends Base
{
    /**
     * 获取温湿度点位信息
     */
    public function getTHPoint(){

        $params = $this->input();

        //实例化模型
        $thpoint = new thp();

        if(isset($params['page'])){
            $page_size = isset($params['page_size'])?$params['page_size']:20;
            $page = ($params['page']-1)*$page_size;
            $count = $thpoint->getTHPoint($params, true);
            $result = $thpoint->getTHPoint($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];
            return $this->output($data);
        }

        $result = $thpoint->getTHPoint($params);
        $this->outPut($result);
    }

    /**
     * 添加温湿度点位
     * 韩
     */
    public function addTHPoint(){

        $params = $this->input();

        $paramRule = [
            'position_id'=>'number',
            'th_point_name'=>'number',
            'remark'=>'string',
            'user_id'=>'number',
            'status'=>'number'
        ];

        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $THPoint = new thp();
        $result = $THPoint->addTHPoint($params);

        $this->outPut($result);
    }

    /**
     * 修改温湿度点位根据温湿度点位ID
     * 韩
     */
    public function updateTHPointByTHPointId(){

        $params = $this->input();

        $paramRule = [
            'wms_th_point_uuid'=>'string'
        ];
        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $thpoint = new thp();
        $result = $thpoint->updateTHPointByTHPointId($params);

        $this->outPut($result);
    }

    /**
     * 获取温湿度数据信息
     */
    public function getTH(){

        $params = $this->input();

        //实例化模型
        $th = new th();

        if(isset($params['page'])){
            $page_size = isset($params['page_size'])?$params['page_size']:20;
            $page = ($params['page']-1)*$page_size;
            $count = $th->getTH($params, true);
            $result = $th->getTH($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];
            return $this->output($data);
        }

        $result = $th->getTH($params);
        $this->outPut($result);
    }

    /**
     * 添加温湿度数据
     * 韩
     */
    public function addTH(){

        $params = $this->input();

        $paramRule = [

        ];

//        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $th = new th();
        $result = $th->addTH($params);

        $this->outPut($result);

    }

    /**
     * 添加温湿度数据发送人
     * 韩
     */
    public function addTHSendUser(){

        $params = $this->input();

        $paramRule = [

        ];

        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $THSendUser = new thc();
        $result = $THSendUser->addTHSendUser($params);

        $this->outPut($result);
    }
}