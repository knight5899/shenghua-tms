<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\orders\OrdersFinance as of;
use app\index\model\bill\CustomerBill as ob;
use app\index\model\bill\SupplierBill as sb;
use app\index\model\bill\SupplierBillInfo;
use think\Model;
class Bill extends Base
{
	
	private $_customerBill;
	private $_supplierBillInfo;
	public function __construct()
	{

		$this->_customerBill = new ob();
		$this->_supplierBillInfo = new SupplierBillInfo();
		parent::__construct();
	}

    /**
     * 获取客户对账数据
     */
    public function getCustomerBill(){

        $params = $this->input();

        //实例化模型
        $customerBill = new of();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerBill->getOrdersFinance($params, true);
            $result = $customerBill->getOrdersFinance($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $dataResult = $customerBill->getOrdersFinance($params);
        $this->outPut($dataResult);
    }
	//获取客户赔款金额
	public function getCustomerBillMoneyInfo(){
		
		$params = $this->input();
		$customerBill = new ob;
		$dataResult = $customerBill->getCustomerBillMoney($params);
        $this->outPut($dataResult);
	}
	//获取承运商赔款金额
	public function getSupplierBillMoneyInfo(){
		
		$params = $this->input();
		$supplierBill = new sb;
		$dataResult = $supplierBill->getSupplierBillMoney($params);
        $this->outPut($dataResult);
	}

    public function getCustomerBillOnlly(){

        $params = $this->input();

        //实例化模型
        $customerYesBill = new ob();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerYesBill->getCustomerBillOnly($params, true);
            $result = $customerYesBill->getCustomerBillOnly($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }

        $dataResult = $customerYesBill->getCustomerBillOnly($params);
        $this->outPut($dataResult);

    }

    public function getCustomerBillInfoOnlly(){

        $params = $this->input();

        //实例化模型
        $customerYesBill = new ob();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerYesBill->getCustomerYesBill3($params, true);
            $result = $customerYesBill->getCustomerYesBill3($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }

        $dataResult = $customerYesBill->getCustomerYesBill3($params);
        $this->outPut($dataResult);

    }

    public function getCustomerBillInfo(){

        $params = $this->input();

        //实例化模型
        $customerYesBill = new ob();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerYesBill->getCustomerBill($params, true);
            $result = $customerYesBill->getCustomerBill($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }

        $dataResult = $customerYesBill->getCustomerBill($params);
        $this->outPut($dataResult);

    }

    public function getCustomerYesBill2(){

        $params = $this->input();

        //实例化模型
        $customerYesBill = new ob();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerYesBill->getCustomerYesBill2($params, true);
            $result = $customerYesBill->getCustomerYesBill2($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }

        $dataResult = $customerYesBill->getCustomerYesBill2($params);
        $this->outPut($dataResult);

    }

    public function getCustomerYesBill3(){

        $params = $this->input();

        //实例化模型
        $customerYesBill = new ob();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerYesBill->getCustomerYesBill3($params, true);
            $result = $customerYesBill->getCustomerYesBill3($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }

        $dataResult = $customerYesBill->getCustomerYesBill3($params);
        $this->outPut($dataResult);

    }

    public function getCustomerYesBill(){

        $params = $this->input();

        //实例化模型
        $customerYesBill = new ob();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerYesBill->getCustomerYesBill($params, true);
            $result = $customerYesBill->getCustomerYesBill($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }

        $dataResult = $customerYesBill->getCustomerYesBill($params);
        $this->outPut($dataResult);

    }

    public function getCustomerBillInData(){
	
        $params = $this->input();

        unset($params["appKey"]);
        unset($params['appSecret']);
        unset($params['user_id']);

        //实例化模型
        $customerBill = new of();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerBill->getCustomerBillInData($params, true);
            $result = $customerBill->getCustomerBillInData($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $dataResult = $customerBill->getCustomerBillInData($params);
        $this->outPut($dataResult);

    }

    public function getCustomerYesBillInData(){

        $params = $this->input();

        //实例化模型
        $customerBill = new of();

        $dataResult = $customerBill->getCustomerYesBillInData($params);
        $this->outPut($dataResult);

    }

    public function addCustomerBill(){

        $params = $this->input();

        unset($params["appKey"]);
        unset($params['appSecret']);
        unset($params['user_id']);

        $paramRule = [

        ];

        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $thpoint = new ob();
        $result = $thpoint->addCustomerBill($params);

        $this->outPut($result);

    }

    public function editCustomerInvoiceBill(){
        $params = $this->input();
        $result = $this->_customerBill->editCustomerInvoiceBill($params);

        $this->outPut($result);

    }

    public function updateCustomerBill(){
    	$params = $this->input();
    	$result = $this->_customerBill->updateCustomerBill($params);
    	 
    	
    	 
    	$this->outPut($result);    	
    	
    }

    /**
     * 承运商对账数据
     */
    public function getSupplierBill(){

        $params = $this->input();

        //实例化模型
        $supplierBill = new sb();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $supplierBill->getSupplierBill($params, true);
            $result = $supplierBill->getSupplierBill($params, false, 'true', $page, $page_size);
            for($i=0;$i<count($result);$i++){
            	$supplierBillInfoParams['supplier_bill_number'] = $result[$i]['supplier_bill_number'];
            	$supplierBillInfoResult = $this->_supplierBillInfo->getSupplierBillInfo($supplierBillInfoParams);
            	$result[$i]['supplier_bill_info_array'] = $supplierBillInfoResult;
            }
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $result = $supplierBill->getSupplierBill($params);
        for($i=0;$i<count($result);$i++){
        	$supplierBillInfoParams['supplier_bill_number'] = $result[$i]['supplier_bill_number'];
        	$supplierBillInfoResult = $this->_supplierBillInfo->getSupplierBillInfo($supplierBillInfoParams);
        	$result[$i]['supplier_bill_info_array'] = $supplierBillInfoResult;
        }
        $this->outPut($result);
    }
    /**
     * 承运商对账数据
     */
    public function getSupplierBillInfo(){
    
    	$params = $this->input();

    
    	if(isset($params['page'])) {
    		$page_size = isset($params['page_size']) ? $params['page_size'] : 20;
    		$page = ($params['page'] - 1) * $page_size;
    		$count = $this->_supplierBillInfo->getSupplierBillInfo($params, true);
    		$result = $this->_supplierBillInfo->getSupplierBillInfo($params, false, 'true', $page, $page_size);
    		$data = [
    				'count' => $count,
    				'list' => $result,
    				'page_count' => ceil($count / $page_size)
    		];
    		return $this->output($data);
    	}
    	$dataResult = $this->_supplierBillInfo->getSupplierBillInfo($params);
    	$this->outPut($dataResult);
    }
    public function addSupplierBill(){

        $params = $this->input();

        $paramRule = [

        ];

        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $supplierBill = new sb();
        $result = $supplierBill->addSupplierBill($params);

        $this->outPut($result);

    }

    public function updateSupplierBill(){
    
    	$params = $this->input();
    
    	$paramRule = [
    
    	];
    
    	$this->paramCheckRule($paramRule,$params);
    
    	//实例化模型
    	$supplierBill = new sb();
    	$result = $supplierBill->updateSupplierBill($params);
    
    	$this->outPut($result);
    
    }
    
}