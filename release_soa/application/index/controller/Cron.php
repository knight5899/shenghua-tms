<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\cron\Cron as cr;

class Cron extends Base
{
    /**
     * 获取定时任务信息
     */
    public function getCron(){

        $params = $this->input();

        $data['method'] = $params['method'];
        $data['channel'] = $params['channel'];
        $data['status'] = $params['status'];

        $cron = new cr();
        $res = $cron->where($data)->find();
        $this->outPut($res);
    }

}