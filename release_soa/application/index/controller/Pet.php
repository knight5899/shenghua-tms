<?php
namespace app\index\controller;

use app\common\help\Help;
use app\index\controller\Base;
use app\index\model\pet_article\PetArticleType;
use app\index\model\pet_article\PetArticle;
use app\common\help\Contents;
use think\Model;
use think\Controller;

class Pet extends Base
{
	private $_language;
    //_lang Base里的属性，
    public function __construct()
    {

        parent::__construct();
    }

    /**
     * addOtaArticle
     *
     * 添加文章
     * @author shj
     * @return void
     * Date: 2019/5/15vcvc
     * Time: 17:05
     */
    public function addOtaArticle(){
        $params = $this->input();

        $model = new Article();

        $data = [
            'title'=>$params['title'],
            'website_uuid'=>$params['website_uuid']
        ];
        $this->checkNameIsRepetition('ota_article',$data);


        $paramRule = [
            'website_uuid' => 'string',
            'title'=>'string',
            'content'=>'string',
        ];
        $this->paramCheckRule($paramRule,$params);


        $result = $model->addOtaArticle($params);

        $this->outPut($result);
    }

    /**
     * getOtaArticle
     *
     * 获取文章详情
     * @author shj
     * @return void
     * Date: 2019/5/15
     * Time: 17:05
     */
    public function getOtaArticle(){
        $params = $this->input();

        $model = new Article();

        $result = $model->getOneArticle($params);

        $this->outPut($result);
    }

    /**
     * getArticleList
     *
     * 获取文章列表
     * @author shj
     * @return void
     * Date: 2019/5/15
     * Time: 17:05
     */
    public function getArticleList(){
        $params = $this->input();

        $model = new Article();

        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $model->getArticleList($params, true);
            $result = $model->getArticleList($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];

            return $this->output($data);
        }

        $result = $model->getArticleList($params);

        $this->outPut($result);
    }

    /**
     * updateOtaArticle
     *
     * 修改文章
     * @author shj
     * @return void
     * Date: 2019/5/15
     * Time: 17:06
     */
    public function updateOtaArticle(){
        $params = $this->input();

        $model = new Article();

        $where['ota_article_id'] = $params['ota_article_id'];
        $articleInfo = $model->getOneArticle($where);
        //重复性验证
        if($articleInfo['title'] == $params['title']){
        }else{
            //开始判断名字是否重复
            $data = [
                'title'=>$params['title'],
                'website_uuid'=>$articleInfo['website_uuid']
            ];
            $this->checkNameIsRepetition('ota_article',$data);
            //结束判断名字重复
        }
        $paramRule = [
            'title'=>'string',
            'content'=>'string',
        ];
        $this->paramCheckRule($paramRule,$params);
        $result = $model->updateArticleById($params);

        $this->outPut($result);
    }

    /**
     *
     * 添加文章分类
     * @author hym
     */
    public function addPetArticleType(){
    	

        $params = $this->input();

        $model = new PetArticleType();


        $data = [
            'pet_article_type_name'=>$params['pet_article_type_name'],
 
        ];
        $this->checkNameIsRepetition('pet_article_type',$data);


        $paramRule = [
       
            'pet_article_type_name'=>'string',
        ];
        $this->paramCheckRule($paramRule,$params);


        $result = $model->addPetArticleType($params);

        $this->outPut($result);
    }

    /**
     * 获取文章分类
     * @author hym
     * @return void
     */
    public function getPetArticleType(){

        $params = $this->input();

        $model = new PetArticleType();
        if(isset($params['page'])){
        	$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
        	$page = ($params['page']-1)*$page_size;
        	$count = $model->getPetArticleType($params, true);
        	$result = $model->getPetArticleType($params,false,'true',$page,$page_size);
        	$data = [
        			'count'=>$count,
        			'list'=>$result,
        			'page_count'=>ceil($count/$page_size)
        	];
        
        	return $this->output($data);
        }
        
        $result = $model->getPetArticleType($params);

        $this->outPut($result);
    }



    /**
     *
     * 修改文章分类
     * @author hym
     * @return void
     */
    public function updatePetArticleTypeByPetArticleTypeId(){
        $params = $this->input();

        $model = new PetArticleType();

        $where['pet_article_type_id'] = $params['pet_article_type_id'];
        $Info = $model->getPetArticleType($where);
        //重复性验证
        if($Info[0]['pet_article_type_name'] == $params['pet_article_type_name']){
        }else{
            //开始判断名字是否重复
            $data = [
                'pet_article_type_name'=>$params['pet_article_type_name'],
           
            ];
            $this->checkNameIsRepetition('pet_article_type',$data);
            //结束判断名字重复
        }


        $paramRule = [
            'pet_article_type_name'=>'string',
        ];
        $this->paramCheckRule($paramRule,$params);


        $result = $model->updatePetArticleTypeByPetArticleTypeId($params);

        $this->outPut($result);
    }
    /**
     *
     * 添加文章
     * @author hym
     */
    public function addPetArticle(){
    	 
    
    	$params = $this->input();
    
    	$model = new PetArticle();
    
    

    
    	$paramRule = [
    			 
    			'pet_article_name'=>'string',
    	];
    	$this->paramCheckRule($paramRule,$params);
    
    
    	$result = $model->addPetArticle($params);
    
    	$this->outPut($result);
    }
    /**
     * 获取文章
     * @author hym
     * @return void
     */
    public function getPetArticle(){
    
    	$params = $this->input();
    
    	$model = new PetArticle();
    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $model->getPetArticle($params, true);
    		$result = $model->getPetArticle($params,false,'true',$page,$page_size);
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    
    		return $this->output($data);
    	}
    
    	$result = $model->getPetArticle($params);
    
    	$this->outPut($result);
    }    
    /**
     *
     * 修改文章
     * @author hym
     * @return void
     */
    public function updatePetArticleByPetArticleId(){
    	$params = $this->input();
    
    	$model = new PetArticle();
    
    	$where['pet_article_id'] = $params['pet_article_id'];
    	$Info = $model->getPetArticle($where);
    	//重复性验证
    	if($Info[0]['pet_article_name'] == $params['pet_article_name']){
    	}else{
    		//开始判断名字是否重复
    		$data = [
    				'pet_article_name'=>$params['pet_article_name'],
    				 
    		];
    		$this->checkNameIsRepetition('pet_article',$data);
    		//结束判断名字重复
    	}
    
    
    	$paramRule = [
    			'pet_article_name'=>'string',
    	];
    	$this->paramCheckRule($paramRule,$params);
    
  
    	$result = $model->updatePetArticleByPetArticleId($params);
    
    	$this->outPut($result);
    }    
}
