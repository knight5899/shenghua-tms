<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\cron\Senduser as su;

class Senduser extends Base
{
    /**
     * 获取定时任务信息
     */
    public function getSendUser(){

        $params = $this->input();

        $data['status'] = $params['status'];

        $cron = new su();
        $res = $cron->where($data)->select();
        $this->outPut($res);
    }

}