<?php
namespace app\index\controller;
use app\common\help\Help;
use app\index\model\turnover\Turnoverbox;

use think\config;


use think\Model;
use think\Controller;
use app\common\help\Contents;
use app\index\service\PublicService;
use think\Validate;

class Turnover extends Base
{



	private $_deviceBoxLog;
	private $_deviceBox;
    //_lang Base里的属性，
    public function __construct()
    {
   
    	$this->_deviceBoxLog = new Turnoverbox();

        parent::__construct();
    }
    

    /**
     * 获取订单
     */
    public function getTurnoverBoxLog(){

    	$params = $this->input();
    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_deviceBoxLog->getTurnoverBoxLog($params, true);
    		$result = $this->_deviceBoxLog->getTurnoverBoxLog($params,false,'true',$page,$page_size);
    		

    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    	
    		return $this->output($data);
    	}
    	$result=  $this->_deviceBoxLog->getTurnoverBoxLog($params);
    	$this->outPut($result);    	
    }
    
    public function getTurnoverBox(){
    	$params = $this->input();
    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_deviceBoxLog->getTurnoverBox($params, true);
    		$result = $this->_deviceBoxLog->getTurnoverBox($params,false,'true',$page,$page_size);
    	
    	
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    		 
    		return $this->output($data);
    	}
    	$result=  $this->_deviceBoxLog->getTurnoverBox($params);
    	$this->outPut($result);
    	
    }
}
