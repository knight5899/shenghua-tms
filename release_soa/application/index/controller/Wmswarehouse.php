<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\wmswarehouse\Warehouse as wwh;
use app\index\model\wmswarehouse\Region as wr;

class Wmswarehouse extends Base
{
    /**
     * 获取仓库信息
     */
    public function getWarehouse(){

        $params = $this->input();

        //实例化模型
        $warehouse = new wwh();

        if(isset($params['page'])){
            $page_size = isset($params['page_size'])?$params['page_size']:20;
            $page = ($params['page']-1)*$page_size;
            $count = $warehouse->getWarehouse($params, true);
            $result = $warehouse->getWarehouse($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];
            return $this->output($data);
        }

        $warehouseResult = $warehouse->getWarehouse($params);
        $this->outPut($warehouseResult);
    }

    /**
     * 添加仓库
     * 韩
     */
    public function addWarehouse(){

        $params = $this->input();

        $paramRule = [
            'company_id'=>'number',
            'ware_name'=>'string',
            'ware_province_id'=>'number',
            'ware_city_id'=>'number',
            'ware_area_id'=>'number',
            'ware_address'=>'string',
            'contacts_id'=>'number',
            'status'=>'number'
        ];

        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $warehouse = new wh();
        $result = $warehouse->addWarehouse($params);

        $this->outPut($result);
    }

    /**
     * 修改仓库根据仓库ID
     * 韩
     */
    public function updateWarehouseByWarehouseId(){

        $params = $this->input();

        $paramRule = [
            'ware_id'=>'string'
        ];
        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $warehouse = new wh();
        $result = $warehouse->updateWarehouseByWarehouseId($params);

        $this->outPut($result);
    }

    /**
     * 获取仓库区域信息
     */
    public function getRegion(){

        $params = $this->input();

        //实例化模型
        $warehouse = new wr();

        if(isset($params['page'])){
            $page_size = isset($params['page_size'])?$params['page_size']:20;
            $page = ($params['page']-1)*$page_size;
            $count = $warehouse->getRegion($params, true);
            $result = $warehouse->getRegion($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];
            return $this->output($data);
        }

        $warehouseResult = $warehouse->getRegion($params);
        $this->outPut($warehouseResult);
    }
}