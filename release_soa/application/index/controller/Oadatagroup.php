<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\oadata\OaDataGroup as oadg;

class OadataGroup extends Base
{
    /**
     * 获取资料信息
     */
    public function getDataGroup(){

        $params = $this->input();

        //实例化模型
        $oadatagroup = new oadg();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $oadatagroup->getOaDataGroup($params, true);
            $result = $oadatagroup->getOaDataGroup($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }
        $dataResult = $oadatagroup->getOaDataGroup($params);
        $this->outPut($dataResult);
    }


    /**
     * 获取資料分组信息
     */
    public function getDataGroupAjax(){

        $params = $this->input();

        //实例化模型
        $oadatagroup = new oadg();

        $warehouseResult = $oadatagroup->getOaDataGroupAjax($params);
        $this->outPut($warehouseResult);
    }


    /**
     *  添加资料
     */
    public function addDataGroup(){

        $params = $this->input();

        $paramRule = [
            'level_id'=>'number',
            'father_id'=>'number',
            'group_name'=>'string'
        ];

        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $oadatagroup = new oadg();
        $result = $oadatagroup->addDataGroup($params);

        $this->outPut($result);
    }

    /**
     * 修改资料根据资料ID
     * 韩
     */
    public function updateDataGroupByGroupId(){

        $params = $this->input();

        $paramRule = [
            'group_id'=>'number'
        ];
        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $oadatagroup = new oadg();
        $result = $oadatagroup->updateDataGroupByGroupId($params);

        $this->outPut($result);
    }
}