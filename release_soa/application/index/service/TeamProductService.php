<?php
namespace app\index\service;


use app\index\model\branchcompany\CompanyOrder;
use app\index\model\branchcompany\CompanyOrderCustomer;
use app\index\model\branchcompany\CompanyOrderProduct;
use app\index\model\branchcompany\CompanyOrderProductTeam;
use app\index\model\branchcompany\CompanyOrderDiy;
use app\index\model\branchcompany\CompanyOrderProductSource;
use app\index\model\branchcompany\CompanyOrderRelation;
use app\index\model\branchcompany\CompanyOrderProductDiy;
use app\index\model\finance\Receivable;
use app\index\model\finance\ReceivableCustomer;
use app\index\model\finance\Cope;
use app\index\model\product\TeamProduct;
use app\index\model\product\TeamProductAllocation;
use app\index\model\product\RouteTemplate;
use app\index\model\source\TourGuide;
use app\index\model\source\Vehicle;
use app\index\model\source\ScenicSpot;
use app\index\model\source\Visa;
use app\index\model\source\Cruise;
use app\index\model\source\Flight;
use app\index\model\source\Dining;
use app\index\model\source\Hotel;
use app\index\model\source\SingleSource;
use app\index\model\source\OwnExpense;

use think\Model;
use app\common\help\Help;
use think\Hook;

class TeamProductService{
	
	
	private $_company_order_product;
	private $_company_order_customer;
	private $_company_order_product_team;
	private $_company_order_product_source;
	private $_company_order_product_diy;
	private $_hotel;
	private $_team_product;
	private $_team_product_allocation;
	public function __construct(){
		
		$this->_team_product = new TeamProduct();
		$this->_team_product_allocation = new TeamProductAllocation();
	}

	
	
	//通过 传递来的团队产品ID变更团队产品编号
	public function updateTeamProductNumberByTeamProductId($params){
		$team_product_id = $params['team_product_id'];
		if(empty($team_product_id)){
			
			
			exit();
		}
		
		$team_product_params_two = [
				'team_product_id'=>$team_product_id
		
		];
		$team_product_result = $this->_team_product->getTeamProduct($team_product_params_two);
		
	
		
// 		$pk_id = $this->insertGetId($data);
		//通过 公司ID 出团日期 以及团队产品编号去查询该团处于当天第几个团
		$team_product_rank = [
			'team_product_id'=>$team_product_result[0]['team_product_id'],
			'begin_time'=>date('Ymd',$team_product_result[0]['begin_time']),
			'company_id'=>$team_product_result[0]['company_id']
				
				
		];
		$team_product_rank =$this->_team_product->getTeamProductRank($team_product_rank);
		if(empty($team_product_rank)){
			$team_product_code = 1;
			
		}else{
			$team_product_code = $team_product_rank[0]['rank'];
		}

 		$team_product_code = str_pad($team_product_code,2,'0',STR_PAD_LEFT );
		error_log(print_r($team_product_code,1));
 		//再获取线路模板code
 		$route_template = new RouteTemplate();
 		$route_template_result = $route_template->getOneRouteTemplate($team_product_result[0]['route_template_id']);
 		
 		if(strlen($route_template_result['product_code'])>=3){
 			$product_code = substr($route_template_result['product_code'],0,strlen($route_template_result['product_code'])-2);
 			
 			
 			$team_product_params = [
 					'team_product_id'=>$team_product_result[0]['team_product_id'],
 					'team_product_number'=>$product_code.'-'.date('ymd',$team_product_result[0]['begin_time']).$team_product_code
 			];
 			
 			$this->_team_product->updateTeamProductBaseByTeamProductBaseId($team_product_params);
 			
 		}
		return 1;
		
	}
	
}